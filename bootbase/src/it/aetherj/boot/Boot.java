/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot;

import java.io.*;
import java.lang.reflect.Constructor;
import java.net.*;
import java.util.ArrayList;
import java.util.jar.*;

import javax.swing.UIManager;
 
/**
 * This is used to simplify jar loading
 * It also defines the logging mechanism at the beginning
 * NOTE: for all to work Boot must be on a jar by itself and ALL the rest in a separate set of jars...
 */
public final class Boot 
  {
  private static final String classname="Boot";

  // You create a manifest entry named as follow and that will be started by boot
  private static final String PROGRAM_MAIN_CLASS_KEY="program-Main-Class";
  
  /**
   * Boot starting point is here.
   * NOTE: The order of the creation of items IS relevant !
   */
  public static void main(String[] args)
    {
    Boot instance = new Boot(args);
    // This is outside new Boot() so the instance is ready.
    instance.safeRun();
    }

  private final String[]  programArgs;

  private Log       log;
  private File      programDir;
  private URL[]     runtimeClassPath;

  /**
   * I do not expect people to be able to create their own Boot Instances.
   * Boot instances can be only created by main()
   */
  private Boot ( String [] args )
    {
    programArgs = args;
    }
    
  public Log getLog ()
    {
    return log;
    }
     
  public File getProgramDir ()
    {
    return programDir;
    }

  private void safeRun ()
    {
    if ( getArgValue("-help") != null ) 
      {
      System.out.println("Boot: -redirect=false to stop redirecting out and err to the GUI");
      System.out.println("      -javaplaf       to use standard Java Look And Feel");
      return;
      }

    try 
      {
      run();
      } 
    catch (Exception exc) 
      {
      exc.printStackTrace();
      }
      
    }


    
  /**
   * Called from main, runs with a valid Boot class
   * Setup a custom classloader here.
   */
  private void run() throws Exception
    {
    boolean systemPlaf=true;
    String javaPlafReq = getArgValue("-javaplaf");
    
    if ( javaPlafReq != null && javaPlafReq.equalsIgnoreCase("true") )  
      systemPlaf = false;
    
    if ( systemPlaf )
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
  
    // UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());

    boolean redirectOutErr = true;
    String redirReq = getArgValue("-redirect");
    if ( redirReq != null && redirReq.equalsIgnoreCase("false") )    
      {
      System.out.println("Boot: stdout and stderr go to TTY console");
      redirectOutErr = false;
      }

    log = new Log(redirectOutErr);

    programDir = extractProgramDir();
    log.userPrintln(classname+".run: programDir="+programDir);

    runtimeClassPath = getLibraries(programDir);
    String programMainClass = getProgramMainClass();
    log.userPrintln(classname+".run: programMainClass="+programMainClass);

    ClassLoader bootLoader = getClass().getClassLoader();
    ClassLoader runtimeLoader = new URLClassLoader(runtimeClassPath, bootLoader);

    Class<?> mainClass = Class.forName(programMainClass, true, runtimeLoader);

    Class<?> [] paramType = { Boot.class };
    Constructor<?> constructor = mainClass.getConstructor(paramType);
    
    Object [] paramVal  = { this };
    constructor.newInstance(paramVal);
    }
 
  /**
   * Returns an array of URL describing all jars needd to run
   */
  private URL [] getLibraries(File baseDir) throws MalformedURLException
    {
    ArrayList<URL> libraryList = new ArrayList<URL>(20);
    File libDir = new File(baseDir,"lib");
    
    File [] fileList = libDir.listFiles();
    for ( int index=0; index<fileList.length; index++ )
      {
      File aFile = fileList[index];
      
      if ( ! aFile.isFile() ) continue;
      if ( ! aFile.canRead() ) continue;
      if ( ! aFile.getName().endsWith(".jar") ) continue;

      libraryList.add(aFile.toURI().toURL());
      }
      
    return libraryList.toArray(new URL[libraryList.size()] );
    }
    
	private File extractProgramDir() 
    {
    String givenDir = getArgValue ("programDir");
    
    if ( givenDir != null ) 
      return new File(givenDir);

    String bootPath = getClass().getResource("Boot.class").getPath();
    bootPath = stripExclamation(bootPath);
    bootPath = stripFileColon (bootPath);
    bootPath = decodeUrl(bootPath);
    File bootFile = new File(bootPath);
    return bootFile.getParentFile();
    }

  /**
   * Returns the boot class, the one that will be created by Boot
   * Throws an exception if it cannot find what wanted..
   */
  private String getProgramMainClass ()
    {
    JarFile aJar=null;
    
    try
      {
      String bootJar = getClass().getResource("Boot.class").getPath();
      bootJar = stripExclamation(bootJar);
      File aFile = new File(new URI(bootJar));
      aJar = new JarFile(aFile);
      Manifest aManifest = aJar.getManifest();
      Attributes mainAtt = aManifest.getMainAttributes();
      String risul = mainAtt.getValue(PROGRAM_MAIN_CLASS_KEY);
      if ( risul == null )
        throw new IllegalArgumentException("Missing "+PROGRAM_MAIN_CLASS_KEY+" into Manifest");

      return risul;
      }
    catch ( Exception exc )
      {
      log.exceptionShow(classname+".getProgramMainClass()",exc);
      return null;
      }
    finally
      {
      closeJar (aJar); 
      }
    }
   
  /**
   * Utility, strip what is after the ! if any.
   */
  private String stripExclamation ( String input )
    {
    int index = input.indexOf("!");
    if ( index < 0 ) return input;
    return input.substring(0,index);
    }


  /**
   * Utility, strip the file: ath the beginning, if any
   */
  private String stripFileColon ( String input )
    {
    if (!input.startsWith("file:")) return input;
    return input.substring(5);
    }

  /**
   * Utility, decodes this url putting real chars instead of "%xx"
   * To test it use a pathname that has spaces in it..
   */
  private String decodeUrl (String input)
    {
    try 
      {
      return java.net.URLDecoder.decode(input, "UTF-8");
      }
    catch(UnsupportedEncodingException exc) 
      {
      log.exceptionShow("decodeUrl",exc);
      return ("");
      }
    }

  /**
   * This returns the value of a given program argument paramenter.
   * the syntax is ArgName=argVlaue there is no - in the parameter If it is not found returns null
   * NOTE: I can match also -help (no = sign and no value) or similar as a shorthand for -help=true
   * @return the value, if found othervise null.
   * @param argName the name of the parameter to search for.
   */
  public String getArgValue ( String argName )
    {
    if ( argName == null ) return null;

    if ( argName.length() < 2 ) return null;
      
    if ( programArgs == null ) return null;

    if ( programArgs.length < 1 ) return null;

    String lookFor=argName+"=";
    for ( int index=0; index<programArgs.length; index++ )
      {
      String oneArg = programArgs[index];
      if ( oneArg.startsWith(lookFor) ) 
        return oneArg.substring(lookFor.length());
      }

    // I also want to be able to scan for no paramenters commends, like -help
    // In this case the returned value is true, since that option is true...
    for ( int index=0; index<programArgs.length; index++ )
      {
      String oneArg = programArgs[index];
      if ( oneArg.startsWith(argName)) return "true";
      }

    return null;
    }
    
  /**
   * The usual close exception trapped utility
   */
  private void closeJar ( JarFile aJar )
    {
    try { aJar.close(); }
    catch ( Exception exc ) { }
    }

  public String toString ()
    {
    return classname+" "+programDir;
    }
  }




