package it.aetherj.boot.dbase;

import java.sql.*;
import java.util.LinkedList;

import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.cfgproperty.PropertyGroup;

/**
 * This handles all peculiarity of the connection with the dbase
 * One of the key point is that this must use only the logger
 * Otherwise we end up in circular problems trying to setup the system.
 * 
 * Now, we are in server mode here... that means multiple clients using....
 * Well, we need really one database connection for each client, just to avoid any messing
 * Up with bits and pieces. We have to remember that this is a service for few people
 * and if we ever grow it the problem will be more in the client handling than in the
 * database....
 * 
 * SO, we need to create a Dbase for EVERY client connected.
 * THEREFORE rs closing and so on can be here (GOOD)
 * 
 * This class is not final since we may wish to have subclasses of this that add functionality
 * We therefore need to have a few things protected. 
 */
public class Dbase 
  {
  private static final String classname="Dbase.";

  public static final String HSQLDB_jclass = "org.hsqldb.jdbcDriver";
  public static final String POSTGRES_jclass = "org.postgresql.Driver";

  public static final String KEY_jdbcClass="jdbcClass";
  public static final String KEY_jdbcUrl="jdbcUrl";
  public static final String KEY_jdbcUser="jdbcUser";  
  public static final String KEY_jdbcPassword="jdbcPassword";
  public static final String KEY_dbaseSchema="dbaseSchema";     // The default schema to use for operations.
  public static final String KEY_jdbcConnTout="jsbcConnTout";   // if available JdbcConnect timeout


  protected final PrintlnProvider logger;

  private final LinkedList<Statement> statementList = new LinkedList<Statement>();

  // It is not final since I need to close/recreate it.
  // Also, try to keep it private so we can manage when to close it.
  protected volatile Connection dbConn;
  // holds the class that describes the current dbase quirks
  private DbaseQuirks dbQuirks;   
  
  /** 
   * the constructor should really do NOTHING beside allocate basic vars
   * This is because I need the system to be setup before doing any activity ! 
   * Beside this Dbase MUST use ONLY the log, I have nothing anyway setup now..
   */
  public Dbase ( PrintlnProvider logger )
    {
    this.logger = logger;
    }

  public DbaseQuirks getDbaseQuirks (  )
    {
    return dbQuirks;
    }

  /**
   * The first thing to do is to connect.
   * It is left to the caller so an error can be managed !
   * To connect to localhost use "jdbc:hsqldb:hsql://localhost"
   * Note: if connection fails the reason is written to the logger.
   * Note: This class MUST be loaded in the same classloader where all the 
   * drivers are loaded, othervise it cannot find the classes !
   * @return true if connect OK, false othervise
   */
  public final Dbase connect( PropertyGroup config ) 
    {
    String dbUrl = (String)config.getValue(KEY_jdbcUrl);
    
    try
      {
      String dbJclass = config.getValueString(KEY_jdbcClass,HSQLDB_jclass);
      // DriverManager.setLogWriter(new PrintWriter(System.out));
      Class.forName(dbJclass);
      
      // map the dbase quirks appropriately
      dbQuirks = POSTGRES_jclass.equals(dbJclass) ? new DbaseQuirksPostgress(dbJclass, "Postgress") : new DbaseQuirks(dbJclass, "HSQLDB");
      
      String jdbcUser = config.getValueString(KEY_jdbcUser,"guest");
      String jdbcPassword = config.getValueString(KEY_jdbcPassword,"empty");
      
      Integer cTmout = (Integer)config.getValue(KEY_jdbcConnTout);
      if ( cTmout != null ) DriverManager.setLoginTimeout(cTmout.intValue());
      
      dbConn = DriverManager.getConnection(dbUrl,jdbcUser,jdbcPassword);

      return this;
      }
    catch ( Exception exc )
      {
      logger.println(classname+"connect() dbUrl="+dbUrl,exc);

      return null;
      }
    }


  public final boolean isConnected ()
    {
    if ( dbConn == null )
      return false;
    
    try
      {
      return ! dbConn.isClosed();
      }
    catch ( Exception exc )
      {
      return false;
      }
    }
    
  /**
   * Subclasses must be able to provide their own compatible Ris.
   * You have to extends a Dbase to get things working and provide a way
   * to create a Ris.
   */
  protected Ris newRis ( Dbase dbase, Statement statement, int updateCount )
    {
    return new Ris (dbase,statement,updateCount);
    }

  protected Ris newRis ( Dbase dbase, Statement statement, Exception exc )
    {
    return new Ris (dbase,statement,exc);
    }
    
  protected Brs newBrs ( Dbase dbase, Statement statement, ResultSet rs )
    {
    return new Brs( dbase, statement, rs);
    }
  
  protected Brs newBrs ( Dbase dbase, Statement statement, Exception exc )
    {
    return new Brs( dbase, statement, exc );
    }
  
  /**
   * Return true if the dbase is closed, maybe connection did not succeeed.
   * @return true if closed false if open.
   */
  public final boolean isClosed ()
    {
    try
      {
      return dbConn.isClosed();
      }
    catch ( Exception exc )
      {
      return true;
      }
    }
    
  /**
   * Use this when you have done with this database connection.
   * Closes all still open statements and then close the connection.
   * Note that you may still repoen it using the connect, if you wish.
   */
  public final void close ()
    {
    // no connection, nothing to do at all
    if ( dbConn == null ) return;

    try
      {
      // if connection is closed there is nothing to do !
      if ( dbConn.isClosed() ) return;
 
      // Close possibly still open statements.
      closeStatementList();

      // Close the connection
      dbConn.close();
      }
    catch ( Exception exc )
      {
      logger.println(classname+"close()",exc);
      }
    finally
      {
      // in ANY case I must mark the connection as gone
      dbConn=null;      
      }

    }

  /**
   * Closes all resultsets used on this time around.
   * This MUST be called every time a client has finished to handle a batch of requests.
   */
  public final void closeStatementList ()
    {
    int listSize = statementList.size();
    for ( int index=0; index<listSize; index++ )
      {
      try
        {
        // The try is for each close attempt, since I wish to try to close all of them.
        Statement aStatement = statementList.get(index);
        aStatement.close();
        }
      catch ( Exception exc )
        {
        logger.println(classname+"closeStatementList()",exc);
        }
      }
    statementList.clear();
    }

  /**
   * Explicitly closes a statement, this also means that it can be removed from thelist
   * of satements to be closed.
   * NOTE: This MUST be called from the SAME thread that called the adding.
   */
  public final void closeStatement ( Statement aStatement )
    {
    // It may happen if someone calls a close for a statement that did not got created
    // It is really not a big deal and not a reason to generate an error.
    if ( aStatement == null ) return;

    try
      {
      aStatement.close();
      statementList.remove(aStatement);
      }
    catch ( Exception exc )
      {
      logger.println(classname+"closeStatement()",exc);
      }
    }


  /**
   * Add a bRs to the list of things to be closed by this dbase, sooner or later.
   * This MUST be called by the same tread that will do the close !
   */
  protected final void addStatementToClose ( Statement aStatement )
    {
    // There is really no point at all to add a null statement
    if ( aStatement == null ) return;
    
    statementList.add(aStatement);
    }

  /**
   * This is used so I can add generic parameters to all calls.
   * This is for generic statements written as a string.
   * NOTE: This adds the resulting statement to the list of statements to close.
   */
  public Statement getStatement () throws SQLException
    {
    Statement risul = dbConn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
    addStatementToClose ( risul );
    return risul;
    }

  /**
   * This is used so I can add generic paramenters to all calls.
   * This makes a prepared statement, one that uses the ? to specify params.
   * NOTE: This adds the resulting statement to the list of statements to close.
   */
  public Dprepared getPreparedStatement ( String statement ) throws SQLException
    {
    PreparedStatement risul = newPreparedStatement(statement);
    return new Dprepared(this,risul);    
    }

  /**
   * This is used so I can add generic parameters to all calls.
   * This makes a prepared statement, one that uses the ? to specify params.
   * NOTE: This adds the resulting statement to the list of statements to close.
   */
  public Dprepared getPreparedStatementReturnKeys (String statement ) throws SQLException
    {
    PreparedStatement risul = newPreparedStatementReturnKeys(statement);
    return new Dprepared(this,risul);
    }

  /**
   * Subclasses may need this to create better Dprepared
   * @param statement
   * @param options
   * @return
   * @throws SQLException
   */
  protected PreparedStatement newPreparedStatement ( String statement ) throws SQLException
    {
    PreparedStatement risul = dbConn.prepareStatement(statement,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY );
    addStatementToClose ( risul );
    return risul;
    }
    
  protected PreparedStatement newPreparedStatementReturnKeys ( String statement ) throws SQLException
    {
    PreparedStatement risul = dbConn.prepareStatement(statement,PreparedStatement.RETURN_GENERATED_KEYS);
    addStatementToClose ( risul );
    return risul;
    }

  /**
   * This is mostly used by the BrsCopy but the idea is quite general.
   * Let's say you have a single dbase connection shared between multiple threads.
   * Normaly such a connection is not multithread capable, but you want one capable.
   * What you do is to use this method to create an order of execution
   * Tested 10/03/05 Damiano, mettendo uno sleep per fare durare l'operazione
   * @param statement
   */
  public final synchronized int[] synchronizedExecuteBatch ( PreparedStatement statement ) throws SQLException
    {
    return statement.executeBatch();
    }

  public DatabaseMetaData getMetaData () throws SQLException
    {
    return dbConn.getMetaData();
    }

  /**
   * This returns Brs with the tables availabe to this database.
   * I may need to add a few paramenters to this one, we will see.
   * @param schemaPattern a schema name pattern; must match the schema name
   *        as it is stored in the database; "" retrieves those without a schema;
   *        <code>null</code> means that the schema name should not be used to narrow
   *        the search
   */
  public final Brs getTables ( String schemaPattern )
    {
    try
      {
      DatabaseMetaData meta = getMetaData();
      return newBrs (this, null, meta.getTables(null,schemaPattern,null,null));        
      }
    catch ( Exception exc )
      {
      return newBrs (this, null, exc);        
      }
    }
    
  /**
   * This returns Brs with the primary keys for the given table.
   * Again, this may need to have paramenters...
   */
  public final Brs getPrimaryKeys ( String tableName )
    {
    try
      {
      DatabaseMetaData meta = getMetaData();
      return newBrs (this, null, meta.getPrimaryKeys(null,null,tableName));        
      }
    catch ( Exception exc )
      {
      return newBrs (this, null, exc);        
      }
    }    
    

  /**
   * Returns the name of the primary column. at the moment we handle only one :-)
   * @return a string holding the primary key for a given table.
   */
  public final String getPrimaryKeyName (String tableName)
    {
    String risul = "";
    
    Brs aRs = getPrimaryKeys(tableName.toUpperCase());
    if ( aRs.next() ) risul=aRs.getString("COLUMN_NAME");
    aRs.close();
    
    return risul;
    }



  /**
   * This returns Brs with a table schema of the given table
   */
  public final Brs getTableSchema ( String tableName )
    {
    try
      {
      DatabaseMetaData meta = getMetaData();
      String upperName = tableName.toUpperCase();
      return newBrs (this, null, meta.getColumns(null,null,upperName,null));        
      }
    catch ( Exception exc )
      {
      return newBrs (this, null, exc);        
      }
    }    


  /**
   * A generic select for quick use.
   * @param expression a generic select expression
   * @return a Brs that you can extract information.
   */
  public final Brs select( String expression) 
    {
    Statement statement=null;
    try
      {
      statement = getStatement();
      return newBrs (this,statement, statement.executeQuery(expression));
      }
    catch ( Exception exc )
      {
      return newBrs (this, statement, exc);        
      }
    }


  /**
   * Use this whan you know you are doing an update.
   * NOTE: a close is done right after the update.
   */
  public final Ris update(String expression) 
    {
    Statement statement=null;
    
    try
      {
      statement = getStatement();
      Ris result = new Ris(this, statement, statement.executeUpdate(expression)); 
      result.close();
      return result;
      }
    catch ( Exception exc )
      {
      return new Ris (this, statement, exc);
      }
    }

  /**
   * Use this when you do not know if the result is a Resultset or an integer
   * You cannot close it since you may want to get a resultset from it !
   */
  public final Ris execute(String expression) 
    {
    Statement statement=null;
    
    try
      {
      statement = getStatement();
      return new Ris(this, statement, statement.execute(expression)); 
      }
    catch ( Exception exc )
      {
      return new Ris (this, statement, exc);
      }
    }




  /**
   * This returns the next index from a given column/table, it is NOT atomic !!!!
   * @return the default value if some failure
   * @param tbl_name the table name to work on.
   * @param col_name the column name that is normally the primary key, but not necessarily.
   * @param defValue the value to return if something fails
   */
  public final int getNextIndex ( String tbl_name, String col_name, int defValue )
    {
    Integer newid=null;
    Brs brs = select ("SELECT MAX("+col_name+")+1 FROM "+tbl_name);
    if ( brs.next() ) newid=brs.getInteger(1);
    brs.close();

    // Normally we exit with this one.
    if ( newid != null ) return newid.intValue();

    // But the first record being inserted uses this.
    return defValue;
    }


  /**
   * Returns true if the given string is not null and not empty
   */
  final boolean notEmpty ( String aStr )
    {
    if ( aStr == null ) return false;
    if ( aStr.length() < 1 ) return false;
    
    return true;
    }

  /**
   * Converts a java.util.Date into a java.sql.Date.
   * If input == null then null is returned.
   */
  public final java.sql.Date toSqlDate ( java.util.Date input )
    {
    if ( input == null ) return null;
    
    return new java.sql.Date(input.getTime());
    }
    
  /**
   * Utility, to make code simpler.
   * NOTE: null is NOT a valid value for id_value, never !
   * Beside the fact that an id is always not null using a null as value would return an empty set
   * and this is not what we want.
   * NOTE: It is possible that this method returns an empty set if the ID is not found !
   * @return a Brs that can be empty is the match is not found.
   */
  public final Brs getThisRow ( String tbl_name, String id_colname, Integer id_value ) throws SQLException 
    {
    String query= "SELECT * FROM "+tbl_name+" WHERE "+id_colname+"=?";
    Dprepared statement = getPreparedStatement(query);
    statement.setInt(1,id_value);
    return statement.executeQuery(); 
    }

   
  /**
   * Gives some information on this object.
   * @return
   */
  public String toString ()
    {
    return "Dbase dbConn="+dbConn;
    }
    
    
    
  } 

