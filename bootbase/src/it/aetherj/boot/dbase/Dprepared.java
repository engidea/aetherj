package it.aetherj.boot.dbase;

import java.math.BigDecimal;
import java.sql.*;


/**
 * This hides a prepared statement so you can do work with objects instead of native types.
 * NOT final anymore, so you can add some setters to handle custom types
 */
public class Dprepared
  {  
  private final Dbase dbase;
  private final PreparedStatement prepared;
  
  public Dprepared(Dbase dbase, PreparedStatement prepared)
    {
    this.dbase = dbase;
    this.prepared = prepared;
    }
  
  public PreparedStatement getPreparedStatement()
    {
    return prepared;
    }
  
  public Dprepared setNull ( int position, int type ) throws SQLException
    {
    prepared.setNull(position,type);
    return this;
    }

  public Dprepared setInt ( int position, int value ) throws SQLException
    {
    prepared.setInt(position,value);
    return this;
    }
    
  public Dprepared setLong ( int position, long value ) throws SQLException
    {
    prepared.setLong(position,value);
    return this;
    }

  public Dprepared setInt ( int position, Integer value ) throws SQLException
    {
    if ( value == null )
      prepared.setNull(position,Types.INTEGER);
    else
      prepared.setInt(position,value.intValue());
      
    return this;
    }
  
  public Dprepared setDouble ( int position, Double value ) throws SQLException
    {
    if ( value == null )
      prepared.setNull(position,Types.DECIMAL);
    else
      prepared.setDouble(position,value.doubleValue());
      
    return this;
    }

  public Dprepared setString ( int position, String value ) throws SQLException
    {
    if ( value == null )
      prepared.setNull(position,Types.VARCHAR);
    else
      prepared.setString(position,value);
      
    return this;
    }
  
  /**
   * Sets the current field as a timestamp with value "now" 
   * @param position
   * @return
   * @throws SQLException
   */
  public Dprepared setTimestamp ( int position ) throws SQLException
    {
    prepared.setTimestamp(position,new Timestamp(System.currentTimeMillis()));

    return this;
    }
  
  public Dprepared setTimestamp ( int position, Timestamp value ) throws SQLException
    {
    if ( value == null )
      prepared.setNull(position,Types.TIMESTAMP);
    else
      prepared.setTimestamp(position,value);
      
    return this;
    }

  public Dprepared setBoolean ( int position, Boolean value ) throws SQLException
    {
    if ( value == null )
      prepared.setNull(position,Types.BOOLEAN);
    else
      prepared.setBoolean(position,value.booleanValue());
      
    return this;
    }

  public Dprepared setBoolean ( int position, boolean value ) throws SQLException
    {
    prepared.setBoolean(position,value);
    return this;
    }


  public Dprepared setBigDecimal ( int position, BigDecimal value ) throws SQLException
    {
    if ( value == null )
      prepared.setNull(position,Types.DECIMAL);
    else
      prepared.setBigDecimal(position,value);
      
    return this;
    }

  public Dprepared setBinary ( int position, byte [] value ) throws SQLException
    {
    if ( value == null )
      prepared.setNull(position,Types.BINARY);
    else
      prepared.setBytes(position,value);
      
    return this;
    }

  public Dprepared setObject ( int position, Object value ) throws SQLException
    {
    if ( value == null )
      prepared.setNull(position,Types.NULL);
    else
      prepared.setObject(position,value);
      
    return this;
    }

  public Brs executeUpdateReturnKeys() throws SQLException
    {
    int risul =  prepared.executeUpdate();
    return new Brs(dbase,prepared,prepared.getGeneratedKeys());
    }

  public int executeUpdate () throws SQLException
    {
    return prepared.executeUpdate();
    }

  public Brs executeQuery () throws SQLException
    {
    return new Brs(dbase,prepared,prepared.executeQuery());
    }

  /**
   * This is like the executeQuery but it ALWAYS return a Brs even when it fails.
   * In case of error the returned Brs will be empty and holding an exception.
   * You still have to close it !
   * @return
   */
  public Brs executeQueryTry ()
    {
    try
      {
      return new Brs(dbase,prepared,prepared.executeQuery());
      }
    catch ( Exception exc )
      {
      return new Brs(dbase,prepared,exc);
      }
    }
    
  public void close ()
    {
    dbase.closeStatement(prepared);
    }
  }
  
