package it.aetherj.boot.dbase;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Date;

import it.aetherj.boot.Log;


/**
 * It is useful to have the two types of constructor anyway since I may want to handle exception
 * or I may not want to do it.
 */
public class Brs 
  {
  private static final String classname="Brs.";

  protected final Dbase     brsDbase;
  protected final ResultSet brsRs;
  protected final Statement brsStatement;
  protected final Exception brsException;
  protected final boolean   isEmptyBrs;      // This signal that this Brs is an empty one

  private boolean closed=false;
  
  /**
   * Use this when you have to return a Brs but there is nothing to return.
   */
  public Brs (Dbase myDbase, Statement aStatement)
    {
    isEmptyBrs = true;
    brsDbase = myDbase;
    brsStatement = aStatement;
    brsRs = null;
    brsException = null;
    }
    
  /**
   * Use this when you have a valid resultset to return.
   * @param aRs
   * @param aStatement
   * @param dbase
   */
  public Brs(Dbase dbase, Statement aStatement, ResultSet aRs )
    {
    brsDbase = dbase;
    brsStatement = aStatement;
    brsRs  = aRs;
    brsException = null;
    isEmptyBrs = false;
    }


  /**
   * Use this when an exception occurred, no resultset but an exception to report.
   * @param exc
   * @param aStatement
   * @param myDbase
   */
  public Brs (Dbase myDbase, Statement aStatement, Exception exc)
    {
    brsDbase = myDbase;
    brsStatement = aStatement;
    brsRs  = null;
    brsException = exc;
    isEmptyBrs = true;
    }

  /**
   * If you want to make a Brs from another Brs, used when you want to share a brs content
   * during subclassing.
   * @param parentBrs it MUST be a non empty, not failed brs.
   */
  public Brs ( Brs parentBrs )
    {
    this(parentBrs.brsDbase,parentBrs.brsStatement,parentBrs.brsRs);
    }
    
    


  /**
   * Returns the row or < 0 if no row selected.
   */
  public final int getRow()
    {
    try
      {
      return brsRs.getRow();
      }
    catch ( SQLException exc )
      {
      return -1;
      }
    }

  /**
   * Put the cursor on the given row.
   * If all goes well it returns the same row else -1;
   */
  public final int absolute ( int rowIndex )
    {
    try
      {
      if ( rowIndex == 0 ) 
        {
        brsRs.beforeFirst();
        return 0;
        }
        
      if ( brsRs.absolute(rowIndex)) return rowIndex;
      }
    catch ( SQLException exc )
      {
      return -1;  // This is for generic database Error
      }

    // This is for when I try to be in an impossible place
    return -1;    
    }

  /**
   * Go to the last record available and return the index of it.
   * The first row is index 1, no row is 0
   * If an error occurs it will return 0
   */
  public final int last ()
    {
    try 
      { 
      brsRs.last(); 
      return brsRs.getRow();
      }
    catch ( SQLException exc ) 
      { 
      return 0;
      }
    }
    
  /**
   * For a scrllable resultset this moves the pointer as if nothing has been read.
   * NOTE: No exception is thrown if something wird happens...
   */
  public final void beforeFirst ()
    {
    try 
      { 
      brsRs.beforeFirst(); 
      }
    catch ( SQLException exc ) 
      { 
      return;
      }
    }

  /**
   * Get the given column as an object
   */
  public final Object getObject ( int column )
    {
    try
      {
      return brsRs.getObject(column);
      }
    catch ( SQLException exc )
      {
      return null;
      }
    }


  /**
   * Get the given column as a bytep[
   */
  public final byte[] getBytes ( String column )
    {
    try
      {
      return brsRs.getBytes(column);
      }
    catch ( Exception exc )
      {
      return new byte[0];
      }
    }

  /**
   * This handles the exception as a false.
   * I need to give some more information on where it failed since otherwise I have no result at all
   * and I don't know why. For the fields... well, I really would like to know if I am hitting the 
   * wrong field name...
   */
  public final boolean next ()
    {
    try
      {
      // if I already know it is empty then forget about it.
      if ( isEmptyBrs ) return false;
      
      return brsRs.next();  
      }
    catch ( Exception exc )
      {
      System.err.println("Brs.next(): Exception="+exc.toString());
      exc.printStackTrace();
      return false;
      }
    }

  public final boolean isClosed ()
    {
    return closed;
    }
    
  /**
   * Note that the documentation says that this method may be not available
   * @return
   */
  public final boolean isLast ()
    {
    try
      {
      // if I already know it is empty then forget about it.
      if ( isEmptyBrs ) return true;
      
      return brsRs.isLast();  
      }
    catch ( Exception exc )
      {
      System.err.println("Brs.isLast(): Exception="+exc.toString());
      exc.printStackTrace();
      return true;
      }
    }

  
  public final String getString (String fieldName)
    {
    try
      {
      return brsRs.getString(fieldName);
      }
    catch ( Exception exc )
      {
      System.err.println("Brs.getString(): Exception="+exc.toString());
      return null;
      }
    }

  public final String getString (int fieldIndex)
    {
    try
      {
      return brsRs.getString(fieldIndex);
      }
    catch ( Exception exc )
      {
      System.err.println("Brs.getString(): Exception="+exc.toString());
      return null;
      }
    }


  /**
   * There are cases when it is irrelevant to know if the original value was null
   * @return default if the content was null or if there was an error, the value othervise.
   * @param fieldName the name of the field to get.
   */
  public final int getInteger (  String fieldName, int defaultVal )
    {
    try
      {
      int risul = brsRs.getInt(fieldName);
      if ( brsRs.wasNull() ) return defaultVal;
      return risul;
      }
    catch ( SQLException exc )
      {
//      System.err.println("Brs.getInteger(): Exception="+exc.toString());
      System.err.println(Log.exceptionExpand("Brs.getInteger: fieldName="+fieldName, exc));
      return defaultVal;
      }
    }

  /**
   * There are cases when it is irrelevant to know if the original value was null
   * @return default if the content was null or if there was an error, the value othervise.
   * @param fieldName the name of the field to get.
   */
  public final long getLong (  String fieldName, long defaultVal )
    {
    try
      {
      long risul = brsRs.getLong(fieldName);
      if ( brsRs.wasNull() ) return defaultVal;
      return risul;
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getLong(): Exception="+exc.toString());
      return defaultVal;
      }
    }



  /**
   * This returns an integer since we really want to know if the read was null.
   * @return null if the content was null or if there was an error, the value othervise.
   * @param fieldName the name of the field to get.
   */
  public final Integer getInteger (  String fieldName )
    {
    try
      {
      int risul = brsRs.getInt(fieldName);
      if ( brsRs.wasNull() ) return null;
      return Integer.valueOf(risul);
      }
    catch ( SQLException exc )
      {
//      System.err.println("Brs.getInteger(): Exception="+exc.toString());
      System.err.println(Log.exceptionExpand("Brs.getInteger: fieldName="+fieldName, exc));
      return null;
      }
    }


  /**
   * This returns a Double and can be used with GWT since javascript does not support BigDecimal
   * @return null if the content was null or if there was an error, the value othervise.
   * @param fieldName the name of the field to get.
   */
  public final Double getDouble ( String fieldName )
    {
    try
      {
      double risul = brsRs.getDouble(fieldName);
      if ( brsRs.wasNull() ) return null;
      return Double.valueOf(risul);
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getDouble(): Exception="+exc.toString());
      return null;
      }
    }

  /**
   * This returns an integer since we really want to know if the read was null
   */
  public final Integer getInteger (  int fieldIndex )
    {
    try
      {
      int risul = brsRs.getInt(fieldIndex);
      
      if ( brsRs.wasNull() ) 
        return null;
      
      return Integer.valueOf(risul); 
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getInteger(i): Exception="+exc.toString());
      return null;
      }
    }

  /**
   * This returns an int since I am more interested to have a reasonable value
   */
  public final int getInteger (  int fieldIndex, int default_value )
    {
    try
      {
      int risul = brsRs.getInt(fieldIndex);
      
      if ( brsRs.wasNull() ) 
        return default_value;
      
      return risul; 
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getInteger(i,d): Exception="+exc.toString());

      return default_value;
      }
    }

  /**
   * Obsolete, not really appropriate. Use getTimestamp.
   * @Deprecated use getTimestampInstead
   * @param colName
   * @return
   */
  public final Date getDate ( String colName )
    {
    try
      {
      return brsRs.getTimestamp(colName);
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getDate(): Exception="+exc.toString());
      return null;
      }
    }

  public final Date getDate (int fieldIndex )
    {
    try
      {
      return brsRs.getTimestamp(fieldIndex);
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getDate(i): Exception="+exc.toString());
      return null;
      }
    }

  
  public final Timestamp getTimestamp ( String colName )
    {
    try
      {
      return brsRs.getTimestamp(colName);
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getDate(): Exception="+exc.toString());
      return null;
      }
    }



  public final BigDecimal getBigDecimal ( String colname )
    {
    try
      {
      return brsRs.getBigDecimal(colname);
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getBigDecimal(string): Exception="+exc.toString());
      return null;
      }
    }

  public final BigDecimal getBigDecimal ( int colIndex )
    {
    try
      {
      return brsRs.getBigDecimal(colIndex);
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getBigDecimal(int): Exception="+exc.toString());
      return null;
      }
    }

  /**
   * Returns a Boolean.
   * Note that if the original value was null then null is returned.
   */
  public final Boolean getBoolean ( String col )
    {
    try
      {
      boolean risul = brsRs.getBoolean(col);
      if ( brsRs.wasNull() ) return null;
      return Boolean.valueOf(risul);
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getBoolean(col): Exception="+exc.toString());
      return null;
      }
    }

  /**
   * Returns a boolean.
   * Note that if the origina value was null then default is returned.
   */
  public final boolean getBoolean ( String col, boolean defval )
    {
    try
      {
      boolean risul = brsRs.getBoolean(col);
      if ( brsRs.wasNull() ) return defval;
      return risul;
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getBoolean(col,defval): Exception="+exc.toString());
      return defval;
      }
    }


  /**
   * Returns a Boolean
   * Note that if the origina value was null then null is returned.
   */
  public final Boolean getBoolean ( int colIndex )
    {
    try
      {
      boolean risul = brsRs.getBoolean(colIndex);
      if ( brsRs.wasNull() ) return null;
      return Boolean.valueOf(risul);
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getBoolean(int): Exception="+exc.toString());
      return null;
      }
    }

  /**
   * Returning a Time
   */
  public final java.sql.Time getTime (int fieldIndex )
    {
    try
      {
      return brsRs.getTime(fieldIndex);
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getTime(i): Exception="+exc.toString());
      return null;
      }
    }

  /**
   * Returning a Time
   */
  public final java.sql.Time getTime ( String colName )
    {
    try
      {
      return brsRs.getTime(colName);
      }
    catch ( SQLException exc )
      {
      System.err.println("Brs.getTime(): Exception="+exc.toString());
      return null;
      }
    }

  /**
   * The resultset has failed even if there is no values to returns.
   * @return 
   */
  public final boolean hasFailed ()
    {
    // If the Brs is empty, built empty, then it is not failed, it is empty...
    if ( isEmptyBrs ) return false;
    
    if ( brsException != null ) return true;
    
    return brsRs == null;
    }

  /**
   * Returns the exception that has been caught.
   * @return the exception or null if no exception.
   */
  public final Exception getException ()
    {
    return brsException;
    }

  /**
   * For all other cases when you need the raw resultset you can use this one..
   */
  public final ResultSet getResultSet ()
    {
    return brsRs;
    }


  /**
   * Close is needed to clear up resources
   */
  public final void close () 
    {
    try
      {
      closed = true;
      if ( brsStatement != null ) 
        brsDbase.closeStatement(brsStatement);
      else if ( brsRs != null ) 
        brsDbase.closeStatement(brsRs.getStatement());
      }
    catch ( Exception exc )
      {
      System.err.println(classname+"close() exc="+exc);
      exc.printStackTrace();
      }
    }

  /**
   * Return the metadata of a resultset, so you can display it
   */
  public final ResultSetMetaData getMetaData()
    {
    try
      {
      return brsRs.getMetaData();
      }
    catch ( Exception exc )
      {
      System.err.println(classname+"getMetaData() exc="+exc);
      return null;
      }
    }

  }