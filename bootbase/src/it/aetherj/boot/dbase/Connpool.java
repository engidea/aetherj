package it.aetherj.boot.dbase;

import java.util.ArrayList;

import it.aetherj.boot.PrintlnProvider;

/**
 * It happens quite often to desire a connection pool and a reliable one.
 * Also, I have a nice architecture that I wish to extend...
 */
public abstract class Connpool 
  {
  private static final String classname="Connpool.";

  private final PrintlnProvider log;
  private final ArrayList<Dbase> freeList;
  private final ArrayList<Dbase> usedList;
  
  private boolean reallyCloseConnection = false;
  
  public Connpool( PrintlnProvider log )
    {
    this.log = log;
    
    freeList = new ArrayList<Dbase>();
    usedList = new ArrayList<Dbase>();
    }
  
  public void setReallyCloseConnection( boolean reallyCloseConnection )
    {
    this.reallyCloseConnection = reallyCloseConnection;      
    }
  
  /**
   * You may wish to know how big the free size is.
   */
  public final int getFreeCount ()
    {
    return freeList.size();
    }

  /**
   * This class must be extended so the subclass provides a way to create new
   * dbase objects when needed. I need this since I really wish to be able to extend
   * a Dbase object to provide my own functionalities.
   */
  protected abstract Dbase newConnectedDbase ();
  
  /**
   * Returns an existing Dbase object if one is free, othervise a new one.
   * Note that the new connection should be a connected one if it an exception 
   * must be thrown.
   * @return a new or reused dbase object or null if it cannot be created.
   */
  public synchronized final Dbase getDbase ()
    {
    int freeSize = freeList.size();
    Dbase risul;
    
    if (  freeSize < 1 )
      {
      // There is nothing free available, create a new one and add it to the used.
      risul = newConnectedDbase();
      }
    else
      {
      // Get and remove the last element in the list.
      risul = freeList.remove(freeSize-1);

      // if dbase is closed let me get a new one...
      if ( risul.isClosed() ) 
        {
        log.println(classname+"getDbase: reopening a closed DB");
        risul = newConnectedDbase();
        }
      }

    // If newConnectedDbase fails, it will return null, do not add null to thelist.
    if ( risul != null ) usedList.add(risul);
    
    return risul;
    }
    
  /**
   * Once a client has done using the dbase it MUST release it.
   * releasin a dbase will commit all that is pending.
   * releasing a Dbase will close all statement list possibly pending close.
   */
  public synchronized final void releaseDbase( Dbase dbase )
    {    
    if ( dbase == null ) return;
    
    dbase.closeStatementList();
    usedList.remove(dbase);
  
    if ( reallyCloseConnection ) dbase.close();

    if ( ! dbase.isClosed() ) 
      {
      // if the dbase is not closed then add it back to the freeList of connection available
      freeList.add(dbase);
      }
    }
    
  /**
   * When you are exiting you have to close all open connections.
   */
  public synchronized final void close()
    {
    for (int index=0; index<usedList.size(); index++)
      {
      Dbase dbase = usedList.get(index);
      dbase.close();
      }

    for (int index=0; index<freeList.size(); index++)
      {
      Dbase dbase = freeList.get(index);
      dbase.close();
      }

    usedList.clear();
    freeList.clear();
    }
  
  }