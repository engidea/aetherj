/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot;

public class KeyValueInteger implements KeyValueInterface<Integer>
  {
  public final Integer key;
  public Object value;

  public KeyValueInteger ( Integer key, Object value )
    {
    this.key = key;
    this.value = value;
    }
    
  public KeyValueInteger ( int key, Object value )
    {
    this.key = Integer.valueOf(key);
    this.value = value;
    }

  /**
   * There is a need to have a uniform interface to both the IdValue and KeyValue 
   * so the code could be somewhat simpler.
   * @return 
   */
  public Integer getKey ()
    {
    return key;
    }
    
  public boolean equals (Object obj)
    {
    if ( obj == null ) return false;
    
    if ( ! ( obj instanceof KeyValueInteger ) ) return false;
		
		KeyValueInteger anobj = (KeyValueInteger)obj;

		if ( ! isEqualKey(anobj.key )) return false;
		
		if ( ! isEqualValue(anobj.value )) return false;

		return true;
    }
    
		
	/**
	 * NUll keys are considered equals
	 * @param akey
	 * @return
	 */
	private boolean isEqualKey ( Integer akey )
		{
		// are both keys null ?
		if ( akey == null && key == null ) return true;
		
		// is one of teh key null ?
		if ( akey == null || key == null ) return false;
		
		return akey.equals(key);
		}
		
	private boolean isEqualValue ( Object avalue )
		{
		if ( avalue == null && value == null ) return true;
		
		// is one of teh value null ?
		if ( avalue == null || value == null ) return false;
		
		// now both values are non null
		return avalue.equals(value);
		}	
	
  public String toString ()
    {
    if ( value == null ) return null;
    
    return value.toString();
    }
  } 
