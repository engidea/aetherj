/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot;

/**
 * Almost everywhere there is the need to store a key and a value
 * Now fully generic !
 */
public class KeyValue<E> implements KeyValueInterface<E>
  {
  public final E key;
  public Object value;

  public KeyValue ( E key, Object value )
    {
    this.key = key;
    this.value = value;
    }
    
  /**
   * There is a need to have a uniform interface to both the IdValue and KeyValue 
   * so the code could be somewhat simpler.
   * @return 
   */
  public E getKey ()
    {
    return key;
    }
    
  public boolean equals (Object obj)
    {
    if ( obj == null ) return false;
    
    if ( obj instanceof KeyValue )   
      {
      KeyValue<E> anobj = (KeyValue<E>)obj;
      return key.equals(anobj.key);
      }
      
    return false;
    }
    
  public String toString ()
    {
    if ( value == null ) return null;
    
    return value.toString();
    }
  } 
