/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

import java.awt.*;

import javax.swing.*;

public class IntegerProperty extends PropertyInterface
  {
  private static final String classname="IntegerProperty.";
  
  private final String key;
  private final JPanel workPanel;
  protected final JTextField inputField;
  protected final Integer defaultValue;
  protected Integer value;
  
  public IntegerProperty (String key, String label, Integer defaultValue, int columns )
    {
    if ( defaultValue == null )
      throw new IllegalArgumentException (classname+"IntegerProperty() defaultValue MUST be not null");
      
    this.defaultValue = defaultValue;
    this.key = key;

    workPanel = new JPanel(new BorderLayout());
    workPanel.add(new JLabel(label),BorderLayout.WEST);
    
    inputField=new JTextField(""+defaultValue,columns);
    inputField.setBackground(Color.cyan);

    workPanel.add(inputField, BorderLayout.CENTER);

    value = defaultValue;
    }
    
  /**
   * This is what will go into the main panel.
   */
  public JComponent getComponentToDisplay() 
    {
    return workPanel;
    }

  public String getKey ()
    {
    return key;
    }

  /**
   * Implements the interface.
   */
  public void setStringValue ( String stringValue )
    {
    value = convertStringToInteger(stringValue);
    inputField.setText(""+value);
    }
  
  /**
   * Implements the interface.
   */
  public String getStringValue ()
    {
    value = convertStringToInteger(inputField.getText());
    return value.toString();
    }

  /**
   * Implements the interface.
   */
  public Integer getValue ()
    {
    value = convertStringToInteger(inputField.getText());
    inputField.setText(""+value);
    return value;
    }

  /**
   * Try to convert what the "user" or the system says to an Integer.
   * If this is not possible the default value will be returned.
   */
  private final Integer convertStringToInteger (String stringValue)
    {
    try 
      {
      // decode can decode also hexadecimal numbers.
      return Integer.decode(stringValue);
      }
    catch ( Exception exc )
      {
      return defaultValue;
      }
    }
    
    
  }
