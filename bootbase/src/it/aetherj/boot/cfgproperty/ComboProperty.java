/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.*;

import it.aetherj.boot.KeyValueInterface;

/**
 * Provides a property that can be selected using a combo.
 * The issue here is to handle the returned object, that I believe should be just
 * the object being stored in the combo, all of it !
 * The caller should know what the object is and act accordingly.
 */
public final class ComboProperty extends PropertyInterface
  {
  private static final String classname="ComboProperty.";
  
  private final String key;
  private final JPanel workPanel;
  private final JComboBox inputField;
  private Object currentObject; 

  /**
   * The default value can be a string even if we have a Vector of values made of different values.
   * In any case values must be a vector made of items that implements KeyValueInterface
   */
  public ComboProperty(String key, String label, String defaultValue, Vector items )
    {
    this.key = key;
    
    if ( (items.size() > 0) && !(items.firstElement() instanceof KeyValueInterface) )
      throw new IllegalArgumentException(classname+"ComboProperty: items must be a KeyValueInterface");
      
    workPanel = new JPanel(new BorderLayout());
    workPanel.add(new JLabel(label),BorderLayout.WEST);
    workPanel.add(inputField=new JComboBox(items));
    setSelectedItem ( defaultValue );
    currentObject = inputField.getSelectedItem();
    }
    
  /**
   * This is what will go into the main panel.
   */
  public JComponent getComponentToDisplay() 
    {
    return workPanel;
    }

  /**
   * Implements the interface.
   */
  public String getKey ()
    {
    return key;
    }

  /**
   * Implements the interface.
   */
  public void setStringValue ( String value )
    {
    setSelectedItem ( value );
    currentObject = inputField.getSelectedItem();
    }
  
  /**
   * Implements the interface.
   */
  public String getStringValue ()
    {
    currentObject = inputField.getSelectedItem();

    if ( currentObject == null ) return "";

    return ((KeyValueInterface<String>)currentObject).getKey();
    }

  /**
   * Implements the interface.
   */
  public Object getValue ()
    {
    return currentObject;
    }


  /**
   * This sets the given Key as the selected item.
   * If key is null nothing happens.
   */
  private void setSelectedItem ( String key )
    {
    if ( key == null ) return;
    
    for (int index=0; index < inputField.getItemCount(); index ++ )
      {
      KeyValueInterface aKey = (KeyValueInterface) inputField.getItemAt(index);
      if ( key.equals(aKey.getKey()) ) 
        {
        inputField.setSelectedIndex(index);
        break;
        }
      }
    }
  
    
    
  }
