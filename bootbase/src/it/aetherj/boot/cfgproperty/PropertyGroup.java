/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

import java.util.*;

import javax.swing.JComponent;


/**
 * The idea is that you can ground a set of properties into a "panel", this is most useful
 * when you have some recuring poperties and you need to keep the same name of the poperty but
 * need more than one. An example is the set of property to connect to the database, if you have
 * moe than one DB you need more than one set of them.
 * 
 * To nicely visualize them you should use something like this.
 *   workFrame = new JFrame("Poperties");
 *   workPanel = new JPanel(new GridLayout(0,1));
 *   workPanel.setBorder(new TitledBorder(" Program Configuration "));
 * And add the various componets one after the other.
 */
public class PropertyGroup
  {
  private static final String classname="PropertyGroup.";

  private final String prefix;
  private final HashMap<String,PropertyInterface> propertyMap;
  
  public PropertyGroup( String prefix )
    {
    this.prefix = prefix;
    propertyMap = new HashMap<String,PropertyInterface>();
    }
  
  /**
   * returns the prefix this panel is esponsible for
   */
  public String getPrefix ()
    {
    return prefix;
    }
    
  /**
   * Adds one property to the managed list.
   */
  public PropertyInterface addProperty( PropertyInterface aProperty )
    {
    if ( aProperty == null ) 
      return null;
    
    String key = aProperty.getKey();

    if ( propertyMap.get(key) != null )
      throw new IllegalArgumentException (classname+"addProperty() key="+key+" already present in the map");
      
    propertyMap.put(aProperty.getKey(),aProperty);
    
    return aProperty;
    }
    
  /**
   * Retrieve the JComponent that shows this property, so you can put it into a panel.
   */
  public final JComponent getComponentToDisplay( String key )
    {
    if ( key == null ) return null;
    
    PropertyInterface aProperty = propertyMap.get(key);
    if ( aProperty == null ) return null;

    return aProperty.getComponentToDisplay(); 
    }
    
  public final PropertyInterface getProperty ( String key )
    {
    if ( key == null ) return null;
 
    return propertyMap.get(key);
    }

  /**
   * Retuns the value, null if the given key is not found.
   * @param key the string key for the property.
   * @return 
   */
  public final Object getValue ( String key )
    {
    PropertyInterface aProperty = getProperty(key);

    if ( aProperty == null ) return null;

    return aProperty.getValue(); 
    }
  
  public final int getValueInt ( String key, int defaultval )
    {
    PropertyInterface aProperty = getProperty(key);
  
    if ( aProperty == null ) return defaultval;
  
    try
      {
      return Integer.parseInt(aProperty.getStringValue());
      }
    catch ( Exception exc )
      {
      return defaultval;
      }
    }
    
  public final String getValueString ( String key, String defaultval )
    {
    PropertyInterface aProperty = getProperty(key);
  
    if ( aProperty == null ) return defaultval;
  
    return aProperty.getStringValue();
    }

  /**
   * Allow some external method to set the value of a given property.
   * @param key the id of the property to be changed.
   * @param value the new value to be set.
   */
  public final void setStringValue ( String key, String value )
    {
    if ( key == null ) return;
    
    PropertyInterface aProperty = propertyMap.get(key);
    
    if ( aProperty == null ) return;

    aProperty.setStringValue(value); 
    }
    
  /**
   * Load the properties from the set of properties given.
   * This is where the prefix comes in hand.
   * Package access since it should be used only by ProperttyStore
   */
  final void load ( Properties javaPro )
    {
    for (PropertyInterface aProperty : propertyMap.values() )
      {
      String wantKey = prefix+aProperty.getKey();
      String value = javaPro.getProperty(wantKey);

      if ( value == null ) continue;
      
      aProperty.setStringValue(value);
      }
    }
    
  /**
   * Saves the poperties into the returned Poperties.
   * NOTE that the key is prepended with the prefix.
   * Package access since it should be used only by ProperttyStore
   */
  final Properties save ()
    {
    Properties javaPro = new Properties();

    for (PropertyInterface aProperty : propertyMap.values() )
      {
      String key = prefix+aProperty.getKey();
      String value = aProperty.getStringValue();
      javaPro.setProperty(key,value);
      }

    return javaPro;
    }
  }
