/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class FontChooser
  {
  private static final String classname = "FontChooser.";

  private final JFrame workFrame;
  private final JPanel workPanel;
  private final FontChooserListener fontChooserListener;
  private final FontSelectedListener fontSelectedListener;
  private final Font defaultFont;
  private final String defaultFontFamily;   // Should be != null

  private DefaultListModel fontListModel;
  private JList fontList;

  private JLabel fontFamily;  
  private JCheckBox fontBold;
  private JCheckBox fontItalic;

  private JSpinner     fontSize;
  private SpinnerModel fontSizeModel;

  private JTextArea previewArea;
  
  private JButton okButton;
  private JButton applyButton;

  /**
   * You need to give a callback to signal when the font selection is done and the current value.
   * If defaultFont is null an exception will be thrown.
   */
  public FontChooser(FontSelectedListener fontSelectedListener, Font defaultFont )
    {
    if ( defaultFont == null ) throw new IllegalArgumentException(classname+"FontChooser defaultFont==null");
    
    this.fontSelectedListener = fontSelectedListener;
    this.defaultFont = defaultFont;
    defaultFontFamily = defaultFont.getFamily();
    
    fontChooserListener = new FontChooserListener();
        
    workFrame = new JFrame("Selezione Font");
    workFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    workPanel = (JPanel)workFrame.getContentPane();

    workPanel.add(newFontPanel(),BorderLayout.CENTER);
    workPanel.add(newButtons(),BorderLayout.SOUTH);
    
    workFrame.pack();
    workFrame.setLocationRelativeTo(null);    
    workFrame.setVisible(true);
    
    // Now it is time to start loading fonts. A timeintensive activity...
    Thread fontLoader = new Thread(new GetFontListThread());
    fontLoader.setPriority(Thread.MIN_PRIORITY);
    fontLoader.start();
    }

  /**
   * Creates the main panel where fonts are displayed.
   */
  private JPanel newFontPanel ()
    {
    // Just two parts equal size, one with the list of fonts, the other with the information
    JPanel aPanel = new JPanel(new GridLayout(0,2));
    aPanel.setBorder(new TitledBorder("  Seleziona un Font  "));
   
    fontListModel = new DefaultListModel();
    fontList = new JList(fontListModel);
    fontList.setCellRenderer(new FontListCellRenderer());
    fontList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    fontList.addListSelectionListener(new ListSelectionHandler());
    aPanel.add(new JScrollPane(fontList));

    aPanel.add(newRightPanel());
    return aPanel;
    }

  /**
   * I should do all the labels on top and on the center of a Border Layout put the text to display.
   */
  private JPanel newRightPanel ()
    {
    JPanel aPanel = new JPanel(new BorderLayout());
    aPanel.setBorder(new EmptyBorder(0,4,0,4));

    aPanel.add(newDetailPanel(),BorderLayout.NORTH);

    // The panel is needed only to handle the empty border...
    JPanel previewPanel = new JPanel(new BorderLayout());
    previewPanel.setBorder(new EmptyBorder(4,4,4,4));
    previewArea = new JTextArea("Un esempio di questo Font");
    previewArea.setBorder(new CompoundBorder(new LineBorder(Color.BLACK),new EmptyBorder(0,2,0,2)));
    previewArea.setLineWrap(true);
    
    previewPanel.add(previewArea,BorderLayout.CENTER);
    aPanel.add(previewPanel, BorderLayout.CENTER);

    return aPanel;
    }

  /**
   * Details are the font name, size, the bold/italics.
   */
  private JPanel newDetailPanel ()
    {
    JPanel aPanel = new JPanel(new GridLayout(0,1));

    JPanel fontPanel = new JPanel(new BorderLayout());
    fontPanel.add(new JLabel ("Nome Font:  "),BorderLayout.WEST);
    fontPanel.add(fontFamily = new JLabel(),BorderLayout.CENTER);
    aPanel.add(fontPanel);

    int defSize=defaultFont.getSize();
    fontSizeModel = new SpinnerNumberModel(defSize,5,40,1);
    fontSize = new JSpinner(fontSizeModel);
    fontSize.addChangeListener(fontChooserListener);
    aPanel.add(wrappedLabelInput("Grandezza ",fontSize));
    
    fontBold = new JCheckBox();
    fontBold.setSelected(defaultFont.isBold());
    fontBold.addActionListener(fontChooserListener);
    aPanel.add(wrappedLabelInput("Grassetto ",fontBold));
    
    fontItalic = new JCheckBox();
    fontItalic.setSelected(defaultFont.isItalic());
    fontItalic.addActionListener(fontChooserListener);
    aPanel.add(wrappedLabelInput("Italico ",fontItalic));

    return aPanel;
    }
    
    
  private JPanel wrappedLabelInput ( String label, JComponent input )
    {
    JPanel aPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING,1,1));
    aPanel.add(new Label(label));
    aPanel.add(input);
    return aPanel;
    }

  /**
   * Buttons and selectors for calendar here
   */
  private JPanel newButtons ()
    {
    JPanel aPanel = new JPanel();
    aPanel.setBorder(new EmptyBorder(4,0,0,2));

    aPanel.add(okButton=new JButton("OK"));
    okButton.addActionListener(fontChooserListener);

    aPanel.add(applyButton=new JButton("Apply"));
    applyButton.addActionListener(fontChooserListener);
    
    return aPanel;
    }


  /**
   * Update the preview label and notify of the font name.
   */
  private void updatePreviewLabel()
    {
    Font selectedFont = (Font)fontList.getSelectedValue();
    if ( selectedFont == null ) return;

    Integer newSize = (Integer)fontSizeModel.getValue();
    int bits = fontBold.isSelected() ? Font.BOLD:0;
    bits += fontItalic.isSelected() ? Font.ITALIC:0;
    
    Font newFont = selectedFont.deriveFont(bits,newSize.floatValue());
    
    fontFamily.setText(newFont.getFamily());
    previewArea.setFont(newFont);   
    }



private final class FontChooserListener implements ActionListener, ChangeListener
  {
  public void stateChanged(ChangeEvent event )
    {
    // We have only the spinner bound to this.
    updatePreviewLabel();
    }
  
  public void actionPerformed(ActionEvent event)
    {
    Object source=event.getSource();

    // This MUST be first.
    updatePreviewLabel();
    
    if ( source==applyButton)
      {
      fontSelectedListener.fontSelected(previewArea.getFont());
      }

    if ( source==okButton)
      {
      // If there is a callback available call it.
      if ( fontSelectedListener != null ) fontSelectedListener.fontSelected(previewArea.getFont());

      workFrame.dispose();
      }
    }
  }


private final class ListSelectionHandler implements ListSelectionListener 
  {
  public void valueChanged(ListSelectionEvent event) 
    {
    if (event.getValueIsAdjusting() ) return;

    updatePreviewLabel();
    }
  }
    
  
  
private final class FontListCellRenderer extends JLabel implements ListCellRenderer 
  {
  private static final long serialVersionUID = 1L;

  public FontListCellRenderer() 
    {
    setOpaque(true);
    }

  public Component getListCellRendererComponent (
    JList list,
    Object value,
    int index,
    boolean isSelected,
    boolean cellHasFocus)
    {
    if ( value instanceof Font ) 
      {
      Font aFont = (Font)value;        
      setFont(aFont);
      setText(aFont.getFamily());
      }
    else
      {
      setText(value.toString());
      }
    
    setBackground(isSelected ? Color.red : Color.white);
    setForeground(isSelected ? Color.white : Color.black);
    return this;
    }
  }  


/**
 * I need a thread since othervise the user will look and wonder what is happening.
 */
private final class GetFontListThread implements Runnable
  {
  public void run()
    {
    GraphicsEnvironment env =  GraphicsEnvironment.getLocalGraphicsEnvironment();
    String fontListData[] = env.getAvailableFontFamilyNames();
    Thread currentThread=Thread.currentThread();
    
    for ( int index=0; index<fontListData.length; index++ )
      {
      String familyName = fontListData[index];
      Font aFont = new Font(familyName,Font.PLAIN,13);
      try
        {
        // it is OK to wait for swing to have done its work.
        SwingUtilities.invokeAndWait(new AddListElement(aFont));
        }
      catch ( Exception exc )
        {
        System.err.println (classname+"GetFontListThread exc="+exc);
        }
      }
    }

  }
  
  
/**
 * Used to add fonts to the list in a thread safe way.
 */
private final class AddListElement implements Runnable
  {
  Font aFont;
  
  AddListElement ( Font aFont )
    {
    this.aFont = aFont;  
    }
  public void run ()
    {
    // Yes, addElement is NOT thread safe and this is the only way to make it so.
    fontListModel.addElement(aFont);
    if ( defaultFontFamily.equals(aFont.getFamily())) fontList.setSelectedValue(aFont,true);
    }
  }



  } // End of main class

