/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

public final class IntegerHexProperty extends IntegerProperty
  {
  public IntegerHexProperty (String key, String label, Integer defaultValue, int columns )
    {
    super(key,label,defaultValue,columns);
    }

  /**
   * Implements the interface.
   */
  public void setStringValue ( String stringValue )
    {
    value = convertHexStringToInteger(stringValue);
    refreshGui(value);
    }
  
  private String refreshGui(Integer val)
    {
    String risul = "0x"+Integer.toHexString(val);
    
    inputField.setText(risul);
    
    return risul;
    }
  
  /** 
   * Implements the interface.
   */
  public String getStringValue ()
    {
    value = convertHexStringToInteger(inputField.getText());
    return refreshGui(value);
    }

  /**
   * Implements the interface.
   */
  public Integer getValue ()
    {
    value = convertHexStringToInteger(inputField.getText());
    refreshGui(value);
    return value;
    }

  private final Integer convertHexStringToInteger (String stringValue)
    {
    try 
      {
      // decode can decode also hexadecimal numbers.
      if ( ! stringValue.startsWith("0x"))
        stringValue = "0x"+stringValue;
      
      return Integer.decode(stringValue);
      }
    catch ( Exception exc )
      { 
      return defaultValue;
      }
    }
    
    
  }
