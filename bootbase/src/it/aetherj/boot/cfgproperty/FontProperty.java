/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public final class FontProperty extends PropertyInterface implements ActionListener, FontSelectedListener
  {
  private static final String classname = "FontProperty.";
  private final String key;
  private final String label;
  private final JPanel workPanel;
  private final JTextField sampleField;
  private final JButton showChooserBtn;
  private final Font defaultValue;
  private Font fontValue;

  public FontProperty(String key, String label, Font defaultValue)
    {
    if (defaultValue == null)
      throw new IllegalArgumentException(classname + "FontProperty() defaultValue==null");

    this.key = key;
    this.label = label;
    this.defaultValue = defaultValue;

    showChooserBtn = new JButton(getIcon("font.gif"));
    showChooserBtn.setMargin(new Insets(1, 1, 1, 1));
    showChooserBtn.setToolTipText("Premi qui per cambiare il font");
    showChooserBtn.addActionListener(this);

    sampleField = new JTextField(" Esempio ");

    FlowLayout layout = new FlowLayout(FlowLayout.TRAILING, 1, 1);
    workPanel = new JPanel(layout);
    workPanel.add(new JLabel(label));
    workPanel.add(sampleField);
    workPanel.add(showChooserBtn);

    fontValue = defaultValue;
    sampleField.setFont(fontValue);
    }

  /**
   * This is what will go into the main panel.
   */
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }

  public String getKey()
    {
    return key;
    }

  /**
   * Implements the interface.
   */
  public void setStringValue(String value)
    {
    try
      {
      String [] split = value.split(":");
      String fontFamily = split[0];
      int fontStyle = Integer.parseInt(split[1]);
      int fontSize = Integer.parseInt(split[2]);
      fontValue = new Font(fontFamily,fontStyle,fontSize);
      }
    catch (Exception exc)
      {
      fontValue = defaultValue;
      }

    sampleField.setFont(fontValue);
    }

  /**
   * Implements the interface.
   */
  public String getStringValue()
    {
    return fontValue.getFamily()+":"+fontValue.getStyle()+":"+fontValue.getSize();
    }

  /**
   * Implements the interface.
   */
  public Object getValue()
    {
    return fontValue;
    }

  public void actionPerformed(ActionEvent event)
    {
    FontChooser chooser = new FontChooser(this, fontValue);
    }

  public void fontSelected ( Font selectedFont )
    {
    if ( selectedFont == null ) return;
    fontValue = selectedFont;
    sampleField.setFont(fontValue);
    }


  }
