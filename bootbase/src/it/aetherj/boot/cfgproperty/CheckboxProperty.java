/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

import java.awt.FlowLayout;

import javax.swing.*;

public final class CheckboxProperty extends PropertyInterface
  {
  private final String key;
  private final JPanel workPanel;
  private final JCheckBox inputField;
  private Boolean value;
  
  public CheckboxProperty(String key, String label, boolean defaultValue )
    {
    this.key = key;
    this.value = Boolean.valueOf(defaultValue);

    workPanel = new JPanel(new FlowLayout(FlowLayout.LEFT,1,1));
    
    workPanel.add(new JLabel(label));

    inputField = new JCheckBox();   
    inputField.setSelected(defaultValue);
 
    workPanel.add(inputField);
    }
    
  /**
   * This is what will go into the main panel.
   */
  public JComponent getComponentToDisplay() 
    {
    return workPanel;
    }

  public String getKey ()
    {
    return key;
    }

  /**
   * Implements the interface.
   */
  public void setStringValue ( String stringValue )
    {
    value = Boolean.valueOf(stringValue);
    inputField.setSelected( value.booleanValue() );
    }
  
  /**
   * Implements the interface.
   */
  public String getStringValue ()
    {
    value = Boolean.valueOf(inputField.isSelected());
    return value.toString();
    }

  /**
   * Implements the interface.
   */
  public Object getValue ()
    {
    return value;
    }
   
  }
