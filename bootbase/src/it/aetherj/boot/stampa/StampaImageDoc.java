package it.aetherj.boot.stampa;


import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.ImageIO;
import javax.print.*;
import javax.print.attribute.DocAttributeSet;

/**
 * See the documentation for Doc for further information.
 * Basically, what I want to do is to have a class that can print an image directly.
 * To use it you do:
 * 
 *  DocPrintJob printJob = defaultService.createPrintJob();
 *  StampaImageDoc sd = new StampaImageDoc(image);
 *  printJob.print(sd,userAttributes);
 *  
 *  Of course you got to find first a service that supports what it is requested here.
 *  NOTE: This will NOT print an image into multiple pages !
 *  It is mainly an example on how to use pipes+ImageIO to stream data out.
 */
public final class StampaImageDoc implements Doc, Runnable
  {
  private static final String classname="StampaImageDoc.";
  
  private final BufferedImage printableImage;
  private final PipedOutputStream writeHere;
  private final PipedInputStream  readHere;
  
  /**
   * Just give a BufferedImage to print and this will print it.
   * @throws java.io.IOException
   * @param image
   */
  public StampaImageDoc ( BufferedImage image ) throws IOException
    {
    printableImage = image;  

    writeHere = new PipedOutputStream();
    readHere = new PipedInputStream(writeHere);
    
    Thread aThread = new Thread(this);
    aThread.start();      
    }
    
  /**
   * Determines the doc flavor in which this doc object will supply its piece of print data. 
   * It can only be an INPUT_STREAM.JPEG and therefore it is hardcoded.
   */
  public DocFlavor getDocFlavor()
    {
    return DocFlavor.INPUT_STREAM.JPEG;
    }
    
  /**
   * The InputStream where to read data from.
   */
  public Object getPrintData() throws IOException
    {
    return readHere;
    }

    
  /**
   * Can return null.
   */
  public DocAttributeSet getAttributes()
    {
    return null;
    }
    
  /**
   * Should not be used.
   */
  public Reader getReaderForText() throws IOException
    {
    System.err.println (classname+"getReaderForText() CALLED");
    return null;
    }
    
  /**
   * Used as getPrintData.
   */
  public InputStream getStreamForBytes() throws IOException
    {
    return (InputStream)getPrintData();
    }

  /**
   * It MUST be a Thread that does the decoding othervise you lock up the GUI.
   * Having a thread does make the memory consumption really small.
   */
  public void run ()
    {
    try
      {
      if ( ! ImageIO.write(printableImage,"jpeg",writeHere) )
        System.out.println ("Cannot find writer for jpeg");
        
      // You MUST add this one, ImageIO.write do NOT really close writeHere.
      writeHere.close();
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      }
    }
  }

