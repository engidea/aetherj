package it.aetherj.boot.stampa;


import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.print.*;

import javax.swing.JComponent;


/**
 * A simple Pageable class that can split a large drawing canvas over multiple pages.
 * The pages in a canvas are laid out on pages going left to right and then top to bottom.
 * One of the nice things is that if the printer has adequate resolution the result is a nice printout.
 */
public class StampaSwingComponent implements Pageable 
  {
  private static final String classname="StampaSwingComponent.";
  
  private int mNumPagesX;
  private int mNumPagesY;
  private int mNumPages;
  
  private double scaleX=1, scaleY=1;
  private final Dimension  scaledSize;         // The component size after the scaling.

  private final JComponent component;
  private final Dimension  componentSize;
  private final StampaPageFormat mFormat;

  /**
   * NOTE: The component getSize() returns a value tha is reasonably in the same units as the
   * PageFormat, i.e. every INCH there are 72 dots. So the two are comparable !
   * 
   * @param component The component that we wish to print.
   * @param format The description of the pages on to which the canvas will be drawn.
   */
  public StampaSwingComponent(JComponent component, StampaPageFormat mFormat) 
    {
    this.component = component;
    this.mFormat = mFormat;
    
    System.out.println (classname+"mFormat="+mFormat);

    componentSize = component.getSize();
    scaledSize = componentSize;

    // If what we want to print is bigger than where we can print it..
    if ( componentSize.getWidth() > mFormat.getImageableWidth() ) scaleToFitX();

    calculateComponentPages();
    }


  /**
   * Calculate dthe component page number.
   */
  private void calculateComponentPages() 
    {
    mNumPagesX = (int) ((scaledSize.getWidth() + mFormat.getImageableWidth() - 1)/ mFormat.getImageableWidth());
    mNumPagesY = (int) ((scaledSize.getHeight() + mFormat.getImageableHeight() - 1)/ mFormat.getImageableHeight());
    mNumPages = mNumPagesX * mNumPagesY;
    }

  /**
   * Returns the number of pages over which the canvas will be drawn.
   */
  public int getNumberOfPages() 
    {
    return mNumPages;
    }
    
  /** 
   * Returns the PageFormat of the page specified by pageIndex.
   *
   * @param pageIndex the zero based index of the page whose PageFormat is being requested
   * @return the PageFormat describing the size and orientation.
   * @exception IndexOutOfBoundsException the Pageable  does not contain the requested page.
   */
  public PageFormat getPageFormat(int pageIndex) throws IndexOutOfBoundsException 
    {
    if (pageIndex >= mNumPages) throw new IndexOutOfBoundsException();

    return mFormat;
    }

  
  /**
   * calculate the scale that is needed to fit this X into the available space.
   */
  public void scaleToFitX() 
    {
    scaleX = mFormat.getImageableWidth()/componentSize.getWidth();
    scaleY = scaleX;

    scaledSize.setSize(componentSize.getWidth() * scaleX, componentSize.getHeight() * scaleY );
    calculateComponentPages();
    }

  /**
   * Calculate the scale that is needed to fit this Y into the available space.
   * Is this really used ?
   */
  public void scaleToFitY() 
    {
    scaleY = mFormat.getImageableHeight()/componentSize.getHeight();
    scaleX = scaleY;

    scaledSize.setSize(componentSize.getWidth() * scaleX, componentSize.getHeight() * scaleY );
    calculateComponentPages();
    }


  /**
   * Returns the <code>Printable</code> instance responsible for rendering the page specified by <code>pageIndex</code>.
   * In a Vista, all of the pages are drawn with the same Printable. This method however creates
   * a Printable which calls the canvas's Printable. This new Printable
   * is responsible for translating the coordinate system so that the desired part of the canvas hits the page.
   *
   * The Vista's pages cover the canvas by going left to right and then top to bottom. In order to change this
   * behavior, override this method.
   *
   * @param pageIndex the zero based index of the page whose Printable is being requested
   * @return the Printable that renders the page.
   * @exception IndexOutOfBoundsException the Pageable does not contain the requested page.
   */
  public Printable getPrintable(int pageIndex) throws IndexOutOfBoundsException 
    {
    if (pageIndex >= mNumPages) throw new IndexOutOfBoundsException();

    double originX = (pageIndex % mNumPagesX) * mFormat.getImageableWidth();
    double originY = (pageIndex / mNumPagesX) * mFormat.getImageableHeight();
    Point2D.Double origin = new Point2D.Double(originX, originY);
    return new StampaSwingComponent.TranslatedPrintable( origin );
    }
    
/**
 * This inner class's sole responsibility is to translate
 * the coordinate system before invoking a canvas's
 * painter. The coordinate system is translated in order
 * to get the desired portion of a canvas to line up with
 * the top of a page.
 */
public final class TranslatedPrintable implements Printable 
  {
  /**
   * The upper-left corner of the part of the canvas that will be displayed on this page. This corner
   * is lined up with the upper-left of the imageable area of the page.
   */
  private Point2D mOrigin;

  /**
   * Create a new Printable that will translate the drawing done by painter on to the
   * imageable area of a page.
   *
   * @param painter The object responsible for drawing the canvas
   * @param origin The point in the canvas that will be mapped to the upper-left corner of the page's imageable area.
   */
  public TranslatedPrintable( Point2D origin ) 
    {
    mOrigin = origin;
    }


  /**
   * Prints the page at the specified index into the specified  {@link Graphics} context in the specified
   * format. A PrinterJob calls the  Printableinterface to request that a page be
   * rendered into the context specified by  graphics. The format of the page to be drawn is
   * specified by pageFormat. The zero based index of the requested page is specified by pageIndex. 
   * If the requested page does not exist then this method returns
   * NO_SUCH_PAGE; otherwise PAGE_EXISTS is returned.
   * The Graphics class or subclass implements the {@link PrinterGraphics} interface to provide additional
   * information. If the Printable object* aborts the print job then it throws a {@link PrinterException}.
   * @param graphics the context into which the page is drawn 
   * @param pageFormat the size and orientation of the page being drawn
   * @param pageIndex the zero based index of the page to be drawn
   * @return PAGE_EXISTS if the page is rendered successfully or NO_SUCH_PAGE if pageIndex specifies a non-existent page.
   * @exception java.awt.print.PrinterException thrown when the print job is terminated.
   */
  public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException 
    {
    Graphics2D g2 = (Graphics2D) graphics;

    // This places itself in the proper page.
    g2.translate(-mOrigin.getX(), -mOrigin.getY());

    // This moves where it is acceptable to write.
    g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

    // Sets the scaling to fit it into the place
    g2.scale(scaleX, scaleY);
    
    // Do the actual printing.
    component.printAll(g2);
    
    return PAGE_EXISTS;
    }

  }

  } // END of main class