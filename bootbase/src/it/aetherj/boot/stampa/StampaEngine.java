package it.aetherj.boot.stampa;


import javax.print.*;
import javax.print.attribute.*;
import javax.print.event.PrintJobListener;
import javax.swing.JComponent;

import it.aetherj.boot.Log;

/**
 * Provides basic services to print Swing Component.
 */ 
public class StampaEngine
  {
  private static final String classname="StampaEngine.";
  
  private final Log log;
  private final DocFlavor wantedDocFlavor;
  private final PrintRequestAttributeSet userAttributes;
  
  private volatile PrintService[] availableServices;
  private volatile PrintService defaultService;


  /**
   * Constructor.
   */
  public StampaEngine( Log log )
    {
    if ( log == null ) throw new IllegalArgumentException (classname+"StampaEngine() log==null");
    
    this.log = log;
    
    wantedDocFlavor = DocFlavor.SERVICE_FORMATTED.PAGEABLE;
    userAttributes = new HashPrintRequestAttributeSet();

    // Lazy initialization of the printer system.
    new Thread(new InitPrintService()).start();
    }

  /**
   * When you want to select the service you can use this one.
   */
  public void selectService()
    {
    // We do a rescan of the services, the user may have added some more.
    availableServices = getAvailablePrintServices();

    // Get what should be the default service.
    
    PrintService aService = (defaultService != null) ? defaultService : PrintServiceLookup.lookupDefaultPrintService();

    /* This does not work reliably.    
    ServiceUIFactory factory = defaultService.getServiceUIFactory();
    if (factory != null) 
      {
      System.out.println (classname+"selectService() factory="+factory);
      JComponent swingui = (JComponent)factory.getUI(ServiceUIFactory.MAIN_UIROLE, ServiceUIFactory.JCOMPONENT_UI);
      if (swingui != null) 
        {
        JFrame jf = new JFrame("PIPPO");
        jf.getContentPane().add( swingui );
        jf.pack();
        jf.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
        jf.setVisible( true );
        }    
      }
    */
    
    defaultService = ServiceUI.printDialog(null, 100, 80, availableServices, aService, wantedDocFlavor, userAttributes);
    }

  /**
   * Call this method when you want to print something on the selected service.
   * @param component the JComponent to print.
   * @return true if there was an error doing the work, false if all went well.
   */
  public boolean printComponent(JComponent component, PrintJobListener printJobListener )
    {
    if (component == null) return log.messageShow(classname+"printComponent: ERROR component==null");

    // In any case ask for where to print.
    selectService();
    
    // If still there is no service that can be used then fail.
    if ( defaultService == null ) return log.messageShow(classname+"printComponent: non e' disponibile una stampante");
    
    DocPrintJob printJob = defaultService.createPrintJob();
    if ( printJobListener != null ) printJob.addPrintJobListener(printJobListener);

    StampaPageFormat pageFormat = new StampaPageFormat(defaultService,printJob,userAttributes);
    StampaSwingComponent stampa = new StampaSwingComponent(component,pageFormat);
    SimpleDoc doc = new SimpleDoc(stampa, wantedDocFlavor ,null);

    // Start the actual printing.
    new Thread(new PerformPrinting(printJob,doc,userAttributes)).start();

    // no erro happened during the printer setup.    
    return false;
    }




  /**
   * Returns all the print services that can print for the current docFlavor
   */
  private PrintService[] getAvailablePrintServices ()
    {
    PrintService []services = PrintServiceLookup.lookupPrintServices(wantedDocFlavor, null);
  
    log.userPrintln("Lista stampanti per docFlavor="+wantedDocFlavor);
    for ( int index=0; index<services.length; index++ )
      {
      PrintService aService = services[index];
      log.userPrintln("  nome="+aService.getName());
      }

    return services;
    }



/**
 * We need lazy initialization of the service since it is quite time consuming.
 * Not only that, it blocks the GUI and this is bad.
 * Note that we do not initialize the default service, we let the user choose.
 */
private final class InitPrintService implements Runnable
  {
  public void run()
    {
    availableServices = getAvailablePrintServices();

    log.userPrintln("Servizio di stampa pronto");
    }
  }



/**
 * We also need printing on a separate thread since othervise it will lock the gui again
 */
private final class PerformPrinting implements Runnable
  {
  private final DocPrintJob printJob;
  private final Doc doc;
  private final PrintRequestAttributeSet attributes;
  
  PerformPrinting ( DocPrintJob printJob, Doc doc, PrintRequestAttributeSet attributes )
    {
    this.printJob = printJob;
    this.doc = doc;
    this.attributes = attributes;
    }

  public void run ()
    {
    try
      {
      // It is not clear if this stops or not, since it migth then it is better in a thread.
      printJob.print(doc,attributes);
      }
    catch (Exception exc)
      {
      log.exceptionShow(classname,exc);
      }
    }
  }
    




  } // END MAIN CLASS
