package it.aetherj.boot.stampa;

import java.awt.*;
import java.awt.image.BufferedImage;

import javax.swing.*;

public class StampaPreview
  {
  public StampaPreview()
    {
    }

  /**
   * Show a given image into a JFrame
   * 
   * @param img
   */
  private void showImage(BufferedImage img)
    {
    JFrame jf = new JFrame();
    PaintableJPanel jp = new PaintableJPanel();
    jp.setImage(img);
    JScrollPane jsp = new JScrollPane(jp);
    jf.getContentPane().add(jsp);
    Insets i = jf.getInsets();
    jf.pack();
    jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    jf.setVisible(true);
    }

  private final class PaintableJPanel extends JPanel
    {
    private static final long serialVersionUID = 1L;

    Image img;

    protected PaintableJPanel()
      {
      super();
      }

    public Dimension getPreferredSize()
      {
      Dimension risul = new Dimension();
      risul.width = img.getWidth(null);
      risul.height = img.getHeight(null);

      return risul;
      }

    public void setImage(Image img)
      {
      this.img = img;
      }

    public void paint(Graphics g)
      {
      g.drawImage(img, 0, 0, this);
      }
    }

  }