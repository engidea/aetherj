package it.aetherj.boot.stampa;


import java.awt.print.*;

import javax.print.*;
import javax.print.attribute.*;
import javax.print.attribute.standard.*;

/**
 * Manages a PageFormat and other amenities that comes from the more advanced javax.print
 */
public class StampaPageFormat extends PageFormat
  {
  private static final String classname="StampaPageFormat.";
  private static final int DOT_PER_INCH = 72;

  private final HashAttributeSet allAttributes;
  
  /**
   * Creates a new class taking the values from the provided items.
   * They must be not null, they may be empty but must be not null.
   */
  public StampaPageFormat (PrintService printService, DocPrintJob printJob, PrintRequestAttributeSet userAttributes)
    {
    allAttributes = mergeAllAttributes(printService, printJob, userAttributes );
    printAllAttributes(classname+"Attributes");
    updatePrintableArea();
    updateOrientation();
    }
  
  /**
   * Someone way wish to know if this is a color printer request.
   */
  public Boolean isColorPrintRequest (  )
    {
    Chromaticity color = (Chromaticity)allAttributes.get(Chromaticity.class);

    if ( color == null ) 
      return null;
    
    return Boolean.valueOf(color.equals(Chromaticity.COLOR));
    }


  /**
   * Someone way wish to know if this is a color printer.
   * @return true if it can print colors, false othervise, null if it cannot tell.
   */
  public Boolean isColorPrinter ( )
    {
    ColorSupported color = (ColorSupported)allAttributes.get(ColorSupported.class);
    if ( color == null ) return null;
    
    return Boolean.valueOf(color.equals(ColorSupported.SUPPORTED));
    }

  /**
   * Try to extract the printer dpi.
   * @return an Integer with the dpi or null if no property can be found.
   */
  public Integer getPrinterDpi ( )
    {
    PrinterResolution pr = (PrinterResolution) allAttributes.get(PrinterResolution.class);
    if ( pr == null ) return null;

    return Integer.valueOf(pr.getFeedResolution(PrinterResolution.DPI));
    }
  
  /**
   * Merges the attributes that may come from three places.
   */
  private HashAttributeSet mergeAllAttributes ( PrintService printService, DocPrintJob printJob, PrintRequestAttributeSet userAttributes )
    {
    HashAttributeSet result = new HashAttributeSet();
    
    // This adds what is published by this print service.
    result.addAll(printService.getAttributes());
    
    // Then we add what his print job may have, usually nothing
    result.addAll(printJob.getAttributes());
    
    // Then we add the default values.
    Class<?> [] suppCategory = printService.getSupportedAttributeCategories();
    for ( int index=0; index<suppCategory.length; index++ )
      {
      Class<? extends Attribute> category = (Class<? extends Attribute>)suppCategory[index];
      Object value = printService.getDefaultAttributeValue(category);
      if ( value == null ) 
        System.err.println(classname+"getAllAttributes: category="+category+" has null value");
      else
        result.add((Attribute)value);
      }
      
    // Finally we add the set that the user has selected..
    result.addAll(userAttributes);
    
    return result;
    }

  /**
   * You can call this one to print the attributes of this class.
   */
  public void printAllAttributes ( String header )
    {
    Attribute [] attributes = allAttributes.toArray();
    
    System.out.println(header);
    for ( int index=0; index<attributes.length; index++ )
      {
      Attribute anAttr = attributes[index];
      System.out.println("  category="+anAttr.getCategory()+" name="+anAttr.getName());
      }
    }
  
  /**
   * Update the printable area, not that if no useful information is found the current will be untouched.
   */
  private void updatePrintableArea (  )
    {
    MediaPrintableArea mpa = (MediaPrintableArea) allAttributes.get(MediaPrintableArea.class);
    if ( mpa == null ) return;

    double originX = mpa.getX(MediaPrintableArea.INCH) * DOT_PER_INCH;
    double originY = mpa.getY(MediaPrintableArea.INCH) * DOT_PER_INCH;
    double width = mpa.getWidth(MediaPrintableArea.INCH) * DOT_PER_INCH;
    double height = mpa.getHeight(MediaPrintableArea.INCH) * DOT_PER_INCH;

    Paper paper = getPaper();
    paper.setImageableArea(originX,originY,width,height);
    setPaper(paper);
    }

  
  /**
   * Updates this page format with the orientation taken from the given attributes.
   */
  private void updateOrientation ( )
    {
    OrientationRequested orientation = (OrientationRequested) allAttributes.get(OrientationRequested.class);
    if ( orientation == null ) return;
    
    if ( orientation.equals(OrientationRequested.LANDSCAPE) ) setOrientation(PageFormat.LANDSCAPE);
    if ( orientation.equals(OrientationRequested.PORTRAIT) ) setOrientation(PageFormat.PORTRAIT);
    if ( orientation.equals(OrientationRequested.REVERSE_LANDSCAPE) ) setOrientation(PageFormat.REVERSE_LANDSCAPE);
    }
  
  
  public String toString ()
    {
    StringBuffer risul = new StringBuffer(512);
    risul.append("StampaPageFormat (inches): \n");
    risul.append("  originX="+getImageableX()/DOT_PER_INCH+"\n");
    risul.append("  originY="+getImageableY()/DOT_PER_INCH+"\n");
    risul.append("  imageable width="+getImageableWidth()/DOT_PER_INCH+"\n");
    risul.append("  imageable height="+getImageableHeight()/DOT_PER_INCH+"\n");
    risul.append("  orientation="+getOrientation()+"\n");
    risul.append("  dpi="+getPrinterDpi()+"\n");
    risul.append("  color printer="+isColorPrinter()+"\n");
    risul.append("  color request="+isColorPrintRequest()+"\n");

    return risul.toString();    
    }
  }