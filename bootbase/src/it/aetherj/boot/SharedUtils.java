/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot;

import java.awt.*;
import java.io.*;
import java.net.Socket;

 
public class SharedUtils 
  {
  private static final String classname="SharedUtils.";

  /**
   * returns a Point that can be used to place the window in center of screen.
   * See also: workFrame.setLocationRelativeTo(null);
   * @param windowSize
   * @return 
   */
  public static Point getCenter ( Dimension windowSize )
    {
    Point videoCenter = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
    videoCenter.translate(-windowSize.width/2,-windowSize.height/2);
    return videoCenter;
    }

  /**
   * Returns a point that is in the middle of the screen.
   * @return 
   */
  public static Point getCenter (  )
    {
    Point videoCenter = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
    return videoCenter;
    }


  public static Dimension getUpperHalfScreen ()
    {
    Rectangle bound = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
    int width = (int)bound.getWidth();
    int height = (int)bound.getHeight();
    return  new Dimension (width,height/2);
    }

  public static Dimension getScreenDimension ()
    {
    Rectangle bound = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
    int width = (int)bound.getWidth();
    int height = (int)bound.getHeight();
    return  new Dimension (width,height);
    }
    
    
  public static Thread invokeRunnable ( Runnable aRunnable )
    {
    Thread aThread = new Thread(aRunnable);
    aThread.start();
    return aThread;
    }
    
    
   public static void close ( Object aStream )
    {
    if ( aStream == null ) return;
    
    try 
      {
      if ( aStream instanceof OutputStream ) ((OutputStream)aStream).close();
      if ( aStream instanceof InputStream )  ((InputStream)aStream).close();
      if ( aStream instanceof Socket )       ((Socket)aStream).close();
      }
    catch ( Exception exc )
      {
      System.out.println (classname+"close: Exception="+exc);
      exc.printStackTrace();
      }
    }


  /**
   * Convert a string in Integer.
   * @param input the string base 10 to be converted
   * @return the given Integer if exception, defaultVal can be null
   */
  public static final Integer getInteger ( String input, Integer defaultVal )
    {
    try
      {
      return Integer.decode(input);
      }
    catch ( Exception exc )
      {
      return defaultVal;
      }
    }


  /**
   * Convert a string into an int with a default.
   * @param input the tring base 10 to be converted.
   * @return the int value or the given default if an error occours.
   */
  public static final int getInt ( String input, int defVal )
    {
    try
      {
      return Integer.parseInt(input);
      }
    catch ( Exception exc )
      {
      return defVal;
      }
    }


  /**
   * Converts an array of bytes into the equivalend Hex.
   * If input is null then null is returned.
   * If input is zero len then an empty string is returned.
   */
  private static final String[] hexMap = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };   

  public static final String convByteToHex ( byte [] input )
    {
    StringBuffer risul = new StringBuffer(200);
    
    if ( input == null ) return null;
    
    for ( int index=0; index<input.length; index++ )
      {
      int oneByte = input[index] & 0xFF;

      byte nibble = (byte)(oneByte & 0x0F );
      risul.append(hexMap[nibble]);

      nibble = (byte)(oneByte >>> 4);   // It is an unsigned shift right (zero insertion)
      risul.append(hexMap[nibble]);        
      }
      
    return risul.toString();
    }
    
  /**
   * Converts a string of hexadecimal into a byte array.
   * If null is given null is returned.
   * If empty is given empty is returned.
   * MUST be an even length string !
   */
  public static final byte[] convHextoByte ( String input )
    {
    if ( input == null ) return null;

    int risulLen = input.length() / 2;
    if ( (risulLen * 2) != input.length() ) throw new IllegalArgumentException ("convHexToByte input.length()="+input.length()+" is odd");

    byte [] risul = new byte[risulLen];
    int inputLen=input.length();
    int inputIndex = 0;
    int risulIndex = 0;
    byte aByte = 0;
    
    while (inputIndex < inputLen )
      {
      aByte  = (byte)(convertNibble( input.charAt(inputIndex++)) << 4);
      aByte |= (byte)convertNibble( input.charAt(inputIndex++));
      
      risul[risulIndex++] = aByte;
      }
      
    return risul;
    }
    
  private static int convertNibble ( char nibble )
    {
    switch ( nibble )
      {
      case '0': return 0;
      case '1': return 1;
      case '2': return 2;
      case '3': return 3;
      case '4': return 4;
      case '5': return 5;
      case '6': return 6;
      case '7': return 7;
      case '8': return 8;
      case '9': return 9;
      case 'A': return 10;
      case 'B': return 11;
      case 'C': return 12;
      case 'D': return 13;
      case 'E': return 14;
      case 'F': return 15;
      case 'a': return 10;
      case 'b': return 11;
      case 'c': return 12;
      case 'd': return 13;
      case 'e': return 14;
      case 'f': return 15;
      }

    throw new IllegalArgumentException ("convertNibble bad nibble="+nibble);
    }
    
    
    
    
  }
