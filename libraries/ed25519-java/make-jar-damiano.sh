

#find src -type f -name "*.class" -delete

javac -source 1.8 -target 1.8 -d classes/ -sourcepath src/ src/net/i2p/crypto/eddsa/*.java

jar cf ed25519.jar -C classes .
