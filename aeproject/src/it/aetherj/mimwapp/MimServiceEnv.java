/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.mimwapp;

import java.io.*;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.*;

import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbtbls.*;
import it.aetherj.backend.gui.config.AetherParams;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.McwURLConnection;
import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.gwtwapp.client.beans.WebSession;
import it.aetherj.protocol.MimEntity;
import it.aetherj.shared.*;
import it.aetherj.shared.v0.*;
 

/**
 * Servlets classes are not newed at each request
 * So, each method should have its own environment be passed around to work !
 * You can allocate a new DB connection here and pick up things that are interesting
 * It is a contract guarantee that his class is new'ed when a new request comes in, so you can safely store
 * status in this one since it will be lod when the request completes.
 */
public final class MimServiceEnv implements PrintlnProvider
  {
  private static final String classname = "MimServiceEnv.";
  
  public static final String mim_protov = "v0";   // this is what we are serving

  private final ServletContext servletContext;
  private final HttpServletRequest servletRequest;
  private final Locale clientLocale;
  private final WebStat wstat;
  private final String remoteAddr;
  private final AedbFactory aedbFactory;

  public  final AetherParams aetherParams;
  public  final ServletsUtils servletUtils;  // needed for most use, including label mappings
  public  final CryptoEddsa25519 crypto;

  private String  ident;       // used to write something if db is not closed on finalized.
  private AeDbase dbase;       // I need to be able to NULL it to detach from env.

  /**
   * This MUST be created every time a METHOD is called on a HttpServlet
   * @param serviceServlet
   */
  public MimServiceEnv(HttpServlet serviceServlet, HttpServletRequest servletRequest)
    {
    this.servletContext = serviceServlet.getServletContext();
    this.servletRequest = servletRequest;
    this.remoteAddr     = servletRequest.getRemoteAddr();
    this.clientLocale   = servletRequest.getLocale();
    this.servletUtils   = new ServletsUtils();
    
    this.wstat = (WebStat) servletContext.getAttribute(WebContextListener.KEY_WebStat);

    this.crypto       = new CryptoEddsa25519();
    this.aedbFactory  = wstat.aedbFactory;
    this.aetherParams = wstat.aetherParams;
    
    // NOTE: it is duty of the servlet to release DB connection once DONE
    dbase = (AeDbase)wstat.connpool.getDbase();
    }
  
  public AeDbase dbase()
    {
    return dbase;
    }
  
  public boolean isNonceValid ( MimPublicKey pkt_publicKey, MimNonce pkt_nonce, MimTimestamp pkt_timestamp )
    {
    return wstat.nonceMap.isValid(pkt_publicKey, pkt_nonce, pkt_timestamp);
    }
  
  public MimPayload newMimPayload(MimEntity entity)
    {
    return aetherParams.newMimPayload(entity);
    }
  
  
  public McwAddressTableModel getAddressModel()
    {
    return wstat.mcwAddressModel;
    }
  
  public void writeResponse (HttpServletResponse resp, int s_code, String value ) throws IOException
    {
    setResponseTypeJson(resp);
    resp.setStatus(s_code);

    PrintWriter writer = resp.getWriter();
    
    writer.write(value);
  
    writer.close();   
    } 
  
  
  private void setResponseTypeJson ( HttpServletResponse resp )
    {
    resp.setContentType(McwURLConnection.content_type);
    resp.setCharacterEncoding(McwURLConnection.content_encoding);
    resp.addHeader("Transfer-Encoding", "chunked");
    }
  
  public void writeResponse ( HttpServletResponse resp, MimPayload payload ) throws IOException
    {
    setResponseTypeJson(resp);
    resp.setStatus(AettpStatus.res_OK);

    payload.signPayload(crypto, aetherParams.getPrivateKey());
    
    AeJson json = new AeJson();

    if ( shouldPrint(Aedbg.M_HTTP_tx,Aedbg.L_debug))
      {
      String s = json.writeValueAsString(payload);
      println(s);
      PrintWriter w = resp.getWriter();
      w.print(s);
      w.close();
      }
    else
      {
      OutputStream ostream = resp.getOutputStream();
      json.writeValue(ostream, payload);
      }
    }
    

  /**
   * This is WAY slower but it shows what is happening
   * Use stat.dbg.wishMaskLevel(0x50,0x1); to enable the test for serialize
   * Use https://json-diff.com/ to compare two json
   * @param valueType
   * @return
   * @throws IOException
   */
  private <T> T getRequestLogged ( Class<T> valueType) throws IOException
    {
    BufferedReader reader = servletRequest.getReader();
    StringBuilder sbuilder = new StringBuilder(65000);

    String input;
       
    while ((input = reader.readLine()) != null)
      sbuilder.append(input);
    
    reader.close();
  
    if ( sbuilder.length() <= 0 )
      {
      println("rx is empty");
      return null;
      }
    
    String received=sbuilder.toString();

    println(received);
    
    AeJson mapper = new AeJson();
    T robject = mapper.readValue(received, valueType);
    
    if ( shouldPrint(Aedbg.M_compare_rx,Aedbg.L_debug))
      {
      // This is used when something goes wrong with the Json mapping... happens
      String compare = mapper.writeValueAsString(robject);
    
      if ( ! compare.equals(received))
        {
        println("Do NOT MATCH"); 
        println(compare);
        }
      else
        {
        println("Source == Compare");
        }
      }
    
    return robject;
    }
  
  public boolean shouldPrint ( int mask, int level )
    {
    return wstat.dbg.shouldPrint(mask,level);
    }
  
  /**
   * Map directly into an object, way faster
   * NOTE that JsonParser.Feature.AUTO_CLOSE_SOURCE is normally TRUE
   * @param valueType
   * @return
   * @throws IOException
   */
  public <T> T getRequest ( Class<T> valueType ) throws IOException
    {
    if ( shouldPrint(Aedbg.M_HTTP_rx,Aedbg.L_debug))
      return getRequestLogged(valueType);
    
    AeJson mapper = new AeJson();
    BufferedReader reader = servletRequest.getReader();
    return mapper.readValue(reader, valueType);
    }

  
  
  public void setWebSession(WebSession webSession)
    {
    if ( webSession == null ) return;
    
    HttpSession htses = servletRequest.getSession(true);
    
    if ( htses == null )
      {
      println(classname+"setWebSession: servletRequest.getSession(true) FAILED");
      return;
      }
    }
  
  public String getRemoteAddress ()
    {
    return remoteAddr;      
    }

  /**
   * Return a translated English label into what is requested by the user locale
   * @param from_page
   * @param label_english
   * @return
   */
  public String getLabel ( String from_page, String label_english )
    {
    if ( wstat.labelFactory == null ) return label_english;
    
    Locale locale = getClientLocale();

    String want_lang = locale.toString();
    
    return wstat.labelFactory.getLabel(from_page, label_english, want_lang);
    }
   
   public McwAddress mcwAddressSave ( MimAddress p_address )
     {
     return wstat.mcwAddressModel.addressSave ( p_address );
     }
   
   public AedbOperations getAedb ( MimEntity entity )
     {
     return aedbFactory.getAedb(entity);
     }
  
  public MimServiceEnv setIdent ( String ident )
    {
    this.ident = ident;
    return this;
    }
  
  public ApplicationVersion getAppVersion ()
    {
    return wstat.appVersion;    
    }
  
  public void logEvent ( Integer wana_id, String logall_service, String logall_dboper, String event_desc)
    {
    if ( dbase == null )
      {
      println(classname+"logEvent: ERROR dbase==null");
      return;
      }
    
    if ( ! dbase.isConnected() ) 
      {
      println(classname+"logEvent: ERROR dbase NOT connected");
      return;
      }

    try
      {
      dbase.dbaseLogInsert(wana_id,logall_service,logall_dboper,event_desc);        
      }
    catch ( Exception lexc )
      {
      println(classname+"logEvent ",lexc);
      }
    }
    
  public void logError ( String desc, Exception exc)
    {
    if ( dbase == null )
      {
      println(classname+"logError: ERROR dbase==null");
      return;
      }
    
    if ( ! dbase.isConnected() ) 
      {
      println(classname+"logError: ERROR dbase NOT connected");
      return;
      }

    try
      {
      dbase.dbaseLogInsert(1,"change",desc,exc.toString());        
      }
    catch ( Exception lexc )
      {
      println(classname+"logError ",lexc);
      }
    }
    
  @Override
  public void println ( String message )
    {
    wstat.println(message);    
    }
  
  @Override
  public void println ( String message, Throwable exc )
    {
    wstat.println(message,exc);    
    }

  public Locale getClientLocale ()
    {
    if ( clientLocale == null ) return Locale.ENGLISH;
    
    return clientLocale;
    }
  
  /**
   * WARNING: You MUST call this one once you have finished using processing the method to release
   * the dbase resources or possibly something else.
   */
  public void dispose ()
    {
    // give the dbase back to the conn pool
    wstat.connpool.releaseDbase(dbase);

    // make sure nobody will use it again
    dbase=null;
    }
  
  
  /**
   * You MUST call dispose but if you ever forget maybe this will try to recover.
   */
  public void finalize () throws Throwable
    {
    if ( dbase == null ) return;
    
    if ( dbase.isClosed() ) return;
    
    println(classname+"finalize() remote="+remoteAddr+" ident="+ident+" NOTICE closing Dbase");

    dispose();
    }
  
  
  
  }  

