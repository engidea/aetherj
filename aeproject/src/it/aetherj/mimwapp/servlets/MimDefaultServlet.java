package it.aetherj.mimwapp.servlets;

import java.io.*;
import java.sql.SQLException;

import javax.servlet.http.*;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.dbtbls.AedbCache;
import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.mimwapp.MimServiceEnv;
import it.aetherj.protocol.*;
import it.aetherj.shared.*;
import it.aetherj.shared.v0.*;

public class MimDefaultServlet extends HttpServlet
  {
  private static final long serialVersionUID = 1L;
  private static final String classname="MimDefaultServlet";
  
  public static final String index_json = "index.json";

  private long srvlet_start_time;  // set when either GET or POST is called
  
  /**
   * Subclasses define a method that returns the entity we are working with
   * With this I should be able to manage the GET cache in a reasonable way
   * @return
   */
  protected MimEntity entity()
    {
    return MimEntityList.mimwwwroot;
    }
  
  /**
   * If subclasses wish to set a specific mask they must override this
   * @return
   */
  protected int getDebugMask()
    {
    return 0;
    }
  
  /**
   * Subclasses provides me with the path base, that is the part that "constant"
   * @return
   */
  protected final String pbase()
    {
    MimEntity e = entity();
    return e.getMethod()+"/";
    }
  
  protected void resetStartTime ()
    {
    srvlet_start_time=System.currentTimeMillis();
    }
  
  protected long getStartTime ()
    {
    return srvlet_start_time;
    }
  
  private void wrCacheEntityIndex ( MimServiceEnv env, HttpServletRequest req, HttpServletResponse resp ) throws IOException, SQLException
    {
    env.println("  wrCacheEntityIndex: "+entity());

    final AedbCache aedbCache = (AedbCache)env.getAedb(MimEntityList.aecache);

    // this is asking for the index cache, get it from the cache
    String towrite = aedbCache.getEntityIndex(env.dbase(), entity()); 

    if ( towrite == null )
      writeErrorResponse(env, req, resp, AettpStatus.res_NOTFound, "null cache content, possibly cache rebuild");
    else
      env.writeResponse(resp,AettpStatus.res_OK, towrite);
    }

  private void wrCacheEntityContent ( MimServiceEnv env, HttpServletResponse resp, String method, String what ) throws IOException, SQLException
    {
    env.println("  wrCacheEntityContent: "+entity()+" method="+method+" what="+what);
    
    if ( what == null )
      {
      resp.setStatus(AettpStatus.res_NOContent);
      return;
      }

    final AedbCache aedbCache = (AedbCache)env.getAedb(MimEntityList.aecache);
    
    String pid_s = method.substring(MimResultsCache.cache_prefix.length());
    Integer pid_i = Aeutils.convStringToInteger(pid_s, 0);
    
    String towrite=null;
    
    if ( what.equals("0.json"))
      towrite = aedbCache.getContent(env.dbase(),entity(),AedbCache.cn_content, pid_i);
    else if ( what.equals("manifest"))
      towrite = aedbCache.getContent(env.dbase(),entity(),AedbCache.cn_manifest, pid_i);
    else if ( what.equals("index"))
      towrite = aedbCache.getContent(env.dbase(),entity(),AedbCache.cn_index, pid_i);

    if ( towrite == null )
      {
      resp.setStatus(AettpStatus.res_NOContent);
      return;
      }
    
//    verifyPayloadSignature(env, towrite);
    
    env.writeResponse(resp, AettpStatus.res_OK, towrite);
    }
  
  
  private void verifyPayloadSignature ( MimServiceEnv env, String input ) throws JsonProcessingException
    {
    AeJson mapper = new AeJson();
    CryptoEddsa25519 crypto = new CryptoEddsa25519();
    
    MimPayload rebuild = mapper.readValue(input, MimPayload.class);

    if ( rebuild.isSignatureValid(crypto) )
      {
      env.println("  srvlet rebuild VALID");
      }
    else
      {
      env.println("  srvlet rebuild BAD");
      }

    
    }
  
  
  protected void doGetWork (MimServiceEnv env, HttpServletRequest req, HttpServletResponse resp ) throws IOException, SQLException 
    {
    final String uri = req.getRequestURI();

    if ( entity() == MimEntityList.mimwwwroot )
      {
      String msg= classname+".doGetWork: UNMAPPED mimwwwroot wanted "+uri;
      writeErrorResponse(env, req, resp, AettpStatus.res_NOTFound, msg);
      return;
      }
    
    String sub = uri.substring(pbase().length());
    
    String [] split = sub.split("/");

    String method = split[0]; 
    
    env.println(classname+".doGetWork: entity="+entity()+" sub="+sub+" method="+method);
     
    if ( index_json.equals(method))
      wrCacheEntityIndex(env,req,resp);
    else if ( method.startsWith(MimResultsCache.cache_prefix))
      wrCacheEntityContent(env,resp,method,split[1]);      
    else
      {
      String msg= "UNMAPPED entity="+entity()+" wanted "+uri;
      writeErrorResponse(env, req, resp, AettpStatus.res_NOTFound, msg);
      }
    }

  public final void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException 
    {
    MimServiceEnv env = new MimServiceEnv(this,req).setIdent(classname+".doGet");

    try
      { 
      resetStartTime();
      doGetWork(env,req,resp);
      }
    catch ( Exception exc )
      {
      // I do not know why but apparently there is an exception every request ...
      env.println(classname+".doGet",exc);        
      }
    
    env.dispose();
    }
  
  
  
  protected void writeErrorResponse ( MimServiceEnv  env, HttpServletRequest req, HttpServletResponse resp, int errcode, String msg ) throws IOException  
    {
    env.println(classname+".writeErrorResponse: from "+env.getRemoteAddress()+" url "+req.getRequestURI());
    env.println("   errcode="+errcode+" msg="+msg);

    resp.setContentType("text/html");
    resp.setCharacterEncoding("UTF-8");
    resp.setStatus(errcode);

    PrintWriter out = resp.getWriter();
    
    out.println("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN' >");
    out.println("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
    out.println("<head>");
    out.println("<title>Aetherj error message</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>"+msg+"</p>");
    out.println("</body>");
    out.println("</html>");
    out.close();
    }
  
  
  protected MimPayload getMimPayload ( MimServiceEnv  env, HttpServletRequest req, HttpServletResponse resp) throws IOException 
    {
    // need to pick up the information that is being sent
    MimPayload mimreq = env.getRequest( MimPayload.class );

    if ( ! mimreq.isSignatureValid(env.crypto))
      {
      writeErrorResponse(env, req, resp, AettpStatus.res_Mismatch, "Signature is NOT valid");
      return null;
      }
    
    if ( ! env.isNonceValid(mimreq.node_public_key, mimreq.nonce, mimreq.timestamp))
      {
      writeErrorResponse(env, req, resp, AettpStatus.res_Mismatch, "NOnce is NOT valid");
      return null;
      }
     
    
    MimPowValue pow = mimreq.verifyPow(null);
    if ( ! pow.isValid() )
      {
      writeErrorResponse(env, req, resp, AettpStatus.res_Mismatch, "POW is NOT valid");
      return null;
      }
    
    return mimreq;
    }
  
  protected void doPostWork ( MimServiceEnv  env, HttpServletRequest req, HttpServletResponse resp) throws Exception
    {
    writeErrorResponse(env,req,resp,AettpStatus.res_NOTFound,"UNSUPPORTED url");
    }
  
  public final void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException 
    {
    MimServiceEnv env = new MimServiceEnv(this,req).setIdent(classname+".doPost");

    try
      {
      resetStartTime();
      doPostWork(env,req,resp);
      }
    catch ( Exception exc )
      {
      env.println(classname+".doPost",exc);        
      }
    
    env.dispose();
    }
  }
