package it.aetherj.mimwapp.servlets;

import java.sql.SQLException;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.dbtbls.*;
import it.aetherj.backend.mimclient.McwAddressTableModel;
import it.aetherj.mimwapp.MimServiceEnv;
import it.aetherj.protocol.*;
import it.aetherj.shared.Aedbg;
import it.aetherj.shared.v0.*;

public class MimResponseServlet<T extends MimEmessagesMethods>
  {
  private static final String classname="MimResponseServlet";
  
  private static final int cache_rows_max=200;
  
  private final MimServiceEnv env;
  private final MimEntity entity;
  
  private final T [] r_type; 

  public MimResponseServlet(MimServiceEnv env, MimEntity entity, T [] r_type )
    {
    this.env = env;
    this.entity=entity;
    this.r_type=r_type;
    }
  
  @SuppressWarnings("unchecked")
  private MimItemsArray<T>getDbResponse ( MimFilter [] filters ) throws SQLException
    {
    if ( env.shouldPrint(Aedbg.M_res_filter, Aedbg.L_debug) )
        env.println(classname+"getDbResponse: entity="+entity+" filters="+filters);
    
    MimItemsArray<T>rows = new MimItemsArray<>(r_type);
    
    if ( filters == null || filters.length < 1 )
      return rows;
    
    if ( entity.equals(MimEntityList.aeaddres))
      {
      // Addresses have a special treatment
      fillMcwAddressResponse((MimItemsArray<MimAddress>)rows,filters);
      return rows;
      }
    
    if ( env.shouldPrint(Aedbg.M_res_filter, Aedbg.L_debug) )
      env.println(classname+"getDbResponse: entity="+entity);

    AedbOperations dboper = env.getAedb(entity);

    for ( MimFilter filter : filters )
      getDbResponse ( rows, dboper, filter ); 

    return rows;
    }

  private void fillMcwAddressResponse (MimItemsArray<MimAddress>rows,  MimFilter [] filters ) throws SQLException
    {
    if ( env.shouldPrint(Aedbg.M_res_filter, Aedbg.L_debug) )
      env.println(classname+"fillMcwAddressResponse: ");

    for ( MimFilter filter : filters )
      getMcwAddressResponse ( rows, filter ); 
    
    }

  private void getMcwAddressResponse(MimItemsArray<MimAddress>rows, MimFilter filter ) throws SQLException
    {
    if ( filter == null || filter.isNull() )
      return;
    
    if ( env.shouldPrint(Aedbg.M_res_filter, Aedbg.L_debug) )
      env.println(classname+"getMcwAddressResponse: filter="+filter);

    if ( filter.isTimestamp() )
      getMcwAddressResponse(rows, new MimFilterTimestamp(filter));  
    
    
    }

  private void getMcwAddressResponse(MimItemsArray<MimAddress>rows, MimFilterTimestamp filter ) throws SQLException
    {
    McwAddressTableModel tmodel = env.getAddressModel();
     
    Iterator<MimAddress>iter =tmodel.selectForServlet(filter);
     
    while ( iter.hasNext() && rows.size() < cache_rows_max )
      {
      MimAddress item = iter.next();
      rows.add(item);
      }
    
    }

  
  /**
   * Get some responses from DB using the given filter
   * @param rows
   * @param dboper
   * @param filter
   * @throws SQLException 
   */
  private void getDbResponse(MimItemsArray<T>rows, AedbOperations dboper, MimFilter filter ) throws SQLException
    {
    if ( filter == null || filter.isNull() )
      return;
    
    if ( filter.isTimestamp() )
      getDbResponse(rows, dboper, new MimFilterTimestamp(filter));
    else if ( filter.isFingerprint() )
      getDbResponse(rows, dboper, new MimFilterFingerprint(filter));
    
    
    }
  
  private void getDbResponse(MimItemsArray<T>rows, AedbOperations dboper, MimFilterFingerprint filter ) throws SQLException
    {
    if ( ! (dboper instanceof AedbMimOperations) )
      {
      env.println(classname+".getDbResponse: unsupported class "+dboper.getClass());
      return;
      }
    
    AedbMimOperations<T>dbomim = (AedbMimOperations<T>)dboper;
    
    // I wish to know exctly how many params I have to create
    int limit = Math.min(cache_rows_max - rows.size(), filter.size());
    
    Iterator<T>iter = dbomim.selectForServlet(env.dbase(), filter, limit);
    
    while ( iter.hasNext() && rows.size() < cache_rows_max )
      {
      T item = iter.next();
      rows.add(item);
      }
    
    }

  private void getDbResponse(MimItemsArray<T>rows, AedbOperations dboper, MimFilterTimestamp filter ) throws SQLException
    {
    if ( ! (dboper instanceof AedbMimOperations) )
      {
      env.println(classname+".getDbResponse: unsupported class "+dboper.getClass());
      return;
      }
    
    AedbMimOperations<T>dbomim = (AedbMimOperations<T>)dboper;
    
    int limit = cache_rows_max - rows.size();
    
    Iterator<T>iter = dbomim.selectForServlet(env.dbase(), filter, limit);
    
    while ( iter.hasNext() && rows.size() < cache_rows_max )
      {
      T item = iter.next();
      rows.add(item);
      }
    
    }
  
  
  public MimPayload getMimResponse ( MimFilter [] filters ) throws SQLException, JsonProcessingException
    {
    MimItemsArray<T>rows = getDbResponse(filters);
    
    MimPayload payload = env.newMimPayload(entity);
    
    // this is because the original Aether has arrays instead of lists, whatever + no usable OOP 
    if ( entity.equals(MimEntityList.aeaddres))
      {
      payload.response.addresses = (MimAddress[])rows.toArray();
      }
    if ( entity.equals(MimEntityList.aekey))
      {
      payload.response.keys = (MimKey[])rows.toArray();
      }
    else if ( entity.equals(MimEntityList.aeboard))
      {
      payload.response.boards = (MimBoard[])rows.toArray();
      }
    else if ( entity.equals(MimEntityList.aethread))
      {
      payload.response.threads = (MimThread[])rows.toArray();
      }
    else if ( entity.equals(MimEntityList.aepost))
      {
      payload.response.posts = (MimPost[])rows.toArray();
      }
    else if ( entity.equals(MimEntityList.aevote))
      {
      payload.response.votes = (MimVote[])rows.toArray();
      }
    else if ( entity.equals(MimEntityList.aetrustate))
      {
      payload.response.truststates = (MimTruststate[])rows.toArray();
      }
    
    return payload;
    }

  
  
  
  }
