/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.mimwapp.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.*;

import it.aetherj.backend.mimclient.McwAddress;
import it.aetherj.backend.mimclient.ssl.McwJobStats;
import it.aetherj.mimwapp.MimServiceEnv;
import it.aetherj.protocol.*;
import it.aetherj.shared.*;
import it.aetherj.shared.v0.*; 


public final class VoteServlet extends MimDefaultServlet
  {
  private static final long serialVersionUID = 1L;
  private static final String classname="VoteServlet";
  
  private static final MimEntity entity = MimEntityList.aevote;
  private static final int debug_mask = Aedbg.entity_Vote;

  @Override
  protected MimEntity entity()
    {
    return  entity;
    }

  /**
   * Do the actual work, here I know that the session is valid.
   * @throws SQLException 
   */
  protected void doPostWork (MimServiceEnv env, HttpServletRequest req, HttpServletResponse resp ) throws IOException, SQLException 
    {
    env.println(classname+".doPostWork() POST ");

    // need to pick up the information that is being sent
    MimPayload mimreq = getMimPayload(env, req, resp);
    
    if ( mimreq == null )
      return;
    
    // make sure location is correct
    mimreq.address.location = env.getRemoteAddress();
    
    // assume it is good, save the given address as a peer
    McwAddress address = env.mcwAddressSave(mimreq.address);
 
    MimResponseServlet<MimVote>res_srv = new MimResponseServlet<>(env, entity, new MimVote[0]);
    
    MimPayload p_res = res_srv.getMimResponse(mimreq.filters);
    
    env.writeResponse(resp, p_res);

    address.setSyncStats(new McwJobStats(AettpStatus.OK, getStartTime()));
    } 
  }
