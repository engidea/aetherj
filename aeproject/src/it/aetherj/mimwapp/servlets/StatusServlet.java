/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.mimwapp.servlets;

import java.io.IOException;

import javax.servlet.http.*;

import it.aetherj.mimwapp.MimServiceEnv;
import it.aetherj.protocol.*;
import it.aetherj.shared.AettpStatus; 

/**
 * This is bound to /v0/ping/status, 
 * Annotation is a mess, some people on drug invented this whole mess


@WebServlet(
    name = "StatusServlet",
    urlPatterns = {"/v0/ping/status","/v0/status"}
  )

 */

public final class StatusServlet extends MimDefaultServlet
  {
  private static final long serialVersionUID = 1L;
  private static final String classname="StatusServlet";

  protected MimEntity entity()
    {
    return  MimEntityList.aestatus;
    }
  
  @Override
  protected void doGetWork(MimServiceEnv env, HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
    env.println(classname+".doGetWork() from "+req.getRemoteAddr());
    
    resp.setStatus(AettpStatus.res_OK);
    }
  
  }
