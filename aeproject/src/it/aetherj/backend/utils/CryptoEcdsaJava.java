/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.utils;

import java.security.*;
import java.security.interfaces.*;
import java.security.spec.*;

import it.aetherj.backend.Stat;
import it.aetherj.protocol.MimHex;

/**
 * The following methods work with Java 9 and nothing else, pure Java
 * This is an attempt to use pure Java for the elliptic curve, apparently it is not available
 * WARNING: This code is UNTESTED, skeleton
 */
public class CryptoEcdsaJava
  {
  private static final String classname="CryptoEcdsa";
  
  private static final String curveName = "secp521r1";
  private static final String publicFmt="X.509";
  private static final String privateFmt="PKCS#8";
  
  public final Stat stat;
  
  private final KeyFactory keyFactory;


  public CryptoEcdsaJava(Stat stat)
    {
    this.stat = stat;
    
    keyFactory = newKeyFactory();
    }

  private KeyFactory newKeyFactory()
    {
    try
      {
      return KeyFactory.getInstance("EC");
      }
    catch ( Exception exc )
      {
      // this, normally, must not fail, if it does, I want to know it as early as possible
      stat.log.exceptionShow(classname+".construct", exc);
      return null;
      }
    }
  
  public KeyPair newKeyPair ()
    {
    try
      {
      KeyPairGenerator generator = KeyPairGenerator.getInstance("EC");
      ECGenParameterSpec spec = new ECGenParameterSpec(curveName);
      generator.initialize(spec);
      return generator.generateKeyPair();
      }
    catch ( Exception exc )
      {
      stat.log.println(classname+".newKeyPair",exc);
      return null;
      }
    }
  
  /**
   * This is the reverse of the previous toHex
   * @param source the first part before the : is the format
   * @return the converted PrivateKey
   * @throws InvalidKeySpecException
   */
  public ECPrivateKey getPrivate(String source) throws InvalidKeySpecException  
    {
    String [] split = source.split(":");
    
    if ( ! privateFmt.equals(split[0]) )
        throw new IllegalArgumentException("Can only parse "+privateFmt);
    
    byte[]encoded = MimHex.convStringToBin(split[1]);
    EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encoded);
    PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
    return (ECPrivateKey)privateKey;
    }
  
  /**
   * This is the reverse of the previous toHex
   * @param source the first part before the : is the format
   * @return the converted ECPublicKey
   * @throws InvalidKeySpecException
   */
  public ECPublicKey getPublic(String source) throws InvalidKeySpecException 
    {
    String [] split = source.split(":");
  
    if ( ! publicFmt.equals(split[0]) )
      throw new IllegalArgumentException("Can only parse "+publicFmt);
  
    byte[]encoded = MimHex.convStringToBin(split[1]);
    EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encoded);
    PublicKey pub = keyFactory.generatePublic(publicKeySpec);
    return (ECPublicKey)pub;
    }
  
  public String toHex ( ECPrivateKey key )
    {
    StringBuilder risul = new StringBuilder(500);
    
    risul.append(key.getFormat());
    risul.append(":");
    risul.append(MimHex.convBinToHexString(key.getEncoded()));
    
    return risul.toString();
    }
  
  public String toHex (ECPublicKey key )
    {
    StringBuilder risul = new StringBuilder(500);
    
    risul.append(key.getFormat());
    risul.append(":");
    risul.append(MimHex.convBinToHexString(key.getEncoded()));
    
    return risul.toString();
    }

  public ECPrivateKey getPrivate ( KeyPair kp )
    {
    return (ECPrivateKey)kp.getPrivate(); 
    }
    
  public ECPublicKey getPublic ( KeyPair kp )
    {
    return (ECPublicKey)kp.getPublic();
    }

  public void listAlgo()
    {
    for (Provider provider : Security.getProviders())
      {
      stat.log.println(provider.getName());

      for (Provider.Service service : provider.getServices()) 
        stat.log.println("\t" + service.getAlgorithm());
      }
    }
  
  private void println ( String s )
    {
    stat.log.println(s);
    }

  
  public void testConversion ()
    {
    try
      {
      KeyPair pp=newKeyPair();
      ECPublicKey pub=getPublic(pp);
      String pubs=toHex(pub);
      println("o pubs "+pubs);
      ECPublicKey npub=getPublic(pubs);
      println("r pubs "+toHex(npub));
      
      ECPrivateKey priv=getPrivate(pp);
      String privs=toHex(priv);
      println("o priv "+privs);
      ECPrivateKey npriv=getPrivate(privs);
      println("r priv "+toHex(npriv));
      }
    catch ( Exception exc )
      {
      stat.log.exceptionShow("eRROR", exc);
      }
    }
  
  }
