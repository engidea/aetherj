/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.utils;

import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.Map.Entry;

import it.aetherj.backend.Stat;
import it.aetherj.shared.v0.*;

/*
 * From nonces.go of the original Aether
Why?
Nonces prevent replay attacks.
Imagine this scenario. A remote sends you a request. This is a POST request specifying an intricate query. This request is signed by the remote's public key, so we know that it's coming from a valid aenode.
Somehow, that request gets captured. Normally, the connections between nodes are encrypted by TLS, so there is no practical way this can easily happen. But let's say it happened, and the connection got MiTM'ed. The attacker got a copy of the remote's request to us.
What can this attacker, let's call him Mallory, can do to us? Well, it can send that copy of that request thousands of times to us, creating a DDoS that we would need to deal with. It cannot tamper with the request, though, since it is signed. Obviously, the local aenode will notice that it's being asked to do something off, and it will ban this PK for a period of time. But the PK that is banned will be the PK of the original remote, not Mallory's, since Mallory is just resenting the original remote's request to us many times.
So what Mallory achieved is he managed to get us to ban an innocent third party because he managed to MiTM the connection and sent a copy of the request. That's not the end of the world, but it's also not that great.
So, to prevent that, we are doing two things:
a) We are adding nonces to every request.
b) We are putting a limit on the difference between the local and the remote (called clock skew). This will come handy later.
What do these things do?
Well, nonces prevent resending of the same request. When you look at a nonce, and see that nonce in the list of nonces you've received in the past, you know it was a duplicate request, and deny it. Mallory just got denied.
This creates a problem though. That means you have to keep track of every nonce you've ever received to be able to check against the list, and that list is going to grow very large.
So we have a maximum allowed clock skew. This means, if the difference between our own current time and the remote's current time is larger than maximum allowed clock skew, it won't be accepted.
Why does this help us? Let's say Mallory got a hold of the request with a timestamp of now. He can only use this request to make replay attacks until the maximum allowed clock skew (MACS) is exceeded. If our MACS is 20 minutes, Mallory's copy of the request will be rendered unacceptable 20 minutes later. That means any request whose timestamp is outside +20 -20 boundaries is declined by default. This is great! Because what it means is that we do not have to keep nonces for longer than that range, anything outside that range is actually auto-declined. Anything inside the range is accepted ONLY if the nonce is something we've not seen before.
One gotcha is that it's not just in the past, but also in the future. That is, if your MACS is 20 minutes, that gives you a 40 minutes range, 20 minutes into the past and 20 minutes into the future. So your nonce deletion point should definitely be higher than 40. It's a good call to have it be 2.5x the MACS.
So limiting clock skew makes it so that we only have to keep the nonces that are within the clock skew range.
*/
public class MimNonceMap
  {
  private static final String classname="NonceMap";
  
  private final Stat stat;
  
  // maximum allowed clock skew
  private static final long clock_skew_s = 20*60;
  
  // do garbage collect every this amount of seconds
  private static final long gc_interval_s = 10;

  private final HashMap<MimPublicKey,NoncePublicKey>noncePkeyMap;
  
  private long last_gc_time_s;   // last time I did a GC
  
  public MimNonceMap(Stat stat) throws NoSuchAlgorithmException 
    {
    this.stat=stat;

    noncePkeyMap = new HashMap<>();
    }
    
  /**
   * Check if this nonce is acceptable
   * @param pkt_publicKey
   * @param pkt_nonce
   * @param pkt_timestamp
   * @return true if it is a valid nonce
   */
  public synchronized boolean isValid ( MimPublicKey pkt_publicKey, MimNonce pkt_nonce, MimTimestamp pkt_timestamp )
    {
    if ( pkt_publicKey == null || pkt_nonce == null || pkt_timestamp == null )
      return false;
    
    if ( pkt_publicKey.isNull() || pkt_nonce.isNull() || pkt_timestamp.isNull() )
      return false;
    
    long now_s = System.currentTimeMillis() / 1000;

    // if the provided timestamp is too old in any case for my current time
    if ( ! isTimestampValid(now_s, pkt_timestamp.getJsonValue()) )
      return false;

    // if required do a garbage collect
    garbageCollect(now_s);
    
    // Attempt to get info on the given public key
    NoncePublicKey nonpk = noncePkeyMap.get(pkt_publicKey);
    
    if ( nonpk == null )
      {
      nonpk = new NoncePublicKey(now_s, pkt_nonce);
      noncePkeyMap.put(pkt_publicKey, nonpk);
      return true;
      }
    
    NonceTime noncetime = nonpk.get(pkt_nonce);
    
    if ( noncetime== null)
      {
      nonpk.put(now_s, pkt_nonce);
      return true;
      }

    // nonce is already present, request denied
    return false;
    }
  
  private boolean isTimestampValid (long now_s, long have_timestamp_s )
    {
    if ( have_timestamp_s > now_s + clock_skew_s )
      return false;
    
    if ( have_timestamp_s < now_s - clock_skew_s )
      return false;
    
    return true;
    }
  
  private boolean isGarbageCollect (long now_s,  long ins_time_s)
    {
    return now_s > ins_time_s + clock_skew_s;
    }
    
  private void garbageCollect (long now_s, NoncePublicKey noncepk)
    {
    // this iterator allows the it.remove
    Iterator<Entry<MimNonce,NonceTime>> it = noncepk.entrySet().iterator();
    
    while (it.hasNext()) 
      {
      Entry<MimNonce, NonceTime> pair = it.next();
      NonceTime nontime = pair.getValue();
      
      if ( isGarbageCollect(now_s, nontime.ins_timestamp_s))
        it.remove(); 
      }
    
    }
  
  private void garbageCollect (long now_s)
    {
    if ( now_s < last_gc_time_s + gc_interval_s )
      return;
    
    // remember the last time I did a GC
    last_gc_time_s=now_s;
    
    // this iterator allows the it.remove
    Iterator<Entry<MimPublicKey, NoncePublicKey>> it = noncePkeyMap.entrySet().iterator();
    
    while (it.hasNext()) 
      {
      Entry<MimPublicKey, NoncePublicKey> pair = it.next();
      NoncePublicKey nonpk = pair.getValue();
      garbageCollect(now_s, nonpk);

      if ( nonpk.size() < 1 )
        it.remove(); 
      }
    }

  private void println(String message)
    {
    stat.log.println(message);
    }
    
  public String toString()
    {
    StringBuilder risul = new StringBuilder(500);
    
    risul.append("noncePkeyMap.size="+noncePkeyMap.size());
    risul.append(" last_gc_time_s="+last_gc_time_s);
    
    return risul.toString();
    }
  }
