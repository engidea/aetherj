/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.utils;

import java.io.IOException;
import java.security.*;
import java.security.spec.*;

import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;
import net.i2p.crypto.eddsa.*;
import net.i2p.crypto.eddsa.KeyPairGenerator;
import net.i2p.crypto.eddsa.spec.*;

/**
 * From file signaturing.go it says that the algorithm used is ED25519 from
 * https://golang.org/pkg/crypto/ed25519/
 * The web says: The signature algorithm is ECDSA. The specific curve is given
 * at the 'type' field of the key object, but is usually secp521r1.
 *
 * One of the issues is that it is not clear what is stored in the config files
 */
public final class CryptoEddsa25519  
  {
  // this is a parameter table that can be defined static, it is just parameters
  public static final EdDSAParameterSpec paramSpec=EdDSANamedCurveTable.getByName(EdDSANamedCurveTable.ED_25519);

  private static final String publicFmt = "X.509";
  private static final String publicFmt_A = "X.509_A";
  
  private static final String privateFmt = "PKCS#8";
  private static final String privateFmt_Seed = "PKCS#8_Seed";

  private final MimDigest digestAlgo;

  public CryptoEddsa25519() 
    {
    digestAlgo = new MimDigest();
    }

  public static KeyPair newKeyPair()
    {
    KeyPairGenerator generator = new KeyPairGenerator();
    generator.initialize(256, new SecureRandom());
    return generator.generateKeyPair();
    }

  
  public static String toHex ( PrivateKey key )
    {
    if ( key == null )
      return null;
    
    StringBuilder risul = new StringBuilder(500);
    
    risul.append(key.getFormat());
    risul.append(":");
    risul.append(MimHex.convBinToHexString(key.getEncoded()));
    
    return risul.toString();
    }

  /**
   * This is the reverse of toHex(PrivateKey)
   * @param source 
   * @return
   * @throws InvalidKeySpecException
   */
  public static PrivateKey parsePrivateKey(String source) throws InvalidKeySpecException
    {
    if ( source == null )
      throw new IllegalArgumentException("source is null");
    
    String [] split = source.split(":");
    
    if ( ! privateFmt.equals(split[0]) )
        throw new IllegalArgumentException("Can only parse "+privateFmt);
    
    byte[]encoded = MimHex.convStringToBin(split[1]);

    PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
    return new EdDSAPrivateKey(keySpec);
    }
  
  public static String toHex (PublicKey key )
    {
    if ( key == null )
      return null;

    StringBuilder risul = new StringBuilder(500);
    
    risul.append(key.getFormat());
    risul.append(":");
    risul.append(MimHex.convBinToHexString(key.getEncoded()));
    
    return risul.toString();
    }

  /**
   * This is the reverse of toHex(PublicKey)
   * @param source 
   * @return PublicKey
   * @throws InvalidKeySpecException
   */
  public static PublicKey parsePublicKey(String source) throws InvalidKeySpecException
    {
    if ( source == null )
      throw new IllegalArgumentException("source is null");

    String [] split = source.split(":");
    
    if ( ! publicFmt.equals(split[0]) )
        throw new IllegalArgumentException("Can only parse "+publicFmt);
    
    byte[]encoded = MimHex.convStringToBin(split[1]);

    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
    return new EdDSAPublicKey(keySpec);
    }
  
  /**

  func Sign(input string, privKey *ed25519.PrivateKey) (string, error) {
  // This creates an ed25519 signature for the input provided.
  // Mind that signatures are generated on hashes of items, not the item itself. So the first step in either signing or verifying is to hash the input provided.
  inputByte := []byte(input)
  hasher := sha256.New()
  hasher.Write(inputByte)
  hash := hasher.Sum(nil)
  sigAsByte, err := privKey.Sign(nil, hash, crypto.Hash(0))
  if err != nil {
    return "", errors.New(fmt.Sprint(
      "Signing failed. err: ", err))
  }
  signature := hex.EncodeToString(sigAsByte)
  return signature, nil
}
  
 */

  /**
   * In theory I should use the other one with PrivateKey
   * @param privateKey
   * @param digest
   * @return
   */
  public MimSignature mimSign (MimPrivateKey privateKey , MimDigest digest )
    {
    return mimSign(privateKey.getPrivateKey(),digest);
    }
  
  /**
   * The new way of doing signatures on the digest of whatever was digested before 
   */
  public MimSignature mimSign (PrivateKey privateKey , MimDigest digest )
    {
    byte []digested = digest.digestEnd();
    return new MimSignature(sign(privateKey,digested));
    }

  
  public MimSignature mimSign ( MimPrivateKey privateKey, String input )
    {
    return mimSign(privateKey.getPrivateKey(),input);
    }

  
  public synchronized MimSignature mimSign (PrivateKey privateKey , String input )
    {
    try
      {
      byte []digested = digestAlgo.digestOnce(input);
      byte []b_out = sign(privateKey, digested);
      return new MimSignature(b_out);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      return new MimSignature();
      }
    }

  private byte[] sign(PrivateKey privateKey,byte[] messageByte) 
    {
    try
      {
      EdDSAEngine signature = new EdDSAEngine(MessageDigest.getInstance(paramSpec.getHashAlgorithm()));
      signature.initSign(privateKey);
      signature.setParameter(EdDSAEngine.ONE_SHOT_MODE);
      signature.update(messageByte);
      return signature.sign();
      }
    catch ( Exception exc )
      {
      return new byte[0];
      }
    }

  /**
   * This is used in Aether old since it string all together and then comes here
   * The logic is that if there is no public key the content is verified by default
   */
  public boolean mimVerify ( MimPublicKey pk, String input, MimSignature signature )
    {
    if ( pk == null )
      return true;
    
    return mimVerify(pk.getPublicKey(),input,signature);
    }

  /**
   * This is when you have a digest with all the pieces digested and you are at last step of verification
   * The logic is that if there is no public key the content is verified by default
   */
  public boolean mimVerify ( MimPublicKey pk, MimDigest digest, MimSignature signature )
    {
    if ( pk == null )
      return true;

    byte []digested = digest.digestEnd();
    return verify(pk.getPublicKey(), digested, signature.getBytes());
    }
  
  private synchronized boolean mimVerify ( PublicKey pk, String input, MimSignature signature )
    {
    try
      {
      byte []digested = digestAlgo.digestOnce(input);
      return verify(pk, digested, signature.getBytes());
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      return false;
      }
    }

  /**
   * Verify that the whatever "decrypted" with publicKey matches signature
   * @param publicKey
   * @param whatever
   * @param signatureByte
   * @return
   */
  private boolean verify(PublicKey publicKey, byte[] whatever, byte[] signatureByte)
     {
     try
        {
        EdDSAEngine signature = new EdDSAEngine(MessageDigest.getInstance(paramSpec.getHashAlgorithm()));
        signature.initVerify(publicKey);
        signature.setParameter(EdDSAEngine.ONE_SHOT_MODE);
        signature.update(whatever);
        return signature.verify(signatureByte);
        }
      catch ( Exception exc )
        {
        exc.printStackTrace();
        return false;
        }
     }
 
  public static String toHex_Seed(PrivateKey key ) throws IOException, InvalidKeySpecException 
    {
    // Need this since the generic PrivateKey does NOT have a getSeed()
    PKCS8EncodedKeySpec encoded = new PKCS8EncodedKeySpec(key.getEncoded());
    EdDSAPrivateKey s_private = new EdDSAPrivateKey(encoded);
    byte[] seed = s_private.getSeed();
    return privateFmt_Seed+":"+MimHex.convBinToHexString(seed);
    }
  
  /**
   * This is the reverse of toHex_Seed(PrivateKey)
   * Only the private key is exported
   * @param source
   * @return
   */
  public static PrivateKey parsePrivate_Seed(String source)
    {
    String [] split = source.split(":");
    
    if ( ! privateFmt_Seed.equals(split[0]) )
        throw new IllegalArgumentException("Can only parse "+privateFmt_Seed);
    
    byte[]seed = MimHex.convStringToBin(split[1]);

    EdDSAPrivateKeySpec privateSpec = new EdDSAPrivateKeySpec(seed, paramSpec);
    return new EdDSAPrivateKey(privateSpec);
    }
  
  /**
   * This converts a public key to the format that is used by mim
   * This is kind of proof of concept since it adds a tag at the beginning, so, it is NOT really what Mim uses
   * @param key
   * @return
   * @throws InvalidKeySpecException
   */
  public String toHex_A(PublicKey key) throws InvalidKeySpecException  
    {
    // Need this since the generic PublicKey does NOT have a getA()
    X509EncodedKeySpec encoded = new X509EncodedKeySpec(key.getEncoded());
    EdDSAPublicKey s_public = new EdDSAPublicKey(encoded);
    byte[] pub_A = s_public.getA().toByteArray();
    return publicFmt_A+":"+MimHex.convBinToHexString(pub_A);
    }

  /**
   * This is the reverse of toHex_A(PublicKey key)
   * @param source
   * @return
   */
  public PublicKey parsePublic_A(String source)
    {
    String [] split = source.split(":");
    
    if ( ! publicFmt_A.equals(split[0]) )
        throw new IllegalArgumentException("Can only parse "+publicFmt_A);
    
    byte[]pk = MimHex.convStringToBin(split[1]);

    EdDSAPublicKeySpec publicSpec = new EdDSAPublicKeySpec(pk, paramSpec);
    return new EdDSAPublicKey(publicSpec);
    }

  
  /*
  private void println(String s)
    {
    stat.log.println(s);
    }

  public void testone() throws Exception
    {
    String message = "d02_message.txt";
  
    KeyPair keyPair = newKeyPair();
    PrivateKey privateKey = keyPair.getPrivate();
    PublicKey publicKey = keyPair.getPublic();
  
    String privateB, publicB;
    println("privateA "+toHex(privateKey));
    privateB = toHex_Seed(privateKey);
    println("privateB "+privateB);
    
    PrivateKey privateKeyB = parsePrivate_Seed(privateB);
    println("privateKeyB "+toHex(privateKeyB));
    
    println("publicA "+toHex(publicKey));
    publicB = toHex_A(publicKey);
    println("publicB "+publicB);
    
    PublicKey publicKeyB = parsePublic_A(publicB);
    println("publicKeyB "+toHex(publicKeyB));
    
    byte[] signatureByte = sign(privateKeyB, message.getBytes("UTF-8"));
    println("signatureByte: " + MimHex.convBinToHexString(signatureByte));
    
    boolean signatureGood = verify(publicKeyB, message.getBytes("UTF-8"), signatureByte);
    println("signatureGood: " + signatureGood);
    }
*/
    
    }

  
