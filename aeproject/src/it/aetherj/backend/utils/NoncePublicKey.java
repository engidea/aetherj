/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.utils;

import java.util.*;
import java.util.Map.Entry;

import it.aetherj.shared.v0.MimNonce;

/**
 * The map is first indexed by public key and then, inside, there are a list of nonces
 */
public class NoncePublicKey
  {
  private final HashMap<MimNonce,NonceTime>nonceMap;
  
  NoncePublicKey (long now_s, MimNonce nonce)
    {
    nonceMap = new HashMap<>();
    
    put(now_s, nonce);
    }
  
  public NonceTime get(MimNonce nonce)
    {
    return nonceMap.get(nonce);
    }
  
  public void put (long now_s, MimNonce nonce)
    {
    nonceMap.put(nonce, new NonceTime(now_s));
    }
  
  public int size()
    {
    return nonceMap.size();
    }
  
  public Set<Entry<MimNonce,NonceTime>> entrySet()
    {
    return nonceMap.entrySet();
    }
  }
