/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.utils;

import java.math.BigInteger;
import java.security.*;
import java.security.cert.*;
import java.security.spec.*;
import java.util.*;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import it.aetherj.boot.PrintlnProvider;
import it.aetherj.protocol.MimHex;
import sun.security.util.ObjectIdentifier;
import sun.security.x509.*;

/**
 * The following methods work with Java 9 and nothing else, pure Java
 * Used to create the certificate for the web server, self signed
 * See https://magnus-k-karlsson.blogspot.com/2018/05/how-to-create-x509-certificate-in-java.html
 * NOTE: You MUST use compiler compliance level 1.8 if you wish to compile this, apparently 9 cannot see the methods ....
 * However, you MUST use a 9 runtime, otherwise methods are not avilable
 * What a MESS Oracle has done
 */
public class CryptoRSA
  {
  private static final String classname = "CryptoRSA";
  
  private static final String publicFmt = "X.509";
  private static final String privateFmt = "PKCS#8";
  
  private static final String signAlgo="SHA256withRSA";

  private final PrintlnProvider feedback; 
  private final KeyFactory keyFactory;

  public CryptoRSA(PrintlnProvider feedback)
    {
    this.feedback = feedback;
    this.keyFactory = newKeyFactory();
    }

  private KeyFactory newKeyFactory()
    {
    try
      {
      return KeyFactory.getInstance("RSA");
      }
    catch ( Exception exc )
      {
      feedback.println(classname+".construct", exc);
      throw new IllegalArgumentException("getInstance(\"RSA\") FAILED");
      }
    }

  
  public KeyPair newKeyPair()
    {
    try
      {
      KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
      generator.initialize(2048, new SecureRandom());
      return generator.generateKeyPair();
      } 
    catch (Exception exc)
      {
      feedback.println(classname + ".newKeyPair", exc);
      throw new IllegalArgumentException("KeyPairGenerator.getInstance(\"RSA\") FAILED");
      }
    }

  /**
   * NOTE that I have a tiny heading that says the format of the given data
   */
  public String toHex ( PrivateKey key )
    {
    StringBuilder risul = new StringBuilder(500);
    
    risul.append(key.getFormat());
    risul.append(":");
    risul.append(MimHex.convBinToHexString(key.getEncoded()));
    
    return risul.toString();
    }

  /**
   * This is the reverse of toHex(PrivateKey)
   */
  public PrivateKey parsePrivateKey(String source) throws InvalidKeySpecException
    {
    String [] split = source.split(":");
    
    if ( ! privateFmt.equals(split[0]) )
        throw new IllegalArgumentException("Can only parse "+privateFmt);
    
    byte[]encoded = MimHex.convStringToBin(split[1]);

    PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
    return keyFactory.generatePrivate(keySpec);
    }
  
  public String toHex (PublicKey key )
    {
    if ( key == null )
      return null;

    StringBuilder risul = new StringBuilder(500);
    
    risul.append(key.getFormat());
    risul.append(":");
    risul.append(MimHex.convBinToHexString(key.getEncoded()));
    
    return risul.toString();
    }

  /**
   * This is the reverse of toHex(PublicKey)
   */
  public PublicKey parsePublicKey(String source) throws InvalidKeySpecException
    {
    if ( source == null )
      throw new IllegalArgumentException("source is null");

    String [] split = source.split(":");
    
    if ( ! publicFmt.equals(split[0]) )
        throw new IllegalArgumentException("Can only parse "+publicFmt);
    
    byte[]encoded = MimHex.convStringToBin(split[1]);

    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
    return keyFactory.generatePublic(keySpec);
    }

  
  /**
   * This self signed certificate is to be used by the webserver to provide "secure" connection
   */
  public X509CertImpl newX509cert(PrivateKey privKey, PublicKey publicKey ) throws Exception
    {
    int validDays = 365;
    
    X500Name distinguishedName = new X500Name("Aether","Aether","Privacy","IT");
  
    X509CertInfo info = new X509CertInfo();
  
    Date since = new Date(); // Since Now
    Date until = new Date(since.getTime() + validDays * 86400000l); // Until x days (86400000 milliseconds in one day)
    
    CertificateValidity interval = new CertificateValidity(since, until);
    BigInteger sn = new BigInteger(64, new SecureRandom());

    info.set(X509CertInfo.VALIDITY, interval);
    info.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(sn));
    info.set(X509CertInfo.SUBJECT, distinguishedName);
    info.set(X509CertInfo.ISSUER, distinguishedName);
    info.set(X509CertInfo.KEY, new CertificateX509Key(publicKey));
    info.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));
    
    // X509v3 extensions
    CertificateExtensions exts = new CertificateExtensions();

    // Authority Information Access
//      ArrayList<AccessDescription> accDescr = new ArrayList<>();
//      String urlCA = "https://getaether.net";
//      accDescr.add(new AccessDescription(AccessDescription.Ad_CAISSUERS_Id, new GeneralName(new URIName(urlCA))));
//      exts.set(AuthorityInfoAccessExtension.NAME, new AuthorityInfoAccessExtension(accDescr));
    
    // X509v3 Authority Key Identifier
    exts.set(AuthorityKeyIdentifierExtension.NAME,
      new AuthorityKeyIdentifierExtension(new KeyIdentifier(publicKey),
      new GeneralNames().add(new GeneralName(distinguishedName)), new SerialNumber(1)));

    // X509v3 Basic Constraints: critical=true, ca=false, pathLen=1
    exts.set(BasicConstraintsExtension.NAME, new BasicConstraintsExtension(true, true, 1));

    // Extensions[6]: X509v3 Key Usage
    KeyUsageExtension keyUsage = new KeyUsageExtension();
    keyUsage.set(KeyUsageExtension.DIGITAL_SIGNATURE, Boolean.TRUE);
    keyUsage.set(KeyUsageExtension.KEY_ENCIPHERMENT, Boolean.TRUE);
    keyUsage.set(KeyUsageExtension.KEY_CERTSIGN, Boolean.TRUE);
    exts.set(KeyUsageExtension.NAME, keyUsage);

    // X509v3 Extended Key Usage
    Vector<ObjectIdentifier> keyUsages = new Vector<>();
    // OID defined in RFC 3280 Sections 4.2.1.13
    keyUsages.addElement(ObjectIdentifier.newInternal(new int[] { 1, 3, 6, 1, 5, 5, 7, 3, 1 }));       // serverAuth
//      keyUsages.addElement(ObjectIdentifier.newInternal(new int[] { 1, 3, 6, 1, 5, 5, 7, 3, 2 }));        // clientAuth
    exts.set(ExtendedKeyUsageExtension.NAME, new ExtendedKeyUsageExtension(keyUsages));

    info.set(X509CertInfo.EXTENSIONS, exts);

    AlgorithmId algo = new AlgorithmId(AlgorithmId.sha256WithRSAEncryption_oid);
    info.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(algo));
    
    // Sign the cert to identify the algorithm that is used.
    X509CertImpl cert = new X509CertImpl(info);
    cert.sign(privKey, signAlgo);

    // Update the algorithm and sign again
    algo = (AlgorithmId)cert.get(X509CertImpl.SIG_ALG);
    info.set(CertificateAlgorithmId.NAME + "." + CertificateAlgorithmId.ALGORITHM, algo);
    
    cert = new X509CertImpl(info);
    cert.sign(privKey, signAlgo);
    
    return cert;
    }    
  
  /**
   * NOTE That there is no header or trailer data
   * @param cert
   * @return
   * @throws CertificateEncodingException 
   */
  public String toBase64 ( X509CertImpl cert ) 
    {
    try
      {
      return Base64.encode(cert.getEncoded());
      }
    catch ( Exception exc )
      {
      return "";
      }
    }

  /**
   * This is the reverse of toBase64 ( X509CertImpl cert )
   * NOTE that there is NO heading or trailing of the data
   * @param source
   * @return
   * @throws Base64DecodingException 
   * @throws CertificateException 
   */
  public X509CertImpl parseX509Cert ( String source ) throws Base64DecodingException, CertificateException
    {
    if ( source == null )
      throw new IllegalArgumentException("source is null");
    
    byte []certBytes = Base64.decode(source);
    return new X509CertImpl(certBytes);
    }
  
  

  
  
  
  }
