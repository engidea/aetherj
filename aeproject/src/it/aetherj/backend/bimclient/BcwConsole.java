package it.aetherj.backend.bimclient;

import java.awt.BorderLayout;
import java.security.NoSuchAlgorithmException;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.gui.WinposStore;
import it.aetherj.backend.gui.utils.ComponentProviderSaveWinpos;
import it.aetherj.boot.*;

// NOT used at the moment
public class BcwConsole implements ComponentProviderSaveWinpos
  {
  private static final String classname="BcwConsole";
    
  public static final String rm_Auto="Automatic";
  public static final String rm_Onclick="On Click";
  
  public static final String re_Auto="Automatic";
  public static final String re_Status="Status";     // normally a GET request    /v1/status
  public static final String re_Node="Node";         // can be both GET and POST  /v1/aenode
  public static final String re_PingNode="PingNode";         // can be both GET and POST  /v1/aenode
  public static final String re_Address="Addresses";
  public static final String re_Aekeys="Public Keys";
  public static final String re_Board="Boards";
  public static final String re_Thread="Thread";
  public static final String re_Post="Post";
  public static final String re_Vote="Vote";
  
  public static final String rs_Auto="Automatic";
  public static final String rs_RX_Cache="Receive Cache";
  public static final String rs_POST_Sync="POST Sync";
  public static final String rs_RX_Wish="Receive Wished";
  public static final String rs_GET="HTTP GET";
  public static final String rs_POST="HTTP POST";
  
//  private static final String[] rm_Combo = { rm_Auto, rm_Onclick };
  private static final String[] rm_Combo = { rm_Auto, rm_Onclick };
  private static final String[] re_Combo = { re_Auto, re_Status, re_Node, re_PingNode, re_Address, re_Aekeys, re_Board, re_Thread, re_Post, re_Vote };
  private static final String[] rs_Combo = { rs_Auto, rs_RX_Cache, rs_POST_Sync, rs_RX_Wish, rs_GET, rs_POST };
   
  private final Stat stat;
  private final JPanel  workPanel;
  private final JDesktopPane desktopPanel;

  private final LimitedTextArea logArea;
  private final JTextField feedbackField;

  private final AeDbase dbase;    // use this one to operate with the dbase and leave stat.dbase free !

  
  private JComboBox<String> runMode;   // Automatic, OnClick
  private JComboBox<String> runEntity;  // Automatic, Address, Key(UPKI), Board, Thread, Post, Vote 
  private JComboBox<String> runState;  // Automatic, Sync, Bootstrap, Introduction
   
  private int tickCounter;
  
  /**
   * Constructor
   * @param stat
   * @throws NoSuchAlgorithmException 
   */
  public BcwConsole(Stat stat) 
    {
    this.stat = stat;
    this.logArea       = new LimitedTextArea(3000);
    this.desktopPanel  = new JDesktopPane();
    this.runMode       = new JComboBox<String>(rm_Combo);
    this.runEntity     = new JComboBox<String>(re_Combo);
    this.runState      = new JComboBox<String>(rs_Combo);
    
    dbase = stat.newDbase();
    
    feedbackField = new JTextField(4);
    feedbackField.setEditable(false);
    
    workPanel = newWorkPanel();
    
    }
      
  private JPanel newWorkPanel()
    {
    addDesktopPanel(logArea,"Mim Client Console Log");
    
    JPanel aPanel = new JPanel(new BorderLayout());

    aPanel.add(newNorthPanel(),BorderLayout.NORTH);
    aPanel.add(desktopPanel,BorderLayout.CENTER);
    
    return aPanel;
    }


  String getRunEntity ()
    {
    return (String)runEntity.getSelectedItem();
    }

  String getRunState ()
    {
    return (String)runState.getSelectedItem();
    }

  private String getRunMode ()
    {
    return (String)runMode.getSelectedItem();
    }

  public boolean isRunModeAutomatic ()
    {
    return rm_Auto.equals(getRunMode());
    }

  public boolean isRunModeOnClick ()
    {
    return rm_Onclick.equals(getRunMode());
    }

  /**
   * Add a JComponet to the desktop
   * @param provider
   */
  private void addDesktopPanel ( ComponentProvider provider, String title )
    {
    if ( provider == null ) 
      {
      stat.println(classname+".addDesktopPanel: ERROR provider==null title="+title);
      return;
      }
    
    JComponent panel = provider.getComponentToDisplay();
    JInternalFrame internal = new JInternalFrame(title);
    internal.add(panel);

    desktopPanel.add(internal);
    
    internal.pack();
    internal.setBounds(stat.winposStore.getWindowBounds(internal.getTitle()));
    internal.setVisible(true);
    internal.setIconifiable(true);
    internal.setResizable(true);
    }

  @Override
  public void saveWindowPosition (WinposStore wpstore)
    {
    for (JInternalFrame frame :  desktopPanel.getAllFrames() )
      wpstore.putWindowBounds(frame);
    }

  private JLabel newJLabel(String msg)
  {
  return new JLabel(msg);
  }
  
  private JPanel newNorthPanel ()
    {
    JPanel aPanel = new JPanel();

    aPanel.add(newJLabel("Run Mode"));
    aPanel.add(runMode);
    
    aPanel.add(newJLabel(" On Entity "));
    aPanel.add(runEntity);
    
    aPanel.add(newJLabel(" State "));
    aPanel.add(runState);
    
    aPanel.add(feedbackField);
    
    return aPanel;
    }
    
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }


  }
