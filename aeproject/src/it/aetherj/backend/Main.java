/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend;

import java.awt.EventQueue;
import java.io.*;
import java.nio.file.Files;
import java.security.Permission;

import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.SchemaManager;
import it.aetherj.backend.dbase.labels.LabelFactory;
import it.aetherj.backend.dbtbls.AedbFactory;
import it.aetherj.backend.gui.*;
import it.aetherj.backend.gui.config.*;
import it.aetherj.backend.gui.images.ImageProvider;
import it.aetherj.backend.gui.panels.GuiAebendPanel;
import it.aetherj.backend.gui.utils.*;
import it.aetherj.backend.gui.webuser.WuserManager;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.McwSSLFactory;
import it.aetherj.backend.server.*;
import it.aetherj.backend.utils.MimNonceMap;
import it.aetherj.boot.*;
import it.aetherj.boot.stampa.StampaEngine;
import it.aetherj.shared.*;

/**
 * https://getaether.net/mim-docs/ 
 * https://github.com/stleary/JSON-java
 * https://blog.jayway.com/2014/02/16/using-different-context-handlers-on-different-ports-in-jetty-9/
 * https://docs.codeberg.org
 */
public final class Main implements ConfigChangedInterface
  {
  private static final String classname="Main";
 
  private final Stat stat;

  /**
   * Careful, if any Exception is thrown here it will abort startup with a weird exception
   */
  public Main( Boot boot )   
    {
    stat = new Stat(boot); 
    stat.log.setProgressStep("m0");
    
    // THEN, This is needed to prevent libraries to call System.exit() and ZOT the server.
    System.setSecurityManager(new MainSecurityManager());
    
    // allocate basic/essential bits.
    Thread runFoundation = new Thread(new FoundationRunnable());
    runFoundation.start();
    }
      
  
	/**
   * ACK one of the issues is that SQL server may not be ready when Jrnc starts
   * so, even if params are OK the first connect FAILS.
   * In this case I HAVE to retry a few times, just a few
   */
  private boolean dbaseConnectDone ()
    {
    // do not try forever, it may go into a spin loop and crash the machine
    for (int index=3; index > 0; index--)
      {
      // Attempt one dbase connect, if successful return
      if ( stat.dbase.connect(stat.config.mainDbaseProperty) != null ) 
        return true;

      // Some message is needed
      stat.println(classname+".dbaseConnectDone: "+index+" database connect left");
      
      // If connect fails the log will display automatically
      Aeutils.sleepSec(5);
      }
      
    // I did not manage to connect after the attempts.
    return false;
    }

  /**
   * Tries the first dbase connection. It fails if the connection IS configured BUT
   * I cannot connect, it succeed if there is NO connection configured.
   * @return
   */
  private void firstDbaseConnection ()
    {
    // I initialize the var just to avoid silly null pointers
    stat.dbase = new AeDbase(stat);
        
    // if connection is OK, the we are set !
    if ( dbaseConnectDone() ) return;

    // dbase connection MAY fail
    stat.println(classname+".firstDbaseConnection: CANNOT connect to dbase, check params");

    // let me show the configuration panel to the user
    stat.config.setVisible(true);
    }

  /**
   * Called when the configuration changes.
   * WARNING: be sure that a configuration change does NOT trigger a configuration change !
   */
  public void configChangedCallback()
    {
    try
      {
      int mask = stat.config.getIntProperty(ConfigProperties.KEY_debugMask,0);
      int level = stat.config.getIntProperty(ConfigProperties.KEY_debugLevel,0);
      stat.dbg.wishMaskLevel(mask, level);
      stat.consoleMenubar.configChangedCallback();
      }
    catch ( Exception exc )
      {
      stat.println(classname+".configChangedCallback",exc);        
      }
    }
  
  private File preparedDbaseFile () throws IOException
    {
    // the actual file
    File scriptFile= new File(stat.datastoreDir,ConfigProperties.HSQL_dbfilename+".script");
    
    // the filename as wanted by hsqldb
    File risul=new File(stat.datastoreDir,ConfigProperties.HSQL_dbfilename);

    if ( scriptFile.canWrite() )
      return risul;
    
    if ( scriptFile.exists() )
      throw new IllegalArgumentException(scriptFile+" exists but it is NOT writable, ERROR");
    
    // Need to copy the skeleton Dbase from an initial file
    File sourceFile = new File(stat.datastoreDir,ConfigProperties.HSQL_dbfilename+".initial");
    
    Files.copy(sourceFile.toPath(), scriptFile.toPath());
    
    return risul;
    }
  
  /**
   * Starts the Hsqldb server, if necessary.
   * NOTE: You MUST run this one in a thread different than Swing since otherwise
   * if the server hangs it will stop swing refreshing.
   * The new startup method should allow deletion of 
   * server.properties
   * ammi.properties 
   * stor.properties
   * @throws IOException 
   */ 
  private void hsqldbStartServer (org.hsqldb.Server dbsrv) throws IOException 
    {
    stat.println("hsqldbStartServer: Starting HSQLDB");
    
    int port = stat.config.programProperty.getValueInt(ConfigProperties.KEY_hsqldbPort, 9005);

    dbsrv.setAddress(ConfigProperties.HSQL_listen);  // listen on localhost only
    dbsrv.setPort(port);
     
    dbsrv.setDaemon(true);
    dbsrv.setTrace(stat.dbg.shouldPrint(Aedbg.M_dbase,Aedbg.L_trace)); 
    dbsrv.setSilent(true); 
      
    dbsrv.setDatabaseName(0, ConfigProperties.HSQL_dbname);
    
    File path = preparedDbaseFile(); 
    stat.println("   db_path="+path);
    
    dbsrv.setDatabasePath(0, "file:"+path.getPath());
    
    dbsrv.start();

    Aeutils.sleepSec(1);

    stat.println("hsqldb: "+dbsrv.getStateDescriptor());
    stat.println("  addr "+dbsrv.getAddress());
    stat.println("  db 0 "+dbsrv.getDatabasePath(0,false));
    
    stat.println(classname+" HSQLDB Server created");
    }
  


	private void runFoundation()
		{
    // This is the first, note how it uses only log
    stat.config = new ConfigProperties(stat.datastoreDir,stat.log);
    stat.config.initialize();
    stat.log.setProgressStep("f0");
		
		// set the change listener that will push config changes.
    stat.config.setConfigChangeListener(this);
		
    // dbase uses dbg so It must be here
    stat.dbg = new Aedbg();
    int mask = stat.config.getIntProperty(ConfigProperties.KEY_debugMask,0);
    int level = stat.config.getIntProperty(ConfigProperties.KEY_debugLevel,0);
    stat.dbg.wishMaskLevel(mask,level);
    stat.log.setProgressStep("f1");
 
    // the following do not use any GUI
    stat.winposStore     = new WinposStore(stat.datastoreDir);
    stat.utils           = new Aeutils(stat);
    stat.mimSSLFactory   = new McwSSLFactory(stat);
    stat.dbProperty      = new DbProperty(stat);
    
    // this will restore possibly previously saved windows bounds to Log position
    stat.log.setBounds(stat.winposStore.getWindowBounds(Log.frameTitle));
    
    try
      {
      // attempt to run Dbase server
      stat.dbaseServer=new org.hsqldb.Server(); 
      hsqldbStartServer(stat.dbaseServer);
      stat.log.setProgressStep("f3");

      // run activities that may fail in trapped mode, crypto stuff
      stat.nonceMap   = new MimNonceMap(stat);
      stat.log.setProgressStep("f4");
      }
    catch ( Exception exc )
      {
      stat.println("runTrapped", exc);
      }

    // should be fine, hopefully
    firstDbaseConnection();
    stat.log.setProgressStep("f5");
    
    // label factory uses the dbase to do initialization
    // unfortunately it needs to be here since the GUI is created when classes are new'ed
    stat.labelFactory = new LabelFactory(stat);
    stat.labelFactory.initialize();
    stat.log.setProgressStep("f6");

    // this is in swing thread so I can create GUI bits safely  
    // but Do NOT lock things up by doing any real work, only new classes and GUI !!
    EventQueue.invokeLater(new MainGuiRunnable());
		}
	
/**
 * The problem is that I need translated labels to create the GUI
 * BUT I do NOT want to do DB access at GUI thread.
 * Therefore I need to setup the system dbase and basic pieces (non GUI ones)
 * here and this will then call the GUI init
 * NOTE: If dbase connection FAILS I disable the pieces that cannot run without dbase
 * but I leave the others on and ready (this allows you to check the parameters !!!)
 */
private final class FoundationRunnable implements Runnable
  {
  public void run()
    {
		runFoundation();
    }
  }

private final class MainGuiRunnable implements Runnable
  {
  public void run()
    {
    try
      {
      stat.fileChooser     = new GuiFileChooser(stat);
      stat.imageProvider   = new ImageProvider(stat);
      stat.stampaEngine    = new StampaEngine(stat.log);
      stat.importConfig    = new ImportConfigGui(stat);
      stat.consoleMenubar  = new ConsoleMenubar(stat);
      stat.console         = new ConsoleGui(stat); 
      stat.guiLogin        = new GuiLogin(stat);
      
      stat.mcwRunningModel = new McwRunnerTableModel();    // Before MceConsole
      stat.mcwAddressModel = new McwAddressTableModel(stat);  // Before McwConsole
      stat.mcwConsole      = new McwConsole(stat);
      stat.log.setProgressStep("g1");
      
      stat.aebendConsole   = new AebendConsole(stat);         // MUST be before GuiAebendPanel
      stat.aeParams        = new AetherParams(stat, stat.aebendConsole.getLog());
      stat.aeParamsGui     = new AetherParamsGui(stat, stat.aeParams);
      
      stat.backendPanel    = new GuiAebendPanel(stat); 
      stat.wuserManager    = new WuserManager(stat);
      
      stat.beanshell       = new BshFrame(stat);
      stat.webServer       = new WebServer(stat);
      stat.infoFrame       = new RuntimeInfoFrame(stat);
      stat.sqlquery        = new SqlqueryFrame(stat);
      stat.log.setProgressStep("g2");

      stat.dbSchemaManager  = new SchemaManager(stat);
      stat.aedbFactory      = new AedbFactory(stat, stat.mcwConsole.getLog());
      stat.log.setProgressStep("g3");
      
      // initial screen is the server, you can use the menu to change view
//      stat.console.setContent(stat.mcwConsole);

      stat.console.setContent(stat.aebendConsole);
      stat.log.setProgressStep("g4");

      // this will start the other servers, if appropriate
      stat.aebendConsole.startServer();      
      stat.log.setProgressStep("g4");

      // show the console
      stat.console.setVisible(true);      
      
      // hide the boot log
      stat.log.setVisible(false);
      }
    catch ( Exception exc )
      {
      // this is really an exception show since GUI may not be up yet.
      stat.log.println(classname,exc);
      }
    }
  }

/**
 * I need this so I can trap somebody in the added libraries that just go for a System.exit()
 * It happens for sure with hsqldb and it is something I need to control.
 * It may happen with other libraries as well, so I need to check.
 */
private final class MainSecurityManager extends SecurityManager 
  {
  @Override
  public void checkExit(int status) 
    {
    if ( status != stat.getExitCode() ) 
      {
      SecurityException exc = new SecurityException("attempted exit");
      // Use a println, NOT a gui, the gui may be broken !
      System.err.println (classname+" exception="+Log.exceptionExpand(classname,exc));
      throw exc;
      }
    }

  @Override
  public void checkPermission(Permission perm) 
    {
    // Basically I permit everything else.
    // you NEED this one otherwise the end result is a constant deny
    // I am not really sure why...
//    System.out.println("checkPermission perm="+perm);
    }

  }



  } // End of main class.




