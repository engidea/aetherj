/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.server;

import java.util.*;

/**
 * This is just a hash map since all params are indexed by name and have a value
 * I am making a separate name so I can better qualify the meaning.
 */
public class FactoryParamMap extends HashMap<String,Object>
  {
  private static final long serialVersionUID = 1L;

  private static final String classname="ParamMap.";
  
  public static final String param_type   = "param_type";
  
  /**
   * Empty constructor when you want an empty map.
   */
  public FactoryParamMap ()
    {
    }
  
  
  public String getCsvKeyValues ()
    {
    StringBuilder risul = new StringBuilder(2000);
    
    Set<String> keys = super.keySet();
    
    for ( String key : keys )
      {
      Object value = super.get(key);
      risul.append(key);
      risul.append('\t');
      risul.append(" "+value);
      risul.append('\n');
      }
    
    return risul.toString();
    }

  /**
   * If you just have one key you can use this one.
   * @param key
   * @param value
   */
  public FactoryParamMap(String key, Object value)
    {
    put (key,value);
    }
    
  /**
   * This is slightly different than the original since you can concatenate instances.
   * @param key
   * @param value
   * @return
   */
  public FactoryParamMap put ( String key, Object value )
    {
    super.put(key,value);
    
    return this;
    }
    
  public String getStringParam ( String key, String defaultVal )    
    {
    Object val = super.get(key);
    if ( val == null ) return defaultVal;
    
    if ( !(val instanceof String) ) 
      {
      System.err.println(classname+"getStringParam WARN key="+key+" object="+val.getClass());
      return defaultVal;
      }
    
    return (String)val;
    }
    
  public final Integer getIntegerParam ( String key )
    {
    Object val = super.get(key);
    if ( val == null ) return null;
    
    if ( !(val instanceof Integer) ) 
      {
      System.err.println(classname+"getIntParam WARN key="+key+" object="+val.getClass());
      return null;
      }
    
    return (Integer)val;
    }
    
  public final int getIntParam ( String key, int defaultVal )
    {
    Object val = super.get(key);
    if ( val == null ) return defaultVal;
    
    if ( !(val instanceof Integer) ) 
      {
      System.err.println(classname+"getIntParam WARN key="+key+" object="+val.getClass());
      return defaultVal;
      }
    
    Integer anInt = (Integer)val;
    
    return anInt.intValue();
    }
  
  public final boolean getBooleanParam ( String key, boolean defaultVal )
    {
    Object val = super.get(key);
    if ( val == null ) return defaultVal;
    
    if ( !(val instanceof Boolean) ) 
      {
      System.err.println(classname+"getIntParam WARN key="+key+" object="+val.getClass());
      return defaultVal;
      }
    
    Boolean aval = (Boolean)val;
    
    return aval.booleanValue();
    }
  
  
  }
