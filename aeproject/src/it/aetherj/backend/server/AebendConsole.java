/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.server;

import java.awt.BorderLayout;
import java.awt.event.*;
import java.sql.SQLException;

import javax.swing.*;

import org.hsqldb.Database;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.gui.WinposStore;
import it.aetherj.backend.gui.utils.ComponentProviderSaveWinpos;
import it.aetherj.boot.*;
import it.aetherj.shared.Aeutils;


/**
 * There is the need to have a startup server since quite a few actions must be done before
 * you can start GWT and MIM servers
 */
public final class AebendConsole implements ComponentProviderSaveWinpos
  {
  private static final String classname="AebendConsole";

  private final ButtonManager buttonManager = new ButtonManager();
  
  private final Stat stat;
  private final JPanel  workPanel;
  private final JDesktopPane desktopPanel;
  private final LimitedTextArea logArea;
  
  private AeDbase dbase;             // use this one to operate with the dbase and leave stat.dbase free !
  private JTextField feedbackField;
  private JButton pauseShutdownBtn;

  private volatile boolean pauseShutdown=false;
  
  /**
   * Constructor
   * @param stat
   */
  public AebendConsole(Stat stat)
    {
    this.stat = stat;
    this.logArea      = new LimitedTextArea(3000);
    this.desktopPanel = new JDesktopPane();
    
    pauseShutdownBtn = stat.utils.newJButton("Pause/Continue Shutdown",buttonManager);

    workPanel = newWorkPanel();
    }
  
  public PrintlnProvider getLog ()
    {
    return logArea;
    }
    
  /**
   * Add a JComponet to the desktop
   * @param provider
   */
  private void addDesktopPanel ( ComponentProvider provider, String title )
    {
    if ( provider == null ) 
      {
      stat.println(classname+"addDesktopPanel: ERROR provider==null title="+title);
      return;
      }
    
    JComponent panel = provider.getComponentToDisplay();
    JInternalFrame internal = new JInternalFrame(title);
    internal.add(panel);

    desktopPanel.add(internal);
    
    internal.pack();
    internal.setBounds(stat.winposStore.getWindowBounds(internal.getTitle()));
    internal.setVisible(true);
    internal.setIconifiable(true);
    internal.setResizable(true);
    }

  @Override
  public void saveWindowPosition (WinposStore wpstore)
    {
    for (JInternalFrame frame :  desktopPanel.getAllFrames() )
      wpstore.putWindowBounds(frame);
    }
    
  private JPanel newWorkPanel()
    {
    addDesktopPanel(logArea,"Controller Activity Log");

    JPanel aPanel = new JPanel(new BorderLayout());

    aPanel.add(newNorthPanel(),BorderLayout.NORTH);
    aPanel.add(desktopPanel,BorderLayout.CENTER);
     
    return aPanel;
    }
  
  private JPanel newNorthPanel ()
    {
    feedbackField = new JTextField(1);
    feedbackField.setEditable(false);
    
    JPanel aPanel = new JPanel();
    aPanel.add(pauseShutdownBtn);
    aPanel.add(feedbackField);
    
    return aPanel;
    }
    
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }

  /**
   * Actually start this server as a daemon
   * Must be called once and be called in SWING thread
   */
  public void startServer()
    {
    if ( ! Aeutils.SwingIsEventDispatchThread() )
      throw new IllegalStateException("NOT in a Swing thread, FIX IT");
    
    addDesktopPanel(stat.webServer,"Web Server");
    addDesktopPanel(stat.aeParamsGui,"AetherJ Parameters");
    addDesktopPanel(stat.webServer.getGwtContextLog(),"Gwt Context Log");
    addDesktopPanel(stat.webServer.getMimContextLog(),"Mim Context Log");
    addDesktopPanel(stat.webServer.getBimContextLog(),"Bim Context Log");
    
    stat.wuserManager.refreshTable();
		
    Aeutils.newThreadStart(new AebendServerRunnable(), "Aebend Server");
    }

  /**
   * Basically clean up all tables that may be left out from previous unclean shutdown.
   * You should call this one on shutdown too, just to signal a clean exit...
   */
  private void prepareDbaseForWork () throws Exception
    {
    // at this stage I should upgrade the dbase, if needed
    // the idea is that I store scripts that can be executed that will update the DB from one version to the other
    // this is really a separate job.....
    DbaseSchemaUpdater updater = new DbaseSchemaUpdater(stat,logArea);

    // this updates the schema using instructions
    String curDbversion=updater.updateSchema();

    stat.appVersion.setDbaseVersion(curDbversion);
    
    println("Database Version "+curDbversion);

    // It may happen that the scripts update the user definition, so, I need to reconnect
    stat.dbase.close();
    
    // Reconnect
    stat.dbase.connect(stat.config.mainDbaseProperty); 
    }

  
  private void waitSystemShutdown ()
    {
    int counter=0;
  
    while ( ! stat.waitHaveShutdownReq_s(1) )
      {
      feedbackField.setText(Integer.toString(counter++));

      if ( counter > 9 ) counter=0;
      }
    }
  
  
  
  private void controllerRunTry () throws Exception
    {
    // Clean up and update dbase
    prepareDbaseForWork ();

    // now open a dbase for this server
    dbase = stat.newWorkDbase();
    
    if ( ! dbase.isConnected() )
      {
      println("NO dbase available, aborting");
      return;        
      }
    
    // adjust all tables using the newly created dbase
    stat.dbSchemaManager.checkTableSchemaCorrect(dbase);
    
    if ( ! stat.dbSchemaManager.isTableSchemaCorrect() )
      {
      println("NO dbase schema correct, aborting");
      return;        
      }

    // initialize all handlers for dbase
    stat.aedbFactory.initialize();

    println("Backend Version "+stat.appVersion.getBackendVersion());
    
    // parameters loading cannot fail since if params non existem then they will be created
    stat.aeParams.initializeDone();
    
    // I can now start the rest of the system
    stat.mcwConsole.startServer();
    stat.webServer.startServer();
    }
  
  /**
   * This class is just a starter and a runner up monitor
   * As a starter it is in charger of setting up the dbase and then startup the two webservers
   */
  private void aebendControllerRun()
    {
    try
      {
      controllerRunTry();
            
      // wait for anybody to ask for shutdown
			waitSystemShutdown();

			// save current configuration
	    stat.aeParams.saveToDatastoreOnExit();
      }
    catch ( SQLException exc )
			{
			StringBuilder error = new StringBuilder(2000);
			
      error.append(Log.exceptionExpand(classname+"run()",exc));
			
			SQLException next = exc.getNextException();

			if ( next != null )
	      error.append(Log.exceptionExpand(classname+"next: ",next));
			
      logError(error.toString());
			}
    catch ( Exception exc )
      {
      String errorString = Log.exceptionExpand(classname+"run()",exc);
      logError(errorString);
      }

    serverShutdown();
    }


  /**
   * this is called after a shutdown, to wait for possible pause to look for issues
   */
  private void serverShutdownPause (int start_index, int end_index)
    {
    try
      {
      for (int index=start_index; index > end_index; )
        {
        Aeutils.sleepSec(1);

        // If shutdown is paused, just go on on the loop and wait...
        if ( pauseShutdown ) continue;

        // tell the user how things are going 
        logArea.print(index+" ");
        
        index--;
        }
      }
    catch ( Exception exc )
      {
      stat.println(classname+".serverShutdownPause",exc);
      }
    }

  /**
   * This actually request to shutdown, MUST be run in NON swing thread and MUST be only one of it running
   */
  private void serverShutdown ()
    {
    try 
      {
      println("Server Closing Down");

      // this is the standard way to stop the webserver
      stat.webServer.stopServer();
  
      // give some time to other thread to close down
      serverShutdownPause(5,3);
      
      stat.aedbFactory.aedbCache.dropTable(dbase);
      
      // close my database, I can do it now
      dbase.close();
      
      stat.println("SHUTDOWN HSQLDB");
      stat.dbaseServer.shutdownWithCatalogs(Database.CLOSEMODE_NORMAL);
      stat.println("SHUTDOWN DONE");
      
      // need to see the actual message
      serverShutdownPause(3,0);
      
      // Since there is a security manager installed you need to use the appropriate exit code.
      stat.systemExit();
      }
    catch ( Exception exc )
      {
      // This usually never happens, or at least I never seen it.
      stat.log.exceptionShow(classname+"serverShutdown",exc);
      }
  
    }
      
  /**
   * @Override
   * @param error
   */
  public void logError(String error)
    {
    try
      {
      println("logError: "+error);
      dbase.dbaseLogInsert(null,Dbkey.logall_svc_backend,null,error);
      }
    catch ( Exception exc )
      {
      logArea.println(classname+"logError: msg="+error+" exception="+exc);
      }
    }
    
  public void println(String message)
    {
    logArea.println(message);
    }

  public void println(String message, Throwable exception)
    {
    logArea.println(message,exception);
    }
    
private final class ButtonManager implements ActionListener
  {
  public void actionPerformed(ActionEvent e)
    {
    Object source = e.getSource();
    
    if ( source == pauseShutdownBtn ) 
      {
      pauseShutdown = ! pauseShutdown;
      println("pauseShutdown="+pauseShutdown);
      }
    }
  }

private final class AebendServerRunnable implements Runnable
  {
  public void run ()
    {
    aebendControllerRun();
    }
  }


  }
