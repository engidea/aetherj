package it.aetherj.backend.server;

import java.awt.BorderLayout;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.boot.*;

public class ContextLog implements ComponentProvider, PrintlnProvider
  {
  private final Stat stat;
  private final JPanel workPanel;  
  private final LimitedTextArea weblog;

  public ContextLog(Stat stat)
    { 
    this.stat = stat;

    weblog = new LimitedTextArea(2000);

    workPanel = newWorkPanel();
    }
  
  private JPanel newWorkPanel()
    {
    JPanel risul = new JPanel(new BorderLayout());
    
//    risul.add(newNorthPanel(),BorderLayout.NORTH);
    risul.add(weblog.getComponentToDisplay(),BorderLayout.CENTER);        
    
    return risul;
    }
  
  @Override
  public void println(String message)
    {
    weblog.println(message);
    }

  @Override
  public void println(String message,Throwable exc)
    {
    weblog.println(message,exc);
    }


  @Override
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    } 
  
  }
