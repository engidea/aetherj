/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.server;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.URI;

import javax.swing.*;

import org.eclipse.jetty.rewrite.handler.*;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.WebAppContext;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.config.AetherParams;
import it.aetherj.boot.*;
import it.aetherj.protocol.AetherCo;
import it.aetherj.shared.Aeutils;

/**
 * Web server now starts both the MIM and GWT containers
 */
public class WebServer implements ComponentProvider
  {
  private static final String classname="WebServer.";
  
  public static final String share_SystemStat="SystemStat";
  public static final String share_CntxLog="CntxLog";
  
  private final Stat stat;
  private final JPanel workPanel;  
  private final LimitedTextArea srvlog;
  private final ContextLog mimCntxlog,bimCntxlog;
  private final ContextLog gwtCntxlog;
  private final File webHomeDir; 
  
  private JTextField clientUrl;
  private Server webServer;

  public WebServer(Stat stat)
    { 
    this.stat = stat;

    srvlog = new LimitedTextArea(2000);
    gwtCntxlog = new ContextLog(stat);
    mimCntxlog = new ContextLog(stat);
    bimCntxlog = new ContextLog(stat);
    webHomeDir = new File(stat.boot.getProgramDir(),"websrv");

    workPanel = newWorkPanel();
    }
  
	public String getWebServerAddress ()
		{
		StringBuilder address = new StringBuilder(200);

		address.append("https://localhost");
		address.append(":"+stat.aeParams.getBackendPort());
		address.append(AetherCo.gwtwapp_context);
		
		return address.toString();
		}
	
	 private JPanel newWorkPanel()
	    {
	    JPanel risul = new JPanel(new BorderLayout());
	    
	    risul.add(newNorthPanel(),BorderLayout.NORTH);
	    risul.add(srvlog.getComponentToDisplay(),BorderLayout.CENTER);        
	    
	    return risul;
	    }

    private JPanel newNorthPanel()
      {
      JPanel risul = new JPanel();
      
      JButton startBrowser = new JButton("Start Browser");
      startBrowser.addActionListener(new StartBrowser());
      
      clientUrl = new JTextField(30);  // MUST be before super(stat)
      
      risul.add(startBrowser);
      risul.add(clientUrl);
      
      return risul;
      }
	 
	private void println(String message)
  	{
  	srvlog.println(message);
  	}

	private void println(String message,Throwable exc)
    {
    srvlog.println(message,exc);
    }


  /**
   * Actually start this server as a daemon.
   */
  public void startServer()
    {
    Thread aThread = new Thread(new StartWebServer());
    // MUST set the correct classloader since now Jetty do "some magic" with it...
    aThread.setContextClassLoader(stat.getClassLoader());
    aThread.setName("MIM Web Server");
    aThread.setDaemon(true);
    aThread.setPriority(Thread.MIN_PRIORITY);
    aThread.start();
    }

  public JComponent getComponentToDisplay()
    {
    return workPanel;      
    }
  
  public ContextLog getMimContextLog()
    {
    return mimCntxlog;
    }
  
  public ContextLog getBimContextLog()
    {
    return bimCntxlog;
    }

  public ContextLog getGwtContextLog()
    {
    return gwtCntxlog;
    }

  protected WebAppContext newWebAppFromWar (String prefix, String contextPath) throws IOException
    {
    File warFile = new File(webHomeDir,prefix+".war");
    
    if ( ! warFile.canRead() )
      return null;
    
    println("newWebAppFromWar: WAR file "+warFile);
    
    WebAppContext webapp = new WebAppContext();
    webapp.setContextPath(contextPath);
    webapp.setWar(warFile.getCanonicalPath());
    
    webapp.setDefaultsDescriptor(new File(webHomeDir,"webdefault.xml").getCanonicalPath());
    
//    WebAppClassLoader loader = new WebAppClassLoader(stat.getClassLoader(),webapp);
//    webapp.setClassLoader(loader); 
//    webapp.setParentLoaderPriority(true);
    
    return webapp;
    }
  
  /**
   * This tries to start the app from a directory, if it exists
   */
  protected WebAppContext newWebAppFromDir (String prefix, String contextPath) throws IOException
    {
    File warDir = new File(stat.boot.getProgramDir(),prefix);
    
    if ( ! warDir.exists() )
      return null;
    
    println("newWebAppFromDir: war directory "+warDir);
    
    String warDirPath = warDir.getCanonicalPath();
    
    WebAppContext webapp = new WebAppContext(warDirPath,contextPath);
    webapp.setDescriptor(warDirPath+"/WEB-INF/web.xml");
    webapp.setDefaultsDescriptor(new File(webHomeDir,"webdefault.xml").getCanonicalPath());

//    webapp.setParentLoaderPriority(true);

// this is not really needed, if you set the Thread context classloader
//    WebAppClassLoader loader = new WebAppClassLoader(stat.getClassLoader(),webapp);
//    webapp.setClassLoader(loader); 
        
    return webapp;
    }

  
  private void printHandlerInfo ( WebAppContext ctx )
    {
    println("printContextInfo");
    println("   getClassPath="+ctx.getClassPath());
    println("   getExtraClasspath="+ctx.getExtraClasspath());
    println("   getBaseResource="+ctx.getBaseResource());
    println("   getDescriptor="+ctx.getDescriptor());
    println("   getDefaultsDescriptor="+ctx.getDefaultsDescriptor());
    }
    
  private void printHandlerInfo ( RewriteHandler ctx )
    {
    println("printRewriteInfo");
    println("   toString="+ctx);
    }
  
  private void printHandlerInfo ( Handler ha )
    {
    if ( ha instanceof WebAppContext )
      printHandlerInfo((WebAppContext)ha);
    else if ( ha instanceof RewriteHandler )
      printHandlerInfo((RewriteHandler)ha);
    else
      println("printHandlerInfo: NOT supported "+ha.getClass());
    }
  
  /**
   * Note that some of the magic is time dependent, meaning that an app context is NOT fully populated when you wish
   * This is especially true for classloading
   * @param ctx
   */
  protected void printHandlersInfo (  )
    {
    Handler h = webServer.getHandler();
    
    if ( h == null ) return;
    
    if ( ! ( h instanceof HandlerCollection ) )
      {
      println("printHandlersInfo: BAD class "+h.getClass());
      return;
      }
    
    HandlerCollection hc = (HandlerCollection)h;
    
    Handler []ha = hc.getHandlers();
    
    for ( Handler hh : ha )
      printHandlerInfo(hh);
    
    }
  
  
  
  /**
   * Try to stop the server, this MUST be on a NON  swing thread
   */
  public void stopServer()
    {
    println("SHUTDOWN"); 
    
    try
      {
      // Doing a graceful stop result surely in a hangup if the server is still active.
      if ( webServer != null ) 
        webServer.stop();
      
      println("STOPPED");         
      }
    catch ( Exception exc )
      {
      stat.log.exceptionShow(classname+"stopServer()",exc);
      }
    }
  
  /**
   * If you want to add specific general log to a specific handler
  private RequestLogHandler newRequestLogHandler ()
    {
    RequestLogHandler risul = new RequestLogHandler();
    WebRequestLog requestLog = new WebRequestLog(stat,srvlog);
    risul.setRequestLog(requestLog);  
    return risul;
    }
   */
  
  protected WebAppContext newWebApp(String prefix, String contextPath, ContextLog ctxLog ) throws Exception
    {
    WebAppContext webapp = newWebAppFromWar(prefix, contextPath);

    if ( webapp == null )
      webapp = newWebAppFromDir(prefix, contextPath);
    
    if ( webapp == null )
      throw new IllegalArgumentException("NO WAR or war directory available for application");

/* Configuring servlets using Annotations requires a bunch of never ending jars
 * Crap  
    webapp.setConfigurations(new Configuration[] {
        new AnnotationConfiguration(), 
        new WebXmlConfiguration(),
        new WebInfConfiguration(),
        new PlusConfiguration(), 
        new MetaInfConfiguration(), 
        new FragmentConfiguration(),
        new FragmentConfiguration() });
*/
    
    // Give the following objects to the webapp context
    webapp.setAttribute(share_SystemStat,stat);
    webapp.setAttribute(share_CntxLog,ctxLog);

    return webapp;
    }

  private RewriteHandler newRewriteRootToAetherj ()
    {
    RewriteHandler rewrite = new RewriteHandler(); 
  
    RedirectPatternRule redirect = new RedirectPatternRule();
    redirect.setPattern("/");
    redirect.setLocation("/aetherjweb/");
    redirect.setStatusCode(301);
    redirect.setTerminating(true);
    rewrite.addRule(redirect);
  
    return rewrite;
    }
  
  
  protected void startWebServer()
    {
    println("Avvio server Web");         
    
    try
      {
      System.setProperty("jetty.home",webHomeDir.getCanonicalPath());
        
      int webPort = stat.aeParams.getBackendPort();

      if ( webPort == 0 )
        {
        webPort = stat.utils.findFreeTcpPort();
        println("  web port is 0, found a new port "+webPort);
        }
      else if ( ! stat.utils.isPortAvailable(webPort))
        {
        println("  web port "+webPort+" is not available");
        webPort = stat.utils.findFreeTcpPort();
        println("  found a new port "+webPort);
        }

      // save it to configuration, regardless of value
      stat.aeParams.setBackendPort(webPort);

      if ( webPort == 0 )
        {
        String msg="ABORT: Cannot allocate TCP port";
        println(msg);
        stat.console.showMessageDialog(msg);
        return;
        }

      println("Webserver port="+webPort);   

      // should be OK even if it is not in Swing thread..
      clientUrl.setText(getWebServerAddress());

      webServer=new Server();

      // HttpConfiguration is a collection of configuration information appropriate for http and https. The default
      // scheme for http is <code>http</code> of course, as the default for secured http is <code>https</code> but
      // we show setting the scheme to show it can be done.  The port for secured communication is also set here.
      HttpConfiguration http_config = new HttpConfiguration();
      http_config.setSecureScheme("https");
      http_config.setSecurePort(webPort);
      http_config.setOutputBufferSize(32768);
      
      // I set the standard HTTP connector to a port, it should not be used in production
/*
      ServerConnector http = new ServerConnector(webServer,new HttpConnectionFactory(http_config));        
      http.setIdleTimeout(30000);
      http.setPort(8080);
      http.setName("MimConnectorHttp");
*/
      
      // SSL requires a certificate so we configure a factory for ssl contents with information pointing to what
      // keystore the ssl connection needs to know about. Much more configuration is available the ssl context,
      // including things like choosing the particular certificate out of a keystore to be used.
      SslContextFactory sslContextFactory = new SslContextFactory.Server();
  
      sslContextFactory.setKeyStore(stat.aeParams.getKeyStore());
      sslContextFactory.setKeyStorePassword(AetherParams.keystorePass);
    
//      sslContextFactory.setKeyStorePath(jetty_home + "/etc/keystore");
//      sslContextFactory.setKeyStorePassword("OBF:1vny1zlo1x8e1vnw1vn61x8g1zlu1vn4");
//      sslContextFactory.setKeyManagerPassword("OBF:1u2u1wml1z7s1z7a1wnl1u2g");
      
      // A new HttpConfiguration object is needed for the next connector and you can pass the old one as an
      // argument to effectively clone the contents. On this HttpConfiguration object we add a
      // SecureRequestCustomizer which is how a new connector is able to resolve the https connection before
      // handing control over to the Jetty Server.
      HttpConfiguration https_config = new HttpConfiguration(http_config);
      https_config.addCustomizer(new SecureRequestCustomizer());
      
      // We create a second ServerConnector, passing in the http configuration we just made along with the
      // previously created ssl context factory. Next we set the port and a longer idle timeout.
      ServerConnector https = new ServerConnector(
          webServer,
          new SslConnectionFactory(sslContextFactory,"http/1.1"),
          new HttpConnectionFactory(https_config));

      https.setPort(webPort);
      https.setIdleTimeout(500000);
      
      // Here you see the server having multiple connectors registered with it, now requests can flow into the server
      // from both http and https urls to their respective ports and be processed accordingly by jetty. A simple
      // handler is also registered with the server so the example has something to pass requests off to.

      // Set the connectors
//      webServer.setConnectors(new Connector[] { http, https }); 

      webServer.setConnectors(new Connector[] { https });
      
      HandlerCollection collection = new HandlerCollection();
      collection.addHandler(newWebApp("gwtwar",AetherCo.gwtwapp_context,gwtCntxlog));
      collection.addHandler(newWebApp("mimwar",AetherCo.mimwapp_context,mimCntxlog));
      collection.addHandler(newWebApp("bimwar",AetherCo.bimwapp_context,bimCntxlog));

/*      
adding it works for /aetherjweb but generate exception and I do not have the "other end" to check what is happening...
 
16:34:34 java.lang.IllegalStateException: WRITER
16:34:34  at org.eclipse.jetty.server.Response.getOutputStream(Response.java:776)
16:34:34  at org.eclipse.jetty.rewrite.handler.RedirectPatternRule.apply(RedirectPatternRule.java:86)
      
            collection.addHandler(newRewriteRootToAetherj());
*/

      webServer.setHandler(collection);
      
      webServer.setRequestLog(new WebRequestLog());
            
      webServer.start ();
      
      println("Server Web avviato correttamente");   
      
      printHandlersInfo();
      
      // wait until the started thread ends
      webServer.join();
      }
    catch ( Exception exc )
      {
      println(classname+"startServer()",exc);
      }
    
    println("startWebServer: END");         
    }

private final class StartBrowser implements ActionListener, Runnable
  {
  public void actionPerformed(ActionEvent e)
    {
    Aeutils.newThreadStart(this,"Web Browser Starter");
    }
  
  public void run()
    {
    // I should jump to the correct browser page
    if ( ! Desktop.isDesktopSupported() )
      {
      println(classname+".run(): ERROR: NO DesktopSupport ! ");
      return;
      }
      
    Desktop desktop = Desktop.getDesktop();
    if ( ! desktop.isSupported(Desktop.Action.BROWSE))
      {
      println(classname+".run(): ERROR: Desktop.Action.BROWSE NOT supported ! ");
      return;
      }
    
    try
      {
      String address = getWebServerAddress();

      URI uri = new URI(address+"/");
      
      desktop.browse(uri);
      }
    catch ( Exception exc )
      {
      println(classname+"run: ",exc);
      }
    }
  }
	
private final class StartWebServer implements Runnable
  {
  public void run ()
    {
    startWebServer();
    }  
  }


private final class WebRequestLog implements RequestLog
  { 
  @Override
  public void log(Request request, Response response)
    {
    StringBuilder risul = new StringBuilder(200);
  
    risul.append("from="+request.getRemoteAddr());
    risul.append(" "+request.getMethod());
    risul.append(" "+request.getRequestURL());
    
    String query = request.getQueryString();
    if ( query != null ) risul.append(query);
    
    srvlog.println(risul.toString());
    }
  }





  


  } // END main class





