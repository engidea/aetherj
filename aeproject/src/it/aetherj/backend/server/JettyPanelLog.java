/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.server;

import org.eclipse.jetty.util.log.Logger;

import it.aetherj.backend.Stat;
import it.aetherj.boot.*; 

/**
 * This is another one that some smarty panty has gotten wrong
 * Some magic is done in a way that forbit you to bind a Logger to a webserver when the webserver is created
 * The root of the issue is the STATIC keyword and the existence of "singletons"
 * 
 * Some intern had probably worked in Java at a higher level of what his brain allowed.
 * 
 * So, no, it is NOT possible to replace the logger in Jetty, it just fails
 * Ah, yes, someone will tell me that by some even darker magic it is possible.... like chopping your hand off to fix the finger pain
 *
 */
public class JettyPanelLog implements org.eclipse.jetty.util.log.Logger
  {
  private static final String classname="JettyPanelLog";
  
  private final Stat stat;
  private final LimitedTextArea logArea;

  private boolean isDebug;
  
  public JettyPanelLog(Stat stat, LimitedTextArea logArea)
    {
    this.stat = stat;
    this.logArea = logArea;
    }

  @Override
  public String getName()
    {
    return classname;
    }

  @Override
  public void warn(String msg, Object... args)
    {
    StringBuilder risul = new StringBuilder(1000);
    risul.append("WARN");
    risul.append(msg);
    
    logArea.println(risul.toString());
    }

  @Override
  public void warn(Throwable thrown)
    {
    logArea.println(Log.exceptionExpand("WARN",thrown));
    }

  @Override
  public void warn(String msg, Throwable thrown)
    {
    logArea.println(Log.exceptionExpand(msg,thrown));
    }

  @Override
  public void info(String msg, Object... args)
    {
    StringBuilder risul = new StringBuilder(1000);
    risul.append("INFO");
    risul.append(msg);
    
    logArea.println(risul.toString());
    }

  @Override
  public void info(Throwable thrown)
    {
    logArea.println(Log.exceptionExpand("INFO",thrown));
    }

  @Override
  public void info(String msg, Throwable thrown)
    {
    logArea.println(Log.exceptionExpand(msg,thrown));
    }

  @Override
  public boolean isDebugEnabled()
    {
    return isDebug;
    }

  @Override
  public void setDebugEnabled(boolean enabled)
    {
    isDebug = enabled;
    }

  @Override
  public void debug(String msg, Object... args)
    {
    StringBuilder risul = new StringBuilder(1000);
    risul.append("DBG");
    risul.append(msg);
    
    logArea.println(risul.toString());
    }

  @Override
  public void debug(String msg, long value)
    {
    StringBuilder risul = new StringBuilder(1000);
    risul.append(msg);
    risul.append(value);
    
    logArea.println(risul.toString());
    }

  @Override
  public void debug(Throwable thrown)
    {
    logArea.println(Log.exceptionExpand("DEBUG",thrown));
    
    }

  @Override
  public void debug(String msg, Throwable thrown)
    {
    logArea.println(Log.exceptionExpand(msg,thrown));
    }

  @Override
  public Logger getLogger(String name)
    {
    if ( name == null )
      return null;
    
    if ( name.equals(classname))
      return this;
    
    return null;
    }

  @Override
  public void ignore(Throwable ignored)
    {
    }

  }
