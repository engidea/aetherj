/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.security.MessageDigest;
import java.sql.SQLException;
import java.util.*;

import it.aetherj.backend.*;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.gwtwapp.client.beans.AegwUser;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * This is a web user, it should have an associated Aether user, but who knows, so, I am having a separate table
 */
public final class AedbWebuser extends AedbOperations
  {
  private static final String classname="AedbWebuser";
  
  public static final MimEntity mimEntity = MimEntityList.webuser;
  
  public static final String cn_tbl      = "webuser_tbl";
  public static final String cn_id       = "webuser_id";
  public static final String cn_ukey_id  = "webuser_ukey_id";    // the associated key to this user, i any
  public static final String cn_login    = "webuser_login";
  public static final String cn_hashpass = "webuser_hashpass";   
  public static final String cn_note     = "webuser_note";
 
  private static final String cn_view    = "webuser_view";
  
  private AedbUkey   aedbUkey;    // needed to manage the pki relations

  
  public AedbWebuser ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    aedbUkey   = stat.aedbFactory.aedbUpki;
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }

  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 

  public Brs selectUsingPrimaryId ( AeDbase dbase, Integer row_id ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_id+"=? ";
    Dprepared prepared=dbase.getPreparedStatement(query);
    prepared.setInt(1, row_id);
    return prepared.executeQuery();
    }

  /**
   * This looks into the record and ask to ukey to retrieve the record for ukey
   * This code is quite simple since the joined column has all fields with proper prefix
   * use getWuserIdent
  public MimKey getMimKey(AeDbase dbase, Integer wuser_id) throws SQLException
    {
    Brs ars = selectUsingPrimaryId (dbase,wuser_id);
    
    MimKey risul = new MimKey();
    
    // the following works since I have a view that includes ukey_tbl
    
    if ( ars.next() )
      risul = aedbUkey.getMimMesage(ars);
    
    ars.close();
    
    return risul;
    }
   */
  
  /**
   * May return empty object if primary key is not found
   */
  public WuserIdent getWuserIdent(AeDbase dbase, Integer wuser_id) throws SQLException
    {
    Brs ars = selectUsingPrimaryId (dbase,wuser_id);
    
    WuserIdent risul=null;
    
    // the following works since I have a view that includes ukey_tbl
    
    if ( ars.next() )
      risul = new WuserIdent(ars);
    
    ars.close();
    
    if ( risul == null )
      risul = new WuserIdent();
    
    return risul;
    }

  
  /**
   * Check that the user/pw are actually ok
   * @return the wana_id associated with this user/pw
   */
  public Integer isGoodUsernamePassword (AeDbase dbase, String username, String password )
    {
    String query = "SELECT * FROM "+cn_tbl+" WHERE "+cn_login+"=?";

    byte [] aeusr_hashpass = null;
    Integer aeusr_id = null;

    try
      {
      Dprepared prepared = dbase.getPreparedStatement(query);
      prepared.setString(1,username);
      Brs ars = prepared.executeQuery();
      if ( ars.next() ) 
        {
        aeusr_id = ars.getInteger(cn_id);
        aeusr_hashpass = ars.getBytes(cn_hashpass);
        }
      ars.close();
      prepared.close();
      }
    catch ( Exception exc )
      {
      println(classname+"isGoodUsernamePassword",exc);
      return null;
      }

    // basically user not found
    if ( aeusr_id == null ) return null;
    
    // there is a special case when the user id is 1 and the password is blank
    if ( aeusr_id.intValue() == 1 && aeusr_hashpass == null ) return aeusr_id;

    if ( ! isGoodHash(password, aeusr_hashpass )) 
      {
      println(classname+"isGoodUsernamePassword: Bad password for user="+username);
      return null;
      }
      
    return aeusr_id;
    }
   
  /**
   * This MUST be the same algorithm used in the WEB
   * @param clearPass
   * @return
   */
  private byte[] getDigestedPass ( String clearPass )
    {
    try
      {
      byte [] bytePass = clearPass.getBytes(Const.utf16leCharset);
      MessageDigest hasher  = MessageDigest.getInstance("SHA1");
      return hasher.digest(bytePass);
      }
    catch (Exception exc)
      {
      println(classname+"getDigestedPass",exc);
      return new byte[0];
      }
    }

  private boolean isGoodHash ( String clearPass, byte [] wantByteHash )
    {
    if ( wantByteHash == null ) 
      {
      println(classname+"isGoodHash: NULL wantByteHash");
      return false;
      }
    
    byte [] haveByteHash = getDigestedPass(clearPass);
    
    return Arrays.equals(wantByteHash, haveByteHash);
    }
    
  /**
   * There has to be a login and a valid password
   * @param dbase
   * @param login
   * @param password
   * @return
   * @throws SQLException
   */
  private Integer insertUser(AeDbase dbase, AegwUser wuser ) throws SQLException
    {
    if ( ! wuser.hasLogin() )
      throw new IllegalArgumentException("Login is null or shorter than 4 chars");

    if ( ! wuser.hasPassword() )
      throw new IllegalArgumentException("password is null or shorter than 8 chars");
    
    String query = "INSERT INTO  "+cn_tbl+" ( webuser_login,webuser_hashpass ) VALUES ( ?,? ) ";
    
    byte []hash = getDigestedPass(wuser.wuser_password);
    
    Dprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setString(insIndex++,wuser.wuser_login);
    prepared.setBinary(insIndex++,hash);

    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
    
  private Integer updateUser(AeDbase dbase, AegwUser wuser ) throws SQLException
    {
    byte []hash=null;
    
    if ( wuser.hasPassword() )
      hash = getDigestedPass(wuser.wuser_password);

    StringBuilder query = new StringBuilder(500);
    query.append("UPDATE "+cn_tbl+" SET ");
    query.append(cn_login+"=? ");
    query.append(hash == null ? "" : ", "+cn_hashpass+"=? ");
    query.append("WHERE "+cn_id+"=?");
    
    Dprepared prepared = dbase.getPreparedStatement(query.toString());
    int insIndex=1;
    prepared.setString(insIndex++,wuser.wuser_login);
    if ( hash != null ) prepared.setBinary(insIndex++,hash);
    prepared.setInt(insIndex++, wuser.wuser_id);

    int a = prepared.executeUpdate();
    
    prepared.close();

    return wuser.wuser_id;
    }

  /**
   * Return the ID of the row inserted or the ID of the row that is already present
   * @throws SQLException 
   */
  public Integer dbaseSave ( AeDbase dbase, AegwUser wuser ) throws SQLException
    {
    if ( wuser == null )
      return null;
    
    if ( wuser.wuser_id == null )
      return insertUser(dbase, wuser );

    Brs brs = selectUsingPrimaryId(dbase, wuser.wuser_id);
    boolean have_row = brs.next();
    brs.close();
    
    if ( have_row )
      return updateUser(dbase, wuser);
    else
      return insertUser(dbase, wuser);
    }
  
  /**
   * Ok, in theory, the ukey is already set, so, this has just to add the private key and set the relationship
   * A check should be done that we are actually setting the same public key
   */
  public int dbaseUpdate ( AeDbase dbase, Integer wuser_id, Integer ukey_id, MimPublicKey pubKey, MimPrivateKey privKey ) throws SQLException
    {
    //Integer ukey_id = aedbUkey.dbaseSave(dbase, pubKey, privKey );
    if ( aedbUkey.dbaseUpdate(dbase, ukey_id, pubKey, privKey) != 1 )
      throw new IllegalArgumentException("impossible: ukey_id="+ukey_id+"does not match public key"+pubKey.toString());
    
    String query="UPDATE "+cn_tbl+" SET "+cn_ukey_id+"=? WHERE "+cn_id+"=?";
    
    Dprepared prepared = dbase.getPreparedStatement(query.toString());
    int insIndex=1;
    prepared.setInt(insIndex++,ukey_id);
    prepared.setInt(insIndex++, wuser_id);

    int a = prepared.executeUpdate();
    
    prepared.close();

    return a;
    }
  
  
  
  public ArrayList<AegwUser>selectAll ( AeDbase dbase )
    {
    ArrayList<AegwUser>risul = new ArrayList<AegwUser>();
    
    String query = "SELECT * FROM "+cn_view+" ORDER BY "+cn_login;
    
    Brs ars = dbase.select(query);
    
    while ( ars.next() )
      risul.add(newUser(ars));
    
    return risul;
    }
  
  
  public AegwUser newUser( Brs rs )
    {
    AegwUser risul = new AegwUser();
    
    risul.wuser_id=rs.getInteger(cn_id);
    risul.wuser_login=rs.getString(cn_login);
    
    return risul;
    }
    
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowInteger(cn_ukey_id));           // it may be null if not yet attached          
    addSchemaRow(new SchemaRowVarchar(cn_login,30));  
    addSchemaRow(new SchemaRowVarbinary(cn_hashpass,100));
    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }

  @Override
  public boolean createTable (ExecuteProvider db ) 
    {
    super.createTable(db);
      
    return db.executeDbaseDone("INSERT INTO "+cn_tbl+" (webuser_login) VALUES ('admin')");
    }

  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    // login names must be unique
    String query="CREATE UNIQUE INDEX webuser_login_index ON "+cn_tbl+" ( "+cn_login+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX webuser_login_index");
    }  

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+cn_view+" AS "+ 
      " SELECT webuser_tbl.*,"+
      AedbUkey.cn_tbl+".* "+
      " FROM "+cn_tbl+
      " LEFT OUTER JOIN "+AedbUkey.cn_tbl+" ON "+cn_ukey_id+"="+AedbUkey.cn_id
      );
    }
  }

/**
 * Basic user identity returned to callers to operate
 */
public static final class WuserIdent
  {
  public final Integer wuser_id;
  public final Integer ukey_id;
  public final MimPublicKey ukey_public;
  public final MimPrivateKey ukey_private;
  public final MimFingerprint ukey_fingerprint;
  
  public WuserIdent ()
    {
    wuser_id=null;
    ukey_id=null;
    ukey_public=null;
    ukey_private=null;
    ukey_fingerprint=null;
    }
  
  public WuserIdent(Brs ars)
    {
    wuser_id = ars.getInteger(cn_id);
    ukey_id  = ars.getInteger(AedbUkey.cn_id);
    ukey_public = new MimPublicKey(ars.getBytes(AedbUkey.cn_ukey_public));
    ukey_private = new MimPrivateKey(ars.getBytes(AedbUkey.cn_ukey_private));
    ukey_fingerprint = new MimFingerprint(ars.getBytes(AedbUkey.cn_fingerprint));
    }
  
  /**
   * This is protected from null values and does something reasonable
   * @param other
   * @return
   */
  public boolean isFingerprintEqual ( MimFingerprint other )
    {
    if ( ukey_fingerprint == null || other == null ) return false;
    
    return ukey_fingerprint.equals(other);
    }
  
  }

  } 

