/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;
import java.util.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * Some object in the system needs to have a couple of private and public key, for whatever reason
 * So, it refers to this table with a specific ID to refer to it
 * Since it is used to identify somebody, there should be just ONE dbepki for each object....
 * Or, if you want two of them... well, have to references to it
 */
public final class AedbThread extends AedbMimOperations <MimThread>
  {
  private static final String classname="AedbThread";
  
  public static final MimEntity mimEntity = MimEntityList.aethread;
  
  public static final String cn_tbl         = "aethread_tbl";
  public static final String cn_id          = "aethread_id";
  
  public static final String cn_fingerprint = "aethread_fingerprint";
  public static final String cn_creation    = "aethread_creation";
  public static final String cn_pow         = "aethread_pow";
  public static final String cn_signature   = "aethread_signature";
  
  public static final String cn_board_id    = "aethread_board_id";   // this thread refers to this board
  public static final String cn_name        = "aethread_name";
  public static final String cn_link        = "aethread_link";
  public static final String cn_body        = "aethread_body";
  public static final String cn_entity_v    = "aethread_entity_v";
  public static final String cn_owner_id    = "aethread_owner_id";   // this thread is owned by this user
  public static final String cn_note        = "aethread_note";
  
  public static final String cn_updla_time  = "aethread_updla_time";
  public static final String cn_updla_pow   = "aethread_updla_pow";
  public static final String cn_updla_sign  = "aethread_updla_sign";
  
  public static final String cn_post_cnt    = "aethread_post_cnt";    // how many posts it has, adjusted by the GC
  public static final String cn_votes_cnt   = "aethread_votes_cnt";   // how many votes, positive + negatives
 
  public static final String cn_view        = "aethread_view";
  
  private AedbUkey    aedbUkey;     // needed to manage the pki relations
  private AedbBoard   aedbBoard;    // needed to manage the board relations
  private AedbWanted  aedbWanted;   // needed to add wanted info on users
  
  public AedbThread ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    aedbUkey   = stat.aedbFactory.aedbUpki;
    aedbBoard  = stat.aedbFactory.aedbBoard;
    aedbWanted = stat.aedbFactory.aedbWanted;
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }

  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }

  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 

  @Override
  public Integer getPrimaryId (AeDbase dbase, MimFingerprint srch_val ) throws SQLException
    {
    String query="SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_fingerprint+"=?";
    MimDprepared prepared=dbase.getPreparedStatement(query);
    prepared.setValue(1, srch_val);
    Brs ars = prepared.executeQuery();
    
    Integer risul = null;
    
    if ( ars.next() ) 
      risul=ars.getInteger(1);
    
    ars.close();
    
    return risul;
    }

  @Override
  public Iterator<MimThread> selectForCache ( AeDbase dbase ) throws SQLException
    {
    String query = "SELECT * "+
    " FROM "+cn_view+
    " WHERE "+cn_owner_id+" IS NOT NULL "+
    " ORDER BY lastu";

    Dprepared prepared=dbase.getPreparedStatement(query);
    
    return new MimThreadIterator(prepared.executeQuery());
    }
  
  @Override
  public Iterator<MimThread> selectForServlet ( AeDbase dbase, MimFilterTimestamp filter, int limit ) throws SQLException
    {
    MimTimestamp start = filter.getStart();
    MimTimestamp end = filter.getEnd();
    
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT * ");
    query.append(" FROM "+cn_view );
    query.append(" WHERE "+cn_owner_id+" IS NOT NULL ");
    if ( ! start.isNull() ) query.append(" AND lastu >= ? ");
    if ( ! end.isNull() )  query.append(" AND lastu <= ? ");
    query.append(" ORDER BY lastu");
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    if ( ! start.isNull() ) prepared.setValue(insIndex++, start);
    if ( ! end.isNull() ) prepared.setValue(insIndex++, end);
    prepared.setInt(insIndex++, limit);

    return new MimThreadIterator(prepared.executeQuery());
    }

  @Override
  public Iterator<MimThread> selectForServlet ( AeDbase dbase, MimFilterFingerprint filter, int limit ) throws SQLException
    {
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT * ");
    query.append(" FROM "+cn_view );
    query.append(" WHERE "+cn_owner_id+" IS NOT NULL ");
    query.append(" AND "+cn_fingerprint+" IN ");
    query.append(newSqlInlist(limit));
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    insIndex = addSqlInvalues(prepared, insIndex, filter, limit);
    prepared.setInt(insIndex++, limit);

    return new MimThreadIterator(prepared.executeQuery());
    }

  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj instanceof MimThread )
      return dbaseSave(dbase,(MimThread)obj);
    else if ( obj == null )
      throw new IllegalArgumentException(classname+"dbaseSave: obj is null");
    else
      throw new IllegalArgumentException(classname+"dbaseSave: unsupported class "+obj.getClass());
    }
  
  public Integer dbaseSave ( AeDbase dbase, MimThread row) throws SQLException
    {
    Integer prim_id=getPrimaryId(dbase, row.fingerprint);
    
    if ( prim_id == null )
      prim_id=dbaseInsert(dbase, row);
    else 
      dbaseUpdate(dbase, prim_id, row);
    
    // make sure that I do not want this anymore
    aedbWanted.dbaseDelete(dbase, mimEntity, row.fingerprint);

    return prim_id;
    }

  /**
   * Inserts quite a few fields since the object might come in separate steps, due to Aether logic on object insert
   */
  public Integer dbaseUpdate ( AeDbase dbase, Integer prim_id, MimThread row) throws SQLException
    {
    if ( ! isMimRowAfterDbaseRow (dbase, prim_id, row))
      return prim_id;
    
    Integer owner_id = aedbUkey.getPrimaryId(dbase, row.owner);
    
    if ( owner_id == null )
      owner_id = aedbUkey.dbaseInsert(dbase, row.owner, row.owner_publickey);
    
    Integer board_id = aedbBoard.getPrimaryId(dbase, row.board); 
    
    if ( board_id == null )
      board_id = aedbBoard.dbaseInsert(dbase, row.board );

    String query="UPDATE "+cn_tbl+" SET "+
        cn_creation+"=?,"+
        cn_signature+"=?,"+
        cn_pow+"=?,"+
        cn_board_id+"=?,"+
        cn_name+"=?,"+
        cn_link+"=?,"+
        cn_body+"=?,"+
        cn_entity_v+"=?,"+
        cn_owner_id+"=?,"+
        cn_updla_time+"=?,"+
        cn_updla_pow+"=?,"+
        cn_updla_sign+"=?,"+
        cn_note+"=?"+
    " WHERE "+cn_id+"=?";
    
    MimDprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.signature);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setInt(insIndex++,board_id);
    prepared.setString(insIndex++,row.name);
    prepared.setString(insIndex++,row.link);
    prepared.setString(insIndex++,row.body);
    prepared.setInt(insIndex++,row.entity_version);
    prepared.setInt(insIndex++,owner_id);
    prepared.setValue(insIndex++,row.last_update);
    prepared.setValue(insIndex++,row.update_proof_of_work);
    prepared.setValue(insIndex++,row.update_signature);
    prepared.setString(insIndex++,null);
    prepared.setInt(insIndex++,prim_id);
    
    prepared.executeUpdate();
    prepared.close();
    
    return prim_id;
    }

  public Integer dbaseInsert ( AeDbase dbase, MimFingerprint fingerprint ) throws SQLException 
    {
    String query = "INSERT INTO "+cn_tbl+" ( "+cn_fingerprint+" ) VALUES ( ? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    prepared.setValue(1,fingerprint);
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  

    prepared.close();

    aedbWanted.dbaseInsert(dbase, MimEntityList.aethread, fingerprint);

    return primary_id;
    }

  
  public Integer dbaseInsert ( AeDbase dbase, MimThread row) throws SQLException 
    {
    Integer owner_id = aedbUkey.getPrimaryId(dbase, row.owner);
    
    if ( owner_id == null )
      owner_id = aedbUkey.dbaseInsert(dbase, row.owner, row.owner_publickey);
    
    Integer board_id = aedbBoard.getPrimaryId(dbase, row.board); 
    
    if ( board_id == null )
      board_id = aedbBoard.dbaseInsert(dbase, row.board );

    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_creation+","+
       cn_signature+","+
       cn_pow+","+
       cn_board_id+","+
       cn_name+","+
       cn_link+","+
       cn_body+","+
       cn_entity_v+","+
       cn_owner_id+","+
       cn_updla_time+","+
       cn_updla_pow+","+
       cn_updla_sign+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.fingerprint);
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.signature);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setInt(insIndex++,board_id);
    prepared.setString(insIndex++,row.name);
    prepared.setString(insIndex++,row.link);
    prepared.setString(insIndex++,row.body);
    prepared.setInt(insIndex++,row.entity_version);
    prepared.setInt(insIndex++,owner_id);
    prepared.setValue(insIndex++,row.last_update);
    prepared.setValue(insIndex++,row.update_proof_of_work);
    prepared.setValue(insIndex++,row.update_signature);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
  
  /**
   * This is used to present data to web, so, it is appropriate to present in descending order of update
   */
  public Brs selectUsingBoard ( AeDbase dbase, int w_board_id ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_board_id+"=? ORDER BY "+cn_lastu+" DESC ";
    Dprepared prepared = dbase.getPreparedStatement(query);
    prepared.setInt(1,w_board_id);
    return prepared.executeQuery();
    }
  
  public Brs selectUsingPrimaryId ( AeDbase dbase, Integer row_id ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_id+"=? ";
    Dprepared prepared=dbase.getPreparedStatement(query);
    prepared.setInt(1, row_id);
    return prepared.executeQuery();
    }

  
  
  /**
   * It may return an empty Thread but not a null one
   */
  public MimThread getMimMesage (AeDbase dbase, Integer row_id) throws SQLException
    {
    Brs ars = selectUsingPrimaryId (dbase,row_id);
    
    MimThread risul = new MimThread();
    
    if ( ars.next() )
      risul = getMimMesage(ars);
    
    ars.close();
    
    return risul;
    }

  @Override
  public MimThread getMimMesage ( Brs ars )
    {
    MimThread risul = new MimThread();
    
    risul.fingerprint = new MimFingerprint(ars.getBytes(cn_fingerprint));
    risul.creation    = new MimTimestamp(ars.getTimestamp(cn_creation));
    risul.proof_of_work = new MimPowValue(ars.getString(cn_pow));
    risul.signature   = new MimSignature(ars.getBytes(cn_signature));
    
    risul.owner = new MimFingerprint(ars.getBytes(AedbUkey.cn_fingerprint));
    risul.owner_publickey = new MimPublicKey(ars.getBytes(AedbUkey.cn_ukey_public));

    risul.board = new MimFingerprint(ars.getBytes(AedbBoard.cn_fingerprint));

    risul.name = ars.getString(cn_name);
    risul.body = ars.getString(cn_body);
    risul.link = ars.getString(cn_link);
    risul.entity_version = ars.getInteger(cn_entity_v, MimThread.def_entity_v);
    
    risul.last_update = new MimTimestamp(ars.getTimestamp(cn_updla_time));
    risul.update_proof_of_work = new MimPowValue(ars.getString(cn_updla_pow));
    risul.update_signature = new MimSignature(ars.getBytes(cn_updla_sign));

    return risul;
    }
  
private class MimThreadIterator implements Iterator<MimThread> 
  { 
  private final Brs ars;
  private boolean haveNext=false;
  
  MimThreadIterator (Brs ars) 
    { 
    this.ars = ars; 
    } 
    
  public boolean hasNext() 
    {
    if ( ars.isClosed()) return false;
    
    // we already know we have a next, you need to read it using next()
    if ( haveNext ) return true;
    
    // do we have a next ?
    haveNext = ars.next();
    
    if ( ! haveNext )
      ars.close();
    
    return haveNext;
    } 
    
  @Override
  public MimThread next() 
    {
    if ( ! haveNext )
      throw new NoSuchElementException("You need to call hasNext() first");
    
    haveNext = false;
    
    return getMimMesage(ars);    
    }
  } 
   
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addProvableHeader(cn_id, cn_fingerprint, cn_creation, cn_pow, cn_signature);

    addSchemaRow(new SchemaRowInteger(cn_board_id));  // may not have it
    
    addSchemaRow(new SchemaRowVarchar(cn_name,MimThread.name_len_max));  
    addSchemaRow(new SchemaRowVarchar(cn_link,MimThread.link_len_max));
    addSchemaRow(new SchemaRowVarchar(cn_body,MimThread.body_len_max));  // this could be a CLOB
    addSchemaRow(new SchemaRowInteger(cn_entity_v));
    addSchemaRow(new SchemaRowInteger(cn_owner_id));                     // may not have it...

    addSchemaRow(new SchemaRowTimestamp(cn_updla_time));
    addSchemaRow(new SchemaRowVarchar(cn_updla_pow,MimPowValue.pow_ascii_len));
    addSchemaRow(new SchemaRowVarbinary(cn_updla_sign,MimSignature.signature_len));

    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    
    addSchemaRow(new SchemaRowInteger(cn_post_cnt));
    addSchemaRow(new SchemaRowInteger(cn_votes_cnt));
    }


  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+cn_view+" AS "+ 
        " SELECT aethread_tbl.*,ukey_public,ukey_fingerprint,"+
        AedbBoard.cn_fingerprint+","+
        AedbBoard.cn_name+","+
        " COALESCE("+cn_updla_time+","+cn_creation+") AS "+cn_lastu+
        " FROM "+cn_tbl+
        " LEFT OUTER JOIN "+AedbBoard.cn_tbl+" ON "+cn_board_id+"="+AedbBoard.cn_id+
        " LEFT OUTER JOIN "+AedbUkey.cn_tbl+" ON "+cn_owner_id+"="+AedbUkey.cn_id
        );
    }
  
  @Override
  public void dropConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT aethread_owner_ref");
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT aethread_board_ref");
    }
    
  
  @Override
  public void createConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT aethread_owner_ref "+
       "FOREIGN KEY ("+cn_owner_id+") REFERENCES "+AedbUkey.cn_tbl+" ("+AedbUkey.cn_id+") ON DELETE CASCADE ");

    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT aethread_board_ref "+
        "FOREIGN KEY ("+cn_board_id+") REFERENCES "+AedbBoard.cn_tbl+" ("+AedbBoard.cn_id+") ON DELETE CASCADE ");

    }

  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX thread_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX thread_fingerprint_index");
    }  
  
  }
      
      
  } 

