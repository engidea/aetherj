/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.backend.mimclient.McWishFingerprint;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;

/**
 * Hold all the MimFIngerprint that are missing from system and I wish
 */
public final class AedbWanted extends AedbOperations
  {
  private static final String classname="AedbWanted";
  
  public static final MimEntity mimEntity = MimEntityList.aewanted;
  
  public static final String cn_tbl      = "aewant_tbl";
  public static final String cn_id       = "aewant_id";
  public static final String cn_entity   = "aewant_entity";       // this is what, address, board, keys....
  public static final String cn_fprint   = "aewant_fprint";       // the fingerprint I wish from the entity
  public static final String cn_note     = "aewant_note";
 
  private static final String cn_view    = "aewant_view";
  
  public AedbWanted ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }

  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 

  public McWishFingerprint getFingerprints(AeDbase dbase, MimEntity entity) throws SQLException
    {
    McWishFingerprint risul = new McWishFingerprint(entity);
    
    String query = "SELECT "+cn_fprint+" FROM "+cn_tbl+" WHERE "+cn_entity+"=? ";
    
    MimDprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setString(insIndex++,entity.getValue());
     
    Brs ars = prepared.executeQuery();
    
    while (ars.next())
      risul.add(new MimFingerprint(ars.getBytes(cn_fprint)));
    
    ars.close();
    
    return risul;
    }
  
  private Integer getPrimaryId ( AeDbase dbase, MimEntity entity, MimFingerprint fprint) throws SQLException
    {
    String query = "SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_entity+"=? AND "+cn_fprint+"=?";
      
    MimDprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setString(insIndex++,entity.getValue());
    prepared.setValue(insIndex++,fprint);
     
    Integer primary_id=null;
    
    Brs ars = prepared.executeQuery();
    if ( ars.next() )
      primary_id=ars.getInteger(1);
    
    ars.close();
     
    return primary_id;
    }

  public Integer dbaseInsert(AeDbase dbase, MimEntity entity, MimFingerprint fprint) throws SQLException 
    {
    Integer primary_id = getPrimaryId(dbase, entity, fprint);
    
    if ( primary_id != null )
      return primary_id;
    
    String query = "INSERT INTO "+cn_tbl+" ( "+
        cn_entity+","+
        cn_fprint+","+
        cn_note+
        " ) VALUES ( ?,?,? )";
      
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setString(insIndex++,entity.getValue());
    prepared.setValue(insIndex++,fprint);
    prepared.setString(insIndex++, null);
     
    Brs ars = prepared.executeUpdateReturnKeys();

    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
     
    ars.close();  
    prepared.close();
    return primary_id;
    }
  
  
  /**
   * Needed before regenerating the cache, delete all cache of the given entity
   * @param dbase
   * @param entity
   * @return the number of rows deleted
   * @throws SQLException 
   */
  public int dbaseDelete (AeDbase dbase, MimEntity entity ) throws SQLException
    {
    String query = "DELETE FROM "+cn_tbl+" WHERE "+cn_entity+"=?";
    
    MimDprepared prepared=dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setString(insIndex++, entity.getValue());

    int risul = prepared.executeUpdate();
    prepared.close();
    
    return risul;
    }
    
  public int dbaseDelete (AeDbase dbase, MimEntity entity, MimFingerprint fprint ) throws SQLException
    {
    String query = "DELETE FROM "+cn_tbl+" WHERE "+cn_entity+"=? AND "+cn_fprint+"=?";
    
    MimDprepared prepared=dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setString(insIndex++, entity.getValue());
    prepared.setValue(insIndex++, fprint);

    int risul = prepared.executeUpdate();
    prepared.close();
    
    return risul;
    }
    
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowVarchar(cn_entity,MimEntity.entity_len_max)).setNotNullable();  
    addSchemaRow(new SchemaRowVarbinary(cn_fprint,MimFingerprint.bytes_len));
    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }
  
  
  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+cn_view+" AS "+ 
      "SELECT * FROM "+cn_tbl);
    }
    
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX aewant_entity_index ON "+cn_tbl+" ( "+cn_entity+","+cn_fprint+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX aewant_entity_index");
    }  
  
  /*
  @Override
  public void dropConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE aewant_tbl    DROP CONSTRAINT aewant_node_ref");
    }
  
  @Override
  public void createConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE aewant_tbl ADD CONSTRAINT aewant_node_ref FOREIGN KEY (aewant_node_id ) REFERENCES aenode_tbl (aenode_id) ");
    }
  */
  }
      
      
  } 

