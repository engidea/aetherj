/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * Some object in the system needs to have a couple of private and public key, for whatever reason
 * It also happens that users in Aether are identified by a public key
 * Since it is used to identify somebody, there should be just ONE usrpki for each object....
 * Or, if you want two of them... well, have to references to it
 * 
 * to show a hex form of raw value use hsql rawtohex
 * the opposite is hextoraw
 */
public final class AedbUkey extends AedbMimOperations<MimKey>
  {
  private static final String classname="AedbUkey";
  
  public static final MimEntity mimEntity = MimEntityList.aekey;
  
  public static final String cn_tbl          = "ukey_tbl";
  public static final String cn_id           = "ukey_id";
  public static final String cn_fingerprint  = "ukey_fingerprint";   // Mim primary key
  public static final String cn_creation     = "ukey_creation";
  public static final String cn_pow          = "ukey_pow";

  public static final String cn_type         = "ukey_type";          // 1)
  public static final String cn_ukey_public  = "ukey_public";        // 2) the public part of a key pair
  public static final String cn_expire       = "ukey_expire";        // 3) 
  public static final String cn_user_name    = "ukey_user_name";     // 4) 
  public static final String cn_user_info    = "ukey_user_info";     // 5)
  public static final String cn_signature    = "ukey_signature";     // 6) 

  public static final String cn_updla_time   = "ukey_updla_time";    // 7) last update from MIM system
  public static final String cn_updla_pow    = "ukey_updla_pow";     // 8)
  public static final String cn_updla_sign   = "ukey_updla_sign";    // 9)
  
  public static final String cn_updlo_time   = "ukey_updlo_time";    // 10) last update from local update
  public static final String cn_lastu_time   = "ukey_lastu_time";    // 11) last usage of this "user" (last referenced)
  
  public static final String cn_ukey_private  = "ukey_private";       // the private part of a pair, if given
  public static final String cn_ukey_X509cert = "ukey_X509cert";      // the certificate that has the above public key, if any
  public static final String cn_note          = "ukey_note";
 
  private static final String cn_view         = "ukey_view";


  private AedbWanted aedbWanted;   // needed to add wanted info on users
  
  public AedbUkey ( Stat stat, PrintlnProvider println )
    {
    super(stat,println,mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    aedbWanted = stat.aedbFactory.aedbWanted;
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }

  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }
  
  public Brs select (AeDbase dbase) throws SQLException
    {
    String query = "SELECT * "+
        " FROM "+cn_view+
        " WHERE "+cn_signature+" IS NOT NULL "+
        " ORDER BY lastu"; 
     
    Dprepared prepared=dbase.getPreparedStatement(query.toString());
    
    return prepared.executeQuery();
    }
  
  public Iterator<MimKey> selectForCache ( AeDbase dbase ) throws SQLException
    {
    return new MimKeyIterator(select(dbase));
    }
  
  @Override
  public Iterator<MimKey> selectForServlet ( AeDbase dbase, MimFilterTimestamp filter, int limit ) throws SQLException
    {
    MimTimestamp start = filter.getStart();
    MimTimestamp end = filter.getEnd();
    
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT "+cn_view+".* ");
    query.append(" FROM "+cn_view );
    
    query.append(" WHERE "+cn_signature+" IS NOT NULL ");
    if ( ! start.isNull() ) query.append(" AND lastu >= ? ");
    if ( ! end.isNull() )  query.append(" AND lastu <= ? ");
    query.append(" ORDER BY lastu");
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    
    if ( ! start.isNull() ) prepared.setValue(insIndex++, start);
    if ( ! end.isNull() ) prepared.setValue(insIndex++, end);
    prepared.setInt(insIndex++, limit);

    return new MimKeyIterator(prepared.executeQuery());
    }
  
  @Override
  public Iterator<MimKey> selectForServlet ( AeDbase dbase, MimFilterFingerprint filter, int limit ) throws SQLException
    {
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT * ");
    query.append(" FROM "+cn_view );
    query.append(" WHERE "+cn_signature+" IS NOT NULL ");
    query.append(" AND "+cn_fingerprint+" IN ");
    query.append(newSqlInlist(limit));
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    insIndex = addSqlInvalues(prepared, insIndex, filter, limit);
    prepared.setInt(insIndex++, limit);

    return new MimKeyIterator(prepared.executeQuery());
    }

  @Override
  public Brs selectUsingPrimaryId ( AeDbase dbase, Integer row_id ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_id+"=?";
    Dprepared prepared = dbase.getPreparedStatement(query);
    prepared.setInt(1,row_id);
    return prepared.executeQuery();
    }

  public MimFingerprint getFingerprint ( AeDbase dbase, Integer row_id ) throws SQLException
    {
    Brs ars = selectUsingPrimaryId(dbase, row_id);
    
    MimFingerprint risul;
    
    if ( ars.next() )
      risul = new MimFingerprint(ars.getBytes(cn_fingerprint));
    else
      risul = new MimFingerprint();
    
    ars.close();
    
    return risul;
    }
  
  @Override
  public Integer getPrimaryId (AeDbase dbase, MimFingerprint srch_val ) throws SQLException
    {
    String query="SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_fingerprint+"=?";
    MimDprepared prepared=dbase.getPreparedStatement(query);
    prepared.setValue(1, srch_val);
    Brs ars = prepared.executeQuery();
    
    Integer risul = null;
    
    if ( ars.next() ) 
      risul=ars.getInteger(1);
    
    ars.close();
    
    return risul;
    }
  
  
  private BigInteger getMax ( AeDbase dbase )
    {
    String query = "SELECT MAX("+cn_id+") FROM "+cn_tbl;
    
    Brs ars = dbase.select(query);
    
    long risul=1;
    
    if ( ars.next() )
      risul = ars.getInteger(1,1);
    
    ars.close();
    
    return BigInteger.valueOf(risul * 10);
    }
    
  /**
   * It happens that I wish to save a Ukey while work in progress on create a new user
   * This is since POW is done at web client BUT fingerprint depends on POW 
   * So, if fingerprint is null I create a fake fingerprint using the (max) of the primary column so 
   * it is "unique" and at the same time null since it is too short
   */
  private Integer dbaseSave ( AeDbase dbase, MimKey row) throws SQLException
    {
    if ( row.fingerprint == null || row.fingerprint.isNull() )
      {
      BigInteger anumber = getMax(dbase);
      // this will create an INVALID fingerprint that is not null...
      row.fingerprint = new MimFingerprint(anumber.toByteArray());
      }
    
    Integer row_id = getPrimaryId(dbase, row.fingerprint);
    
    if ( row_id != null )
      dbaseUpdate(dbase,row_id,row);
    else
      row_id=dbaseInsert(dbase,row);
    
    // make sure that I do not want this anymore
    aedbWanted.dbaseDelete(dbase, mimEntity, row.fingerprint);
    
    return row_id;
    }
  
  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj instanceof MimKey  )
      return dbaseSave(dbase, (MimKey)obj);

    if ( obj == null )
      throw new IllegalArgumentException("dbaseSave: obj is null");
      
    throw new IllegalArgumentException("dbaseSave: unsupported class "+obj.getClass());
    }
    
  /**
   * Attempt to update ukey_id with the given privKey but ONLY if pubKey matches
   * @return the number of rows updated, must be 1 for operation OK
   */
  public int dbaseUpdate ( AeDbase dbase, Integer ukey_id, MimPublicKey pubKey, MimPrivateKey privKey ) throws SQLException
    {
    String query = "UPDATE "+cn_tbl+" SET "+
        cn_ukey_private+"=?,"+
        cn_note+"=? "+
        " WHERE "+cn_id+"=? "+
        " AND "+cn_ukey_public+"=? ";

      MimDprepared prepared = dbase.getPreparedStatement(query);
      int insIndex=1;
      prepared.setValue(insIndex++,privKey);
      prepared.setString(insIndex++,"updateprivate");
      prepared.setInt(insIndex++,ukey_id);
      prepared.setValue(insIndex++,pubKey);
      
      int r_count = prepared.executeUpdate();
      
      prepared.close();
    
      return r_count;
      }
  
  public Integer dbaseUpdate ( AeDbase dbase, Integer prim_id, MimKey row ) throws SQLException
    {
    if ( ! isMimRowAfterDbaseRow (dbase, prim_id, row))
      return prim_id;
    
    String query = "UPDATE "+cn_tbl+" SET "+
      cn_fingerprint+"=?,"+
      cn_creation+"=?,"+
      cn_pow+"=?,"+
      cn_signature+"=?,"+
      cn_type+"=?,"+
      cn_user_name+"=?,"+
      cn_ukey_public+"=?,"+
      cn_expire+"=?,"+
      cn_user_info+"=?,"+
      cn_updla_time+"=?,"+
      cn_updla_pow+"=?,"+
      cn_updla_sign+"=?"+
      " WHERE "+cn_id+"=?";

    MimDprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.fingerprint);
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setValue(insIndex++,row.signature);
    prepared.setString(insIndex++,row.type);
    prepared.setString(insIndex++,row.name);
    prepared.setValue(insIndex++,row.key);
    prepared.setValue(insIndex++,row.expiry);
    prepared.setString(insIndex++,row.info);
    prepared.setValue(insIndex++,row.last_update);
    prepared.setValue(insIndex++,row.update_proof_of_work);
    prepared.setValue(insIndex++,row.update_signature);

    prepared.setInt(insIndex++, prim_id);
    
    prepared.executeUpdate();
    
    prepared.close();
    
    return prim_id;
    }

  /**
   * I use this and will filter out the rows that are not fully populated
   */
  public Integer dbaseInsert(AeDbase dbase, MimFingerprint fprint, MimPublicKey key_s ) throws SQLException
    {
    if ( fprint == null || fprint.isNull() )
      throw new IllegalArgumentException("Cannot insert null Fingerprint");

    if ( key_s == null || key_s.isNull() )
      throw new IllegalArgumentException("Cannot insert null public key");
      
    String query = "INSERT INTO  "+cn_tbl+" ( "+cn_fingerprint+","+cn_ukey_public+" ) VALUES ( ?,? ) ";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,fprint);
    prepared.setValue(insIndex++,key_s);
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();
    
    // here I may queue for further full info
    aedbWanted.dbaseInsert(dbase, MimEntityList.aekey, fprint);

    return primary_id;
    }

  
  public Integer dbaseInsert ( AeDbase dbase, MimKey row) throws SQLException
    {
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_creation+","+
       cn_pow+","+
       cn_signature+","+
       cn_type+","+
       cn_user_name+","+
       cn_ukey_public+","+
       cn_expire+","+
       cn_user_info+","+
       cn_updla_time+","+
       cn_updla_pow+","+
       cn_updla_sign+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,? )";
     
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.fingerprint);
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setValue(insIndex++,row.signature);
    prepared.setString(insIndex++,row.type);
    prepared.setString(insIndex++,row.name);
    prepared.setValue(insIndex++,row.key);
    prepared.setValue(insIndex++,row.expiry);
    prepared.setString(insIndex++,row.info);
    prepared.setValue(insIndex++,row.last_update);
    prepared.setValue(insIndex++,row.update_proof_of_work);
    prepared.setValue(insIndex++,row.update_signature);
    
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
  
  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 

  /**
   * It may return an empty object but not a null one
   */
  public MimKey getMimMesage (AeDbase dbase, Integer row_id) throws SQLException
    {
    Brs ars = selectUsingPrimaryId (dbase,row_id);
    
    MimKey risul = new MimKey();
    
    if ( ars.next() )
      risul = getMimMesage(ars);
    
    ars.close();
    
    return risul;
    }

  @Override
  public MimKey getMimMesage ( Brs ars )
    {
    MimKey risul = new MimKey();
    
    risul.fingerprint = new MimFingerprint(ars.getBytes(cn_fingerprint));
    risul.creation    = new MimTimestamp(ars.getTimestamp(cn_creation));
    risul.proof_of_work = new MimPowValue(ars.getString(cn_pow));
    risul.signature   = new MimSignature(ars.getBytes(cn_signature));
  
    risul.type   = ars.getString(cn_type);
    risul.name   = ars.getString(cn_user_name);
    risul.key    = new MimPublicKey(ars.getBytes(cn_ukey_public));
    risul.expiry = new MimTimestamp(ars.getTimestamp(cn_expire));
    risul.info   = ars.getString(cn_user_info);
    
    risul.last_update = new MimTimestamp(ars.getTimestamp(cn_updla_time));
    risul.update_proof_of_work = new MimPowValue(ars.getString(cn_updla_pow));
    risul.update_signature = new MimSignature(ars.getBytes(cn_updla_sign));
    
    risul.privateKey = new MimPrivateKey(ars.getBytes(cn_ukey_private));
  
    return risul;
    }
  
private class MimKeyIterator implements Iterator<MimKey> 
  { 
  private final Brs ars;
  private boolean haveNext=false;
  
  MimKeyIterator (Brs ars) 
    { 
    this.ars = ars; 
    } 
    
  public boolean hasNext() 
    {
    if ( ars.isClosed()) return false;
    
    // we already know we have a next, you need to read it using next()
    if ( haveNext ) return true;
    
    // do we have a next ?
    haveNext = ars.next();
    
    if ( ! haveNext )
      ars.close();
    
    return haveNext;
    } 
    
  @Override
  public MimKey next() 
    {
    if ( ! haveNext )
      throw new NoSuchElementException("You need to call hasNext() first");
    
    haveNext = false;
    
    return getMimMesage(ars);    
    }
  } 
 
  
/**
 * Need to add unique to public key  
 */
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addProvableHeader(cn_id, cn_fingerprint, cn_creation, cn_pow, cn_signature);
    
    addSchemaRow(new SchemaRowVarchar(cn_type,20));
    addSchemaRow(new SchemaRowVarchar(cn_user_name,80));  
    addSchemaRow(new SchemaRowVarbinary(cn_ukey_public,MimPublicKey.pub_key_bytes_len)).setNotNullable();
    addSchemaRow(new SchemaRowTimestamp(cn_expire));
    addSchemaRow(new SchemaRowVarchar(cn_user_info,32000));

    addSchemaRow(new SchemaRowTimestamp(cn_updla_time));
    addSchemaRow(new SchemaRowVarchar(cn_updla_pow,MimPowValue.pow_ascii_len));
    addSchemaRow(new SchemaRowVarbinary(cn_updla_sign,MimSignature.signature_len));  

    addSchemaRow(new SchemaRowTimestamp(cn_updlo_time));
    addSchemaRow(new SchemaRowTimestamp(cn_lastu_time));

    addSchemaRow(new SchemaRowVarbinary(cn_ukey_private,MimPrivateKey.priv_key_bytes_len));  // I may not have a private key
    addSchemaRow(new SchemaRowVarchar(cn_ukey_X509cert,1000));
    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }

  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+cn_view+" AS "+ 
        " SELECT "+cn_tbl+".*, "+
        " COALESCE("+cn_updla_time+","+cn_creation+") AS lastu "+
        " FROM "+cn_tbl);
    }
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String  query="CREATE UNIQUE INDEX ukey_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);

    /*
    query="CREATE UNIQUE INDEX ukey_pub_index ON "+cn_tbl+" ( "+cn_ukey_public+" ) ";
    dbexec.executeDbaseDone(query);
    */
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
//    dbexec.executeDbaseDone("DROP INDEX ukey_pub_index");
    dbexec.executeDbaseDone("DROP INDEX ukey_fingerprint_index");
    }  
  }
      
/**
 * You CANNOT insert partial key since it will NOT pass the fingerprint test

public Integer getPrimaryId (AeDbase dbase, MimPublicKey srch_val ) throws SQLException
  {
  if ( srch_val == null || srch_val.isNull() ) return null;
  
  String query="SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_ukey_public+"=?";
  
  Dprepared prepared=dbase.getPreparedStatement(query.toString());
  prepared.setBinary(1, srch_val.getPub_A_bytes());
  Brs ars = prepared.executeQuery();
  
  Integer risul = null;
  
  if ( ars.next() ) 
    risul=ars.getInteger(getPrimaryColName());
  
  ars.close();
  
  return risul;
  }

 *
 *
 *
 */

      
  } 

