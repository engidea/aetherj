/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.backend.mimclient.McwAddress;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

public final class AedbAddress extends AedbOperations
  {
  private static final String classname="AedbAddress.";
  
  public static final MimEntity mimEntity = MimEntityList.aeaddres;

  public static final String cn_tbl          = "adrs_tbl";
  public static final String cn_id           = "adrs_id";
  public static final String cn_location     = "adrs_location";
  public static final String cn_sublocation  = "adrs_sublocation";
  public static final String cn_port         = "adrs_port";
  public static final String cn_iptype       = "adrs_iptype";
  public static final String cn_type         = "adrs_type";         // See BimAddress,type_DYNAMIC
  public static final String cn_update_time  = "adrs_update_time";  // update time of this address, to manage expire
  public static final String cn_ping_time    = "adrs_ping_time";    // Successful me calling this address
  public static final String cn_sync_time    = "adrs_sync_time";    // Successful this address calling me
  public static final String cn_protoVMajor  = "adrs_ProtoVMajor";
  public static final String cn_protoVMinor  = "adrs_ProtoVMinor";
  public static final String cn_clientVMajor = "adrs_ClientVMajor";
  public static final String cn_clientVMinor = "adrs_ClientVMinor";
  public static final String cn_client_name  = "adrs_ClientName";
  public static final String cn_client_type  = "adrs_ClientType";   // String needed since Aether only allows Aether as client....
  public static final String cn_note         = "adrs_note";
 
  private static final String cn_view        = "adrs_view";
  
  public static final String cv_client_type_aether ="Aeo";     // to be used with cn_client_type
  public static final String cv_client_type_aetherj="Aej";     // to be used with cn_client_type
  
  public AedbAddress ( Stat stat, PrintlnProvider println )
    {
    super(stat,println,mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }
  
  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 
  
  /**
   * Someone may wish to find the primary_id for further use
   * @return the primary_id if there is one
   */
  public Integer getPrimaryId (AeDbase dbase, String location, String sublocation, int port ) throws SQLException
    {
    String query = "SELECT "+cn_id+" FROM "+cn_tbl+
        " WHERE "+cn_location+"=? AND "+cn_port+"=? AND "+cn_sublocation+"=? ";
     
     Dprepared prepared=dbase.getPreparedStatement(query.toString());
     
     int insIndex=1;
     prepared.setString(insIndex++, location);
     prepared.setInt(insIndex++, port);
     prepared.setString(insIndex++, sublocation);
     
     Brs ars = prepared.executeQuery();
     
     Integer risul = null;
     
     if ( ars.next() ) 
       risul=ars.getInteger(cn_id);
     
     ars.close();
     
     return risul;
    }
  
  /**
   * This is called when importing from a received cache
   */
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj instanceof McwAddress )
      return dbaseSave(dbase,(McwAddress)obj);
    
    if ( obj instanceof MimAddress )
      {
      // this possibly adds a new row to the model, if not present
      McwAddress address = stat.mcwAddressModel.addressSave((MimAddress)obj);
      // this will either insert or update depending if primary_id is set
      return dbaseSave(dbase, address);
      }
    
    throw new IllegalArgumentException("dbaseSave is NOT implemented for "+obj.getClass());
    }
  
  public Integer dbaseSave ( AeDbase dbase, McwAddress row) throws SQLException
    {
    if ( row == null ) 
      return null;

    if ( row.isNull() )
      return row.setPrimaryId(dbaseInsert(dbase, row));
    else 
      return dbaseUpdate(dbase, row);
    }
  
  /**
   * Maybe I should update more columns ?
   * @param dbase
   * @param mcwrow
   * @return
   * @throws SQLException
   */
  private Integer dbaseUpdate ( AeDbase dbase, McwAddress mcwrow) throws SQLException
    {
    String query = "UPDATE "+cn_tbl+" SET "+
        cn_client_name+"=?,"+
        cn_ping_time+"=?,"+
        cn_sync_time+"=?,"+
        cn_client_type+"=?,"+
        cn_note+"=?"+
        " WHERE "+cn_id+"=?";
     
     MimAddress row = mcwrow.getMimAddress();
     
     MimDprepared prepared = dbase.getPreparedStatement(query);
     int insIndex=1;
     prepared.setString(insIndex++,row.client.name);
     prepared.setValue(insIndex++,mcwrow.getPingStats());
     prepared.setValue(insIndex++,mcwrow.getSyncStats());
     prepared.setString(insIndex++,mcwrow.getClientType());
     prepared.setString(insIndex++,null);
     prepared.setInt(insIndex++, mcwrow.getPrimaryId());
     
     prepared.executeUpdate();
     prepared.close();
    
    return mcwrow.getPrimaryId();
    }
  
  public int dbaseDelete ( AeDbase dbase, McwAddress row) throws SQLException
    {
    if ( row == null )
      return 0;
    
    String query = "DELETE FROM "+cn_tbl+" WHERE "+cn_id+"=?";
    
    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++,row.getPrimaryId());
    
    int risul=prepared.executeUpdate();
    
    prepared.close();
    
    return risul;
    }
  
  public Integer dbaseInsert ( AeDbase dbase, McwAddress mcwrow) throws SQLException
    {
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_location+","+
       cn_sublocation+","+
       cn_port+","+
       cn_iptype+","+
       cn_type+","+              
       cn_client_name+","+
       cn_update_time+","+
       cn_ping_time+","+
       cn_sync_time+","+
       cn_protoVMajor+","+
       cn_protoVMinor+","+
       cn_clientVMajor+","+
       cn_clientVMinor+","+
       cn_client_type+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
    
    MimAddress row = mcwrow.getMimAddress();
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setString(insIndex++,row.location);
    prepared.setString(insIndex++,row.sublocation);
    prepared.setInt(insIndex++,row.port);
    prepared.setInt(insIndex++,row.location_type);
    prepared.setInt(insIndex++,row.type);
    prepared.setString(insIndex++,row.client.name);
    prepared.setTimestamp(insIndex++,mcwrow.getUpdateTimestamp());
    prepared.setValue(insIndex++,mcwrow.getPingStats());
    prepared.setValue(insIndex++,mcwrow.getSyncStats());
    prepared.setInt(insIndex++, row.protocol.version_major);
    prepared.setInt(insIndex++, row.protocol.version_minor);
    prepared.setInt(insIndex++, row.client.version_major);
    prepared.setInt(insIndex++, row.client.version_minor);
    prepared.setString(insIndex++,mcwrow.getClientType());
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
  
  public Brs select ( AeDbase dbase )
    {
    String query = "SELECT * FROM "+cn_view;
    
    return dbase.select(query);
    }
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowVarchar(cn_location,300)).setNotNullable();  
    addSchemaRow(new SchemaRowVarchar(cn_sublocation,300)).setNotNullable();  
    addSchemaRow(new SchemaRowInteger(cn_port)).setNotNullable();  
    addSchemaRow(new SchemaRowInteger(cn_iptype));  
    addSchemaRow(new SchemaRowInteger(cn_type));
    addSchemaRow(new SchemaRowTimestamp(cn_update_time));
    addSchemaRow(new SchemaRowTimestamp(cn_ping_time));  
    addSchemaRow(new SchemaRowTimestamp(cn_sync_time));
    addSchemaRow(new SchemaRowVarchar(cn_client_name,MimClient.name_len_max));
    addSchemaRow(new SchemaRowInteger(cn_protoVMajor));  
    addSchemaRow(new SchemaRowInteger(cn_protoVMinor));  
    addSchemaRow(new SchemaRowInteger(cn_clientVMajor));  
    addSchemaRow(new SchemaRowInteger(cn_clientVMinor));  
    addSchemaRow(new SchemaRowVarchar(cn_client_type,10));
    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));  
    }


  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+cn_view+" AS "+ 
      "SELECT adrs_tbl.* "+
      " FROM "+cn_tbl);
    }
  
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX adrs_loc_port_index ON "+cn_tbl+" ( "+
      cn_location+","+cn_port+","+cn_sublocation+" ) ";
    
    dbexec.executeDbaseDone(query);
    }

  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX adrs_loc_port_index");
    }  

  }
      
      
  } 

