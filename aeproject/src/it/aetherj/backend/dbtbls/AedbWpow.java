/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;

/**
 * I need to save intermediate results while waiting for client to calculate POW
 * The logic is that when server send to client the string to calculate the POW it will also save here the json to reconstruct
 * the object to the original state, so, once pow arrives, it can recreate the object save th epow and complete the operation 
 * AHHHH, since on insert I do not have the id (I have not yet inserted), I will use the null_entity_id
 * This means that you can only attempt insert for ONE entity at a time (may be an issue for votes....)
 */
public final class AedbWpow extends AedbOperations
  {
  private static final String classname="AedbWpow";
  
  public static final MimEntity mimEntity = MimEntityList.aewpow;
  
  public static final String cn_tbl        = "aewpow_tbl";
  public static final String cn_id         = "aewpow_id";
  public static final String cn_entity     = "aewpow_entity";       // this is what, address, board, keys....
  public static final String cn_wuser_id   = "aewpow_wuser_id";     // The web user that has created this row
  public static final String cn_entity_id  = "aewpow_entity_id";    // The primary id of the row that is being managed, if available
  public static final String cn_jsons      = "aewpow_jsons";        // The json source, a CLOB
  public static final String cn_creation   = "aewpow_creation";     // Timestamp to garbage collect rows 
  public static final String cn_note       = "aewpow_note";
 
  private static final String cn_view      = "aewpow_view";
  
  public AedbWpow ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }

  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 

  
  public Integer dbaseInsert(AeDbase dbase, Integer wuser_id, MimEntity entity, Integer entity_id, String jsons ) throws SQLException
    {
    String query = "INSERT INTO "+cn_tbl+" ( "+
        cn_wuser_id+","+
        cn_entity+","+
        cn_entity_id+","+
        cn_jsons+","+
        cn_creation+","+
        cn_note+
        " ) VALUES ( ?,?,?,?,?,? )";
      
     MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
     int insIndex=1;
     prepared.setInt(insIndex++,wuser_id);
     prepared.setValue(insIndex++,entity);
     prepared.setInt(insIndex++,entity_id);
     prepared.setString(insIndex++,jsons);
     prepared.setTimestamp(insIndex++);
     prepared.setString(insIndex++, null);
     
     Brs ars = prepared.executeUpdateReturnKeys();

     Integer primary_id=null;
     
     if ( ars.next() ) 
       primary_id = ars.getInteger(1);
     
     ars.close();  
     prepared.close();
     return primary_id;
    }
  
  
  /**
   * Periodic cleaning up, delete all rows older than a specific date
   */
  public int dbaseDeleteOlder (AeDbase dbase, java.util.Date adate ) throws SQLException
    {
    String query = "DELETE FROM "+cn_tbl+" WHERE "+cn_creation+" < ?";
    MimDprepared prepared=dbase.getPreparedStatement(query);
    prepared.setDate(1, adate);
    int risul = prepared.executeUpdate();
    prepared.close();
    
    return risul;
    }
    
  /**
   * Delete the given row
   */
  public int dbaseDelete (AeDbase dbase, Integer row_id ) throws SQLException
    {
    String query = "DELETE FROM "+cn_tbl+" WHERE "+cn_id+"=?";
    MimDprepared prepared=dbase.getPreparedStatement(query);
    prepared.setInt(1, row_id);
    int risul = prepared.executeUpdate();
    prepared.close();
    
    return risul;
    }

  /**
   * Return the Json for the given wuser, entity and id and then, deletes the row
   */
  public WpowRecord getJsons (AeDbase dbase, Integer wuser_id, Integer row_id  ) throws SQLException
    {
    String query="SELECT * FROM "+cn_tbl+" WHERE "+cn_id+"=? ";
    MimDprepared prepared=dbase.getPreparedStatement(query);
    prepared.setInt(1, row_id);
    Brs ars = prepared.executeQuery();
    
    WpowRecord risul = new WpowRecord();
    
    Integer huser_id=null;
    
    if ( ars.next() ) 
      {
      huser_id=ars.getInteger(cn_wuser_id);
      risul.json_string=ars.getString(cn_jsons);
      risul.entity_id=ars.getInteger(cn_entity_id);
      }
    
    ars.close();
    
    if ( wuser_id.equals(huser_id))
      {
      dbaseDelete(dbase,row_id);
      return risul;
      }
    else
      {
      throw new IllegalArgumentException("wuser_id="+wuser_id+" != from huser_id="+huser_id);
      }
    }

  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowInteger(cn_wuser_id)).setNotNullable();
    addSchemaRow(new SchemaRowVarchar(cn_entity,MimEntity.entity_len_max)).setNotNullable();  
    addSchemaRow(new SchemaRowInteger(cn_entity_id));
    addSchemaRow(new SchemaRowTimestamp(cn_creation));
    addSchemaRow(new SchemaRowClob(cn_jsons,col_len_100MB));
    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }
  
  @Override
  public String getTableName ()
    {
    return cn_tbl;
    } 

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+cn_view+" AS "+ 
      "SELECT * FROM "+cn_tbl);
    }
  
  @Override
  public void dropConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT wpow_wuser_ref");
    }
    
  @Override
  public void createConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT wpow_wuser_ref "+
       "FOREIGN KEY ("+cn_wuser_id+") REFERENCES "+AedbWebuser.cn_tbl+" ("+AedbWebuser.cn_id+") ON DELETE CASCADE ");

    }
  
/*
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX wpow_user_entity_index ON "+cn_tbl+
        " ( "+
        cn_wuser_id+","+
        cn_entity+","+
        cn_entity_id+
        " ) ";
    dbexec.executeDbaseDone(query);
    }
*/
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX wpow_user_entity_index");
    }  
  }
      
public static final class WpowRecord
  {
  public String  json_string;
  public Integer entity_id;    // mmay be null if the record has not yet been inserted
  }


  } 

