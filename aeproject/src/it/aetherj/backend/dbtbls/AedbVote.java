/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;
import java.util.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * up vote a thread result in type=1 type_class=1 and an empty field in the post_id
 * down voting a thread result in an updated vote with type=2, class=1
 * 
 * up vote a post result in type=1 type_class=1 and a target that is the post + post_id
 * down voting a thread result in an updated vote with type=2, class=1
 * 
 * 
 * 
 * 
 */
public final class AedbVote extends AedbMimOperations  <MimVote>
  {
  private static final String classname="AedbVote";
  
  public static final MimEntity mimEntity = MimEntityList.aevote;
  
  public static final String cn_tbl         = "aevote_tbl";
  public static final String cn_id          = "aevote_id";

  public static final String cn_fingerprint = "aevote_fingerprint";
  public static final String cn_creation    = "aevote_creation";
  public static final String cn_pow         = "aevote_pow";
  public static final String cn_signature   = "aevote_signature";
  
  public static final String cn_owner_id    = "aevote_owner_id";    // this vote is owned by this user
  public static final String cn_board_id    = "aevote_board_id";    // this thread refers to this board
  public static final String cn_thread_id   = "aevote_thread_id";   // the thread of the above board

  public static final String cn_target      = "aevote_target";      // target what ? a MimFingerprint
  
  public static final String cn_post_id     = "aevote_post_id";     // MAYBE refers to this parent
  
  public static final String cn_type_class  = "aevote_type_class";  // what is it ?
  public static final String cn_type        = "aevote_type";        // what is it ?
  public static final String cn_entity_v    = "aevote_entity_v";    // what is it ?
  public static final String cn_meta        = "aevote_meta";        // boilerplate for... everything
  
  public static final String cn_updla_time  = "aevote_updla_time";  // last update
  public static final String cn_updla_pow   = "aevote_updla_pow";
  public static final String cn_updla_sign  = "aevote_updla_sign";
  public static final String cn_note        = "aevote_note";
 
  public static final String cn_view        = "aevote_view";
  
  private AedbUkey   aedbUkey;    // needed to manage the pki relations
  private AedbBoard  aedbBoard;   // needed to manage the board relations
  private AedbThread aedbThread;  // 
  private AedbPost   aedbPost;    //
  private AedbWanted aedbWanted;   // needed to add wanted info on users

  
  public AedbVote ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    aedbUkey   = stat.aedbFactory.aedbUpki;
    aedbBoard  = stat.aedbFactory.aedbBoard;
    aedbThread = stat.aedbFactory.aedbThread;
    aedbPost   = stat.aedbFactory.aedbPost;
    aedbWanted = stat.aedbFactory.aedbWanted;
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }
  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }

  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 
  
  @Override
  public Integer getPrimaryId (AeDbase dbase, MimFingerprint srch_val ) throws SQLException
    {
    String query="SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_fingerprint+"=?";
    MimDprepared prepared=dbase.getPreparedStatement(query);
    prepared.setValue(1, srch_val);
    Brs ars = prepared.executeQuery();
    
    Integer risul = null;
    
    if ( ars.next() ) 
      risul=ars.getInteger(1);
    
    ars.close();
    
    return risul;
    }

  
  public Iterator<MimVote> selectForCache ( AeDbase dbase ) throws SQLException
    {
    String query = "SELECT * "+
    " FROM "+cn_view+
    " ORDER BY lastu";
    
    Dprepared prepared=dbase.getPreparedStatement(query.toString());
    
    return new MimVoteIterator(prepared.executeQuery());
    }

  @Override
  public Iterator<MimVote> selectForServlet ( AeDbase dbase, MimFilterTimestamp filter, int limit ) throws SQLException
    {
    MimTimestamp start = filter.getStart();
    MimTimestamp end = filter.getEnd();
    
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT * ");
    query.append(" FROM "+cn_view );
    query.append(" WHERE "+cn_signature+" IS NOT NULL ");
    if ( ! start.isNull() ) query.append(" AND lastu >= ? ");
    if ( ! end.isNull() )  query.append(" AND lastu <= ? ");
    query.append(" ORDER BY lastu");
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    
    if ( ! start.isNull() ) prepared.setValue(insIndex++, start);
    if ( ! end.isNull() ) prepared.setValue(insIndex++, end);
    prepared.setInt(insIndex++, limit);
    
    return new MimVoteIterator(prepared.executeQuery());
    }

  @Override
  public Iterator<MimVote> selectForServlet ( AeDbase dbase, MimFilterFingerprint filter, int limit ) throws SQLException
    {
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT * ");
    query.append(" FROM "+cn_view );
    query.append(" WHERE "+cn_signature+" IS NOT NULL ");
    query.append(" AND "+cn_fingerprint+" IN ");
    query.append(newSqlInlist(limit));
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    insIndex = addSqlInvalues(prepared, insIndex, filter, limit);
    prepared.setInt(insIndex++, limit);

    return new MimVoteIterator(prepared.executeQuery());
    }

  
  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj instanceof MimVote )
      {
      MimVote row = (MimVote)obj;
      
      Integer prim_id=getPrimaryId(dbase, row.fingerprint);
      
      if ( prim_id == null )
        prim_id=dbaseInsert(dbase, row);
      else 
        dbaseUpdate(dbase, prim_id, row);
      
      // make sure that I do not want this anymore
      aedbWanted.dbaseDelete(dbase, mimEntity, row.fingerprint);

      return prim_id;
      }
    
    println(classname+"dbaseSave: unsupported class "+obj.getClass());
    return null;
    }

  
  
  /**
   * This is a general idea, but it is surely important for votes
   * I must NOT update a vote with an older vote !! I need to check for this
   */
  public Integer dbaseUpdate ( AeDbase dbase, Integer prim_id, MimVote row) throws SQLException
    {
    if ( ! isMimRowAfterDbaseRow (dbase, prim_id, row))
      return prim_id;

    String query="UPDATE "+cn_tbl+" SET "+
        cn_creation+"=?,"+
        cn_pow+"=?,"+
        cn_signature+"=?,"+
        cn_board_id+"=?,"+
        cn_thread_id+"=?,"+
        cn_target+"=?,"+
        cn_post_id+"=?,"+
        cn_owner_id+"=?,"+ 
        cn_type+"=?,"+
        cn_type_class+"=?,"+
        cn_entity_v+"=?,"+
        cn_meta+"=?,"+
        cn_updla_time+"=?,"+
        cn_updla_pow+"=?,"+
        cn_updla_sign+"=?,"+
        cn_note+"=?"+
    " WHERE "+cn_id+"=?";

    Integer owner_id = aedbUkey.getPrimaryId(dbase, row.owner);
    
    if ( owner_id == null )
      owner_id = aedbUkey.dbaseInsert(dbase, row.owner, row.owner_publickey);
    
    Integer board_id = aedbBoard.getPrimaryId(dbase, row.board); 
    
    if ( board_id == null )
      board_id = aedbBoard.dbaseInsert(dbase, row.board );
    
    Integer thread_id = aedbThread.getPrimaryId(dbase, row.thread);
    
    if ( thread_id == null )
      thread_id = aedbThread.dbaseInsert(dbase, row.thread );
    
    // It happens that you may wish to vote a Thread or vote a post, the target may be a thread (same as row.thread or a post)
    Integer post_id=null; // assume this vote is for a Thread
    
    if ( ! sameFingerprint(row.thread, row.target ))
      {
      post_id = aedbPost.getPrimaryId(dbase, row.target);

      // I cannot declare a fault here, this vote MAY refer to a Post that WILL be inserted later, but I do not know
      // the only reasonable thing to do (since Aether DB is broken) is to have a periodic task that fix things up..
      // this is the reason I save the target in db, even if it is not strictly needed
      // IF there was a separate field I COULD have done it properly, but, no, there is not
      }

    MimDprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setValue(insIndex++,row.signature);
    prepared.setInt(insIndex++,board_id);
    prepared.setInt(insIndex++,thread_id);
    prepared.setValue(insIndex++,row.target);
    prepared.setInt(insIndex++,post_id);
    prepared.setInt(insIndex++,owner_id);
    prepared.setInt(insIndex++,row.type);
    prepared.setInt(insIndex++,row.typeclass);
    prepared.setInt(insIndex++,row.entity_version);
    prepared.setValue(insIndex++,row.meta);
    prepared.setValue(insIndex++,row.last_update);
    prepared.setValue(insIndex++,row.update_proof_of_work);
    prepared.setValue(insIndex++,row.update_signature);
    prepared.setString(insIndex++,null);
    prepared.setInt(insIndex++,prim_id);
    
    prepared.executeUpdate();
    prepared.close();
    
    return prim_id;
    }
  
  private boolean sameFingerprint ( MimFingerprint a, MimFingerprint b )
    {
    if ( a==null || b==null ) return false;
    
    return a.equals(b);
    }
  
  public Integer dbaseInsert ( AeDbase dbase, MimVote row) throws SQLException
    {
    Integer owner_id = aedbUkey.getPrimaryId(dbase, row.owner);
    
    if ( owner_id == null )
      owner_id = aedbUkey.dbaseInsert(dbase, row.owner, row.owner_publickey);
    
    Integer board_id = aedbBoard.getPrimaryId(dbase, row.board); 
    
    if ( board_id == null )
      board_id = aedbBoard.dbaseInsert(dbase, row.board );
    
    Integer thread_id = aedbThread.getPrimaryId(dbase, row.thread);
    
    if ( thread_id == null )
      thread_id = aedbThread.dbaseInsert(dbase, row.thread );
    
    // It happens that you may wish to vote a Thread or vote a post, the target may be a thread (same as row.thread or a post)
    Integer post_id=null; // assume this vote is for a Thread
    
    if ( ! sameFingerprint(row.thread, row.target ))
      {
      post_id = aedbPost.getPrimaryId(dbase, row.target);
    
      // I cannot declare a fault here, this vote MAY refer to a vote that WILL be inserted later, but I do not know
      // the only reasonable thing to do (since Aether DB is broken) is to have a periodic task that fix things up..
      // this is the reason I save the target in db, even if it is not strictly needed
      // IF there was a separate field I COULD have done it properly, but, no, there is not
      }
    
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_creation+","+
       cn_pow+","+
       cn_signature+","+
       cn_board_id+","+
       cn_thread_id+","+
       cn_target+","+
       cn_post_id+","+
       cn_owner_id+","+ 
       cn_type+","+
       cn_type_class+","+
       cn_entity_v+","+
       cn_meta+","+
       cn_updla_time+","+
       cn_updla_pow+","+
       cn_updla_sign+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.fingerprint);
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setValue(insIndex++,row.signature);
    prepared.setInt(insIndex++,board_id);
    prepared.setInt(insIndex++,thread_id);
    prepared.setValue(insIndex++,row.target);
    prepared.setInt(insIndex++,post_id);
    prepared.setInt(insIndex++,owner_id);
    prepared.setInt(insIndex++,row.type);
    prepared.setInt(insIndex++,row.typeclass);
    prepared.setInt(insIndex++,row.entity_version);
    prepared.setValue(insIndex++,row.meta);
    prepared.setValue(insIndex++,row.last_update);
    prepared.setValue(insIndex++,row.update_proof_of_work);
    prepared.setValue(insIndex++,row.update_signature);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
  
  public Brs selectUsingThreadId ( AeDbase dbase, int thread_id ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_thread_id+"=?";

    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++,thread_id);
    
    return prepared.executeQuery();
    }
  
  public Brs selectUsingPrimaryId ( AeDbase dbase, Integer row_id ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_id+"=? ";
    Dprepared prepared=dbase.getPreparedStatement(query);
    prepared.setInt(1, row_id);
    return prepared.executeQuery();
    }

  /**
   * It may return an empty row but not a null one
   */
  public MimVote getMimMesage (AeDbase dbase, Integer thread_id) throws SQLException
    {
    Brs ars = selectUsingPrimaryId (dbase,thread_id);
    
    MimVote risul = new MimVote();
    
    if ( ars.next() )
      risul = getMimMesage(ars);
    
    ars.close();
    
    return risul;
    }

    
  @Override
  public MimVote getMimMesage ( Brs ars )
    {
    MimVote risul = new MimVote();
    
    risul.fingerprint   = new MimFingerprint(ars.getBytes(cn_fingerprint));
    risul.creation      = new MimTimestamp(ars.getTimestamp(cn_creation));
    risul.proof_of_work = new MimPowValue(ars.getString(cn_pow));
    risul.signature     = new MimSignature(ars.getBytes(cn_signature));

    risul.board      = new MimFingerprint(ars.getBytes(AedbBoard.cn_fingerprint));
    risul.thread     = new MimFingerprint(ars.getBytes(AedbThread.cn_fingerprint));
    risul.target     = new MimFingerprint(ars.getBytes(cn_target));

    risul.owner           = new MimFingerprint(ars.getBytes(AedbUkey.cn_fingerprint));
    risul.owner_publickey = new MimPublicKey(ars.getBytes(AedbUkey.cn_ukey_public));

    risul.typeclass  = ars.getInteger(cn_type_class, MimVote.typclass_discussion);
    risul.type       = ars.getInteger(cn_type, MimVote.type_upvote);
    risul.entity_version = ars.getInteger(cn_entity_v, MimVote.default_entity_v);
    risul.meta       = new MimMeta(ars.getString(cn_meta));
    
    risul.last_update = new MimTimestamp(ars.getTimestamp(cn_updla_time));
    risul.update_proof_of_work = new MimPowValue(ars.getString(cn_updla_pow));
    risul.update_signature = new MimSignature(ars.getBytes(cn_updla_sign));

    return risul;
    }
  
private class MimVoteIterator implements Iterator<MimVote> 
  { 
  private final Brs ars;
  private boolean haveNext=false;
  
  MimVoteIterator (Brs ars) 
    { 
    this.ars = ars; 
    } 
    
  public boolean hasNext() 
    {
    if ( ars.isClosed()) return false;
    
    // we already know we have a next, you need to read it using next()
    if ( haveNext ) return true;
    
    // do we have a next ?
    haveNext = ars.next();
    
    if ( ! haveNext )
      ars.close();
    
    return haveNext;
    } 
    
  @Override
  public MimVote next() 
    {
    if ( ! haveNext )
      throw new NoSuchElementException("You need to call hasNext() first");
    
    haveNext = false;

    return getMimMesage(ars);    
    }
  } 
  
   
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addProvableHeader(cn_id, cn_fingerprint, cn_creation, cn_pow, cn_signature);

    addSchemaRow(new SchemaRowInteger(cn_owner_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_board_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_thread_id)).setNotNullable();
    addSchemaRow(new SchemaRowVarbinary(cn_target,MimFingerprint.bytes_len)).setNotNullable();
    
    addSchemaRow(new SchemaRowInteger(cn_post_id));  // may be null if I am voting a thread
    
    addSchemaRow(new SchemaRowInteger(cn_type));        // meaning what ?
    addSchemaRow(new SchemaRowInteger(cn_type_class));  // Meaning what ?
    addSchemaRow(new SchemaRowInteger(cn_entity_v)); 
    addSchemaRow(new SchemaRowVarchar(cn_meta,MimMeta.len_max));

    addSchemaRow(new SchemaRowTimestamp(cn_updla_time));
    addSchemaRow(new SchemaRowVarchar(cn_updla_pow,MimPowValue.pow_ascii_len));
    addSchemaRow(new SchemaRowVarbinary(cn_updla_sign,MimSignature.signature_len));

    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }

  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override 
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    String query="CREATE VIEW "+cn_view+" AS "+ 
        " SELECT aevote_tbl.*,aeboard_fingerprint,aethread_fingerprint,ukey_public,ukey_fingerprint, "+
        " COALESCE("+cn_updla_time+","+cn_creation+") AS lastu "+
        " FROM "+cn_tbl+
        " LEFT OUTER JOIN aeboard_tbl ON aevote_board_id=aeboard_id "+
        " LEFT OUTER JOIN aethread_tbl ON aevote_thread_id=aethread_id "+
        " LEFT OUTER JOIN ukey_tbl ON aevote_owner_id=ukey_id ";
    
    db.executeDbaseDone(query);
    }
   
  @Override
  public void dropConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT aevote_owner_ref");
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT aevote_thread_ref");
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT aevote_board_ref");
    }
    
  
  @Override
  public void createConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT aevote_owner_ref "+
       "FOREIGN KEY ("+cn_owner_id+") REFERENCES "+AedbUkey.cn_tbl+" ("+AedbUkey.cn_id+") ON DELETE CASCADE ");

    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT aevote_board_ref "+
        "FOREIGN KEY ("+cn_board_id+") REFERENCES "+AedbBoard.cn_tbl+" ("+AedbBoard.cn_id+") ON DELETE CASCADE ");

    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT aevote_thread_ref "+
        "FOREIGN KEY ("+cn_thread_id+") REFERENCES "+AedbThread.cn_tbl+" ("+AedbThread.cn_id+") ON DELETE CASCADE ");

    }

  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX vote_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX vote_fingerprint_index");
    }  

  }
      
      
  } 

