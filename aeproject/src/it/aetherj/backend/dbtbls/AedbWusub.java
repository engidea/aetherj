/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.gwtwapp.client.beans.AegwUsub;
import it.aetherj.protocol.*;

/**
 * A web user subscribes to a bunch of Boards and it needs to be in db, now, since we have multiple users for each server
 */
public final class AedbWusub extends AedbOperations
  {
  private static final String classname="AedbWusub";
  
  public static final MimEntity mimEntity = MimEntityList.wusub;
  
  public static final String cn_tbl       = "wusub_tbl";
  public static final String cn_id        = "wusub_id";
  public static final String cn_wuser_id  = "wusub_wuser_id";   // this row is bound to this web user
  public static final String cn_board_id  = "wusub_board_id";   // this row is attached to this from_user_id (will be created)
  public static final String cn_notify    = "wusub_notify";     // boolean   
  public static final String cn_last_seen = "wusub_last_Seen";  // Timestamp
  public static final String cn_note      = "wusub_note";
 
  private static final String cn_view     = "wusub_view";
  
  private AedbBoard  aedbBoard;   // needed to manage the board relations

  public AedbWusub ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    aedbBoard  = stat.aedbFactory.aedbBoard;
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }

  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 
    
  private Integer dbaseInsert(AeDbase dbase, AegwUsub wuser ) throws SQLException
    {
    String query = "INSERT INTO "+cn_tbl+" ( "+
        cn_wuser_id+","+
        cn_board_id+","+
        cn_notify+","+
        cn_last_seen+","+
        cn_note+
        " ) VALUES ( ?,?,?,?,? )";
    
    Dprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setInt(insIndex++,wuser.wusub_wuser_id);
    prepared.setInt(insIndex++,wuser.wusub_board_id);
    prepared.setBoolean(insIndex++,wuser.wusub_notify);
    prepared.setTimestamp(insIndex++,wuser.wusub_last_seen);
    prepared.setString(insIndex, null);

    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
    
  private Integer dbaseUpdate(AeDbase dbase, AegwUsub wuser ) throws SQLException
    {
    String query="UPDATE "+cn_tbl+" SET "+
        cn_board_id+"=?,"+
        cn_notify+"=?,"+
        cn_note+"=?"+
    " WHERE "+cn_id+"=?";
    
    Dprepared prepared = dbase.getPreparedStatement(query.toString());
    int insIndex=1;
    prepared.setInt(insIndex++,wuser.wusub_board_id);
    prepared.setBoolean(insIndex++,wuser.wusub_notify);
    prepared.setString(insIndex++,null);
    prepared.setInt(insIndex++, wuser.wusub_id);

    prepared.executeUpdate();
    
    prepared.close();

    return wuser.wusub_id;
    }

  public Integer getPrimaryId (AeDbase dbase, Integer wuser_id, Integer board_id ) throws SQLException
    {
    String query = "SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+
    cn_wuser_id+"=? AND "+cn_board_id+"=? ";
    
    Dprepared prepared=dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, wuser_id);
    prepared.setInt(insIndex++, board_id);
    
    Brs ars = prepared.executeQuery();
    
    Integer risul = null;
    
    if ( ars.next() ) 
      risul=ars.getInteger(getPrimaryColName());
    
    ars.close();
    
    return risul;
    }
  
  
  /**
   * It may not be the case that I have a dbase insert, it may be an update using the retreived id
   */
  private Integer dbaseInsertTry(AeDbase dbase, AegwUsub wusub ) throws SQLException
    {
    if ( wusub.board_fingerprint == null )
      return dbaseInsert(dbase, wusub);
      
    MimFingerprint fprint = new MimFingerprint(wusub.board_fingerprint);
    
    Integer board_id = aedbBoard.getPrimaryId(dbase, fprint); 
    
    if ( board_id == null )
      board_id = aedbBoard.dbaseInsert(dbase, fprint );
    
    wusub.wusub_board_id = board_id;
    
    // now, wait a second, there may already be a wusub associated with this user...
    wusub.wusub_id = getPrimaryId(dbase, wusub.wusub_wuser_id, wusub.wusub_board_id);
    
    if ( wusub.wusub_id == null )
      return dbaseInsert(dbase, wusub);
    else
      return dbaseUpdate(dbase, wusub);
    }
  
  /**
   * Return the ID of the row inserted or the ID of the row that is already present
   * @throws SQLException 
   */
  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws Exception
    {
    if ( ! (obj instanceof AegwUsub) )
      throw new IllegalArgumentException("BAD class="+obj.getClass());
    
    return dbaseSave(dbase,(AegwUsub)obj);
    }

  public Integer dbaseSave ( AeDbase dbase, AegwUsub wusub ) throws Exception
    {
    if ( wusub.wusub_id == null )
      return dbaseInsertTry(dbase, wusub );
    else
      return dbaseUpdate(dbase, wusub);
    }
  
  public Brs selectUsingWuserId ( AeDbase dbase, Integer wuser_id ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_wuser_id+"=? ";

    Dprepared prepared=dbase.getPreparedStatement(query.toString());
    int insIndex=1;
    
    prepared.setInt(insIndex++, wuser_id);

    return prepared.executeQuery();
    }


  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowInteger(cn_wuser_id)).setNotNullable();  
    addSchemaRow(new SchemaRowInteger(cn_board_id)).setNotNullable();  
    addSchemaRow(new SchemaRowBoolean(cn_notify));
    addSchemaRow(new SchemaRowTimestamp(cn_last_seen));
    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }

  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    // A user can subscribe once only to a board
    String query="CREATE UNIQUE INDEX wusub_user_board_index ON "+cn_tbl+" ( "+cn_wuser_id+","+cn_board_id+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX wusub_user_board_index");
    }  

  @Override
  public void dropConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT wusub_wuser_ref");
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT wusub_board_ref");
    }
    
  
  @Override
  public void createConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT wusub_wuser_ref "+
       "FOREIGN KEY ("+cn_wuser_id+") REFERENCES "+AedbWebuser.cn_tbl+" ("+AedbWebuser.cn_id+") ON DELETE CASCADE ");

    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT wusub_board_ref "+
        "FOREIGN KEY ("+cn_board_id+") REFERENCES "+AedbBoard.cn_tbl+" ("+AedbBoard.cn_id+") ON DELETE CASCADE ");

    }
  
  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    String query="CREATE VIEW "+cn_view+" AS "+ 
        " SELECT wusub_tbl.*,"+
        AedbBoard.cn_name+","+
        AedbBoard.cn_fingerprint+","+
        AedbWebuser.cn_login+
        " FROM "+cn_tbl+
        " LEFT OUTER JOIN "+AedbWebuser.cn_tbl+" ON "+cn_wuser_id+"="+AedbWebuser.cn_id+
        " LEFT OUTER JOIN "+AedbBoard.cn_tbl+" ON "+cn_board_id+"="+AedbBoard.cn_id;
        
    db.executeDbaseDone(query);
    }
  }
      
      
  } 

