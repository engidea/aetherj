/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;
import java.util.Iterator;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * I wish to have a clear type being returned by the methods
 *
 */
public abstract class AedbMimOperations<T extends MimEmessagesMethods> extends AedbOperations
  {
  private static final String classname="AedbMimOperations";
  
  // this is a static so I can find out easily where I am using it, but you cannot change it at will
  public static final String cn_lastu = "lastu";
  
  // This, eventually will be associated with a user, maybe
  public static MimPowParams powParams = new MimPowParams(21, 600);
  
  
  public AedbMimOperations(Stat stat, PrintlnProvider println, MimEntity mimEntity)
    {
    super(stat, println, mimEntity);
    }
  
  
  /**
   * Aether has quite often, possibly always, a fingerprint on a post, a unique key
   * @return the column name in the table where this value is stored
   */
  protected abstract String getFingerprintColName ();
  
  public abstract Integer getPrimaryId (AeDbase dbase, MimFingerprint fingerprint ) throws SQLException;

  
  protected String newSqlInlist ( int limit )
    {
    StringBuilder risul = new StringBuilder(1000);
    risul.append(" (");
    
    boolean addcomma=false;
    
    for (int index=0; index<limit; index++)
      {
      if ( addcomma )
        risul.append(',');
      else
        addcomma=true;
      
      risul.append('?');
      }

    risul.append(") ");

    return  risul.toString();
    }
  
  protected int addSqlInvalues ( MimDprepared prepared, int insIndex, MimFilterFingerprint filter, int limit ) throws SQLException
    {
    for ( int index=0; index<limit; index++)
      prepared.setValue(insIndex++, filter.get(index));
    
    return insIndex;
    }
  
  public abstract Brs selectUsingPrimaryId(AeDbase dbase, Integer prim_id) throws SQLException;
  
  /**
   * If true it means that the incoming Mim row is good to be saved
   * NOTE: I also include here a check for possible illegal values in incoming, a timestamp that is in the future
   * @throws SQLException 
   */
  protected boolean isMimRowAfterDbaseRow ( AeDbase dbase, Integer prim_id, MimEmessagesMethods i_row ) throws SQLException
    {
    MimTimestamp i_row_updtime = i_row.peekLastUpdate();

    // I just want to be sure I a reasonably out of possible issues with timezones, hopefully
    MimTimestamp now = new MimTimestamp();
    now = now.add_hours(24);
    
    if ( i_row_updtime.isAfter(now))
      {
      println(classname+".isMimRowAfterDbaseRow: entity="+dbaseEntity+" fingerprint="+i_row.peekFingerprint()+" i_updtime="+i_row_updtime);
      return false;
      }
    
    Brs ars = selectUsingPrimaryId(dbase, prim_id);
    
    MimTimestamp db_row_updtime=null;
    
    if ( ars.next() )
      db_row_updtime=new MimTimestamp(ars.getTimestamp(cn_lastu));

    ars.close();
    
    boolean is_good = i_row_updtime.isAfter(db_row_updtime);
    
    if ( ! is_good )
      {
      // If the value is exactly the same, I am seeing the same value coming back 
      if ( i_row_updtime.isEqual(db_row_updtime) )
        return true;
      
      StringBuilder msg = new StringBuilder(500);
      msg.append(classname+".isMimRowAfterDbaseRow: entity="+dbaseEntity);
      msg.append(" fingerprint="+i_row.peekFingerprint());
      msg.append(" i_updtime="+i_row_updtime);
      msg.append(" db_row_updtime="+db_row_updtime);
      println(msg.toString());
      }

    return is_good;
    }

  
  /**
   * Provides a method to return rows that belong to cache
   */
  public abstract Iterator<T> selectForCache ( AeDbase dbase ) throws SQLException;
  
  public abstract Iterator<T> selectForServlet ( AeDbase dbase, MimFilterTimestamp filter, int limit ) throws SQLException;

  public abstract Iterator<T> selectForServlet ( AeDbase dbase, MimFilterFingerprint filter, int limit ) throws SQLException;

  /**
   * A class that implements this interface must provide a method that converts a Brs into a MimMessage
   */
  public abstract T getMimMesage ( Brs ars );
  
  }
