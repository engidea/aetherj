/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;
import java.util.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * Some object in the system needs to have a couple of private and public key, for whatever reason
 * So, it refers to this table with a specific ID to refer to it
 * Since it is used to identify somebody, there should be just ONE dbepki for each object....
 * Or, if you want two of them... well, have to references to it
 */
public final class AedbBoard extends AedbMimOperations<MimBoard>
  {
  private static final String classname="AedbBoard";
  
  public static final MimEntity mimEntity = MimEntityList.aeboard;
  
  public static final String cn_tbl          = "aeboard_tbl";
  public static final String cn_id           = "aeboard_id";
  
  public static final String cn_fingerprint  = "aeboard_fingerprint";  // 1) mim primary key
  public static final String cn_creation     = "aeboard_creation";     // 6) 
  public static final String cn_pow          = "aeboard_pow";          // 7)
  public static final String cn_signature    = "aeboard_signature";    // 8)

  public static final String cn_name         = "aeboard_name";          // 2) 
  public static final String cn_owner_id     = "aeboard_owner_id";      // 3,4) refers to a public key, if not present, insert it   
  public static final String cn_desc         = "aeboard_desc";          // 5) description

  public static final String cn_updla_time  = "aeboard_update_time";    // 9)
  public static final String cn_updla_pow   = "aeboard_update_pow";     // 10)
  public static final String cn_updla_sign  = "aeboard_update_sign";    // 11)
  
  public static final String cn_rx_time     = "aeboard_rx_time";  // 12) Local arrival
  
  public static final String cn_entity_v    = "aeboard_entity_v";      // 13)
  public static final String cn_lang        = "aeboard_lang";          // 14)
  public static final String cn_meta        = "aeboard_meta";          // 15)

  public static final String cn_thread_cnt  = "aeboard_thread_cnt";    // how many threads it has, adjusted by the GC
  
  public static final String cn_note        = "aeboard_note";
   
  private static final String cn_view       = "aeboard_view";
  
  private AedbUkey   aedbUkey;      // needed to manage the pki relations
  private AedbWanted aedbWanted;   // needed to add wanted info on users
  
  public AedbBoard ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity); 
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override 
  public void initialize()
    {
    aedbUkey = stat.aedbFactory.aedbUpki;
    aedbWanted = stat.aedbFactory.aedbWanted;
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }
  
  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }

  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 
 
  @Override
  public Integer getPrimaryId (AeDbase dbase, MimFingerprint srch_val ) throws SQLException
    {
    String query="SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_fingerprint+"=?";
    MimDprepared prepared=dbase.getPreparedStatement(query);
    prepared.setValue(1, srch_val);
    Brs ars = prepared.executeQuery();
    
    Integer risul = null;
    
    if ( ars.next() ) 
      risul=ars.getInteger(1);
    
    ars.close();
    
    return risul;
    }

  public Iterator<MimBoard> selectForCache ( AeDbase dbase ) throws SQLException
    {
    String query = "SELECT * "+
    " FROM "+cn_view+
    " WHERE "+cn_owner_id+" IS NOT NULL "+
    " ORDER BY lastu";
    
    Dprepared prepared=dbase.getPreparedStatement(query.toString());
    
    return new MimBoardIterator(prepared.executeQuery());
    }

  @Override
  public Iterator<MimBoard> selectForServlet ( AeDbase dbase, MimFilterTimestamp filter, int limit ) throws SQLException
    {
    MimTimestamp start = filter.getStart();
    MimTimestamp end = filter.getEnd();
    
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT * ");
    query.append(" FROM "+cn_view );
    query.append(" WHERE "+cn_signature+" IS NOT NULL ");
    if ( ! start.isNull() ) query.append(" AND lastu >= ? ");
    if ( ! end.isNull() )  query.append(" AND lastu <= ? ");
    query.append(" ORDER BY lastu");
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    
    if ( ! start.isNull() ) prepared.setValue(insIndex++, start);
    if ( ! end.isNull() ) prepared.setValue(insIndex++, end);
    prepared.setInt(insIndex++, limit);

    return new MimBoardIterator(prepared.executeQuery());
    }

  @Override
  public Iterator<MimBoard> selectForServlet ( AeDbase dbase, MimFilterFingerprint filter, int limit ) throws SQLException
    {
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT * ");
    query.append(" FROM "+cn_view );
    query.append(" WHERE "+cn_signature+" IS NOT NULL ");
    query.append(" AND "+cn_fingerprint+" IN ");
    query.append(newSqlInlist(limit));
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    insIndex = addSqlInvalues(prepared, insIndex, filter, limit);
    prepared.setInt(insIndex++, limit);

    return new MimBoardIterator(prepared.executeQuery());
    }

  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws Exception
    {
    if ( obj == null ) 
      return null;
    
    if ( obj instanceof MimBoard )
      return dbaseSave(dbase, (MimBoard)obj);

    if ( obj instanceof MimBoardIndex )
      return dbaseSave(dbase, (MimBoardIndex)obj);

    println(classname+"dbaseSave: unsupported class "+obj.getClass());
    return null;
    }

  private Integer dbaseSave ( AeDbase dbase, MimBoardIndex row) throws SQLException
    {
    throw new SQLException("UNSUPPORTED");
    
//    return null;
    }
  
  public Integer dbaseSave ( AeDbase dbase, MimBoard row) throws Exception
    {
    Integer prim_id=getPrimaryId(dbase, row.fingerprint);
    
    if ( prim_id == null )
      prim_id=dbaseInsert(dbase, row);
    else 
      dbaseUpdate(dbase, prim_id, row);
    
    // make sure that I do not want this anymore
    aedbWanted.dbaseDelete(dbase, mimEntity, row.fingerprint);
    
    return prim_id;
    }
    
  public Integer dbaseUpdate ( AeDbase dbase, Integer prim_id, MimBoard row) throws SQLException
    {
    if ( ! isMimRowAfterDbaseRow (dbase, prim_id, row))
      {
      println(classname+".dbaseUpdate: NO update: MimKey older than db row_id="+prim_id);
      return prim_id;
      }

    Integer owner_id = aedbUkey.getPrimaryId(dbase, row.owner);
    
    if ( owner_id == null )
      owner_id = aedbUkey.dbaseInsert(dbase, row.owner, row.owner_publickey);

    String query = "UPDATE "+cn_tbl+" SET "+
        cn_signature+"=?,"+
        cn_name+"=?,"+
        cn_owner_id+"=?,"+
        cn_desc+"=?,"+
        cn_creation+"=?,"+
        cn_pow+"=?,"+
        cn_rx_time+"=?,"+
        cn_entity_v+"=?,"+
        cn_lang+"=?,"+ 
        cn_meta+"=?,"+
        cn_updla_time+"=?,"+
        cn_updla_pow+"=?,"+
        cn_updla_sign+"=?,"+ 
        cn_note+"=?"+
        " WHERE "+cn_id+"=?";

     MimDprepared prepared = dbase.getPreparedStatement(query);
     int insIndex=1;

     prepared.setValue(insIndex++,row.signature);
     prepared.setString(insIndex++,row.name);
     prepared.setInt(insIndex++,owner_id);
     prepared.setString(insIndex++,row.description);
     prepared.setValue(insIndex++,row.creation);
     prepared.setValue(insIndex++,row.proof_of_work);
     prepared.setTimestamp(insIndex++);
     prepared.setInt(insIndex++,row.entity_version);
     prepared.setValue(insIndex++,row.language);
     prepared.setString(insIndex++,row.meta);
     prepared.setValue(insIndex++,row.last_update);
     prepared.setValue(insIndex++,row.update_proof_of_work);
     prepared.setValue(insIndex++,row.update_signature);
     prepared.setString(insIndex++,null);
     prepared.setInt(insIndex++, prim_id);
     
     prepared.executeUpdate();
     prepared.close();

     return prim_id;
     }

  public Integer dbaseInsert ( AeDbase dbase, MimFingerprint fingerprint ) throws SQLException 
    {
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_note+
       " ) VALUES ( ?,? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,fingerprint);
    prepared.setString(insIndex++,"missing");
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();
    
    aedbWanted.dbaseInsert(dbase, MimEntityList.aeboard, fingerprint);

    return primary_id;
    }

  
  public Integer dbaseInsert ( AeDbase dbase, MimBoard row) throws SQLException 
    {
    Integer owner_id = aedbUkey.getPrimaryId(dbase, row.owner);
    
    if ( owner_id == null )
      owner_id = aedbUkey.dbaseInsert(dbase, row.owner, row.owner_publickey);
    
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_signature+","+
       cn_name+","+
       cn_owner_id+","+
       cn_desc+","+
       cn_creation+","+
       cn_pow+","+
       cn_rx_time+","+
       cn_entity_v+","+
       cn_lang+","+ 
       cn_meta+","+
       cn_updla_time+","+
       cn_updla_pow+","+
       cn_updla_sign+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.fingerprint);
    prepared.setValue(insIndex++,row.signature);
    prepared.setString(insIndex++,row.name);
    prepared.setInt(insIndex++,owner_id);
    prepared.setString(insIndex++,row.description);
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setTimestamp(insIndex++);
    prepared.setInt(insIndex++,row.entity_version);
    prepared.setValue(insIndex++,row.language);
    prepared.setString(insIndex++,row.meta);
    prepared.setValue(insIndex++,row.last_update);
    prepared.setValue(insIndex++,row.update_proof_of_work);
    prepared.setValue(insIndex++,row.update_signature);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();
    
//    verifyId(dbase, primary_id);
    
    return primary_id;
    }
  
  /*
  private void verifyId ( AeDbase dbase, Integer prim_id ) throws Exception
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_id+"=?";
    
    MimDprepared prepared=dbase.getPreparedStatement(query);
    prepared.setInt(1, prim_id);
    Brs ars = prepared.executeQuery();
    
    MimBoard brd=null;
     
    if ( ars.next() ) 
      brd=getMimMesage(ars);
    
    ars.close();
    
    boolean good=false;
    
    if ( brd != null )
      good=brd.verifyFingerprint();
    
    if ( ! good )
      println("test us "+good);
    else
      println("test us "+good);

    }
    */
  
  public Brs selectAllValid ( AeDbase dbase )
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+
        cn_creation+" IS NOT NULL "+
        " ORDER BY "+cn_lastu+" DESC ";
    
    return dbase.select(query);
    }
  
  @Override
  public Brs selectUsingPrimaryId ( AeDbase dbase, Integer row_id ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_id+"=? ";
    Dprepared prepared=dbase.getPreparedStatement(query);
    prepared.setInt(1, row_id);
    return prepared.executeQuery();
    }
  
  /**
   * This looks into the record and ask to ukey to retrieve the record for ukey
   * This code is quite simple since the joined column has all fields with proper prefix
   */
  public MimBoard getMimMesage(AeDbase dbase, Integer row_id) throws SQLException
    {
    Brs ars = selectUsingPrimaryId (dbase,row_id);
    
    MimBoard risul = new MimBoard();
    
    if ( ars.next() )
      risul = getMimMesage(ars);
    
    ars.close();
    
    return risul;
    }
  
  @Override
  public MimBoard getMimMesage (Brs ars )
    {
    MimBoard risul = new MimBoard();

    risul.fingerprint   = new MimFingerprint(ars.getBytes(cn_fingerprint));
    risul.creation      = new MimTimestamp(ars.getTimestamp(cn_creation));
    risul.proof_of_work = new MimPowValue(ars.getString(cn_pow));
    risul.signature     = new MimSignature(ars.getBytes(cn_signature));
    
    risul.name = ars.getString(cn_name);
    risul.description = ars.getString(cn_desc);
    risul.owner = new MimFingerprint(ars.getBytes(AedbUkey.cn_fingerprint));
    risul.owner_publickey = new MimPublicKey(ars.getBytes(AedbUkey.cn_ukey_public));
    
    risul.entity_version = ars.getInteger(cn_entity_v, 1);
    risul.language = new MimLang(ars.getString(cn_lang));
    risul.meta = ars.getString(cn_meta);
    
    risul.last_update = new MimTimestamp(ars.getTimestamp(cn_updla_time));
    risul.update_proof_of_work = new MimPowValue(ars.getString(cn_updla_pow));
    risul.update_signature = new MimSignature(ars.getBytes(cn_updla_sign));
    
    return risul;
    }
    
private class MimBoardIterator implements Iterator<MimBoard> 
  { 
  private final Brs ars;
  private boolean haveNext=false;
  
  MimBoardIterator (Brs ars) 
    { 
    this.ars = ars; 
    } 
    
  public boolean hasNext() 
    {
    if ( ars.isClosed()) return false;
    
    // we already know we have a next, you need to read it using next()
    if ( haveNext ) return true;
    
    // do we have a next ?
    haveNext = ars.next();
    
    if ( ! haveNext )
      ars.close();
    
    return haveNext;
    } 
    
  @Override
  public MimBoard next() 
    {
    if ( ! haveNext )
      throw new NoSuchElementException("You need to call hasNext() first");
    
    haveNext = false;
    
    return getMimMesage(ars);    
    }
  } 
 
    
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance() 
    {
    addProvableHeader(cn_id, cn_fingerprint, cn_creation, cn_pow, cn_signature);
    
    addSchemaRow(new SchemaRowVarchar(cn_name,255));  
    addSchemaRow(new SchemaRowInteger(cn_owner_id));     // owner id MAY be null if board inserted to satisfy other objects 
    addSchemaRow(new SchemaRowVarchar(cn_desc,65535));
    addSchemaRow(new SchemaRowTimestamp(cn_updla_time));
    addSchemaRow(new SchemaRowVarchar(cn_updla_pow,MimPowValue.pow_ascii_len));
    addSchemaRow(new SchemaRowVarbinary(cn_updla_sign,MimSignature.signature_len));
    addSchemaRow(new SchemaRowTimestamp(cn_rx_time));
    addSchemaRow(new SchemaRowInteger(cn_entity_v));
    addSchemaRow(new SchemaRowVarchar(cn_lang,MimLang.len_max));
    addSchemaRow(new SchemaRowVarchar(cn_meta,100));
    addSchemaRow(new SchemaRowInteger(cn_thread_cnt));
    
    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }


  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+cn_view+" AS "+ 
        " SELECT aeboard_tbl.*,"+
        AedbUkey.cn_ukey_public+","+
        AedbUkey.cn_fingerprint+","+
        AedbUkey.cn_user_name+","+
        " COALESCE("+cn_updla_time+","+cn_creation+") AS "+cn_lastu+
        " FROM "+cn_tbl+
        " LEFT OUTER JOIN "+AedbUkey.cn_tbl+" ON "+cn_owner_id+"="+AedbUkey.cn_id
        );
  

    
    
    }
  
  @Override
  public void dropConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT aeboard_owner_ref");
    }
  
  @Override
  public void createConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT aeboard_owner_ref "+
       "FOREIGN KEY ("+cn_owner_id+") REFERENCES "+AedbUkey.cn_tbl+" ("+AedbUkey.cn_id+") ON DELETE CASCADE ");
    }
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX board_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);
    }
    
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX board_fingerprint_index");
    }  

  }
      
      
  } 

