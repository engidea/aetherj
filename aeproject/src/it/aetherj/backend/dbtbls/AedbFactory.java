/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.util.HashMap;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.protocol.MimEntity;
import it.aetherj.shared.v0.MimEntityMethods;

public class AedbFactory
  {
  private final Stat stat;
  private final HashMap<String, AedbOperations> pgrpMap = new HashMap<>();

  public final AedbWebuser   aedbWebuser;
  public final AedbWusub     aewusub;
  public final AedbAddress   aedbAddress;
  public final AedbNode      aedbNode;
  public final AedbUkey      aedbUpki;
  public final AedbBoard     aedbBoard;
  public final AedbThread    aedbThread;
  public final AedbPost      aedbPost;
  public final AedbVote      aedbVote;
  public final AedbTstamp    aedbTstamp;
  public final AedbCache     aedbCache;
  public final AedbWanted    aedbWanted;
  public final AedbTruststate aedbTstate;
  public final AedbBrdowner  aedbBrdowner;
  public final AedbWpow      aedbWpow;
  public final AedbChat      aedbChat;
  
  /**
   * @param println
   */
  public AedbFactory(Stat stat, PrintlnProvider println)
    {
    this.stat = stat;

    // this holds the login code, so, it is a bit more hard wired
    aedbWanted   = new AedbWanted(stat, println);
    aedbWebuser  = new AedbWebuser(stat, println);
    aewusub      = new AedbWusub(stat, println);
    aedbAddress  = new AedbAddress(stat, println);
    aedbNode     = new AedbNode(stat, println);
    aedbUpki     = new AedbUkey(stat, println);
    aedbBoard    = new AedbBoard(stat, println);
    aedbThread   = new AedbThread(stat, println);
    aedbPost     = new AedbPost(stat, println);
    aedbVote     = new AedbVote(stat, println);
    aedbTstamp   = new AedbTstamp(stat, println);
    aedbCache    = new AedbCache(stat, println);
    aedbTstate   = new AedbTruststate(stat, println);
    aedbBrdowner = new AedbBrdowner(stat, println);
    aedbWpow     = new AedbWpow(stat, println);
    aedbChat     = new AedbChat(stat, println);
    
    // from here on I just attach all known rpcdb tables to the map, so later the check on creation can be done
    addAedbTable(aedbWanted);
    addAedbTable(aedbNode);
    addAedbTable(aedbAddress);
    addAedbTable(aedbUpki);    // independent
    addAedbTable(aedbBoard);   // depends on upki
    addAedbTable(aedbThread);  // depends on board
    addAedbTable(aedbPost);    // depends on Thread
    addAedbTable(aedbVote);    // depends on Post
    addAedbTable(aedbWebuser);
    addAedbTable(aewusub);     // depends on web user and board
    addAedbTable(aedbTstamp);
    addAedbTable(aedbCache);
    addAedbTable(aedbTstate);
    addAedbTable(aedbBrdowner);
    addAedbTable(aedbWpow);
    addAedbTable(aedbChat);
    }

  public Integer isGoodUsernamePassword (AeDbase dbase, String username, String password )
    {
    return aedbWebuser.isGoodUsernamePassword(dbase, username, password);
    }
    
  private void addAedbTable(AedbOperations atable)
    {
    // NOTE that I add what should be a unique map key
    pgrpMap.put(atable.getHashMapKey(), atable);
    }

  public AedbOperations getAedb(MimEntityMethods obj)
    {
    return getAedb(obj.getMimEntity());
    }

  public AedbOperations getAedb(MimEntity obj)
    {
    return pgrpMap.get(obj.getValue());
    }

  public void initialize()
    {
    for ( AedbOperations handler : pgrpMap.values()  )
      handler.initialize();
    }
  }
