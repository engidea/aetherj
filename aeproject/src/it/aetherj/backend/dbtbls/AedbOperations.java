/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.MimEntity;
import it.aetherj.shared.HashMapKeyProvider;
import it.aetherj.shared.v0.MimPageManifest;

public abstract class AedbOperations implements HashMapKeyProvider
  {
  private static final String classname="AedbOperations";
  
  protected static final int cn_note_len=100;
  protected static final int col_len_1MB=1024*1024;
  protected static final int col_len_10Mb=10*1024*1024;
  protected static final int col_len_100MB=100*1024*1024;
  
  protected final Stat stat;
  protected final MimEntity dbaseEntity;

  private final PrintlnProvider println;

  /**
   * The Entity uniquely identify what this operation is about
   */
  public AedbOperations ( Stat stat, PrintlnProvider println, MimEntity mimEntity )
    {
    this.stat = stat;
    this.println = println;
    this.dbaseEntity = mimEntity;
    }

  protected final void println(String message)
    {
    println.println(message);
    }
  
  protected final void println(String message, Exception exc)
    {
    println.println(message,exc);
    }

  @Override
  public String getHashMapKey()
    {
    return dbaseEntity.getValue();
    }
  
  public int getRowCount (AeDbase dbase) throws SQLException
    {
    String query = "SELECT COUNT(*) FROM "+getTableName();
  
    Dprepared prepared=dbase.getPreparedStatement(query);
    Brs ars = prepared.executeQuery();

    Integer risul = 0;
    
    if ( ars.next() ) 
      risul=ars.getInteger(1);
    
    ars.close();
    
    return risul.intValue();
    }
  
  public final int deleteDbaseRow (AeDbase dbase, Integer row_id) throws SQLException
    {
    String query = "DELETE FROM "+getTableName()+" WHERE "+getPrimaryColName()+"=?";
    
    println(classname+".deleteDbaseRow: entity="+dbaseEntity+" row_id="+row_id);

    MimDprepared prepared = dbase.getPreparedStatement(query);
    prepared.setInt(1, row_id);
    int risul= prepared.executeUpdate();
    prepared.close(); 
     
    
    return risul;
    }

    
  /**
   * Subclasses must tell to the rest of the system what is the main "table".
   * One of the main use of the main table is to perform delete, an assumption is that it is really a table..
   * @return a String that will be used to perform delete on records, can be null meaning no delete.
   */
  protected abstract String getTableName ();

  /**
   * Subclasses must tell me what is the primary column name, it will be used to mark such column as primary
   * when lists or details are returned.
   * @return a String representing the column name or an exception if this is not available.
   */
  protected abstract String getPrimaryColName ();
  
  
  /**
   * Subclasses MUST tell to the rest of the system what is the name of the view that will be used for listing.
   * @return a string with a view that should be used to do the listing.
   */
  protected abstract String getViewName ();
  
  /**
   * Subclasses that wish to initialize things once all classes are setup should do something on this one.
   * It will be called after ALL handlers have been setup but before dbase begins operate.
   * What you typically do is to retrieve references to objects that you need to work, Eg: storico 
   */
  public void initialize()
    {
    }
  
  /**
   * Attempt to save the given object
   * Normally the primary if of the object created is returned
   * However, it may happen .... because addresses have a middle storage...
   */
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws Exception
    {
    if ( obj == null )
      throw new IllegalArgumentException("dbaseSave obj is null");
    
    throw new IllegalArgumentException("dbaseSave is NOT implemented for "+obj.getClass());
    }
  
  
  public void dbaseSaveManifest ( AeDbase dbase, MimPageManifest manifest)
    {
    }
  
  
  }
