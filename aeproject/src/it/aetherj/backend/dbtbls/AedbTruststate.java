/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;
import java.util.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

public final class AedbTruststate extends AedbMimOperations<MimTruststate> 
  {
  private static final String classname="AedbTruststate";
  
  public static final MimEntity mimEntity = MimEntityList.aetrustate;
  
  public static final String cn_tbl         = "aetste_tbl";
  public static final String cn_id          = "aetste_id";

  public static final String cn_fingerprint = "aetste_fingerprint";
  public static final String cn_creation    = "aetste_creation";
  public static final String cn_pow         = "aetste_pow";
  public static final String cn_signature   = "aetste_signature";
  
  public static final String cn_target      = "aetste_target";       // target what ?
  public static final String cn_typeclass   = "aetste_typeclass";
  public static final String cn_type        = "aetste_type";
  
  public static final String cn_domain      = "aetste_domain";
  public static final String cn_owner_id    = "aetste_owner_id";   // this thread is owned by this user
  public static final String cn_entity_v    = "aetste_entity_v";
  public static final String cn_realm       = "aetste_realm";
 
  public static final String cn_updla_time  = "aetste_updla_time";
  public static final String cn_updla_pow   = "aetste_updla_pow";
  public static final String cn_updla_sign  = "aetste_updla_sign";

  public static final String cn_note        = "aetste_note";

  public static final String cn_view        = "aetste_view";

  private AedbUkey   aedbUkey;      // needed to manage the pki relations
  private AedbWanted aedbWanted;   // needed to add wanted info on users

  
  public AedbTruststate ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    aedbUkey   = stat.aedbFactory.aedbUpki;
    aedbWanted = stat.aedbFactory.aedbWanted;
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }
  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }

  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 
  
  @Override
  public Integer getPrimaryId (AeDbase dbase, MimFingerprint srch_val ) throws SQLException
    {
    String query="SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_fingerprint+"=?";
    MimDprepared prepared=dbase.getPreparedStatement(query);
    prepared.setValue(1, srch_val);
    Brs ars = prepared.executeQuery();
    
    Integer risul = null;
    
    if ( ars.next() ) 
      risul=ars.getInteger(1);
    
    ars.close();
    
    return risul;
    }

  public Iterator<MimTruststate> selectForCache ( AeDbase dbase ) throws SQLException
    {
    String query = "SELECT "+cn_view+".* "+
    " FROM "+cn_view+
    " ORDER BY lastu";
    
    Dprepared prepared=dbase.getPreparedStatement(query.toString());
    
    return new MimTruststateIterator(prepared.executeQuery());
    }

  @Override
  public Iterator<MimTruststate> selectForServlet ( AeDbase dbase, MimFilterTimestamp filter, int limit ) throws SQLException
    {
    MimTimestamp start = filter.getStart();
    MimTimestamp end = filter.getEnd();
    
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT "+cn_view+".* "); 
    query.append(" FROM "+cn_view );
    query.append(" WHERE "+cn_signature+" IS NOT NULL ");
    if ( ! start.isNull() ) query.append(" AND lastu >= ? ");
    if ( ! end.isNull() )  query.append(" AND lastu <= ? ");
    query.append(" ORDER BY lastu");
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    
    if ( ! start.isNull() ) prepared.setValue(insIndex++, start);
    if ( ! end.isNull() ) prepared.setValue(insIndex++, end);
    prepared.setInt(insIndex++, limit);

    return new MimTruststateIterator(prepared.executeQuery());
    }

  @Override
  public Iterator<MimTruststate> selectForServlet ( AeDbase dbase, MimFilterFingerprint filter, int limit ) throws SQLException
    {
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT * ");
    query.append(" FROM "+cn_view );
    query.append(" WHERE "+cn_owner_id+" IS NOT NULL ");
    query.append(" AND "+cn_fingerprint+" IN ");
    query.append(newSqlInlist(limit));
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    insIndex = addSqlInvalues(prepared, insIndex, filter, limit);
    prepared.setInt(insIndex++, limit);

    return new MimTruststateIterator(prepared.executeQuery());
    }

  
  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj instanceof MimTruststate )
      {
      MimTruststate row = (MimTruststate)obj;
      
      Integer prim_id=getPrimaryId(dbase, row.fingerprint);
      
      if ( prim_id == null )
        prim_id=dbaseInsert(dbase, row);
      else 
        dbaseUpdate(dbase, prim_id, row);
    
      // make sure that I do not want this anymore
      aedbWanted.dbaseDelete(dbase, mimEntity, row.fingerprint);

      return prim_id;
      }

    println(classname+".dbaseSave: unsupported class "+obj.getClass());
    return null;
    }
  
  public Integer dbaseUpdate ( AeDbase dbase, Integer prim_id, MimTruststate row) throws SQLException
    {
    if ( ! isMimRowAfterDbaseRow (dbase, prim_id, row))
      return prim_id;

    Integer owner_id = aedbUkey.getPrimaryId(dbase, row.owner);
    
    if ( owner_id == null )
      owner_id = aedbUkey.dbaseInsert(dbase, row.owner, row.owner_publickey);

    String query="UPDATE "+cn_tbl+" SET "+
        cn_creation+"=?,"+
        cn_pow+"=?,"+
        cn_signature+"=?,"+
        cn_target+"=?,"+
        cn_owner_id+"=?,"+
        cn_typeclass+"=?,"+
        cn_type+"=?,"+
        cn_domain+"=?,"+
        cn_entity_v+"=?,"+
        cn_realm+"=?,"+
        cn_updla_time+"=?,"+
        cn_updla_pow+"=?,"+
        cn_updla_sign+"=?,"+
        cn_note+"=?"+
    " WHERE "+cn_id+"=?";
    
    MimDprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setValue(insIndex++,row.signature);
    prepared.setValue(insIndex++,row.target);
    prepared.setInt(insIndex++,owner_id);
    prepared.setInt(insIndex++,row.typeclass);
    prepared.setInt(insIndex++,row.type);
    prepared.setValue(insIndex++,row.domain);
    prepared.setInt(insIndex++,row.entity_version);
    prepared.setValue(insIndex++,row.realm_id);
    prepared.setValue(insIndex++,row.last_update);
    prepared.setValue(insIndex++,row.update_proof_of_work);
    prepared.setValue(insIndex++,row.update_signature);
    prepared.setString(insIndex++,null);
    prepared.setInt(insIndex++,prim_id);
    
    prepared.executeUpdate();
    prepared.close();
    
    return prim_id;
    }
  
  public Integer dbaseInsert ( AeDbase dbase, MimTruststate row) throws SQLException
    {
    Integer owner_id = aedbUkey.getPrimaryId(dbase, row.owner);
    
    if ( owner_id == null )
      owner_id = aedbUkey.dbaseInsert(dbase, row.owner, row.owner_publickey);
        
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_creation+","+
       cn_pow+","+
       cn_signature+","+
       cn_target+","+
       cn_owner_id+","+
       cn_typeclass+","+
       cn_type+","+
       cn_domain+","+
       cn_entity_v+","+
       cn_realm+","+
       cn_updla_time+","+
       cn_updla_pow+","+
       cn_updla_sign+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.fingerprint);
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setValue(insIndex++,row.signature);
    prepared.setValue(insIndex++,row.target);
    prepared.setInt(insIndex++,owner_id);
    prepared.setInt(insIndex++,row.typeclass);
    prepared.setInt(insIndex++,row.type);
    prepared.setValue(insIndex++,row.domain);
    prepared.setInt(insIndex++,row.entity_version);
    prepared.setValue(insIndex++,row.realm_id);
    prepared.setValue(insIndex++,row.last_update);
    prepared.setValue(insIndex++,row.update_proof_of_work);
    prepared.setValue(insIndex++,row.update_signature);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
  
  @Override
  public Brs selectUsingPrimaryId ( AeDbase dbase, Integer row_id ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_id+"=?";
    Dprepared prepared = dbase.getPreparedStatement(query);
    prepared.setInt(1,row_id);
    return prepared.executeQuery();
    }
  
  @Override
  public MimTruststate getMimMesage ( Brs ars )
    {
    MimTruststate risul = new MimTruststate();
    
    risul.fingerprint = new MimFingerprint(ars.getBytes(cn_fingerprint));
    risul.creation    = new MimTimestamp(ars.getTimestamp(cn_creation));
    risul.proof_of_work = new MimPowValue(ars.getString(cn_pow));
    risul.signature   = new MimSignature(ars.getBytes(cn_signature));
    
    risul.target = new MimFingerprint(ars.getBytes(cn_target));
    risul.owner = new MimFingerprint(ars.getBytes(AedbUkey.cn_fingerprint));
    risul.owner_publickey = new MimPublicKey(ars.getBytes(AedbUkey.cn_ukey_public));

    risul.typeclass = ars.getInteger(cn_typeclass, MimTruststate.def_typeclass);
    risul.type      = ars.getInteger(cn_type, MimTruststate.def_type);

    risul.domain = new MimFingerprint(ars.getBytes(cn_domain));

    risul.entity_version = ars.getInteger(cn_entity_v, MimTruststate.def_entity_v);
    risul.realm_id = new MimFingerprint(ars.getBytes(cn_realm));
    
    risul.last_update = new MimTimestamp(ars.getTimestamp(cn_updla_time));
    risul.update_proof_of_work = new MimPowValue(ars.getString(cn_updla_pow));
    risul.update_signature = new MimSignature(ars.getBytes(cn_updla_sign));

    return risul;
    }

private class MimTruststateIterator implements Iterator<MimTruststate> 
  { 
  private final Brs ars;
  private boolean haveNext=false;
  
  MimTruststateIterator (Brs ars) 
    { 
    this.ars = ars; 
    } 
    
  public boolean hasNext() 
    {
    if ( ars.isClosed()) return false;
    
    // we already know we have a next, you need to read it using next()
    if ( haveNext ) return true;
    
    // do we have a next ?
    haveNext = ars.next();
    
    if ( ! haveNext )
      ars.close();
    
    return haveNext;
    } 
    
  @Override
  public MimTruststate next() 
    {
    if ( ! haveNext )
      throw new NoSuchElementException("You need to call hasNext() first");
    
    haveNext = false;
    
    return getMimMesage(ars);    
    }
  } 
  
  
  
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addProvableHeader(cn_id, cn_fingerprint, cn_creation, cn_pow, cn_signature);
    
    addSchemaRow(new SchemaRowVarbinary(cn_target,MimFingerprint.bytes_len));
    addSchemaRow(new SchemaRowInteger(cn_owner_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_typeclass));
    addSchemaRow(new SchemaRowInteger(cn_type));
    addSchemaRow(new SchemaRowVarbinary(cn_domain,MimFingerprint.bytes_len));
    addSchemaRow(new SchemaRowInteger(cn_entity_v));
    addSchemaRow(new SchemaRowVarbinary(cn_realm,MimFingerprint.bytes_len));
    
    addSchemaRow(new SchemaRowTimestamp(cn_updla_time));
    addSchemaRow(new SchemaRowVarchar(cn_updla_pow,MimPowValue.pow_ascii_len));
    addSchemaRow(new SchemaRowVarbinary(cn_updla_sign,MimSignature.signature_len));
    
    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }


  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }
 
  @Override
  public void createView ( ExecuteProvider db )
    {
    String query="CREATE VIEW "+cn_view+" AS "+ 
        " SELECT aetste_tbl.*,ukey_public,ukey_fingerprint, "+
        " COALESCE("+cn_updla_time+","+cn_creation+") AS lastu "+
        " FROM "+cn_tbl+
        " LEFT OUTER JOIN ukey_tbl ON aetste_owner_id=ukey_id ";
    
    db.executeDbaseDone(query);
    }
  
  @Override
  public void dropConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT aetste_owner_ref");
    }
    
  
  @Override
  public void createConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT aetste_owner_ref "+
       "FOREIGN KEY ("+cn_owner_id+") REFERENCES "+AedbUkey.cn_tbl+" ("+AedbUkey.cn_id+") ON DELETE CASCADE ");

    }

  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX aetste_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX aetste_fingerprint_index");
    }  
  
  
  }
      
      
  } 

