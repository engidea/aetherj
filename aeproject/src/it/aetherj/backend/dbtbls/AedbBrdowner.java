/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.MimBoardOwner;

/**
 * This is a MIM User, possibly an Owner and BoardOwner
 * It is not clear how one goes from the fingerprint to the public/private 

type BoardOwner struct {
  KeyFingerprint Fingerprint `json:"key_fingerprint"` // Fingerprint of the key the ownership is associated to.
  Expiry         Timestamp   `json:"expiry"`          // When the ownership expires.
  Level          uint8       `json:"level"`           // mod(1)
}

 * 
 */
public final class AedbBrdowner extends AedbOperations
  {
  private static final String classname="AedbBrdowner";
  
  public static final MimEntity mimEntity = MimEntityList.brdowner;
  
  public static final String cn_tbl         = "brdowner_tbl";
  public static final String cn_id          = "brdowner_id";
  public static final String cn_board_id    = "brdowner_board_id";      // this row belongs to this board
  public static final String cn_ukey_id     = "brdowner_ukey_id";       // This user is board owner of the board
  public static final String cn_ukey_fprint = "brdowner_fingerprint";   // possible duplicate if I do not have the user
  public static final String cn_expiry      = "brdowner_expiry";   
  public static final String cn_level       = "brdowner_level";
  public static final String cn_note        = "brdowner_note";
  
  private static final String cn_view       = "brdowner_view";
  
  public AedbBrdowner ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }

  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 
    
  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    /*
    if ( obj == null ) 
      return null;
    
    if (! ( obj instanceof MimBoardOwner) )
      {
      println.println(classname+"dbaseSave: unsupported class "+obj.getClass());
      return null;
      }
    
    MimBoardOwner row = (MimBoardOwner)obj;
    
    Integer prim_id=getPrimaryId(dbase, row.key_fingerprint);
    
    if ( prim_id == null )
      return dbaseInsert(dbase, row);
    else 
      return dbaseUpdate(dbase, prim_id, row);
      */
    return null;
    }
  
  public Integer dbaseUpdate ( AeDbase dbase, Integer prim_id, MimBoardOwner row) throws SQLException
    {
    return prim_id;
    }
  
  public Integer dbaseInsert ( AeDbase dbase, MimBoardOwner row) throws SQLException
    {
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_ukey_fprint+","+
       cn_expiry+","+
       cn_level+
       " ) VALUES ( ?,?,? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.key_fingerprint);
    prepared.setValue(insIndex++,row.expiry);
    prepared.setInt(insIndex++,row.level);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
    
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary(); 
    addSchemaRow(new SchemaRowInteger(cn_board_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_ukey_id));
    addSchemaRow(new SchemaRowVarbinary(cn_ukey_fprint,MimFingerprint.bytes_len));
    addSchemaRow(new SchemaRowInteger(cn_expiry));
    addSchemaRow(new SchemaRowInteger(cn_level));
    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }

  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+cn_view+" AS "+ 
        "SELECT brdowner_tbl.* FROM brdowner_tbl");
    }
  }
      
      
  } 

