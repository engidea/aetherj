/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;
import it.aetherj.shared.AeJson;
import it.aetherj.shared.v0.*;

/**
 * Having cache on filesystem is possibly easy but you end up with dealing with the "unique filename" issue
 * Hopefully, this is simpler to manage in the long run
 * It mirrors the logic $HOME/.cache/Air Labs/Aether/v0/....
 */
public final class AedbCache extends AedbOperations
  {
  private static final String classname="AedbCache";
  
  public static final MimEntity mimEntity = MimEntityList.aecache;
  
  public static final String cn_tbl        = "acache_tbl";
  public static final String cn_id         = "acache_id";
  public static final String cn_entity     = "acache_entity";       // this is what, address, board, keys....
  public static final String cn_page       = "acache_page";         // normally 0, may be 1,2,3, depending on .. ??
  public static final String cn_index      = "acache_index";        // holds the index for the given entity , also a CLOB
  public static final String cn_manifest   = "acache_manifest";  
  public static final String cn_content    = "acache_content";      // this must be a CLOB, character large object 
  public static final String cn_note       = "acache_note";
 
  private static final String cn_view      = "acache_view";
  
  public AedbCache ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }
  
  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 

  /**
   * You can drop the table on exit, it will be recreated on bootstrap
   * @param dbase
   */
  public void dropTable ( AeDbase dbase )
    {
    dbase.execute("DROP VIEW "+cn_view);
    dbase.execute("DROP TABLE "+cn_tbl);
    }
  
  /**
   * Should insert into cache the given payload
   * From payload you get the entity name 
   * The content of the cache is the serialization of the payload
   * @param dbase
   * @param pay_row the whole content to save, already signed
   * @return
   * @throws JsonProcessingException 
   * @throws SQLException 
   */
  public Integer dbaseInsert(AeDbase dbase, MimPayload pay_row, MimPayload pay_mfest, MimPayload pay_subi ) throws JsonProcessingException, SQLException
    {
    MimEntity entity = pay_row.entity;
    
    AeJson om = new AeJson();
    String pay_row_s   = om.writeValueAsString(pay_row);
    String pay_mfest_s = om.writeValueAsString(pay_mfest);
    String pay_subi_s   = om.writeValueAsString(pay_subi);

    String query = "INSERT INTO "+cn_tbl+" ( "+
        cn_entity+","+
        cn_content+","+
        cn_manifest+","+
        cn_index+","+
        cn_note+
        " ) VALUES ( ?,?,?,?,? )";
      
     MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
     int insIndex=1;
     prepared.setValue(insIndex++,entity);
     prepared.setString(insIndex++,pay_row_s);
     prepared.setString(insIndex++,pay_mfest_s);
     prepared.setString(insIndex++,pay_subi_s);
     prepared.setString(insIndex++, null);
     
     Brs ars = prepared.executeUpdateReturnKeys();

     Integer primary_id=null;
     
     if ( ars.next() ) 
       primary_id = ars.getInteger(1);
     
     ars.close();  
     prepared.close();
     return primary_id;
    }
  
  /**
   * This save the index for the given entity
   * @param dbase
   * @param entity
   * @param index
   * @return
   * @throws SQLException 
   * @throws JsonProcessingException 
   * @throws CloneNotSupportedException 
   */
  public Integer dbaseInsertIndex(AeDbase dbase, MimEntity entity, MimItemsArray<MimResultsCache> index) throws SQLException, JsonProcessingException, CloneNotSupportedException
    {
    MimPayload payload = stat.aeParams.newMimPayload(entity);
    
    payload.caching.pregenerated=true;
    payload.results = index.toArray();
     
    CryptoEddsa25519 crypto = new CryptoEddsa25519();
    payload.signPayload(crypto, stat.aeParams.getPrivateKey());
    
    AeJson om = new AeJson();
    String source = om.writeValueAsString(payload);
    
    String query = "INSERT INTO "+cn_tbl+" ( "+
        cn_entity+","+
        cn_index+","+
        cn_note+
        " ) VALUES ( ?,?,? )";
      
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setString(insIndex++,entity.getValue());
    prepared.setString(insIndex++,source);
    prepared.setString(insIndex++, null);
    
    Brs ars = prepared.executeUpdateReturnKeys();
     Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();
    
    return primary_id;
    }
  
  /**
   * Needed before regenerating the cache, delete all cache of the given entity
   * @param dbase
   * @param entity
   * @return the number of rows deleted
   * @throws SQLException 
   */
  public int dbaseDelete (AeDbase dbase, MimEntity entity ) throws SQLException
    {
    String query = "DELETE FROM "+cn_tbl+" WHERE "+cn_entity+"=?";
    
    MimDprepared prepared=dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setString(insIndex++, entity.getValue());

    int risul = prepared.executeUpdate();
    prepared.close();
    
    return risul;
    }
    
  /**
   * This is the index for the whole entity
   * @param dbase
   * @param entity
   * @return
   * @throws SQLException
   */
  public String getEntityIndex (AeDbase dbase, MimEntity entity ) throws SQLException
    {
    String query="SELECT "+cn_index+" FROM "+cn_tbl+" WHERE "+cn_entity+"=? "+
    " AND "+cn_manifest+" IS NULL "+
    " AND "+cn_content+" IS NULL ";

    MimDprepared prepared=dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setString(insIndex++, entity.getValue());
    Brs ars = prepared.executeQuery();
    
    String risul=null;
    
    if ( ars.next() ) 
      risul=ars.getString(cn_index); 
    
    ars.close();
    
    return risul;
    }

  /**
   * This will return a reader, or something like that...
   * @param dbase
   * @param entity
   * @param primary_id
   * @return
   * @throws SQLException
   */
  public String getContent (AeDbase dbase, MimEntity entity, String what, Integer primary_id ) throws SQLException
    {
    String query="SELECT "+what+" FROM "+cn_tbl+" WHERE "+cn_id+"=? ";

    MimDprepared prepared=dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, primary_id);
    Brs ars = prepared.executeQuery();
    
    String risul=null;
    
    if ( ars.next() ) 
      risul=ars.getString(what);
    
    ars.close();
    
    return risul;
    }
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowVarchar(cn_entity,MimEntity.entity_len_max)).setNotNullable();  
    addSchemaRow(new SchemaRowInteger(cn_page));
    addSchemaRow(new SchemaRowClob(cn_index,col_len_10Mb));
    addSchemaRow(new SchemaRowVarchar(cn_manifest,col_len_1MB));
    addSchemaRow(new SchemaRowClob(cn_content,col_len_100MB));
    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }
  
  @Override
  public String getTableName ()
    {
    return cn_tbl;
    } 

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+cn_view+" AS "+ 
      "SELECT * FROM "+cn_tbl);
    }
      
  }
      
      
  } 

