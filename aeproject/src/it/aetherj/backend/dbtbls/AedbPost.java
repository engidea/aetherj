/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;
import java.util.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * Some object in the system needs to have a couple of private and public key, for whatever reason
 * So, it refers to this table with a specific ID to refer to it
 * Since it is used to identify somebody, there should be just ONE dbepki for each object....
 * Or, if you want two of them... well, have to references to it
 */
public final class AedbPost extends AedbMimOperations <MimPost>
  {
  private static final String classname="AedbPost";
  
  public static final MimEntity mimEntity = MimEntityList.aepost;
  
  public static final String cn_tbl         = "aepost_tbl";
  public static final String cn_id          = "aepost_id";

  public static final String cn_fingerprint = "aepost_fingerprint";
  public static final String cn_creation    = "aepost_creation";
  public static final String cn_pow         = "aepost_pow";
  public static final String cn_signature   = "aepost_signature";
  
  public static final String cn_board_id    = "aepost_board_id";   // this post refers to this board, redundant, info present in the thread..
  public static final String cn_thread_id   = "aepost_thread_id";

  public static final String cn_parent_id   = "aepost_parent_id";  // refers to this post, may be null
  public static final String cn_parent_fp   = "aepost_parent_fp";  // and refers to this parent post
  
  public static final String cn_body        = "aepost_body";
  public static final String cn_owner_id    = "aepost_owner_id";   // this thread is owned by this user
  public static final String cn_entity_v    = "aepost_entity_v";
  public static final String cn_realm       = "aepost_realm";
 
  public static final String cn_updla_time  = "aepost_updla_time";
  public static final String cn_updla_pow   = "aepost_updla_pow";
  public static final String cn_updla_sign  = "aepost_updla_sign";
  
  public static final String cn_votes_cnt   = "aepost_votes_cnt";   // how many votes up - down

  public static final String cn_note        = "aepost_note";

  public static final String cn_view        = "aepost_view";

  private AedbUkey   aedbUkey;      // needed to manage the pki relations
  private AedbBoard  aedbBoard;  // needed to manage the board relations
  private AedbThread aedbThread;  // 
  private AedbWanted aedbWanted;   // needed to add wanted info on users

  
  public AedbPost ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    aedbUkey   = stat.aedbFactory.aedbUpki;
    aedbBoard  = stat.aedbFactory.aedbBoard;
    aedbThread = stat.aedbFactory.aedbThread;
    aedbWanted = stat.aedbFactory.aedbWanted;
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }
  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }

  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 
  
  @Override
  public Integer getPrimaryId (AeDbase dbase, MimFingerprint srch_val ) throws SQLException
    {
    String query="SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_fingerprint+"=?";
    MimDprepared prepared=dbase.getPreparedStatement(query);
    prepared.setValue(1, srch_val);
    Brs ars = prepared.executeQuery();
    
    Integer risul = null;
    
    if ( ars.next() ) 
      risul=ars.getInteger(1);
    
    ars.close();
    
    return risul;
    }

  @Override
  public Iterator<MimPost> selectForCache ( AeDbase dbase ) throws SQLException
    {
    String query = "SELECT * "+
    " FROM "+cn_view+
    " ORDER BY lastu";
    
    Dprepared prepared=dbase.getPreparedStatement(query.toString());
    
    return new MimPostIterator(prepared.executeQuery());
    }

  @Override
  public Iterator<MimPost> selectForServlet ( AeDbase dbase, MimFilterTimestamp filter, int limit ) throws SQLException
    {
    MimTimestamp start = filter.getStart();
    MimTimestamp end = filter.getEnd();
    
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT * ");
    query.append(" FROM "+cn_view );
    query.append(" WHERE "+cn_signature+" IS NOT NULL ");
    if ( ! start.isNull() ) query.append(" AND lastu >= ? ");
    if ( ! end.isNull() )  query.append(" AND lastu <= ? ");
    query.append(" ORDER BY lastu");
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    
    if ( ! start.isNull() ) prepared.setValue(insIndex++, start);
    if ( ! end.isNull() ) prepared.setValue(insIndex++, end);
    prepared.setInt(insIndex++, limit);

    return new MimPostIterator(prepared.executeQuery());
    }

  @Override
  public Iterator<MimPost> selectForServlet ( AeDbase dbase, MimFilterFingerprint filter, int limit ) throws SQLException
    {
    StringBuilder query = new StringBuilder(400);
    query.append("SELECT * ");
    query.append(" FROM "+cn_view );
    query.append(" WHERE "+cn_signature+" IS NOT NULL ");
    query.append(" AND "+cn_fingerprint+" IN ");
    query.append(newSqlInlist(limit));
    query.append(" LIMIT ?");
    
    MimDprepared prepared = dbase.getPreparedStatement(query.toString());
    
    int insIndex=1;
    insIndex = addSqlInvalues(prepared, insIndex, filter, limit);
    prepared.setInt(insIndex++, limit);

    return new MimPostIterator(prepared.executeQuery());
    }

  
  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj instanceof MimPost )
      {
      MimPost row = (MimPost)obj;
      
      Integer prim_id=getPrimaryId(dbase, row.fingerprint);
      
      if ( prim_id == null )
        prim_id=dbaseInsert(dbase, row);
      else 
        dbaseUpdate(dbase, prim_id, row);
    
      // make sure that I do not want this anymore
      aedbWanted.dbaseDelete(dbase, mimEntity, row.fingerprint);

      return prim_id;
      }

    println(classname+"dbaseSave: unsupported class "+obj.getClass());
    return null;
    }
  
  public Integer dbaseUpdate ( AeDbase dbase, Integer prim_id, MimPost row) throws SQLException
    {
    if ( ! isMimRowAfterDbaseRow (dbase, prim_id, row))
      return prim_id;

    Integer owner_id = aedbUkey.getPrimaryId(dbase, row.owner);
    
    if ( owner_id == null )
      owner_id = aedbUkey.dbaseInsert(dbase, row.owner, row.owner_publickey);
    
    Integer board_id = aedbBoard.getPrimaryId(dbase, row.board); 
    
    if ( board_id == null )
      board_id = aedbBoard.dbaseInsert(dbase, row.board );
    
    Integer thread_id = aedbThread.getPrimaryId(dbase, row.thread);
    
    if ( thread_id == null )
      thread_id = aedbThread.dbaseInsert(dbase, row.thread );
    
    Integer parent_id = getPrimaryId(dbase, row.parent);
    
    String query="UPDATE "+cn_tbl+" SET "+
        cn_creation+"=?,"+
        cn_pow+"=?,"+
        cn_signature+"=?,"+
        cn_board_id+"=?,"+
        cn_thread_id+"=?,"+
        cn_parent_id+"=?,"+
        cn_parent_fp+"=?,"+
        cn_body+"=?,"+
        cn_entity_v+"=?,"+
        cn_realm+"=?,"+
        cn_owner_id+"=?,"+
        cn_updla_time+"=?,"+
        cn_updla_pow+"=?,"+
        cn_updla_sign+"=?,"+
        cn_note+"=?"+
    " WHERE "+cn_id+"=?";
    
    MimDprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setValue(insIndex++,row.signature);
    prepared.setInt(insIndex++,board_id);
    prepared.setInt(insIndex++,thread_id);
    prepared.setInt(insIndex++,parent_id);
    prepared.setValue(insIndex++,row.parent);
    prepared.setString(insIndex++,row.body);
    prepared.setInt(insIndex++,row.entity_version);
    prepared.setValue(insIndex++,row.realm_id);
    prepared.setInt(insIndex++,owner_id);
    prepared.setValue(insIndex++,row.last_update);
    prepared.setValue(insIndex++,row.update_proof_of_work);
    prepared.setValue(insIndex++,row.update_signature);
    prepared.setString(insIndex++,null);
    prepared.setInt(insIndex++,prim_id);
    
    prepared.executeUpdate();
    prepared.close();
    
    return prim_id;
    }
  
  public Integer dbaseInsert ( AeDbase dbase, MimPost row) throws SQLException
    {
    Integer owner_id = aedbUkey.getPrimaryId(dbase, row.owner);
    
    if ( owner_id == null )
      owner_id = aedbUkey.dbaseInsert(dbase, row.owner, row.owner_publickey);
    
    Integer board_id = aedbBoard.getPrimaryId(dbase, row.board); 
    
    if ( board_id == null )
      board_id = aedbBoard.dbaseInsert(dbase, row.board );
    
    Integer thread_id = aedbThread.getPrimaryId(dbase, row.thread);
    
    if ( thread_id == null )
      thread_id = aedbThread.dbaseInsert(dbase, row.thread );
    
    Integer parent_id = getPrimaryId(dbase, row.parent);

    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_creation+","+
       cn_pow+","+
       cn_signature+","+
       cn_board_id+","+
       cn_thread_id+","+
       cn_parent_id+","+
       cn_parent_fp+","+
       cn_body+","+
       cn_entity_v+","+
       cn_realm+","+
       cn_owner_id+","+
       cn_updla_time+","+
       cn_updla_pow+","+
       cn_updla_sign+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.fingerprint);
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setValue(insIndex++,row.signature);
    prepared.setInt(insIndex++,board_id);
    prepared.setInt(insIndex++,thread_id);
    prepared.setInt(insIndex++,parent_id);
    prepared.setValue(insIndex++,row.parent);
    prepared.setString(insIndex++,row.body);
    prepared.setInt(insIndex++,row.entity_version);
    prepared.setValue(insIndex++,row.realm_id);
    prepared.setInt(insIndex++,owner_id);
    prepared.setValue(insIndex++,row.last_update);
    prepared.setValue(insIndex++,row.update_proof_of_work);
    prepared.setValue(insIndex++,row.update_signature);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }

  public Brs selectUsingFingerprint ( AeDbase dbase, MimFingerprint fprint ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_tbl+" WHERE "+cn_fingerprint+"=?";
    MimDprepared prepared = dbase.getPreparedStatement(query);
    prepared.setValue(1,fprint);
    return prepared.executeQuery();
    }

  public Brs selectUsingThreadId ( AeDbase dbase, int thread_id ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_thread_id+"=? ORDER BY "+cn_lastu+" DESC ";
    Dprepared prepared = dbase.getPreparedStatement(query);
    prepared.setInt(1,thread_id);
    return prepared.executeQuery();
    }
  
  public Brs selectUsingTimeLimit ( AeDbase dbase, Date w_utime, int w_limit ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+
       cn_lastu+" < ? "+
       " ORDER BY "+cn_lastu+" DESC "+
       " LIMIT ?";
    
    MimDprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setDate(insIndex++,w_utime);
    prepared.setInt(insIndex++,w_limit);
    return prepared.executeQuery();
    }

  @Override
  public Brs selectUsingPrimaryId ( AeDbase dbase, Integer row_id ) throws SQLException
    {
    String query = "SELECT * FROM "+cn_view+" WHERE "+cn_id+"=? ";
    Dprepared prepared=dbase.getPreparedStatement(query);
    prepared.setInt(1, row_id);
    return prepared.executeQuery();
    }

  /**
   * It may return an empty Post but not a null one
   */
  public MimPost getMimMesage (AeDbase dbase, Integer post_id) throws SQLException
    {
    Brs ars = selectUsingPrimaryId (dbase,post_id);
    
    MimPost risul = new MimPost();
    
    if ( ars.next() )
      risul = getMimMesage(ars);
    
    ars.close();
    
    return risul;
    }

  
  public MimPost getMimMesage ( Brs ars )
    {
    MimPost risul = new MimPost();
    
    risul.fingerprint = new MimFingerprint(ars.getBytes(cn_fingerprint));
    risul.creation    = new MimTimestamp(ars.getTimestamp(cn_creation));
    risul.proof_of_work = new MimPowValue(ars.getString(cn_pow));
    risul.signature   = new MimSignature(ars.getBytes(cn_signature));
    
    risul.board   = new MimFingerprint(ars.getBytes(AedbBoard.cn_fingerprint));
    risul.thread  = new MimFingerprint(ars.getBytes(AedbThread.cn_fingerprint));
    risul.parent  = new MimFingerprint(ars.getBytes(cn_parent_fp));

    risul.body    = ars.getString(cn_body);

    risul.owner = new MimFingerprint(ars.getBytes(AedbUkey.cn_fingerprint));
    risul.owner_publickey = new MimPublicKey(ars.getBytes(AedbUkey.cn_ukey_public));
    risul.entity_version  = ars.getInteger(cn_entity_v, MimPost.def_entity_v);
    risul.realm_id        = new MimFingerprint(ars.getBytes(cn_realm));
    
    risul.last_update     = new MimTimestamp(ars.getTimestamp(cn_updla_time));
    risul.update_proof_of_work = new MimPowValue(ars.getString(cn_updla_pow));
    risul.update_signature = new MimSignature(ars.getBytes(cn_updla_sign));

    return risul; 
    }

private class MimPostIterator implements Iterator<MimPost> 
  { 
  private final Brs ars;
  private boolean haveNext=false;
  
  MimPostIterator (Brs ars) 
    { 
    this.ars = ars; 
    } 
    
  public boolean hasNext() 
    {
    if ( ars.isClosed()) return false;
    
    // we already know we have a next, you need to read it using next()
    if ( haveNext ) return true;
    
    // do we have a next ?
    haveNext = ars.next();
    
    if ( ! haveNext )
      ars.close();
    
    return haveNext;
    } 
    
  @Override
  public MimPost next() 
    {
    if ( ! haveNext )
      throw new NoSuchElementException("You need to call hasNext() first");
    
    haveNext = false;
    
    return getMimMesage(ars);    
    }
  } 
  
  
  
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addProvableHeader(cn_id, cn_fingerprint, cn_creation, cn_pow, cn_signature);
    
    addSchemaRow(new SchemaRowInteger(cn_board_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_thread_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_parent_id));  // TODO if null, it will be filled up by the garbage collector, if _fp is not null
    addSchemaRow(new SchemaRowVarbinary(cn_parent_fp,MimFingerprint.bytes_len));
    addSchemaRow(new SchemaRowVarchar(cn_body,65535));
    addSchemaRow(new SchemaRowInteger(cn_owner_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_entity_v));
    addSchemaRow(new SchemaRowVarbinary(cn_realm,MimFingerprint.bytes_len));
    
    addSchemaRow(new SchemaRowTimestamp(cn_updla_time));
    addSchemaRow(new SchemaRowVarchar(cn_updla_pow,MimPowValue.pow_ascii_len));
    addSchemaRow(new SchemaRowVarbinary(cn_updla_sign,MimSignature.signature_len));
    
    addSchemaRow(new SchemaRowInteger(cn_votes_cnt));

    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }


  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }
 
  @Override
  public void createView ( ExecuteProvider db )
    {
    String query="CREATE VIEW "+cn_view+" AS "+ 
        " SELECT aepost_tbl.*,aeboard_fingerprint,aethread_fingerprint,ukey_public,ukey_fingerprint, "+
        " COALESCE("+cn_updla_time+","+cn_creation+") AS "+cn_lastu+
        " FROM "+cn_tbl+
        " LEFT OUTER JOIN "+AedbBoard.cn_tbl+" ON aepost_board_id="+AedbBoard.cn_id+
        " LEFT OUTER JOIN "+AedbThread.cn_tbl+" ON aepost_thread_id="+AedbThread.cn_id+
        " LEFT OUTER JOIN "+AedbUkey.cn_tbl+" ON aepost_owner_id="+AedbUkey.cn_id;
        
    db.executeDbaseDone(query);
    }
  
  @Override
  public void dropConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT aepost_owner_ref");
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT aepost_thread_ref");
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT aepost_board_ref");
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" DROP CONSTRAINT aepost_post_ref");
    }
    
  
  @Override
  public void createConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT aepost_owner_ref "+
       "FOREIGN KEY ("+cn_owner_id+") REFERENCES "+AedbUkey.cn_tbl+" ("+AedbUkey.cn_id+") ");

    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT aepost_board_ref "+
        "FOREIGN KEY ("+cn_board_id+") REFERENCES "+AedbBoard.cn_tbl+" ("+AedbBoard.cn_id+") ");

    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT aepost_thread_ref "+
        "FOREIGN KEY ("+cn_thread_id+") REFERENCES "+AedbThread.cn_tbl+" ("+AedbThread.cn_id+") ");

    db.executeDbaseDone("ALTER TABLE "+cn_tbl+" ADD CONSTRAINT aepost_post_ref "+
        "FOREIGN KEY ("+cn_parent_id+") REFERENCES "+AedbPost.cn_tbl+" ("+AedbPost.cn_id+") ");
    
    
    }

  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX post_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX post_fingerprint_index");
    }  
  
  
  }
      
      
  } 

