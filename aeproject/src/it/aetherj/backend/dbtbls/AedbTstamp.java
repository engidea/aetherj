/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.MimTimestamp;

/**
 * When a client connects to an aether server it should request all data from a given time for a given entity
 * So, it needs to remember the last successful date it has done operation
 */
public final class AedbTstamp extends AedbOperations
  {
  private static final String classname="AedbTstamp";
  
  public static final MimEntity mimEntity = MimEntityList.aetstamp;
  
  public static final String cn_tbl          = "aetst_tbl";
  public static final String cn_id           = "aetst_id";
  public static final String cn_node_id      = "aetst_node_id"; 
  public static final String cn_entity       = "aetst_entity";
  public static final String cn_receive_time = "aetst_receive_time";  
  public static final String cn_note         = "aetst_note";
 
  private static final String cn_view        = "aetst_view";
  
  public AedbTstamp ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    }

  @Override
  public final String getTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }
  
  @Override
  public final String getViewName ()
    {
    return cn_view;
    } 

  public MimTimestamp getLatestSyncTime (AeDbase dbase, Integer node_id, MimEntity entity ) throws SQLException
    {
    String query="SELECT "+cn_receive_time+" FROM "+cn_tbl+" WHERE "+cn_node_id+"=? AND "+cn_entity+"=?";

    MimDprepared prepared=dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, node_id);
    prepared.setValue(insIndex++, entity);
    Brs ars = prepared.executeQuery();
    
    Timestamp risul = null;
    
    if ( ars.next() ) 
      risul=ars.getTimestamp(cn_receive_time);
    
    ars.close();
    
    return new MimTimestamp(risul);
    }

  
  private Integer getPrimaryId ( AeDbase dbase, Integer node_id, MimEntity entity ) throws SQLException
    {
    String query="SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_node_id+"=? AND "+cn_entity+"=?";

    MimDprepared prepared=dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, node_id);
    prepared.setValue(insIndex++, entity);
    Brs ars = prepared.executeQuery();
    
    Integer risul = null;
    
    if ( ars.next() ) 
      risul=ars.getInteger(1);
    
    ars.close();
    
    return risul;

    }

  public Integer dbaseSave ( AeDbase dbase, Integer node_id, MimEntity entity ) throws SQLException
    {
    Integer row_id = getPrimaryId(dbase, node_id, entity);
    
    if ( row_id == null )
      row_id = dbaseInsert(dbase, node_id, entity );
    else
      dbaseUpdate(dbase, row_id );
    
    return row_id;
    }

  private void dbaseUpdate ( AeDbase dbase, Integer row_id ) throws SQLException
    {
    String query = "UPDATE "+cn_tbl+" SET "+cn_receive_time+"=? WHERE "+cn_id+"=?";
    
    MimDprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setTimestamp(insIndex++);
    prepared.setInt(insIndex++,row_id);
    
    prepared.executeUpdate();
    prepared.close();
    }
  
  private Integer dbaseInsert ( AeDbase dbase, Integer node_id, MimEntity entity ) throws SQLException
    {
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_node_id+","+
       cn_entity+","+
       cn_receive_time+","+
       cn_note+
       " ) VALUES ( ?,?,?,? )";
     
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setInt(insIndex++,node_id);
    prepared.setValue(insIndex++,entity);
    prepared.setTimestamp(insIndex++);
    prepared.setString(insIndex++, null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();
    
    return primary_id;
    }
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowInteger(cn_node_id)).setNotNullable();
    addSchemaRow(new SchemaRowVarchar(cn_entity,MimEntity.entity_len_max)).setNotNullable();  
    addSchemaRow(new SchemaRowTimestamp(cn_receive_time)).setNotNullable();
    addSchemaRow(new SchemaRowVarchar(cn_note,cn_note_len));
    }
  
  
  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+cn_view+" AS "+ 
      "SELECT * FROM "+cn_tbl);
    }
    
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX aetst_nodent_index ON "+cn_tbl+" ( "+cn_node_id+","+cn_entity+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX aetst_nodent_index");
    }  
  
  @Override
  public void dropConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE aetst_tbl    DROP CONSTRAINT aetst_node_ref");
    }
  
  @Override
  public void createConstraint ( ExecuteProvider db )
    {
    db.executeDbaseDone("ALTER TABLE aetst_tbl ADD CONSTRAINT aetst_node_ref FOREIGN KEY (aetst_node_id ) REFERENCES aenode_tbl (aenode_id) ");
    }
  
  }
      
      
  } 

