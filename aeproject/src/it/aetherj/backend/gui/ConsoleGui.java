/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.utils.*;
import it.aetherj.boot.Log;
import it.aetherj.shared.*;


public final class ConsoleGui
  {
  private static final String classname="ConsoleGui.";
  
  private final Stat stat;
  private final ExitProgramRunnable exitProgramRunnable = new ExitProgramRunnable();
  
  private final ConsoleFrame consoleFrame;
  private final JPanel workPanel;
  
  private volatile ComponentProviderSaveWinpos curCenterProvider;
  private volatile JComponent curCenterPanel;
  
  /**
   * Constructor
   */
  public ConsoleGui(Stat stat)
    {
    this.stat = stat;
     
    // this changes the foreground color for inactive components to RED
    UIManager.put("TextField.inactiveForeground",Color.red);
    
    consoleFrame = new ConsoleFrame("Aether backend + frontend server");
    consoleFrame.addWindowListener(new ConsoleWindowListener());
    
    consoleFrame.setJMenuBar(stat.consoleMenubar.getMenuBar());
    
    workPanel = (JPanel)consoleFrame.getContentPane();
    }
    
  /**
   * Sets the content of the console.
   * Give a JComponent and it will replace the current content, if not null obviously
   * @param provider a provider for JComponent
   */
  public void setContent ( ComponentProviderSaveWinpos provider )
    {
    if ( provider == null ) return;
    
    curCenterProvider = provider;
    
    JComponent aComponent = provider.getComponentToDisplay();
    
    if ( curCenterPanel != null ) workPanel.remove(curCenterPanel);
    workPanel.add(aComponent,BorderLayout.CENTER);
    curCenterPanel = aComponent;
    workPanel.revalidate();
    workPanel.repaint();
    }
  
 
  public void setVisible ( boolean flag )
    {
    consoleFrame.setBounds(stat.winposStore.getWindowBounds(consoleFrame.getTitle()));
    consoleFrame.setVisible(flag);
    }

  public void saveWindowPosition ()
    {
    consoleFrame.saveWindowPosition(stat.winposStore);
    
    if ( curCenterProvider != null ) 
      curCenterProvider.saveWindowPosition(stat.winposStore);

    stat.fileChooser.saveWindowPosition(stat.winposStore);
    stat.beanshell.saveWindowPosition(stat.winposStore);
    stat.sqlquery.saveWindowPosition(stat.winposStore);
    
    stat.winposStore.putWindowBounds(Log.frameTitle,stat.log.getBounds());
    
    stat.winposStore.save();
    }
  
	public JFrame getJFrame()
		{
		return 	consoleFrame;		
		}
	
  /**
   * Shows a message dialog using this frame as a holder for it.
   * This is needed to avoid lockup of the GUI due to missing synchronization.
   * A message can be anything that JOptionPane.showMessageDialog support.
   */
  public final void showMessageDialog ( Object message )
    {
    if ( SwingUtilities.isEventDispatchThread() )
      JOptionPane.showMessageDialog(consoleFrame,message);
    else
      SwingUtilities.invokeLater(new SwingShowMessage(message));    
    }

  public final int showConfirmDialog ( String message )
    {
    if ( SwingUtilities.isEventDispatchThread() )
      return JOptionPane.showConfirmDialog(consoleFrame,message);

    stat.println(classname+"showConfirmDialog: NOT in swing thread, return cancel");

    return JOptionPane.CANCEL_OPTION;
    }
  
  /**
   * A simple utility class to do things properly, thread safe
   */
private final class SwingShowMessage implements Runnable
  {
  Object message;

  SwingShowMessage ( Object message )
    {
    this.message=message;
    }
  
  public void run ()
    {
    JOptionPane.showMessageDialog(consoleFrame,message,"SWING FIX",JOptionPane.INFORMATION_MESSAGE);
    }
  }
  
private final class ConsoleWindowListener extends WindowAdapter
  {
  public void windowClosing(WindowEvent e) 
    {
    // make sure that the GUI is not stuck on program existing
    Aeutils.newThreadStart(exitProgramRunnable, "Gui Exit Program");
    }
  }

private class ExitProgramRunnable implements Runnable
  {
  @Override
  public void run()
    {
    if ( stat.haveShutdownReq() )
      {
      // if this is the SECOND time a use press on close, then something is stuck and needs to be KILLED
      // Since there is a security manager installed you need to use the appropriate exit code.
      stat.systemExit();
      }
    else
      {
      // First time event, just set the flag and let the rest of application shutdown
      stat.setShutdown(true);
      // it should happen in a timely manner, if not, just click again and it will be killed
      }
    }
  }



  }
