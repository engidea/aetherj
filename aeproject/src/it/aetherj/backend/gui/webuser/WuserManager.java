/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.webuser;

import java.awt.BorderLayout;
import java.awt.event.*;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.WinposStore;
import it.aetherj.backend.gui.utils.ComponentProviderSaveWinpos;
import it.aetherj.gwtwapp.client.beans.AegwUser;
 

/**
 * This holds the various panels together
 */
public final class WuserManager implements ComponentProviderSaveWinpos
  {
  private static final String classname="WuserManager.";
  
  private final Stat stat;

  private final JPanel          workPanel;
  private final WuserTableModel tableModel;
  private final WuserTable      wusertable;
  private final WuserEdit       wuserEdit;
  private final JButton         newButton;
  
  
  public WuserManager(Stat stat)
    {
    this.stat = stat;
  
    tableModel = new WuserTableModel(stat);
    wusertable = new WuserTable(stat,this,tableModel);
    wuserEdit  = new WuserEdit(stat,this);
    
    newButton = stat.utils.newJButton("New", new ManagerButtonListener());

    workPanel = newWorkPanel();
    }
  
  /**
   * This will ask the model to refresh table from dbase data
   */
  public void refreshTable()
    {
    tableModel.reloadTableData();
    }
  
  public void editWuser ( AegwUser user )
    {
    wuserEdit.edit(user);
    }
    
  private JPanel newNorthPanel()
    {
    JPanel risul = new JPanel();
    
    risul.add(newButton);
    
    return risul;
    }
  
  private JPanel newWorkPanel()
    {
    JPanel risul = new JPanel(new BorderLayout());
    
    risul.add(newNorthPanel(), BorderLayout.NORTH);
    risul.add(wusertable.getComponentToDisplay(),BorderLayout.CENTER);
    risul.add(wuserEdit.getComponentToDisplay(),BorderLayout.SOUTH);
    
    return risul;
    
    }
  
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }
    
private class ManagerButtonListener implements ActionListener
  {
  public void actionPerformed(ActionEvent event)
    {
    // Creating a new user is just editing an empty one, save will create it
    editWuser(new AegwUser());
    }
  }

  @Override
  public void saveWindowPosition(WinposStore wpstore)
    {
    // there is nothing to do
    }




  
  }
