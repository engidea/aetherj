package it.aetherj.backend.gui.webuser;

import java.awt.event.MouseAdapter;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.TableColumn;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.utils.JtableDateRenderer;
import it.aetherj.backend.mimclient.McwRunnerTableModel;
import it.aetherj.boot.ComponentProvider;
import it.aetherj.gwtwapp.client.beans.AegwUser;

public class WuserTable implements ComponentProvider
  {
  private final Stat stat;
  private final WuserManager parent;
  
  private final JScrollPane workPanel;
  private final WuserTableModel model;  

  private JTable jtable;
  
  public WuserTable(Stat stat, WuserManager parent, WuserTableModel model )
    {
    this.stat = stat;
    this.parent = parent;
    this.model = model;
    
    workPanel = newWorkPanel();
    }
    
  private JScrollPane newWorkPanel()
    {
    jtable = new JTable(model);
    
    setColWidth(jtable, McwRunnerTableModel.col_id,40,100);
    
    TableColumn column = jtable.getColumnModel().getColumn(McwRunnerTableModel.col_start_date);
    column.setCellRenderer(new JtableDateRenderer(JtableDateRenderer.format_hhmmss));
    
    jtable.addMouseListener(new McwMouseAdapter());
//    McwSelectionListener listener = new McwSelectionListener();
//    jtable.getSelectionModel().addListSelectionListener(listener);
    
    JScrollPane risul = new JScrollPane(jtable);

    return risul;
    }
  
  private void setColWidth ( JTable jt, int col_index, int pref_w, int max_w )
    {
    TableColumn column = jt.getColumnModel().getColumn(col_index);
    column.setPreferredWidth(pref_w);
    column.setMaxWidth(max_w);
    }

    
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }
  
  public String toString()
    {
    return model.toString();
    }

private class McwMouseAdapter extends MouseAdapter
  {
  @Override
  public void mouseClicked(java.awt.event.MouseEvent evt) 
    {
    int row = jtable.rowAtPoint(evt.getPoint());
    row = jtable.convertRowIndexToModel(row);

    AegwUser user = model.getWuser(row);
    parent.editWuser ( user );
    }
  }
 
/**
 * This results active every time the row is selected, even if I have NOT clicked on it
 */
private class McwSelectionListener implements ListSelectionListener
  {
  public void valueChanged(ListSelectionEvent event)
    {
    if ( event.getValueIsAdjusting() ) return;

    int selection = jtable.getSelectedRow();
    if ( selection < 0 ) return;
    
    // convert the selection into the index of the table model
    selection = jtable.convertRowIndexToModel(selection);

    }
  }


  }
