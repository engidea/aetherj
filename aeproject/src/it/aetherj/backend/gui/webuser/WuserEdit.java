/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.webuser;

import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbtbls.AedbWebuser;
import it.aetherj.backend.gui.utils.SwingGridBagLayout;
import it.aetherj.boot.ComponentProvider;
import it.aetherj.gwtwapp.client.beans.AegwUser;
import it.aetherj.shared.Aeutils;
 

/**
 * Used to change basic info on a web user
 */
public final class WuserEdit implements ComponentProvider
  {
  private static final String classname="WuserEdit.";
  
  private final EditButtonListener buttonListener = new EditButtonListener();
  
  private final Stat stat;
  private final WuserManager parent;

  private final JPanel      workPanel;
  private final JTextField  idField,loginField,passwordField;
  private final JLabel      messageLabel;

  private JButton        saveButton,deleteButton;
  
  private AegwUser curWuser = new AegwUser();
  
  public WuserEdit(Stat stat, WuserManager parent)
    {
    this.stat = stat;
    this.parent=parent;
  
    idField = new JTextField(5);
    idField.setEditable(false);
    
    loginField = new JTextField(20);
    passwordField = new JTextField(40);
    
    messageLabel = new JLabel();

    saveButton = stat.utils.newJButton("Save",buttonListener);
    deleteButton = stat.utils.newJButton("Delete", buttonListener);

    workPanel = newWorkPanel();
    }
  
  private JLabel newJLabel(String text)
    {
    return stat.labelFactory.newJLabel(classname, text);
    }
    
  private JPanel newWorkPanel()
    {
    SwingGridBagLayout layout = new SwingGridBagLayout();

    layout.addComponentEastIncx(newJLabel("user ID"));
    layout.addComponentWestIncx(idField).newline();
    
    layout.addComponentEastIncx(newJLabel("User Login"));
    layout.addComponentWestIncx(loginField).newline();

    layout.addComponentEastIncx(newJLabel("Password"));
    layout.addComponentWestIncx(passwordField).newline();

    layout.addComponentWestIncx(deleteButton);
    layout.addComponentEastIncx(saveButton).newline();
    
    layout.gbc.gridwidth=2;
    layout.addComponentIncx(messageLabel);
    
    JPanel risul = layout.getJPanel();
    risul.setBorder(new TitledBorder("   Edit Web User  "));
    
    return risul;
    }
  
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }
  
  public void edit ( AegwUser wuser )
    {
    if ( wuser == null )
      return;
    
    curWuser = wuser;
    
    idField.setText(""+wuser.wuser_id);
    loginField.setText(wuser.wuser_login);
    }

private class EditButtonListener implements ActionListener
  {
  public void actionPerformed(ActionEvent event)
    {
    String login = loginField.getText();
    String password = passwordField.getText();

    AegwUser n_wuser = new AegwUser(curWuser.wuser_id,login,password);
     
    Aeutils.newThreadStart(new ChangeUserLoginPassword(n_wuser),"Change Login Password");
    }
  }

private class ChangeUserLoginPassword implements Runnable
  {
  private AegwUser wuser;
  
  ChangeUserLoginPassword ( AegwUser wuser )
    {
    this.wuser=wuser;
    }
    
  public void run()
    {
    try
      {
      AeDbase dbase = stat.newWorkDbase();
  
      AedbWebuser db = stat.aedbFactory.aedbWebuser;
      
      curWuser.wuser_id = db.dbaseSave(dbase, wuser);

      dbase.close();

      passwordField.setText(null);
      messageLabel.setText("Data Updated id="+curWuser);
      parent.refreshTable();
      }
    catch ( Exception exc )
      {
      stat.log.exceptionShow(classname+"ChangeUserLoginPassword",exc);
      }
    }
  }



  
  }
