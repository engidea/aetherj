package it.aetherj.backend.gui.webuser;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbtbls.AedbWebuser;
import it.aetherj.gwtwapp.client.beans.AegwUser;
import it.aetherj.shared.Aeutils;

public class WuserTableModel extends AbstractTableModel
  {
  private static final long serialVersionUID = 1L;

  private final Stat stat;
  
  public static final int col_id=0;
  public static final int col_name=1;
  public static final int col_note=2;
  public static final int col_count=3;
  
  private final ArrayList<AegwUser> quserList = new ArrayList<AegwUser>();

  public WuserTableModel(Stat stat )
    {
    this.stat=stat;
    }
  
  public synchronized int getCount ()
    {
    return quserList.size();
    }

  /**
   * This must load using NON swing thread and then refresh table in swing
   */
  public void reloadTableData ()
    {
    Aeutils.newThreadStart(new ReloadTableData(), "Reload Wuser table data");
    }
  
  public synchronized AegwUser getWuser (int index)
    {
    try
      {
      return quserList.get(index);
      }
    catch ( Exception exc )
      {
      return null;
      }
    }
    
  public synchronized void addWuser(AegwUser mcwNode)
    {
    if ( mcwNode == null ) return;
    
    int rowindex = quserList.size();

    quserList.add(mcwNode);
    
    fireTableRowsInserted(rowindex,rowindex);
    }
    
  public synchronized void removeRunner(AegwUser mcwNode)
    {
    if ( mcwNode == null ) return;
    
    int rowindex = quserList.size();

    quserList.remove(mcwNode);
    
    fireTableRowsDeleted(rowindex,rowindex);
    }
    
  @Override
  public String getColumnName(int column) 
    {
    switch ( column )
      {
      case col_id:  
        return "Id";
        
      case col_name:  
        return "Name";
        
      case col_note:  
        return "Note";
        
      default:
        return "NULL col="+column;
      }
    }
    
  @Override
  public Class<?> getColumnClass(int col)
    {
    switch ( col )
      {
      case col_id:
        return Integer.class;
        
      case col_name:  
      case col_note:  
        return String.class;
      
      default:
        return Object.class;
      }
    }

  @Override
  public int getRowCount()
    {
    return quserList.size();
    }

  @Override
  public int getColumnCount()
    {
    return col_count;
    }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex)
    {
    AegwUser mcwNode = getWuser(rowIndex);

    if ( mcwNode == null ) return "null row="+rowIndex;
    
    switch ( columnIndex )
      {
      case col_id: 
        return mcwNode.wuser_id;

      case col_name: 
        return mcwNode.wuser_login;

      case col_note: 
        return mcwNode.wuser_note;
        
      default:
        return "NULL col="+columnIndex;
      }
    }
  
  
  public String toString ()
    {
    return "wuser size="+quserList.size();
    }
  
  private synchronized void refreshTableData(ArrayList<AegwUser>users)
    {
    quserList.clear();
    quserList.addAll(users);
    
    super.fireTableDataChanged(); 
    }
  
private class ReloadTableData implements Runnable
  {
  @Override
  public void run()
    {
    AedbWebuser db = stat.aedbFactory.aedbWebuser;
    
    ArrayList<AegwUser>users=db.selectAll(stat.dbase);

    Aeutils.SwingInvokeLater(new  RefreshTableData(users));
    }
  }

private class RefreshTableData implements Runnable
  {
  private final ArrayList<AegwUser>users;
  
  RefreshTableData ( ArrayList<AegwUser>users )
    {
    this.users=users;
    }

  @Override
  public void run()
    {
    refreshTableData(users);
    }
  }



  }
