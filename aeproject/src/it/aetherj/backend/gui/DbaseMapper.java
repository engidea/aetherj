/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;

/**
 * You can add this as combo and you can get this object when combo row selected
 * Then you ask to create a new dbase and wait for it on callback
 * It needs to be a Runnable since an external dbase may hang
 */
public class DbaseMapper
  {
  private static final String classname="DbaseMapper";
  private final Stat stat;
  
  public static final String MAIN_DBASE="Main Dbase";

  public final String desc;
  
  DbaseMapper (Stat p_stat, String p_desc )
    {
    stat = p_stat;
    desc = p_desc;
    }
  
  /**
   * The new dbase will be set by the thread.
   * A bit of messing with timings but it should be better than locking up the machine.
   */
  public void newDbaseRequest (DbaseConnectCallback risul_callback)
    {
    Thread thread = new Thread(new NewDbaseRunnable(risul_callback));
    thread.setPriority(Thread.MIN_PRIORITY);
    thread.setName("DbaseMapper: "+desc);
    thread.start();
    }
    
  @Override
  public String toString ()
    {
    return desc;
    }
  
private class NewDbaseRunnable implements Runnable
  {
  DbaseConnectCallback risul_callback;
  
  NewDbaseRunnable ( DbaseConnectCallback p_risul_callback )
    {
    risul_callback = p_risul_callback;
    }

  @Override
  public void run()
    {
    AeDbase risul;
    
    if ( desc.equals(MAIN_DBASE) )
      risul = stat.newWorkDbase();
    else
      risul = null;
    
    stat.log.userPrintln(classname+".newDbase run() risul="+risul);
    
    if ( risul_callback != null ) risul_callback.onDbaseConnected(risul);
    }
  }

interface DbaseConnectCallback
  {
  void onDbaseConnected ( AeDbase dbase );
  }


  }
