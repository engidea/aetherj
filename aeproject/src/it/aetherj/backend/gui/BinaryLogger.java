/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.text.*;

import it.aetherj.boot.*;


/**
 * It happens quite often that you need a text area that has a limit on the number of lines
 * you can put in it, it is useful when you have some sort of logging and you want to avoid it to fill up
 * the entire system.
 */
public final class BinaryLogger implements ComponentProvider, PrintlnProvider
  {
  private static final String []byteToStr = { 
    "00","01","02","03","04","05","06","07","08","09","0A","0B","0C","0D","0E","0F",
    "10","11","12","13","14","15","16","17","18","19","1A","1B","1C","1D","1E","1F",
    "20","21","22","23","24","25","26","27","28","29","2A","2B","2C","2D","2E","2F",
    "30","31","32","33","34","35","36","37","38","39","3A","3B","3C","3D","3E","3F",
    "40","41","42","43","44","45","46","47","48","49","4A","4B","4C","4D","4E","4F",
    "50","51","52","53","54","55","56","57","58","59","5A","5B","5C","5D","5E","5F",
    "60","61","62","63","64","65","66","67","68","69","6A","6B","6C","6D","6E","6F",
    "70","71","72","73","74","75","76","77","78","79","7A","7B","7C","7D","7E","7F",
    "80","81","82","83","84","85","86","87","88","89","8A","8B","8C","8D","8E","8F",
    "90","91","92","93","94","95","96","97","98","99","9A","9B","9C","9D","9E","9F",
    "A0","A1","A2","A3","A4","A5","A6","A7","A8","A9","AA","AB","AC","AD","AE","AF",
    "B0","B1","B2","B3","B4","B5","B6","B7","B8","B9","BA","BB","BC","BD","BE","BF",
    "C0","C1","C2","C3","C4","C5","C6","C7","C8","C9","CA","CB","CC","CD","CE","CF",
    "D0","D1","D2","D3","D4","D5","D6","D7","D8","D9","DA","DB","DC","DD","DE","DF",
    "E0","E1","E2","E3","E4","E5","E6","E7","E8","E9","EA","EB","EC","ED","EE","EF",
    "F0","F1","F2","F3","F4","F5","F6","F7","F8","F9","FA","FB","FC","FD","FE","FF" };

  private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

  private final JScrollPane workPanel;
  private final JTextPane textPane;
  private final StyledDocument workDoc;
  private final int linesMax;

  private SimpleAttributeSet binAttribute;
  private SimpleAttributeSet asciiAttribute;
  
  private SimpleAttributeSet txBinAttribute;
  private SimpleAttributeSet txAsciiAttribute;
  
  private final Color txBackgroundColor  = Color.blue;
  
  public BinaryLogger ()
    {
    this(1000);
    }

  /**
   * Construct a given limited text area that holds at max lineMax lines.
   * @param linesMax
   */
  private BinaryLogger(int linesMax)
    {
    this.linesMax = linesMax;
    
    workDoc = new DefaultStyledDocument();

    textPane = new JTextPane(workDoc);
    textPane.setBackground(Color.black);
    textPane.setForeground(Color.white);

    workPanel = new JScrollPane(textPane);
    
    binAttribute = new SimpleAttributeSet();
    //binAttribute.addAttribute(StyleConstants.CharacterConstants.Bold,Boolean.TRUE);
    binAttribute.addAttribute(StyleConstants.CharacterConstants.Foreground,Color.red);
    
    txBinAttribute = new SimpleAttributeSet(binAttribute);
    txBinAttribute.addAttribute(StyleConstants.CharacterConstants.Background,txBackgroundColor);
    
    asciiAttribute = new SimpleAttributeSet();
    //binAttribute.addAttribute(StyleConstants.CharacterConstants.Bold,Boolean.TRUE);
    asciiAttribute.addAttribute(StyleConstants.CharacterConstants.Foreground,Color.yellow);

    txAsciiAttribute = new SimpleAttributeSet(asciiAttribute);
    txAsciiAttribute.addAttribute(StyleConstants.CharacterConstants.Background,txBackgroundColor);
    }
 

	public void setPreferredSize ( Dimension dimension)
		{
		textPane.setPreferredSize(dimension);
		}

  /**
   * implements ComponentProvider.
   * @return a component that is the visual part of this object.
   */
  public final JComponent getComponentToDisplay()
    {
    return workPanel;
    }

  /**
   * MOSTLY FOR DEBUGGING.
   * Once you have a binary logger object you can create a frame out of it and have the logger
   * to be displayed here. There are no ways to close it, it is assumed that you will restart the application.
   */
  public void newJFrame ()
    {
    JFrame aframe = new JFrame("Binary Logger DEBUG window");
    JPanel apanel = (JPanel)aframe.getContentPane();
    apanel.add(getComponentToDisplay(),BorderLayout.CENTER);
    aframe.pack();
    aframe.setVisible(true);
    }
    
    
  /**
   * Prints a byte array but does NOT insert newline !
   * @param value
   */
  public final void printRxByteArray ( byte [] value )
    {
    printByteArray (value,binAttribute,asciiAttribute);
    }
    
  public final void printlnRxByteArray ( byte [] value )
    {
    printByteArray (value,binAttribute,asciiAttribute);
    
    print("\n");
    }
    
  public final void printTxByteArray ( byte [] value )
    {
    printByteArray (value,txBinAttribute,txAsciiAttribute);
		}
	
  public final void printlnTxByteArray ( byte [] value )
    {
    printByteArray (value,txBinAttribute,txAsciiAttribute);
    
    print("\n");
    }

  /**
   * Basically attempts to print the given byte array value printing the unprintable chars
   * in hex format.
   * @param value
   */
  private final void printByteArray ( byte [] value, SimpleAttributeSet binSet, SimpleAttributeSet asciiSet )
    {
    // We guess that the result should fit in a double size...
    StringBuffer asciiRisul = new StringBuffer(value.length);
    StringBuffer binRisul   = new StringBuffer(value.length*2);
    
    boolean haveAscii = false;
    boolean haveBin = false;
    
    for (int index=0; index<value.length; index++ )
      {
      byte ch = value[index];
  
      if ( ch < ' ' || ch > 'z' ) 
        {
        // this is when the char is a binary value
        haveBin = true;
        binRisul.append( byteToStr[ch & 0x00ff]);
        if ( haveAscii ) 
          {
          print(asciiRisul.toString(), asciiSet);
          asciiRisul = new StringBuffer(value.length);
          haveAscii = false;
          }
        }
      else
        {
        // this is when the byte is an ascii value    
        haveAscii = true;
        asciiRisul.append((char)ch);
        if ( haveBin ) 
          {
          print(binRisul.toString(), binSet);
          binRisul = new StringBuffer(value.length);
          haveBin = false;
          }
        }
      }
      
    // this handles the last few bytes of the byte array
    
    if ( haveAscii ) print(asciiRisul.toString(), asciiSet);

    if ( haveBin ) print(binRisul.toString(), binSet);
    }
    
    
  public final void print ( String value )
    {
    print ( value, null);
    }
    
		
	public void clear ()
		{
		try
			{
			int doc_len = workDoc.getLength();
			workDoc.remove(0, doc_len);		
			textPane.setCaretPosition(0);
			}
		catch ( Exception exc )
			{
			exc.printStackTrace();
			}
		}
		
  /**
   * Prints a string without adding a /n at the end.
   * @param value
   */
  private final void print ( String value, AttributeSet attrib )
    {
    try
      {
      Element rootElement = workDoc.getDefaultRootElement();
      // Shorten the document until is is shorter than linesMax lines
      int deleteLines = rootElement.getElementCount() - linesMax;
      for (int index=0; index<deleteLines; index++ )    
        {
        Element elem = rootElement.getElement(0);
        workDoc.remove(elem.getStartOffset(),elem.getEndOffset());
        }

      // Then insert the new message.    
      workDoc.insertString(workDoc.getLength(),value,attrib);
      textPane.setCaretPosition(workDoc.getLength());
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      }
    }


	/**
	 * ONLY prints the newline
	 */
	public final void print_newline ()
		{
    try
      {
      workDoc.insertString(workDoc.getLength(),"\n",null);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      }
		}

  /**
   * Prints the given message in th text area.
   * Before printing it it makes sure that there are less linesMax in the current area.
   * After the printing there may be more than linesMax, but the next print will trim them.
   */
  public final void println ( String message )
    {
    try
      {
      Element rootElement = workDoc.getDefaultRootElement();
      // Shorten the document until is is shorter than linesMax lines
      int deleteLines = rootElement.getElementCount() - linesMax;
      for (int index=0; index<deleteLines; index++ )    
        {
        Element elem = rootElement.getElement(0);
        workDoc.remove(elem.getStartOffset(),elem.getEndOffset());
        }

      // Then insert the new message. prepend time to it...
      workDoc.insertString(workDoc.getLength(),dateFormat.format(new Date())+" ",null);
      workDoc.insertString(workDoc.getLength(),message,null);
      textPane.setCaretPosition(workDoc.getLength());
      workDoc.insertString(workDoc.getLength(),"\n",null);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      }
    }

  @Override
  public void println(String message, Throwable exception)
    {
    println (Log.exceptionExpand(message, exception));
    }
    
  }
