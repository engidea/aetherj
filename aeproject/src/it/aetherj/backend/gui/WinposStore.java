/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui;

import java.awt.Rectangle;
import java.io.*;
import java.util.*;

import javax.swing.JInternalFrame;

/**
 * I need to save window position and dimensions between runs.
 */
public final class WinposStore
  {
  public static final String winposStoreFname = "winposStore.bin";
  
  private final File storageFile;
  
  private HashMap<String,WinposProperties> storeMap;
  
  public WinposStore(File storageDir)
    {
    this.storageFile = new File(storageDir,winposStoreFname);
    this.storeMap = loadMap();
    
    verifyMap();
    }

  private void verifyMap ()
    {
    try
      {
      Collection<WinposProperties>values = storeMap.values();
      Iterator<WinposProperties>iter = values.iterator();

      if ( iter.hasNext() )
        {
        @SuppressWarnings("unused")
        WinposProperties prop = iter.next();
        }
      }
    catch ( Exception exc )
      {
      storeMap.clear();
      }
    }
  
  private HashMap<String,WinposProperties> loadMap ()
    {
    try 
      {
      FileInputStream fi = new FileInputStream(storageFile);
      ObjectInputStream si = new ObjectInputStream(fi);  
      @SuppressWarnings("unchecked")
      HashMap<String,WinposProperties> risul = (HashMap<String, WinposProperties>) si.readObject();
      si.close();
      return risul;
      } 
    catch (Exception exc) 
      {
      // most likely the file does not exist...
      exc=null;
      }
      
    return new HashMap<String,WinposProperties>();
    }



  /**
   * Save the bounds currently here in a default filename in the 
   * program working dir ....
   */
  public void save()
    {
    try 
      {
      File parentDir = storageFile.getParentFile();
      
      if ( ! parentDir.exists() )
        parentDir.mkdirs();
      
      FileOutputStream fo = new FileOutputStream(storageFile);
      ObjectOutputStream so = new ObjectOutputStream(fo);
      so.writeObject(storeMap);
      so.flush();
      so.close();
      } 
    catch (Exception e) 
      {
      e.printStackTrace();
      System.exit(1);
      }
    }
    
  public void putWindowBounds (String title, Rectangle bounds)
    {
    if ( title == null || bounds == null ) return;
    
    // I need to pick up the previous stored entity to update it
    WinposProperties prop = storeMap.get(title);
    
    if ( prop == null ) prop = new WinposProperties();
    
    prop.winPosition = bounds;
    
    storeMap.put(title,prop);
    }
    
  public void putWindowBounds (JInternalFrame frame)
    {
    if ( frame == null ) return;
    
    putWindowBounds(frame.getTitle(), frame.getBounds());
    }

  
  
  public void putFile ( String title, File afile )
    {
    if ( title == null || afile == null ) return;
    
    // I need to pick up the previous stored entity to update it
    WinposProperties prop = storeMap.get(title);
    
    if ( prop == null ) prop = new WinposProperties();
    
    prop.filePosition = afile;
    
    storeMap.put(title,prop);
    }
  
  public Rectangle getWindowBounds (String title)
    {
    Rectangle risul = new Rectangle(10,20,800,600);

    if ( title == null ) return risul;
    
    WinposProperties prop = storeMap.get(title);
    
    if ( prop == null ) return risul;
    
    return prop.winPosition == null ? risul : prop.winPosition;
    }
  
  public File getFile ( String title, File adefault )
    {
    if ( title == null ) return adefault;
    
    WinposProperties prop = storeMap.get(title);
    
    if ( prop == null ) return adefault;
    
    return prop.filePosition == null ? adefault : prop.filePosition;
    }

  
public static class WinposProperties implements Serializable
  {
  private static final long serialVersionUID = 1L;

  public Rectangle winPosition;
  public File filePosition;      // for the file chooser
  public String stringPosition;  // to be used
  }
  
  
}  // End of main class

