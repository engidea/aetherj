package it.aetherj.backend.gui.config;

import java.awt.Font;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.utils.SwingGridBagLayout;
import it.aetherj.boot.ComponentProvider;
import it.aetherj.shared.Aeutils;

public class AetherParamsGui implements ComponentProvider
  {
  private static final String classname = "AetherParamsGui";
  
  private final ButtonListener buttonListener = new ButtonListener();

  private final Stat stat;
  private final AetherParams params;
  
  private final JComponent     workPanel;
  private final JButton        saveButton,newIdentityButton;
  
  private final JTextField     nodeName,bootAddress;
  private final JTextField     bestAddressCountField,bestAddressEveryField;
  private final JTextField     ploadPowLenthField,powTimeoutField;
  private final JTextField     wishWebPortField,mimobjExpireField;
  private final JTextArea      X509certArea;
  private final JTextField     nodeFingerprint;

  public AetherParamsGui(Stat stat, AetherParams params )
    {
    this.stat = stat;
    this.params = params;
    
    saveButton = stat.utils.newJButton("Save", buttonListener);
    newIdentityButton = stat.utils.newJButton("New Identity", buttonListener);
    
    nodeName              = new JTextField(30);
    bootAddress           = new JTextField(40);
    bestAddressCountField = new JTextField(5);
    bestAddressEveryField = new JTextField(5);
    ploadPowLenthField    = new JTextField(5);
    powTimeoutField       = new JTextField(5);
    wishWebPortField      = new JTextField(5);
    mimobjExpireField     = new JTextField(5);
    nodeFingerprint       = newReadonlyText(44);
    
    X509certArea = new JTextArea(20,80);
    X509certArea.setFont( new Font( "Monospaced", Font.PLAIN, 10 ) );
    X509certArea.setEditable(false); 
    
    this.workPanel = newWorkPanel();
    }

  private JTextField newReadonlyText (int len)
    {
    JTextField risul = new JTextField(len);
    risul.setEditable(false);
    return risul;
    }
    
  /**
   * Called by the datastore when a load or save is performed
   */
  public void refreshGui ()
    {
    nodeName.setText(params.getNodeName());
    bootAddress.setText(params.getBootLocation());
    bestAddressCountField.setText(""+params.getBestAddressCount());
    bestAddressEveryField.setText(""+params.getBestAddressEvery_s());
    ploadPowLenthField.setText(""+params.getPloadPowLength());
    powTimeoutField.setText(""+params.getPowTimeout_s());
    wishWebPortField.setText(""+params.getBackendPort());
    mimobjExpireField.setText(""+params.getMimobjExpireMn());
    
    X509certArea.setText(params.getX509cert_s());
    nodeFingerprint.setText(params.getNodeFingerprint().toString());
    }
    
  private JLabel newJLabel(String eng)
    {
    return stat.labelFactory.newJLabel(classname, eng);
    }
  
  private JScrollPane newWorkPanel()
    {
    SwingGridBagLayout layout = new SwingGridBagLayout();

    layout.setGridwidth(3);
    layout.addComponentEastIncx(saveButton).newline();
    
    layout.setGridwidth(1);
    layout.addComponentEastIncx(newJLabel("Node name"));

    layout.setGridwidth(2);
    layout.addComponentWestIncx(nodeName).newline();

    layout.setGridwidth(1);
    layout.addComponentEastIncx(newJLabel("Bootstrap address"));

    layout.setGridwidth(2);
    layout.addComponentWestIncx(bootAddress).newline();

    layout.setGridwidth(1);
    layout.addComponentEastIncx(newJLabel("Wish web port"));
    layout.addComponentWestIncx(wishWebPortField).newline();

    layout.addComponentEastIncx(newJLabel("Best Addresses Count"));
    layout.addComponentWestIncx(bestAddressCountField).newline();

    layout.addComponentEastIncx(newJLabel("Run Best Addresses every"));
    layout.addComponentWestIncx(bestAddressEveryField);
    layout.addComponentWestIncx(newJLabel("seconds")).newline();

    layout.addComponentEastIncx(newJLabel("Payload POW Length"));
    layout.addComponentWestIncx(ploadPowLenthField);
    layout.addComponentWestIncx(newJLabel("range 16-21")).newline();

    layout.addComponentEastIncx(newJLabel("Mim POW Timeout"));
    layout.addComponentWestIncx(powTimeoutField);
    layout.addComponentWestIncx(newJLabel("seconds")).newline();

    layout.addComponentEastIncx(newJLabel("Objects Expire "));
    layout.addComponentWestIncx(mimobjExpireField);
    layout.addComponentWestIncx(newJLabel("monts")).newline();

    layout.setGridwidth(1);
    layout.addComponentEastIncx(newJLabel("Node Fingerprint"));

    layout.setGridwidth(2);
    layout.addComponentWestIncx(nodeFingerprint).newline();

    layout.setGridwidth(3);
    JScrollPane scroll = new JScrollPane(X509certArea);
    scroll.setBorder(new TitledBorder("  Web X509 PEM Certificate  "));
    layout.addComponentWestIncx(scroll).newline();
    
//    layout.setGridwidth(1);
//    layout.addComponentWestIncx(newIdentityButton);
    
    JPanel panel = layout.getJPanel();
    
    return new JScrollPane(panel);
    }
  
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }
  
  private void saveClickFun ()
    {
    params.setNodeName(nodeName.getText());
    params.setBootLocation(bootAddress.getText());
    params.setBestAddressCount(bestAddressCountField.getText());
    params.setBestAddressEvery_s(bestAddressEveryField.getText());
    params.setPloadPowLength(ploadPowLenthField.getText());
    params.setBackendPort(wishWebPortField.getText());
    params.setPowTimeout_s(powTimeoutField.getText());
    params.setMimobjExpireMn(mimobjExpireField.getText());
    
    Aeutils.newThreadStart(new SaveParamsRun(), "SaveParamsRun");
    }

  private void newIdentityFun ()
    {
    }
  
private final class ButtonListener implements ActionListener
  {
  @Override
  public void actionPerformed(ActionEvent event)
    {
    Object source = event.getSource();
    
    if ( source == saveButton )
      saveClickFun();
    else if ( source == newIdentityButton )
      newIdentityFun();
    }
  }
  
  
private final class SaveParamsRun implements Runnable
  {
  public void run()
    { 
    try
      {
      params.saveToDatastore();
      }
    catch ( Exception exc )
      {
      stat.println("SaveParamsRun", exc);
      }
    }
  }
  
  }