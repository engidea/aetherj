/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.Locale;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import it.aetherj.backend.gui.utils.*;
import it.aetherj.boot.Log;
import it.aetherj.boot.cfgproperty.*;
import it.aetherj.boot.dbase.Dbase;

/**
 * There are various options that need to be set for the program.
 * This class encapsulate that and allow
 * - User to change options and save new value
 * - Program to get the various values when they are needed
 * When this class is newed it is also ready to serve options. 
 */
public final class ConfigProperties 
  {
  private static final String classname="ProgramConfig.";
  private static final String propertiesFname="aetherj.properties";
  
  private final ButtonHandler handler = new ButtonHandler();

  public static final String HSQL_dbname="aetherdb";
  public static final String HSQL_dbfilename="aetherdb";
  public static final String HSQL_listen="127.0.0.1";

  
  public static final String KEY_hsqldbPort="hsqldbPort";
	public static final String KEY_webBrowserAutostart="webBrowserAutostart";
  
  public static final String KEY_localeOverride="localeOverride";
  public static final String KEY_debugMask="debugMask";
  public static final String KEY_debugLevel="debugLevel";
  
  private final Log  log; 

  private final ConsoleFrame workFrame;
  private final JButton saveButton;
  
  private final PropertyStore propertyStore;  
   
  public  final PropertyGroup programProperty;        // use the getters to get the actual values
  public  final PropertyGroup mainDbaseProperty;      
  
  private ConfigChangedInterface configChangeListener; 
  private Locale labels_locale;                       // something like it_IT, like windows...
  
  /**
   * Constructor.
   * We need the boot to extract the working directory
   * The Log to log exceptions and various messages
   */
  public ConfigProperties ( File datastoreDir, Log log  )
    {
    this.log = log; 
     
    propertyStore = new PropertyStore(new File(datastoreDir,propertiesFname),"AetherJ Server");
    
    mainDbaseProperty= new PropertyGroup("dbaseOne.");
    propertyStore.addPropertyGroup(mainDbaseProperty);
    
    programProperty= new PropertyGroup("program.");
    propertyStore.addPropertyGroup(programProperty);
    
    saveButton=new JButton("Save");
    saveButton.addActionListener(handler);

    workFrame = new ConsoleFrame("Properties",JFrame.HIDE_ON_CLOSE);
    
    SwingGridBagLayout panel = new SwingGridBagLayout();
    panel.gbc.anchor=GridBagConstraints.NORTHWEST;
    panel.gbc.fill=GridBagConstraints.HORIZONTAL;

    panel.addComponentIncx(newDbaseConnectInfoPanel(mainDbaseProperty,"  Database  ")).newline();

    panel.addComponentIncx(newProgramPropertiesPanel(programProperty,"  Application ")).newline();

    panel.gbc.anchor=GridBagConstraints.SOUTHEAST;
    panel.gbc.fill=GridBagConstraints.NONE;

    panel.addComponentIncx(saveButton);
    
    // needed to handle space on small screens
    JScrollPane scroll = new JScrollPane(panel.getJPanel());
    
    workFrame.setContentPane(scroll);
    workFrame.pack();
    workFrame.setLocationRelativeTo(null);
    }

  /**
   * Returns the configuration panel, where you specify how to connect
   * to the Dbase server, this works for any number of databases !
   */
  private JPanel newDbaseConnectInfoPanel(PropertyGroup propertyPanel, String borderLabel )
    {
    JPanel aPanel = new JPanel(new GridLayout(0,1));
    aPanel.setBorder(new TitledBorder(borderLabel));  
      
    PropertyInterface aProp=propertyPanel.addProperty(new StringProperty(Dbase.KEY_jdbcClass, "jdbc Class: ","org.hsqldb.jdbcDriver",30));
    aPanel.add(aProp.getComponentToDisplay()); 
   
    aProp=propertyPanel.addProperty(new StringProperty(Dbase.KEY_jdbcUrl, "jdbc URL: ","jdbc:hsqldb:hsql://localhost:9005/"+HSQL_dbname,30));
    aPanel.add(aProp.getComponentToDisplay());

    // Careful, username IS case sensitive and HSQLDB uses uppercases
    aProp=propertyPanel.addProperty(new StringProperty(Dbase.KEY_jdbcUser, "jdbc Username: ","AETHERDBU",20));
    aPanel.add(aProp.getComponentToDisplay());

    aProp=propertyPanel.addProperty(new PasswordProperty(Dbase.KEY_jdbcPassword, "jdbc Password: ","aetherpass",20));
    aPanel.add(aProp.getComponentToDisplay());

    aProp=propertyPanel.addProperty(new StringProperty(Dbase.KEY_dbaseSchema, "Dbase Schema: ","public",20));
    aPanel.add(aProp.getComponentToDisplay());

    return aPanel;
    } 

  
  /**
   * Returns a panel where you decide what you can start as optional components.
   */
  private JPanel newProgramPropertiesPanel (PropertyGroup propertyPanel, String borderLabel )
    {
    PropertyInterface aProp; 
    
    JPanel aPanel = new JPanel();
    aPanel.setLayout(new BoxLayout(aPanel,BoxLayout.Y_AXIS));
    aPanel.setBorder(new TitledBorder(borderLabel));
    
    aProp=propertyPanel.addProperty(new CheckboxProperty(KEY_webBrowserAutostart, "Browser Autostart ",false));
    aPanel.add(aProp.getComponentToDisplay());
    
    aProp=propertyPanel.addProperty(new IntegerProperty(KEY_hsqldbPort,  "HSQLDB port ",9005,4));
    aPanel.add(aProp.getComponentToDisplay());

    aProp=propertyPanel.addProperty(new StringProperty(KEY_localeOverride, "Locale Override: ","",10));
    aPanel.add(aProp.getComponentToDisplay());

    aProp=propertyPanel.addProperty(new IntegerHexProperty(KEY_debugMask,  "Debug Mask Hex",0,4));
    aPanel.add(aProp.getComponentToDisplay());

    aProp=propertyPanel.addProperty(new IntegerHexProperty(KEY_debugLevel,  "Debug Level Hex",0,4));
    aPanel.add(aProp.getComponentToDisplay());

    return aPanel; 
    }

  private Locale splitLocaleOverride ( String input )    
    {
    if ( input == null || input.length() < 1 ) return null;
    
    String []split = input.split("_");
    
    if ( split.length >= 2 ) return new Locale(split[0],split[1]);
    
    if ( split.length >= 1 ) return new Locale(split[0]);
    
    return null;
    }
  
  /**
   * This initialize the whole stuff. It is here so I can manage not
   * to throw too many exceptions from the constructor
   */
  public void initialize ()
    {
    propertyStore.load();

    // set the locale code to a default value
    labels_locale = Locale.getDefault();
    
    Locale localeOverride = splitLocaleOverride((String)programProperty.getValue(KEY_localeOverride) );
    
    // allow an override if there is one
    if ( localeOverride != null ) labels_locale = localeOverride;
    }
    
  /**
   * Returns the given property as int, it returns defVal if non existent.
   * The value is taken from the program configuration pool. 
   * @return the saved value or the default.
   */
  public int getIntProperty ( String propName, int defval )
    {
    Object risul = programProperty.getValue(propName);
    if ( risul == null )       
      {
      log.userPrintln(classname+"getIntProperty() propName="+propName+" is unknown");     
      return defval;
      }

    if ( risul instanceof Integer ) return ((Integer)risul).intValue();

    log.userPrintln(classname+"getIntProperty() propName="+propName+" risul Class="+risul.getClass());     
    return defval;
    }

  /**
   * Returns the given property as String, it returns defVal if non existent.
   * The value is taken from the progrtam configuration pool.
   * @return the saved value or the default.
   */
  public String getStringProperty ( String propName, String defval )
    {
    Object risul = programProperty.getValue(propName);
    if ( risul == null )       
      {
      log.userPrintln(classname+"getStringProperty() propName="+propName+" is unknown");     
      return defval;
      }

    if ( risul instanceof String ) return (String)risul;

    log.userPrintln(classname+"getStringProperty() propName="+propName+" risul Class="+risul.getClass());     

    return defval;
    }


  /**
   * Returns the given property as boolean, it returns defVal if non existent.
   * The value is taken from the progrtam configuration pool.
   * @return the saved value or the default.
   */
  public boolean getProperty ( String propName, boolean defval )
    {
    Object risul = programProperty.getValue(propName);
    if ( risul == null )       
      {
      log.userPrintln(classname+"getBooleanProperty() propName="+propName+" is unknown");     
      return defval;
      }

    if ( risul instanceof Boolean ) return ((Boolean)risul).booleanValue();

    log.userPrintln(classname+"getBooleanProperty() propName="+propName+" risul Class="+risul.getClass());     

    return defval;
    }
    
  public void setVisible ( boolean flag )
    {
    workFrame.setVisible(flag);
    }
    
  public void setConfigChangeListener ( ConfigChangedInterface listener )
    {
    configChangeListener = listener;
    }
    
  /**
   * When the save button is pressed I come here
   */
  private void saveButtonPress()
    {
    setVisible(false);
    propertyStore.save();
    
    if ( configChangeListener != null )
      configChangeListener.configChangedCallback();
    }
      
  public Locale  getLabelsLocale()
    {
    return labels_locale;
    }
    
    
private class ButtonHandler implements ActionListener 
  {
  public void actionPerformed(ActionEvent event)
    {
    Object button = event.getSource();
    
    if ( button == saveButton ) saveButtonPress();
    }
  }


}

