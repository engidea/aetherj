package it.aetherj.backend.gui.config;

/**
 * This is better than the original golang, still can be included into the entity, that would be even better
 * @author damiano
 *
 */
public class EntitiesPageSize
  {
  public EntityPageSize board = new EntityPageSize(200,8000,30000);
  public EntityPageSize thread = new EntityPageSize(400,1600,30000);
  public EntityPageSize post = new EntityPageSize(400,1600,30000);
  public EntityPageSize votes = new EntityPageSize(400,1600,30000);
  public EntityPageSize keys = new EntityPageSize(400,1600,30000);
  public EntityPageSize truststates = new EntityPageSize(400,1600,30000);
  public EntityPageSize addresses = new EntityPageSize(400,1600,30000);
  
public static class EntityPageSize 
  {
  public final int dataSize;
  public final int indexSize;
  public final int manifestSize;
  
  public EntityPageSize(int dataSize, int indexSize, int manifestSize)
    {
    this.dataSize=dataSize;
    this.indexSize=indexSize;
    this.manifestSize=manifestSize;
    }
  }
  
  }
