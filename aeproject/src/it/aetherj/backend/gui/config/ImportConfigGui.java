/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.config;

import java.awt.BorderLayout;
import java.awt.event.*;
import java.io.File;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.WinposStore;
import it.aetherj.backend.gui.utils.*;
import it.aetherj.boot.LimitedTextArea;

/**
 * Provide a way to import configuration from prebious Aether
 * @param stat
 */
public class ImportConfigGui implements ComponentProviderSaveWinpos
  {
  private static final String classname="WuserManager.";
  
  private final ButtonListener buttonListener = new ButtonListener();

  private final Stat stat;

  private JPanel         workPanel;
  private JButton        loginButton;
  private JTextField     usernameField;
  private JPasswordField passwordField;
  private JLabel         messageLabel;
  private LimitedTextArea log;
  
  private File          configFile;
  private JButton       selectFileButton,importButton;
  private JLabel        selectedFileLabel;
  
  
  public ImportConfigGui(Stat stat)
    {
    this.stat = stat;
    
    log = new LimitedTextArea(1000);
    
    selectFileButton = stat.utils.newJButton("Choose File", buttonListener);
    importButton = stat.utils.newJButton("Import", buttonListener);
    
    selectedFileLabel = new JLabel();
    
    this.workPanel = newWorkPanel();
    }

  private JPanel newWorkPanel()
    {
    JPanel risul = new JPanel(new BorderLayout());
    
    risul.add(newNorthPanel(),BorderLayout.NORTH);
    risul.add(log.getComponentToDisplay(),BorderLayout.CENTER);
    risul.add(newSouthPanel(),BorderLayout.SOUTH);
    
    return risul;
    }
  
  private JPanel newNorthPanel()
    {
    SwingGridBagLayout gbl = new SwingGridBagLayout();
    
    gbl.addComponentWestIncx(selectFileButton);
    gbl.addComponentEastIncx(stat.labelFactory.newJLabel(classname, "Filename"));
    gbl.addComponentWestIncx(selectedFileLabel);
    
    
    return gbl.getJPanel();
    }

  
  
  private JPanel newSouthPanel ()
    {
    JPanel risul = new JPanel();
    
    risul.add(importButton);
    
    return risul;
    }
  
  @Override
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }
  

  @Override
  public void saveWindowPosition(WinposStore wpstore)
    {
    }

private final class ButtonListener implements ActionListener
  {
  @Override
  public void actionPerformed(ActionEvent event)
    {
    Object source = event.getSource();
    
    if ( source == selectFileButton )
      {
      int returnCode = stat.fileChooser.showOpenDialog(stat.console.getJFrame());
      if ( returnCode != JFileChooser.APPROVE_OPTION ) return;

      configFile = stat.fileChooser.getSelectedFile();
      selectedFileLabel.setText(configFile.getName());
      }
    
    if ( source == importButton )
      {
      if ( configFile == null )
        {
        log.print("ConfigFile is null");
        return;
        }
      
      if ( ! configFile.canRead() )
        {
        log.println("ConfigFile cannot read");
        return;
        }
            
//      JSONObject obj = stat.jsonUtils.newFromFile(configFile);
      
//      log.println("obj="+obj.toString(2));
      
      
      }
    }
  }


  
  }
