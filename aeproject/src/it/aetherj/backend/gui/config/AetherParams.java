/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.config;

import java.io.*;
import java.net.URL;
import java.security.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.utils.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.protocol.*;
import it.aetherj.shared.*;
import it.aetherj.shared.bim.*;
import it.aetherj.shared.v0.*;
import sun.security.x509.X509CertImpl;

/**
 * This is a bridge class from the raw configuration values, the GUI and the application
 */
public class AetherParams
  {
  private static final String classname="AetherParams";

  private static final String fname_parametersJson      = "parameters.json";         // the whole bundle of parameters
  private static final String fname_parametersJsonSaved = "parameters.json.saved";   // the whole bundle of parameters
  
  private static final int val_bestAddressCountMin  = 3;
  private static final int val_minPowLength         = 16;
  
  private static final int min_mimobj_expire_mn     = 6;     // less than 6 months is useless
  private static final int val_mimobj_expire_mn     = 12;    // start with a preset value of one year

  private static final int min_bestAddressEvery_s   = 120;
  private static final int val_bestAddressEvery_s   = 240;
  
  public static final int min_PowTimeout_s=10;
  public static final int max_PowTimeout_s=600;
  public static final int val_PowTimeout_s=90;
  
  public static final String keystorePass="password";
  public static final String keystoreEntry="AetherJ";

  private final Stat stat;
  private final PrintlnProvider feedback;

  private AetherParamsJson pjson = new AetherParamsJson();
  
  private PublicKey publicKey;             // this is the one used by MIM server
  private MimPublicKey mimPublicKey=new MimPublicKey();         //
  private MimFingerprint nodeFingerprint=new MimFingerprint();  // Node ID is the fingerprint of the backend's public key
  
  private PrivateKey privateKey;   // this is the one used by MIM server

  // the following are used ONLY for the web link, NOT to encrypt/decrypt the mim pieces/signatures
  private final CryptoRSA cryptoRSA;
  
  private PublicKey webPublicKey;    // this is the one used by WEB server
  private PrivateKey webPrivateKey;  // this is the one used by WEB server
  private X509CertImpl X509cert;
  private KeyStore webKeystore;
  
  private volatile MimTimestamp mimobjExpireTime;   // a mim object that has an update time BEFORE this time can be DELETED
    
  public AetherParams(Stat stat, PrintlnProvider feedback)
    {
    this.stat = stat;
    this.feedback = feedback;
    
    cryptoRSA   = new CryptoRSA(feedback);
    }

  /**
   * This is guarantee to return a not null value
   * If there are issues the value generated will be 2yr in the past, meaning that anything after that date is valid
   * @return
   */
  public synchronized MimTimestamp getMimobjExpireTime ()
    {
    // go back in time of roughly two years
    if ( mimobjExpireTime == null )
      updateMimobjExpireTime(24);
    
    return mimobjExpireTime;
    }
  
  /**
   * This MUST be called once a day to update the value, since it is a value that depends on current date
   * @param months the number of months to go back in time, positive number
   */
  private void updateMimobjExpireTime ( int months )
    {
    MimTimestamp now = new MimTimestamp();
    // go back in time of roughly two years
    mimobjExpireTime = now.add_months(-months);
    }
  
  public int getMimobjExpireMn()
    {
    if ( pjson.mimobj_expire_mn < min_mimobj_expire_mn )
      pjson.mimobj_expire_mn=min_mimobj_expire_mn;

    // no upper limit...
    
    return pjson.mimobj_expire_mn;
    }

  public void setMimobjExpireMn ( String value )
    {
    pjson.mimobj_expire_mn = Aeutils.convStringToInteger(value, val_mimobj_expire_mn);

    // the following will adjust, if needed
    getMimobjExpireMn();
    
    updateMimobjExpireTime(pjson.mimobj_expire_mn);
    }

  public int getBestAddressEvery_s()
    {
    if ( pjson.bestAddressEvery_s < min_bestAddressEvery_s )
      pjson.bestAddressEvery_s=min_bestAddressEvery_s;

    // no upper limit...
    
    return pjson.bestAddressEvery_s;
    }

  public void setBestAddressEvery_s ( String value )
    {
    pjson.bestAddressEvery_s = Aeutils.convStringToInteger(value, val_bestAddressEvery_s);
    // the following will adjust, if needed
    getBestAddressEvery_s();
    }

  
  public int getBestAddressCount()
    {
    if ( pjson.bestAddressCount < val_bestAddressCountMin )
      pjson.bestAddressCount=val_bestAddressCountMin;
    
    if ( pjson.bestAddressCount > 20 )
      pjson.bestAddressCount=20;
    
    return pjson.bestAddressCount;
    }

  public void setBestAddressCount ( String value )
    {
    pjson.bestAddressCount = Aeutils.convStringToInteger(value, val_bestAddressCountMin);
    // the following will adjust, if needed
    getBestAddressCount();
    }

  public int getPloadPowLength()
    {
    if ( pjson.ploadPowLength < val_minPowLength )
      pjson.ploadPowLength=val_minPowLength;
    
    if ( pjson.ploadPowLength > 30 )
      pjson.ploadPowLength=30;
    
    return pjson.ploadPowLength;
    }

  public void setPloadPowLength ( String value )
    {
    pjson.ploadPowLength = Aeutils.convStringToInteger(value, val_minPowLength);
    // the following will adjust, if needed
    getPloadPowLength();
    }

  public int getPowTimeout_s()
    {
    if ( pjson.mimPowTimeout_s < min_PowTimeout_s )
      pjson.mimPowTimeout_s=min_PowTimeout_s;
    
    if ( pjson.mimPowTimeout_s > max_PowTimeout_s )
      pjson.mimPowTimeout_s=max_PowTimeout_s;
    
    return pjson.mimPowTimeout_s;
    }

  public MimPowParams getPowParams ()
    {
    return new MimPowParams(getPloadPowLength(),getPowTimeout_s());
    }

  public void setPowTimeout_s ( String value )
    {
    pjson.mimPowTimeout_s = Aeutils.convStringToInteger(value, val_PowTimeout_s);
    // the following will adjust, if needed
    getPowTimeout_s();
    }

  public String getNodeName()
    {
    return pjson.mimNodeName;
    }
  
  public void setNodeName(String name)
    {
    if ( name == null || name.length() < 3 )
      return;
    
    pjson.mimNodeName= name;
    }
  
  public String getBootLocation()
    {
    return pjson.bootLocation;
    }
  
  public void setBootLocation (String location )
    {
    try
      {
      if ( ! location.startsWith("https://"))
        location="https://"+location;

      URL url = new URL(location);
      
      String address = url.getHost();
      
      if ( address.length() < 5 )
        address="bootstrap.getaether.net";
      
      int port=url.getPort();
      
      if ( port < 0 ) port = 443;
      
      pjson.bootLocation=address+":"+port;
      }
    catch ( Exception exc )
      {
      feedback.println("setBootLocation", exc);
      }
    }
  
  public MimAddress getBackendAddress ()
    {
    return new MimAddress(getBackendPort());
    }

  public int getBackendPort ()
    {
    return pjson.nodeWebPort;
    }
  
  public MimPublicKey getMimPublicKey ()
    {
    return mimPublicKey;
    }
  
  public MimFingerprint getNodeFingerprint ()
    {
    return nodeFingerprint;
    }

  
  public PrivateKey getPrivateKey ()
    {
    return privateKey;
    }
  
  public void setBackendPort ( String port )
    {
    setBackendPort(Aeutils.convStringToInteger(port, 80));
    }
  
  public void setBackendPort ( int port )
    {
    if ( port < 0 || port > 0xFFFF )
      throw new IllegalArgumentException("Invalid port "+port);
    
    pjson.nodeWebPort=port;
    }
  
  /**
   * If it fails loading, no big deal, I will create a reasonable set
   */
  private void attemptLoadParameters ()
    {
    File fromfile = new File(stat.datastoreDir,fname_parametersJson);
    
    if ( ! fromfile.canRead() )
      {
      println("attemptLoadParameters: cannot read "+fromfile);
      return;
      }
    
    try
      {
      BufferedReader reader = new BufferedReader(new FileReader(fromfile));
      AeJson json = new AeJson();
      pjson = json.readValue(reader, AetherParamsJson.class);
      }
    catch ( Exception exc )
      {
      feedback.println("attemptLoadParameters",exc);
      }
    }
    
  /**
   * So, to create a new identity, if you do NOT have one, just SAVE the configuration and it will be "recreated" on next boot
   * @return true if I managed to load the parameters or create a new bunch of them
   */
  public void initializeDone() throws JsonMappingException, JsonProcessingException
    {
    // Attempt to load parameters from filesystem
    attemptLoadParameters();

    // this will give an initial set of values to display
    stat.aeParamsGui.refreshGui();

    try
      {
      // if it fails default will be created
      privateKey = CryptoEddsa25519.parsePrivateKey(pjson.mimNodePrivateKey);
      publicKey = CryptoEddsa25519.parsePublicKey(pjson.mimNodePublicKey);
      }
    catch ( Exception exc )
      {
      newPublicPrivateKeys();
      }

    // I can now do this, since I have the values setup
    mimPublicKey = new MimPublicKey(publicKey);
    nodeFingerprint = mimPublicKey.getFingerprint();

    try
      {
      // if it fails default will be created
      webPrivateKey = cryptoRSA.parsePrivateKey(pjson.webPrivateKey);
      webPublicKey = cryptoRSA.parsePublicKey(pjson.webPublicKey);
      }
    catch ( Exception exc )
      {
      newWebPublicPrivateKeys();
      }

    try
      {
      // if it fails default will be created
      X509cert = cryptoRSA.parseX509Cert(pjson.webCertificate);
      }
    catch ( Exception exc )
      {
      newX509cert();
      }
    
    try
      {
      // This should not fail, never
      webKeystore = newKeyStore();
      }
    catch ( Exception exc )
      {
      feedback.println("newKeystore FAILED", exc);
      // I need to know ASAP that this failed
      throw new IllegalArgumentException("new KeyStore FAILED");
      }
        
    updateMimobjExpireTime(pjson.mimobj_expire_mn);
    
    // now, publish official parameters
    stat.aeParamsGui.refreshGui();
    }
  
  /**
   * Build a payload holding the default info of a the given entity, it has to be new since timestamp
   */
  public MimPayload newMimPayload(MimEntity entity)  
    {
    MimPayload payload = new MimPayload(entity);
    
    payload.endpoint=new MimEndpoint(entity);
    payload.address=getBackendAddress();
    payload.node_public_key=getMimPublicKey();
    payload.nonce = new MimNonce();

    return payload;
    }

  /**
   * Build a payload holding the default info of a the given entity, it has to be new since timestamp
   */
  public BimPayload newBimPayload(MimEntity entity)  
    {
    BimPayload payload = new BimPayload(entity);
    
    payload.address=new BimAddress(getBackendPort());
    payload.node_public_key=getMimPublicKey();
    payload.nonce = new MimNonce();

    return payload;
    }

  private void newX509cert ()
    {
    try 
      {
      println("   Creating new X509 certificate");
      X509cert = cryptoRSA.newX509cert(webPrivateKey, webPublicKey);
      }
    catch ( Exception exc )
      {
      feedback.println(classname+".newX509cert", exc);
      }
    }

  
  public String getX509cert_s ()
    {
    StringBuilder risul = new StringBuilder(2000);
    risul.append("-----BEGIN CERTIFICATE-----");
    risul.append('\n');
    risul.append(cryptoRSA.toBase64(X509cert));
    risul.append('\n');
    risul.append("-----END CERTIFICATE-----");
    
    return risul.toString();
    }
  
  public X509CertImpl getX509cert ()
    {
    return X509cert;
    }
    
  private void newWebPublicPrivateKeys ()
    {
    try 
      {
      println("   Creating new WEB Public and private key");

      KeyPair kp =cryptoRSA.newKeyPair();
      webPublicKey = kp.getPublic();
      webPrivateKey = kp.getPrivate();
      }
    catch ( Exception exc )
      {
      feedback.println(classname+".newWebPublicPrivateKeys", exc);
      }
    }

  private void newPublicPrivateKeys ()
    {
    try 
      {
      println("   Creating new MIM Public and private key");

      KeyPair kp = CryptoEddsa25519.newKeyPair();
      publicKey  = kp.getPublic();
      privateKey = kp.getPrivate();
      }
    catch ( Exception exc )
      {
      feedback.println(classname+".newPublicPrivateKeys", exc);
      }
    }

  public KeyStore getKeyStore ()
    {
    return webKeystore;
    }
  
  private KeyStore newKeyStore( ) throws Exception 
    {
    KeyStore keystore = KeyStore.getInstance("JKS");
    
    // this is needed otherwise it throws exception of Keystore not initialized
    keystore.load(null);
    
    java.security.cert.Certificate []carr = new java.security.cert.Certificate[1];
    carr[0]=X509cert;
        
    keystore.setKeyEntry(keystoreEntry, webPrivateKey, keystorePass.toCharArray(), carr);
    
    return keystore;
    }

  public void saveToDatastoreOnExit( ) 
    {
    saveToDatastore();
    }
  
 
  
  /**
   * There should be only one running at a given time
   * @throws JsonProcessingException
   */
  public synchronized void saveToDatastore( )
    {
    pjson.mimNodePrivateKey = CryptoEddsa25519.toHex(privateKey);
    pjson.mimNodePublicKey  = CryptoEddsa25519.toHex(publicKey);
    pjson.webPrivateKey     = cryptoRSA.toHex(webPrivateKey);
    pjson.webPublicKey      = cryptoRSA.toHex(webPublicKey);
    pjson.webCertificate    = cryptoRSA.toBase64(X509cert);

    AeJson json = new AeJson();
    File tofile = new File(stat.datastoreDir,fname_parametersJson);
    File savefile = new File(stat.datastoreDir,fname_parametersJsonSaved);

    if ( tofile.exists() )
      {
      if ( tofile.renameTo(savefile) )
        println("saveToDatastore: mv OK "+tofile);
      else
        println("saveToDatastore: mv KO "+tofile);
      }
    
    try
      {
      String pstring = json.writeValueAsStringPretty(pjson);
      PrintWriter writer = new PrintWriter(tofile);
      writer.write(pstring);
      writer.close();
      }
    catch ( Exception exc )
      {
      stat.log.exceptionShow(classname+".saveToDatastore", exc);
      }
    
    println("saveToDatastore: "+tofile);
    }
  
  private void println(String message)
    {
    feedback.println(message);
    }
  
  
  public String toString()
    {
    return pjson.toString();
    }
  }
