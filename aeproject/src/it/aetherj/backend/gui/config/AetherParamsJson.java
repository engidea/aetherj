package it.aetherj.backend.gui.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * I need a way to save the params in a kind of portable way, so, you can migrate/restore the server
 * I cannot just Json the AetherParams since there is quite a lot of extra stuff that would be sent
 * So, this is the container holding all previous values
 * I should have done this earlier...
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class AetherParamsJson
  {
  public String mimNodeName="AetherJ Node"; 
  public String mimNodePublicKey;    
  public String mimNodePrivateKey;
  
  public String webPublicKey;
  public String webPrivateKey;
  public String webCertificate;
  
  public String bootLocation;
  public int nodeWebPort;        // the TCP port the back end is listening to, actual value
  public int bestAddressCount;   // how many best addresses do we wish to work ?

  public int ploadPowLength;       // The POW difficulty to use for payload
  public int mimPowTimeout_s;
  public int bestAddressEvery_s;
  public int mimobj_expire_mn;

  public AetherParamsJson()
    {
    //bootstrap.getaether.net
    bootLocation = "https://2.197.20.219:9443";
    nodeWebPort=9443;
    }
  
  public String toString()
    {
    return "nodeWebPort="+nodeWebPort;
    }
  }
