/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.images;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.*;
import javax.swing.border.*;

import it.aetherj.backend.Stat;

public final class ImageProvider
  {
  private static final String classname="ImageProvider.";
  
  public static final String ONLINE  = "online.gif";
  public static final String OFFLINE = "offline.gif";
  public static final String LOGOBIG = "logo.jpg";
  public static final String LOGOMEDIUM = "logo120x116.gif";
  public static final String EDIT_SMALL  = "edit.gif";
  public static final String FORWARD_SMALL  = "forward.gif";
  public static final String DELETE_SMALL  = "trash-small.png";
  
  public static final String CKTRUE="checkbox-true.png";
  public static final String CKFALSE="checkbox-false.png";
  public static final String CKNULL="checkbox-null.png";

  
  private final Stat stat;
    
  public ImageProvider (Stat stat)
    {
    this.stat = stat;
    }
  
  private String getLabel (String label)
    {
    // if no translation available just return the value
    if (stat.labelFactory == null ) return label;
    
    return stat.labelFactory.getLabel(classname,label);
    }
  
  
  /**
   * returns an Icon that is in this directory with the given name, including the extension.
   * @param name
   * @return
   */
  public ImageIcon getIcon ( String name )
    {
    try
      {
      URL aUrl = getClass().getResource(name);
      return new ImageIcon(aUrl);
      }
    catch ( Exception exc)
      {
      System.err.println(classname+"getIcon: cannot find file="+name);
      // In any case I return an icon, to avoid messing up the code.
      return new ImageIcon();
      }
    }
    

  private JButton newButton ( ImageIcon icon )
    {
    if ( icon == null ) return new JButton("Missing ICON");
    
    JButton button = new JButton(icon);
    
    button.setBorder(new BevelBorder(BevelBorder.RAISED));
    
    return button;
    }

  public JButton newButton ( String fname )
    {
    ImageIcon icon = getIcon(fname);
    JButton button = newButton(icon);
    return button;
    }

  public JButton newButton ( String fname, ActionListener listener )
    {
    JButton risul = newButton(fname);
    
    if ( risul == null ) return risul;
    
    if ( listener != null ) risul.addActionListener(listener);
    
    return risul;
    }
  
  public JButton newSimageButton (ActionListener listener)
    {
    JButton risul =  newButton("color_swatch.png",listener);
    risul.setToolTipText(getLabel("Image Manager"));
    return risul;
    }

  public JButton newTickButton (ActionListener listener)
    {
    JButton risul =  newButton("tick_32x32.png",listener);
    risul.setToolTipText(getLabel("Confirm Choice"));
    return risul;
    }

  public JButton newEditSmallButton (ActionListener listener)
    {
    JButton risul = newButtonTidy(EDIT_SMALL,listener);
    risul.setToolTipText(getLabel("Edit"));
    return risul;
    }

  public JButton newCameraEditButton (ActionListener listener)
    {
    JButton risul = newButton("camera_edit.png",listener);
    risul.setToolTipText(getLabel("Camera Edit"));
    return risul;
    }
  
  /**
   * Can be used to save to disk or to CEMI, really
   * @param listener
   * @return
   */
  public JButton newSaveButton (ActionListener listener)
    {
    JButton risul = newButtonTidy("save-small.gif",listener);
    risul.setToolTipText(getLabel("Save"));
    return risul;
    }

  public JButton newSaveToDisk (ActionListener listener)
    {
    JButton risul = newButtonTidy("save-to-disk.png",listener);
    risul.setToolTipText(getLabel("Save To Disk"));
    return risul;
    }

  public JButton newLoadFromDisk (ActionListener listener)
    {
    JButton risul = newButtonTidy("load-from-disk.png",listener);
    risul.setToolTipText(getLabel("Load From Disk"));
    return risul;
    }

  
  public JButton newClearButton (ActionListener listener)
    {
    JButton risul = newButton("clear-small.gif",listener);
    risul.setToolTipText(getLabel("Clear"));
    return risul;
    }

  public JButton newOfflineButton (ActionListener listener)
    {
    JButton risul = newButton(OFFLINE,listener);
    risul.setToolTipText("Connection");
    return risul;
    }

  public JButton newGetDataButton (ActionListener listener)
    {
    JButton risul = newButtonTidy ("get-data-small.png",listener);
    risul.setToolTipText(getLabel("Get Data From CEMI"));
    return risul;
    }
  
    
  public JButton newDeleteButton (ActionListener listener)
    {
    JButton risul =  newButtonTidy(DELETE_SMALL,listener);
    risul.setToolTipText("Delete");
    return risul;
    }
    
  public JButton newNewButton (ActionListener listener)
    {
    JButton risul = newButtonTidy("new-small.gif",listener);
    risul.setToolTipText(getLabel("New"));
    return risul;
    }

  public JButton newOpenFileButton (ActionListener listener)
    {
    JButton risul = newButtonTidy("open-file-small.gif",listener);
    risul.setToolTipText("Open");
    return risul;
    }

  public JButton newRefreshButton (ActionListener listener)
    {
    JButton risul = newButtonTidy("refresh-22x22.png",listener);
    risul.setToolTipText(getLabel("Refresh"));
    return risul;
    }

  public JButton newForwardButton (ActionListener listener)
    {
    JButton risul =  newButton(FORWARD_SMALL,listener);
    risul.setToolTipText("Jump Forward");
    return risul;
    }
  

  public JButton newPrintButton (ActionListener listener)
    {
    JButton risul =  newButton("printer-small.gif",listener);
    risul.setToolTipText(getLabel("Print"));
    return risul;
    }

  public JButton newPlusButton ()
    {
    JButton risul = newButton("plus-small.gif");
    risul.setToolTipText("Add object");
    return risul;
    }

  public JButton newExecuteButton (ActionListener listener)
    {
    JButton risul = newButtonTidy("execute-22x22.png",listener);
    risul.setToolTipText("Execute action");
    return risul;
    }


  public JButton newButtonTidy ( String iconFname, ActionListener listener )
    {
    return newButtonTidy(getIcon(iconFname),listener);
    }
  
  /**
   * A button that is tidy, meaning that the border is just 1px black 
   * You can change the background, if you wish....
   * @param icon
   * @param listener
   * @return
   */
  public JButton newButtonTidy ( ImageIcon icon, ActionListener listener )
    {
    JButton risul = new JButton(icon);
    
    if ( listener != null ) risul.addActionListener(listener);
    
    javax.swing.border.Border border = new CompoundBorder(new LineBorder(Color.black,1), new EmptyBorder(1,1,1,1));

    risul.setBorder(border);
    
    risul.setBackground(Color.white); 
    
    return risul;
    }
    
  }
