/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui;

import java.awt.BorderLayout;
import java.awt.event.*;

import javax.swing.*;

import bsh.Interpreter;
import bsh.util.JConsole;
import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.utils.ConsoleFrame;
import it.aetherj.boot.ComponentProvider;

/**
 * This is used to manage giving commands to database and see results.
 * It should be instantiated later since it makes use of bits of stat.
 */
public final class BshFrame implements ComponentProvider
  {  
  private final String classname="BshFrame";
  private final Stat  stat; 

  private final ConsoleFrame workFrame;  
  private final JPanel       workPanel;  
  private final Interpreter  bshInterp;
  private final JConsole     bshConsole;

  private JButton helpBtn;
  
  /**
   * What will be available is a panel where languages are managed.
   */
  public BshFrame(Stat stat)
    {
    this.stat = stat;

    workFrame = new ConsoleFrame ("Beanshell",JFrame.HIDE_ON_CLOSE);

    workPanel = (JPanel)workFrame.getContentPane();

    bshConsole = new JConsole();
    workPanel.add(bshConsole,BorderLayout.CENTER); 
    workPanel.add(newButtons(),BorderLayout.NORTH);
    
    // need to set it here otherwise it gets lost and therefore, reset
    workFrame.setBounds(stat.winposStore);
    
    bshInterp = new Interpreter(bshConsole);

    try
      {
      bshInterp.set("stat",stat);
      }
    catch ( Exception exc )
      {
      stat.log.exceptionShow(classname+"BshPanel",exc);  
      }
    
    Thread aThread = new Thread(bshInterp);
    aThread.setName("BeanShell");
    aThread.setPriority(Thread.MIN_PRIORITY);
    aThread.setDaemon(true);

    // You NEED to provide the correct class loader to beanshell to see the classes correctly !
    // The following does NOT work
    //  BshClassManager mgr = bshInterp.getClassManager();
    //  mgr.setClassLoader(aLoader);
    aThread.setContextClassLoader(getClass().getClassLoader());

    aThread.start();
    }
 
  
  public void saveWindowPosition(WinposStore w)
    {
    workFrame.saveWindowPosition(w);
    }
  
  private JPanel newButtons ()
    {
    JPanel aPanel = new JPanel();
    helpBtn = new JButton("Help");
    helpBtn.addActionListener(new HelpClass());
    
    aPanel.add(helpBtn);
    
    return aPanel;
    }
    
  public Interpreter getInterpreter ()
    {
    return bshInterp;
    }

  /**
   * Returns the panel to display.
   */
  public JComponent getComponentToDisplay ()
    {
    return workPanel;
    }
    
  /**
   * Display this console in a separate frame.
   */
  public void setVisible ( boolean visible )
    {
    workFrame.setVisible(visible);
    }
    
private final class HelpClass implements ActionListener
  {
  public void actionPerformed(ActionEvent e)
    {
    String msg = "available object: stat\n"+
    "   use exmple: (mask)(level)\n" +
    "      print HTTP rx+tx at debug level\n"+
		"      stat.dbg.wishMaskLevel(0x18,0x1)";

    bshConsole.println(msg);
		}

  }
  } 
