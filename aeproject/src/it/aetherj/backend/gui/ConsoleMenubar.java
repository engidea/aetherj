/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui;

import java.awt.event.*;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.config.ConfigChangedInterface;
import it.aetherj.backend.gui.utils.HelpProvider;


public final class ConsoleMenubar implements ConfigChangedInterface
  {
  private static final String classname="ConsoleMenubar.";

  private final Stat stat;
  private final BarMenuListener menuListener = new BarMenuListener();
  
  private JMenuBar  menuBar;
  private JMenuItem logMenuItem;
  private JMenuItem configfMenuItem;
  private JMenuItem importConfigMenuItem;
  private JMenuItem schemaMgrMenuItem;
  private JMenuItem userMgrMenuItem;

  private JMenuItem saveWinposMenuItem;
  private JMenuItem beanshellMenuItem;
  
  private JMenuItem aebendMenuItem,mcwConsoleMenuItem;
  private JMenuItem controllerMenuItem;
  private JMenuItem aboutMenuItem;  
  private JMenuItem helpMenuItem;  
  private JMenuItem infoFrameMenuItem;  
  private JMenuItem sqlQeryMenuItem;
    
  public ConsoleMenubar(Stat stat)
    {
    this.stat = stat;
    }
    
  public JMenuBar getMenuBar()
    {
    if ( menuBar != null ) 
      throw new IllegalArgumentException("you cannot call getMenuBar twice");
    
    menuBar = new JMenuBar();
    menuBar.setToolTipText(classname);
    
    menuBar.add(getAebendMenu());
    menuBar.add(getConfigurationMenu());
    menuBar.add(getToolsMenu());
    menuBar.add(Box.createHorizontalGlue());
    menuBar.add(newHelpMenu());

    // this one will verify which menus are active or not, based on the config
    configChangedCallback();
    
    return menuBar;
    }

  /**
   * Utility for shorter code
   * @param label
   * @return
   */
  private String getLabel(String label)
    {
    return stat.labelFactory.getLabel(classname,label);
    }
    
  private JMenu newHelpMenu()
    {
    JMenu aMenu = new JMenu(getLabel("Help"));

    helpMenuItem = newJMenuItem(aMenu,getLabel("User Manual"));
    aboutMenuItem = newJMenuItem(aMenu,getLabel("Program Version"));
    
    return aMenu;
    }    


  private JMenu getAebendMenu()
    {
    JMenu aMenu = new JMenu(getLabel("Components"));
    
    controllerMenuItem = newJMenuItem(aMenu,getLabel("Main Controller"));
    mcwConsoleMenuItem = newJMenuItem(aMenu,getLabel("Mim Client Worker"));
    userMgrMenuItem = newJMenuItem(aMenu,getLabel("Manage Webusers"));

    return aMenu;
    }


  private JMenu getConfigurationMenu()
    {
    JMenu aMenu = new JMenu(getLabel("Configuration"));
    
    configfMenuItem = newJMenuItem(aMenu,getLabel("Program Configuration"));
    importConfigMenuItem = newJMenuItem(aMenu,getLabel("Import Configuration"));
    schemaMgrMenuItem = newJMenuItem(aMenu,getLabel("Schema Manager"));

    return aMenu;
    }

  private JMenu getToolsMenu()
    {
    JMenu aMenu = new JMenu(getLabel("Tools"));
    
    logMenuItem = newJMenuItem(aMenu,getLabel("Show Log"));
    infoFrameMenuItem = newJMenuItem(aMenu,getLabel("Informations"));
    saveWinposMenuItem = newJMenuItem(aMenu,getLabel("Save Window Position"));
    beanshellMenuItem = newJMenuItem(aMenu,"Show Beanshell");
    sqlQeryMenuItem = newJMenuItem(aMenu,"Sql Query");
    
    return aMenu;
    }
  
  private JMenuItem newJMenuItem ( JMenu parent, String name )
    {
    JMenuItem risul = new JMenuItem(name);
    
    risul.addActionListener(menuListener);
    parent.add(risul);
    
    return risul;
    }

    
  private void showHelpManuale ()
    {
    HelpProvider provider = new HelpProvider(stat,HelpProvider.aetherj_man_key);
    
    provider.actionPerformed(null);
    }
    
  private void showAboutInformation()
    {
    stat.console.showMessageDialog("AetherJ 1.x\n"+stat.appVersion);
    }
    
  public void configChangedCallback()
    {
    }
  
private final class BarMenuListener implements ActionListener
  {
  public void actionPerformed(ActionEvent event)
    {
    Object source = event.getSource();
    
    if ( source == logMenuItem ) 
      stat.log.setVisible(true);
    else if ( source == configfMenuItem ) 
      stat.config.setVisible(true);
    else if ( source == importConfigMenuItem ) 
      stat.console.setContent(stat.importConfig);
    else if ( source == schemaMgrMenuItem ) 
      stat.console.setContent(stat.dbSchemaManager);
    else if ( source == saveWinposMenuItem ) 
      stat.console.saveWindowPosition();
    else if ( source == beanshellMenuItem ) 
      stat.beanshell.setVisible(true);
    else if ( source == sqlQeryMenuItem ) 
      stat.sqlquery.setVisible(true);
    else if ( source == controllerMenuItem ) 
      stat.console.setContent(stat.aebendConsole);
    else if ( source == mcwConsoleMenuItem ) 
      stat.console.setContent(stat.mcwConsole);
    else if ( source == userMgrMenuItem ) 
      stat.console.setContent(stat.wuserManager);
    else if ( source == aebendMenuItem ) 
      stat.console.setContent(stat.backendPanel);
    else if ( source == aboutMenuItem ) 
      showAboutInformation();
    else if ( source == infoFrameMenuItem ) 
      stat.infoFrame.setVisible(true);
    else if ( source == helpMenuItem ) 
      showHelpManuale();
    }
  }
  }
