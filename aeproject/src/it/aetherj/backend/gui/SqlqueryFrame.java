/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.sql.*;
import java.util.Vector;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.gui.utils.ConsoleFrame;
import it.aetherj.boot.*;
import it.aetherj.boot.dbase.*;
import it.aetherj.shared.Aeutils;
 
 
/**
 * This is used to manage giving commands to database and see results
 * To execute commands you MUST select the command and then press enter 
 * Enter alone does not do anything anymore.
 */
public final class SqlqueryFrame implements PrintlnProvider
  {  
  private final ButtonClickHander clickHandler = new ButtonClickHander();
  private final Stat stat;
  
  private final JPanel      workPanel;
  private final ConsoleFrame workFrame;
  private final LimitedTextArea queryOut;
  private final JTable      workTable;
  private final JScrollPane tableScrollPane;

  private JTextArea   queryIn;
  private final JButton     tableButton,executeButton;
  
  private final JTextField  schemaField,tblnameField, typeField;  // The user can write what schema to use.
  
  private int executeCounter;
  
  public SqlqueryFrame(Stat p_stat)
    {
    stat = p_stat;

    schemaField = new JTextField("",10);  
    schemaField.setToolTipText("a schema name pattern; must match the schema name as it is stored in the database");
    schemaField.setBorder(new TitledBorder("Schema"));
    
    tblnameField = new JTextField("",10);  
    tblnameField.setToolTipText("table name must match as it is stored in the database");
    tblnameField.setBorder(new TitledBorder("Tbl Name"));

    typeField = new JTextField("TABLE",10);  
    typeField.setToolTipText("TABLE, INDEX, VIEW ... ");
    typeField.setBorder(new TitledBorder("Type"));
    
    // We need a button to show the DB tables
    tableButton = new JButton("Tables");
    tableButton.addActionListener(clickHandler);
    
    // I need a button to execute a script
    executeButton = new JButton("Execute Script");
    executeButton.addActionListener(clickHandler);

    // Output for plain text
    queryOut = new LimitedTextArea(500);

    // Output for tabular result
    workTable  = new JTable();
    workTable.setCellSelectionEnabled(true);
    tableScrollPane = new JScrollPane(workTable);

    workPanel = new JPanel(new BorderLayout());
    
    JSplitPane splitOut  = new JSplitPane(JSplitPane.VERTICAL_SPLIT, queryOut.getComponentToDisplay(),tableScrollPane);
    splitOut.setDividerLocation(0.2);
    splitOut.setResizeWeight(0.2);    
    
    JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, newInputPane(),splitOut);
    splitPane.setDividerLocation(0.2);
    splitPane.setResizeWeight(0.2);    

    workPanel.add(splitPane,BorderLayout.CENTER);
    
    // I also want to be able to show this into a separate Frame.    
    workFrame = new ConsoleFrame("SQL Query",JFrame.HIDE_ON_CLOSE);
    workFrame.setContentPane(workPanel);

    // need to set it here otherwise it gets lost and therefore, reset
    workFrame.setBounds(stat.winposStore);
    }
    
  /**
   * Display this console in a separate frame.
   */
  public void setVisible ( boolean visible )
    {
    workFrame.setVisible(visible);
    }

  @Override
  public void println(String message)
    {
    queryOut.println(message);
    }
  
  @Override
  public void println(String message, Throwable exception)
    {
    queryOut.println(message, exception);
    }

  /**
   * Other part of the system can add instructions to be executed, so they are already in proper position
   * to be executed
   * @param sqlInstruction
   */
  public void appendSqlInstruction ( String sqlInstruction )
    {
    queryIn.append(sqlInstruction+"\n"); 
    }
  
  /**
   * This will return the input pane, the one with the buttons
   */
  private JPanel newInputPane()
    {
    // Input area
    queryIn = new JTextArea();
    queryIn.setLineWrap(true);
    queryIn.addKeyListener(new QueryKeyAdapter());
    queryIn.setToolTipText("Write your SQL code here, select the code you wish to run and presse ENTER");
    JScrollPane inScrollPane = new JScrollPane(queryIn);
    inScrollPane.setPreferredSize(new Dimension(400,100));

    // We need to put the two above into a border Layout pane
    JPanel risul = new JPanel(new BorderLayout());
    risul.add(newNorthPanel(),BorderLayout.NORTH);
    risul.add(inScrollPane,BorderLayout.CENTER);

    return risul;
    }


  private JPanel newNorthPanel ()
    {
    JPanel risul = new JPanel();
    risul.add(schemaField);
    risul.add(tblnameField);
    risul.add(typeField);
    risul.add(tableButton);
    risul.add(executeButton);
    
    return risul;
    }
  

  /**
   * This executes a SQL line, we will see about returning results....
   */
  private void executeSql ( String sqlCmd )
    {
    Thread thread = new Thread(new ExecuteSqlLine(sqlCmd));
    thread.setDaemon(true);
    thread.setName("Execute SQL Line");
    thread.start();
    }
    
  private String [] getTypes ()
    {
    String type = typeField.getText();
    
    if ( type.length() < 1 ) 
      return null;
    
    String [] types = {type};
    
    return types;  
    }
    
private class ShowDbaseTables implements Runnable
    {
    public void run()
      {
      String schema = schemaField.getText();
      
      if ( schema.length() < 1 ) 
        schema = null;
  
      String name = tblnameField.getText();
        
      if ( name.length() < 1 ) 
        name = null;

      try
        {
        AeDbase dbase = stat.newWorkDbase();
        DatabaseMetaData meta = dbase.getMetaData();
        Brs result = new Brs (dbase, null, meta.getTables(null,schema,name,getTypes()));  
        SwingUtilities.invokeAndWait(new ShowResultset(dbase, result ));
        result.close();
        dbase.close();
        }
      catch ( Exception exc )
        {
        stat.println("ShowDbaseTables", exc);        
        }
      }
    }

private class ShowResultset implements Runnable
  {
  private AeDbase dbase;
  private Brs rs;
  
  public ShowResultset(AeDbase dbase, Brs rs)
    {
    this.dbase=dbase;
    this.rs=rs;
    }

  /**
   * This will show the result of an update in a table like form
   * NOTE that the dbase MUST be connected for this to work...
   */
  public void run (  )
    {
    try
      {
      ResultSetMetaData meta = rs.getMetaData();
      int colCount = meta.getColumnCount(); 
      if ( colCount < 1 )
        {
        println("showRs: colCount < 1 ");
        return;
        }

      // This takes care of the header
      Vector<String> tableHeader = new Vector<String>(colCount);
      
      for (int index = 0; index < colCount; index++) 
        tableHeader.add(meta.getColumnLabel(index + 1));
      
      DefaultTableModel tableModel = new DefaultTableModel (tableHeader,0);

      // Now we take care of the data
      while ( rs.next() ) 
        {
        Object [] tableRow = new Object[colCount];
        
        for (int curCol = 0; curCol < colCount; curCol++) 
          tableRow[curCol] = rs.getObject(curCol + 1);

        tableModel.addRow(tableRow);
        }

      workTable.setModel(tableModel);
      tableScrollPane.setViewportView(workTable);
      
      rs.close();
      dbase.close();
      }        
    catch ( Exception exc )
      {
      println("showSelectRisul",exc);
      }    
    }
  }

  
private class ExecuteSqlLine implements Runnable
  {
  String sqlCmd;
  
  ExecuteSqlLine ( String p_line )
    {
    sqlCmd = p_line;
    }
  
  @Override
  public void run ()
    {
    
    try
      {
      AeDbase dbase = stat.newWorkDbase();
      
      Ris ris = dbase.execute(sqlCmd);
      
      if ( ris.getException() != null ) 
        {
        println ("ExecuteSqlLine.run:",ris.getException());
        ris.close();
        dbase.close();
        return;
        }
      
      if ( ris.getResultType() ) 
        SwingUtilities.invokeAndWait(new ShowResultset(dbase, ris.getResultSet()));
      else
        showUpdateRisul ( ris.getUpdateCount() );
      
      ris.close();
      dbase.close();
      }
    catch ( Exception exc)
      {
      stat.log.exceptionShow("AHHHHH", exc);
      }
    }
  }
  
  /**
   * THis shows the result of an update
   */
  private void showUpdateRisul ( int risulVal )
    {
    executeCounter++;
    println("Risul ["+executeCounter+"]="+risulVal);
    }


/** 
 * This is used to get the current line on the input pane
 */
private final class QueryKeyAdapter extends KeyAdapter
  {
  @Override
  public void keyPressed(KeyEvent e)
    {
    if( e.getKeyCode() !=  KeyEvent.VK_ENTER ) 
      return;
      
    String selectedText=queryIn.getSelectedText();
    
    if ( selectedText == null || selectedText.length() < 1 )
      return;
    
    e.consume(); // I do not want it to be processed normally.

    executeSql ( selectedText );

/*
    try
      {
      int lineNum = queryIn.getLineOfOffset(queryIn.getCaretPosition());
      int begin = queryIn.getLineStartOffset(lineNum);
      int end = queryIn.getLineEndOffset(lineNum);

      String textLine = queryIn.getText(begin, end-begin);
      executeSql ( textLine );
      }
    catch (Exception ble)
      {
      System.err.println("BadLocationException"+ble.getMessage());
      }
*/
    }
  }

/**
 * Display a dialog box where you can write the name of a script and if you say OK you execute it.
 */
private final class ExecuteDialog implements ActionListener
  {
  private JButton     okButton;
  private JDialog     thisDialog;
  private JTextField  scriptNameField;

  public ExecuteDialog(  )
    {
    JPanel aPanel = new JPanel();

    aPanel.add(new JLabel("Nome Script"));
    scriptNameField=new JTextField(20);
    aPanel.add(scriptNameField);
    
    JPanel buttonPanel = new JPanel();
    okButton = new JButton("OK");
    okButton.addActionListener(this);
    buttonPanel.add(okButton);

    JPanel mainPanel = new JPanel(new BorderLayout());
    mainPanel.add(aPanel,BorderLayout.CENTER);
    mainPanel.add(buttonPanel,BorderLayout.SOUTH);

    // This does not NEED to be a modal dialog, we may want to look at the results...
    thisDialog = new JDialog(stat.console.getJFrame(),"Esegui SQL Script");
    thisDialog.setContentPane(mainPanel);
    thisDialog.pack();
    thisDialog.setLocationRelativeTo(null);
    thisDialog.setVisible(true);
    }

  @Override
  public void actionPerformed(ActionEvent event)
    {
    thisDialog.dispose();
    
    String scriptName = scriptNameField.getText();
    println("ExecuteDialog scriptName="+scriptName);
    runScript(scriptName);
    }
    
  private void runScript ( String scriptName )
    {
    try
      {
      FileReader scriptFile = new FileReader(scriptName);
      BufferedReader scriptReader = new BufferedReader(scriptFile);
      String line;
      
      while ( (line = scriptReader.readLine()) != null )
        executeSql ( line );
      
      scriptFile.close();
      }
    catch ( Exception exc )
      {
      stat.console.showMessageDialog("runScript() exception="+exc);
      }
    }
  }


private final class ButtonClickHander implements ActionListener
  {
  @Override
  public void actionPerformed(ActionEvent event)
    {
    Object source = event.getSource();

    if ( source == tableButton ) 
      Aeutils.newThreadStart(new ShowDbaseTables() , "show tables");
    else if ( source == executeButton ) 
      new ExecuteDialog();
    }
  }

  /**
   * You MUST run this on a NON GUI thread
   */
  public int copyTable ( AeDbase fromDb, String fromQuery, AeDbase toDb, String toTableName ) throws SQLException
    {
    if ( SwingUtilities.isEventDispatchThread() )
      {
      println("copyTableRunnableFun: must NOT be run on Swing thread");
      return 0;
      }
    
    if ( fromDb == null )
      {
      println("copyTableRunnableFun: fromDb==null");
      return 0;
      }
    
    if ( toDb == null )
      {
      println("copyTableRunnableFun: toDb==null");
      return 0;
      }
    
    BrsCopy storSrv = new BrsCopy(stat, toDb, toTableName);

    Dprepared statement=fromDb.getPreparedStatement(fromQuery);
    Brs aRs = statement.executeQuery();
    int retcode = storSrv.copy(aRs, this); 
    aRs.close();
    
    return retcode;
    }
  
  public String help()
    {
    String msg = "todb=stat.newMainDbase();\n"+
       "sql.disabeReferentialIntegrity(todb);\n"+
       "fromdb=stat.newExtDbase();\n"+
       "sql.copyTable (fromdb,\"select * from ana_tbl where ana_id <> 1\",todb,\"ana_tbl\");";
    
    return msg;
    }

  public void saveWindowPosition(WinposStore w)
    {
    workFrame.saveWindowPosition(w);
    }
    
  }

