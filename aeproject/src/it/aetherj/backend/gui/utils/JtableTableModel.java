/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.utils;

import javax.swing.table.*;

/**
 * This is needed since both rtiem table model and Db table model express the same concept in different ways
 * Other classes do not care the details but need a uniform interface
 */
public interface JtableTableModel extends TableModel
  {

  public Object getValueAt(int rowIndex, String col_name );
  
  public int getColumnIndex_key ( String colkey );
  
  public void showColumn ( String colkey, TableColumnModel columnModel, boolean show );
  
  public void syncTableColumnModelIdentifiers (TableColumnModel columnModel);
  
  }
