/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.utils;

import java.awt.*;
import java.awt.event.*;
import java.net.URI;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.boot.ComponentProvider;
import it.aetherj.gwtwapp.server.servlets.PageHelpServlet;


/**
 * I could use javahelp 2.0 but that does not have a handy support for printing, meaning that you cannot just print the help..
 * This is not good, we want a single help, not many.. Damiano
 */
public final class HelpProvider implements ComponentProvider,ActionListener
  {
  private static final String classname="HelpProvider.";

  public static final String aetherj_man_key = "aetherj_man_pdf";
  
  private final Stat stat;
  private final String help_key;            // the service we are willing to lookup
  private final String help_bookmark;       // the bookmark where to jump
  
  private final JPanel workPanel;
  private final JButton helpButton;
  
  public HelpProvider(Stat stat, String help_key )
    {
    this(stat,help_key,null);
    }
  
  public HelpProvider(Stat stat, String help_key, String help_bookmark)
    {
    this.stat = stat;
    this.help_key = help_key == null ? "" : help_key;
    this.help_bookmark = help_bookmark == null ? "" : help_bookmark;
    
    helpButton = stat.imageProvider.newButtonTidy("help-22x22.png", this);
    
    workPanel = newWorkPanel();
    }

  private JPanel newWorkPanel()
    { 
    helpButton.setToolTipText(help_bookmark);
    
    JPanel risul = new JPanel(new FlowLayout(FlowLayout.LEFT,2,2));
    
    risul.add(helpButton);
    
    return risul;
    }

  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }

  public void actionPerformed(ActionEvent e)
    {
    if ( ! Desktop.isDesktopSupported() )
      {
      stat.println(classname+"actionPerformed: ERROR: NO DesktopSupport ! ");
      return;
      }
      
    Desktop desktop = Desktop.getDesktop();
    if ( ! desktop.isSupported(Desktop.Action.BROWSE))
      {
      stat.println(classname+"actionPerformed: ERROR: Desktop.Action.BROWSE NOT supported ! ");
      return;
      }
    
    try
      {
      StringBuilder builder = new StringBuilder(200);
			
			builder.append(stat.webServer.getWebServerAddress());
			builder.append(PageHelpServlet.web_xml_url_pattern);
			
      if ( help_key.length() > 0 ) builder.append("?pagehelp_key="+help_key);
      if ( help_bookmark.length() > 0 ) builder.append("#"+help_bookmark);
      
      URI uri = new URI(builder.toString());
      
      stat.println("open "+uri);
      
      desktop.browse(uri);
      }  
    catch ( Exception exc )
      {
      stat.println(classname+".actionPerformed",exc);
      }
    }
  }
