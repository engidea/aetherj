package it.aetherj.backend.gui.utils;

import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class JtableDateRenderer extends DefaultTableCellRenderer 
  {
  private static final long serialVersionUID = 1L;

  public static final String format_hhmmss = "hh:mm:ss";
  
  private final SimpleDateFormat f;

  public JtableDateRenderer ( String format )
    {
    f = new SimpleDateFormat(format);
    }
  
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) 
    {
    if( value instanceof Date) 
      value = f.format(value);
    else 
      value = "";
    
    return super.getTableCellRendererComponent(table, value, isSelected,hasFocus, row, column);
    }
  }
  
