/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.utils;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.WinposStore;

/**
 * It is useful to keep the status of the file chooser
 * Most notably it remembers previous selected directory
 */
public final class GuiFileChooser
  {
  private static final String WINPOS_key = "GuiFileChooser";
  
  private final Stat stat;
  
  private JFileChooser fileChooser;
  private File currentDirectory;
  
  /**
   * Constructor.
   * @param stat
   */
  public GuiFileChooser(Stat stat) 
    {
    this.stat = stat;
    
    new Thread(new FileChoserCreator()).start();
    }

  public void saveWindowPosition(WinposStore wpstore)
    {
    if ( fileChooser == null ) return;
    
    File selected = fileChooser.getSelectedFile();
    if ( selected == null ) return;
    
    File seldir = selected.getParentFile();
    if ( seldir == null ) return;
    
    wpstore.putFile(WINPOS_key, seldir);
    }
  
  private synchronized void newFileChoser()
    {
    fileChooser = new JFileChooser();
//    fileChooser.addActionListener(new ApproveListener());
    fileChooser.setMultiSelectionEnabled(true);    
    
    File basedir = stat.winposStore.getFile(WINPOS_key, stat.boot.getProgramDir());

    if ( basedir != null )
      fileChooser.setCurrentDirectory(basedir);
    }

  public synchronized final File showOpenDirectoryDialog(Component parent)
    {
    File risul = null;
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    fileChooser.setAcceptAllFileFilterUsed(false);
    
    if ( fileChooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION)
      risul = fileChooser.getSelectedFile();
    
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fileChooser.setAcceptAllFileFilterUsed(true);

    return risul;
    }

  public synchronized final int showOpenDialog(Component parent)
    {
    fileChooser.setFileHidingEnabled(false);
    
    return fileChooser.showOpenDialog(parent);
    }

  /**
   * Show an open dialog with included a specific file filter
   * @param parent
   * @param fileFilter not null
   * @return
   */
  public synchronized final File showOpenDialog(Component parent, FileFilter fileFilter)
    {
    if ( fileFilter == null )
      throw new IllegalArgumentException("Need to pass a FileFilter");
    
    File risul = null;
    
    fileChooser.addChoosableFileFilter(fileFilter);
    fileChooser.setFileFilter(fileFilter);
    
    if ( fileChooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION)
      risul = fileChooser.getSelectedFile();
    
    fileChooser.removeChoosableFileFilter(fileFilter);
    
    return risul;
    }
  
  public synchronized final int showSaveDialog(Component parent)
    {
    return fileChooser.showSaveDialog(parent);
    }

  public synchronized File getSelectedFile ()
    {
    return fileChooser.getSelectedFile();
    }
    
  public synchronized File [] getSelectedFiles()
    {
    return fileChooser.getSelectedFiles();
    }

private final class FileChoserCreator implements Runnable 
  {
  @Override
  public void run()
    {
    newFileChoser();
    }
  }

  /**
   * It should be possible to be notified when the user says OK and store the dir.
private final class ApproveListener implements ActionListener
  {
  @Override
  public void actionPerformed(ActionEvent event)
    {
    currentDirectory = fileChooser.getCurrentDirectory();
    fileChooser.setCurrentDirectory(currentDirectory);
    System.out.println ("NEW CURDIR="+currentDirectory);
    }
 
  }
   */
  
  

  }
