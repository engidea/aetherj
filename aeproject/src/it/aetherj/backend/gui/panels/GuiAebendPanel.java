/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.panels;

import java.awt.*;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.WinposStore;
import it.aetherj.backend.gui.utils.*;
import it.aetherj.boot.ComponentProvider;

public final class GuiAebendPanel implements ComponentProviderSaveWinpos
  {
  private static final String classname="GuiAebendPanel.";
  
  private final Stat stat;

  private final JPanel workPanel;

  // from now on there are the panels that will be used by the user

	private JPanel           innerPanel;
  private JComponent       curCenterPanel; 
  private JSplitPaneBetter innerSplit;  

  private final GuiAebendStatus statusPanel;

   
    
  public GuiAebendPanel(Stat stat)
    {
    this.stat = stat;
		
    statusPanel     = new GuiAebendStatus(stat,this);

    // create work panel now 
    workPanel = newWorkPanel();
    }

  private JPanel newWorkPanel ()
    {
    JPanel risul = new JPanel(new BorderLayout());

    innerPanel = new JPanel(new BorderLayout());

    risul.add(statusPanel.getComponentToDisplay(),BorderLayout.NORTH);

    innerSplit = new JSplitPaneBetter(JSplitPane.HORIZONTAL_SPLIT, new JLabel("Menu"),innerPanel );
    
    Rectangle rect = stat.winposStore.getWindowBounds(classname+"innerSplit");

    innerSplit.setDividerLocation(rect.x);

    // split panel goes at the center, so I can resize the menu
    risul.add(innerSplit,BorderLayout.CENTER);

    return risul;
    }

  
  
  /**
   * Sets the content of the panel.
   * Give a JComponent and it will replace the current content.
   */
  public void setContent ( ComponentProvider aContent )
    {
    JComponent aComponent = aContent.getComponentToDisplay();
    
    if ( curCenterPanel != null ) innerPanel.remove(curCenterPanel);
    innerPanel.add(aComponent,BorderLayout.CENTER);
    curCenterPanel = aComponent;
    innerPanel.revalidate();
    innerPanel.repaint();
    }


  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }


  @Override
  public void saveWindowPosition(WinposStore wpstore)
    { 
		Rectangle rect = new Rectangle();

		// I now save the divider location		
		rect.setLocation(innerSplit.getDividerLocation(), 0 );
		
		wpstore.putWindowBounds(classname+"innerSplit",rect);

    wpstore.save();
    }
  

  } // end main class


  
