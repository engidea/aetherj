/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.panels;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.gui.utils.SwingGridBagLayout;
import it.aetherj.boot.ComponentProvider;


public final class GuiAebendStatus implements ComponentProvider
  {
  private final StatusButtonListener buttonListener = new StatusButtonListener();

  private final Stat stat;
  private final JPanel workPanel;
  private final GuiAebendPanel guiPanel;

  private final JTextField imp_msg_field;
  private final JTextField transfer_progress_field;
  private final JTextField imp_msg_error_field;
  private final JTextField jrncMessageTime;
  private final JTextField imp_nome_field;

  private JButton refreshImpiantiButton,connectStatusButton,impEditButton;

  private Thread statusPollingThread;

  /**
   * Constructor
   * @param stat
   * @param guiPanel
   */
  public GuiAebendStatus(Stat stat, GuiAebendPanel guiPanel)
    {
    this.stat = stat;
    this.guiPanel = guiPanel;

    imp_nome_field=new JTextField();
    imp_msg_field=new JTextField();
    transfer_progress_field=new JTextField(8);
    imp_msg_error_field=new JTextField();
    imp_msg_error_field.setForeground(Color.red);
    jrncMessageTime=new JTextField(8);
    
    workPanel = newStatusPanel();
    }

  /**
   * An impianto is connected only when the value is true, a null value (maniang 
   * that we are dialing is a NOT connected status.
   * @return
   */
  public boolean isImpiantoConnected ()
    {
    return true;
    }

  /**
   * A resource wants to know if this "connection" can read
   * deprecated to be replaced
   * @param resourceReadLevel
   * @return
   */
  public boolean canReadResource ( int resourceReadLevel )
    {
    return true;
    }
  
  /**
   * deprecated to be replaced
   * @param resourceWriteLevel the level that the resource requires to be writable.
   * @return true if I can write this resource against this impianto.
   */
  public final boolean canWriteResource ( int resourceWriteLevel )
    {
    return true;
    }

  private JPanel newStatusPanel ()
    {
    refreshImpiantiButton = stat.imageProvider.newRefreshButton(buttonListener);
    
    connectStatusButton=stat.imageProvider.newOfflineButton(buttonListener);
    connectStatusButton.setBorder(new EmptyBorder(4,6,4,6));
    connectStatusButton.setEnabled(false);

    impEditButton = stat.imageProvider.newEditSmallButton(buttonListener);
    
    JLabel messageLabel = new JLabel("Message:");
    messageLabel.setForeground(Color.blue);

// viene bruttino con sfondo grigio...
//    jrncMessage.setEditable(false);
//    jrncMessageTime.setEditable(false);

    jrncMessageTime.setForeground(Color.red);
    
    // TIme to put all components into a panel.
    SwingGridBagLayout gbr = new SwingGridBagLayout();
    gbr.gbc.fill=GridBagConstraints.HORIZONTAL;
    
    gbr.addComponentWestIncx(refreshImpiantiButton);
    gbr.addComponentWestIncx(connectStatusButton);
    
    gbr.addComponentWestIncx(new JLabel("Nome"));

    gbr.gbc.weightx=0.3;
    gbr.addComponentWestIncx(imp_nome_field);

    gbr.gbc.weightx=0;
    gbr.addComponentWestIncx(impEditButton);    
    gbr.addComponentWestIncx(messageLabel);
    
    gbr.gbc.weightx=0.2;
    gbr.addComponentWestIncx(imp_msg_field);

    gbr.gbc.weightx=0;
    gbr.addComponentWestIncx(transfer_progress_field);

    gbr.gbc.weightx=0.2;
    gbr.addComponentWestIncx(imp_msg_error_field);

    gbr.gbc.weightx=0;
    gbr.addComponentWestIncx(jrncMessageTime);
    
    return gbr.getJPanel();    
    }

    
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }


  /**
   * Connection to impianto means posting a request into the dbase
   * It will be duty of jrncServer to actually try to connect.
   */
  private void handleConnectStatusPress()
    {

    }


  private void clearStatusGui ()
    {
    imp_nome_field.setText("Not selected");
    imp_msg_field.setText(null);
		transfer_progress_field.setText(null);
    imp_msg_error_field.setText(null);
    jrncMessageTime.setText("--");

    connectStatusButton.setEnabled(false);
    }
  
  
  
  
  private void updateStatusGui (AeDbase adb)
    {

    imp_msg_field.setText("uno");
		transfer_progress_field.setText("uno");
    imp_msg_error_field.setText("uno");
    jrncMessageTime.setText("uno");
    }
  
  private void impEditButtonClick ()
    {
    }
  
 /* 
private class JrncStatusPolling implements Runnable
  {
  public void run()
    {
    AeDbase mydb = stat.newWorkDbase();
    
    // when the thread starts then it enables the button
    connectStatusButton.setEnabled(true);
    
    while ( ! stat.waitHaveShutdownReq_s(1) )
      {
      updateStatusGui(mydb);
      
      }
      
    mydb.close();

    // we are shutting down you have to report this
//    clearStatusVars();
    
    // when the thread STOPS no imp_id or shutdown then clear gui/button disable
    clearStatusGui();

    // by doing this I allow for restart if the thread dies in strange way
    statusPollingThread = null;
    }
  }
*/

private class StatusButtonListener implements ActionListener
  {
  public void actionPerformed(ActionEvent event )
    {
    Object source = event.getSource();
    
    if ( source == connectStatusButton ) handleConnectStatusPress();
    
    if ( source == impEditButton ) impEditButtonClick(); 
    }
  }
  
    
      
    


  
  }  // END main class
