/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend;

import java.io.File;
import java.util.Random;

import org.hsqldb.Server;

import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.SchemaManager;
import it.aetherj.backend.dbase.labels.LabelFactory;
import it.aetherj.backend.dbtbls.AedbFactory;
import it.aetherj.backend.gui.*;
import it.aetherj.backend.gui.config.*;
import it.aetherj.backend.gui.images.ImageProvider;
import it.aetherj.backend.gui.panels.*;
import it.aetherj.backend.gui.utils.*;
import it.aetherj.backend.gui.webuser.WuserManager;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.McwSSLFactory;
import it.aetherj.backend.server.*;
import it.aetherj.backend.utils.MimNonceMap;
import it.aetherj.boot.*;
import it.aetherj.boot.stampa.StampaEngine;
import it.aetherj.shared.*;

public final class Stat implements PrintlnProvider
  {
  private static final String classname="Stat.";
   
  public final Boot boot; 
  public final Log log; 

  public final ApplicationVersion appVersion; 
  public final File         datastoreDir;   // store all data and config here

  public Aedbg              dbg;
  public LabelFactory       labelFactory;  
  public WinposStore        winposStore; 
  public Aeutils            utils;
  public GuiFileChooser     fileChooser;
  public ConfigProperties   config;
  public ImportConfigGui    importConfig;
  public Server             dbaseServer;
  public AeDbase            dbase;           // the first instance of dbase
  public DbProperty         dbProperty;      // Use to get/set properties in dbase
  public AetherParams       aeParams;    // This needs a dbase and dbProperty, initialize at server start
  public AetherParamsGui    aeParamsGui; // after params since it needs them
  public AedbFactory        aedbFactory;     // Tables to set/get Aether info are here
  public MimNonceMap        nonceMap;
  
  public ImageProvider        imageProvider;
  public StampaEngine         stampaEngine;  
  public ConsoleGui           console; 
  public ConsoleMenubar       consoleMenubar;
  public GuiLogin             guiLogin;
  public BshFrame             beanshell;
  public SqlqueryFrame        sqlquery;
  public McwSSLFactory        mimSSLFactory;
  public SchemaManager        dbSchemaManager;
  public McwRunnerTableModel  mcwRunningModel;
  public McwAddressTableModel mcwAddressModel;
  public McwConsole           mcwConsole;   // must be before the AebendConsole
  public AebendConsole        aebendConsole; 

  public GuiAebendPanel       backendPanel;
  public WuserManager   wuserManager;
  
  public WebServer            webServer;
	public RuntimeInfoFrame     infoFrame;
  
  private final int exitCode;
	
  public Stat ( Boot boot )
    {
    this.boot = boot;
    this.log = boot.getLog();
    this.appVersion = new ApplicationVersion();
    this.exitCode = makeExitCode();
    this.datastoreDir = new File(boot.getProgramDir(),Const.datasoreDirName);
     
    // tell Log how to exit application properly
    log.setExitCode(exitCode);
    }
 
  /**
   * Returns the classloader that has been used to load this class.
   * It is just to make it clear that I want the classloader for the system.
   * @return
   */
  public final ClassLoader getClassLoader ()
    {
    return getClass().getClassLoader();      
    }

  /**
   * This is handled as private since I may wish to do some processing
   * to decide when to shutdown.
   */
  private boolean shutdownRequested;
  
  public synchronized void setShutdown ( boolean shutdownRequested )
    {
    this.shutdownRequested = shutdownRequested;
    
    notifyAll();
    }
  
  public boolean haveShutdownReq ()
    {
    return shutdownRequested;
    }
  
  public boolean waitHaveShutdownReq_s ( int wait_s )
    {
    return waitHaveShutdownReq(wait_s*1000);
    }
  
  /**
   * Wait for a shutdown with timeout, if you use this one on loops you will be notified when shutdown is requested.
   * NOTE that all processes will be waken up, even the ones that close the dbase.
   * @param wait_ms
   * @return
   */
  public synchronized boolean waitHaveShutdownReq ( int wait_ms )
    {
    if ( shutdownRequested )
      return true;
    
    try
      {
      wait(wait_ms);
      }
    catch ( Exception exc )
      {
      println(classname+"waitOnShutdownReq",exc);
      }
    
    return shutdownRequested;
    }

  /**
   * Small utility that makes a new exit code.
   */
  private int makeExitCode ()
    {
    try
      {
      Random random = new Random(System.currentTimeMillis());
      return random.nextInt();
      }
    catch ( Exception exc )
      {
      // It may fail for some weird reason
      System.err.println(classname+"makeExitCode() exception="+exc);
      return 1299;
      }
    }

  /**
   * Eventually I may not to share this I may make all this handling outside Stat
   * @return
   */
  public int getExitCode ()
    {
    return exitCode;
    }
  
  /**
   * If there is a security manager in place this is the proper way to system exit
   */
  public void systemExit ()
    {
    System.exit(exitCode);
    }
  
  /**
   * Create a new AeDbase that is NOT connected
   * This will not fail, the following connect may fail
   * @return
   */
  public AeDbase newDbase()
    {
    return new AeDbase(this);
    }
  
  /**
   * Every single thread MUST have its own dbase.
   * Dbase access is NOT multi thread safe at all and you WILL get all weird stuff if you try.
   * Once a thread dies it MUST close its dbase.
   * @return a non null but possibly closed JrncDbase
   */
  public AeDbase newWorkDbase()
    {
    AeDbase newDbase = newDbase();
    
    if ( ! dbase.isConnected() ) 
      {
      // if main dbase did not connect then there is no connection available !
      println(classname+"newWorkDbase: no database connection available");
      // in any case I return a valid Dbase, it is possible for client to enquire if it is connected or not.
      return newDbase;
      }
      
    if  ( newDbase.connect(config.mainDbaseProperty) != null ) return newDbase;

    // dbase connection failed, let me PRINT error message
    // do NOT do GUI interaction here, you may stop the system !!!
    // it will bomb quite soon but maybe not... difficult to know...
    println(classname+"newWorkDbase: Dbase connect failed: see log for details");

    return newDbase;
    }

  public void listThreads ()
    {
    int count = Thread.activeCount();
    Thread []threads = new Thread[count];
    Thread.enumerate(threads);
    
    log.userPrintln("Threads");
    for ( int index=0; index<threads.length; index++ )
      {
      Thread thread = threads[index];
      println("  "+thread.getName()+" deamon="+thread.isDaemon());
      }
    }

  public String toString ()
    {
    StringBuilder risul = new StringBuilder(500);

    risul.append("available commands\n");
    risul.append("newWorkDbase()     Create a new DB connection\n");
    risul.append("listThreads()      list active threads\n");
    
    return risul.toString();
    }

  @Override
  public void println(String message)
    {
    log.println(message);
    }

  @Override
  public void println(String message, Throwable exception)
    {
    log.println(message, exception);
    }
  }
