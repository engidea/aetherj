package it.aetherj.backend.mimclient;

import java.sql.SQLException;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbtbls.AedbCache;
import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * Simply to wrap in a single space the method to generate cache pages
 */
public class McwCacheAddresses
  {
  private static final String classname = "McwCacheAddresses";

  private final Stat stat;
  private final AeDbase dbase;
  private final AedbCache aedbCache;
  private final PrintlnProvider logger;
  private final MimEntity entity = MimEntityList.aeaddres;
  
  public McwCacheAddresses(Stat stat, AeDbase dbase, PrintlnProvider logger)
    {
    this.stat = stat;
    this.dbase=dbase;
    this.logger=logger;
    
    if ( logger == null )
      throw new IllegalArgumentException("logger is NULL");
    
    aedbCache = stat.aedbFactory.aedbCache;
    }
  
  
  public void refreshAddressCache (Iterator<McwAddress>iter) throws Exception
    {
    // clear up previous cache, if any
    aedbCache.dbaseDelete(dbase, entity);
    
    // now I have to split this into chunks of 100 addresses
    // A sub method will generate a cache entry and add one line to the cache index...
    // until there are no more addresses
    
    MimItemsArray<MimResultsCache> index = new MimItemsArray<MimResultsCache>(new MimResultsCache [0]);
    
    int insCounter=0;
    
    while ( iter.hasNext() )
      insCounter += genCacheRow(iter, index); 
    
    // here I should save the index, the global index of caches
    aedbCache.dbaseInsertIndex(dbase, entity, index);
    
    println(classname+".refreshAddressCache: insCounter="+insCounter);
    }
     
  private void println(String message)
    {
    logger.println(message);
    }
  
  /**
   * NOTE that addresses do NOT have index or manifest nor they have signatures
   * This is why they have "special treatment"
   * @throws CloneNotSupportedException 
   */
  private int genCacheRow (Iterator<McwAddress>source, MimItemsArray<MimResultsCache> main_index ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    int counter=0;

    MimItemsArray<MimAddress> addresses = new MimItemsArray<MimAddress>(new MimAddress [0]);
    
    McwAddress first_item=null;
    McwAddress last_item=null;
    
    while ( source.hasNext() && counter++ < 200 )
      {
      McwAddress row = source.next();
      
      if ( first_item==null)
        first_item=row;
      
      last_item=row;
      
      addresses.add(row.getMimAddress());
      }
    
    MimPayload pay_rows = stat.aeParams.newMimPayload(entity);
    
    pay_rows.caching.pregenerated=true;
    pay_rows.response.addresses = addresses.toArray();
    
    CryptoEddsa25519 crypto = new CryptoEddsa25519();
    pay_rows.signPayload(crypto, stat.aeParams.getPrivateKey());
    
    // now, I can ask to save to the cache for this bunch of addresses
    Integer row_id = aedbCache.dbaseInsert(dbase, pay_rows, null, null );
    
    MimTimestamp starts_from = new MimTimestamp(first_item.getUpdateTimestamp());
    MimTimestamp ends_at = new MimTimestamp(last_item.getUpdateTimestamp());
    
    // now I can add to the main index this entry
    main_index.add(new MimResultsCache(row_id,starts_from,ends_at));
    
    return counter;
    }
  
  }