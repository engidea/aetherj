package it.aetherj.backend.mimclient;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbtbls.*;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.MimFingerprint;
import it.aetherj.shared.v0.MimVote;

/**
 * This does garbage collect, then recalculate the cached values (since they may change)
 * You can set how many days to consider content valid in config
 */
public class McwGcollect
  {
  private static final String classname="McwGcollect";
  
  private final Stat stat;
  private final McwAddressTableModel parent;

  private AeDbase dbase;                   // Dbase will be given on recalc run
  
  public McwGcollect(Stat stat, McwAddressTableModel parent)
    {
    this.stat = stat;
    this.parent=parent;
    
    dbase = stat.newDbase();
    }
  
  public void garbageCollectRun(AeDbase dbase)
    {
    this.dbase=dbase;

    if ( dbase.isClosed() )
      {
      //  remember to close dbase later...
      println(classname+".garbageCollectRun() ERROR dbase is closed -> END");
      return;
      }
    

    try
      { 
      gcPosts();  
      gcThreads();
      recalcPostCount ();
      recalcThreadsCount ();
      recalcVotesRef();
      recalcThreadVotes();
      recalcPostVotes();
      }
    catch ( Exception exc )
      {
      parent.println(classname+".recalcRun()", exc);
      }
    }
  
  private void println(String message)
    {
    parent.println(message);
    }

  // ====================================================================================================
  
  private void gcThreads () throws SQLException
    {
    MimGcStats gcStats = new MimGcStats();

    // I want all posts that have a last update before the end of lifetime
    String query = "SELECT * FROM "+AedbThread.cn_view+" WHERE "+AedbMimOperations.cn_lastu+" < ? ";
    
    MimDprepared prepared = dbase.getPreparedStatement(query);
    prepared.setValue(1, stat.aeParams.getMimobjExpireTime());
    Brs ars = prepared.executeQuery();
    
    while ( ars.next() )
      gcThread(gcStats, ars);

    ars.close();
    
    println(classname+".gcThreads: "+gcStats);
    }

  /**
   * This is a post old enough to be deleted, but can I actually delete it ?
   * It must not have any posts bound to it
   * TODO add a filter into database insert !
   * @param ars
   * @throws SQLException 
   */
  private void gcThread ( MimGcStats gcStats, Brs ars ) throws SQLException
    {
    Integer row_id = ars.getInteger(AedbThread.cn_id);
    
    String query = "SELECT COUNT(*) FROM "+AedbPost.cn_tbl+" WHERE "+AedbPost.cn_thread_id+"=?";
    
    MimDprepared prepared = dbase.getPreparedStatement(query);
    prepared.setInt(1, row_id);
    Brs count_rs = prepared.executeQuery();
    
    int children_count=0;
    
    if ( count_rs.next() )
      children_count=ars.getInteger(1,0);
     
    count_rs.close();
    
    if ( children_count > 0 )
      gcStats.incCannotDelete();
    else
      gcStats.deleted_count += stat.aedbFactory.aedbThread.deleteDbaseRow(dbase,row_id);
    }
  
  // ====================================================================================================
  
  /**
   * One of the issues is if a post has subposts... what to do ?
   * I cannot break up the relationships..., so the pruning has to be from the leaves, 
   * This means I can prune only posts that have no children....
   * So, I need to have referential integrity on posts, it should work since the parent is inserted, eventually
   * Since I am looking for leaves... a leaf is a post that has no children, no other post point to it
   * @throws SQLException 
   */
  private void gcPosts () throws SQLException
    {
    MimGcStats gcStats = new MimGcStats();

    // I want all posts that have a last update before the end of lifetime
    String query = "SELECT * FROM "+AedbPost.cn_view+" WHERE "+AedbMimOperations.cn_lastu+" < ? ";
    
    MimDprepared prepared = dbase.getPreparedStatement(query);
    prepared.setValue(1, stat.aeParams.getMimobjExpireTime());
    Brs ars = prepared.executeQuery();
    
    while ( ars.next() )
      gcPost(gcStats, ars);

    ars.close();
    
    println(classname+".gcPosts: "+gcStats);
    }

  /**
   * This is a post old enough to be deleted, but can I actually delete it ?
   * Wait, I also must NOT insert it if it comes over from the net !
   * TODO add a filter into dbase insert !
   * @param ars
   * @throws SQLException 
   */
  private void gcPost ( MimGcStats gcStats, Brs ars ) throws SQLException
    {
    Integer row_id = ars.getInteger(AedbPost.cn_id);
    
    String query = "SELECT COUNT(*) FROM "+AedbPost.cn_tbl+" WHERE "+AedbPost.cn_parent_id+"=?";
    
    MimDprepared prepared = dbase.getPreparedStatement(query);
    prepared.setInt(1, row_id);
    Brs count_rs = prepared.executeQuery();
    
    int children_count=0;
    
    if ( count_rs.next() )
      children_count=count_rs.getInteger(1,0); 
     
    count_rs.close();
    
    if ( children_count > 0 )
      gcStats.incCannotDelete();
    else
      gcStats.deleted_count += stat.aedbFactory.aedbPost.deleteDbaseRow(dbase,row_id);
    }
  
  
  /**
   * How many threads does a board have ?
   * @throws SQLException
   */
  private void recalcPostVotes () throws SQLException
    {
    Brs ars = selectAll("SELECT * FROM "+AedbPost.cn_tbl);
    
    while ( ars.next() )
      recalcPostsVotes(ars);

    ars.close();
    } 

  public void recalcPostVotes( AeDbase db, Integer thread_id ) throws SQLException
    {
    int upvotes = getPostVotes(db, thread_id, MimVote.type_upvote); 
    int downvotes = getPostVotes(db, thread_id, MimVote.type_downvote);
    postUpdateVotes(db, thread_id, upvotes-downvotes);
    }

  private void recalcPostsVotes ( Brs ars ) throws SQLException
    {
    Integer post_id = ars.getInteger(AedbPost.cn_id);
    int upvotes = getPostVotes(dbase,post_id, MimVote.type_upvote);
    int downvotes = getPostVotes(dbase,post_id, MimVote.type_downvote);
    postUpdateVotes(dbase, post_id, upvotes-downvotes);
    }
  
  private int postUpdateVotes (AeDbase db, Integer row_id, int votes ) throws SQLException
    {
    String query = "UPDATE "+AedbPost.cn_tbl+
        " SET "+AedbPost.cn_votes_cnt+"=? "+
        " WHERE "+AedbPost.cn_id+"=?";
    
    Dprepared prepared = db.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, votes);
    prepared.setInt(insIndex++, row_id);
    int risul = prepared.executeUpdate();
    prepared.close();
    return risul;
    }

  
  /**
   * NOTE: This assumes that the cn_post_id is correct, to be so, you have to have the recalc of this done 
   * @see recalcVotesRef
   */
  private int getPostVotes (AeDbase db, Integer post_id, int vote_type ) throws SQLException
    {
    String query="SELECT COUNT(*) as upvotes FROM "+AedbVote.cn_tbl+" WHERE "+
    AedbVote.cn_post_id+"=?"+
    " AND "+AedbVote.cn_type+"=? "+
    " AND "+AedbVote.cn_type_class+"="+MimVote.typclass_discussion;
    
    Dprepared prepared = db.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, post_id);
    prepared.setInt(insIndex++, vote_type);
    
    Brs ars = prepared.executeQuery();

    int votes=0;

    if ( ars.next() )
      votes=ars.getInteger("upvotes", 0);
    
    prepared.close();
    
    return votes;
    }

  // ============================================================================================

  
  /**
   * How many threads does a board have ?
   * @throws SQLException
   */
  private void recalcThreadVotes () throws SQLException
    {
    Brs ars = selectAll("SELECT * FROM "+AedbThread.cn_tbl);
    
    while ( ars.next() )
      recalcThreadVotes(ars);

    ars.close();
    } 

  public void recalcThreadVotes( AeDbase db, Integer thread_id ) throws SQLException
    {
    int upvotes = getThreadVotes(db, thread_id, MimVote.type_upvote); 
    int downvotes = getThreadVotes(db, thread_id, MimVote.type_downvote);
    threadUpdateVotes(db, thread_id, upvotes-downvotes);
    }
  
  private void recalcThreadVotes ( Brs ars ) throws SQLException
    {
    Integer thread_id = ars.getInteger(AedbThread.cn_id);
    
    int upvotes = getThreadVotes(dbase, thread_id, MimVote.type_upvote);
    int downvotes = getThreadVotes(dbase, thread_id, MimVote.type_downvote);
    
    threadUpdateVotes(dbase, thread_id, upvotes-downvotes);
    }
  
  private int threadUpdateVotes (AeDbase db, Integer row_id, int votes ) throws SQLException
    {
    String query = "UPDATE "+AedbThread.cn_tbl+
        " SET "+AedbThread.cn_votes_cnt+"=? "+
        " WHERE "+AedbThread.cn_id+"=?";
    
    Dprepared prepared = db.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, votes);
    prepared.setInt(insIndex++, row_id);
    int risul = prepared.executeUpdate();
    prepared.close();
    return risul;
    }

  
  
  private int getThreadVotes ( AeDbase db, Integer thread_id, int vote_type ) throws SQLException
    {
    String query="SELECT COUNT(*) as upvotes FROM "+AedbVote.cn_view+" WHERE "+
    AedbVote.cn_thread_id+"=?"+
    " AND "+AedbVote.cn_post_id+" IS NULL "+
    " AND "+AedbVote.cn_type+"=? "+
    " AND "+AedbVote.cn_type_class+"="+MimVote.typclass_discussion;
    
    Dprepared prepared = db.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, thread_id);
    prepared.setInt(insIndex++, vote_type);
    
    Brs ars = prepared.executeQuery();

    int votes=0;

    if ( ars.next() )
      votes=ars.getInteger("upvotes", 0);
    
    prepared.close();
    
    return votes;
    }
    
  
  
  // ============================================================================================
  
  
  /**
   * This is needed to fix the Votes that have been added without a post present
   * It finds out what are the votes that need to have the post adjusted
   */
  private void recalcVotesRef () throws SQLException
    {
    String query="SELECT * FROM "+AedbVote.cn_view+" WHERE "+
    AedbVote.cn_post_id+" IS NULL "+
    " AND "+AedbVote.cn_target+" != "+AedbThread.cn_fingerprint;    
    
    Brs ars = selectAll(query);
    
    VoteStats voteStats = new VoteStats();
    
    while ( ars.next() )
      recalcVotesRef(ars, voteStats);

    ars.close();
    
    println(classname+".recalcVotesRef: "+voteStats);
    }
  
  /**
   * The Brs has all rows where there is no current post_id and the target is != than thread
   * @throws SQLException 
   */
  private void recalcVotesRef(Brs ars, VoteStats voteStats) throws SQLException
    {
    Integer vote_id = ars.getInteger(AedbVote.cn_id);
    
    MimFingerprint target = new MimFingerprint(ars.getBytes(AedbVote.cn_target));
    
    // here I have to look into posts to see if there is a post available...
    Brs prs = stat.aedbFactory.aedbPost.selectUsingFingerprint(dbase, target);
    
    if ( prs.next() )
      {
      Integer post_id = prs.getInteger(AedbPost.cn_id);
      voteUpdatePostId(vote_id, post_id);
      voteStats.board_found++;
      }
    else
      {
      voteStats.board_missed++;
      }
    }

  private int voteUpdatePostId ( Integer row_id, Integer post_id ) throws SQLException
    {
    String query = "UPDATE "+AedbVote.cn_tbl+
        " SET "+AedbVote.cn_post_id+"=? "+
        " WHERE "+AedbVote.cn_id+"=?";
    
    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, post_id);
    prepared.setInt(insIndex++, row_id);
    int risul = prepared.executeUpdate();
    prepared.close();
    return risul;
    }

  /**
   * How many threads does a board have ?
   * @throws SQLException
   */
  private void recalcThreadsCount () throws SQLException
    {
    Brs ars = selectAll("SELECT * FROM "+AedbBoard.cn_tbl);
    
    while ( ars.next() )
      recalcThreadsCount(ars);

    ars.close();
    } 

  private void recalcThreadsCount ( Brs ars ) throws SQLException
    {
    Integer row_id = ars.getInteger(AedbBoard.cn_id);
    
    int child_count = getThreadCount(row_id);

    // need to do a set post count even when zero, since it may have changed due to expire
    setThreadCount(row_id,child_count);
    
//    if ( stat.dbg.shouldPrint(Aedbg.M_dbase, Aedbg.L_debug))
//      println("thread_id="+thread_id+" post_count="+post_count);
    }

  private int getThreadCount ( Integer parent_id ) throws SQLException
    {
    if ( parent_id == null )
      return 0;
    
    String query = "SELECT COUNT(*) AS threads FROM "+AedbThread.cn_tbl+" WHERE "+AedbThread.cn_board_id+"=?";
    Dprepared prepared = dbase.getPreparedStatement(query);
    prepared.setInt(1, parent_id);
    
    Brs ars = prepared.executeQuery();

    int risul=0;

    if ( ars.next() )
      risul=ars.getInteger("threads", 0);
    
    prepared.close();
    
    return risul;
    }

  private int setThreadCount ( Integer row_id, int count ) throws SQLException
    {
    if ( row_id == null )
      return 0;
    
    String query = "UPDATE "+AedbBoard.cn_tbl+
        " SET "+AedbBoard.cn_thread_cnt+"=? "+
        " WHERE "+AedbBoard.cn_id+"=?";
    
    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, count);
    prepared.setInt(insIndex++, row_id);
    int risul = prepared.executeUpdate();
    prepared.close();
    
    return risul;
    }


  // =============================================================================
  
  private void recalcPostCount () throws SQLException
    {
    Brs ars = selectAll("SELECT * FROM "+AedbThread.cn_tbl);
    
    while ( ars.next() )
      recalcPostCountA(ars);

    ars.close();
    }

  private void recalcPostCountA ( Brs ars ) throws SQLException
    {
    Integer row_id = ars.getInteger(AedbThread.cn_id);
    
    int child_count = getPostCount(row_id);

    // need to do a set post count even when zero, since it may have changed due to expire
    setPostCount(row_id,child_count);
    
//    if ( stat.dbg.shouldPrint(Aedbg.M_dbase, Aedbg.L_debug))
//      println("thread_id="+thread_id+" post_count="+post_count);
    }

  private int setPostCount ( Integer thread_id, int count ) throws SQLException
    {
    if ( thread_id == null )
      return 0;
    
    String query = "UPDATE "+AedbThread.cn_tbl+
        " SET "+AedbThread.cn_post_cnt+"=? "+
        " WHERE "+AedbThread.cn_id+"=?";
    
    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, count);
    prepared.setInt(insIndex++, thread_id);
    int risul = prepared.executeUpdate();
    prepared.close();
    
    return risul;
    }

  

  private int getPostCount ( Integer thread_id ) throws SQLException
    {
    if ( thread_id == null )
      return 0;
    
    String query = "SELECT COUNT(*) AS posts FROM "+AedbPost.cn_tbl+" WHERE "+AedbPost.cn_thread_id+"=?";
    Dprepared prepared = dbase.getPreparedStatement(query);
    prepared.setInt(1, thread_id);
    
    Brs ars = prepared.executeQuery();

    int risul=0;

    if ( ars.next() )
      risul=ars.getInteger("posts", 0);
    
    prepared.close();
    
    return risul;
    }
  

  
  private Brs selectAll ( String query ) throws SQLException
    {
    Dprepared prepared = dbase.getPreparedStatement(query);
    return prepared.executeQuery();
    }

  

private static class VoteStats
  {
  public int board_found;
  public int board_missed;
  
  public String toString()
    {
    return "found="+board_found+" missed="+board_missed;
    }
  }

private static class MimGcStats
  {
  public int cannot_delete_count;
  public int deleted_count;
  
  public void incCannotDelete()
    {
    cannot_delete_count++;
    }
  
  public String toString()
    {
    return "deleted_count="+deleted_count+" cannot_delete_count="+cannot_delete_count;
    }
  
  }

  }
