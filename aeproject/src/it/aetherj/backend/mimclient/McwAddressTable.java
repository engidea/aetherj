/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient;

import java.awt.BorderLayout;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.TableColumn;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.utils.JtableDateRenderer;
import it.aetherj.backend.mimclient.jobs.*;
import it.aetherj.boot.ComponentProvider;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.MimAddress;

public class McwAddressTable implements ComponentProvider
  {
  private static final String classname="McwAddressTable";
  
  private final ButtonListener buttonListener = new ButtonListener();
  
  private final Stat stat;
  private final McwConsole parent;
  
  private final JComponent workPanel;
  private final McwAddressTableModel model;  

  private final JButton clearButton,deleteRow,addAddressButton;
  private final JTextField addAddressField;
  
  private JTable jtable;
  
  public McwAddressTable(Stat stat, McwConsole parent )
    {
    this.stat = stat;
    this.parent = parent;
    this.model = stat.mcwAddressModel;

    clearButton = stat.utils.newJButton("Clear Table", buttonListener);
    deleteRow = stat.utils.newJButton("Delete Row", buttonListener);
    addAddressButton = stat.utils.newJButton("Add", buttonListener);
    
    addAddressField = new JTextField(30);
    
    workPanel = newWorkPanel();
    }
    
  
  private JPanel newWorkPanel()
    {
    JPanel risul = new JPanel(new BorderLayout());
    
    risul.add(newNorthPanel(),BorderLayout.NORTH);
    risul.add(newCenterPanel(), BorderLayout.CENTER);
    
    return risul;
    }

  private JPanel newNorthPanel()
    {
    JPanel risul = new JPanel();
    
    risul.add(clearButton);
    risul.add(deleteRow);
    risul.add(addAddressField);
    risul.add(addAddressButton);
    
    return risul;
    }
  
  private JScrollPane newCenterPanel()
    {
    jtable = new JTable(model);
    
    setColWidth(jtable,McwAddressTableModel.col_id, 50, 100);
    setColWidth(jtable,McwAddressTableModel.col_start_time, 80, 160);
    setColWidth(jtable,McwAddressTableModel.col_res_code, 80, 160);
    setColWidth(jtable,McwAddressTableModel.col_srvtype, 40, 80);
  
    TableColumn column = jtable.getColumnModel().getColumn(McwAddressTableModel.col_start_time);
    column.setCellRenderer(new JtableDateRenderer(JtableDateRenderer.format_hhmmss));
    
    jtable.setAutoCreateRowSorter(true);
    
    jtable.addMouseListener(new McwMouseAdapter());
    
    JScrollPane risul = new JScrollPane(jtable);

    return risul;
    }
  
  private void setColWidth ( JTable jt, int col_index, int pref_w, int max_w )
    {
    TableColumn column = jt.getColumnModel().getColumn(col_index);
    column.setPreferredWidth(pref_w);
    column.setMaxWidth(max_w);
    }
    
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }

  public McwAddress getSelected ()
    {
    int row=jtable.getSelectedRow();
    
    if ( row < 1 )
      return null;
    
    row = jtable.convertRowIndexToModel(row);
    
    return model.getAddress(row);
    }
  
  /*
  private boolean setSelectedPrimaryId ( Integer w_id )
    {
    if ( w_id == null )
      return false;
    
    int model_row = model.getRowIndexForPrimaryId(w_id);
    if ( model_row < 0 )
      return false;
    
    int table_row = jtable.convertRowIndexToView(model_row);
    
    jtable.setRowSelectionInterval(table_row, table_row);
    return true;
    }
    */
  public String toString()
    {
    return model.toString();
    }

  private MimEntity getSelectedEntity ( )
    {
      switch ( parent.getRunEntity() )
      {
      case McwConsole.re_Status: return MimEntityList.aestatus;

      case McwConsole.re_Node: return MimEntityList.aenode;

      case McwConsole.re_PingNode: return MimEntityList.pingnode;

      case McwConsole.re_Address: return MimEntityList.aeaddres;
      
      case McwConsole.re_Aekeys: return MimEntityList.aekey;
      
      case McwConsole.re_Board: return MimEntityList.aeboard;
      
      case McwConsole.re_Thread: return MimEntityList.aethread;

      case McwConsole.re_Post: return MimEntityList.aepost;

      case McwConsole.re_Vote:   return MimEntityList.aevote;
        
      default: return null;
      }
    }
    
  private void runOnSelected (McwAddress address)
    {
    if ( address == null )
      return;
    
    McwParams params = new McwParams(getSelectedEntity());
    params.onExitWait_s=5;
    
    String w_run = parent.getRunState(); 

    McwRunner job=null;
    
    switch ( w_run )
      {
      case McwConsole.rs_GET: 
        job = new JobGetEntity(stat, address, params);
        break;

      case McwConsole.rs_POST: 
        job = new JobPostEntity(stat, address, params);
        break;
    
      case McwConsole.rs_RX_Cache: 
        job = new JobGetCache(stat, address, params);
        break;

      case McwConsole.rs_POST_Sync: 
        job = new JobPostSync(stat, address, params);
        break;

      case McwConsole.rs_RX_Wish: 
        job = new JobPostWishes(stat, address, params);
        break;

      default:
        parent.println("runOnSelected: unsupported RUN state="+w_run);
        break;
      }
    
    parent.queue(job);  
    }

  private void rowDeleteFun ()
    {
    int selection = jtable.getSelectedRow();
    
    if ( selection < 0 ) return;
    
    selection = jtable.convertRowIndexToModel(selection);

    stat.mcwAddressModel.remove(selection);
    }
  
  private void addAddressFun ()
    {
    String a_s = addAddressField.getText();
    
    if ( a_s == null || a_s.length() < 10 )
      {
      stat.console.showMessageDialog("address missing: something like https://10.0.0.180:38519");
      return;
      }
    
    try
      {
      MimAddress m_a = new MimAddress(a_s);
      McwAddress n_a = new McwAddress(m_a);
      stat.mcwAddressModel.addMcwAddress(n_a);
      }
    catch ( Exception exc )
      {
      stat.console.showMessageDialog("address invalid: something like https://10.0.0.180:38519");
      }
    }
  
private class McwMouseAdapter extends MouseAdapter
  {
  @Override
  public void mouseClicked(java.awt.event.MouseEvent evt) 
    {
    if ( ! parent.isRunModeOnClick() )
      return;
    
    int row = jtable.rowAtPoint(evt.getPoint());
    
    if ( row < 0 )
      return;
    
    row = jtable.convertRowIndexToModel(row);
    
    McwAddress addr = model.getAddress(row);
    
    runOnSelected(addr);
    }
  }

private final class ButtonListener implements ActionListener
  {
  @Override
  public void actionPerformed(ActionEvent event)
    {
    Object source = event.getSource();
    
    if ( source == clearButton )
      stat.mcwAddressModel.requestClear();
    else if ( source == deleteRow )
      rowDeleteFun();
    else if ( source == addAddressButton )
      addAddressFun();
    }
  }



  }
