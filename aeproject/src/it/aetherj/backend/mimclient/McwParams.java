/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient;

import it.aetherj.protocol.MimEntity;

public class McwParams
  {
  private MimEntity entity;

  public int onExitWait_s;       // seconds to wait on exit
  public boolean hasNoResponse;  // if true the job has no response

  public McwParams( MimEntity entity)
    {
    setEntity(entity);
    }

  public void setEntity(MimEntity entity)
    {
    this.entity=entity;
    
    if ( entity == null )
      throw new IllegalArgumentException("entity is null");
    }
  
  public MimEntity getEntity()
    {
    return entity;
    }
  
  public String getMethod()
    {
    return entity.getMethod();
    }
  
  public String toString ()
    {
    return entity.toString();
    }
  }
