/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient;

import java.net.URL;
import java.sql.SQLException;
import java.util.*;

import javax.swing.table.AbstractTableModel;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbtbls.*;
import it.aetherj.backend.mimclient.jobs.*;
import it.aetherj.backend.mimclient.ssl.McwJobStats;
import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;
import it.aetherj.shared.Aeutils;
import it.aetherj.shared.bim.BimAddress;
import it.aetherj.shared.v0.*;

/**
 * Hold the list of all Nodes that can be contacted by the server
 * There is a periodic task to load - save the model to dbase
 * Objects are saved here ONLY, if you want to insert a new aenode, you add it here, the keeperThread will sync with dbase
 * One of the key issue is that it is NOT possible to iterate over the list AND at the same time change it
 * So, there must be ONLY ONE thread that adds/remove addresses. 
 * Since only this thread knows when the list is not being updated, it is tthis thread that starts the status ping !!
 * Cannot have a separate thread for status request...
 */
public class McwAddressTableModel extends AbstractTableModel implements PrintlnProvider
  {
  private static final String classname="McwAddressTableModel";
  private static final long serialVersionUID = 1L;
  
  private static final int addressStatusIndexNULL=-1;  // this is a way to say there is no valid index
  
  private final FireTableDataChanged fireTableDataChanged = new FireTableDataChanged();
  private final SortByWorkLength sortByWorktime = new SortByWorkLength();

  private final Stat stat;
  
  public static final int col_id=0;
  public static final int col_url=1;
  public static final int col_res_code=2;
  public static final int col_start_time=3;
  public static final int col_job_len=4;
  public static final int col_ping_etime=5;
  public static final int col_sync_etime=6;
  public static final int col_srvtype=7;      // addresses may hold a normal Aether or AetherJ
  
  private static final int col_count=8;
  
  private final List<McwAddress> addressList;

  private final AeDbase dbase;   // the dbase to work with, connection will be done at start
  private final McwGcollect mcwGc;
  
  private PrintlnProvider logger;
  private Thread keeperThread;
  
  private AedbAddress aedbAddress;
  private AedbUkey    aedbUpki;
  
  private int addressStatusIndex=addressStatusIndexNULL;  
  private Date nextSyncBestAddressesTime=new Date();  // when to sync the best addresses
  
  public McwAddressTableModel(Stat stat)
    {
    this.stat=stat;
    this.dbase=stat.newDbase();
    
    addressList = new ArrayList<McwAddress>();
    
    mcwGc = new McwGcollect(stat, this);
    }
  
  
  
  
  
  private int findRowIndex ( String location, String sublocation, int port )
    {
    int len=getCount();
    
    if ( location == null || location.length() < 8 ) return -1;
    
    if ( sublocation == null ) sublocation="";
    
    for (int index=0; index<len; index++)
      {
      McwAddress row=getAddress(index);
      
      if ( row.getPort() != port ) continue;
      
      if ( ! location.equals(row.getLocation()) ) continue;
        
      if ( sublocation.equals(row.getSublocation())) return index;
      }

    return -1;
    }
    
  /**
   * This may be a new address or not, if new then add, if not just update it
   * NOTE: This must NOT actually do the dbase save, that is done later, in a separate thread
   * @param m_a
   */
  public synchronized McwAddress addressSave ( MimAddress m_a )
    {
    McwAddress risul;
    
    int index = findRowIndex(m_a.location, m_a.sublocation, m_a.port);
    
    if ( index >=0 )
      {
      risul = addressList.get(index);
      risul.setMimAddress(m_a);
      }
    else
      {
      risul = new McwAddress(m_a);
      addMcwAddress(risul);
      }
    
    return risul;
    }
  
  public synchronized McwAddress addressSave ( BimAddress m_a )
    {
    McwAddress risul;
    
    int index = findRowIndex(m_a.url_host, "", m_a.port);
    
    if ( index >=0 )
      {
      risul = addressList.get(index);
      risul.setMimAddress(m_a);
      }
    else
      {
      risul = new McwAddress(m_a);
      addMcwAddress(risul);
      }
    
    return risul;
    }
  
  
  public synchronized void requestClear ()
    {
    for ( McwAddress address : addressList )
      address.needs_db_delete=true;
    }
    
  /**
   * Removal needs to go trough the dbase, so it is actually queued
   * @param rowindex
   */
  public void remove (int rowindex)
    {
    McwAddress address = getAddress(rowindex);
    address.needs_db_delete=true;
    }

  /**
   * The thread is in charge of keeping the persistent storage in sync with instance objects
   * @param logger
   */
  public void start(PrintlnProvider logger)
    {
    this.logger=logger;
    
    if ( keeperThread == null )
      {
      keeperThread=Aeutils.newThreadStart(new AddressesKeeper(), classname+".AddressesKeeper");
      println("Synchronizer Thread started");
      }
    else
      {
      println("Synchronizer thread ALREADY started");
      }
    }
    
  @Override
  public void println(String message)
    {
    if ( logger != null )
      logger.println(message);
    else
      stat.println(message);
    }
  
  @Override
  public void println(String message,Throwable exc)
    {
    if ( logger != null )
      logger.println(message,exc);
    else
      stat.println(message,exc);
    }

  @Override
  public String toString ()
    {
    return classname+" count="+addressList.size();
    }
  
  /**
   * Recalculate the votes bound to the given post or thread that is associated with this vote_id
   * @param db
   * @param vote_id
   * @throws SQLException
   */
  public void recalcVotes ( AeDbase db, Integer vote_id ) throws SQLException
    {
    Brs ars = stat.aedbFactory.aedbVote.selectUsingPrimaryId(db, vote_id);
    
    Integer thread_id=null;
    Integer post_id=null;
    
    if ( ars.next() )
      {
      thread_id = ars.getInteger(AedbVote.cn_thread_id);
      post_id   = ars.getInteger(AedbVote.cn_post_id);
      }
    
    ars.close();
    
    if ( post_id != null )
      mcwGc.recalcPostVotes(db, post_id);
    else if ( thread_id != null )
      mcwGc.recalcThreadVotes(db, thread_id);
    else
      println(classname+"recalcThreadVotes: invalid vote_id="+vote_id);
    }

  
  
  private void dbaseLoad () throws SQLException
    {
    // this binds the dbase manager objects
    aedbAddress = stat.aedbFactory.aedbAddress;  
    aedbUpki = stat.aedbFactory.aedbUpki;
    
    // Pick up all addresses from DBASE
    Brs ars = aedbAddress.select(dbase);
    
    while ( ars.next() )
      addressList.add( new McwAddress(ars));

    ars.close();

    // make sure that boot address is available in the table model
    MimAddress boot=new MimAddress(stat.aeParams.getBootLocation());
    addressSave(boot);
    
    // now, even if there is just one, the loading is complete
    Aeutils.SwingInvokeLater(fireTableDataChanged);
    }
  

  public synchronized int getCount ()
    {
    return addressList.size();
    }

  public synchronized McwAddress getAddress (int index)
    {
    try
      {
      return addressList.get(index);
      }
    catch ( Exception exc )
      {
      return null;
      }
    }
    
  public synchronized void addMcwAddress(McwAddress mcwNode)
    {
    if ( mcwNode == null ) return;
    
    int rowindex = addressList.size();

    addressList.add(mcwNode);
    
    Aeutils.SwingInvokeLater(new FireRowInserted(rowindex));
    }
    
    
  @Override
  public String getColumnName(int column) 
    {
    switch ( column )
      {
      case col_id:  
        return "Id";
        
      case col_url:  
        return "URL";
        
      case col_start_time:  
        return "Start Time";

      case col_res_code:  
        return "Res Code";

      case col_job_len:  
         return "J.len(ms)";

      case col_ping_etime:  
        return "Ping end Time";

      case col_sync_etime:  
        return "Sync end Time";

      case col_srvtype:  
        return "Srvt";

      default:
        return "NULL col="+column;
      }
    }
    
  @Override
  public Class<?> getColumnClass(int col)
    {
    switch ( col )
      {
      case col_id:
      case col_res_code:
      case col_job_len:
        return Integer.class;
        
      case col_url:  
        return URL.class;
      
      case col_start_time:  
      case col_ping_etime:  
      case col_sync_etime:
        return Date.class;

      case col_srvtype:  
        return String.class;
        
      default:
        return Object.class;
      }
    }

  @Override
  public int getRowCount()
    {
    return addressList.size();
    }

  @Override
  public int getColumnCount()
    {
    return col_count;
    }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex)
    {
    McwAddress row = getAddress(rowIndex);

    if ( row == null ) return "null row="+rowIndex;
    
    McwJobStats stats = row.getRunStats();
    McwJobStats pingstats = row.getPingStats();
    McwJobStats syncstats = row.getSyncStats();
    
    switch ( columnIndex )
      {
      case col_id: 
        return row.getPrimaryId();

      case col_url: 
        return row.getUrl();

      case col_start_time: 
        return stats.getStartTime();

      case col_res_code: 
        return stats.getHttpResCode();

      case col_job_len: 
        return stats.getWorktime();

      case col_ping_etime: 
        return pingstats.getEndTime();

      case col_sync_etime: 
        return syncstats.getEndTime();
        
      case col_srvtype:  
        return row.getClientType();
      
      default:
        return "NULL col="+columnIndex;
      }
    }

  
  private synchronized void dbaseSyncRun () throws SQLException
    {
    Iterator<McwAddress>iter = addressList.listIterator();
      
    while(iter.hasNext())
      {
      McwAddress address = iter.next();
      
      if ( address.needs_db_delete )
        {
        println("dbaseSyncRun() deleting "+address);
        
        aedbAddress.dbaseDelete(dbase,address);
        
        iter.remove();
        }
      
      if ( address.needs_db_save )
        {
        println("dbaseSyncRun() saving "+address);

        aedbAddress.dbaseSave(dbase,address);
        address.needs_db_save=false;
        }
      }
    }

  /**
   * This makes a copy since the original may be "adjusted" by the user or in other ways
   * @param howmany
   * @return
   */
  private RoundRobinList getBestAddresses ( int howmany )
    {
    ArrayList<McwAddress> sorted = new ArrayList<McwAddress>();
    
    for ( McwAddress addr : addressList )
      {
      McwJobStats stats = addr.getRunStats();
      
      if ( ! stats.isOK() ) continue;
 
      sorted.add(addr);
      
      if ( sorted.size() >= howmany )
        break;
      }

    Collections.sort(sorted, sortByWorktime);
    
    return new RoundRobinList(sorted);
    }
  
    
  /**
   * Get all possible addresses using mim from the given host
   * NOTE that I am NOT giving up control of the thread, I do the work myself
   * @param address
   */
  private void getNewAddressesInitially (McwAddress address)
    {
    JobGetCache getAddresses = new JobGetCache(stat, address, MimEntityList.aeaddres);
    
    getAddresses.run(logger);
    
    // this will reset the fast scan at the beginning
    addressStatusIndex=addressStatusIndexNULL;
    }
  
  /**
   * Here I have to decide how and when to get new addresses
   * I should select all good addresses to work with and depending on how many I have I should decide
   * Note that I may decide to do something and then wait for next run
   * @throws SQLException 
   */
  private void getNewAddresses () throws SQLException
    {
    int wishCount = stat.aeParams.getBestAddressCount();
        
    // Pick up the best addresses I have
    RoundRobinList bestAddresses = getBestAddresses(wishCount);

    if ( bestAddresses.isEmpty() )
      {
      println("getNewAddresses: Cannot do anything, not any reachable address");
      return;
      }
    
    if ( getCount() < wishCount )
      {
      // If I have few addresses in the table I should try to get some more
      // once I have done this, I should let the algorithm run again
      // this path should happen just once in lifetime of application, the first run
      getNewAddressesInitially (bestAddresses.next());
      return;
      }
      
    // here I have some addresses to connect to, the next step in the lifecycle of application bootstrap is to get
    // the content for all entities. I cannot go in any random order since insert depends on foreign keys...
    if ( aedbUpki.getRowCount(dbase) > 10 ) return;
    
    for ( MimEntity entity : MimEntityList.entityList )
      {
      JobGetCache getentity = new JobGetCache(stat, bestAddresses.next(), entity);
      getentity.run(logger);
      }
    }

  private Date getNextSyncTime (Date now)
    {
    long next_t = now.getTime();
    
    int delta_s = stat.aeParams.getBestAddressEvery_s();
    
    next_t = next_t + delta_s * 1000;
    
    return new Date(next_t);
    }
  
  private void syncBestAddresses()
    {
    final Date now = new Date();
    
    if ( now.before(nextSyncBestAddressesTime)) return;
    
    println("syncBestAddresses(): RUN");
    
    nextSyncBestAddressesTime = getNextSyncTime(now);
    
    // Pick up the best addresses I have
    RoundRobinList bestAddresses = getBestAddresses(stat.aeParams.getBestAddressCount());

    if ( bestAddresses.isEmpty() )
      {
      println("syncBestAddresses: Cannot do anything, not any reachable address");
      return;
      }
    
    while ( bestAddresses.hasNext() )
      {
      McwAddress address = bestAddresses.next();
      
      McwParams params = new McwParams(MimEntityList.aekey);
//      params.onExitWait_s=5;
      
      JobPostSync sync = new JobPostSync(stat, address, params);
      stat.mcwConsole.queue(sync);
      }
    
    }
    
  
  private void addressesKeeperRun_B () throws SQLException
    {
    if ( stat.mcwConsole.isRunModeAutomatic() )
      {
      addressStatusTester();
      
      getNewAddresses();
      
      syncBestAddresses ();
      }
    
    dbaseSyncRun();

    Aeutils.SwingInvokeLater(fireTableDataChanged);
    }
  
  /**
   * Gets all addresses that can be used to fill a cache to be given outside
   * I need to return the McwAddress since I need the timestamp of latest activity
   * @return
   */
  private synchronized Iterator<McwAddress> selectForCache ( )
    {
    ArrayList<McwAddress> sorted = new ArrayList<McwAddress>();
    
    for ( McwAddress addr : addressList )
      {
      if ( addr.isPrivateLocation() )
        continue;
      
      McwJobStats stats = addr.getSyncStats();
      
      if ( stats != null && stats.hasEndTime() ) 
        {
        sorted.add(addr);
        continue;
        }
      
      stats = addr.getPingStats();

      if ( stats != null && stats.hasEndTime() ) 
        {
        sorted.add(addr);
        continue;
        }
      }

    Collections.sort(sorted, new SortByUpdateTime());
    
    return sorted.iterator();
    }

  public synchronized Iterator<MimAddress> selectForServlet ( MimFilterTimestamp filter )
    {
    ArrayList<MimAddress> sorted = new ArrayList<MimAddress>(addressList.size());

    MimTimestamp start = filter.getStart();
    MimTimestamp end = filter.getEnd();
    
    for ( McwAddress addr : addressList )
      {
      if ( addr.isPrivateLocation() )
        continue;
      
      Date updated = addr.getUpdateDate();
      
      if ( start.isNull() )
        {
        if ( end.isNull() )
          sorted.add(addr.getMimAddress());
        else
          {
          if ( updated.before(end.getDate()))
            sorted.add(addr.getMimAddress());
          }
        }
      else
        if ( end.isNull() )
          {
          if ( updated.after(start.getDate()))
            sorted.add(addr.getMimAddress());
          }
        else
          {
          if (  updated.after(start.getDate()) && updated.before(end.getDate()))
            sorted.add(addr.getMimAddress());
          }
      }

    Collections.sort(sorted, new SortMimaByUpdateTime());
    
    return sorted.iterator();
    }
  
  /**
   * This method should do a garbage collect, recalculation of cache values
   * It is here since this class loads the addresses from DB and is in charge of maintaining them 
   */
  private void performGcRefreshAllCache ()
    {
    try
      {
      println("performGcRefreshAllCache: START"); 
      
      mcwGc.garbageCollectRun(dbase);
      

      AedbFactory aef = stat.aedbFactory;
      
      // Need to drop the table holding the cache, maybe

      // TODO: need to expire the addresses too
      new McwCacheAddresses(stat, dbase, logger).refreshAddressCache(selectForCache());

      McwCacheMim.CacheMimParams params = new McwCacheMim.CacheMimParams(stat, this, dbase, new CryptoEddsa25519(), new MimPowCalculator());
      
      // I have to do them in any case since Aether need arrays to convert to Json
      // if it was a list it would have been way easier, also, if OO was used much less code.... whatever
      new McwCacheMim<MimKey,MimKeyIndex>(params, 
          MimEntityList.aekey, new MimKey[0], new MimKeyIndex[0]).refreshCache(aef.aedbUpki.selectForCache(dbase));
      
      new McwCacheMim<MimBoard,MimBoardIndex>(params, 
          MimEntityList.aeboard, new MimBoard[0], new MimBoardIndex[0]).refreshCache(aef.aedbBoard.selectForCache(dbase));
      
      new McwCacheMim<MimThread,MimThreadIndex>(params, 
          MimEntityList.aethread, new MimThread[0],new MimThreadIndex[0]).refreshCache(aef.aedbThread.selectForCache(dbase));

      new McwCacheMim<MimPost,MimPostIndex>(params, 
          MimEntityList.aepost, new MimPost[0],new MimPostIndex[0]).refreshCache(aef.aedbPost.selectForCache(dbase));
      
      new McwCacheMim<MimVote,MimVoteIndex>(params, 
          MimEntityList.aevote, new MimVote[0],new MimVoteIndex[0]).refreshCache(aef.aedbVote.selectForCache(dbase));

      new McwCacheMim<MimTruststate,MimTruststateIndex>(params, 
          MimEntityList.aetrustate, new MimTruststate[0],new MimTruststateIndex[0]).refreshCache(aef.aedbTstate.selectForCache(dbase));

      println("refreshAllCache: END");
      }
    catch ( Exception exc )
      {
      println("refreshAllCache",exc);
      }
    }
  
  
  /**
   * There are two things to synchronize, the first one is the dbase, saving data in dbase
   * The other thing is to regularly do a scan on addresses
   */
  private void addressKeeperRun_A()
    {
    try
      {
      dbase.connect(stat.config.mainDbaseProperty);
    
      if ( ! dbase.isConnected() )
        {
        println("addressesKeeper() dbase connect FAIL -> END");
        return;
        }
      
      println("addressesKeeper() dbase connected");
    
      dbaseLoad();
      
      performGcRefreshAllCache();
      
      
      
      while ( ! stat.waitHaveShutdownReq_s(20) )
        addressesKeeperRun_B();
      
      }
    catch ( Exception exc )
      {
      println("addressesKeeper()",exc);
      }
  
    // when sync run ends, it is time to close up
    dbase.close();
    
    // maybe nobody will see this
    println("addressesKeeper() END");
    }

  /**
   * This is in charge of stating jobs to test if an address is alive 
   * The first run should send quite a few, later, it should queue a different address every xxx seconds
   * MUST be on the same thread that changes the table, so, it knows that things are "stable"
   */
  private void addressStatusTester()
    {
    if ( addressStatusIndex < 0 )
      {
      addressStatusTesterFirst();
      return;
      }
    
    // manage the end of list, wrapping around
    if ( addressStatusIndex >= getCount() )
      addressStatusIndex = 0;

    // pick one and go to the next
    McwAddress address = getAddress(addressStatusIndex++);
    
    if ( address == null )
      {
      println("addressStatusTesterFirst: WArNING address==null");
      return;
      }
    
    JobGetStatus ping = new JobGetStatus(stat, address );
    stat.mcwConsole.queue(ping);
    }

  private void addressStatusTesterFirst()
    {
    int counter=0;

    println("addressStatusTesterFirst: CALL");
    
    for ( McwAddress address : addressList )
      {
      // this go trough all registered addresses and do an initial ping
      JobGetStatus ping = new JobGetStatus(stat, address );
      stat.mcwConsole.queue(ping);
      
      // TODO, need to make it a parameter, in config dbase
      if ( counter++ > 10 ) break;
      }
    
    // this signal that next time it is standard algorithm, continue from where it arrived
    addressStatusIndex=counter;
    }
    
private final class AddressesKeeper implements Runnable
  {
  public void run()
    {
    addressKeeperRun_A();
    }
  }


private final class FireRowInserted implements Runnable
  {
  int rowindex;
  
  FireRowInserted(int p_rowindex)
    {
    rowindex=p_rowindex;
    }
  
  public void run()
    {
    fireTableRowsInserted(rowindex,rowindex);
    }
  }

private final class FireTableDataChanged implements Runnable
  {
  public void run()
    {
    fireTableDataChanged();
    }
  }

private static final class SortByWorkLength implements Comparator<McwAddress>
  {
  @Override
  public int compare(McwAddress a1, McwAddress a2)
    {
    McwJobStats s1=a1.getRunStats();
    McwJobStats s2=a2.getRunStats();
    
    return s1.deltaWorkLength(s2);
    }
  }

private static final class SortByUpdateTime implements Comparator<McwAddress>
  {
  @Override
  public int compare(McwAddress a1, McwAddress a2)
    {
    Date s1=a1.getUpdateDate();
    Date s2=a2.getUpdateDate();
    
    return s1.compareTo(s2);
    }
  }

private static final class SortMimaByUpdateTime implements Comparator<MimEmessagesMethods>
  {
  @Override
  public int compare(MimEmessagesMethods a1, MimEmessagesMethods a2)
    {
    MimTimestamp s1=a1.peekLastUpdate();
    MimTimestamp s2=a2.peekLastUpdate();
    
    Date d1=s1.getDate();
    Date d2=s2.getDate();
    
    return d1.compareTo(d2);
    }
  }


private static final class RoundRobinList
  {
  private ArrayList<McwAddress>list;
  
  private int readIndex=0;
  
  RoundRobinList ( ArrayList<McwAddress>list )
    {
    this.list = list;
    }
  
  public boolean isEmpty ()
    {
    return list.isEmpty();
    }

  /**
   * The list has a next without wrapping if the readIndex is before the end of the list 
   * @return
   */
  public boolean hasNext ()
    {
    return readIndex < list.size();
    }
  
  public McwAddress next ()
    {
    if ( readIndex >= list.size() )
      readIndex=0;
    
    return list.get(readIndex++);
    }
  }

  }
