package it.aetherj.backend.mimclient.jobs;

import java.net.URL;

import it.aetherj.backend.Stat;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.McwURLConnection;
import it.aetherj.shared.AettpStatus;
import it.aetherj.shared.v0.*;

/**
 * Post the list of wishes for a specific content
 * If there is nothing to do, just say it
 */
public class JobPostWishes extends JobPostEntity
  {
  private static final String classname="JobPostWishes";
  
  public JobPostWishes ( Stat stat, McwAddress address, McwParams params )
    {
    super(stat,address, params);
    }
  
  @Override
  public AettpStatus runJob ()
    {
    try
      {
      URL from_url = getUrl();
      
      println("runJob GET "+from_url);
      
      McWishFingerprint  wish = stat.aedbFactory.aedbWanted.getFingerprints(mimcDbase,mcwParams.getEntity());
      
      if ( wish == null || wish.isEmpty() )
        {
        println("  wish list for "+mcwParams+" is empty");
        return AettpStatus.NoToDo;
        }

      MimFilterArray filters = new MimFilterArray();
      filters.add(wish.getMimFilter());
//      filters.add(new MimFilterTimestamp(new MimTimestamp(0),new MimTimestamp()));

      MimPayload payload = newMimPayload(filters);
            
      McwURLConnection conn = new McwURLConnection(stat,from_url,McwURLConnection.method_POST);

      AettpStatus res_code=conn.writeObject(getPrintln(), payload);
      
      if ( res_code.isFail() )
        {
        println(res_code.getMessage());
        return res_code;
        }
       
      MimPayload rx_payload = conn.getResponse(getPrintln(), MimPayload.class);
              
      int imported=mimParse(rx_payload,from_url);
      
      printlnConsole(classname+".runJob: entity="+mcwParams+" imported="+imported);

      return res_code;
      }
    catch ( Exception exc )
      {
      println("runJob",exc);
      return AettpStatus.Exception;
      }
    }  

  
  
  }
