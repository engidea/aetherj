package it.aetherj.backend.mimclient.jobs;

import java.io.IOException;
import java.net.URL;

import it.aetherj.backend.Stat;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.*;
import it.aetherj.protocol.MimEntity;
import it.aetherj.shared.AettpStatus;
import it.aetherj.shared.v0.*;

/**
 * AHHHHHH finally I got some results, now, I need to fully understand them...
 
     return address.getUrl("/v0/c0/votes/index.json");

     you get the content using
     
     /v0/addresses/cache_f9c0c3aff99534ec9b4795dc0a31a69c33d5c2193d23018a096b44128f1442eb/0.json
     /v0/c0/votes/cache_103f23479a41e1fcbe9f016051d8855663d0ab954bf89669892712d444de19f2/manifest/0.json
     /v0/c0/index/cache_2b3b27645da9ebf99832fcfc22c80cb00d25069c6ff8b0a17722156bc7e860f8/0.json
     
 */
public class JobGetCache extends McwRunner
  {
  private static final String classname="JobGetCache";
  
  public JobGetCache(Stat stat, McwAddress addr, MimEntity entity)
    {
    super(stat, addr, new McwParams(entity));
    }

  public JobGetCache(Stat stat, McwAddress addr, McwParams params)
    {
    super(stat, addr, params);
    }

  /**
   * This is the beginning, it gets the indexes for the endpoint I am serving
   * @return
   * @throws IOException
   */
  private MimResultsCache[] getCacheUrls (URL pre) throws IOException
    {
    String file = pre.getFile();
    URL connTo=new URL(pre.getProtocol(),pre.getHost(),pre.getPort(),file+"/index.json");
    
    McwURLConnection conn = new McwURLConnection(stat,connTo);
    
    AettpStatus res_code = conn.getResponseCode();

    println(res_code.getMessage());

    address.setPingStats(new McwJobStats(res_code, getJobStartTime()));

    if ( res_code.isFail() )
      {
      conn.close();
      return null;
      }
    
    MimPayload payload = conn.getResponse(getPrintln(), MimPayload.class);
    
    boolean pl_valid = payload.isSignatureValid(crypto);

    if ( ! pl_valid )
      {
      println(classname+".getCacheUrls() payload signature failed");
      return null;
      }
    
    return payload.results;
    }
  
  /**
   * Return the number of record imported
   * @param cache
   * @param level
   * @return the number of objects imported
   * @throws IOException
   */
  private int importCache( MimResultsCache cache, int level ) throws IOException
    {
    URL pre = super.getUrl();
    String file = pre.getFile();
    String post="/"+cache.response_url+"/"+level+".json";
    
    URL from_url=new URL(pre.getProtocol(),pre.getHost(),pre.getPort(),file+post);
    
    McwURLConnection conn = new McwURLConnection(stat,from_url);
    
    AettpStatus res_code = conn.getResponseCode();

    address.setPingStats(new McwJobStats(res_code, getJobStartTime()));
    
    if ( res_code.isFail() )
      {
      println(res_code.getMessage());
      conn.close();
      return 0;
      }
    
    MimPayload payload = conn.getResponse(getPrintln(), MimPayload.class);

    int imported = mimParse(payload, from_url);
    
    println("imported "+imported);
    
    return imported;
    }
  
  /**
   * This fails quite often, still need to understand what it is for
   * So far it seems easier just to get the actual cache of data...
   */
  private AettpStatus importManifest( MimResultsCache cache, int level ) throws IOException
    {
    URL pre = super.getUrl();
    String file = pre.getFile();
    String post="/"+cache.response_url+"/manifest/"+level+".json";
    
    URL from_url=new URL(pre.getProtocol(),pre.getHost(),pre.getPort(),file+post);
    
    McwURLConnection conn = new McwURLConnection(stat,from_url);
    
    AettpStatus res_code = conn.getResponseCode();

    address.setPingStats(new McwJobStats(res_code, getJobStartTime()));
    
    if ( res_code.isFail() )
      {
      println(res_code.getMessage());
      conn.close();
      return res_code;
      }
    
    MimPayload payload = conn.getResponse(getPrintln(), MimPayload.class);

    println("manifest imported "+mimParse(payload, from_url));
    
    return res_code;
    }

  /**
   * Return the number if items imported
   * @param caches
   * @param level
   * @return
   * @throws IOException
   */
  private int importCacheLevel (MimResultsCache[] caches, int level ) throws IOException
    {
    int imported=0;
    
    for (int index=0; index<caches.length; index++)
      {
      MimResultsCache row = caches[index];
      
//      importManifest(row,level);
      
      imported +=importCache(row,level);
      }
    
    return imported;
    }
  
  @Override
  public AettpStatus runJob ()
    {
    try
      {
      URL from_url = getUrl();
      
      println(classname+".runJob: "+from_url);
      
      MimResultsCache[] caches=getCacheUrls(from_url);
      
      if ( caches == null )
        {
        println("caches are EMPTY");
        return AettpStatus.NOContent;
        }
      
      int imported=0;
      
      for (int level=0; level<MimResultsCache.level_count; level++)
        imported += importCacheLevel(caches,level);
      
      println(classname+".runJob: imported="+imported);
      
      return AettpStatus.OK;
      }
    catch ( Exception exc )
      {
      println("runJob",exc);
      return AettpStatus.Exception;
      }
    }  
  }
