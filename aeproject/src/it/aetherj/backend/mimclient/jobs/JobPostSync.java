package it.aetherj.backend.mimclient.jobs;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbtbls.AedbTstamp;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.AettpStatus;
import it.aetherj.shared.v0.*;

/**
 * This should wrap up the logic in Sync state, so, first a post on node (to get the other node id)
 * then for all endpoints get the "index" (if I ever manage to get the way to do it)
 * and then a post to get the "missing content"
 */
public class JobPostSync extends McwRunner
  {
  private static final String classname="JobPostSync";
  
  private static final MimEntity []entityList = { 
      MimEntityList.aekey,  
      MimEntityList.aeboard, 
      MimEntityList.aethread, 
      MimEntityList.aepost, 
      MimEntityList.aevote };
  
  public JobPostSync(Stat stat, McwAddress addr, McwParams params)
    {
    super(stat, addr, params);
    }

  /**
   * The actual node for an address may have changed since last time, so, I cannot store it into the dbase...
   * @throws SQLException 
   */
  private Integer getNodePrimaryId () throws IOException, SQLException
    {
    // I need to work with the node entity now
    mcwParams.setEntity(MimEntityList.aenode);
    
    MimPayload payload = newMimPayload(null);
    
    resetJobStartTime();
    
    McwURLConnection conn = new McwURLConnection(stat,getUrl(),McwURLConnection.method_POST);

    AettpStatus res_code=conn.writeObject(getPrintln(), payload);

    if ( res_code.isFail() )
      {
      println(res_code.getMessage());
      address.setSyncStats(new McwJobStats(res_code, getJobStartTime()));
      conn.close();
      return null;
      }
    
    payload = conn.getResponse(getPrintln(), MimPayload.class);
    
    if ( payload == null )
      return null;
        
    return stat.aedbFactory.aedbNode.dbaseSave(mimcDbase, payload);
    }
  

//return address.getUrl("/v0/c0/index/cache_2b3b27645da9ebf99832fcfc22c80cb00d25069c6ff8b0a17722156bc7e860f8/0.json");

  private AettpStatus importIndex( MimResultsCache index ) throws IOException
    {
    URL pre = super.getUrl();
    String file = pre.getFile();
    String post="/"+index.response_url+"/0.json";
    
    URL from_url=new URL(pre.getProtocol(),pre.getHost(),pre.getPort(),file+post);
    
    McwURLConnection conn = new McwURLConnection(stat,from_url);
    
    AettpStatus res_code = conn.getResponseCode();
    
    println(res_code.getMessage());

    address.setSyncStats(new McwJobStats(res_code, getJobStartTime()));

    if ( res_code.isFail() )
      {
      conn.close();
      return res_code;
      }
    
    MimPayload payload = conn.getResponse(getPrintln(), MimPayload.class);
    
    println("imported "+mimParse(payload, from_url));
    
    return res_code;
    }
  
  
  
  private void syncEntity(Integer node_id, MimEntity entity ) throws SQLException, IOException
    {
    // from now on, work with this entity
    mcwParams.setEntity(entity);
    
    AedbTstamp aedbTstamp = stat.aedbFactory.aedbTstamp;
    
    MimTimestamp from = aedbTstamp.getLatestSyncTime(mimcDbase, node_id, entity);
    
    MimFilterArray filters = new MimFilterArray();
    filters.add(new MimFilterTimestamp(from, new MimTimestamp()));
    
    MimPayload payload = newMimPayload(filters); 
    
    resetJobStartTime();
    
    URL from_url = getUrl();
    
    McwURLConnection conn = new McwURLConnection(stat,from_url,McwURLConnection.method_POST);

    AettpStatus res_code=conn.writeObject(getPrintln(), payload);

    address.setSyncStats(new McwJobStats(res_code, getJobStartTime()));

    if ( res_code.isFail() )
      {
      println(res_code.getMessage());
      conn.close();
      }
    
    payload = conn.getResponse(getPrintln(), MimPayload.class);

    int imported=mimParse(payload, from_url);
    
    println("Entity="+entity+" imported="+imported);
    }

  @Override
  public AettpStatus runJob ()
    {
    try
      {
      Integer node_id=getNodePrimaryId();
      
      if ( node_id == null )
        return AettpStatus.NOContent;

      if ( stat.haveShutdownReq() )
        return AettpStatus.Shutdown;
      
      for ( MimEntity entity : entityList )
        {
        syncEntity ( node_id, entity);

        if ( stat.haveShutdownReq() )
          break;
        }
      
      return AettpStatus.OK;
      }
    catch ( Exception exc )
      {
      println("runJob",exc);
      return AettpStatus.Exception;
      }
    }  
  }
