/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient.jobs;

import java.io.IOException;
import java.net.URL;

import it.aetherj.backend.Stat;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.McwURLConnection;
import it.aetherj.shared.AettpStatus;
import it.aetherj.shared.v0.MimPayload;

/*
 
 Responses
The response is be paginated if needed, and in JSON format. Besides the response body, you receive a HTTP status code. There are three codes possible.

HTTP 200 OK
Returned when your request succeeds. The body of this request should be the answer you requested.

HTTP 400 Bad request
You will receive this if your request is malformed, or otherwise unserviceable.

HTTP 404 Not found
Returned when the content you requested isn't available.

If you get a 404 trying to get a new page of the result, you have reached to the end of the content provided by pagination. If the reply you have received had 18 pages, and you are trying to reach the 19th page, you will receive an 204 on the 19th page, because the answer will be empty.

HTTP 405 Method not allowed
You will receive this if you try to do a POST request on a aenode that does not support it, such as static nodes.

HTTP 429 Too many requests
You can receive this on any endpoint if the remote aenode becomes too busy.


 
 */
public class JobPostEntity extends McwRunner
  {
  private static final String classname="JobPostEntity";
  
  public JobPostEntity(Stat stat, McwAddress address, McwParams params ) 
    {
    super(stat,address,params);
    }
  
  @Override
  public AettpStatus runJob () throws IOException 
    {
    URL from_url = getUrl();
    
    println(classname+"runJob() "+from_url);

    MimPayload payload = newMimPayload();
    
    McwURLConnection conn = new McwURLConnection(stat,from_url,McwURLConnection.method_POST);

    AettpStatus res_code=conn.writeObject(getPrintln(), payload);

    println(res_code.getMessage());
    
    if ( res_code.isFail() )
      return res_code;
    
    MimPayload rx_payload = conn.getResponse(getPrintln(), MimPayload.class);
      
    int imported=mimParse(rx_payload, from_url);
    
    printlnConsole(from_url+" imported "+imported);

    return res_code;
    }

  }
