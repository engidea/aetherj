/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient.jobs;

import java.net.*;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.McwURLConnection;
import it.aetherj.shared.AettpStatus;
import it.aetherj.shared.v0.MimPayload;

/**
 * This should get aenode info and store them in the proper place
 */
public class JobGetEntity extends McwRunner
  {
  private static final String classname="JobGetEntity";
  
  public JobGetEntity(Stat stat, McwAddress addr, McwParams params ) 
    {
    super(stat, addr, params);
    }
  
  @Override
  public AettpStatus runJob ()
    {
    try
      {
      // This is trapped since  subclass needs the result value in a reliable way
      URL from_url = getUrl();
      
      println("runJob GET "+from_url);
      
      McwURLConnection conn = new McwURLConnection(stat,from_url);
            
      AettpStatus res_code = conn.getResponseCode();
      
      if ( res_code.isFail() || mcwParams.hasNoResponse )
        {
        println(res_code.getMessage());
        return res_code;
        }
      
      MimPayload payload = conn.getResponse(getPrintln(), MimPayload.class);
      
      int imported=mimParse(payload, from_url);

      printlnConsole(classname+" imported="+imported);
      
      return res_code;
      }
    catch ( SocketTimeoutException exc )
      {
      return new AettpStatus(AettpStatus.res_Timeout);
      }
    catch ( ConnectException exc )
      {
      return new AettpStatus(AettpStatus.res_ConnRefused);
      }
    catch ( NoRouteToHostException exc )
      {
      return new AettpStatus(AettpStatus.res_NoRoute);
      }
    catch ( MismatchedInputException exc )
      {
      setJobException(exc);
      println("AAAA",exc);
      return new AettpStatus(AettpStatus.res_Mismatch);
      }
    catch ( Exception exc )
      {
      println(classname+".runJob()",exc);
      setJobException(exc);
      return new AettpStatus(AettpStatus.res_Exception);
      }
    }


  }
