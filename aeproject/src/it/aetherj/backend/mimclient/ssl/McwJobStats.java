package it.aetherj.backend.mimclient.ssl;

import java.sql.Timestamp;
import java.util.Date;

import it.aetherj.shared.AettpStatus;

public class McwJobStats
  {
  private long start_time;
  private long end_time;
  private int  res_code;
  private int  worktime_ms;
  
  /**
   * Construct a "null" object
   */
  public McwJobStats()
    {
    }

  /**
   * This is used when "reconstructing" some basic info
   * do NOT set the res_code to res_OK since this is NOT an actual live test
   * @param p_start_time
   */
  public McwJobStats( Date end_time )
    {
    if ( end_time == null )
      return;
    
//    res_code=AettpStatus.res_OK;
    this.end_time=end_time.getTime();
    }
  
  public McwJobStats(AettpStatus res_code, long start_time_ms )
    {
    this.res_code=res_code.rescode;
    this.start_time=start_time_ms;
    
    long now = System.currentTimeMillis();
    
    if ( res_code.isOK() )
      this.end_time=now;

    // in any case I can do this
    worktime_ms=(int)(now-start_time);
    }

  public boolean isNull ()
    {
    return start_time == 0;
    }
  
  public boolean hasStartTime ()
    {
    return start_time != 0;
    }
  
  /**
   * A stats is OK if both time and rescode are good
   * @return true is above is valid
   */
  public boolean isOK ()
    {
    return start_time > 0 && res_code == AettpStatus.res_OK;
    }

  public boolean isAfter (McwJobStats other )
    {
    if ( other == null )
      return true;
  
    // this event is after the other if my start_time is bigger then the other
    long delta_time = start_time - other.start_time;
    
    return delta_time > 0;
    }
  
  public Date getStartTime ()
    {
    if ( isNull() )
      return null;
    
    return new Date(start_time);
    }

  public boolean isNullEndTime ()
    {
    return end_time == 0;
    }
  
  public boolean hasEndTime ()
    {
    return end_time != 0;
    }

  /**
   * Return Timestamp if there is an end time set, null otherwise
   * @return
   */
  public Timestamp getEndTime ()
    {
    return end_time != 0 ? new Timestamp(end_time) : null;
    }

  public Integer getHttpResCode ()
    {
    if ( isNull() )
      return null;
    
    return Integer.valueOf(res_code);
    }

  /**
   * How long it needed to complete the task, in milliseconds
   * @return
   */
  public Integer getWorktime ()
    {
    if ( isNull() )
      return null;
    
    return Integer.valueOf(worktime_ms);
    }
 
  public int deltaWorkLength ( McwJobStats other )
    {
    long delta_ms = worktime_ms - other.worktime_ms;
    
    if ( delta_ms > Integer.MAX_VALUE )
      delta_ms = delta_ms - Integer.MAX_VALUE;
    
    if ( delta_ms < Integer.MIN_VALUE )
      delta_ms = delta_ms + Integer.MAX_VALUE;

    return (int)delta_ms;
    }
  
  public int deltaStartTime ( McwJobStats other )
    {
    long delta_ms = start_time - other.start_time;
    
    if ( delta_ms > Integer.MAX_VALUE )
      delta_ms = delta_ms - Integer.MAX_VALUE;
    
    if ( delta_ms < Integer.MIN_VALUE )
      delta_ms = delta_ms + Integer.MAX_VALUE;

    return (int)delta_ms;
    }

  
  public String toString()
    {
    return res_code+" "+worktime_ms;
    }
  }
