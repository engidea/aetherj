/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient.ssl;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;

import it.aetherj.backend.Stat;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.shared.*;

public class McwURLConnection
  {
  private static final String classname="McwURLConnection";
  
  public static final String method_POST="POST";
  public static final String method_GET="GET";
  
  public static final String content_type="application/json";
  public static final String content_encoding="UTF-8";
 
  private final Stat stat;
  private final HttpsURLConnection conn;
  
  public McwURLConnection(Stat stat, URL toURL ) throws IOException
    {
    this(stat,toURL,method_GET);
    }
  /**
   * setChunkedStreamingMode
   * @param stat
   * @param toURL
   * @param reqMethod
   * @throws IOException
   */
  public McwURLConnection(Stat stat, URL toURL, String reqMethod) throws IOException
    {
    this.stat = stat;
    
    conn = (HttpsURLConnection)toURL.openConnection();
    
    conn.setSSLSocketFactory(stat.mimSSLFactory.sslSocketFactory);
    conn.setHostnameVerifier(stat.mimSSLFactory.hostnameVerifier);
    
    conn.setRequestProperty("Content-Type", content_type);
    conn.setRequestProperty("Accept", content_type);
    
    if ( reqMethod == null )
      setRequestMethod(method_GET);
    else
      setRequestMethod(reqMethod);
    
    setConnectTimeout(2000);
    }
  
  public void close()
    {
    conn.disconnect();
    }
  
  public void setDoOutput()
    {
    conn.setDoOutput(true);
    }
  
  public void setRequestMethod(String method) throws ProtocolException
    {
    conn.setRequestMethod(method);
    }
  
  public void setRequestProperty(String key, String value)
    {
    conn.setRequestProperty(key, value);
    }

  public void setConnectTimeout ( int timeout_ms )
    {
    conn.setConnectTimeout(timeout_ms);
    }
  
  public AettpStatus getResponseCode() throws IOException
    {
//    pippo();
    return new AettpStatus(conn.getResponseCode());
    }
    
  private void pippo ()
    {
    Map<String,List<String>> amap = conn.getHeaderFields();
    
    Set<String>keys = amap.keySet();
    
    if ( keys.size() < 1 ) return;
    
    for ( String s : keys )
      {
      StringBuilder kkeys = new StringBuilder(500);
      kkeys.append(s);
      kkeys.append(':');
      addHeaderValues(kkeys, amap.get(s));
      stat.println("---> "+kkeys.toString());
      }
    }

  private void addHeaderValues ( StringBuilder risul, List<String>alist )
    {
    if ( alist == null || alist.size() < 1 )return;

    for ( String s : alist )
      {
      risul.append(s);
      risul.append(',');
      } 
    }
  
  private OutputStream getOutputStream() throws IOException
    {
    return conn.getOutputStream();
    }
  
  public AettpStatus writeObject ( PrintlnProvider  print, Object obj) throws IOException
    {
    AeJson mapper = new AeJson();

    setDoOutput();  // first this
    
    OutputStream os = getOutputStream();
  
    if ( stat.dbg.shouldPrint(Aedbg.M_HTTP_tx,Aedbg.L_debug))
      {
      String tosend=mapper.writeValueAsString(obj);
      
      print.println(tosend);
      
      byte[] input = tosend.getBytes("utf-8");
      os.write(input, 0, input.length);
      }
    else
      {
      mapper.writeValue(os, obj);
      }
  
    return getResponseCode();
    }
  

  /**
   * This is WAY slower but it shows what is happening
   * Use stat.dbg.wishMaskLevel(0x50,0x1); to enable the test for serialize
   * Use https://json-diff.com/ to compare two json
   * @param println
   * @param valueType
   * @return
   * @throws IOException
   */
  private <T> T getResponseLogged (PrintlnProvider println, Class<T> valueType) throws IOException
    {
    InputStreamReader isr  = new InputStreamReader(conn.getInputStream());
    BufferedReader breader = new BufferedReader(isr);
    StringBuilder sbuilder = new StringBuilder(100000);
    
    String aline;
       
    while ((aline = breader.readLine()) != null)
      sbuilder.append(aline);
    
    breader.close();
  
    if ( sbuilder.length() <= 0 )
      {
      println.println("rx is empty");
      return null;
      }

    String received=sbuilder.toString();

    println.println(received);
    
    AeJson mapper = new AeJson();
    T robject = mapper.readValue(received, valueType);
    
    if ( stat.dbg.shouldPrint(Aedbg.M_compare_rx,Aedbg.L_debug))
      {
      // This is used when something goes wrong with the Json mapping... happens
      String compare = mapper.writeValueAsString(robject);
    
      if ( ! compare.equals(received))
        {
        println.println("Do NOT MATCH"); 
        println.println(compare);
        }
      else
        {
        println.println("Source == Compare");
        }
      }
    
    return robject; 
    }
  
  /**
   * Map directly into an object, way faster
   * NOTE that JsonParser.Feature.AUTO_CLOSE_SOURCE is normally TRUE
   * @param println
   * @param valueType
   * @return
   * @throws IOException
   */
  public <T> T getResponse (PrintlnProvider println, Class<T> valueType) throws IOException
    {
    if ( stat.dbg.shouldPrint(Aedbg.M_HTTP_rx,Aedbg.L_debug))
      return getResponseLogged(println,valueType);
    
    try
      {
      AeJson mapper = new AeJson();
      InputStreamReader isr = new InputStreamReader(conn.getInputStream());
      BufferedReader br =  new BufferedReader(isr);
      return mapper.readValue(br, valueType);
      }
    catch ( MismatchedInputException exc )
      {
      if ( stat.dbg.shouldPrint(Aedbg.M_HTTP_rx,Aedbg.L_notice))
        println.println("Possible EOF");
      
      return null;
      }
    }
  
  
  }
