/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient;

import java.awt.event.MouseAdapter;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.TableColumn;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.utils.JtableDateRenderer;
import it.aetherj.boot.ComponentProvider;

public class McwRunnerTable implements ComponentProvider
  {
  private final Stat stat;
  private final JScrollPane workPanel;
  private final McwRunnerTableModel model;  

  private JTable jtable;
  
  public McwRunnerTable(Stat stat )
    {
    this.stat = stat;
    this.model = stat.mcwRunningModel;
    
    workPanel = newWorkPanel();
    }
    
  private JScrollPane newWorkPanel()
    {
    jtable = new JTable(model);
    
    setColWidth(jtable, McwRunnerTableModel.col_id,40,100);
    
    TableColumn column = jtable.getColumnModel().getColumn(McwRunnerTableModel.col_start_date);
    column.setCellRenderer(new JtableDateRenderer(JtableDateRenderer.format_hhmmss));
    
    jtable.addMouseListener(new McwMouseAdapter());
//    McwSelectionListener listener = new McwSelectionListener();
//    jtable.getSelectionModel().addListSelectionListener(listener);
    
    JScrollPane risul = new JScrollPane(jtable);

    return risul;
    }
  
  private void setColWidth ( JTable jt, int col_index, int pref_w, int max_w )
    {
    TableColumn column = jt.getColumnModel().getColumn(col_index);
    column.setPreferredWidth(pref_w);
    column.setMaxWidth(max_w);
    }

    
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }
  
  public String toString()
    {
    return model.toString();
    }

private class McwMouseAdapter extends MouseAdapter
  {
  @Override
  public void mouseClicked(java.awt.event.MouseEvent evt) 
    {
    int row = jtable.rowAtPoint(evt.getPoint());
    row = jtable.convertRowIndexToModel(row);
    McwRunner runner = model.getRunner(row);
    runner.showFrame();
    }
  }

/**
 * This results active every time the row is selected, even if I have NOT clicked on it
 */
private class McwSelectionListener implements ListSelectionListener
  {
  public void valueChanged(ListSelectionEvent event)
    {
    if ( event.getValueIsAdjusting() ) return;

    int selection = jtable.getSelectedRow();
    if ( selection < 0 ) return;
    
    // convert the selection into the index of the table model
    selection = jtable.convertRowIndexToModel(selection);
    
    McwRunner impi = model.getRunner(selection);

    impi.showFrame();
    }
  }


 }
