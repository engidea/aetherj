package it.aetherj.backend.mimclient;

import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;

import it.aetherj.backend.dbtbls.AedbAddress;
import it.aetherj.backend.mimclient.ssl.McwJobStats;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.shared.bim.BimAddress;
import it.aetherj.shared.v0.MimAddress;

/**
 * NOTE: You cannot guarantee that a given Node is bound to an address
 * Surely you cannot store it into a dbase
 * The reason being that an address might end up to a different node
 */
public final class McwAddress
  {
  private Integer primary_id;
  
  private MimAddress mimAddress;     // not strictly needed but makes life simpler
  private BimAddress bimAddress;
  
  private Date update_time;          // when this address was used and produced valid results
  
  private McwJobStats ping_stats = new McwJobStats();    // calling out, the last SUCCESSFUL ping stat
  private McwJobStats sync_stats = new McwJobStats();    // the other calling in, again SUCCESSFUL
  
  // this will hold the most recent statistics, do NOT let it be null
  private McwJobStats latest_stats=new McwJobStats();  
    
  private String msg;
  
  public boolean needs_db_save;    // if true it needs a save into dbase
  public boolean needs_db_delete;  // if true it needs to be deleted from dbase (and also from table model)
    
  public McwAddress ( MimAddress row )
    {
    if ( row == null )
      throw new IllegalArgumentException("mima is null");

    mimAddress = row;
    mimAddress.validate();

    update_time=new Date();
    
    needs_db_save=true;  // signal that this address must be saved
    }
  
  public McwAddress ( BimAddress row )
    {
    if ( row == null )
      throw new IllegalArgumentException("row is null");

    bimAddress=row;
    mimAddress=null;

    update_time=new Date();
    
    needs_db_save=true;  // signal that this address must be saved
    }
  
  /**
   * Used in dbase to declare what type of client this is
   * NOTE that a client may change type if a user switch between AetherJ and Aether
   * @return
   */
  public String getClientType ()
    {
    if ( mimAddress != null )
      return AedbAddress.cv_client_type_aether;
    else if ( bimAddress != null )
      return AedbAddress.cv_client_type_aetherj;
    else 
      return null;
    }
  
  /**
   * This address does NOT needs to be saved into DB since it actually comes from the DB
   * TODO: Need to make it a static method with proper parameters
   * @param rs 
   */
  public McwAddress ( Brs rs )
    {
    primary_id=rs.getInteger(AedbAddress.cn_id);

    update_time=rs.getDate(AedbAddress.cn_update_time);
    
    String location=rs.getString(AedbAddress.cn_location);        // normally an IP address
    int port=rs.getInteger(AedbAddress.cn_port);
    
    mimAddress = new MimAddress(location, port);

    mimAddress.sublocation=rs.getString(AedbAddress.cn_sublocation);
    mimAddress.location_type=rs.getInteger(AedbAddress.cn_iptype, MimAddress.location_type_IIPv4);
    mimAddress.type=rs.getInteger(AedbAddress.cn_type, MimAddress.type_DYNAMIC);

    mimAddress.client.name=rs.getString(AedbAddress.cn_client_name);
    
    pushPingStats(new McwJobStats(rs.getDate(AedbAddress.cn_ping_time)));
    pushSyncStats(new McwJobStats(rs.getDate(AedbAddress.cn_sync_time)));
    
    mimAddress.validate(); 
    }
  
  public Integer getPrimaryId ()
    {
    return primary_id;
    }
  
  public Integer setPrimaryId ( Integer p_id )
    {
    return primary_id = p_id;
    }
  
  public boolean isNull ()
    {
    return primary_id==null;
    }
  
  public Timestamp getUpdateTimestamp ()
    {
    return new Timestamp(update_time.getTime());
    }
  
  public Date getUpdateDate ()
    {
    return update_time;
    }

  /**
   * Test is the location is a private address or invalid
   * TODO: need to add a test for 172.16 to 172.31
   * @return
   */
  public boolean isPrivateLocation ()
    {
    String location = getLocation();
    
    if ( location==null || location.length() < 8)
      return true;

    if ( location.startsWith("192.168.")) return true;
    
    if ( location.startsWith("127.")) return true;
    
    if ( location.startsWith("10.")) return true;

    return false;
    }
  
  public MimAddress getMimAddress()
    {
    return mimAddress;
    }
  
  public void setMimAddress(MimAddress mima)
    {
    if ( mima == null )
      return;
    
    mima.validate();
    
    mimAddress=mima;
    bimAddress=null;
    
    needs_db_save=true;
    }

  public void setMimAddress(BimAddress mima)
    {
    if ( mima == null )
      return;
    
    mima.validate();
    
    mimAddress=null;
    bimAddress=mima;
    
    needs_db_save=true;
    }

  public String getLocation ()
    {
    return mimAddress.location;
    }
    
  public String getSublocation()
    {
    return mimAddress.sublocation;
    }
  
  public int getPort()
    {
    return mimAddress.port;
    }
    
  /**
   * Returns latest run stats
   * @return a not null object, may be null in content
   */
  public McwJobStats getRunStats()
    {
    return latest_stats;
    }
  
  /**
   * @param w_id MUST be not null, otherwise NPE
   * @return
   */
  public boolean hasId ( Integer w_id )
    {
    if ( primary_id == null )
      return false;

    return w_id.equals(primary_id);
    }
      
  public void setPingStats (McwJobStats stats)
    {
    if ( stats == null ) 
      return;
      
    if ( stats.isAfter(latest_stats))
      latest_stats=stats;

    if ( stats.isOK ())
      {
      ping_stats=stats;
      update_time=stats.getStartTime();
      needs_db_save=true;
      }
    }
    
  /**
   * Used on dbase load
   */
  public void pushPingStats (McwJobStats stats)
    {
    if ( stats == null ) 
      return;
      
    ping_stats=stats;
    }

  public McwJobStats getPingStats()
    {
    return ping_stats;
    }
  
  public void setSyncStats  (McwJobStats stats)
    {
    if ( stats == null ) 
      return;

    if ( stats.isAfter(latest_stats))
      latest_stats=stats;

    if ( stats.isOK ())
      {
      sync_stats=stats;
      needs_db_save=true;
      }
    }
  
  public void pushSyncStats  (McwJobStats stats)
    {
    if ( stats == null ) 
      return;

    sync_stats=stats;
    }

  public McwJobStats getSyncStats()
    {
    return sync_stats;
    }
  
  public String getMsg ()
    {
    return msg;
    }
  
  public URL getUrl ()
    {
    try
      {
      return new URL("https",getLocation(),getPort(),mimAddress.sublocation);
      }
    catch ( Exception exc )
      {
      return null;
      }
    }

  public String toString()
    {
    StringBuilder risul = new StringBuilder(200);
    
    risul.append("[");
    risul.append(primary_id);
    risul.append("] ");
    risul.append(getLocation());
    risul.append(":");
    risul.append(getPort());
    
    return risul.toString();
    }

  /*
  public boolean equals(Object other)
    {
    if ( other instanceof McwAddress )
      {
      McwAddress oa = (McwAddress)other;

      if ( primary_id == null || oa.primary_id == null )
        return false;

      if ( latest_stats.work_start_ms == 0 || oa.latest_stats.work_start_ms == 0 )
        return false;

      return primary_id.equals(oa.primary_id) && latest_stats.work_start_ms == oa.latest_stats.work_start_ms; 
      }
    else
      {
      return false;
      }
    }
  
  @Override
  public int compareTo(McwAddress other)
    {
    if ( latest_stats.work_start_ms == 0 )
      return -1;
    
    if ( other.latest_stats.work_end_ms == 0 )
      return Integer.MAX_VALUE;
    
    int delta_ms = (int)(latest_stats.work_start_ms - other.latest_stats.work_end_ms);
    if ( delta_ms != 0 )
      return delta_ms;
    
    if ( primary_id == null )
      return -1;
    
    if ( other.primary_id == null )
      return Integer.MAX_VALUE;
    
    return primary_id-other.primary_id;
    }
*/
      
  }
