package it.aetherj.backend.mimclient;

import java.util.Iterator;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbtbls.AedbCache;
import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.protocol.*;
import it.aetherj.shared.AeJson;
import it.aetherj.shared.v0.*;

/**
 * Simply to wrap in a single space the method to generate cache pages
 * Address is a special handling, this should be similar for all other entities
 */
public class McwCacheMim<T extends MimEmessagesMethods, I extends MimIndexesMethods>
  {
  private static final String classname="McwCacheMim";
  
  private static final int cache_rows_max=200;
  private final Stat stat;
  private final AeDbase dbase;
  private final CryptoEddsa25519 crypto;
  private final AedbCache aedbCache;
  private final MimEntity entity;
  private final PrintlnProvider feedback;
  
  private final T [] r_type; 
  private final I [] i_type;

  public McwCacheMim(CacheMimParams params, MimEntity entity, T [] r_type, I [] i_type)
    {
    this.stat = params.stat;
    this.feedback=params.feedback;
    this.dbase=params.dbase;
    this.crypto=params.crypto;
    this.entity=entity;
    this.r_type=r_type;
    this.i_type=i_type;
        
    aedbCache = stat.aedbFactory.aedbCache;
    
    if ( entity.equals(MimEntityList.aeaddres))
      throw new IllegalArgumentException("Addresses MUST be handled differently");
    
    }
    
  private void println ( String msg )
    {
    feedback.println(msg);
    }
  
  public void refreshCache ( Iterator<T> source) throws Exception
    {
    // clear up previous cache
    aedbCache.dbaseDelete(dbase, entity);
    
    // now I have to split this into chunks of xxx items 
    // A sub method will generate a cache entry and add one line to the cache index...
    // until there are no more rows
    
    MimItemsArray<MimResultsCache> main_index = new MimItemsArray<MimResultsCache>(new MimResultsCache [0]);
    
    int insCounter=0;

    while ( source.hasNext() )
      insCounter += genCacheRow(source, main_index); 
    
    // Insert the whole index of the cache rows
    aedbCache.dbaseInsertIndex(dbase, entity, main_index);

    println(classname+".refreshCache: entity="+entity+" insCounter="+insCounter);
    }
          
  /**
   * This is called if there is at least one item to get
   * I know that the items are SORTED in ASCENDING order, so, the first one is the oldest and the last one youngest
   * @param source
   * @param main_index
   * @throws Exception
   */
  private int genCacheRow (Iterator<T>source, MimItemsArray<MimResultsCache> main_index ) throws Exception
    {
    int counter=0;

    MimItemsArray<T> rows = new MimItemsArray<T>(r_type);
    
    // manifests are all of the same type
    MimItemsArray<MimPageManifestE>manifests=new MimItemsArray<MimPageManifestE>(new MimPageManifestE[0]);
    
    // sub indexes are of different type... crap
    MimItemsArray<MimIndexesMethods>sub_index = new MimItemsArray<MimIndexesMethods>(i_type);

    T first_item=null;
    T last_item=null;
    
    while ( source.hasNext() && counter++ < cache_rows_max )
      {
      T mimrow = source.next();
      
      if ( ! mimrow.verifyFingerprint() )
        {
        // Cannot throw an exception since it will stop recreating the cache
        println(classname+".genCacheRow: "+entity+" Fingerprint BAD "+mimrow.peekFingerprint());
        continue;
        }
      
      if ( ! mimrow.verifySignature(crypto) )
        {
        // Cannot throw an exception since it will stop recreating the cache
        println(classname+".genCacheRow: "+entity+" Signature BAD "+mimrow.peekFingerprint());
        continue;
        }

      MimPowValue pow = mimrow.verifyPow();
      if ( ! pow.isValid() )
        {
        // Cannot throw an exception since it will stop recreating the cache
        println(classname+".genCacheRow: "+entity+" POW BAD "+mimrow.peekFingerprint());
        continue;
        }

      if ( first_item == null )
        first_item = mimrow;
      
      // the last insert will have this updated to actually be the last item
      last_item = mimrow;
      
      rows.add(mimrow);
      manifests.add(new MimPageManifestE(mimrow.peekFingerprint(),mimrow.peekLastUpdate()));
      sub_index.add(mimrow.peekMimIndex());
      }
    
    MimPayload pay_rows = stat.aeParams.newMimPayload(entity);
    pay_rows.caching.pregenerated=true;

    MimPayload pay_manifest = stat.aeParams.newMimPayload(entity);
    pay_manifest.caching.pregenerated=true;
    pay_manifest.endpoint = new MimEndpoint(MimEndpoint.mainfest);
    MimPageManifest mfest = new MimPageManifest(0,manifests.toArray());

    MimPayload pay_index = stat.aeParams.newMimPayload(entity);
    pay_index.caching.pregenerated=true;
    pay_manifest.endpoint = entity.getIndexEndpoint();
    
    // this is because the original Aether has arrays instead of lists, whatever + no usable OOP 
    if ( entity.equals(MimEntityList.aekey))
      {
      pay_rows.response.keys = (MimKey[])rows.toArray();
      pay_manifest.response.keys_manifest = mfest.tosSingleArray();
      pay_index.response.keys_index = (MimKeyIndex[])sub_index.toArray();
      }
    else if ( entity.equals(MimEntityList.aeboard))
      {
      pay_rows.response.boards = (MimBoard[])rows.toArray();
      pay_manifest.response.boards_manifest = mfest.tosSingleArray();
      pay_index.response.boards_index = (MimBoardIndex[])sub_index.toArray();
      }
    else if ( entity.equals(MimEntityList.aethread))
      {
      pay_rows.response.threads = (MimThread[])rows.toArray();
      pay_manifest.response.threads_manifest = mfest.tosSingleArray();
      pay_index.response.threads_index = (MimThreadIndex[])sub_index.toArray();
      }
    else if ( entity.equals(MimEntityList.aepost))
      {
      pay_rows.response.posts = (MimPost[])rows.toArray();
      pay_manifest.response.posts_manifest = mfest.tosSingleArray();
      pay_index.response.posts_index = (MimPostIndex[])sub_index.toArray();
      }
    else if ( entity.equals(MimEntityList.aevote))
      {
      pay_rows.response.votes = (MimVote[])rows.toArray();
      pay_manifest.response.votes_manifest = mfest.tosSingleArray();
      pay_index.response.votes_index = (MimVoteIndex[])sub_index.toArray();
      }
    else if ( entity.equals(MimEntityList.aetrustate))
      {
      pay_rows.response.truststates = (MimTruststate[])rows.toArray();
      pay_manifest.response.truststates_manifest = mfest.tosSingleArray();
      pay_index.response.truststates_index = (MimTruststateIndex[])sub_index.toArray();
      }
    
    CryptoEddsa25519 crypto = new CryptoEddsa25519();
    
    pay_rows.signPayload(crypto,     stat.aeParams.getPrivateKey());
//    verifyPayloadSignature(pay_rows);
    
    pay_manifest.signPayload(crypto, stat.aeParams.getPrivateKey());
    pay_index.signPayload(crypto,    stat.aeParams.getPrivateKey());
    
    // now, I can ask to save to the cache for this lot of rows
    Integer row_id = aedbCache.dbaseInsert(dbase, pay_rows, pay_manifest, pay_index );
    
    // This pick up the time from the first and the last, it should be correct
    MimTimestamp starts_from = first_item.peekLastUpdate();
    MimTimestamp ends_at = last_item.peekLastUpdate();
    
    // now I can add to the index this entry
    MimResultsCache index_row = new MimResultsCache(row_id,starts_from,ends_at);
    main_index.add(index_row);
    
    return counter;
    }
  
  private void verifyPayloadSignature ( MimPayload input ) throws JsonProcessingException
    {
    AeJson mapper = new AeJson();
    
    if ( input.isSignatureValid(crypto) )
      {
      println(classname+".vfpl1: "+entity+" VALID");
      }
    else
      {
      println(classname+".vfpl1: "+entity+" BAD");
      }
    
    String asstring = mapper.writeValueAsString(input);
    
    MimPayload rebuild = mapper.readValue(asstring, MimPayload.class);

    if ( rebuild.isSignatureValid(crypto) )
      {
      println("  rebuild VALID");
      }
    else
      {
      println("  rebuild BAD");
      }

    
    }
    
  
  
public static class CacheMimParams
  {
  public final Stat stat;
  public final AeDbase dbase;
  public final CryptoEddsa25519 crypto;
  public final MimPowCalculator powCalc;
  public final PrintlnProvider feedback;
  
  public CacheMimParams ( Stat stat, PrintlnProvider feedback, AeDbase dbase, CryptoEddsa25519 crypto, MimPowCalculator powCalc )
    {
    this.stat=stat;
    this.feedback=feedback;
    this.dbase=dbase;
    this.crypto=crypto;
    this.powCalc = powCalc;
    }
  }
  
  }