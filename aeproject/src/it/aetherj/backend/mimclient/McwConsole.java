/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient;

import java.awt.BorderLayout;
import java.security.NoSuchAlgorithmException;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.gui.WinposStore;
import it.aetherj.backend.gui.utils.ComponentProviderSaveWinpos;
import it.aetherj.boot.*;
import it.aetherj.shared.*;


/**
 * The Mim client is the one in charge of deciding what to do, the "server" is stateless
 * The first question is where to connect and when, there is the need for
 * - get the list of possible connected nodes
 * - decide who to connect to
 * - get the most up to date info
 * the first need the bootstrapper, that basically gives a way to do a GET on addresses
 * the second is a periodic job that scan the list of addresses and ping + recors ping time + decide who to delete
 * the third takes a certain number ot "best hit" and try to get info
 * 
 * There is the need to have a bunch of "Runnables" + some GUI to have feedback on what is happening
 * NOTE: A client informs a server of its existence trough a POST on some endpoint, with the right info.
 * 
 * If a record cannot be found while inserting, create a new istance of DataToGet with the ID to get
 * Also, when getting initial content, get all, even most recent upki and then all boards
 * Missing content happens quite often with Votes....
 */
public final class McwConsole implements ComponentProviderSaveWinpos
  {
  private static final String classname="McwConsole";
    
  public static final String rm_Auto="Automatic";
  public static final String rm_Onclick="On Click";
  
  public static final String re_Auto="Automatic";
  public static final String re_Status="Status";     // normally a GET request    /v1/status
  public static final String re_Node="Node";         // can be both GET and POST  /v1/aenode
  public static final String re_PingNode="PingNode";         // can be both GET and POST  /v1/aenode
  public static final String re_Address="Addresses";
  public static final String re_Aekeys="Public Keys";
  public static final String re_Board="Boards";
  public static final String re_Thread="Thread";
  public static final String re_Post="Post";
  public static final String re_Vote="Vote";
  
  public static final String rs_Auto="Automatic";
  public static final String rs_RX_Cache="Receive Cache";
  public static final String rs_POST_Sync="POST Sync";
  public static final String rs_RX_Wish="Receive Wished";
  public static final String rs_GET="HTTP GET";
  public static final String rs_POST="HTTP POST";
  
//  private static final String[] rm_Combo = { rm_Auto, rm_Onclick };
  private static final String[] rm_Combo = { rm_Auto, rm_Onclick };
  private static final String[] re_Combo = { re_Auto, re_Status, re_Node, re_PingNode, re_Address, re_Aekeys, re_Board, re_Thread, re_Post, re_Vote };
  private static final String[] rs_Combo = { rs_Auto, rs_RX_Cache, rs_POST_Sync, rs_RX_Wish, rs_GET, rs_POST };
  
  private final Stat stat;
  private final JPanel  workPanel;
  private final JDesktopPane desktopPanel;

  private final LimitedTextArea logArea;
  private final JTextField feedbackField;

  private final AeDbase dbase;    // use this one to operate with the dbase and leave stat.dbase free !

  
  private JComboBox<String> runMode;   // Automatic, OnClick
  private JComboBox<String> runEntity;  // Automatic, Address, Key(UPKI), Board, Thread, Post, Vote 
  private JComboBox<String> runState;  // Automatic, Sync, Bootstrap, Introduction
  
  private final McwRunnerTable runnerTable;
  private final McwAddressTable addressTable;

  private McwScheduler mcwScheduler;  
   
  private int tickCounter;
  
  /**
   * Constructor
   * @param stat
   * @throws NoSuchAlgorithmException 
   */
  public McwConsole(Stat stat) 
    {
    this.stat = stat;
    this.logArea       = new LimitedTextArea(3000);
    this.desktopPanel  = new JDesktopPane();
    this.runnerTable   = new McwRunnerTable(stat);
    this.addressTable  = new McwAddressTable(stat, this);
    this.mcwScheduler  = new McwScheduler(stat,this);
    this.runMode       = new JComboBox<String>(rm_Combo);
    this.runEntity     = new JComboBox<String>(re_Combo);
    this.runState      = new JComboBox<String>(rs_Combo);
    
    dbase = stat.newDbase();
    
    feedbackField = new JTextField(4);
    feedbackField.setEditable(false);
    
    workPanel = newWorkPanel();
    
    for ( McwTask task : mcwScheduler )
      addDesktopPanel(task,task.getTaskName());
    }
      
  private JPanel newWorkPanel()
    {
    addDesktopPanel(logArea,"Mim Client Console Log");
    addDesktopPanel(addressTable,"Mim Addresses Table");
    addDesktopPanel(runnerTable,"Mim Running Table");
    
    JPanel aPanel = new JPanel(new BorderLayout());

    aPanel.add(newNorthPanel(),BorderLayout.NORTH);
    aPanel.add(desktopPanel,BorderLayout.CENTER);
    
    return aPanel;
    }


  String getRunEntity ()
    {
    return (String)runEntity.getSelectedItem();
    }

  String getRunState ()
    {
    return (String)runState.getSelectedItem();
    }

  private String getRunMode ()
    {
    return (String)runMode.getSelectedItem();
    }

  public boolean isRunModeAutomatic ()
    {
    return rm_Auto.equals(getRunMode());
    }

  public boolean isRunModeOnClick ()
    {
    return rm_Onclick.equals(getRunMode());
    }

  /**
   * Add a JComponet to the desktop
   * @param provider
   */
  private void addDesktopPanel ( ComponentProvider provider, String title )
    {
    if ( provider == null ) 
      {
      stat.println(classname+".addDesktopPanel: ERROR provider==null title="+title);
      return;
      }
    
    JComponent panel = provider.getComponentToDisplay();
    JInternalFrame internal = new JInternalFrame(title);
    internal.add(panel);

    desktopPanel.add(internal);
    
    internal.pack();
    internal.setBounds(stat.winposStore.getWindowBounds(internal.getTitle()));
    internal.setVisible(true);
    internal.setIconifiable(true);
    internal.setResizable(true);
    }

  @Override
  public void saveWindowPosition (WinposStore wpstore)
    {
    for (JInternalFrame frame :  desktopPanel.getAllFrames() )
      wpstore.putWindowBounds(frame);
    }

  private JLabel newJLabel(String msg)
  {
  return new JLabel(msg);
  }
  
  private JPanel newNorthPanel ()
    {
    JPanel aPanel = new JPanel();

    aPanel.add(newJLabel("Run Mode"));
    aPanel.add(runMode);
    
    aPanel.add(newJLabel(" On Entity "));
    aPanel.add(runEntity);
    
    aPanel.add(newJLabel(" State "));
    aPanel.add(runState);
    
    aPanel.add(feedbackField);
    
    return aPanel;
    }
    
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }


  void queue ( McwRunner element )
    {
    if ( stat.dbg.shouldPrint(Aedbg.M_scheduler,Aedbg.L_debug))
      println("queuing "+element);
    
    mcwScheduler.queue(element);
    }
  
  /**
   * Actually start this server as a daemon.
   */
  public void startServer()
    {
    Aeutils.newThreadStart(new McwConsoleRunnable(), "Mim Client Worker");
    }
  
  /*
  private void testPow () throws JsonProcessingException
    {
    String in = "{\"node_public_key\":\"8842538dcb073544f7c113c6ae863990052d8f06f109db0a880257e98a4a78e9\",\"page_signature\":\"ae983c85cfe9d92dc5745018a9c575cccd3a378f71065f90f5b99a4232490f30ad311682a67fc885c4f268971931a8cea96c7f9bbd08238b22dc23bb27cae803\",\"proof_of_work\":\"\",\"nonce\":\"a5250ee35aa478bcda0fa106755f426c5956375dfb090dc156e020b4c0246061\",\"entity_version\":1,\"aeaddres\":{\"location\":\"\",\"sublocation\":\"\",\"location_type\":4,\"port\":10536,\"type\":2,\"protocol\":{\"version_major\":1,\"version_minor\":0,\"subprotocols\":[{\"name\":\"c0\",\"version_major\":1,\"version_minor\":0,\"supported_entities\":[\"board\",\"thread\",\"post\",\"vote\",\"key\",\"truststate\"]}]},\"client\":{\"version_major\":2,\"version_minor\":0,\"version_patch\":0,\"name\":\"Aether\"},\"entity_version\":1,\"realm_id\":\"\"},\"filters\":[{\"type\":\"timestamp\",\"values\":[\"0\",\"0\"]}],\"timestamp\":1611168792,\"pagination\":{\"pages\":0,\"current_page\":0},\"caching\":{\"pregenerated\":false,\"current_cache_url\":\"\",\"entity_counts\":null},\"response\":{}}";
    
    String calculated = "MIM1:15::::QKmuyDNYorBzawUf:35668:b388f38561c332c02d094cdbeeb6df4ca151dc7a7b1ec0edb8189fe2bced79b5f4d81ee8021b261adaf1c832a8e2a99a7f8ef19d8dacf50fc781b24586c1780d";
  
    MimPowValue pow = new MimPowValue(calculated);
    
    MimPowCalculator pwoc = new MimPowCalculator();
    boolean valid = pwoc.isPowValid(in,pow);
  
    println("Testing POW isValid="+valid);
    }
 */



  

  /**
   * @Override
   * @param msg
   */
  public void postFeedback(String msg)
    {
    logArea.println("postFeedback: "+msg);
    }
    
  /**
   * @Override
   * @param feedback
   */
  public void logFeedback(String feedback)
    {
    try
      {
      postFeedback(feedback);
      dbase.dbaseLogInsert(null,Dbkey.logall_svc_backend,null,feedback);
      }
    catch ( Exception exc )
      {
      println(classname+".logFeedback: msg="+feedback,exc);
      }
    }

  /**
   * @Override
   * @param error
   */
  public void logError(String error)
    {
    try
      {
      println("logError: "+error);
      dbase.dbaseLogInsert(null,Dbkey.logall_svc_backend,null,error);
      }
    catch ( Exception exc )
      {
      println(classname+".logError: msg="+error,exc);
      }
    }

  /**
   * @Override
   * @param error
   */
  public void postError(String error )
    {
    logArea.println("postError: "+error);
    }

  public PrintlnProvider getLog ()
    {
    return logArea;
    }
  
  void println(String message)
    {
    logArea.println(message);
    }

  void println(String message, Throwable exception)
    {
    logArea.println(message,exception);
    }
    
  public String toString ()
    {
    StringBuilder builder = new StringBuilder(2000);
    
    builder.append(classname+" List of Clients \n");
    
    builder.append(runnerTable.toString());
    
    return builder.toString();
    }
  
    
  private void mcwConsoleRun()
    {
    println(classname+".mcwConsoleRun() START");
    
    dbase.connect(stat.config.mainDbaseProperty);

    if ( ! dbase.isConnected() )
      {
      println("  NO dbase available, aborting");
      return;        
      }
    
    stat.mcwAddressModel.start(logArea);
    mcwScheduler.start();

    while ( ! stat.waitHaveShutdownReq_s(3) )
      {
      feedbackField.setText(Integer.toString(tickCounter++));

      if ( tickCounter > 9 ) tickCounter=0;
      }
    }
  
private final class McwConsoleRunnable implements Runnable
  {
  private static final String classname="McwConsoleRunnable";
  
  public void run ()
    {
    try
      {
      mcwConsoleRun();
      } 
    catch ( Exception exc )
      {
      println(classname,exc);
      }
    finally
      {
      dbase.close();
      }
    
    println(classname+" Thread END");
    }
  }


  }
