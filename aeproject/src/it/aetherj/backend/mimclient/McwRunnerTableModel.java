/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient;

import java.util.*;

import javax.swing.table.AbstractTableModel;

import com.google.gwt.http.client.URL;

/**
 * Hold the list of Mcw that are currently active, running
 */
public class McwRunnerTableModel extends AbstractTableModel
  {
  private static final long serialVersionUID = 1L;
  
  public static final int col_id=0;
  public static final int col_url=1;
  public static final int col_start_date=2;
  public static final int col_count=3;
  
  private final ArrayList<McwRunner> runnerList = new ArrayList<McwRunner>();

  public synchronized int getCount ()
    {
    return runnerList.size();
    }

  public synchronized McwRunner getRunner (int index)
    {
    try
      {
      return runnerList.get(index);
      }
    catch ( Exception exc )
      {
      return null;
      }
    }
    
  public synchronized void addRunner(McwRunner mcwNode)
    {
    if ( mcwNode == null ) return;
    
    int rowindex = runnerList.size();

    runnerList.add(mcwNode);
    
    fireTableRowsInserted(rowindex,rowindex);
    }
    
  public synchronized void removeRunner(McwRunner mcwNode)
    {
    if ( mcwNode == null ) return;
    
    int rowindex = runnerList.size();

    runnerList.remove(mcwNode);
    
    fireTableRowsDeleted(rowindex,rowindex);
    }
    
  @Override
  public String getColumnName(int column) 
    {
    switch ( column )
      {
      case col_id:  
        return "Id";
        
      case col_url:  
        return "URL";
        
      case col_start_date:  
        return "Start Date";
        
      default:
        return "NULL col="+column;
      }
    }
    
  @Override
  public Class<?> getColumnClass(int col)
    {
    switch ( col )
      {
      case col_id:
        return Integer.class;
        
      case col_url:  
        return URL.class;
        
      case col_start_date:  
        return Date.class;
      
      default:
        return Object.class;
      }
    }

  @Override
  public int getRowCount()
    {
    return runnerList.size();
    }

  @Override
  public int getColumnCount()
    {
    return col_count;
    }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex)
    {
    McwRunner mcwNode = getRunner(rowIndex);

    if ( mcwNode == null ) return "null row="+rowIndex;
    
    switch ( columnIndex )
      {
      case col_id: 
        return mcwNode.getAddressId();

      case col_url: 
        return mcwNode.getUrl();

      case col_start_date: 
        return mcwNode.getJobStartDate();
        
      default:
        return "NULL col="+columnIndex;
      }
    }
  
  
  public String toString ()
    {
    StringBuilder builder = new StringBuilder(2000);
    
    for ( McwRunner row : runnerList )
      {
      builder.append(" "+row);
      builder.append("\n");
      }
    
    return builder.toString();
    }
  
  }
