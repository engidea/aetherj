/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase;

import java.sql.*;

import org.hsqldb.types.Types;

import it.aetherj.backend.mimclient.ssl.McwJobStats;
import it.aetherj.boot.dbase.Dprepared;
import it.aetherj.protocol.*;
import it.aetherj.shared.bim.BimAddress;
import it.aetherj.shared.v0.*;

public class MimDprepared extends Dprepared
  {
  public MimDprepared(AeDbase dbase, PreparedStatement prepared)
    {
    super(dbase, prepared);
    }
  
  public void setValue(int insIndex, BimAddress addr ) throws SQLException
    {
    if ( addr == null || addr.isNull() )
      super.setNull(insIndex,Types.VARCHAR);
    else
      super.setString(insIndex,addr.toUrl());
    }

  public void setValue(int insIndex, MimTimestamp tstamp ) throws SQLException
    {
    if ( tstamp == null || tstamp.isNull() )
      super.setNull(insIndex,Types.TIMESTAMP);
    else
      super.setTimestamp(insIndex,tstamp.getTimestamp());
    }
  
  public void setValue(int insIndex, MimLang lang ) throws SQLException
    {
    if ( lang == null || lang.isNull() )
      super.setNull(insIndex,Types.VARCHAR);
    else
      super.setString(insIndex,lang.toString());
    }
  
  public void setValue(int insIndex, MimMeta meta ) throws SQLException
    {
    if ( meta == null || meta.isNull() )
      super.setNull(insIndex,Types.VARCHAR);
    else
      super.setString(insIndex,meta.toString());
    }

  public void setValue(int insIndex, MimFingerprint fprint ) throws SQLException
    {
    if ( fprint == null || fprint.isNull() )
      super.setNull(insIndex,Types.VARBINARY);
    else
      super.setBinary(insIndex, fprint.getFingerprint());
    }
  
  public void setValue(int insIndex, MimPublicKey pkey ) throws SQLException
    {
    if ( pkey == null || pkey.isNull() )
      super.setNull(insIndex,Types.VARBINARY);
    else
      super.setBinary(insIndex, pkey.getPub_A_bytes() );
    }
  
  public void setValue(int insIndex, MimPrivateKey pkey ) throws SQLException
    {
    if ( pkey == null || pkey.isNull() )
      super.setNull(insIndex,Types.VARBINARY);
    else
      super.setBinary(insIndex, pkey.getSeed_bytes() );
    }

  public void setValue ( int insIndex, MimPowValue row ) throws SQLException
    {
    if ( row == null )
      super.setNull(insIndex,Types.VARCHAR);
    else
      super.setString(insIndex, row.toString() );
    }
  
  public void setValue ( int insIndex, MimSignature row ) throws SQLException
    {
    if ( row == null )
      super.setNull(insIndex,Types.VARBINARY);
    else
      super.setBinary(insIndex, row.getBytes() );
    }

  public void setValue ( int insIndex, MimEntity row ) throws SQLException
    {
    if ( row == null )
      super.setNull(insIndex,Types.VARCHAR);
    else
      super.setString(insIndex, row.getValue() );
    }
  
  public void setValue(int insIndex, McwJobStats tstamp ) throws SQLException
    {
    if ( tstamp == null || tstamp.isNullEndTime() )
      super.setNull(insIndex,Types.TIMESTAMP);
    else
      super.setTimestamp(insIndex,tstamp.getEndTime());
    }

  public void setDate ( int insIndex, java.util.Date adate ) throws SQLException
    {
    if ( adate == null )
      super.setNull(insIndex,Types.TIMESTAMP);
    else
      super.setTimestamp(insIndex,new Timestamp(adate.getTime()));
    }
  
  
  
  }
   
