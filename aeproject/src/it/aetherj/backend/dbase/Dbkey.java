/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase;

public interface Dbkey
  {
  // info on database types
  String pgcol_val_type_dec  = "dec";
  String pgcol_val_type_int  = "int";
  String pgcol_val_type_long = "long";   // new as dec 2012
  String pgcol_val_type_str  = "str";
  String pgcol_val_type_bin  = "bin";
  String pgcol_val_type_date = "date";
  String pgcol_val_type_bool = "bool";
  
  String label_tbl = "mlang.label_tbl";
  String label_id = "label_id";
  String label_english = "label_english";
  String label_last_use_day = "label_last_use_day";

  String lblsee_tbl = "mlang.lblsee_tbl";
  String lblsee_id = "lblsee_id";
  String lblsee_label_id = "lblsee_label_id";
  String lblsee_path = "lblsee_path";

  String lblmap_tbl      = "mlang.lblmap_tbl";
  String lblmap_id       = "lblmap_id";
  String lblmap_label_id = "lblmap_label_id";
  String lblmap_lang = "lblmap_lang";
  String lblmap_value = "lblmap_value";

  
  String  pagehelp_tbl  = "mlang.pagehelp_tbl";
  String  pagehelp_id   = "pagehelp_id";
  String  pagehelp_lang = "pagehelp_lang";
  String  pagehelp_key  = "pagehelp_key";
  String  pagehelp_pdf  = "pagehelp_pdf";
  String  pagehelp_desc = "pagehelp_desc";
  
  // Generic table to log events 
  String logall_tbl         = "aether.logall_tbl";
  String logall_id          = "logall_id";
  String logall_date        = "logall_date";
  String logall_service     = "logall_service";   // something like logall_svc_backend
  String logall_dboper      = "logall_dboper";    // some sort of db operation
  String logall_aeusr_id    = "logall_aeusr_id";
  String logall_aeusr_ident = "logall_aeusr_ident";
  String logall_message     = "logall_message";
  String logall_exception   = "logall_exception";   // if it is an exception, then it is here

  String logall_svc_backend = "backend";
  
  
  
  
  }
