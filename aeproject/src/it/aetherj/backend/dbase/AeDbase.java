/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Locale;

import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;

public final class AeDbase extends Dbase   
  {
  private static final String classname="AeDbase";
  
	public static final String  getdate_function="now()";  

	public static final String  db_true="true";     // use 1 for sqlexpress
	public static final String  db_false="false";   // use 0 for sqlxpress
  
  public AeDbase(PrintlnProvider log)
    {
    super(log);
    }
        
  public MimDprepared getPreparedStatement ( StringBuilder statement ) throws SQLException
    {
    PreparedStatement risul = newPreparedStatement(statement.toString());
    return new MimDprepared(this,risul);    
    }

  
  public MimDprepared getPreparedStatement ( String statement ) throws SQLException
    {
    PreparedStatement risul = newPreparedStatement(statement);
    return new MimDprepared(this,risul);    
    }

  public MimDprepared getPreparedStatementReturnKeys (String statement ) throws SQLException
    {
    PreparedStatement risul = newPreparedStatementReturnKeys(statement);
    return new MimDprepared(this,risul);
    }
  
  public void deleteTableContent ( String tableName ) throws SQLException
    {
    String query = "DELETE FROM "+tableName;
    Dprepared prepared = super.getPreparedStatement(query);
    int risul = prepared.executeUpdate();
//    logger.userPrintln(classname+"deleteTableContent tbl="+tableName+" ris="+risul);
    prepared.close();
    }
    

	/**
	 * Drop the given table, fully qualified
	 * @param tableName
	 * @throws SQLException
	 */
  public void dropTable ( String tableName ) throws SQLException
    {
    String query = "DROP TABLE "+tableName;
    Dprepared prepared = super.getPreparedStatement(query);
    int risul = prepared.executeUpdate();
    logger.println(classname+"dropTable tbl="+tableName+" ris="+risul);
    prepared.close();
    }

  public static final Timestamp getSqlTimestamp ()
    {
    return new Timestamp(System.currentTimeMillis());
    }
    
  public static final Timestamp getSqlTimestamp (java.util.Date javaDate )
    {
    // let me be fair, if you give me null I give null back !
    if ( javaDate == null ) return null;
    
    return new Timestamp(javaDate.getTime());
    }
    
  public Brs selectTableUsing ( String table_name, String column_name, Integer key )
    {
    if ( key == null ) 
      return new Brs(this,null,new IllegalArgumentException(classname+"selectTableUsing: NULL values are not allowed as key"));
    
		return selectTableUsing(table_name, column_name, key.intValue());
    }

  public Brs selectTableUsing ( String table_name, String column_name, int key )
    {
    String query = "SELECT * FROM "+table_name+" WHERE "+column_name+"=?";

    try
      {
      Dprepared prepared = getPreparedStatement(query);
      prepared.setInt(1,key);
      return prepared.executeQueryTry();
      }
    catch ( Exception exc)
      {
      logger.println(classname+"selectTableUsing:",exc);
      return new Brs(this,null,exc);
      }
    }

  public Brs selectTableUsing ( String table_name, String column_name, String key )
    {
    String query = "SELECT * FROM "+table_name+" WHERE "+column_name+"=?";

    try
      {
      Dprepared prepared = getPreparedStatement(query);
      prepared.setString(1,key);
      return prepared.executeQueryTry();
      }
    catch ( Exception exc)
      {
      logger.println(classname+"selectTableUsing:",exc);
      return new Brs(this,null,exc);
      }
    }
  
  
  public Brs selectTable ( String table_name )
    {
    String query = "SELECT * FROM "+table_name;

    try
      {
      Dprepared prepared = getPreparedStatement(query);
      return prepared.executeQueryTry();
      }
    catch ( Exception exc)
      {
      logger.println(classname+"selectTable:",exc);
      return new Brs(this,null,exc);
      }
    }

  /**
   * An utility method to set a value in a prepared statement giving the index, the type and the value.
   * It handles correctly null values.
   * @param prepared
   * @param preparedIndex
   * @param typeSql
   * @param value
   * @throws SQLException
   */
  public static Dprepared setPreparedValue ( Dprepared prepared, int preparedIndex, int typeSql, Object value ) throws SQLException
    {
    try
      {
			if ( value == null )
				return prepared.setNull(preparedIndex, typeSql);
			
      switch ( typeSql )
        {
        case Types.INTEGER:
          return prepared.setInt(preparedIndex,(Integer)value);
          
        case Types.VARCHAR:
          return prepared.setString(preparedIndex,(String)value);
          
        case Types.DECIMAL:
          return prepared.setBigDecimal(preparedIndex,(BigDecimal)value);
  
        case Types.TIMESTAMP:
          return prepared.setTimestamp(preparedIndex,getSqlTimestamp((java.util.Date)value));
  
        case Types.VARBINARY:
          return prepared.setBinary(preparedIndex,(byte[])value);
  
        case Types.BOOLEAN:
          return prepared.setBoolean(preparedIndex,(Boolean)value);
        }
      }
    catch ( Exception exc )
      {
      throw new IllegalArgumentException(classname+"setPreparedValue: preparedIndex="+preparedIndex+" typeSql="+typeSql+" value="+value+" exception="+exc);
      }

    throw new IllegalArgumentException(classname+"setPreparedValue: preparedIndex="+preparedIndex+" UNSUPORTED typeSql="+typeSql+" value="+value);
    }

  /**
   * Given the current table definition, check that such table exists in the Dbase
   * @return true if the table exist, false otherwise.
   */
  public boolean dbTableExist ( String table_name) throws SQLException
    {
		String query= "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE table_name=? ";
    
		// table names in schema are all upper case
		String uc_table_name = table_name.toUpperCase();
		
    Dprepared prepared = getPreparedStatement(query);
    prepared.setString(1,uc_table_name);

    Brs ars = prepared.executeQuery();
    
    boolean risul = ars.next();
    
    ars.close();    
    
    return risul;
    }


    
  /**
   * I will do a search for best match in the dbase, this means that if in the dbase I have it-IT
   * if someone ask me for it I will match the it-IT in any case
   * NO, NOT possible since this results in duplicated labels since if someone inserts using it it will also match it-IT
   * so you MUST use the strict equals and get the languages right.
   * @param language
   * @return
   * @throws SQLException
   */
  public Brs getLabelsAndTranslation (Locale language) throws SQLException
    {
    String query = "SELECT * FROM mlang.label_tbl "+
      " left outer join "+
      " ( select * from mlang.lblmap_tbl where lblmap_lang=? ) as current_tbl "+ 
      " ON lblmap_label_id=label_id "+
      " ORDER BY label_english ASC ";

    logger.println(classname+"getLabelsAndTranslation: language="+language);
    
    Dprepared prepared = getPreparedStatement(query);
    prepared.setString(1,language.toString());
    return prepared.executeQuery();
    }
    
    
      
  private Integer getLabelId ( String label_english ) throws SQLException
    {
    Integer risul = null;
    Brs ars = selectLabel ( label_english );    
    if ( ars.next() ) risul = ars.getInteger(Dbkey.label_id);
    ars.close();
    return risul;
    }


  private Brs selectLabel ( String label_english ) throws SQLException
    {
    String query = "SELECT * FROM "+Dbkey.label_tbl+" WHERE "+Dbkey.label_english+"=?";
    Dprepared prepared = getPreparedStatement(query);
    prepared.setString(1,label_english);
    return prepared.executeQuery();
    }
  
  /**
   * Insert the given label into the dbase 
	 * If it fails, possibly because the label already exists then null is returned
   * @param label_english
   * @return the newly inserted label_id
   */
  private Integer insertLabel (String label_english ) throws SQLException
    {
		String query = "INSERT INTO  "+Dbkey.label_tbl+" ( " +Dbkey.label_english+" ) VALUES ( ? ) ";
		Dprepared prepared=null;
		
		try
			{
			Integer risul = null;
			prepared = getPreparedStatementReturnKeys(query);
			prepared.setString(1,label_english);
			Brs ars = prepared.executeUpdateReturnKeys();
			if ( ars.next() ) risul = ars.getInteger(1);
			ars.close();  
			prepared.close();
			return risul;
			}
		catch ( Exception exc )
			{
			if ( prepared != null ) prepared.close();
			return null;				
			}
    }
  
  private int updateLabelUseDate ( Integer label_id ) throws SQLException
    {
    String query = "UPDATE " + Dbkey.label_tbl + " SET " +Dbkey.label_last_use_day +"=? WHERE "+Dbkey.label_id+"=?";
    
    Dprepared prepared = getPreparedStatement(query);
    
    int index=1;
    prepared.setTimestamp(index++);
    prepared.setInt(index++,label_id);
    
    int risul = prepared.executeUpdate();
    prepared.close();
    return risul;
    }
    
  private Brs selectLabelmap ( Integer label_id, Locale lang_id ) throws SQLException
    {
    String query = "SELECT * FROM "+Dbkey.lblmap_tbl+" WHERE "+Dbkey.lblmap_label_id+"=? AND "+Dbkey.lblmap_lang+"=?";
    Dprepared prepared = getPreparedStatement(query);
    prepared.setInt(1,label_id);
    prepared.setString(2,""+lang_id);
    return prepared.executeQuery();
    }

  private Integer getLabelmapId ( Integer label_id, Locale lang_id  ) throws SQLException
    {
    Integer risul = null;
    Brs ars = selectLabelmap(label_id,lang_id);
    if ( ars.next() ) risul = ars.getInteger(Dbkey.lblmap_id);
    ars.close();
    return risul;
    }
  
  /**
   * Insert the given label map into the dbase 
   * @param label_english
   * @return the newly inserted label_id
   */
  private Integer insertLabelmap ( Integer label_id, Locale lang_id, String label_value ) throws SQLException
    {
    Integer risul = null;
    
    String query = "INSERT INTO  "+Dbkey.lblmap_tbl+
      " ( " +Dbkey.lblmap_label_id+","+Dbkey.lblmap_lang+","+Dbkey.lblmap_value+" ) " +
      " VALUES ( ?,?,? ) ";
    
    Dprepared prepared = getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setInt(insIndex++,label_id);
    prepared.setString(insIndex++,""+lang_id);
    prepared.setString(insIndex++,label_value);

    Brs ars = prepared.executeUpdateReturnKeys();
    if ( ars.next() ) risul = ars.getInteger(1);
    ars.close();  
    prepared.close(); 

    return risul;
    }


  private int updateLabelmap ( Integer labelmap_id, String label_value ) throws SQLException
    {
    String query = "UPDATE  " + Dbkey.lblmap_tbl + " SET " +Dbkey.lblmap_value +"=? WHERE "+Dbkey.lblmap_id+"=?";
    
    Dprepared prepared = getPreparedStatement(query);
    int insIndex=1;
    prepared.setString(insIndex++,label_value);
    prepared.setInt(insIndex++,labelmap_id);
    int risul = prepared.executeUpdate();
    prepared.close();
    return risul;
    }

  /**
   * This can also be used to update the real label value
   * @param label_id
   * @param lang_id
   * @param label_value
   * @return
   * @throws SQLException
   */
  private int updateLabelValue (Integer label_id, Locale lang_id, String label_value ) throws SQLException
    {
    // skip null value
    if ( label_value == null ) return 0;
    
    // and also empty value (I accept a single char....)
    if ( label_value.length() < 1 ) return 0;
    
    Integer labelmap_id = getLabelmapId ( label_id, lang_id );
    if ( labelmap_id == null ) 
      labelmap_id = insertLabelmap ( label_id, lang_id, label_value );
    
    if ( labelmap_id == null ) 
      throw new IllegalArgumentException(classname+"labelSaveSeen: Cannot insert label_value="+label_value);

    return updateLabelmap(labelmap_id, label_value);
    }


  private Brs selectLabelSee ( Integer label_id, String see_path ) throws SQLException
    {
    String query = "SELECT * FROM "+Dbkey.lblsee_tbl+" WHERE "+Dbkey.lblsee_label_id+"=? AND "+Dbkey.lblsee_path+"=?";
    Dprepared prepared = getPreparedStatement(query);
    prepared.setInt(1,label_id);
    prepared.setString(2,see_path);
    return prepared.executeQuery();
    }
  
  
  
  private int updateLabelSeen ( Integer label_id, String from_class ) throws SQLException
    {
    if ( from_class == null ) return 0;

    Brs ars = selectLabelSee(label_id, from_class);
  
    // if there is not already a row, then insert it
    if ( ! ars.next() ) insertLabelSeen (label_id,from_class );

    ars.close();
    return 1;
    }
  
  private void insertLabelSeen ( Integer label_id, String from_class ) throws SQLException
    {
    if ( from_class == null ) return;

		String query = "INSERT INTO  " + Dbkey.lblsee_tbl + " ( " + Dbkey.lblsee_label_id + "," + Dbkey.lblsee_path + " ) VALUES ( ?,? ) ";
		Dprepared prepared = null;
		
		try
			{
			prepared = getPreparedStatement(query);
			prepared.setInt(1,label_id);
			prepared.setString(2,from_class);
			prepared.executeUpdate();
			prepared.close();
			}
		catch ( Exception exc )
			{
			if ( prepared != null ) prepared.close();	
			}
    }
  
  /**
   * This is a request to update the seen value of a label, I do not have the label id and this is correct !
   * This can be used to update the labels from a text file too !!!
   * @param lang_id the language we are working with
   * @param label_english the english label to update
   * @param from_class if null then no update of class usage is done
   * @param label_value the actual value to sed into the dbase
   */
  public void labelSave ( Locale lang_id, String label_english, String from_class, String label_value )  throws SQLException  
    {
    if ( label_english == null ) return;
    
    if ( label_english.length() < 1 ) return;
    
		// attempt insert, will fail if the label already exists
		Integer label_id = insertLabel(label_english);		
		if ( label_id == null ) label_id = getLabelId(label_english);
    if ( label_id == null ) throw new IllegalArgumentException(classname+"labelSave: Cannot handle label="+label_english);
    
    // update label use date only if I am givena class where the update is taking place
    if ( from_class != null ) updateLabelUseDate(label_id);
    
    // if the given value is not null then update the value
    updateLabelValue ( label_id, lang_id, label_value );
    
    // last part to update is the path reference..
    updateLabelSeen ( label_id, from_class );
    }
    
  /**
   * Inserts an event into permanent storage.
   * Generally speaking messages should be in English since we need to understand then if something goes wrong.
   */
  public int dbaseLogInsert ( 
    Integer user_id, 
    String logall_service,
    String logall_dboper,
    String event_desc ) throws SQLException
    {
    // this is a reasonable fix to avoid DB error
    if ( logall_service == null ) logall_service = "null service: FIXIT";
    
    String query = "INSERT INTO "+Dbkey.logall_tbl+" ( "+
      Dbkey.logall_date+","+
      Dbkey.logall_service+","+
      Dbkey.logall_dboper+","+
      Dbkey.logall_aeusr_id+","+
      Dbkey.logall_aeusr_ident+","+
      Dbkey.logall_message+
      " ) VALUES ( ?,?,?,?,?,? )";
    
    Dprepared prepared = getPreparedStatement(query);
    
    int index = 1;
    prepared.setTimestamp(index++);
    prepared.setString(index++,logall_service);
    prepared.setString(index++,logall_dboper);
    prepared.setInt(index++,user_id);
    prepared.setString(index++,"user_id="+user_id);
    prepared.setString(index++,event_desc);
    
    int risul = prepared.executeUpdate();
    prepared.close();
    return risul;
    }

  
  
/*    
  public void finalize() throws Throwable
    {
    // Can be comment out when I am sure garbaging is OK
    logger.userPrintln(classname+"finalize");
    super.finalize();
    }
*/    

  }
