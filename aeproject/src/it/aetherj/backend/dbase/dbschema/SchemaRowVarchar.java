/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase.dbschema;

import java.sql.Types;

public class SchemaRowVarchar extends SchemaRow
  {
  public SchemaRowVarchar(String col_name, int col_size)
    {
    super(col_name, Types.VARCHAR);

    super.col_size = col_size;
    }
 
  /**
   * HSQLDB does not need a size for a VARCHAR if the LONGVARCHAR version is used
   */
  protected String getColTypeString()
    {
    return "VARCHAR(" + col_size + ")";
//    return "LONGVARCHAR";
    }
  }
