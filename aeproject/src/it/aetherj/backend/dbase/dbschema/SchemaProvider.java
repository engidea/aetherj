/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase.dbschema;

import java.sql.*;
import java.util.ArrayList;

import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;

/**
 * I need a way to delete tables before creating them, this is especially true for
 * views and similar, also true for constraints...
 * So, all bits and pieces that do this kind of operation will implement this.
 * Together with a Table and a couple of checkboxes I should be able to do what I need.
 * The new version is like this following the following ideas.
 * 1) I need to bundle table/contraint/indexes creation with the class that is actually
 * providing services, it is just simpler AND allows for extending the server.
 * 2) I need more granularity for creation /destroy, maning I canot put all constraints
 * and indexes in the same place, they have to be one for each table.
 *
 * So, the idea is that there is a registry of dbschema provider and when an ambito is added
 * then it adds the right schema.
 * NOTE: Depending on the actual content of views or other bits and pieces it may happen that
 * the order of operations is not the right one, so just ask for delete or create twice
 * Unfortunately the "perfect" solution is just plain difficult or impossible.
 *
 * 13aprile2009 I need a way to check if the table is "correct", meaning that it represents
 * what is wanted by the code. I need this since otherwise I will end with maintenance issues
 * and I would need to check table differences by hand.
 * VIEW creation is an issue when you start having a net of relationship (the same apply for tables)
 * The best way is to alter views to "refresh" them, but I need a registry of views...
 *
 */
public abstract class SchemaProvider implements Comparable<SchemaProvider>
  {
  private static final String classname="SchemaProvider.";
  
  private final ArrayList<SchemaRow>schema_rows =  new ArrayList<SchemaRow>();
  
  public SchemaRow addSchemaRow ( SchemaRow arow )
    {
    schema_rows.add(arow);      
    return arow;
    }
  
  /**
   * This may return null if no table should be created, happens
   * @param db
   * @return
   */
  private String getCreateTableStatement (ExecuteProvider db)
    {
    String tbl_name = getTableName();
    
    if ( tbl_name == null ) return null;
    
    StringBuilder risul = new StringBuilder(1000);
    boolean add_comma = false;
    
    risul.append("CREATE TABLE "+tbl_name+" ( ");

    for ( int index=0; index<schema_rows.size(); index++ )
      {
      // if I need to add a comma just do it and put a space after it
      if ( add_comma ) risul.append(", ");
      
      SchemaRow row = schema_rows.get(index);
      
      risul.append(row.getRowCreateStatement(db));
      
      // after the first round always add a comma
      add_comma = true;
      }
    
    // closed parenthesis at the end
    risul.append(" ) ");
    
    return risul.toString();
    }
  
  // null means do nothing, false means delete, true means create.
  Boolean createTable      = null;
  Boolean createView       = null;
  Boolean createConstraint = null;
  Boolean createIndex      = null;
   
  public final boolean isDropTable ()
    {
    return createTable != null && createTable.booleanValue() == false;
    }
    
  public final boolean isCreateTable ()
    {
    return createTable != null && createTable.booleanValue();
    }
    
  public final boolean isDropView ()
    {
    return createView != null && createView.booleanValue() == false;
    }
    
  public final boolean isCreateView ()
    {
    return createView != null && createView.booleanValue();
    }

  public final boolean isDropConstraint ()
    {
    return createConstraint != null && createConstraint.booleanValue() == false;
    }
    
  public final boolean isCreateConstraint ()
    {
    return createConstraint != null && createConstraint.booleanValue();
    }

  public final boolean isDropIndex ()
    {
    return createIndex != null && createIndex.booleanValue() == false;
    }
    
  public final boolean isCreateIndex ()
    {
    return createIndex != null && createIndex.booleanValue();
    }

  /**
   * This returns a standard combo creation statement based on the given prefix/table
   * @param comboTableName
   * @return
   */
  protected String getCreateComboStatement ( String comboTableName )
    {
    String comboPrefix = comboTableName.substring(0,comboTableName.length()-4); 
    
    StringBuilder risul = new StringBuilder();
    risul.append("CREATE TABLE "+comboTableName+" ( "); 
    risul.append(comboPrefix+"_id INTEGER NOT NULL PRIMARY KEY, ");
    risul.append(comboPrefix+"_val VARCHAR(30) NOT NULL, ");
    risul.append(comboPrefix+"_desc VARCHAR(40), ");
    risul.append("row_index INTEGER ");       // Used to order things in the combo
    risul.append(" ) ");    
    return risul.toString();
    }

  /**
   * Use this one when you need a varchar ID
   * @param comboTableName
   * @return
   */
  protected String getCreateComboStatementStrid ( String comboTableName )
    {
    String comboPrefix = comboTableName.substring(0,comboTableName.length()-4); 
    
    StringBuilder risul = new StringBuilder();
    risul.append("CREATE TABLE "+comboTableName+" ( "); 
    risul.append(comboPrefix+"_id VARCHAR(10) NOT NULL PRIMARY KEY, ");
    risul.append(comboPrefix+"_val VARCHAR(30) NOT NULL, ");
    risul.append(comboPrefix+"_desc VARCHAR(40), ");
    risul.append("row_index INTEGER ");       // Used to order things in the combo
    risul.append(" ) ");    
    return risul.toString();
    }

  /**
   * This returns a standard combo creation statement based on the given prefix/table
   * @param comboTableName
   * @return
   */
  protected String getCreateComboStatementString ( String comboTableName )
    {
    String comboPrefix = comboTableName.substring(0,comboTableName.length()-4); 
    
    StringBuilder risul = new StringBuilder();
    risul.append("CREATE TABLE "+comboTableName+" ( "); 
    risul.append(comboPrefix+"_id VARCHAR(30) NOT NULL PRIMARY KEY, ");
    risul.append(comboPrefix+"_val VARCHAR(30) NOT NULL, ");
    risul.append(comboPrefix+"_desc VARCHAR(40), ");
    risul.append("row_index INTEGER ");       // Used to order things in the combo
    risul.append(" ) ");    
    return risul.toString();
    }


  protected void insertIntoCombo ( ExecuteProvider db, String table_name, int id, String value, String desc ) 
    {
    String comboPrefix = table_name.substring(0,table_name.length()-4); 

    if ( desc == null ) desc = "";
    
    StringBuilder risul = new StringBuilder();
    risul.append("INSERT INTO "+table_name+" ( "); 
    risul.append(comboPrefix+"_id, ");
    risul.append(comboPrefix+"_val, ");
    risul.append(comboPrefix+"_desc, ");
    risul.append("row_index ");       // Used to order things in the combo
    risul.append(" ) VALUES (");
    risul.append(id+",");   
    risul.append("'"+value+"',");   
    risul.append("'"+desc+"',");   
    risul.append(id+")");   
    
    db.executeDbaseDone(risul.toString());    
    }

  protected void insertIntoCombo ( ExecuteProvider db, String table_name, String id, String value, String desc ) 
    {
    String comboPrefix = table_name.substring(0,table_name.length()-4); 

    if ( desc == null ) desc = "";
    
    StringBuilder risul = new StringBuilder();
    risul.append("INSERT INTO "+table_name+" ( "); 
    risul.append(comboPrefix+"_id, ");
    risul.append(comboPrefix+"_val, ");
    risul.append(comboPrefix+"_desc ");
    risul.append(" ) VALUES (");
    risul.append("'"+id+"',");   
    risul.append("'"+value+"',");   
    risul.append("'"+desc+"')");   
    
    db.executeDbaseDone(risul.toString());    
    }


  /**
   * Implements the comparable so I can sort up all the tables.
   * See SchemaTableModel and the sort done on the schema providers
   * @param obj
   * @return
   */
  @Override
  public int compareTo(SchemaProvider provider)  
    {
    if ( provider == null ) return -1;
    
    String my_key = getTableModelKey();
    String ot_key = provider.getTableModelKey();
    
    if ( my_key == null || ot_key == null ) return -2;
    
    return my_key.compareTo(ot_key);
    }

  /**
   * This is used to display a string in the table model and normally report the table name
   * However, if table name is null then you should override this to report something decent
   * @return
   */
  public String getTableModelKey ()
    {
    return getTableName();
    }
    
  /**
   * Returns the name of the table we are working with.
   * NOTE: Even create and drop table may not be provided since we may just be handling views...
   * In this case returning null is appropriate
   */
  public String getTableName ()
    {
    return null;
    }
  
  /**
   * The idea is that when the system starts it will call verify table to verify that the current schema
   * is actually what the system wants.
   * System start should stop and a request for service should be display.
   * @param dbexec
   * @return true if schema is correct, false otherwise
   */
  public boolean isTableSchemaCorrect ( ExecuteProvider dbexec ) throws SQLException 
    { 
    String t_name = getTableName();
    
    if ( t_name == null ) return true;
    
    // this should work on both hsqldb and postgres
    Brs ars = dbexec.executeSelect("SELECT * FROM "+t_name+" LIMIT 1");

    // surely if I have nothing back the schema is not correct !!!
    if ( ars == null ) return false;
    
    if ( ars.hasFailed() )
      {
      dbexec.println("isTableSchemaCorrect: ars.hasFailed() for table "+t_name+" exc="+ars.getException());
      return false;
      }

    ResultSetMetaData metadata = ars.getMetaData();
    if ( metadata == null ) 
      {
      // this should not happen anymore now
      dbexec.println("isTableSchemaCorrect: NULL metdatata for table "+t_name+" use CREATE");
      return false;
      }
    
    boolean finalResult = true; // assume that the final result is a good schema

    for (SchemaRow row : schema_rows )
      {
      // messages are already printed by the called method
      // I just need to remember that the final result is a fail
      finalResult &= row.isRowSchemaCorrect(dbexec, t_name, metadata); 
      }
    
    if ( finalResult )
      {
      // if final result is good, try to verify the table content
      finalResult &= verifyTableContent(dbexec);
      }
    
    return finalResult;
    }
 
  /**
   * Try to verify if the table content is correct and attempt to safely adjust it
   * @param dbexec 
   */
  protected boolean verifyTableContent ( ExecuteProvider dbexec )
    {
    return true;
    }
  
  /**
   * The default implementation uses the list of rows
   * If you want something else, just override the method
   * @param db
   */
  public boolean createTable ( ExecuteProvider db ) 
    {
    String createStr = getCreateTableStatement(db);
    return db.executeDbaseDone(createStr);
    }
  
  /**
   * This is a reasonable default implementation
   * @param dbexec
   */
  public void dropTable   ( ExecuteProvider dbexec ) 
    {
    String tbl = getTableName();
    
    if ( tbl != null )
      dbexec.executeDbaseDone("DROP TABLE "+tbl);
    }
  
  public void createView ( ExecuteProvider dbexec ) {}
  public void dropView   ( ExecuteProvider dbexec ) {}

  public void createConstraint ( ExecuteProvider dbexec ) {}
  public void dropConstraint   ( ExecuteProvider dbexec ) {}
  
  public void createIndex ( ExecuteProvider dbexec ) {}
  public void dropIndex   ( ExecuteProvider dbexec ) {}
  
  protected void addProvableHeader (String cn_id, String cn_fingerprint, String cn_creation, String cn_pow, String cn_signature )
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowVarbinary(cn_fingerprint,MimFingerprint.bytes_len)).setNotNullable();  
    addSchemaRow(new SchemaRowTimestamp(cn_creation));
    addSchemaRow(new SchemaRowVarchar(cn_pow,MimPowValue.pow_ascii_len));
    addSchemaRow(new SchemaRowVarbinary(cn_signature,MimSignature.signature_len));
    }
  
  protected void addUpdatableHeader ( String cn_update_last )
    {
    addSchemaRow(new SchemaRowTimestamp(cn_update_last));
    }

  @Override
  public String toString()
    {
    return classname+"tbl_name["+getTableName()+"]";      
    }
  }
  

/*

 @Override
  public void dropConstraint ( ExecuteProvider db )
    {
    db.executeDbase("ALTER TABLE ana_tbl    DROP CONSTRAINT ana_anatype_ref");
    db.executeDbase("ALTER TABLE ana_tbl    DROP CONSTRAINT ana_natosex_ref");
    db.executeDbase("ALTER TABLE ana_tbl    DROP CONSTRAINT ana_super_ref");
    db.executeDbase("ALTER TABLE ana_tbl    DROP CONSTRAINT ana_ganda_ref");
    db.executeDbase("ALTER TABLE ana_tbl    DROP CONSTRAINT ana_unique_login"); 
    }
    
  
  @Override
  public void createConstraint ( ExecuteProvider db )
    {
    db.executeDbase("ALTER TABLE ana_tbl ADD CONSTRAINT ana_anatype_ref FOREIGN KEY (ana_anatype_id ) REFERENCES anatype_tbl (anatype_id) ");
    db.executeDbase("ALTER TABLE ana_tbl ADD CONSTRAINT ana_natosex_ref FOREIGN KEY (ana_nato_sex_id ) REFERENCES sextype_tbl (sextype_id) ");
    db.executeDbase("ALTER TABLE ana_tbl ADD CONSTRAINT ana_super_ref   FOREIGN KEY (ana_super_ana_id ) REFERENCES ana_tbl (ana_id) ");
    db.executeDbase("ALTER TABLE ana_tbl ADD CONSTRAINT ana_ganda_ref   FOREIGN KEY (ana_ganda_id )  REFERENCES ganda_tbl (ganda_id) ");
    db.executeDbase("ALTER TABLE ana_tbl ADD CONSTRAINT ana_unique_login UNIQUE (ana_username)");
    }
    
    
  public void createView ( ExecuteProvider db )
    {
    db.executeDbase("CREATE VIEW "+Keyword.cat_search_view+" AS "+ 
    "SELECT cat_tbl.*,ana_nome,ana_cognome,catclass_val FROM cat_tbl "+
    "LEFT OUTER JOIN ana_tbl ON cat_madeby_ana_id = ana_id "+
    "LEFT OUTER JOIN catclass_tbl ON cat_catclass_id = catclass_id " );    
    }
    
    
    @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX adrs_loc_port_index ON "+cn_tbl+" ( "+
      cn_location+","+
      cn_port+
      " ) ";
    
    dbexec.executeDbaseDone(query);
    }

  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX adrs_loc_port_index");
    }  

    
  }             

 */



