/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase.dbschema;

import java.util.*;

import javax.swing.table.AbstractTableModel;

import it.aetherj.backend.Stat;


/**
 * This model is to be used with objects that are of type SchemaProvider.
 * It should handle the three cycle of select...
 */
public final class SchemaTableModel extends AbstractTableModel implements Iterable<SchemaProvider>
  {
  private static final long serialVersionUID = 1L;
  private static final String classname="SchemaTableModel.";
  
  private static final String [] columnName = { "Table","View","Constr","Index","Nome" };

  static final int COL_table=0;
  static final int COL_view=1;
  static final int COL_constr=2;
  static final int COL_index=3;
  static final int COL_kname=4;
  
  private final Stat stat;
  private final ArrayList<SchemaProvider> providers = new ArrayList<SchemaProvider>();
   
  /**
   * Constructor
   * @param stat 
   */
  SchemaTableModel ( Stat stat )
    {
    this.stat = stat;
    }

  /**
   * Adds a schema provider to the ones being managed.
   * Note: if a provider already exists with that name it is overwritten.
   * this is how we manage to "update" providers with new definitions.
   * Note: at each new entry the list is sorted, this is quite heavvy if you do it oftern...
   * @param anItem 
   */
  public void add ( SchemaProvider anItem )
    {
    if ( anItem == null ) return;
    
    String name = anItem.getTableModelKey();
    if ( name == null ) return;
    
    int curIndex = get ( name );
    if ( curIndex >= 0 )
      {
      // It is quite useful to spot errors to know when a definition is overwritten.
      stat.println(classname+"add() ERROR overwriting name="+name);
      providers.set(curIndex,anItem);
      }
    else
      providers.add(anItem);
    
    fireTableDataChanged();
    }


  /**
   * Returns the SchemaProvider index with the given name.
   * Note: this is quite slow operation, so try to use sparingly.
   * If nothing is found then -1 is returned.
   * @param wantTableName the name to match, case sensitive not null.
   * @return -1 if not found, a positive if found.
   */
  public int get ( String wantTableName )
    {
    int rowCount = getRowCount();
    for ( int index=0; index<rowCount; index++)
      {
      String haveTableName = get(index).getTableName();
      if ( wantTableName.equals(haveTableName)) return index;
      }
    
    return -1;
    }

  /**
   * Get the schema provider with the given index in the list.
   * @param index 
   * @return 
   */
  public SchemaProvider get ( int index )
    {
    return providers.get(index);
    }

  @Override
  public int getRowCount() 
    { 
    if ( providers == null ) return 0;
    
    return providers.size(); 
    }

  @Override
  public int getColumnCount() 
    { 
    return columnName.length;
    }

  @Override
  public String getColumnName(int col)  
    {
    return columnName[col];
    }

  /**
   * To be able to correctly display various column items you need this one being correct.
   */
  @Override
  public Class<?> getColumnClass(int col)
    {
    switch ( col )
      {
      case COL_table: 
      case COL_view: 
      case COL_constr:
      case COL_index: return Boolean.class;
      
      case COL_kname: return String.class;
      }
      
    stat.println(classname+"getColumnClass() Unknown col="+col);
    
    return Object.class;
    }
    
  /**
   * This is part of the Swing table interface. Given a row/col returns the object in it.
   * What I need to do is to return the REAL object and then have a special visualizer for each of them.
   */
  @Override
  public Object getValueAt(int row, int col)
    {
    SchemaProvider aRow = get(row);
    if (aRow == null)  return classname+"getValueAt: ERROR: no row at row=" + row + " col=" + col;

    if ( col == COL_table ) return aRow.createTable;
    
    if ( col == COL_view ) return aRow.createView;

    if ( col == COL_constr ) return aRow.createConstraint;

    if ( col == COL_index ) return aRow.createIndex;

    if ( col == COL_kname ) return aRow.getTableModelKey();
    
    return classname+"getValueAt: ERROR: at row=" + row + " col=" + col;
    }

  /**
   * You can call this one to toggle the value at the given row/column
   * If you hit the wrong column, nothing happens.
   */
  public void toggleValueAt(int row, int col)
    {
    SchemaProvider aRow = get(row);

    if ( col == COL_table ) 
      aRow.createTable = toggle ( aRow.createTable );
    else if ( col == COL_view )  
      aRow.createView = toggle ( aRow.createView );
    else if ( col == COL_constr ) 
      aRow.createConstraint = toggle ( aRow.createConstraint );
    else if ( col == COL_index )  
      aRow.createIndex = toggle ( aRow.createIndex );
    }

  /**
   * When you want to toggle a full column then you call this one
   */
  public void toggleColumn ( int col )
    {
    int rowCount = getRowCount();

    for (int row=0; row<rowCount; row++) 
      toggleValueAt(row,col);
    }

  /**
   * When you want to toggle a full row then you call this one
   */
  public void toggleRow ( int row )
    {
    toggleValueAt(row,SchemaTableModel.COL_table);
    toggleValueAt(row,SchemaTableModel.COL_view);
    toggleValueAt(row,SchemaTableModel.COL_constr);
    toggleValueAt(row,SchemaTableModel.COL_index);
    }

  /**
   * Tiny utility that toggles a Boolean from null->false->true->null
   * @param curValue 
   * @return 
   */
  private Boolean toggle ( Boolean curValue )
    {
    if ( curValue == null ) 
      return Boolean.FALSE;
    
    if ( curValue.booleanValue() == false) 
      return Boolean.TRUE;
    
    return null;
    }

  @Override
  public Iterator<SchemaProvider> iterator()
    {
    return providers.iterator();
    }
  }
