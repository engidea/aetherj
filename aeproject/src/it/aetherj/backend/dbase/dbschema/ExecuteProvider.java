/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase.dbschema;

import it.aetherj.boot.dbase.*;

/** 
 * Now that I have split the creation outside the manager I must have a way
 * to give a manager to the methods so they can call the appropriate functions.
 * The reason why I do not give the dbase directly is that I wish to control, preview what is happening
 */
public interface ExecuteProvider 
  {
  /**
   * this is what I give to the methods creating things.
   * @param command
   * @return true if the command execute with no error 
   */
  public boolean executeDbaseDone ( String command );
  
  /**
   * Implementer should provide a way to tell me how to handle dbase quirks 
   */
  public DbaseQuirks getDbaseQuirks ( );
  
  /**
   * This is used to get the metadata
   * @param select
   * @return
   */
  public Brs executeSelect ( String select );
  
  /**
   * An execute provider allows executer to give feedback
   * @param message
   */
  public void println(String message);
  
  /**
   * Append the given SQL instruction to the list of instruction to be executed 
   * to update the dbase.
   * @param sql_instruction
   */
  public void appendSqlInstruction ( String sql_instruction );
  }
