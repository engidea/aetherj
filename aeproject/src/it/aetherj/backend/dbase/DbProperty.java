/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.boot.dbase.*;

/**
 * Hold few definitions to manage the property table
 * Constructor may be called at any time, actual usage depends on having a dbase connection
 */
public class DbProperty
  {
  private static final String classname="DbProperty";
  
  private final Stat stat;
 
  // to manage system wide properties
  public static final String property_tbl   = "public.property_tbl";
  public static final String property_id    = "property_id";
  public static final String property_value = "property_value";

  public static final String property_dbase_version = "dbase_version";

 
  public DbProperty(Stat stat)
    {
    this.stat = stat;
    }
  
  private Brs getBrs ( AeDbase dbase, String key ) throws SQLException
    {
    String query = "SELECT * FROM "+property_tbl+" WHERE "+property_id+"=?";
    Dprepared prepared = dbase.getPreparedStatement(query);
    prepared.setString(1,key);
    return prepared.executeQuery();
    }
  
  public String getProperty (AeDbase dbase, String key, String def_value )
    {
    String risul = def_value;

    try
      {
      Brs brs = getBrs(dbase,key);
      
      if ( brs.next() ) 
        risul = brs.getString(property_value);
      
      brs.close();
      }
    catch ( Exception exc )
      {
      stat.log.println(classname+".getProperty",exc);
      }

    return risul;
    }

  public int getProperty (AeDbase dbase, String key, int def_value )
    {
    int risul = def_value;
    
    try
      {
      Brs brs = getBrs(dbase,key);

      if ( brs.next() ) 
        risul = brs.getInteger(property_value,def_value);
      
      brs.close();
      }
    catch ( Exception exc )
      {
      }

    return risul;
    }
  
  public boolean existProperty (AeDbase dbase, String key ) throws SQLException
    {
    boolean risul = false;

    Brs brs = getBrs(dbase,key);
    
    risul = brs.next();
    
    brs.close();
    
    return risul;
    }
  
  /**
   * Use this to write data into dbase, it will take care of doing UPDATE or INSERT as needed
   * @param dbase
   * @param key
   * @param value
   * @return
   * @throws SQLException
   */
  public int setProperty (AeDbase dbase, String key, String value ) throws SQLException
    {
    // skip null value
    if ( value == null ) return 0;
        
    if ( existProperty(dbase, key))
      return updateProperty ( dbase, key, value );
    else
      return insertProperty ( dbase, key, value );
    
    }

  public int setProperty (AeDbase dbase, String key, int value ) throws SQLException
    {
    return setProperty(dbase,key,Integer.toString(value));
    }
  
  
  private int insertProperty ( AeDbase dbase, String key, String value ) throws SQLException
    {
    String query = "INSERT INTO  "+property_tbl+" ( " +property_id+","+property_value+" ) VALUES ( ?,? ) ";
    
    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setString(insIndex++,key);
    prepared.setString(insIndex++,value);

    int risul=prepared.executeUpdate();
    
    prepared.close(); 

    return risul;
    }
  
  private int updateProperty ( AeDbase dbase, String key, String value ) throws SQLException
    {
    String query = "UPDATE  " + property_tbl + " SET " +property_value +"=? WHERE "+property_id+"=?";
    
    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setString(insIndex++,value);
    prepared.setString(insIndex++,key);

    int risul = prepared.executeUpdate();
    
    prepared.close();
    
    return risul;
    }

  
  }
