/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.server.services;

import java.sql.SQLException;
import java.util.*;

import it.aetherj.backend.dbase.Dbkey;
import it.aetherj.backend.dbtbls.*;
import it.aetherj.boot.dbase.*;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.services.RewebSystemService;
import it.aetherj.gwtwapp.server.RemoteServiceServletPublic;
import it.aetherj.protocol.MimFingerprint;

public final class SystemServiceImpl extends RemoteServiceServletPublic implements RewebSystemService
  {
	private static final long serialVersionUID = 1L;
	private static final String classname = "SystemServiceImpl.";

  public AegwPageHelpListRes getPageHelpList(AegwPageHelpListReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwPageHelpListRes risul = new AegwPageHelpListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
 
    try
      {
      String query = "SELECT * FROM "+AegwPageHelp.cn_tbl+" WHERE "+
          AegwPageHelp.cn_lang+"=? ORDER BY pagehelp_desc ASC";
      
      Dprepared prepared = env.dbase.getPreparedStatement(query);
      prepared.setString(1,""+env.getClientLocale());

      Brs ars = prepared.executeQuery();

      while ( ars.next() ) 
        risul.add(getOnePageHelpRecord ( ars ));
      
      prepared.close();
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("getPageHelp",exc);          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }

  private AegwPageHelp getOnePageHelpRecord (Brs ars ) throws SQLException
    {
    AegwPageHelp row = new AegwPageHelp();
     
    row.pagehelp_id       = ars.getInteger(AegwPageHelp.cn_id);
    row.pagehelp_lang     = ars.getString(AegwPageHelp.cn_lang);
    row.pagehelp_key      = ars.getString(AegwPageHelp.cn_key);
    row.pagehelp_filename = ars.getString(AegwPageHelp.cn_filename);
    row.pagehelp_desc     = ars.getString(AegwPageHelp.cn_desc);

    return row;
    }



  public AegwLabelSaveRes saveLabelList(AegwLabelSaveReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwLabelSaveRes risul = new AegwLabelSaveRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      for (int index=0; index<req.labelList.size(); index++)
        {
        String label_value = null;
        
        AegwLabel label = req.labelList.get(index);

        // if I am really saving the value then I need to actually get it
        if ( req.isSaveLabelValueRequest ) label_value = label.label_value;

        env.dbase.labelSave(env.getClientLocale(),label.label_english,label.label_from_page,label_value);
        }
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("saveLabelList",exc);          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }

  private String getAvailableLanguages (GwtServiceEnv env) throws SQLException
    {
    String query = "SELECT DISTINCT "+Dbkey.lblmap_lang+" FROM "+Dbkey.lblmap_tbl;
    StringBuilder risul = new StringBuilder(80);
    
    Dprepared prepared = env.dbase.getPreparedStatement(query);
    Brs ars = prepared.executeQuery();

    boolean insertComma = false;
    
    while ( ars.next() ) 
      {
      if ( insertComma ) risul.append(",");

      risul.append( ars.getString(Dbkey.lblmap_lang) );
      
      insertComma=true;
      }
    
    prepared.close();
    
    return risul.toString();
    }

  public AegwLabelListRes getLabelList(AegwLabelListReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwLabelListRes risul = new AegwLabelListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      String query = "SELECT * FROM mlang.label_tbl "+
        " LEFT OUTER JOIN (SELECT * from mlang.lblmap_tbl where lblmap_lang=? ) filtrata ON lblmap_label_id = label_id "+
        " INNER JOIN (SELECT * FROM mlang.lblsee_tbl WHERE lblsee_path=?) filtropath ON lblsee_label_id = label_id "+
        " ORDER BY label_english ";
      
      Dprepared prepared = env.dbase.getPreparedStatement(query);
      int insIndex=1;
      prepared.setString(insIndex++,""+env.getClientLocale());  // NOT possible to do wildcarding this way I catch it or it-IT and so on
      prepared.setString(insIndex++,req.lblsee_path);
      
      Brs ars = prepared.executeQuery();
      while ( ars.next() )
        {
        AegwLabel label = getDbaseLabel ( ars );
        labelAddUsedList ( env, label );
        risul.add( label );
        }
      prepared.close();
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("getLabelList",exc);          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }
  
  private AegwLabel getDbaseLabel (Brs ars )
    {
    AegwLabel risul      = new AegwLabel();
    risul.label_id        = ars.getInteger(Dbkey.label_id);
    risul.label_english   = ars.getString(Dbkey.label_english);
    risul.label_value     = ars.getString(Dbkey.lblmap_value);
    risul.label_last_day_use = ars.getTimestamp(Dbkey.label_last_use_day);
    return risul;
    }
  
  private void labelAddUsedList(GwtServiceEnv env, AegwLabel label )
    {
    StringBuilder risul = new StringBuilder(80);
    
    Brs ars = env.dbase.selectTableUsing(Dbkey.lblsee_tbl,Dbkey.lblsee_label_id,label.label_id);
    
    boolean addSeparator = false;
    while ( ars.next() )
      {
      if ( addSeparator ) risul.append("  ");
      risul.append(ars.getString(Dbkey.lblsee_path));
      addSeparator=true;
      }
    ars.close();
    
    label.label_used_in_pages=risul.toString();
    }

  public AegwComboRes getComboList(AegwComboReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwComboRes risul = new AegwComboRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      Brs ars = getComboBrs ( env, req );
      
      if ( req.combo_key_type == AegwComboReq.key_type_string )
        risul.stringRowList = getComboStringList(ars,req.key_col,req.value_col);
      
      if ( req.combo_key_type == AegwComboReq.key_type_integer )
        risul.integerRowList = getComboIntegerList(ars,req.key_col,req.value_col);

      ars.close();
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("getComboList",exc);          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }

  /**
   * @param env
   * @param req
   * @return
   * @throws SQLException
   */
  private Brs getComboBrs ( GwtServiceEnv env,AegwComboReq req ) throws SQLException
    {
    String query = getComboQuery(req);
      
    Dprepared prepared = env.dbase.getPreparedStatement(query);
    
    int insIndex=1;
    
    if ( req.select_key != null ) 
      {
      if ( req.combo_key_type == AegwComboReq.key_type_integer ) prepared.setInt(insIndex++,req.select_value_integer);
      if ( req.combo_key_type == AegwComboReq.key_type_string ) prepared.setString(insIndex++,req.select_value_string);
      }
      
    if ( req.select_and_key != null )
      {
      prepared.setInt(insIndex++,req.select_and_value);
      }
    
    return prepared.executeQuery();
    }


  private String getComboQuery ( AegwComboReq req )
    {
    switch (req.getComboSelector())
      {
      case AegwComboReq.combo_selector_first:
        req.combo_tbl  = "combo.regioni";
        req.key_col    = "ID_regione";
        req.value_col  = "nome";
        req.select_key = null;
        break;

      default:
        throw new IllegalArgumentException("SQL Injection detected, User backtraked, authority informed");
      }

    StringBuilder query = new StringBuilder();
    query.append("SELECT "+req.key_col );
    query.append(","+req.value_col );
    query.append(" FROM "+req.combo_tbl);
    if ( req.select_key != null ) query.append(" WHERE "+req.select_key+"=? ");
    if ( req.select_and_key != null ) query.append(" AND "+req.select_and_key+"=? ");
    query.append(" ORDER BY "+req.value_col);
    return query.toString();        
    }

  private ArrayList<AegwComboStringRow>getComboStringList(Brs ars, String key_col, String value_col)
    {
    ArrayList<AegwComboStringRow> risul = new ArrayList<AegwComboStringRow>();
    
    while ( ars.next() ) 
      {
      String key = ars.getString(key_col);
      String value = ars.getString(value_col);
      risul.add(new AegwComboStringRow(key,value));
      }
    
    ars.close();
    
    return risul;
    }

  private ArrayList<AegwComboIntegerRow>getComboIntegerList(Brs ars, String key_col, String value_col)
    {
    ArrayList<AegwComboIntegerRow> risul = new ArrayList<AegwComboIntegerRow>();
    
    while ( ars.next() ) 
      {
      Integer key = ars.getInteger(key_col);
      String value = "["+key+"] "+ars.getString(value_col);
      risul.add(new AegwComboIntegerRow(key,value));
      }
    
    ars.close();
    
    return risul;
    }

  
  @Override
  public AegwThreadListRes getThreadList ( AegwThreadListReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwThreadListRes risul = new AegwThreadListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
  
    if ( req.board_id <= 0 )
      {
      risul.setEnglishErrorMessage("Please select a Board");
      return risul;
      }
    
    try
      {
      AedbThread aedb=(AedbThread)env.getAedb(AedbThread.mimEntity);
      
      Brs ars = aedb.selectUsingBoard(env.dbase, req.board_id);
  
      while ( ars.next() )
        risul.add(GwtsThread.newRecordFromBrs(ars));
  
      ars.close();
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("getThreadList",exc);          
      }
  
    env.dispose();
    risul.markEnd();
    return risul;
    }
    
  @Override
  public AegwBoardListRes getBoardList ( AegwBoardListReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwBoardListRes risul = new AegwBoardListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
  
    try
      {
      AedbBoard aedb=(AedbBoard)env.getAedb(AedbBoard.mimEntity);
      
      Brs ars = aedb.selectAllValid(env.dbase);
  
      while ( ars.next() )
        risul.add(GwtsBoard.newRecordFromBrs(ars));
  
      ars.close();
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("getBoardList",exc);          
      }
  
    env.dispose();
    risul.markEnd();
    return risul;
    }
    
  @Override
  public AegwPostListRes getPostList ( AegwPostListReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwPostListRes risul = new AegwPostListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
  
    if ( req.thread_id <= 0 )
      {
      risul.setEnglishErrorMessage("Please select a Thread");
      return risul;
      }

    try
      {
      AedbPost aedb=(AedbPost)env.getAedb(AedbPost.mimEntity);
      
      Brs ars = aedb.selectUsingThreadId(env.dbase,req.thread_id);
  
      while ( ars.next() )
        risul.add(GwtsPost.newRecordFromBrs(ars));
  
      ars.close();
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("getPostList",exc);          
      }
  
    env.dispose();
    risul.markEnd();
    return risul;
    }
  
  private AegwUsub getOneWusubRecord ( Brs ars ) throws SQLException
    {
    AegwUsub row = new AegwUsub();

    row.wusub_id          = ars.getInteger(AedbWusub.cn_id);
    row.wusub_wuser_id    = ars.getInteger(AedbWusub.cn_wuser_id);
    row.wusub_board_id    = ars.getInteger(AedbWusub.cn_board_id);
    row.board_fingerprint = ars.getString(AedbBoard.cn_fingerprint);
    row.board_name        = ars.getString(AedbBoard.cn_name);
    row.wusub_notify      = ars.getBoolean(AedbWusub.cn_notify);
    row.wusub_last_seen   = ars.getTimestamp(AedbWusub.cn_last_seen);

    return row;
    }

  
  @Override
  public AegwUsubListRes  getWusubList ( AegwUsubListReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwUsubListRes risul = new AegwUsubListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
  
    try
      {
      AedbWusub aedb=(AedbWusub)env.getAedb(AedbWusub.mimEntity);
      
      Brs ars = aedb.selectUsingWuserId(env.dbase,env.getCurWuserId());
  
      while ( ars.next() )
        risul.add(getOneWusubRecord(ars));
  
      ars.close();
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("getWusubList",exc);          
      }
  
    env.dispose();
    risul.markEnd();
    return risul;
    }
  
  
  
  
  public AegwUsubListRes saveWusubList(AegwUsubSaveReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwUsubListRes risul = new AegwUsubListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      Integer wuser_id = env.getCurWuserId();
      
      AedbWusub aedb=(AedbWusub)env.getAedb(AedbWusub.mimEntity);

      for ( AegwUsub wusub : req.list )
        {
        // make sure that the user is correct
        wusub.wusub_wuser_id=wuser_id;
        // save the entry
        aedb.dbaseSave(env.dbase, wusub);
        }

      Brs ars = aedb.selectUsingWuserId(env.dbase,wuser_id);
      
      while ( ars.next() )
        risul.add(getOneWusubRecord(ars));
      
      ars.close();
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("saveWusubList",exc);          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }

  @Override
  public AegwThreadOprRes postThreadOpr(AegwThreadOprReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwThreadOprRes risul = new AegwThreadOprRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      GwtsThread gwts = new GwtsThread(env);
      
      if ( req.isPreparePow())
        gwts.dbaseSavePreparePow( req, risul);
      else if ( req.isUsingPow() )
        gwts.dbaseSaveUsingPow( req, risul);
      else
        gwts.dbaseGetDetails(req,risul);
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("postThreadOpr",exc);          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }

  @Override
  public AegwPostOprRes postPostOpr(AegwPostOprReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwPostOprRes risul = new AegwPostOprRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      GwtsPost gwts = new GwtsPost(env);
      
      if ( req.isPreparePow())
        gwts.dbaseSavePreparePow( req, risul);
      else if ( req.isUsingPow() )
        gwts.dbaseSaveUsingPow( req, risul);
      else
        gwts.dbaseGetDetails(req,risul);
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("postPostOpr",exc);          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }


  @Override
  public AegwVoteOprRes postVoteOpr(AegwVoteOprReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwVoteOprRes risul = new AegwVoteOprRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      GwtsVote gwtsOper = new GwtsVote(env);
      
      if ( req.isPreparePow())
        gwtsOper.dbaseSavePreparePow( req, risul);
      else if ( req.isUsingPow() )
        gwtsOper.dbaseSaveUsingPow( req, risul);
      else
        risul.setEnglishErrorMessage("Unsupported request="+req.req_code);
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("postVoteOpr",exc);          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }

  
  @Override
  public AegwRecentListRes getRecentList ( AegwRecentListReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwRecentListRes risul = new AegwRecentListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
  
    if ( req.want_limit > 200 )
      req.want_limit = 200;

    if ( req.want_date == null )
      req.want_date=new Date();
    
    try
      {
      AedbPost aedb=(AedbPost)env.getAedb(AedbPost.mimEntity);
      
      Brs ars = aedb.selectUsingTimeLimit(env.dbase,req.want_date, req.want_limit);
  
      while ( ars.next() )
        risul.add(GwtsPost.newRecordFromBrs(ars));
  
      ars.close();
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("getRecentList",exc);          
      }
  
    env.dispose();
    risul.markEnd();
    return risul;
    }

  @Override
  public AegwChatListRes getChatList ( AegwChatListReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwChatListRes risul = new AegwChatListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
      
    try
      {
      AedbChat aedb=(AedbChat)env.getAedb(AedbChat.mimEntity);
      
      Brs ars = aedb.selectUsingToUseridFromUserid(env.dbase, env.getCurWuserId(), req.from_user_id);
  
      while ( ars.next() )
        risul.add(GwtsChat.newRecordFromBrs(ars));
  
      ars.close();
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("getChatList",exc);          
      }
  
    env.dispose();
    risul.markEnd();
    return risul;
    }


  @Override
  public AegwChatOprRes postChatOpr(AegwChatOprReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwChatOprRes risul = new AegwChatOprRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      GwtsChat gwts = new GwtsChat(env);
      
      if ( req.isPreparePow())
        gwts.dbaseSavePreparePow( req, risul);
      else if ( req.isUsingPow() )
        gwts.dbaseSaveUsingPow( req, risul);
      else
        gwts.dbaseGetDetails(req,risul);
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("postChatOpr",exc);          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }

  @Override
  public AegwUkeyListRes getUkeyList ( AegwUkeyListReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwUkeyListRes risul = new AegwUkeyListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
      
    try
      {
      AedbUkey aedb=(AedbUkey)env.getAedb(AedbUkey.mimEntity);
      
      Brs ars = aedb.select(env.dbase);
  
      while ( ars.next() )
        risul.add(getUkeyBrief(ars));

      ars.close();
      }
    catch ( Exception exc )
      {
      risul.setEnglishExcption("getUkeyList",exc);          
      }
  
    env.dispose();
    risul.markEnd();
    return risul;
    }

  @Override
  public AegwUkeyOprRes postUkeyOpr(AegwUkeyOprReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwUkeyOprRes risul = new AegwUkeyOprRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      GwtsUkey gwts = new GwtsUkey(env);
      
      if ( req.isPreparePow())
        gwts.dbaseSavePreparePow( req, risul);
      else if ( req.isUsingPow() )
        gwts.dbaseSaveUsingPow( req, risul);
      else
        gwts.dbaseGetDetails(req,risul);
      }
    catch ( Exception exc )
      {
      String msg=it.aetherj.boot.Log.exceptionExpand("postUkeyOpr", exc);
      risul.setEnglishErrorMessage(msg);          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }
  
  
  /**
   * Return a Ukey with few info
   */
  private AegwUkey getUkeyBrief ( Brs ars )
    {
    AegwUkey risul = new AegwUkey();
    
    risul.ukey_id   = ars.getInteger(AedbUkey.cn_id, 0);
    risul.ukey_name = ars.getString(AedbUkey.cn_user_name);
    risul.ukey_fprint_hex = new MimFingerprint(ars.getBytes(AedbUkey.cn_fingerprint)).toString();
    
    return risul;
    }
  
  
  }
