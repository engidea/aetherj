package it.aetherj.gwtwapp.server.services;

import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.dbtbls.*;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.protocol.MimEntityList;
import it.aetherj.shared.Aedbg;
import it.aetherj.shared.v0.*;

/**
 * Even the board can be inserted and has a POW handling
 * 
 *
 */
public class GwtsBoard extends GwtsMim
  {
  private static final String classname="GwtsBoard";
  
  public GwtsBoard(GwtServiceEnv env)
    {
    super(env, MimEntityList.aeboard);
    }

  /**
   * This is dbase save, but I must prepare the Pow to be sent back to the user
   * This will save the "object" in the temporary table amd return to client the POW to be calculated
   */
  public void dbaseSavePreparePow (  AegwBoardOprReq req, AegwBoardOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    AegwBoard row = req.aeboard;
    
    if ( row.board_id != CommonReq.NULL_int )
      dbaseUpdatePreparePow (risul, row  );
    else
      dbaseInsertPreparePow ( risul, row );
    }
    

  public void dbaseUpdatePreparePow ( AegwBoardOprRes risul, AegwBoard row ) throws SQLException, JsonProcessingException, CloneNotSupportedException
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);
    
    // now, I have to get current mim thread content
    MimBoard mboard = aedbBoard.getMimMesage(dbase, row.board_id);
    
    if ( ! myself.isFingerprintEqual(mboard.owner) )
      throw new IllegalArgumentException("Update can only occour on same owner fingerprint");
    
    // At the moment nothing can be changed...
    
    // need to mark now as the update time, this will be signed
    mboard.last_update = new MimTimestamp();
     
    // possibly first since it zaps pow
    mboard.update_signature=mboard.updateSignature(crypto, myself.ukey_private);

    risul.jsonForPow = mboard.calcUpdatePowSource();
    
    String t_s = aejson.writeValueAsString(mboard);
    
    risul.wpow_id = aedbWpow.dbaseInsert(dbase, wuser_id, entity, row.board_id, t_s);
    
    println(classname+".dbaseUpdatePreparePow: wpow_id="+risul.wpow_id);
    }
  
  private void dbaseInsertPreparePow ( AegwBoardOprRes risul, AegwBoard row ) throws SQLException, JsonProcessingException, CloneNotSupportedException 
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);
     
    MimBoard mthread = new MimBoard();
     
    mthread.owner = myself.ukey_fingerprint;
    mthread.owner_publickey = myself.ukey_public;
     
    mthread.name = row.board_name;

    // Signature has to be first
    mthread.signature=mthread.newSignature(crypto, myself.ukey_private);

    risul.jsonForPow = mthread.calcInsertPowSource();
    
    String t_s = aejson.writeValueAsString(mthread);
    
    risul.wpow_id = aedbWpow.dbaseInsert(dbase, wuser_id, entity, row.board_id, t_s);
    
    println(classname+".dbaseInsertPreparePow: wpow_id="+risul.wpow_id);
    }
  
  public void dbaseSaveUsingPow ( AegwBoardOprReq req, AegwBoardOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);

    AedbWpow.WpowRecord t_s = aedbWpow.getJsons(dbase, wuser_id, req.wpow_id);
    MimBoard mthread = aejson.readValue(t_s.json_string, MimBoard.class);
    
    if ( mthread.fingerprint.isInvalid() )
      {
      // POW second
      mthread.proof_of_work = calcPowSigned(req.mimPowValue, myself.ukey_private);

      // Since Fingerprint includes the signature and the POW
      mthread.fingerprint= mthread.calcFingerprint();

      if ( shouldPrint(Aedbg.M_dbase, Aedbg.L_debug))
        println(classname+".dbaseInsertUsingPow: fprint="+mthread.fingerprint);

      Integer row_id=aedbBoard.dbaseInsert(dbase, mthread);
      risul.system_feedback_msg="inserted row_id="+row_id;
      }
    else
      { 
      // POW second
      mthread.update_proof_of_work = calcPowSigned(req.mimPowValue, myself.ukey_private);

      // I retrieve the vote to be adjusted from the wpow
      Integer row_id = t_s.entity_id;
      row_id = aedbBoard.dbaseUpdate(dbase, row_id, mthread);
      risul.system_feedback_msg="updated row_id="+row_id;
      }
    }

  
  public void dbaseGetDetails (  AegwBoardOprReq req, AegwBoardOprRes risul ) throws SQLException, JsonProcessingException, CloneNotSupportedException 
    {
    Brs ars = aedbThread.selectUsingPrimaryId(dbase, req.aeboard.board_id);
    
    if ( ars.next())
      risul.aeboard = newRecordFromBrs(ars);
    
    ars.close();
    }
  
  public static AegwBoard newRecordFromBrs ( Brs ars )
    {
    AegwBoard row = new AegwBoard();

    row.board_id         = ars.getInteger(AedbBoard.cn_id);
    row.board_name       = ars.getString(AedbBoard.cn_name);
    row.board_owner_name = ars.getString(AedbUkey.cn_user_name);
    row.board_desc       = ars.getString(AedbBoard.cn_desc);

    row.board_treads_cnt = ars.getInteger(AedbBoard.cn_thread_cnt,0);

    return row;
    }

  
  }
