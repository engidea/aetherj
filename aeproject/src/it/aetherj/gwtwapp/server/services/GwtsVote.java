package it.aetherj.gwtwapp.server.services;

import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.dbtbls.*;
import it.aetherj.boot.dbase.*;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.protocol.MimEntityList;
import it.aetherj.shared.Aedbg;
import it.aetherj.shared.v0.*;

/**
 * This class is created on the fly when needed, so, you can have some instance variables bound to a specific user
 * @author damiano
 *
 */
public class GwtsVote extends GwtsMim
  {
  private static final String classname="GwtsVote";
  
  private final AedbVote aedbVote;
  
  private Integer my_ukey_id;
  private int     want_vote_type;
  
  public GwtsVote(GwtServiceEnv env)
    {
    super(env, MimEntityList.aevote);
    
    aedbVote = (AedbVote)env.getAedb(AedbVote.mimEntity);
    }

  
  private int findVote ( AegwVote i_vote ) throws SQLException
    {
    StringBuilder query=new StringBuilder();
    query.append("SELECT * FROM "+AedbVote.cn_tbl+" WHERE ");
    query.append(AedbVote.cn_type_class+"="+MimVote.typclass_discussion);
    query.append(" AND "+AedbVote.cn_owner_id+"=? ");
    
    if ( i_vote.vote_post_id == CommonReq.NULL_int )
      query.append(" AND "+AedbVote.cn_thread_id+"=? ");
    else
      query.append(" AND "+AedbVote.cn_post_id+"=? ");

    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++, my_ukey_id);

    if ( i_vote.vote_post_id == CommonReq.NULL_int )
      prepared.setInt(insIndex++, i_vote.vote_thread_id);
    else
      prepared.setInt(insIndex++, i_vote.vote_post_id);
    
    Brs ars = prepared.executeQuery();

    int vote_id=CommonReq.NULL_int;

    if ( ars.next() )
      vote_id=ars.getInteger(AedbVote.cn_id, CommonReq.NULL_int);
    
    prepared.close();
    
    return vote_id;
    }
  
  private Integer getUkeyId ( ) throws SQLException
    {
    Brs ars = aedbWuser.selectUsingPrimaryId(dbase, wuser_id);
    
    Integer risul = null;
    
    if ( ars.next() )
      risul = ars.getInteger(AedbWebuser.cn_ukey_id);
    
    ars.close();
    
    return risul;
    }
  
  /**
   * This is dbase save, but I must prepare the Pow to be sent back to the user
   * This will save the "object" in the temporary table and return to client the POW to be calculated
   * NOTE: The web client does NOT have a Vote id, I need to find out by myself if I have a vote to "adjust"
   */
  public void dbaseSavePreparePow (  AegwVoteOprReq req, AegwVoteOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    my_ukey_id = getUkeyId();
    
    if ( my_ukey_id == null )
      throw new IllegalArgumentException("No ukey_id for current wuser_id="+wuser_id);
    
    want_vote_type = req.aevote.vote_up ? MimVote.type_upvote : MimVote.type_downvote;
    
    AegwVote row = req.aevote;
    
    row.vote_id = findVote (row);
    
    if ( row.vote_id != CommonReq.NULL_int )
      dbaseUpdatePreparePow (risul, row);
    else
      dbaseInsertPreparePow ( risul, row);
    }
    

  private void dbaseUpdatePreparePow ( AegwVoteOprRes risul, AegwVote row ) throws SQLException, JsonProcessingException, CloneNotSupportedException
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);
    
    // now, I have to get current mim thread content
    MimVote mvote = aedbVote.getMimMesage(dbase, row.vote_id);
    
    if ( ! myself.isFingerprintEqual(mvote.owner) )
      throw new IllegalArgumentException("Update can only occour on same owner fingerprint");
    
    // Can change only this, I do not care if it is already the same type
    mvote.type = want_vote_type;
    
    // need to mark now as the update time, this will be signed
    mvote.last_update = new MimTimestamp();
    
    // possibly first since it zaps pow
    mvote.updateSignature(crypto, myself.ukey_private);

    risul.jsonForPow = mvote.calcUpdatePowSource();
    
    String t_s = aejson.writeValueAsString(mvote);
    
    risul.wpow_id = aedbWpow.dbaseInsert(dbase, wuser_id, entity, row.vote_id, t_s);
    
    println(classname+".dbaseUpdatePreparePow: wpow_id="+risul.wpow_id);
    }
  
  private void dbaseInsertPreparePow ( AegwVoteOprRes risul, AegwVote row ) throws SQLException, JsonProcessingException, CloneNotSupportedException 
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);
    MimBoard board   = aedbBoard.getMimMesage(dbase, row.vote_board_id);
    MimThread thread = aedbThread.getMimMesage(dbase, row.vote_thread_id);
    MimPost post     = aedbPost.getMimMesage(dbase, row.vote_post_id);
     
    MimVote mvote = new MimVote();
     
    mvote.board = board.fingerprint;

    mvote.owner = myself.ukey_fingerprint;
    mvote.owner_publickey = myself.ukey_public;
     
    mvote.thread = thread.fingerprint;
    
    if ( post.isNull() )
      mvote.target = thread.fingerprint;
    else
      mvote.target = post.fingerprint;
    
    mvote.typeclass = MimVote.typclass_discussion;
    mvote.type      = want_vote_type;
    
    // Signature has to be first
    mvote.firstSignature(crypto, myself.ukey_private);

    risul.jsonForPow = mvote.calcInsertPowSource();
    
    String t_s = aejson.writeValueAsString(mvote);
    
    risul.wpow_id = aedbWpow.dbaseInsert(dbase, wuser_id, entity, null, t_s);
    
    println(classname+".dbaseInsertPreparePow: wpow_id="+risul.wpow_id);
    }
  
  public void dbaseSaveUsingPow ( AegwVoteOprReq req, AegwVoteOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);

    AedbWpow.WpowRecord t_s = aedbWpow.getJsons(dbase, wuser_id, req.wpow_id);
    MimVote mimrow = aejson.readValue(t_s.json_string, MimVote.class);
    
    println(classname+".dbaseSaveUsingPow: wpow_id="+req.wpow_id);

    Integer row_id;
    
    if ( mimrow.fingerprint.isInvalid() )
      {
      // POW second
      mimrow.proof_of_work = calcPowSigned(req.mimPowValue, myself.ukey_private);

      // Since Fingerprint includes the signature and the POW
      mimrow.fingerprint= mimrow.calcFingerprint();

      if ( shouldPrint(Aedbg.M_dbase, Aedbg.L_debug))
        println(classname+".dbaseInsertUsingPow: fprint="+mimrow.fingerprint);

      row_id=aedbVote.dbaseInsert(dbase, mimrow);
      risul.system_feedback_msg="inserted row_id="+row_id;
      }
    else
      {
      // POW second
      mimrow.update_proof_of_work = calcPowSigned(req.mimPowValue, myself.ukey_private);

      // I retrieve the vote to be adjusted from the wpow
      Integer vote_id = t_s.entity_id;
      row_id = aedbVote.dbaseUpdate(dbase, vote_id, mimrow);
      risul.system_feedback_msg="updated row_id="+row_id;
      } 
    
    env.recalcVotes(dbase, row_id);
    }
  }
