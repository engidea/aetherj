package it.aetherj.gwtwapp.server.services;

import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbtbls.*;
import it.aetherj.backend.gui.config.AetherParams;
import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;
import it.aetherj.shared.AeJson;
import it.aetherj.shared.v0.MimPrivateKey;

/**
 * I just wish to wrap in here all peculiarities of inserting/updating objects when coming from web
 */
public class GwtsBim
  {
  protected final MimEntity entity;
  
  protected final GwtServiceEnv env;
  
  protected final AeDbase dbase;
  
  protected final Integer wuser_id;
  
  protected final AedbUkey     aedbUkey;
  protected final AedbWebuser  aedbWuser;
  protected final AedbBoard    aedbBoard; 
  protected final AedbThread   aedbThread;
  protected final AedbPost     aedbPost;

  protected final AedbWpow     aedbWpow;
  protected final AetherParams aetherParams;
  
  protected final MimPowParams powParams = AedbMimOperations.powParams;
  protected final CryptoEddsa25519 crypto = new CryptoEddsa25519();
  protected final AeJson aejson = new AeJson();
  
  GwtsBim (GwtServiceEnv env, MimEntity entiity )
    {
    this.env=env;
    this.entity=entiity;
    
    dbase = env.dbase;
    aetherParams = env.getAetherParams();
    
    wuser_id = env.getCurWuserId();
    
    aedbUkey=(AedbUkey)env.getAedb(AedbUkey.mimEntity);
    aedbWuser=(AedbWebuser)env.getAedb(AedbWebuser.mimEntity);
    aedbBoard=(AedbBoard)env.getAedb(AedbBoard.mimEntity);
    aedbThread =(AedbThread)env.getAedb(AedbThread.mimEntity);
    aedbPost =(AedbPost)env.getAedb(AedbPost.mimEntity);

    aedbWpow=(AedbWpow)env.getAedb(AedbWpow.mimEntity);
    
    }
  
  protected MimPowValue calcPowSigned ( MimPowValue powUnsigned, MimPrivateKey privateKey) 
    {
    MimSignature signature = crypto.mimSign(privateKey, powUnsigned.toString());
    
    return powUnsigned.getSigned(signature);
    }

  
  protected void println(String message)
    {
    env.println(message);
    }
  
  protected boolean shouldPrint ( int mask, int level )
    {
    return env.shouldPrint(mask, level);
    }
  
  
  
  }
