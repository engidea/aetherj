package it.aetherj.gwtwapp.server.services;

import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.dbtbls.*;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.protocol.MimEntityList;
import it.aetherj.shared.Aedbg;
import it.aetherj.shared.v0.*;

public class GwtsPost extends GwtsMim
  {
  private static final String classname="GwtsPost";
  

  public GwtsPost(GwtServiceEnv env)
    {
    super(env, MimEntityList.aepost);
    }

  /**
   * This is dbase save, but I must prepare the Pow to be sent back to the user
   * This will save the "object" in the temporary table and return to client the POW to be calculated
   * @param risul
   * @param req
   * @throws CloneNotSupportedException 
   * @throws SQLException 
   * @throws JsonProcessingException 
   */
  public void dbaseSavePreparePow (  AegwPostOprReq req, AegwPostOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    AegwPost row = req.aepost;
    
    if ( row.post_id != CommonReq.NULL_int )
      dbaseUpdatePreparePow (risul, row  );
    else
      dbaseInsertPreparePow ( risul, row );
    }
    

  private void dbaseUpdatePreparePow ( AegwPostOprRes risul, AegwPost row ) throws SQLException, JsonProcessingException, CloneNotSupportedException
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);
    
    // now, I have to get current mim thread content
    MimPost mpost = aedbPost.getMimMesage(dbase, row.post_id);
    
    if ( ! myself.isFingerprintEqual(mpost.owner) )
      throw new IllegalArgumentException("Update can only occour on same owner fingerprint");
    
    // This is the only thing that can be changed
    mpost.body = row.post_body;
    
    // need to mark now as the update time, this will be signed
    mpost.last_update = new MimTimestamp();
    
    // possibly first since it zaps pow
    mpost.update_signature=mpost.updateSignature(crypto, myself.ukey_private);

    risul.jsonForPow = mpost.calcUpdatePowSource();
    
    String t_s = aejson.writeValueAsString(mpost);
    
    risul.wpow_id = aedbWpow.dbaseInsert(dbase, wuser_id, entity, row.post_id, t_s);
    
    println(classname+".dbaseUpdatePreparePow: wpow_id="+risul.wpow_id);
    }
  
  private void dbaseInsertPreparePow ( AegwPostOprRes risul, AegwPost row ) throws SQLException, JsonProcessingException, CloneNotSupportedException 
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);
    MimBoard  board  = aedbBoard.getMimMesage(dbase, row.post_board_id);
    MimThread thread = aedbThread.getMimMesage(dbase, row.post_thread_id);

    if ( board.isNull() )
      throw new IllegalArgumentException(classname+".dbaseInsertPreparePow: board is null id="+row.post_board_id);

    if ( thread.isNull() )
      throw new IllegalArgumentException(classname+".dbaseInsertPreparePow: thread is null id="+row.post_thread_id);
     
    MimPost mpost = new MimPost();

    mpost.owner = myself.ukey_fingerprint;
    mpost.owner_publickey = myself.ukey_public;

    mpost.board = board.fingerprint;
    mpost.thread = thread.fingerprint;

    if ( row.post_parent_id != null )
      {
      MimPost ppost = aedbPost.getMimMesage(dbase, row.post_parent_id);
      // this declares this as the parent glued
      mpost.parent=ppost.fingerprint;
      }
    else
      {
      // Current Aether logic is to put the finger print of the Thread here...
      // A pity that this breaks referential integrity
      mpost.parent = thread.fingerprint;
      }
     
    mpost.body = row.post_body;

    // Signature has to be first
    mpost.signature=mpost.newSignature(crypto, myself.ukey_private);

    risul.jsonForPow = mpost.calcInsertPowSource();
    
    String t_s = aejson.writeValueAsString(mpost);
    
    risul.wpow_id = aedbWpow.dbaseInsert(dbase, wuser_id, entity, row.post_id, t_s);
    
    println(classname+".dbaseInsertPreparePow: wpow_id="+risul.wpow_id);
    }
  
  public void dbaseSaveUsingPow ( AegwPostOprReq req, AegwPostOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);

    AedbWpow.WpowRecord t_s = aedbWpow.getJsons(dbase, wuser_id, req.wpow_id);
    MimPost mpost = aejson.readValue(t_s.json_string, MimPost.class);
    
    if ( mpost.fingerprint.isInvalid() )
      {
      // POW second
      mpost.proof_of_work = calcPowSigned(req.mimPowValue, myself.ukey_private);

      // Since Fingerprint includes the signature and the POW
      mpost.fingerprint= mpost.calcFingerprint();

      if ( shouldPrint(Aedbg.M_dbase, Aedbg.L_debug))
        println(classname+".dbaseInsertUsingPow: fprint="+mpost.fingerprint);

      Integer row_id=aedbPost.dbaseInsert(dbase, mpost);
      risul.system_feedback_msg="inserted row_id="+row_id;
      }
    else
      {
      // POW second
      mpost.update_proof_of_work = calcPowSigned(req.mimPowValue, myself.ukey_private);

      // I retrieve the vote to be adjusted from the wpow
      Integer post_id = t_s.entity_id;
      Integer row_id = aedbPost.dbaseUpdate(dbase, post_id, mpost);
      risul.system_feedback_msg="updated row_id="+row_id;
      }
    }
  
  public void dbaseGetDetails (  AegwPostOprReq req, AegwPostOprRes risul ) throws SQLException, JsonProcessingException, CloneNotSupportedException 
    {
    Brs ars = aedbPost.selectUsingPrimaryId(dbase, req.aepost.post_id);
    
    if ( ars.next())
      risul.aepost = newRecordFromBrs(ars);
    
    ars.close();
    
    ars = aedbThread.selectUsingPrimaryId(dbase, req.aepost.post_thread_id);
    
    if ( ars.next())
      risul.aethread = GwtsThread.newRecordFromBrs(ars);
    
    ars.close();
    
    ars = aedbBoard.selectUsingPrimaryId(dbase, req.aepost.post_board_id);
    
    if ( ars.next())
      risul.aeboard = GwtsBoard.newRecordFromBrs(ars);
    
    ars.close();
    }
   
  public static AegwPost newRecordFromBrs ( Brs ars )
    {
    AegwPost row = new AegwPost();

    row.post_id         = ars.getInteger(AedbPost.cn_id,0);
    row.post_board_id   = ars.getInteger(AedbPost.cn_board_id,0);
    row.post_thread_id  = ars.getInteger(AedbPost.cn_thread_id,0);
    row.post_parent_id  = ars.getInteger(AedbPost.cn_parent_id);
    row.post_body       = ars.getString(AedbPost.cn_body);
    
    row.post_votes_cnt  = ars.getInteger(AedbPost.cn_votes_cnt,0); 
    row.post_owner_name = ars.getString(AedbUkey.cn_user_name);

    return row;
    } 

  
  }
