/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.server.services;

import java.sql.SQLException;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.*;

import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbtbls.*;
import it.aetherj.backend.gui.config.AetherParams;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.server.RemoteServiceServletPublic;
import it.aetherj.protocol.MimEntity;
import it.aetherj.shared.*;
 

/**
 * Servlets classes are not newed at each request
 * So, each method should have its own environment be passed around to work !
 * You can allocate a new DB connection here and pick up things that are interesting
 * It is a contract guarantee that his class is new'ed when a new request comes in, so you can safely store
 * status in this one since it will be lod when the request completes.
 */
public final class GwtServiceEnv implements PrintlnProvider
  {
  private static final String classname = "GwtServiceEnv.";
  private static final String webSessionKey = "WebSessionKey";

  private final ServletContext servletContext;
  private final HttpServletRequest servletRequest;
  private final Locale clientLocale;
  private final WebStat wstat;
  private final String remoteAddr;
  private final AedbFactory aedbFactory;
  
	public  final ServletsUtils servletUtils;  // needed for most use, including label mappings

	private ServiceSession serviceSession;     // picked up at service authentication, may be null
  private String ident;         // used to write something if db is not closed on finalized.
  public  AeDbase  dbase;       // I need to be able to NULL it to detach from env.

  /**
   * This MUST be created every time a METHOD is called on a RemoteServiceServletPublic
   * @param serviceServlet
   */
  public GwtServiceEnv(RemoteServiceServletPublic serviceServlet)
    {
    this.servletContext = serviceServlet.getServletContext();
    this.servletRequest = serviceServlet.getServletRequest();
    this.remoteAddr     = servletRequest.getRemoteAddr();
    this.clientLocale   = servletRequest.getLocale();
		this.servletUtils   = new ServletsUtils();

    this.wstat = (WebStat) servletContext.getAttribute(WebContextListener.KEY_WebStat);

    this.aedbFactory = wstat.aedbFactory;
    
    // NOTE: it is duty of the servlet to release DB connection once DONE
    dbase = (AeDbase)wstat.connpool.getDbase();
    }


  /**
   * This MUST be created every time a METHOD is called on a HttpServlet
   * @param serviceServlet
   */
  public GwtServiceEnv(HttpServlet serviceServlet, HttpServletRequest servletRequest)
    {
    this.servletContext = serviceServlet.getServletContext();
    this.servletRequest = servletRequest;
    this.remoteAddr     = servletRequest.getRemoteAddr();
    this.clientLocale   = servletRequest.getLocale();
    this.servletUtils   = new ServletsUtils();
    
    this.wstat = (WebStat) servletContext.getAttribute(WebContextListener.KEY_WebStat);

    this.aedbFactory = wstat.aedbFactory;
    
    // NOTE: it is duty of the servlet to release DB connection once DONE
    dbase = (AeDbase)wstat.connpool.getDbase();
    }
  
  public AetherParams getAetherParams ()
    {
    return wstat.aetherParams;
    }
  
  public boolean shouldPrint ( int mask, int level )
    {
    return wstat.dbg.shouldPrint(mask,level);
    }

  public void recalcVotes ( AeDbase db, Integer vote_id ) throws SQLException
    {
    wstat.mcwAddressModel.recalcVotes(db, vote_id);
    }

  
  public void setWebSession(WebSession webSession)
    {
    if ( webSession == null ) return;
    
    HttpSession htses = servletRequest.getSession(true);
    
    if ( htses == null )
      {
      println(classname+"setWebSession: servletRequest.getSession(true) FAILED");
      return;
      }
    
    // create a new service to persist some values
    serviceSession = new ServiceSession(webSession);
    
    // store it into the servlet attribute
    htses.setAttribute(webSessionKey,serviceSession);
    }

  public ServiceSession getServiceSession ()
    {
    return serviceSession;
    }
  
	public String getRemoteAddress ()
		{
		return remoteAddr;			
		}

	/**
	 * Return a translated English label into what is requested by the user locale
	 * @param from_page
	 * @param label_english
	 * @return
	 */
	public String getLabel ( String from_page, String label_english )
		{
		if ( wstat.labelFactory == null ) return label_english;
		
		Locale locale = getClientLocale();

		String want_lang = locale.toString();
		
		return wstat.labelFactory.getLabel(from_page, label_english, want_lang);
		}

	 public Integer isGoodUsernamePassword(String username, String password)
	   {
  	 return aedbFactory.isGoodUsernamePassword(dbase, username, password);
  	 }
	 
	 public AedbOperations getAedb ( MimEntity entity )
  	 {
  	 return aedbFactory.getAedb(entity);
  	 }
  
	public GwtServiceEnv setIdent ( String ident )
    {
    this.ident = ident;
    return this;
    }
  
  public ApplicationVersion getAppVersion ()
    {
    return wstat.appVersion;    
    }
  
	public void logEvent ( Integer wana_id, String logall_service, String logall_dboper, String event_desc)
		{
    if ( dbase == null )
      {
      println(classname+"logEvent: ERROR dbase==null");
      return;
      }
    
    if ( ! dbase.isConnected() ) 
      {
      println(classname+"logEvent: ERROR dbase NOT connected");
      return;
      }

		try
      {
      dbase.dbaseLogInsert(wana_id,logall_service,logall_dboper,event_desc);        
      }
    catch ( Exception lexc )
      {
      println(classname+"logEvent ",lexc);
      }
		}
	  
  public void logError ( String desc, Exception exc)
    {
    if ( dbase == null )
      {
      println(classname+"logError: ERROR dbase==null");
      return;
      }
    
    if ( ! dbase.isConnected() ) 
      {
      println(classname+"logError: ERROR dbase NOT connected");
      return;
      }

    try
      {
      dbase.dbaseLogInsert(1,"change",desc,exc.toString());        
      }
    catch ( Exception lexc )
      {
      println(classname+"logError ",lexc);
      }
    }
		
	@Override
  public void println ( String message )
    {
    wstat.println(message);    
    }
  
	@Override
  public void println ( String message, Throwable exc )
    {
    wstat.println(message,exc);    
    }

  public Locale getClientLocale ()
    {
    if ( clientLocale == null ) return Locale.ENGLISH;
    
    return clientLocale;
    }
  
  /**
   * WARNING: You MUST call this one once you have finished using processing the method to release
   * the dbase resources or possibly something else.
   */
  public void dispose ()
    {
    // give the dbase back to the conn pool
    wstat.connpool.releaseDbase(dbase);

    // make sure nobody will use it again
    dbase=null;
    }
  
  
  /**
   * You MUST call dispose but if you ever forget maybe this will try to recover.
   */
  public void finalize () throws Throwable
    {
    if ( dbase == null ) return;
    
    if ( dbase.isClosed() ) return;
    
    println(classname+"finalize() remote="+remoteAddr+" ident="+ident+" NOTICE closing Dbase");

    dispose();
    }
  
  public Integer getCurWuserId ()
    {
    if ( serviceSession == null ) return null;
    
    return serviceSession.getCurWuserId();
    }
  
  /**
   * As an extra safety check I have the client give me the WebSession, BUT
   * when a servlet is involved then this parameter is not available !
   * So, having a null input is actually correct for servlets
   * @param input
   * @return
   */
  public boolean isWsessionValid(WebSession input)
    {
    HttpSession htses = servletRequest.getSession();
    if ( htses.isNew() ) 
      {
      println(classname+"isSessionValid: remote="+remoteAddr+" HttpSession isNew -> no valid Wsession");
      return false;
      }
    
    serviceSession = (ServiceSession)htses.getAttribute(webSessionKey);
    if ( serviceSession == null )
      {
      println(classname+"isSessionValid: remote="+remoteAddr+" HttpSession isNew -> no valid Wsession");
      return false;
      }

    if ( input == null ) 
      {
      // if no input session is given then we assume that what we have is correct
      return true;
      }

    // session is then valid if the two match
    return input.equals(serviceSession.webSession);
    }  
  
  }

