package it.aetherj.gwtwapp.server.services;

import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.dbtbls.*;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.protocol.MimEntityList;
import it.aetherj.shared.Aedbg;
import it.aetherj.shared.v0.*;

public class GwtsThread extends GwtsMim
  {
  private static final String classname="GwtsThread";
  
  public GwtsThread(GwtServiceEnv env)
    {
    super(env, MimEntityList.aethread);
    }

  /**
   * This is dbase save, but I must prepare the Pow to be sent back to the user
   * This will save the "object" in the temporary table amd return to client the POW to be calculated
   */
  public void dbaseSavePreparePow (  AegwThreadOprReq req, AegwThreadOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    AegwThread row = req.aethread;
    
    if ( row.thread_id != CommonReq.NULL_int )
      dbaseUpdatePreparePow (risul, row  );
    else
      dbaseInsertPreparePow ( risul, row );
    }
    

  private void dbaseUpdatePreparePow ( AegwThreadOprRes risul, AegwThread row ) throws SQLException, JsonProcessingException, CloneNotSupportedException
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);
    
    // now, I have to get current mim thread content
    MimThread mthread = aedbThread.getMimMesage(dbase, row.thread_id);
    
    if ( ! myself.isFingerprintEqual(mthread.owner) )
      throw new IllegalArgumentException("Update can only occour on same owner fingerprint");
    
    // You cannot change the Thread name since it is included in the fingerprint
    mthread.link = row.thread_link;
    mthread.body = row.thread_body;
    
    // need to mark now as the update time, this will be signed
    mthread.last_update = new MimTimestamp();
    
    // possibly first since it zaps pow
    mthread.update_signature=mthread.updateSignature(crypto, myself.ukey_private);

    risul.jsonForPow = mthread.calcUpdatePowSource();
    
    String t_s = aejson.writeValueAsString(mthread);
    
    risul.wpow_id = aedbWpow.dbaseInsert(dbase, wuser_id, entity, row.thread_id, t_s);
    
    println(classname+".dbaseUpdatePreparePow: wpow_id="+risul.wpow_id);
    }
  
  private void dbaseInsertPreparePow ( AegwThreadOprRes risul, AegwThread row ) throws SQLException, JsonProcessingException, CloneNotSupportedException 
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);
    MimBoard board = aedbBoard.getMimMesage(dbase, row.thread_board_id);
    
    if ( board.isNull() )
      throw new IllegalArgumentException(classname+".dbaseInsertPreparePow: board is null id="+row.thread_board_id);
     
    MimThread mthread = new MimThread();
     
    mthread.board = board.fingerprint;

    mthread.owner = myself.ukey_fingerprint;
    mthread.owner_publickey = myself.ukey_public;
     
    mthread.name = row.thread_name;
    mthread.link = row.thread_link;
    mthread.body = row.thread_body;

    // Signature has to be first
    mthread.signature=mthread.newSignature(crypto, myself.ukey_private);

    risul.jsonForPow = mthread.calcInsertPowSource();
    
    String t_s = aejson.writeValueAsString(mthread);
    
    risul.wpow_id = aedbWpow.dbaseInsert(dbase, wuser_id, entity, row.thread_id, t_s);
    
    println(classname+".dbaseInsertPreparePow: wpow_id="+risul.wpow_id);
    }
  
  public void dbaseSaveUsingPow ( AegwThreadOprReq req, AegwThreadOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);

    AedbWpow.WpowRecord t_s = aedbWpow.getJsons(dbase, wuser_id, req.wpow_id);
    MimThread mthread = aejson.readValue(t_s.json_string, MimThread.class);
    
    if ( mthread.fingerprint.isInvalid() )
      {
      // POW second
      mthread.proof_of_work = calcPowSigned(req.mimPowValue, myself.ukey_private);

      // Since Fingerprint includes the signature and the POW
      mthread.fingerprint= mthread.calcFingerprint();

      if ( shouldPrint(Aedbg.M_dbase, Aedbg.L_debug))
        println(classname+".dbaseInsertUsingPow: fprint="+mthread.fingerprint);

      Integer row_id=aedbThread.dbaseInsert(dbase, mthread);
      risul.system_feedback_msg="inserted row_id="+row_id;
      }
    else
      {
      // POW second
      mthread.update_proof_of_work = calcPowSigned(req.mimPowValue, myself.ukey_private);

      // I retrieve the vote to be adjusted from the wpow
      Integer thread_id = t_s.entity_id;
      Integer row_id = aedbThread.dbaseUpdate(dbase, thread_id, mthread);
      risul.system_feedback_msg="updated row_id="+row_id;
      }
    }

  
  public void dbaseGetDetails (  AegwThreadOprReq req, AegwThreadOprRes risul ) throws SQLException, JsonProcessingException, CloneNotSupportedException 
    {
    Brs ars = aedbThread.selectUsingPrimaryId(dbase, req.aethread.thread_id);
    
    if ( ars.next())
      risul.aethread = newRecordFromBrs(ars);
    
    ars.close();
    }
  
  public static AegwThread newRecordFromBrs ( Brs ars )
    {
    AegwThread row = new AegwThread();

    row.thread_id       = ars.getInteger(AedbThread.cn_id);
    row.thread_name     = ars.getString(AedbThread.cn_name);
    row.thread_board_id = ars.getInteger(AedbThread.cn_board_id,0);
    row.thread_board_name = ars.getString(AedbBoard.cn_name);
    row.thread_link     = ars.getString(AedbThread.cn_link);
    row.thread_body     = ars.getString(AedbThread.cn_body);
    
    row.thread_post_cnt = ars.getInteger(AedbThread.cn_post_cnt, 0);
    row.thread_votes_cnt = ars.getInteger(AedbThread.cn_votes_cnt, 0);

    return row;
    }

  
  }
