/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.server.services;

import java.util.*;

import it.aetherj.backend.dbase.Dbkey;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.services.LoginService;
import it.aetherj.gwtwapp.server.RemoteServiceServletPublic;
import it.aetherj.shared.*;

/**
 * NOTE: This HAS to be mapped in web.xml in WEB-INF like this
 * 

  <servlet>
    <servlet-name>LoginServiceServlet</servlet-name>
    <servlet-class>it.aetherj.frontend.server.services.LoginServiceImpl</servlet-class>
  </servlet>
  
  <servlet-mapping>
    <servlet-name>LoginServiceServlet</servlet-name>
    <url-pattern>/aetherj/services/LoginService</url-pattern>
  </servlet-mapping>

 
 */
public class LoginServiceImpl extends RemoteServiceServletPublic implements LoginService
  {
	private static final long serialVersionUID = 1L;
	
	private static final String classname = "LoginServiceImpl.";
 
  public AegwDictionary getDictionary()
    {
    GwtServiceEnv env = new GwtServiceEnv(this).setIdent(classname+"getDictionary");;

    Locale client_locale = env.getClientLocale();
    
    AegwDictionary risul  = new AegwDictionary();
    risul.client_locale   = ""+client_locale;
    
    ApplicationVersion appv = env.getAppVersion();
    
    risul.dbaseVersion    = appv.getDbaseVersion();
    risul.backendVersion  = appv.getBackendVersion();
    
    try
      { 
      Brs ars = env.dbase.getLabelsAndTranslation(client_locale);
      
      while ( ars.next() ) 
        {
        String label_english = ars.getString(Dbkey.label_english);
        String label_localized = ars.getString(Dbkey.lblmap_value);
        risul.putLabel(label_english,label_localized);
        }
      
      ars.close();
      }
    catch ( Exception exc )
      {
      env.logError(classname+"getMlabelList",exc);
      risul.setEnglishErrorMessage(exc.toString());          
      }

    env.dispose();

    risul.markEnd();
    
    return risul;
    }

	
  private void authenticateWebUser(GwtServiceEnv env, String userName, String userPassword, AegwLoginRes res )
    { 
    Integer wana_id = env.isGoodUsernamePassword(userName,userPassword);
     
    if ( wana_id == null )
      { 
      res.setEnglishErrorMessage("Invalid username/password");
      env.println(classname+"authenticateWebUser: Invalid password for username="+userName);
			env.logEvent( wana_id, "Web Login", "login", "username="+userName+" BAD USER/PW from "+env.getRemoteAddress());
      return;
      }

    // password are OK, let me create a WebSession
    // this is managed using a ServletSession since I need this info even later on  normal servlets
    WebSession wsession = new WebSession(wana_id);

    Random random = new Random();
    wsession.sessionToken = random.nextLong();

    // this must be called only here, at successful login.
    env.setWebSession(wsession);

//    waimpListImport ( env, wana_id );
    
    res.webSession = wsession;
    res.user_login = userName;
    		
		env.logEvent( wana_id, "Web Login", "login", "username="+userName+" from "+env.getRemoteAddress());
    }
  
  public AegwLoginRes authenticateWebUser( String login, String userPassword)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwLoginRes res = new AegwLoginRes();

    try
      {
      authenticateWebUser(env, login, userPassword, res );
      }
    catch ( Exception exc )
      {
      res.setEnglishErrorMessage("Exception: "+exc);
      env.logEvent(null, "web user", "login", "authenticateWebUser: username="+exc);        
      }

    env.dispose();
    return res;
    }


 

  /**
   * Checks for connection status and other stuff
   * @return
   */
  public AegwbConnectionStatus getConnectionStatus(ConnectionStatusReq req)
    {
    GwtServiceEnv env = new GwtServiceEnv(this);
    AegwbConnectionStatus res = new AegwbConnectionStatus();

    if ( ! env.isWsessionValid(req.webSession))
      {
      res.setEnglishErrorMessage("Session is invalid");
      return res;
      }

    if ( env.shouldPrint(Aedbg.M_Gwt_Connection,Aedbg.L_trace))
      env.println("getConnectionStatus: WuserId: "+env.getCurWuserId());
      
    try
      {
      // just some date, now
      res.imp_msg_date=new Date();
      
      }
    catch ( Exception exc )
      {
      res.setEnglishErrorMessage("Exception: "+exc);
      env.logError("getConnectionStatus:",exc);        
      }
    
    env.dispose();
    return res;
    }
  


  
  
  
  
  
  
  
  }
