package it.aetherj.gwtwapp.server.services;

import java.security.KeyPair;
import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.dbtbls.*;
import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.Aedbg;
import it.aetherj.shared.v0.*;

public class GwtsUkey extends GwtsMim
  {
  private static final String classname="GwtsUkey";
  
  public GwtsUkey(GwtServiceEnv env)
    {
    super(env, MimEntityList.aekey);
    }

  /**
   * This is dbase save, but I must prepare the Pow to be sent back to the user
   * This will save the "object" in the temporary table amd return to client the POW to be calculated
   */
  public void dbaseSavePreparePow (  AegwUkeyOprReq req, AegwUkeyOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    AegwUkey row = req.aeukey;
    
    if ( row.ukey_id != CommonReq.NULL_int )
      dbaseUpdatePreparePow (risul, row  );
    else
      dbaseInsertPreparePow ( risul, row );
    }
    

  private void dbaseUpdatePreparePow ( AegwUkeyOprRes risul, AegwUkey row ) throws SQLException, JsonProcessingException, CloneNotSupportedException
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);
    
    // now, I have to get current mim content
    MimKey mukey = aedbUkey.getMimMesage(dbase, row.ukey_id);
    
    if ( ! myself.isFingerprintEqual(mukey.fingerprint) )
      throw new IllegalArgumentException("Update can only occour on same owner fingerprint");
    
    // You cannot change the Ukey name since it is included in the fingerprint
    mukey.info = row.ukey_info;
    
    // need to mark now as the update time, this will be signed
    mukey.last_update = new MimTimestamp();
    
    // possibly first since it zaps pow
    mukey.update_signature=mukey.updateSignature(crypto, myself.ukey_private);

    risul.jsonForPow = mukey.calcUpdatePowSource();
    
    risul.wpow_id = aedbWpow.dbaseInsert(dbase, wuser_id, entity, row.ukey_id, risul.jsonForPow);
    
    println(classname+".dbaseUpdatePreparePow: wpow_id="+risul.wpow_id);
    }

  /**
   * This is a NEW user, so, I need to generate from zero the identity
   * Also, I need to save the private key into the dbase.... ACK ....
   */
  private void dbaseInsertPreparePow ( AegwUkeyOprRes risul, AegwUkey row ) throws SQLException, JsonProcessingException, CloneNotSupportedException 
    {
    KeyPair kp = CryptoEddsa25519.newKeyPair();
    MimPublicKey  publicKey  = new MimPublicKey(kp.getPublic());
    MimPrivateKey privateKey = new MimPrivateKey(kp.getPrivate());
    
    MimKey mukey = new MimKey();

    mukey.key  = publicKey;
    mukey.name = row.ukey_name;
    mukey.info = row.ukey_info; 
     
    // Signature has to be first
    mukey.signature=mukey.newSignature(crypto, privateKey);

    risul.jsonForPow = mukey.calcInsertPowSource();
    
    String t_s = aejson.writeValueAsString(mukey);
    
    // UFF, I need to insert this into dbase since private key, this is going to make the final insert logic peculiar  
    // NOTE that I had to "adjust" the insert to make it happen and you MUST reset the fingerprint later
    row.ukey_id = aedbUkey.dbaseSave(env.dbase, mukey);
    
    // this wires this new record to the user, assigning a private key
    if ( aedbWuser.dbaseUpdate(env.dbase, wuser_id, row.ukey_id, publicKey, privateKey) != 1 )
      env.println(classname+".dbaseInsertPreparePow: failed aedbWuser.dbaseUpdate");
    
    // this put the wanted pow into the table, so, when pow arrives we can do something
    risul.wpow_id = aedbWpow.dbaseInsert(dbase, wuser_id, entity, row.ukey_id, t_s);
    
    println(classname+".dbaseInsertPreparePow: wpow_id="+risul.wpow_id);
    }
  
  /**
   * Ok, now, I  need to be careful here...
   */
  public void dbaseSaveUsingPow ( AegwUkeyOprReq req, AegwUkeyOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    // this works since I have saved both the Ukey and the association in earlier call
    // I need it to pick up the private key that I had created
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);

    if ( myself == null )
      throw new IllegalArgumentException(classname+".dbaseSaveUsingPow: unknown wuser_id="+wuser_id);
    
    AedbWpow.WpowRecord t_s = aedbWpow.getJsons(dbase, wuser_id, req.wpow_id);
    MimKey mkey = aejson.readValue(t_s.json_string, MimKey.class);
    
    // this is the actual case when I should use the invalid
    if ( mkey.fingerprint.isInvalid() )
      {
      // POW second
      mkey.proof_of_work = calcPowSigned(req.mimPowValue, myself.ukey_private);

      // Since Fingerprint includes the signature and the POW
      mkey.fingerprint= mkey.calcFingerprint();

      if ( shouldPrint(Aedbg.M_dbase, Aedbg.L_debug))
        println(classname+".dbaseInsertUsingPow: fprint="+mkey.fingerprint);

      // this is NOT an insert since the record has been inserted before
      Integer row_id=aedbUkey.dbaseUpdate(dbase, myself.ukey_id, mkey);
      risul.system_feedback_msg="Iupdate row_id="+row_id;
      }
    else
      {
      // POW second, on another field
      mkey.update_proof_of_work = calcPowSigned(req.mimPowValue, mkey.privateKey);

      // I retrieve the vote to be adjusted from the wpow
      Integer ukey_id = t_s.entity_id;
      Integer row_id = aedbUkey.dbaseUpdate(dbase, ukey_id, mkey);
      risul.system_feedback_msg="updated row_id="+row_id;
      }
    }

  
  public void dbaseGetDetails (  AegwUkeyOprReq req, AegwUkeyOprRes risul ) throws SQLException, JsonProcessingException, CloneNotSupportedException 
    {
    Brs ars;
    
    if ( req.aeukey != null &&  req.aeukey.ukey_id != 0 )
      {
      // it may happen that there is not yet an association with the user, but I wish to know about a given key
      ars = aedbUkey.selectUsingPrimaryId(dbase, req.aeukey.ukey_id);
      }
    else
      {
      ars = aedbWuser.selectUsingPrimaryId(env.dbase,wuser_id);
      }
    
    if ( ars.next())
      risul.aeukey = newRecordFromBrs(ars);
    
    ars.close();
    }
  
  public static AegwUkey newRecordFromBrs ( Brs ars )
    {
    AegwUkey row = new AegwUkey();

    row.ukey_id   = ars.getInteger(AedbUkey.cn_id,0);
    row.ukey_name = ars.getString(AedbUkey.cn_user_name);
    row.ukey_info = ars.getString(AedbUkey.cn_user_info);
    row.ukey_fprint_hex = new MimFingerprint(ars.getBytes(AedbUkey.cn_fingerprint)).toString();
    row.ukey_pubkey_hex = new MimPublicKey(ars.getBytes(AedbUkey.cn_ukey_public)).toString();

    return row;
    }
  
  
  }
