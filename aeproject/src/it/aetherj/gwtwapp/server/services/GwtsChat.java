package it.aetherj.gwtwapp.server.services;

import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.dbtbls.*;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.Aedbg;
import it.aetherj.shared.bim.BimChat;

public class GwtsChat extends GwtsBim
  {
  private static final String classname="GwtsChat";
  
  protected final AedbChat     aedbChat;

  
  public GwtsChat(GwtServiceEnv env)
    {
    super(env, MimEntityList.aechat);
    
    aedbChat =(AedbChat)env.getAedb(AedbChat.mimEntity);
    }

  /**
   * This is dbase save, but I must prepare the Pow to be sent back to the user
   * This will save the "object" in the temporary table and return to client the POW to be calculated
   * One thing that makes simpler is that chat are NOT updated
   */
  public void dbaseSavePreparePow (  AegwChatOprReq req, AegwChatOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    AegwChat row = req.aechat;
    
    dbaseInsertPreparePow ( risul, row );
    }
  
  private void dbaseInsertPreparePow ( AegwChatOprRes risul, AegwChat row ) throws SQLException, JsonProcessingException, CloneNotSupportedException 
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);

    MimFingerprint to_user = aedbUkey.getFingerprint(dbase, row.chat_to_ukeyid);
    
    if ( to_user.isInvalid() )
      throw new IllegalArgumentException(classname+".dbaseInsertPreparePow: to_user is null id="+row.chat_to_ukeyid);
     
    BimChat chat = new BimChat();
     
    // TODO: There are a few fields to fill up to complete a chat message
    chat.node_fingerprint  = aetherParams.getNodeFingerprint();
    chat.owner_fingerprint = myself.ukey_fingerprint;
    chat.owner_publickey   = myself.ukey_public;
     
    chat.chat_message      = row.chat_message;

    // Signature has to be first
    chat.signature=chat.newSignature(crypto, myself.ukey_private);

    risul.hashForPow = chat.calcInsertPowSource();
    
    String t_s = aejson.writeValueAsString(chat);
    
    risul.wpow_id = aedbWpow.dbaseInsert(dbase, wuser_id, entity, null, t_s);
    
    println(classname+".dbaseInsertPreparePow: wpow_id="+risul.wpow_id);
    }
  
  public void dbaseSaveUsingPow ( AegwChatOprReq req, AegwChatOprRes risul ) throws JsonProcessingException, SQLException, CloneNotSupportedException
    {
    AedbWebuser.WuserIdent myself  = aedbWuser.getWuserIdent(dbase, wuser_id);

    AedbWpow.WpowRecord t_s = aedbWpow.getJsons(dbase, wuser_id, req.wpow_id);
    BimChat chat = aejson.readValue(t_s.json_string, BimChat.class);
    
    // a chat does NOT have an update

    // POW second
    chat.proof_of_work = calcPowSigned(req.mimPowValue, myself.ukey_private);

    // Since Fingerprint includes the signature and the POW
    chat.fingerprint= chat.calcFingerprint();

    if ( shouldPrint(Aedbg.M_dbase, Aedbg.L_debug))
      println(classname+".dbaseInsertUsingPow: fprint="+chat.fingerprint);

    Integer row_id=aedbChat.dbaseInsert(dbase, chat);
    risul.system_feedback_msg="inserted row_id="+row_id;
    }

  
  public void dbaseGetDetails (  AegwChatOprReq req, AegwChatOprRes risul ) throws SQLException, JsonProcessingException, CloneNotSupportedException 
    {
    Brs ars = aedbChat.selectUsingPrimaryId(dbase, req.aechat.chat_id);
    
    if ( ars.next())
      risul.aechat = newRecordFromBrs(ars);
    
    ars.close();
    }

  public static AegwChat newRecordFromBrs ( Brs ars )
    {
    AegwChat row = new AegwChat();

    row.chat_id       = ars.getInteger(AedbChat.cn_id);
    row.chat_from_id  = ars.getInteger(AedbChat.cn_owner_id,0);
    row.chat_message  = ars.getString(AedbChat.cn_message);

    return row;
    }
  
  
  }
