/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.server.servlets;

import java.io.*;
import java.sql.SQLException;
import java.util.Locale;

import javax.servlet.http.*;

import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import it.aetherj.backend.dbase.*;
import it.aetherj.boot.Log;
import it.aetherj.boot.dbase.*;
import it.aetherj.gwtwapp.client.beans.AegwPageHelp;
import it.aetherj.gwtwapp.client.services.RewebServiceFactory;
import it.aetherj.gwtwapp.server.services.GwtServiceEnv;
import it.aetherj.protocol.AetherCo;
import it.aetherj.shared.ServletsUtils; 

public final class PageHelpServlet extends HttpServlet
  {
  private static final long serialVersionUID = 1L;
  private static final String classname="PageHelpServlet.";
  
  // the pattern that this servlet is mapped to
  public static final String web_xml_url_pattern=AetherCo.gwtwapp_context+"/"+RewebServiceFactory.getPageHelpSrvlet;
  
  private Brs getPageHelpRow ( AeDbase dbase, String pagehelp_key, String pagehelp_lang ) throws SQLException
    {
    String sql = "SELECT * FROM " + AegwPageHelp.cn_tbl + " WHERE " +
      AegwPageHelp.cn_key + " =? AND " +
      AegwPageHelp.cn_lang + "=? ";
    
    Dprepared prepared = dbase.getPreparedStatement(sql);
    int insIndex=1;
    prepared.setString(insIndex++,pagehelp_key);
    prepared.setString(insIndex++,pagehelp_lang);
    
    return prepared.executeQuery();
    }


  private void insertPageHelp(AeDbase dbase, Locale lang_id, String pagehelp_key) throws SQLException
    {
    String sql = "INSERT INTO " + Dbkey.pagehelp_tbl + " ( " +
      Dbkey.pagehelp_key + ", " +
      Dbkey.pagehelp_lang + ", " +
      Dbkey.pagehelp_desc +
      " ) VALUES ( ?,?,? ) ";  
    
    Dprepared prepared = dbase.getPreparedStatement(sql);
    int insIndex=1;
    prepared.setString(insIndex++,pagehelp_key);
    prepared.setString(insIndex++,lang_id.toString());
    prepared.setString(insIndex++,"Automatic insert, please update");
    prepared.executeUpdate();
    prepared.close();
    }
 

  private void writePdf (GwtServiceEnv env, String pagehelp_key, HttpServletResponse resp ) throws IOException,SQLException
    {
		ServletsUtils utils = env.servletUtils;
		
    String pagehelp_lang = ""+env.getClientLocale();
    
    Brs ars = getPageHelpRow(env.dbase, pagehelp_key, pagehelp_lang);

    if ( ! ars.next() )  
      {
      ars.close();

      // insert the missing key      
      insertPageHelp(env.dbase,env.getClientLocale(),pagehelp_key);      
      
      // then write a reasonable response
      writeMissingHelp(env,resp,pagehelp_key);

      return;
      }

    byte []pdf_data = ars.getBytes(Dbkey.pagehelp_pdf);
    if ( pdf_data == null || pdf_data.length < 1 )
      {
      ars.close();
      utils.writeHtmlMessage(resp,classname+"writePdf: pdf_data is null or empty pagehelp_key="+pagehelp_key+" for lang="+pagehelp_lang);        
      return;
      }

    ars.close();
    
    resp.setContentLength(pdf_data.length);
    resp.setContentType("application/pdf");
    
    OutputStream out = resp.getOutputStream();
    out.write(pdf_data, 0, pdf_data.length);
    out.close();    
    }


  private void writeMissingHelp (GwtServiceEnv env, HttpServletResponse resp, String pagehelp_key ) throws IOException
    {
    resp.setContentType("text/html");

    PrintWriter out = resp.getWriter();
    
    out.println("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN' >");
    out.println("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
    out.println("<head>");
    out.println("<title>Empty Page Help</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<h1>Help on this page is not yet available</h1>");
    out.println("<p>Language="+env.getClientLocale()+"</p>");
    out.println("<p>Key="+pagehelp_key+"</p>");
    out.println("</body>");
    out.println("</html>");
    out.close();
    }


  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException 
    {
    GwtServiceEnv env = new GwtServiceEnv(this,req).setIdent(classname+"doGet");

    try
      {
      String pagehelp_key = req.getParameter(AegwPageHelp.cn_key);
  
      // if nothing is specified then I assume it is a request from web
      if ( pagehelp_key == null ) pagehelp_key = AegwPageHelp.default_key;

      writePdf(env,pagehelp_key,resp);
      }
    catch ( Exception exc )
      {
      // I do not know why but apparently there is an exception every request ...
      env.println(classname+"doGet",exc);        
      }
    
    env.dispose();
    }
  
  private void fillOneField (ServletsUtils utils, FileItemStream item, AegwPageHelp icat ) throws IOException
    {
    String name = item.getFieldName();
    InputStream stream = item.openStream();

//    System.out.println("Iten id filed="+item.isFormField()+" name="+ name);
    
    if ( name == null )
      {
      System.err.println(classname+"fillOneField: name==null");
      stream.close();
      return;
      }
    
    if (item.isFormField()) 
      {
      if ( name.equals(AegwPageHelp.cn_desc))
        {
        icat.pagehelp_desc = Streams.asString(stream);
        }
      
      if ( name.equals(AegwPageHelp.cn_id))
        { 
        String tmp = Streams.asString(stream);
//        System.out.println(classname+"fillOneField: icat_id tmp="+tmp);
        icat.pagehelp_id = utils.convStringToInteger(tmp);
        }
      } 
    else
      {
      icat.pagehelp_pdf = utils.readByteStream(stream,7*1024*1024);
      icat.pagehelp_filename = item.getName();
      }
    
    stream.close();
    }


  /**
   * Do the actual work, here I know that the session is valid.
   * @param env
   * @param req
   * @param resp
   */
  private void doPostWork (GwtServiceEnv env, HttpServletRequest req, HttpServletResponse resp ) throws Exception
    {
    boolean isMultipart = ServletFileUpload.isMultipartContent(req);

    if ( ! isMultipart ) return;
    
		ServletsUtils utils = env.servletUtils;
    ServletFileUpload upload = new ServletFileUpload();
    AegwPageHelp pagehelp = new AegwPageHelp();

    // Parse the request
    FileItemIterator iter = upload.getItemIterator(req);
    while (iter.hasNext()) 
      {
      FileItemStream item = iter.next();
      fillOneField(utils,item,pagehelp);
      }
    
    env.println("received pagehelp="+pagehelp);
    
    String response = "Page Help Update complete";
    
    try
      {
      // update page help into dbase, catch and report errors
      updatePagehelp ( env, pagehelp );
      }
    catch ( SQLException exc )
      {
      response = Log.exceptionExpand(classname+"doPostWork", exc);
      }
    
    utils.writeHtmlMessage(resp,response);
    }
  
  
  private void updatePagehelp ( GwtServiceEnv env, AegwPageHelp pagehelp ) throws SQLException
    {
    if ( pagehelp.pagehelp_pdf == null || pagehelp.pagehelp_pdf.length < 1 ) 
      updatePagehelpNopdf(env,pagehelp);
    else
      updatePagehelpFull(env,pagehelp);
    }
  
  private void updatePagehelpNopdf ( GwtServiceEnv env, AegwPageHelp pagehelp ) throws SQLException
    {
    String query = "UPDATE "+AegwPageHelp.cn_tbl+" SET "+
      AegwPageHelp.cn_desc+"=? WHERE "+
      AegwPageHelp.cn_id+"=?";
    
    Dprepared prepared = env.dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setString(insIndex++,pagehelp.pagehelp_desc);
    prepared.setInt(insIndex++,pagehelp.pagehelp_id);
    prepared.executeUpdate();
    prepared.close();
    }
  
  private String trim ( String input, int maxlen )
    {
    if ( input == null ) return null;
    
    int start = input.length() - maxlen;
    if ( start < 0 ) start = 0;
    
    return input.substring(start);
    }
  
  private void updatePagehelpFull ( GwtServiceEnv env, AegwPageHelp pagehelp ) throws SQLException
    {
    String query = "UPDATE "+AegwPageHelp.cn_tbl+" SET "+
      AegwPageHelp.cn_desc+"=?,"+
      AegwPageHelp.cn_filename+"=?,"+
      AegwPageHelp.cn_pdf+"=? WHERE "+
      AegwPageHelp.cn_id+"=?";
    
    Dprepared prepared = env.dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setString(insIndex++,pagehelp.pagehelp_desc);
    prepared.setString(insIndex++,trim(pagehelp.pagehelp_filename,40));
    prepared.setBinary(insIndex++,pagehelp.pagehelp_pdf);
    prepared.setInt(insIndex++,pagehelp.pagehelp_id);
    prepared.executeUpdate();
    prepared.close();
    }
  
  
  /**
   * This is used when we wish to upload a new icon or update a new name or insert a new row...
   * or actually, delete an icon...
   * @param req
   * @param resp
   * @throws IOException
   */
  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException 
    {
    GwtServiceEnv env = new GwtServiceEnv(this,req).setIdent(classname+"doPost");

    if ( ! env.isWsessionValid(null)) return;

    try
      {
      doPostWork(env,req,resp);
      }
    catch ( Exception exc )
      {
      env.println(classname+"doPost",exc);        
      }
    
    env.dispose();
    }
  }
