/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.server.servlets;

import java.io.*;
import java.sql.SQLException;

import javax.servlet.http.*;

import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;

import it.aetherj.backend.dbtbls.*;
import it.aetherj.boot.Log;
import it.aetherj.gwtwapp.client.beans.AegwUsub;
import it.aetherj.gwtwapp.client.services.RewebServiceFactory;
import it.aetherj.gwtwapp.server.services.GwtServiceEnv;
import it.aetherj.protocol.AetherCo;
import it.aetherj.shared.*;
import it.aetherj.shared.v0.*; 

/**
 * This should import the user config into the current Wuser
 * It is server side since there is quite a lot og json parsing that is reliable server side only
 * @author damiano
 *
 */
public final class ImportWusubServlet extends HttpServlet
  {
  private static final long serialVersionUID = 1L;
  private static final String classname="ImportWusubServlet.";
  
  // the pattern that this servlet is mapped to
  public static final String web_xml_url_pattern=AetherCo.gwtwapp_context+"/"+RewebServiceFactory.importWusubSrvlet;

  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException 
    {
    GwtServiceEnv env = new GwtServiceEnv(this,req).setIdent(classname+"doGet");
    
    env.dispose();
    }
  
  private ReceivedOneUpload receiveOneUpload (ServletsUtils utils, FileItemStream item ) throws IOException
    {
    ReceivedOneUpload risul = new ReceivedOneUpload();

    String name = item.getFieldName();
    InputStream stream = item.openStream();

//    System.out.println("Iten id filed="+item.isFormField()+" name="+ name);
    
    if ( name == null )
      {
      System.err.println(classname+"fillOneField: name==null");
      stream.close();
      return risul;
      }
    
    risul.value = utils.readByteStream(stream,7*1024*1024);
    risul.name = item.getName();

    stream.close();
    
    return risul;
    }

  /**
   * Need to transform the Json into Object and then parse the object...
   * @param env
   * @param received
   * @throws SQLException
   */
  private void updateWusub ( GwtServiceEnv env, ReceivedOneUpload received ) throws Exception
    {
    AeJson json = new AeJson();
     
    json.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    
    UserConfig config = json.readValue(received.value, UserConfig.class);
    
    updateWuser(env, config );
    updateWusub(env, config.ContentRelations.SubbedBoards);
    }

  private void updateWuser (GwtServiceEnv env, UserConfig config ) throws JsonMappingException, JsonProcessingException, SQLException
    {
    MimPublicKey pubkey = new MimPublicKey(config.MarshaledUserPublicKey);
    env.println("pubkey="+pubkey);
    
    MimPrivateKey privkey = getPrivate(config.UserKeyPair);
    env.println("privkey="+privkey);

//    env.println(config.DehydratedLocalUserKeyEntity);
    AeJson json = new AeJson();
    
    MimKey ukey = json.readValue(config.DehydratedLocalUserKeyEntity, MimKey.class);
    AedbUkey aeukey=(AedbUkey)env.getAedb(AedbUkey.mimEntity);
    
    Integer ukey_id = aeukey.dbaseSave(env.dbase, ukey);
    
    AedbWebuser aedb=(AedbWebuser)env.getAedb(AedbWebuser.mimEntity);
    Integer wuser_id = env.getCurWuserId();

    if ( aedb.dbaseUpdate(env.dbase, wuser_id, ukey_id, pubkey, privkey) != 1 )
      env.println(classname+".updateWuser: failed ");
    
    }
  
  private MimPrivateKey getPrivate ( String userKeyPair)
    {
    if ( userKeyPair == null || userKeyPair.length() < MimPrivateKey.priv_key_bytes_len*2 )
      return new MimPrivateKey();
      
    return new MimPrivateKey(userKeyPair.substring(0,MimPrivateKey.priv_key_bytes_len*2));
    }
  
  private void updateWusub (GwtServiceEnv env, SubbedBoard []subbedBoards ) throws Exception
    {
    env.println("SubbedBoard="+subbedBoards);
    
    AedbWusub aedb=(AedbWusub)env.getAedb(AedbWusub.mimEntity);
    Integer wuser_id = env.getCurWuserId();
    
    for ( SubbedBoard subbed : subbedBoards )
      {
      AegwUsub wusub = new AegwUsub();
      
      wusub.wusub_wuser_id = wuser_id;
      wusub.board_fingerprint = subbed.Fingerprint;
      wusub.wusub_notify = subbed.Notify;
      wusub.wusub_last_seen = subbed.LastSeen.getTimestamp();
      
      aedb.dbaseSave(env.dbase, wusub);
      }
    }
  
  
  /**
   * Do the actual work, here I know that the session is valid.
   */
  private void doPostWork (GwtServiceEnv env, HttpServletRequest req, HttpServletResponse resp ) throws Exception
    {
    boolean isMultipart = ServletFileUpload.isMultipartContent(req);

    if ( ! isMultipart ) return;
    
		ServletsUtils utils = env.servletUtils;
    ServletFileUpload upload = new ServletFileUpload();

    ReceivedOneUpload received=new ReceivedOneUpload();
    
    // Parse the request
    FileItemIterator iter = upload.getItemIterator(req);
    while (iter.hasNext()) 
      {
      FileItemStream item = iter.next();
      received=receiveOneUpload(utils,item);
      }
    
    env.println(classname+".doPostWork() "+received);
    
    String response = "Web user import complete";
    
    try
      {
      // update page help into dbase, catch and report errors
      if ( received.hasValue())
        updateWusub ( env, received );
      }
    catch ( SQLException exc )
      {
      response = Log.exceptionExpand(classname+"doPostWork", exc);
      }
    
    utils.writeHtmlMessage(resp,response);
    }
  
  
  /**
   * This is used when we wish to upload a new settings from Json
   */
  @Override
  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException 
    {
    GwtServiceEnv env = new GwtServiceEnv(this,req).setIdent(classname+"doPost");

    if ( ! env.isWsessionValid(null)) return;

    try
      {
      doPostWork(env,req,resp);
      }
    catch ( Exception exc )
      {
      env.println(classname+"doPost",exc);        
      }
    
    env.dispose();
    }
  
private static class ReceivedOneUpload
  {
  public byte[] value;
  public String name;
  
  public boolean hasValue ()
    {
    return value != null && value.length > 1;
    }
  
  public String toString()
    {
    return "fname="+name;
    }
  }
  

public static class SubbedBoard
  {
  public String Fingerprint;
  public Boolean Notify;
  public MimTimestamp LastSeen;
  }

public static class SFWList
  {
  public String Source; 
  public String[] Boards;
  }

public static class ContentRelations
  {
  public SubbedBoard []SubbedBoards;
  public SFWList SFWList;
  }


public static class UserConfig
  {
  public String UserKeyPair;
  public String MarshaledUserPublicKey;
  public ContentRelations ContentRelations;
  public String DehydratedLocalUserKeyEntity;
  }


  }