/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.varie;

import java.util.Date;

import com.google.gwt.i18n.client.*;
import com.google.gwt.user.datepicker.client.DateBox;

import it.aetherj.gwtwapp.client.RecliStat;

public final class RecliFormatter
  {
  private static final String classname="RecliFormatter.";
  
  private final RecliStat stat;
  private DateTimeFormat shortDateFormat,longDateFormat,dmyhmsDateFormat,shortTimeFormat;
  private NumberFormat formatDecimal,formatTemperature;

  public RecliFormatter ( RecliStat stat )
    {
    this.stat = stat;
    }


  public String getDateAsShortTime ( Date date )
    {
    if ( date == null ) return "";
          
    if ( shortTimeFormat != null ) return shortTimeFormat.format(date);
    
    String pattern = stat.getLabel("format_date_hhmmss",classname,"H:mm:s");

    shortTimeFormat = DateTimeFormat.getFormat(pattern);
    
    return shortTimeFormat.format(date);
    }



  public String getDateAsDmyhms ( Date date )
    {
    if ( date == null ) return "";
          
    if ( dmyhmsDateFormat != null ) return dmyhmsDateFormat.format(date);
    
    String pattern = stat.getLabel("format_date_dmyhms",classname,"d/M/y H:mm:s");

    dmyhmsDateFormat = DateTimeFormat.getFormat(pattern);
    
    return dmyhmsDateFormat.format(date);
    }

  public String getDateShort ( Date value )
    {
    if ( value == null ) return "";
    
    return getDateShort().format(value);
    }

  public String getDateLong ( Date timestamp )
    {
    if ( timestamp == null ) return "";
          
    if ( longDateFormat != null ) return longDateFormat.format(timestamp);
    
    String pattern = stat.getLabel("format_date_long",classname,"d EEEEE M MMMM y");

    longDateFormat = DateTimeFormat.getFormat(pattern);
    
    return longDateFormat.format(timestamp);
    }

  public String formatDecimal ( Double value )
    {
    if ( value == null ) return "";
    
    if ( formatDecimal != null ) return formatDecimal.format(value);
    
    String pattern = stat.getLabel("format_decimal",classname,"#,##0.###");

    formatDecimal = NumberFormat.getFormat(pattern);
    
    return formatDecimal.format(value);
    }
  

  public String formatTemperature ( Double value )
    {
    if ( value == null ) return "";
    
    if ( formatTemperature != null ) return formatTemperature.format(value);
    
    String pattern = stat.getLabel("format_temperature",classname,"##0.0");

    formatTemperature = NumberFormat.getFormat(pattern);
    
    return formatTemperature.format(value);
    }

  /**
   * Needed to make sure that I initialize the format properly
   * @return
   */
  private DateTimeFormat getDateShort ()
    {
    if ( shortDateFormat != null ) return shortDateFormat;
    
    // let me try to get the correct formatter for the double
    String pattern = stat.getLabel("format_date_short",classname,"y-MM-dd");
    shortDateFormat = DateTimeFormat.getFormat(pattern);
    
    return shortDateFormat;
    }
  
  public DateBox.DefaultFormat newDateBoxFormatDateShort ()
    {
    return new DateBox.DefaultFormat(getDateShort());
    }

  
  }
