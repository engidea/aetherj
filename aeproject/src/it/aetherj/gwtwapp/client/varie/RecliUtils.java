/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.varie;

import java.sql.Timestamp;
import java.util.*;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.ui.AnchorLink;

public final class RecliUtils
  {
  private static final String classname="RecliUtils.";
  
  private final RecliStat stat;
	private final DateTimeFormat dateFormatAAMMGG;
	private final DateTimeFormat dateFormatAAAAMMGG;
	private final DateTimeFormat dateFormatMonth;    // used to retrieve the month of a date using GWT
	private final DateTimeFormat dateFormatMday;     // used to retrieve the day of a month in GWT
	private final DateTimeFormat dateFormatYYYY;     // used to retrieve the current year
	private final DateTimeFormat dateFormatYYYYMMDDhhmm;   // used to retrieve a kind of full date
	private final DateTimeFormat dateFormatDayweek;        // used to retrieve the day of week
  
  public RecliUtils ( RecliStat stat )
    {
    this.stat = stat;
		this.dateFormatAAMMGG   = DateTimeFormat.getFormat("yyMMdd");
		this.dateFormatAAAAMMGG = DateTimeFormat.getFormat("yyyyMMdd");
		this.dateFormatMonth    = DateTimeFormat.getFormat("MM");
    this.dateFormatMday     = DateTimeFormat.getFormat("dd");
		this.dateFormatYYYY     = DateTimeFormat.getFormat("yyyy");
		this.dateFormatYYYYMMDDhhmm = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm");
		this.dateFormatDayweek = DateTimeFormat.getFormat("EEEE");
		}

	public String convDateToYYYYMMDDhhmm ( Date input )
		{
		if ( input == null ) return "null-input";
		
		return dateFormatYYYYMMDDhhmm.format(input);
		}

	public Date convAAAAMMGGtoDate ( String AAAAMMGG )
		{
		return dateFormatAAAAMMGG.parse(AAAAMMGG);			
		}


  public AnchorLink newAnchorLinkDownload (ImageResource img_resource)
    {
    Image image = new Image(img_resource);
    image.setTitle("Download Document");
		image.setStylePrimaryName("better-PushButton");
		
		AnchorLink risul = new AnchorLink();
		risul.setWidget(image);

    return risul;
    }

  public PushButton newPushButton (ImageResource source, ClickHandler handler, String tooltip )
    {
    Image image = new Image(source);
    
    if ( tooltip != null ) 
      image.setTitle(tooltip);

    PushButton button = new PushButton(image);
    
    button.setStylePrimaryName("better-PushButton");

    if ( handler != null )  
      button.addClickHandler(handler);
    
    return button;
    }

  public PushButton newNewButton (ClickHandler handler)
    {
    return newPushButton(stat.imageBoundle.newNew(), handler, "New Record");
    }

  public PushButton newEditButton (ClickHandler handler)
    {
    return newPushButton(stat.imageBoundle.newEdit(), handler, "Edit");
    }
  
  public PushButton newBackButton (ClickHandler handler)
    {
    return newPushButton(stat.imageBoundle.newBack(), handler, "Back current page data");
    }

  public PushButton newRefreshButton (ClickHandler handler)
    { 
    return newPushButton(stat.imageBoundle.newRefresh(), handler, "Refresh current page data");
    }
  
  public PushButton newInfoButton (ClickHandler handler)
    { 
    return newPushButton(stat.imageBoundle.newInfo(), handler, "Information");
    }

  public PushButton newLogoffButton (ClickHandler handler)
    { 
    return newPushButton(stat.imageBoundle.newLogoff(), handler, "Logoff");
    }

  public PushButton newSaveButton (ClickHandler handler)
    {
    return newPushButton(stat.imageBoundle.newSave(), handler, "Save current Data");
    }

  public PushButton newHelpButton (ClickHandler handler)
    {
    return newPushButton(stat.imageBoundle.newHelp(), handler, "Help on current Panel");
    }

  
  public String getModelName ( Integer model_num )
    {
    if ( model_num == null ) return "";
    
    return getModelName (model_num.intValue());
    }

  protected String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  
  
  
  
  
  /**
   * Compare prev and cur and return the new if it is different than prev.
   * @param prev
   * @param cur
   * @return
   */
  public static String getNewestValue ( String prev, String cur )
    {
    if ( cur == null  ) return prev;
    
    // From now on we know that cur is not null
    if ( prev == null ) return cur;
    
    // now I know that they are BOTH not null
    if ( cur.equals(prev )) return null;
    
    return cur;
    }

  public static Boolean getNewestValue ( Boolean prev, Boolean cur )
    {
    if ( cur == null  ) return prev;
    
    // From now on we know that cur is not null
    if ( prev == null ) return cur;
    
    // now I know that they are BOTH not null
    if ( cur.equals(prev)) return null;
    
    return cur;
    }

  public static Integer getNewestValue ( Integer prev, Integer cur )
    {
    if ( cur == null  ) return prev;
    
    // From now on we know that cur is not null
    if ( prev == null ) return cur;
    
    // now I know that they are BOTH not null
    if ( cur.equals(prev)) return null;
    
    return cur;
    }

	public Timestamp convDateBoxToTimestamp ( DateBox input )
		{
		if ( input == null ) return null;
		
		Date adate = input.getValue();
		
		if ( adate == null ) return null;
		
		return new Timestamp(adate.getTime());
		}

  public int[] convArrayListInteger ( ArrayList<Integer>input)
    {
    int[] risul = new int[input.size()];
    for (int index = 0; index < risul.length; index++)
      risul[index] = input.get(index).intValue();
      
    return risul;
    }


  private void addRange(ArrayList<Integer> dest, int start, int end, boolean haveRange)
    {
    if (!haveRange)
      {
      dest.add(start);
      return;
      }
    
    int delta = end - start;

    if ( delta > 255) 
      {
      Window.alert(classname+"addRange: illegal delta="+delta);        
      return;
      }
    
    // the range includes the start and end point, so it is a <= 
    for (int index = 0; index <= delta; index++) dest.add(start++);
    }



	public final Integer convTextBoxToInteger ( TextBox input )
		{
		return convTextBoxToInteger(input, null);
		}

	public final Integer convTextBoxToInteger ( TextBox input, Integer defval )
		{
		if ( input == null ) return defval;
		
		return convStringToInteger(input.getText(), defval);
		}
	
	public final int convTextBoxToInt ( TextBox input, int defval )
		{
		if ( input == null ) return defval;
		
		return convStringToInt(input.getText(), defval);
		}

	public final Integer convStringToInteger ( String input, Integer defval )
		{
		try
			{
			return Integer.decode(input);
			}
		catch ( Exception exc )
			{
			return defval;				
			}
		}


	public final int convStringToInt ( String input, int defval )
		{
		try
			{
			return Integer.parseInt(input);
			}
		catch ( Exception exc )
			{
			return defval;				
			}
		}


	public final Double convTextBoxToDouble ( TextBox input, Double defval )
		{
		if ( input == null ) return defval;
		
		return convStringToDouble(input.getText(), defval);
		}


	public final Double convStringToDouble ( String input, Double defval )
		{
		try
			{
			return Double.valueOf(input);
			}
		catch ( Exception exc )
			{
			return defval;				
			}
		}


	public String convIntToTwoDec ( int value )
		{
		String aval = Integer.toString(value);
		
		int strlen = aval.length();
		
		if ( strlen > 1 )
			return aval.substring(strlen-2, strlen);
		
		return "0"+aval;
		}


	public String convDateToAAMMGG ( Date input )
		{
		if ( input == null ) return "000000";
		
		return dateFormatAAMMGG.format(input);
		}

	public String convDateToAAAAMMGG ( Date input )
		{
		if ( input == null ) return "00000000";
		
		return dateFormatAAAAMMGG.format(input);
		}


	/**
	 * Extract from the given date the month part
	 * @param input
	 * @return
	 */
	public String convDateToMonth ( Date input )
		{
		if ( input == null ) return "0";
		
		return dateFormatMonth.format(input);
		}

  /**
   * Extract from the given date the day of week
   * @param input if null an empty string will be returned
   * @return a day of week, full name, like Monday
   */
  public String convDateToDayOfWeek ( Date input )
    {
    if ( input == null ) return "";
    
    return dateFormatDayweek.format(input);
    }

	public int convDateToMonth ( Date input, int def_month )
		{
		if ( input == null ) return def_month;
		
		try
			{
			String amonth = dateFormatMonth.format(input);
			return Integer.parseInt(amonth);
			}
		catch ( Exception exc )
			{
			return def_month;				
			}
		}

	/**
	 * Extract from the given dfate the day of month
	 * @param input
	 * @return
	 */
	public String convDateToMday ( Date input )
		{
		if ( input == null ) return "0";
		
		return dateFormatMday.format(input);
		}

	public int convDateToMday ( Date input, int def_day )
		{
		if ( input == null ) return def_day;
		
		try
			{
			String aday = dateFormatMday.format(input);
			return Integer.parseInt(aday);
			}
		catch ( Exception exc )
			{
			return def_day;				
			}
		}



	public int convDateToYYYY ( Date input, int def_year )
		{
		if ( input == null ) return def_year;
		
		try
			{
			String ayear = dateFormatYYYY.format(input);
			return Integer.parseInt(ayear);
			}
		catch ( Exception exc )
			{
			return def_year;				
			}
		}

/*
  public final void postComboIntegerDataRefresh ( AegwComboReq req, RecliGenericCallback callback )
    {
    RewebImpServiceAsync service = stat.serviceFactory.newImpService();
    req.combo_key_type = AegwComboReq.key_type_integer;
    service.getComboList(req, callback);
    }
*/
	public final String getDbSchemaTableName ( String table_name )
		{
		return "rpca."+table_name;
		}

	public final String getMonthEnglishName ( int month )
		{
		switch ( month )
			{
			case 1: return "January";
			case 2: return "February";
			case 3: return "March";
			case 4: return "April";
			case 5: return "May";
			case 6: return "Jun";
			case 7: return "July";
			case 8: return "August";
			case 9: return "September";
			case 10: return "October";
			case 11: return "November";
			case 12: return "December";
			default: return "???"+month+"???";
			}
		}

  }
