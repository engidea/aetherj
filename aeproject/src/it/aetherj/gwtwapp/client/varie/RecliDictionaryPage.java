/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.varie;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.AegwDictionary;
import it.aetherj.gwtwapp.client.gui.AecliLoginPage;
import it.aetherj.gwtwapp.client.services.LoginServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;

/**
 * The point is that I should give some feedback on what is happening. 
 */
public final class RecliDictionaryPage implements GwtWidgetProvider
  {
  private static final String classname="RecliDictionaryPage.";
  
  private final RecliStat stat;
  private final ComplexPanel workPanel;

  public RecliDictionaryPage(RecliStat stat)
    {
    this.stat = stat;

    workPanel = newWorkPanel();
    
    postMlabelGet();
    }

	/**
	 * At this stage there is not a simple way to center a widget into a page
	 * The best way seems to be this one
	 * However, the rest of the site may be best served in another way
	 * @return
	 */
  private ComplexPanel newWorkPanel()
    {
    GwuiDockPanel risul = new GwuiDockPanel();
    risul.setHorizontalAlignment(DockPanel.ALIGN_CENTER);
    risul.setVerticalAlignment(DockPanel.ALIGN_MIDDLE);
    risul.setCenter(newCenterPanel());

    return risul;
    }


  /**
   * One panel is the image, the other is the actual login page
   * @return
   */
  private HorizontalPanel newCenterPanel()
    {
    HorizontalPanel risul = new HorizontalPanel();
    risul.addStyleName("loginPanel");

    Image logo = new Image(GWT.getHostPageBaseURL()+"images/logo.png");
    risul.add(logo);
    risul.add(newInputPanel());
    return risul;
    }


  private VerticalPanel newInputPanel()
    {
    VerticalPanel risul = new VerticalPanel();

    // this cannot be localized since I do not have the labels, yet.
    risul.add(new Label("Loading localized application"));

    return risul;
    }


  public ComplexPanel getPanelToDisplay()
    {
    return workPanel;
    }


  public void postMlabelGet()
    {
    LoginServiceAsync service = stat.serviceFactory.newLoginService();
    service.getDictionary(new DictionaryDataCallback());
    }


private class DictionaryDataCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwDictionary labels = (AegwDictionary)result;

    if ( isErrorMessage(labels)) return;

    stat.dictionary = labels;

    // Now it is the correct time to create the objects, they have the dictionary to work
    stat.formatter        = new RecliFormatter(stat);       // this should be actually be set when the dictionary is OK
    stat.dictionaryAssist = new DictionaryAssistant(stat);  // this should be actually be set when the dictionary is OK

    stat.logOnBrowser(classname+"onSuccess labels="+labels);

    // create the login page, should really be done once
    stat.loginPage = new AecliLoginPage(stat);
 
    // Now show the login page
    stat.setRootCanvas(stat.loginPage);
    }
  }


}
