/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.uict;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.Column;
    
/**
 * A column that displays its contents with a cell that uses a datatype to display an ImageResource
 */
public abstract class ColumnImageClick<T> extends Column<T, ImageResource> 
		{
		/**
		 * This has no handler defined, basically is just an image to display but with a hand
		 */
    public ColumnImageClick() 
    		{
        super(new CellImageClick());
    		}

    /**
     * use this one when you wish to pass an imageClick that has a click handler defined
     * @param imageClick
     */
    public ColumnImageClick(CellImageClick imageClick) 
    		{
        super(imageClick);
    		}

    
   /**      
    * Given an object returns the Image to display
    * You normally override this one to return the correct data 
    * @param object The value to get
    * @return the image resource that should be display for the given object
    */    
    @Override    
    public ImageResource getValue(T object) 
    	{    
      return null;         
    	}  
		}
