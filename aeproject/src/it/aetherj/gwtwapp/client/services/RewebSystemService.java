/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.services;

import com.google.gwt.user.client.rpc.RemoteService;

import it.aetherj.gwtwapp.client.beans.*;

public interface RewebSystemService extends RemoteService
  {
  public AegwLabelSaveRes saveLabelList(AegwLabelSaveReq req);
  public AegwUsubListRes  saveWusubList ( AegwUsubSaveReq req);
  
  public AegwComboRes        getComboList(AegwComboReq req);
  public AegwPageHelpListRes getPageHelpList ( AegwPageHelpListReq rep );
  
  public AegwUkeyListRes   getUkeyList ( AegwUkeyListReq req);
  public AegwUkeyOprRes    postUkeyOpr ( AegwUkeyOprReq req);

  public AegwBoardListRes  getBoardList ( AegwBoardListReq req);

  public AegwThreadListRes getThreadList ( AegwThreadListReq req);
  public AegwThreadOprRes  postThreadOpr ( AegwThreadOprReq req);

  public AegwPostListRes   getPostList ( AegwPostListReq req);
  public AegwPostOprRes    postPostOpr ( AegwPostOprReq req);
  
  public AegwUsubListRes   getWusubList ( AegwUsubListReq req);

  public AegwVoteOprRes    postVoteOpr ( AegwVoteOprReq req);

  public AegwRecentListRes getRecentList ( AegwRecentListReq req);

  public AegwChatListRes getChatList ( AegwChatListReq req);
  public AegwChatOprRes  postChatOpr ( AegwChatOprReq req);

  }
