/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.services;

import com.google.gwt.user.client.rpc.RemoteService;

import it.aetherj.gwtwapp.client.beans.*;

/**
 * Provide basic services
 * @author damiano
 *
 */
public interface LoginService extends RemoteService
  {
  public AegwDictionary getDictionary();

  public AegwLoginRes authenticateWebUser(String userName, String userPassword);
  public AegwbConnectionStatus getConnectionStatus(ConnectionStatusReq req);

  
  }
