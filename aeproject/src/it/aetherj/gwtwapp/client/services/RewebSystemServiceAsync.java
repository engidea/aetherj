/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;

import it.aetherj.gwtwapp.client.beans.*;

public interface RewebSystemServiceAsync
  {
  public void saveLabelList(AegwLabelSaveReq req, AsyncCallback<?> asyncCallback);
  public void saveWusubList(AegwUsubSaveReq req, AsyncCallback<?> asyncCallback);
  
  public void getComboList(AegwComboReq req,  AsyncCallback<?> asyncCallback);
  public void getPageHelpList(AegwPageHelpListReq req,  AsyncCallback<?> asyncCallback);

  public void getUkeyList ( AegwUkeyListReq req, AsyncCallback<?> asyncCallback);
  public void postUkeyOpr ( AegwUkeyOprReq req, AsyncCallback<?> asyncCallback);

  public void getBoardList ( AegwBoardListReq req, AsyncCallback<?> asyncCallback);

  public void getThreadList ( AegwThreadListReq req, AsyncCallback<?> asyncCallback);
  public void postThreadOpr ( AegwThreadOprReq req, AsyncCallback<?> asyncCallback);
  
  public void getPostList ( AegwPostListReq req, AsyncCallback<?> asyncCallback);
  public void postPostOpr ( AegwPostOprReq req, AsyncCallback<?> asyncCallback);
  
  public void getWusubList ( AegwUsubListReq req, AsyncCallback<?> asyncCallback);

  public void postVoteOpr ( AegwVoteOprReq req, AsyncCallback<?> asyncCallback);

  public void getRecentList ( AegwRecentListReq req, AsyncCallback<?> asyncCallback);
  
  public void getChatList  ( AegwChatListReq req, AsyncCallback<?> asyncCallback);
  public void postChatOpr  ( AegwChatOprReq req, AsyncCallback<?> asyncCallback);
  

  }
