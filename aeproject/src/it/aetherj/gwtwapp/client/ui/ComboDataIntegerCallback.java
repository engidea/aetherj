/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import java.util.ArrayList;

import it.aetherj.gwtwapp.client.beans.AegwComboRes;

/**
 * You may wish to extend this one if you want to do something once the data arives
 */
public class ComboDataIntegerCallback extends RecliGenericCallback
  {
  ComboBoxModel model; 
  ComboBoxKeyValue extraValue=null;
	ArrayList<? extends ComboBoxKeyValue> keyValueList; 
	
  public ComboDataIntegerCallback ( ComboBoxModel model )
    {
    this.model = model;    
    }

  public ComboDataIntegerCallback ( ComboBoxModel model, ComboBoxKeyValue extraValue )
    {
    this.model = model;    
		this.extraValue = extraValue;
    }


  public ComboDataIntegerCallback ( ComboBoxModel model, ComboBoxKeyValue extraValue, ArrayList<? extends ComboBoxKeyValue> keyValueList )
    {
    this.model = model;    
		this.extraValue = extraValue;
		this.keyValueList = keyValueList;
    }


  public void onSuccess(Object result)
    { 
    AegwComboRes res = (AegwComboRes)result;

    if ( isErrorMessage(res)) return;
    
		model.clearKeyValueList();
		model.addKeyValueList(keyValueList);        // if given add the list
    model.addKeyValueList(res.integerRowList);  // then the actual result
		model.addKeyValue(extraValue);              // then an extra default value
		model.refreshCombo();                       // finally refresh all of it
    }
  }
