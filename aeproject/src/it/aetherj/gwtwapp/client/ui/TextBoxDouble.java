/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.TextBox;

/**
 * This allows me to edit a Double using a current box
 */
public class TextBoxDouble extends TextBox
  {
  private Double cur_value;
  
  public TextBoxDouble()
    {
    addStyleName("double-edit-box"); 
    }
  
  public TextBoxDouble( int visible_length, ChangeHandler changeHandler )
		{
		this(visible_length);

		if ( changeHandler != null )
			super.addChangeHandler(changeHandler);
		}
	
  public TextBoxDouble( int visible_length )
		{
		this();
		setVisibleLength(visible_length);	
		}

  public Double getDoubleValue(Double defval)
		{
    try
      {
      cur_value = Double.parseDouble(getText());
      }
    catch ( Exception exc )
      {
      cur_value = defval;
      }
    
    // give feedback
    setDoubleValue(cur_value);
    
    return cur_value;  
		}


  public Double getDoubleValue()
    {
		return getDoubleValue(null);
    }
  
  public void setDoubleValue(Double value)
    {
    cur_value = value;
    
    String risul = "";
    
    if ( cur_value != null ) risul = value.toString();

    setText(risul);
    }

  }
