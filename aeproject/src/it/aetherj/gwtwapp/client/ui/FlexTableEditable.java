/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;

import it.aetherj.gwtwapp.client.gui.AecliGenericRootPage;

/**
 * The idea is that I can use this one to make nice grids
 * Grids that normally do not resize
 * The idea is also that I have label cells and data cells
 */
public class FlexTableEditable extends FlexTable
  {
  private static final String classname="FlexTableEditable.";

  private final AecliGenericRootPage parent;
  
  public FlexTableEditable ( AecliGenericRootPage parent )
    {
    // this is needed to manage label translation
    this.parent = parent;    
    }

  public void setLabelCell ( int row, int col, String value )
    {
    setText(row,col,parent.getLabel(value));      
    getCellFormatter().addStyleName(row,col,"flex-table-label");
    }
  
  public void setWidget ( int row, int col, Widget widget, int colspan, int rowspan )
    {
    setWidget(row,col,widget);
    widget.setWidth("100%");
    getFlexCellFormatter().setColSpan(row, col, colspan);      
    getFlexCellFormatter().setRowSpan(row, col, rowspan);      
    }

  /**
   * The idea is that this tries to get the widget at row/col and then set the content
   * @param row
   * @param col
   * @param value
   */
  public void setString ( int row, int col, String value )
    {
    Widget widget = getWidget(row,col);
    if ( widget == null )
      {
      Window.alert(classname+"setString: ERROR widget==null");
      return;
      }
    
    if ( widget instanceof HasValue )
      {
      HasValue<String> item = (HasValue<String>)widget;
      item.setValue(value);
      return;
      }
    
    
    //Window.alert(classname+"setString: NOTICE unsupported widget="+widget.getClass());
    }


  /**
   * Tries to get the data at the given row/col (in user data) as a string
   * @param row
   * @param column
   * @return
   */
  public String getString ( int row, int column )
    {
    Widget widget = getWidget(row,column);

    if ( widget == null ) return null;

    if ( widget instanceof HasValue )
      {
      HasValue<String> item = (HasValue<String>)widget;
      return item.getValue();
      }

    Window.alert(classname+"getString: NOTICE unsupported widget="+widget.getClass());
    return null;
    }

  }
