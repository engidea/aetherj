package it.aetherj.gwtwapp.client.ui;

import java.util.*;

import com.google.gwt.user.client.ui.TreeItem;

public class GwuiTreeIterator<T extends TreeItem>
  {
  private LinkedList<T>allchildren = new LinkedList<>();

  public GwuiTreeIterator(T root) 
    {
    addChildren(root);
    }

  /**
   * It is assumed that the item has already been added to the list, so, I just need to add the children
   * @param item
   */
  private void addChildren ( T item )
    {
    if ( item == null )
      return;
    
    int child_count = item.getChildCount();
    
    for (int index=0; index<child_count; index++)
      {
      T child = (T)item.getChild(index);

      // add this child to the list
      allchildren.add(child);
      
      // now recurse this child to pick up children
      addChildren(child);
      }
    }
  
  public Iterator<T>iterator()
    {
    return allchildren.iterator();
    }
  }

  
