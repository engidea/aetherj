/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.event.dom.client.ClickHandler;

/**
 * This button just provides a way to represent the boolean true, false, null
 * Normally the button is create in the false state 
 */
public class BooleanButton extends IconButton
  { 
  private final BooleanImageProvider provider;
  private Boolean currentValue;
  
  public BooleanButton(BooleanImageProvider images, ClickHandler handler)
    {
    super();
    
    provider = images;
    
    addClickHandler(handler);
    
    setValue(Boolean.FALSE);
    }
  
  public Boolean getValue ()
    {
    return currentValue;      
    }
  
  public void setValue ( Boolean newValue )
    {
    // no change they are both null
    if ( newValue == null && currentValue == null ) return;
    
    // no change they are both not null and same value
    if ( newValue != null && currentValue != null && newValue.equals(currentValue)) return;
    
    currentValue = newValue;
    
    setIcon(provider.getImage(currentValue));
    }
  }
