/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.user.client.ui.*;


/**
 * It is useful to wrap the default class into a utility one to be specific on where and how it is used
 * Generally speaking you must NOT wrap this panel with a ScrollPanel but you should wrap the subcomponents
 * This provides a generic way to "set" the center widget of a docking panel
 */
public final class DockPanelSplit extends SplitLayoutPanel
  {
  private Widget curCenterWidget=null;

  public DockPanelSplit()
    {
    super(4);
    }

  public DockPanelSplit(int split_size)
    {
    super(split_size);
    }

  /**
   * You MUST call this one last and it will be the panel that can be replaced
   * @param widget
   */
  public void setCenter ( Widget widget )
    {
    if ( widget == null ) return;
    
    if ( curCenterWidget != null ) 
      super.remove(curCenterWidget);
    
    super.add(widget);
    
    curCenterWidget = widget;
    }
  
  public void setCenter ( GwtWidgetProvider provider )
    {
    if ( provider == null ) return;
    
    setCenter ( provider.getPanelToDisplay());
    }
  
  
  
  }
