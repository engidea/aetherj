/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;


import com.google.gwt.cell.client.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.*;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.client.SafeHtmlTemplates.Template;
import com.google.gwt.safehtml.shared.*; 

 
public class IntegerInputCell extends AbstractEditableCell<Integer, IntegerInputCell.ViewData>
	{
	interface Template extends SafeHtmlTemplates
		{
		@Template("<input type=\"text\" value=\"{0}\" tabindex=\"-1\"></input>")
		SafeHtml input(String value);
		}


static class ViewData
	{
	private boolean isEditing;
	private boolean isEditingAgain;
	private Integer original;
	private Integer current;


	public ViewData(Integer anInteger)
		{
		this.original = anInteger;
		this.current = anInteger;
		this.isEditing = true;
		this.isEditingAgain = false;
		}


	@Override
	public boolean equals(Object o)
		{
		if (o == null)	return false;

		ViewData vd = (ViewData)o;
		return equalsOrBothNull(original, vd.original) &&
				 equalsOrBothNull(current, vd.current) && isEditing == vd.isEditing &&
				 isEditingAgain == vd.isEditingAgain;
		}


	public Integer getOriginal()
		{
		return original;
		}


	public Integer getCurrent()
		{
		return current;
		}


	@Override
	public int hashCode()
		{
		return original.hashCode() + current.hashCode() +
				 Boolean.valueOf(isEditing).hashCode() * 29 +
				 Boolean.valueOf(isEditingAgain).hashCode();
		}


	public boolean isEditing()
		{
		return isEditing;
		}


	public boolean isEditingAgain()
		{
		return isEditingAgain;
		}


	public void setEditing(boolean isEditing)
		{
		boolean wasEditing = this.isEditing;
		this.isEditing = isEditing;


		// This is a subsequent edit, so start from where we left off.
		if (!wasEditing && isEditing)
			{
			isEditingAgain = true;
			original = current;
			}
		}


	public void setCurrent(Integer anInteger)
		{
		this.current = anInteger;
		}


	private boolean equalsOrBothNull(Object o1, Object o2)
		{
		return (o1 == null) ? o2 == null : o1.equals(o2);
		}
	}


	private static Template template;
	private final IntegerHtmlRenderer renderer;


	public IntegerInputCell()
		{
		this(IntegerHtmlRenderer.getInstance());
		}


	public IntegerInputCell(IntegerHtmlRenderer renderer)
		{
		super("click", "keyup", "keydown", "blur");
		
		if (template == null)	template = GWT.create(Template.class);

		if (renderer == null)	throw new IllegalArgumentException("renderer == null");

		this.renderer = renderer;
		}


	@Override
	public boolean isEditing(Context context, Element parent, Integer value)
	{
		ViewData viewData = getViewData(context.getKey());
		return viewData == null ? false : viewData.isEditing();
	}


	@Override
	public void onBrowserEvent(Context context, Element parent, Integer value, NativeEvent event, ValueUpdater<Integer> valueUpdater)
		{
		Object key = context.getKey();
		ViewData viewData = getViewData(key);

		if (viewData != null && viewData.isEditing())
			{
			// Handle the edit event.
			editEvent(context, parent, value, viewData, event, valueUpdater);
			}
		else
			{
				String type = event.getType();
				int keyCode = event.getKeyCode();
				boolean enterPressed = "keyup".equals(type) && keyCode == KeyCodes.KEY_ENTER;
				if ("click".equals(type) || enterPressed)
					{
						// Go into edit mode.
						if (viewData == null)
							{
								viewData = new ViewData(value);
								setViewData(key, viewData);
							}
						else
							{
								viewData.setEditing(true);
							}
						edit(context, parent, value);
					}
			}
	}


	@Override
	public void render(Context context, Integer value, SafeHtmlBuilder sb)
		{
		// Get the view data.
		Object key = context.getKey();
		ViewData viewData = getViewData(key);
		if (viewData != null && !viewData.isEditing() && value != null &&	value.equals(viewData.getCurrent()))
			{
				clearViewData(key);
				viewData = null;
			}


		if (viewData != null)
			{
				Integer text = viewData.getCurrent();
				SafeHtml html = renderer.render(text);
				if (viewData.isEditing())
					{
						// Note the template will not treat SafeHtml specially
						sb.append(template.input(html.asString()));
					}
				else
					{
						// The user pressed enter, but view data still exists.
						sb.append(html);
					}
			}
		else if (value != null)
			{
				SafeHtml html = renderer.render(2);
				sb.append(html);
			}
	}


	@Override
	public boolean resetFocus(Context context, Element parent, Integer value)
	{
		if (isEditing(context, parent, value))
			{
				getInputElement(parent).focus();
				return true;
			}
		return false;
	}


	/**
	 * Convert the cell to edit mode.
	 *
	 * @param context the {@link Context} of the cell
	 * @param parent the parent element
	 * @param value the current value
	 */
	protected void edit(Context context, Element parent, Integer value)
	{
		setValue(context, parent, value);
		InputElement input = getInputElement(parent);
		input.focus();
		input.select();
	}


	/**
	 * Convert the cell to non-edit mode.
	 *
	 * @param context the context of the cell
	 * @param parent the parent Element
	 * @param value the value associated with the cell
	 */
	private void cancel(Context context, Element parent, Integer value)
	{
		clearInput(getInputElement(parent));
		setValue(context, parent, value);
	}


	/**
	 * Clear selected from the input element. Both Firefox and IE fire spurious
	 * onblur events after the input is removed from the DOM if selection is not
	 * cleared.
	 *
	 * @param input the input element
	 */
	private native void clearInput(Element input) /*-{
    if (input.selectionEnd)
      input.selectionEnd = input.selectionStart;
    else if ($doc.selection)
      $doc.selection.clear();
  }-*/;


	/**
	 * Commit the current value.
	 * @param context the context of the cell
	 * @param parent the parent Element
	 * @param viewData the {@link ViewData} object
	 * @param valueUpdater the {@link ValueUpdater}
	 */
	private void commit(Context context, Element parent, ViewData viewData,	ValueUpdater<Integer> valueUpdater)
		{
		Integer value = updateViewData(parent, viewData, false);
		clearInput(getInputElement(parent));
		setValue(context, parent, viewData.getOriginal());
		
		if (valueUpdater != null)
			{
			valueUpdater.update(value);
			}
		}


	private void editEvent(Context context, Element parent, Integer value,	 ViewData viewData, NativeEvent event, ValueUpdater<Integer> valueUpdater)
	{
		String type = event.getType();
		boolean keyUp = "keyup".equals(type);
		boolean keyDown = "keydown".equals(type);
		if (keyUp || keyDown)
			{
				int keyCode = event.getKeyCode();
				if (keyUp && keyCode == KeyCodes.KEY_ENTER)
					{
						// Commit the change.
						commit(context, parent, viewData, valueUpdater);
					}
				else if (keyUp && keyCode == KeyCodes.KEY_ESCAPE)
					{
						// Cancel edit mode.
						Integer originalText = viewData.getOriginal();
						if (viewData.isEditingAgain())
							{
								viewData.setCurrent(originalText);
								viewData.setEditing(false);
							}
						else
							{
								setViewData(context.getKey(), null);
							}
						cancel(context, parent, value);
					}
				else
					{
						// Update the text in the view data on each key.
						updateViewData(parent, viewData, true);
					}
			}
		else if ("blur".equals(type))
			{
				// Commit the change. Ensure that we are blurring the input element and
				// not the parent element itself.
				EventTarget eventTarget = event.getEventTarget();
				if (Element.is(eventTarget))
					{
						Element target = Element.as(eventTarget);
						if ("input".equals(target.getTagName().toLowerCase()))
							{
								commit(context, parent, viewData, valueUpdater);
							}
					}
			}
	}


	/**
	 * Get the input element in edit mode.
	 */
	private InputElement getInputElement(Element parent)
	{
		return parent.getFirstChild().<InputElement>cast();
	}


	/**
	 * Update the view data based on the current value.
	 *
	 * @param parent the parent element
	 * @param viewData the {@link ViewData} object to update
	 * @param isEditing true if in edit mode
	 * @return the new value
	 */
	private Integer updateViewData(Element parent, ViewData viewData, boolean isEditing)
		{
		InputElement input = (InputElement)parent.getFirstChild();
		String value = input.getValue();
		Integer anint = Integer.valueOf(value);
		viewData.setCurrent(anint);
		viewData.setEditing(isEditing);
		return anint;
	}
}


