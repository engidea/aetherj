/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.safehtml.shared.*;
import com.google.gwt.text.shared.SafeHtmlRenderer;

public class IntegerHtmlRenderer implements SafeHtmlRenderer<Integer> 
	{
	private static IntegerHtmlRenderer instance;

  public static IntegerHtmlRenderer getInstance() {
    if (instance == null) {
      instance = new IntegerHtmlRenderer();
    }
    return instance;
  }


  private IntegerHtmlRenderer() {
  }


  public SafeHtml render(Integer object) {
    return SafeHtmlUtils.fromString(""+object);
  }


  public void render(Integer object, SafeHtmlBuilder appendable) {
    appendable.append(SafeHtmlUtils.fromString(""+object));
  }
}
