/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.user.client.ui.*;

/**
 * This is mildly deprecated for GwuiDockLayoutPanel
 * @see http://www.gwtproject.org/doc/latest/DevGuideUiPanels.html
 */
public final class GwuiDockPanel extends DockPanel
	{
	private Widget curCenterWidget=null;

	public GwuiDockPanel()
		{
		super();
		
    super.setWidth("100%");

    super.setHeight("100%");

    // Allow 4 pixels of spacing between each cell
    super.setSpacing(3);

//		super.setBorderWidth(1);   // for debug
		}
	
	public void addNorth ( Widget widget )	
		{
		super.add(widget, DockPanel.NORTH);			
		}

	public void addNorth ( GwtWidgetProvider panel_provider )
		{
		if ( panel_provider == null ) return;

		addNorth(panel_provider.getPanelToDisplay());			
		}


	 public void addSouth ( Widget widget )  
	    {
	    super.add(widget, DockPanel.SOUTH);     
	    }

	public void addDestra ( Widget widget )
		{
		super.add(widget, DockPanel.EAST);
		}
	
	public void addSinistra ( Widget widget )
		{
		if ( widget == null ) return;
		
		super.add(widget, DockPanel.WEST);
		}

	public void addSinistra ( Widget widget, String cell_width )
		{
		if ( widget == null ) return;
		
		super.add(widget, DockPanel.WEST);
		
		super.setCellWidth(widget, cell_width);
		}

	/**
	 * Alignment does not work as reliably as one would expect
	 * @param widget
	 * @param align_top
	 */
	public void setCenter ( Widget widget, boolean align_top )
		{
		if ( widget == null ) return;
		
		if ( curCenterWidget != null ) super.remove(curCenterWidget);
		
		super.add(widget, DockPanel.CENTER);
		
		curCenterWidget = widget;
		
		if ( align_top ) 
		  super.setCellVerticalAlignment(widget,  HasVerticalAlignment.ALIGN_TOP);
		}

	public void setCenter ( Widget widget )
		{
		setCenter ( widget, false);
		}
	
	public void setCenter ( GwtWidgetProvider panel_provider )
		{
		if ( panel_provider == null ) return;
		
		setCenter ( panel_provider.getPanelToDisplay() );
		}
	
	


	}
