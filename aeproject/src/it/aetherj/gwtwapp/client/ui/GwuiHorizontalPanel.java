package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.user.client.ui.*;

public class GwuiHorizontalPanel extends HorizontalPanel
  {

  /**
   * Example: risul.addValign(getGwtLabel("Search"),HasVerticalAlignment.ALIGN_MIDDLE);
   * @param widget
   * @param valign
   */
  public void addValign(Widget widget, VerticalAlignmentConstant valign )
    {
    super.add(widget);
    super.setCellVerticalAlignment(widget, valign);
    }
  
 
  }
