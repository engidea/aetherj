/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import it.aetherj.gwtwapp.client.beans.ServiceFeedback;

public abstract class RecliGenericCallback implements AsyncCallback<Object>
  {
  public final void onFailure(Throwable exc)
    {
    Window.alert(exc.toString()); 
    }

  protected boolean isErrorMessage ( ServiceFeedback feedback )
    {
    if ( feedback == null )
      {
      Window.alert("Server request FAILED: feedback==null");
      return true;
      }
    
    String errmsg = feedback.getEnglishErrorMessage();
    if ( errmsg == null || errmsg.length() < 1 ) return false;
    
    Window.alert("Server request FAILED: "+errmsg);
    return true;
    }

  }
