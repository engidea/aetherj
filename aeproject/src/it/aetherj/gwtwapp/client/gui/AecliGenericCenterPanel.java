/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.gui;

import java.util.Date;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.*; 


/**
 * ALl my pages have the top part with the jump to the help and a generic label
 * I wish to construct something that handles that
 */
public abstract class AecliGenericCenterPanel extends AecliGenericRootPage
  {
  private static final String classname="AecliGenericCenterPanel";
  
  public static final int northPanelHeight=60;
  
  private final DockPanelSplit workPanel;        // panel to put pages into the center 
  
  private final String help_tag,pageTitle;

  private Date activityEndDate;      // the last activity date received from server
  
  /**
   * Only subclasses should be able to call constructor
   * @param stat
   * @param pageTitle
   * @param help_tag
   */
  protected AecliGenericCenterPanel(RecliStat stat, String page_desc, String help_tag )
    {
    super(stat);
    
    this.pageTitle = page_desc;
    this.help_tag = help_tag;
    
    workPanel = newWorkPanel();
    }
  
  public String getHelpTag ()
    {
    return help_tag;
    }
  
  public String getPageTitle ()
    {
    return pageTitle;
    }
  
  /**
   * In some way the caller knows that there is a last activity date referred to this "page"
   * It then ask to this page if the given date is newer than the one stored
   * This call also stores the given date as the current one
   * @param activityEndDate should not be null, if it is return will be false
   * @return true if the given date is newer than current stored one
   */
  public boolean isActivityDateLater ( Date lastActivityDate_in )
  	{
  	// It just means that the server never has made a request
  	if ( lastActivityDate_in == null ) return false;
  	
  	if ( this.activityEndDate == null )
  		{
  		this.activityEndDate = 	lastActivityDate_in;
  		// statistically speaking it is likely that the first time there is a new info the page is already "refreshed"
      stat.logOnBrowser(classname+"isActivityDateLater: lastAcivityDate was null");
      // so, it should be OK not to refresh
  		return false;
  		}
  	
  	long last_in = lastActivityDate_in.getTime();
  	long last_have = activityEndDate.getTime();
  	
  	// I can now store the received value
  	this.activityEndDate = 	lastActivityDate_in;

  	boolean risul = last_in > last_have;

//  	stat.logOnBrowser(classname+"isActivityDateLater: lastAcivityDate 'risul="+risul);
  	
  	return risul;
  	}
  
  
  /**
   * subclasses should return a valid ident .... hmmmm, I have to actually send the ident down
   * TODO: this should be clarified...
   * @return
   */
  public String getGwtIdent ()
  	{
  	return null;	
  	}

  
	protected void verifySaveGetallButtonVisible ()
		{
		}

  private HorizontalPanel newTopButtonPanel ()
    {
    HorizontalPanel risul = new HorizontalPanel();
    risul.addStyleName("generic-top-button-panel");
    return risul;
    }
  
  
  /**
   * You can use this one to parse the call end date and decide what to do next...
   */
  protected void parseCallEndDate (ServiceFeedback feedback)
    {
    if ( feedback == null ) return;
    
    Date callEndDate = feedback.getCallEndDate();
    
    if ( callEndDate == null ) return;
    
    if ( activityEndDate == null ) 
      activityEndDate = callEndDate;
    }


  /**
   * Creates a Button that will refresh the page once clicked.
   * @return
   */
  protected PushButton newRefreshButton ()
    {
    RefreshButtonListener listener = new RefreshButtonListener();
    PushButton risul = stat.utils.newRefreshButton(listener);
    return risul;
    }

  private DockPanelSplit newWorkPanel()
    {
    DockPanelSplit risul = new DockPanelSplit(4);

    // you will need to add the north panel using the 
		// center panel will be added later

    return risul;
    }

  /**
   * Anybody should be able to request that the page ask for data refresh from the server
   * this will start a non blocking request whose response will be handled later.
   */
  public abstract void postServerRefreshReq ();
  
  /**
   * Anybody should be able to ask for the gui page refresh, it should update the 
   * GUI with the data currently held in the implementation model.
   * If the data model is empty then it should trigger a postServerRefreshReq();
   */
  public abstract void postGuiRefreshReq ();
  
  /**
   * Anybody should be able to clear the GUI, actually clearing the GUI means clearing the model
   * and then refresh the content. But the implementation can be done in any way you wish
   */
  public abstract void guiClear();

  /**
   * Allow to set the inner center panel
   */
  protected void setInnerCenterPanel(Widget centerWidget)
    {
    workPanel.setCenter(centerWidget);
    }
  
  protected void setInnerCenterPanel(GwtWidgetProvider panelProvider)
    {
    workPanel.setCenter(panelProvider);
    }

  /**
   * Set the widget that should be on the north panel
   */
  protected void addNorth( Widget widget, double size )
    {
    workPanel.addNorth(widget, size);
    }

  public final void postComboIntegerDataRefresh ( AegwComboReq req, RecliGenericCallback callback )
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    req.combo_key_type = AegwComboReq.key_type_integer;
    service.getComboList(req, callback);
    }
 
  @Override
  public Panel getPanelToDisplay()
    {
    return workPanel;
    }
  

private class HelpButtonListener implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    Window.open(stat.serviceFactory.getPagehelpUrl(help_tag),"_blank","");
    }
  }


private class RefreshButtonListener implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    postServerRefreshReq();
    }
  }





  }
