/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.gui;

import com.google.gwt.user.client.ui.Image;

import it.aetherj.gwtwapp.client.RecliStat;

public final class AecliDummyPage extends AecliGenericCenterPanel
  {
  private static final String classname="AecliDummyPage.";
  
  public AecliDummyPage(RecliStat stat)
    {
    super(stat,"Dummy Page","impianti-list-help");
    }

  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  @Override
  public Image newPageButtonImage ()
    {
    return new Image(stat.imageBoundle.newForward());
    }
  
  public boolean isMenuTreeItemVisible()
    {
    // this menu item is visible when there is no impianto selected
    return false;
    }
  

  public void guiClear()
    {
    }

  public void postGuiRefreshReq ()
    {
    }

  	
  public void postServerRefreshReq()
    {
    }



  }  // END MAIN CLASS
