/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.gui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.HTMLTable.CellFormatter;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.AegwLoginRes;
import it.aetherj.gwtwapp.client.services.LoginServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;

 

public final class AecliLoginPage extends AecliGenericRootPage
  {
  private static final String classname="AecliLoginPage";
  
  private final LoginCallback  loginCallback = new LoginCallback();
  
  private final Panel   workPanel;
  
  private final TextBox  loginUsername;
  private final Button   loginButton;
  private final PasswordTextBox loginPassword;

  public AecliLoginPage(RecliStat stat)
    {
    super(stat);
    
    loginUsername = new TextBox();
    loginPassword = new PasswordTextBox();

    loginButton = new Button(getLabel("Login"));
    loginButton.addClickHandler(new LoginButtonListener());
    
    workPanel = newWorkPanel();
    }
  
  
  public void clearForm ()
    {
    loginUsername.setText("");
    loginPassword.setText("");
    }
  
  @Override
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }
  
  private Panel newWorkPanel()
    {
    GwuiGrid risul = new GwuiGrid(1,1);
    risul.addStyleName("login-grid");
    
    risul.setWidget(0, 0,newCenterPanel());
    
    HTMLTable.CellFormatter formatter = risul.getCellFormatter();
    formatter.setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_CENTER);
    formatter.setVerticalAlignment(0, 0, HasVerticalAlignment.ALIGN_MIDDLE);    
    
    return risul;
    }

  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }

  /**
   * One panel is the image, the other is the actual login page
   * @return
   */
  private Panel newCenterPanel ()
    {
		GwuiGrid risul = new GwuiGrid(2,1);
    risul.addStyleName("login-grid-vertical");
    
    risul.setWidget(0, 0, new Image(GWT.getHostPageBaseURL()+"images/logo-134x46.png"));
    CellFormatter frmt = risul.getCellFormatter();
    frmt.addStyleName(0, 0, "login-grid-logo");
    
    HorizontalPanel loginPanel = new HorizontalPanel();
    
//    loginPanel.add(new Image(GWT.getHostPageBaseURL()+"logo-134x46.gif"));
    loginPanel.add(newInputPanel());

		risul.setWidget(1, 0, loginPanel);		
		
    return risul;
    }


  private VerticalPanel newInputPanel ()
    {
    VerticalPanel risul = new VerticalPanel();

    risul.add(newGwtLabelBold("Login Aether User"));
    risul.add(getGwtLabel("Insert Username/Password and press Login button"));
    risul.add(newLoginInput());

    return risul;    
    }
  
  private Grid newLoginInput ()
    {
    GwuiGrid risul = new GwuiGrid(4,2);  // it is rows, columns
    
    int rowIndex=0;
    int colIndex=0;
        
    risul.setText(rowIndex,colIndex++, getLabel("Username"));
    risul.setWidget(rowIndex,colIndex++,loginUsername);
    
    rowIndex++; colIndex=0;
    
    risul.setText(rowIndex,colIndex++, getLabel("Password"));
    risul.setWidget(rowIndex,colIndex++,loginPassword);

    rowIndex++; colIndex=0;

    risul.setWidget(rowIndex,1,loginButton);
    
    return risul;
    }

  public Panel getPanelToDisplay()
    {
    return workPanel;
    }


  
private final class LoginButtonListener implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    LoginServiceAsync service = stat.serviceFactory.newLoginService();
    
    String  login      = loginUsername.getText();
    String  password   = loginPassword.getText();
    
    // prova
    
    service.authenticateWebUser(login, password, loginCallback);
    }
  }




private final class LoginCallback  extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwLoginRes loginres = (AegwLoginRes)result;
    
    if ( isErrorMessage(loginres)) return;

    // save the wsession for future use
    stat.login_res = loginres;
    
    if ( stat.loggedinPage == null )
      {
      // create the rest of the environment, if needed
      stat.loggedinPage = new AecliLoggedinPage(stat);
      }

    // check navigation buttons visibility from previous login
    stat.navigationButtons.checkVisibility();
    // start the ping timer in the status bar
    stat.statusBar.timerStart();

    // this will cover the whole page
    stat.setRootCanvas(stat.loggedinPage);

    // sets the initial page after login
    stat.loggedinPage.setCenterPanel(stat.boardList);
    stat.boardList.postServerRefreshReq();
    }
  }
  
  
  }
