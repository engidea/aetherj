/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.gui;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.*;
import com.google.gwt.user.client.ui.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.services.LoginServiceAsync;
import it.aetherj.gwtwapp.client.ui.RecliGenericCallback;
import it.aetherj.protocol.MimPowValue;

/**
 * This holds the two bars at the top of the page.
 * One of the BIG issues is to align things properly....
 * It seems that the only safe way is use Grid, as much as possible....
 */
public final class AecliStatusBar extends AecliGenericRootPage 
  {
  private static final String classname="AecliStatusBar.";

  private final ButtonsListener buttonsListener = new ButtonsListener();
  
  private final Label boardLabel;         // when navigating between Boards-Threads-Post show
  private final Label threadLabel;        // when navigating between Boards-Threads-Post show
  private final Label powLabel;           // 
  private final Label pageTitleLabel;     // shows to user what panel is currently selected
  
  private final Timer connectionCheckTimer;
  private final ConnectionCheckCallback connectionCheckCallback = new ConnectionCheckCallback();

  private final PushButton refreshBtn,user_btn;
  private final PushButton logoff_btn,helpButton;
  private final Image hearthbeat;

  private final Panel workPanel;
  
  private AegwBoard  aegwBoard  = new AegwBoard();   // do NOT let it become null
  private AegwThread aegwThread = new AegwThread();  // do NOT let it become null
  private AegwPost   aegwPost   = new AegwPost();    // do NOT let it become null
  
  private boolean    heartbright=false;
  
  private int powStep;
  private int powPerSecond;
  
  
  /**
   * Constructor
   * @param stat
   */
  public AecliStatusBar(RecliStat stat)
    {
    super(stat);

    pageTitleLabel      = newGwtLabelBold();

    boardLabel = newLabelClick("statusbar-boardLabel");
    threadLabel = newLabelClick("statusbar-threadLabel");

    powLabel = new Label();
    powLabel.addStyleName("statusbar-powLabel");

    hearthbeat = new Image(stat.imageBoundle.newHeart());
    hearthbeat.addClickHandler(buttonsListener);

    helpButton     = stat.utils.newHelpButton(buttonsListener);

    user_btn       = stat.utils.newEditButton(buttonsListener);
    refreshBtn    = stat.utils.newRefreshButton(buttonsListener);
    logoff_btn     = stat.utils.newLogoffButton(buttonsListener);

    workPanel = newWorkPanel();
    
    connectionCheckTimer = new ConnectionCheckTimer();
    }
  
  private Label newLabelClick ( String style )
    {
    Label label = new Label();
    label.addStyleName(style);
    label.addClickHandler(buttonsListener);
    return label;
    }
  
  
  public void timerStart()
    {
    connectionCheckTimer.schedule(1000); // in ms, following schedule will happen when the message is Rx
    }

  @Override
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }

  public void postPowFeedback(MimPowValue powValue)
    {
    if ( powValue == null )
      {
      powStep=0;
      powPerSecond=0;
      }
    else
      {
      powStep=powValue.getStep();
      powPerSecond=powValue.getPowPerSecond();
      }

    refresh();
    }

  
  private String getCurrentPageTitle ()
    {
    AecliGenericCenterPanel cpage = stat.loggedinPage.getCurrentCenterPage();

    return cpage != null ? cpage.getPageTitle() : "No page selected";
    }
        
  private String getPowDesc ()
    {
    if ( powStep <= 0 ) return "";

    StringBuilder risul = new StringBuilder(80);
    risul.append("[");
    risul.append(powStep);
    risul.append(',');
    risul.append(powPerSecond);
    risul.append("]");

    return risul.toString();
    }
  
  private void refresh ()
    {
    pageTitleLabel.setText(getCurrentPageTitle());
    boardLabel.setText(aegwBoard.getBoardNameBegin(30));
    threadLabel.setText(aegwThread.getThreadNameBegin(30));
    powLabel.setText(getPowDesc());
    }
  
  
  
  public void setRefreshButtonVisible ( boolean visible )
    {
    refreshBtn.setVisible(visible);
    }
  
  private Panel newRightsidePanel()
    {
    HorizontalPanel risul = new HorizontalPanel();
    
    risul.add(helpButton);
    risul.add(hearthbeat);
    risul.add(user_btn);
    risul.add(logoff_btn);
  
    return risul;
    }
  
  private Panel newLeftsidePanel()
    {
    HorizontalPanel risul = new HorizontalPanel();
    
    risul.add(refreshBtn);
    
    risul.add(pageTitleLabel);
    risul.setCellVerticalAlignment(pageTitleLabel, HasVerticalAlignment.ALIGN_MIDDLE);

    risul.add(boardLabel);
    risul.setCellVerticalAlignment(boardLabel, HasVerticalAlignment.ALIGN_MIDDLE);

    Label spacer = new Label(" ");
    spacer.setWidth("1em");
    risul.add(spacer);

    risul.add(threadLabel);
    risul.setCellVerticalAlignment(threadLabel, HasVerticalAlignment.ALIGN_MIDDLE);

    risul.add(powLabel);
    risul.setCellVerticalAlignment(threadLabel, HasVerticalAlignment.ALIGN_MIDDLE);

    return risul;
    }

  
  private Panel newWorkPanel() 
    {
    HorizontalPanel risul = new HorizontalPanel();
    risul.setWidth("100%");
    
    risul.getElement().setAttribute("id", "status-bar-id");

    Panel panel = newLeftsidePanel(); 
    risul.add(panel);
    
    panel = newRightsidePanel(); 
    risul.add(panel);
    risul.setCellHorizontalAlignment(panel, HasHorizontalAlignment.ALIGN_RIGHT);

    return risul;
    }
  
  /**
   * It is legal to pass null to signal that a current board is not selected
   * When this happens
   */
  public void setSelectedBoard ( AegwBoard board )
    {
    // if nothing changes... do nothing
    if ( aegwBoard.equals(board)) return;
    
    aegwBoard=board != null ? board : new AegwBoard();
    
    aegwThread=new AegwThread();
    aegwPost=new AegwPost();
    
    refresh();
    }
  
  public AegwBoard getBoard ()
    {
    return aegwBoard;
    }

  public int getBoardId ()
    {
    return aegwBoard.board_id;
    }

  public void setSelected ( AegwBoard board, AegwThread thread )
    {
    aegwBoard =board  != null ? board : new AegwBoard();
    aegwThread=thread != null ? thread : new AegwThread();
    }

  /**
   * It is legal to pass null to signal that a current thread is not selected
   */
  public void setSelectedThread ( AegwThread thread )
    {
    aegwThread=thread != null ? thread : new AegwThread();
    
    refresh();
    }
  
  public AegwThread getThread ()
    {
    return aegwThread;
    }
  
  public int getThreadId ()
    {
    return aegwThread.thread_id;
    }

  public void setSelected ( AegwBoard board, AegwThread thread, AegwPost post )
    {
    aegwBoard =board  != null ? board  : new AegwBoard();
    aegwThread=thread != null ? thread : new AegwThread();
    setSelected(post);
    }

  public void setSelected ( AegwPost post )
    {
    aegwPost=post != null ? post : new AegwPost();
    }

  public AegwPost getPost ()
    {
    return aegwPost;
    }
  
  public Panel getPanelToDisplay()
    {
    return workPanel;
    }

  private void logoff_fun ()
    {
    // stop current timer polling
    connectionCheckTimer.cancel();
    
    // Clear previous login information
    stat.loginPage.clearForm();
    
    // show login page
    stat.setRootCanvas(stat.loginPage);
    }

  private void showHelpPage ()
    {
    AecliGenericCenterPanel cpage = stat.loggedinPage.getCurrentCenterPage();
    if ( cpage == null )
      {
      stat.logOnBrowser(classname+".showHelpPage: null center page");
      return;
      }

    String help_tag = cpage.getHelpTag();
    if ( help_tag == null )
      {
      stat.logOnBrowser(classname+".showHelpPage: null help_tag");
      return;
      }

    Window.open(stat.serviceFactory.getPagehelpUrl(help_tag),"_blank","");
    }
  


	/**
	 * if a loggedinpage is defined returns the  ident that we are currently in so I can ask for status info
	 * @return
	 */
	private String getActivityGwtIdent()
		{
		AecliLoggedinPage alo = stat.loggedinPage; 			

		if ( alo == null ) 
		  return null;
		
		AecliGenericCenterPanel apro = alo.getCurrentCenterPage();
		
		if ( apro == null ) 
		  return null;
		
//		stat.logOnBrowser("getActivityGwtIdent: apro.class="+apro.getClass().getName());
		
		return apro.getGwtIdent();
		}

	/**
	 * let me see if the current page reflects the returned info, if any
	 * If so then ask for refresh if the returned modification date is later than the stored date
	 * TODO it should be noted that the info should go in the page.... so I probably have to extend root page...
	 * @param res
	 */
  private void parseActivityStatusRes ( AegwActivityStatus  res )
  	{
  	// nothing to do if there is no res
  	if ( res == null ) return;	
  		
  	String gwtIdent_in = res.gwtIdent;
  	
//  	stat.logOnBrowser(classname+"parseActivityStatusRes: res="+res);
  	
  	// also if there is no ident...
  	if ( gwtIdent_in == null ) return;
  		
		AecliLoggedinPage alo = stat.loggedinPage; 			

		// nothing to do if there is no logged in page (maybe possible ?)
		if ( alo == null ) return;
		
		AecliGenericCenterPanel apro = alo.getCurrentCenterPage();
  	
		// no provider for root page ?
		if ( apro == null ) return;
  
		String want_gwtIdent = apro.getGwtIdent();
		
		if ( want_gwtIdent == null )
			{
			// this is generally strange, I did a request for a gwt ident and now I do not have it anymore ???
			stat.logOnBrowser(classname+"parseActivityStatusRes: res="+res+" but want_gwtIdent==null");
			return;
			}
			
		// if the returned ident does not belong to the current page
		if ( ! gwtIdent_in.equals(want_gwtIdent)) return;
  		
//    stat.logOnBrowser(classname+"parseActivityStatusRes: res="+res+" match apro");
    
		// the given activity date is no later than the one stored 
		if ( ! apro.isActivityDateLater(res.updateDate)) return;
  		
		// ask for gui to refresh
    apro.postServerRefreshReq();
  	}
	
private final class ConnectionCheckTimer extends Timer
  {
  public void run()
    {
    LoginServiceAsync service = stat.serviceFactory.newLoginService();
    ConnectionStatusReq req = new ConnectionStatusReq(stat);
    
    req.setActivity( getActivityGwtIdent());
    
    service.getConnectionStatus(req, connectionCheckCallback);
    }
  }

private final class ConnectionCheckCallback  extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwbConnectionStatus conn_status = (AegwbConnectionStatus)result;

    if ( isErrorMessage(conn_status)) return;

    stat.sysinfoPanel.refreshConnectionStatus(conn_status);
    
    if ( heartbright = ! heartbright )
      hearthbeat.setResource(stat.imageBoundle.newHeart());
    else
      hearthbeat.setResource(stat.imageBoundle.newHeartBright());
    		
		parseActivityStatusRes ( conn_status.activityStatus);
		
		refresh();
		
    // normally reschedule in two seconds
    int rescheduleMilliseconds = 2000;
        
    // I could slow things down if nothing is happening...
    connectionCheckTimer.schedule(rescheduleMilliseconds); 
    }
  }

private final class ButtonsListener implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    Object source = event.getSource();
    
    if ( source == logoff_btn ) 
      logoff_fun ();
    else if ( source == hearthbeat ) 
      stat.loggedinPage.setCenterPanel(stat.sysinfoPanel);
    else if ( source == user_btn )
      {
      stat.loggedinPage.setCenterPanel(stat.userEditPanel);
      stat.userEditPanel.postGuiRefreshReq();
      }
    else if ( source == refreshBtn )
      stat.loggedinPage.postServerRefreshReq();
    else if ( source == helpButton )
      showHelpPage();
    else if ( source == boardLabel )
      {
      stat.loggedinPage.setCenterPanel(stat.boardList);
      stat.boardList.postGuiRefreshReq();
      }
    else if ( source == threadLabel )
      {
      stat.loggedinPage.setCenterPanel(stat.threadList);
      stat.threadList.postGuiRefreshReq();
      }
    }
  }



  }  // END MAIN CLASS
