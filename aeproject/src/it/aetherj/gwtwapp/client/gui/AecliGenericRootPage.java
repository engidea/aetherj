/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.gui;

import java.util.Date;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.ui.*;

/**
 * The basic idea is that in many of the sub pages I am sharing code
 * So I put the shared code in here.
 */
public abstract class AecliGenericRootPage implements GwtWidgetProvider
  {
  protected final RecliStat stat; 
  
  public static final String style_itemLabelBold = "itemLabelBold";
  public static final String style_image_as_button = "image-as-button";
  
  public AecliGenericRootPage(RecliStat stat)
    {
    this.stat = stat;
    }

  /**
   * Subclasses can decide if this page is visible on the menu tree based on a logic
   * that they choose. NOTE that they must override this method if they chose so.
   * @return
   */
  public boolean isMenuTreeItemVisible ()
    {
    return true;
    }

  /**
   * Subclasses should declare what is the page button image to be used for the page
   * @return
   */
  public abstract Image newPageButtonImage ();
    
  protected String getDateAsDmyhms (Date date )
    {
    return stat.formatter.getDateAsDmyhms(date);
    }
  
  protected String getDateShort ( Date date )
    {
    return stat.formatter.getDateShort(date);
    }
  
  protected String getUintString ( Integer avalue )
    {
    if ( avalue == null ) return "";
    
    int anint = avalue.intValue();
    
    if ( anint >= 0 ) return Integer.toString(anint);
    
    // here the value is negative and to have the original value I need to do 
    // and then get the four bytes, as it should have been from the beginning
    long risul = anint & 0xFFFFFFFFL;
    
    return Long.toString(risul);
    }
    
	public DateBox newDateBox (ValueChangeHandler<Date> changeHandler)
		{
    DateBox.DefaultFormat format = stat.formatter.newDateBoxFormatDateShort();

    DateBox risul = new DateBox();
    risul.setFormat(format);
    risul.setValue(new Date());
		risul.setWidth("6em");
		
		if ( changeHandler != null ) 
		  risul.addValueChangeHandler(changeHandler);
		
		return risul;
		}
	
	/**
	 * Returns a grid that can be used to put buttons in it, mostly here to have a single place for the css name
	 * @param col_count
	 * @return
	 */
	protected GwuiGrid newButtonsGrid ( int col_count )
		{
		GwuiGrid risul = new GwuiGrid(1,col_count);
		risul.addStyleName("buttons-grid");

		return risul;
		}
	
  public String formatDouble ( Double valie )
    {
    return stat.formatter.formatDecimal(valie);  
    }
  
  public Label getGwtLabel ( String label_english )
    {
    return new Label(getLabel(label_english));      
    }

  

  


  public final DateBox newDateBox()
    {
    DateBox.DefaultFormat format = stat.formatter.newDateBoxFormatDateShort();
    DateBox risul = new DateBox();
    risul.setFormat(format);
    return risul;
    }


  public final TextBox newTextBox ( int visible_length )
    {
    TextBox risul = new TextBox();
    risul.setVisibleLength(visible_length);
    return risul;
    }

  public final Label newLabelSpacer (String width)
    {
    Label risul = new Label();
    risul.setWidth(width);
    return risul;
    }
  
  /**
   * NOTE the check class is NOT called when focus is lost due to mouse click on something not a text box use the one with keypress to catch that
	 */
  public final TextBox newTextBox ( int visible_length, ChangeHandler checkCorrectness )
		{
		TextBox risul = newTextBox(visible_length);

		if ( checkCorrectness != null ) 
		  risul.addChangeHandler(checkCorrectness);

		return risul;
		}

  /**
   * @param visible_length
   * @param keypress a class called when a key is pressed, it is NOT called on DEL
   * @return
   */
  public final TextBox newTextBox ( int visible_length, KeyPressHandler keypress )
    {
    TextBox risul = newTextBox(visible_length);

    if ( keypress != null ) 
      risul.addKeyPressHandler(keypress);

    return risul;
    }


  /**
   * keydown is called even when hitting DEL
   */
  public final TextBox newTextBox ( int visible_length, KeyDownHandler keydown )
    {
    TextBox risul = newTextBox(visible_length);

    if ( keydown != null ) 
      risul.addKeyDownHandler(keydown);

    return risul;
    }

  
  /**
   * Subclasses MUST define a get label method that allows me to get a translated label
   * Using this method I will be able to know which page request which label and ALSO enforce that there is a get Label !
   * @param label_english
   * @return
   */
  public abstract String getLabel (  String label_english );
  
  
  /**
   * Returns a Label that has a css class applied to make it medium bold
   * @param label_english
   * @return
   */
  public Label getGwtLabelBold ( String label_english )
    {
    return newGwtLabelBold(getLabel(label_english));
    }
  
  
  public Label newGwtLabelBold ( )
    {
    Label risul = new Label();

    // it is bold and has a padding on the right
    risul.addStyleName("itemLabelBold");
    
    return risul;
    }
  
  /**
   * No label mapping is done with this one
   */
  public Label newGwtLabelBold ( String label_text )
    {
    Label risul = newGwtLabelBold();
    
    if ( label_text != null )
      risul.setText(label_text);
    
    return risul;
    }

  /**
   * No label mapping is done with this one
   * @param label_text
   * @return
   */
  public Label getBoldMediumRedLabel ( String label_text )
    {
    Label risul = newGwtLabelBold ( label_text);   
    risul.addStyleName("colorRed");
    return risul;
    }

  /**
   * Returns an inline label that can be used to space elements if they are too close together.
   * the with is css width, like 10px, 1em and so on
   * @param width
   * @return
   */
  public InlineLabel getSpace ( String width )
    {
    InlineLabel space = new InlineLabel();
    space.setWidth(width);
    return space;      
    }
	
  }
