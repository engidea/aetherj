/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.gui;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.*;

import it.aetherj.gwtwapp.client.*;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.ui.FlexTableEditable;

public final class AecliInfoShow extends AecliGenericCenterPanel
  {
  private static final String classname="AecliInfoShow.";
  
  private final DateTimeFormat activityTimeFormat = DateTimeFormat.getFormat("H:mm:ss");
  
  private final Label backendVersion;
  private final Label dbaseVersion;
  private final Label gwcliVersion;
  private final Label webUserName;  
  private final Label webUserLogin; 
  private final Label messageLabel; 
  private final Label errorLabel; 
  private final Label timestampLabel; 
  
  
  public AecliInfoShow(RecliStat stat)
    {
    super(stat,"System Information","system_info-help");

    backendVersion = new Label();
    dbaseVersion   = new Label();
    gwcliVersion   = new Label();
    webUserName    = new Label();
    webUserLogin   = new Label();
    messageLabel   = new Label();
    errorLabel     = new Label();
    timestampLabel = new Label();
    
    setInnerCenterPanel(newWorkPanel());
    }

  private String getActivityTime ( Date time )
    {
    if ( time == null ) return "";
    
    return activityTimeFormat.format(time);
    }

  public void refreshConnectionStatus ( AegwbConnectionStatus conn_status )
    {
    messageLabel.setText(conn_status.imp_msg);   
    errorLabel.setText(conn_status.imp_msg_error);
    timestampLabel.setText(getActivityTime(conn_status.imp_msg_date));
    }
  
  
  public void refreshSysinfo (AegwLoginRes res)
    {
    if ( res != null )
      {
      webUserName.setText(res.user_name);
      webUserLogin.setText(res.user_login);
      }
    
    if ( stat.dictionary != null )
      {
      backendVersion.setText(stat.dictionary.backendVersion);
      dbaseVersion.setText(stat.dictionary.dbaseVersion);
      }
    
    gwcliVersion.setText(GwtwappVersion.VERSION);
    }

  private Widget newWorkPanel ()
    {
    FlexTableEditable risul = new FlexTableEditable(this);
    
    int rowIndex=0; int colIndex=0;
    
    risul.setLabelCell(rowIndex,colIndex++,"Web User Name");
    risul.setWidget(rowIndex,colIndex++,webUserName);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Web User Login");
    risul.setWidget(rowIndex,colIndex++,webUserLogin);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Backend Version");
    risul.setWidget(rowIndex,colIndex++,backendVersion);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"GWT Client Version");
    risul.setWidget(rowIndex,colIndex++,gwcliVersion);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Dbase Version");
    risul.setWidget(rowIndex,colIndex++,dbaseVersion);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"System Message");
    risul.setWidget(rowIndex,colIndex++,messageLabel);


    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Error Message");
    risul.setWidget(rowIndex,colIndex++,errorLabel);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Event Timestamp");
    risul.setWidget(rowIndex,colIndex++,timestampLabel);

    return risul;
    }
  
  
  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }
  
  public String getLabel(String input)
    {
    return stat.getLabel(input,classname); 
    }


  public void guiClear()
    {
    }

  public void postGuiRefreshReq ()
    {
    }
  
  public void postServerRefreshReq()
    {
    }

  }
