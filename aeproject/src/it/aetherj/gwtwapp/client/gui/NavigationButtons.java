/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.gui;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.ui.GwtWidgetProvider;


/**
 * Navigation is now made of buttons, not a tree anymore
 */
public final class NavigationButtons implements GwtWidgetProvider
  {
  private static final String classname="NavigationButtons";
  
  private final ButtonsListener buttonsListener = new ButtonsListener();
  private final ArrayList<DisplayButtonItem>pagesArrayList;
  
  private final RecliStat stat;
  private final Panel workPanel;
  
  /**
   * This MUST be new after all other pieces are newed
   * @param stat
   */
  public NavigationButtons(RecliStat stat)
    {
    this.stat = stat;
    
    // first this
    pagesArrayList = newPagesArray();
    // then this one
    workPanel = newWorkPanel();
    
    checkVisibility();
    }

  private ArrayList<DisplayButtonItem> newPagesArray()
    {
    ArrayList<DisplayButtonItem> risul = new ArrayList<DisplayButtonItem>();
    
    risul.add(newButtonItem("Boards",stat.boardList));
    risul.add(newButtonItem("Recent",stat.recentPanel));
    risul.add(newButtonItem("Popular",stat.popularPanel));
    risul.add(newButtonItem("Chat",stat.chatListPanel));
    risul.add(newButtonItem("Users",stat.ukeyListPanel));

    return risul;                                                                 
    }


  private Panel newWorkPanel()
    {
    FlowPanel risul = new FlowPanel();
    risul.addStyleName("navigation-panel");
    
    for (int index=0; index<pagesArrayList.size(); index++)
      {
      DisplayButtonItem item = pagesArrayList.get(index);
      
      if ( item.abutton == null )
        {
        // create if not available
        item.abutton = new PushButton (item.label, buttonsListener);
        }
      
      risul.add(item.abutton);
      }

    return risul;
    }

  private String getLabel (String input)
    {
    return stat.getLabel(input,classname);      
    }

  /**
   * Some part of the application can ask the tree to select a tree item.
   * Selection a tree item also dispatch the show of the associated page.
   * @param displayPage
   */
  public void setSelectedPage ( AecliGenericRootPage displayPage )
    {
    DisplayButtonItem item = getItemUsingPage ( displayPage );
    
    if ( item == null )
      {
      stat.logOnBrowser("setSelectedPage: No menu for "+displayPage);
      return;
      }
    
    dispatchItem ( item );
    }

  private DisplayButtonItem getItemUsingPage ( AecliGenericRootPage displayPage )
    {
    for (DisplayButtonItem item : pagesArrayList)
      if ( item.displayPage == displayPage ) 
        return item;
    
    return null;
    }


  private DisplayButtonItem getButtonSource ( Object source )
    {
    int list_size = pagesArrayList.size();
    
    for (int index=0; index<list_size; index++)
      {
      DisplayButtonItem item = pagesArrayList.get(index);
      
      if ( item.abutton == source ) return item;
      }
    
    return null;
    }
  

  /**
   * When something changes in the system I should check for items visibility
   * It is not entirely clear when to do it this needs some thinking.
   */
  public void checkVisibility ()
    {
    boolean oneVisible = false;
    
    for ( DisplayButtonItem iter : pagesArrayList )
      oneVisible |= checkItemVisibility ( iter );
    
    }

  private boolean checkItemVisibility ( DisplayButtonItem item )
    {
    boolean visible = item.displayPage.isMenuTreeItemVisible();
    
    if ( item.abutton.isVisible() == visible ) return visible;
    
    item.abutton.setVisible(visible);
    
    return visible;
    }


  private DisplayButtonItem newButtonItem (String label, AecliGenericRootPage displayPage )
    {
    DisplayButtonItem risul = new DisplayButtonItem(displayPage, label);
    
    Image animage = displayPage.newPageButtonImage();
    
    if ( animage != null )
      {
      risul.abutton = new PushButton(animage,buttonsListener);
      risul.abutton.setTitle(getLabel(label));
      }
    else
      {
      risul.abutton = new PushButton (label, buttonsListener);
      }
    
    return risul;
    }


  public Panel getPanelToDisplay()
    {
    return workPanel;
    }
  
  
  private void dispatchItem ( DisplayButtonItem selectedItem )
    {
    if ( selectedItem.displayPage == null ) return;
    
    checkVisibility();
    
    if ( selectedItem.displayPage instanceof AecliGenericCenterPanel ) 
      {
      // I need to do an explicit casting otherwise I will not get the correct method
      AecliGenericCenterPanel apage = (AecliGenericCenterPanel)selectedItem.displayPage;
      
      // I need to use the correct binding
      stat.loggedinPage.setCenterPanel(apage);
  
      // This is a page that can be gui refresh, do it
      // NOTE that this might trigger some data request
      apage.postGuiRefreshReq();
      }
    else
      {
      stat.loggedinPage.setCenterPanelWidget(selectedItem.displayPage);
      }
    }

  
private final class ButtonsListener implements ClickHandler
  {
  @Override
  public void onClick(ClickEvent event)
    {
    DisplayButtonItem item = getButtonSource ( event.getSource() );

    if ( item == null ) return;

    dispatchItem(item);
    }
  }

public static final class DisplayButtonItem 
  {
  final AecliGenericRootPage displayPage;
  final String label;
  
  PushButton abutton;
  
  public DisplayButtonItem(AecliGenericRootPage displayPage, String label )
    {
    this.label = label;
    this.displayPage = displayPage;
    }
  
  }

  }  // END MAIN CLASS
