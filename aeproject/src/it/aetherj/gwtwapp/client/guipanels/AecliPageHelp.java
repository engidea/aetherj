/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.gui.AecliGenericCenterPanel;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;

public final class AecliPageHelp extends AecliGenericCenterPanel
  {
  private static final String classname="AecliPageHelp";
  
  private final DataReadyCallback  dataReadyCallback = new DataReadyCallback();
  private final FormPanel uploadForm;
  
  private final PushButton saveButton;
  private final Hidden pagehelp_id = new Hidden();
  private final TextBox pagehelp_desc;
  private final FileUpload pagehelp_upload;
  private final Label pagehelp_key,pagehelp_lang;  


  private final DataGrid<AegwPageHelp> w_cellTable;
  private final ListDataProvider<AegwPageHelp> w_cellTableData;
  private final SingleSelectionModel<AegwPageHelp> w_selection_model = new SingleSelectionModel<AegwPageHelp>();

  private AegwPageHelpListRes r_List;   // the raw list, before filtering
  
  
  public AecliPageHelp(RecliStat stat)
    {
    super(stat,"Page Help Wiring","pagehelp-set-help");
    
    pagehelp_desc = new TextBox();
    pagehelp_upload = new FileUpload();
    pagehelp_key = new Label();
    pagehelp_lang = new Label();
    
    w_cellTable = newCellTable();
    w_cellTableData = new ListDataProvider<AegwPageHelp>();
    w_cellTableData.addDataDisplay(w_cellTable);    

    uploadForm = newFormPanel();

    saveButton = stat.utils.newSaveButton(new FormSubmit());
    
    addNorth(newButtonsPanel(),northPanelHeight);

    setInnerCenterPanel(newWorkPanel());
    }
  
  private Panel newButtonsPanel()
    {
    GwuiGrid risul = newButtonsGrid(2);
    
    int rowIndex=0; int colIndex=0;
    risul.setWidget(rowIndex,colIndex++,saveButton);
    risul.setWidget(rowIndex,colIndex++,newRefreshButton());
    
    return risul;
    }

  @Override
  public Image newPageButtonImage ()
    {
    return new Image(stat.imageBoundle.newHelp());
    }
  
  
  private DataGrid<AegwPageHelp> newCellTable ()
    {
    DataGrid<AegwPageHelp> risul = new DataGrid<AegwPageHelp>(100,stat.dataGridResource);

    // -------------------------------------------------------------
    TextColumn<AegwPageHelp> acol = new TextColumn<AegwPageHelp>() 
      {
      @Override
      public String getValue(AegwPageHelp arow) 
        { 
        return arow.pagehelp_key;   
        }
      };

    risul.addColumn(acol,getLabel("Help Key"));

    // -------------------------------------------------------------
    acol = new TextColumn<AegwPageHelp>() 
      {
      @Override
      public String getValue(AegwPageHelp arow) 
        { 
        return arow.pagehelp_desc;   
        }
      };

    risul.addColumn(acol,getLabel("Description"));

    // -------------------------------------------------------------
    acol = new TextColumn<AegwPageHelp>() 
      {
      @Override
      public String getValue(AegwPageHelp arow) 
        { 
        return arow.pagehelp_filename;   
        }
      };

    risul.addColumn(acol,getLabel("Filename"));

    risul.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);    
      
    risul.setSelectionModel(w_selection_model);
    w_selection_model.addSelectionChangeHandler(new SelectionChangeEvent.Handler() 
      {
      public void onSelectionChange(SelectionChangeEvent event) 
        {
        pageHelpEdit();
        }
      });    
    
    return risul;
    } 
  
  private void pageHelpEdit ()
    {
    if ( r_List == null ) return;
    
    if ( r_List.getListSize() < 1 ) return;

    AegwPageHelp arow = w_selection_model.getSelectedObject();
    
    if (arow == null) arow = r_List.get(0);
    
    pagehelp_id.setValue(""+arow.pagehelp_id);
    pagehelp_key.setText(arow.pagehelp_key);
    pagehelp_lang.setText(arow.pagehelp_lang);
    pagehelp_desc.setText(arow.pagehelp_desc);
    }
  
  
  private Panel newWorkPanel ()
    {
    GwuiDockLayoutPanel risul = new GwuiDockLayoutPanel();
    
    risul.addNorth(uploadForm, 100);
    
    risul.setCenter(w_cellTable);
 
    return risul;
    }

  /**
   * this menu item is visible only if the user has permission to change the users
   * NOTE: Permission check MUST be done at SERVER side, this is just some eye candy
   */
  public boolean isMenuTreeItemVisible ()
    {
    return stat.isWebSuperuser();
    }

  @Override
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  private Widget newFormEditPanel ()
    {
    FlexTableEditable risul = new FlexTableEditable(this);
    
    int rowIndex=0; int colIndex=0;

    risul.setLabelCell(rowIndex,colIndex++,"help Language");
    risul.setWidget(rowIndex,colIndex++,pagehelp_lang);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Help Key");
    risul.setWidget(rowIndex,colIndex++,pagehelp_key);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Help Description");
    // Create a TextBox, giving it a name so that it will be submitted.
    pagehelp_desc.setName(AegwPageHelp.cn_desc);
    risul.setWidget(rowIndex,colIndex++,pagehelp_desc);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Help Pdf File");

    // Create a FileUpload widget.
    pagehelp_upload.setName(AegwPageHelp.cn_pdf);
    risul.setWidget(rowIndex,colIndex++,pagehelp_upload);
    
    rowIndex++; colIndex=0;
    // giving it a name so that it will be submitted.
    pagehelp_id.setName(AegwPageHelp.cn_id);
    risul.setWidget(rowIndex,colIndex++,pagehelp_id);

    return risul;
    }

  private FormPanel newFormPanel ()
    {
    FormPanel risul = new FormPanel();
    
    risul.setAction(stat.serviceFactory.getPagehelpUrl());
    
    // Because we're going to add a FileUpload widget, we'll need to set the
    // form to use the POST method, and multipart MIME encoding.
    risul.setEncoding(FormPanel.ENCODING_MULTIPART);
    risul.setMethod(FormPanel.METHOD_POST);
    
    risul.add(newFormEditPanel());
    
    risul.addSubmitCompleteHandler(new FormUploadComplete());
   
    return risul;
    }

  
  public void guiClear()
    {
    }
  
  public void postGuiRefreshReq ()
    {
    if ( r_List == null ) 
      {
      postServerRefreshReq();
      return;
      }
    
    int rowCountData = r_List.getListSize();
    
    w_cellTable.setRowCount(rowCountData, true);    
    w_cellTable.setVisibleRange(0, rowCountData);
    w_cellTableData.setList(r_List.getList());
    }


  public void postServerRefreshReq()
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    
    service.getPageHelpList(new AegwPageHelpListReq(stat), dataReadyCallback);
    }


private class DataReadyCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    r_List = (AegwPageHelpListRes)result;

    if ( isErrorMessage(r_List)) return;

    parseCallEndDate(r_List);    
    
    postGuiRefreshReq();
    }
  }



private class FormUploadComplete implements FormPanel.SubmitCompleteHandler
  {
  public void onSubmitComplete(FormPanel.SubmitCompleteEvent event) 
    {
    postServerRefreshReq();

    // When the form submission is successfully completed, this event is
    // fired. Assuming the service returned a response of type text/html,
    // we can get the result text here (see the FormPanel documentation for
    // further explanation).
    stat.logOnBrowser(event.getResults());
    }
  }

private class FormSubmit implements ClickHandler 
  {
  public void onClick(ClickEvent event) 
    {
    uploadForm.submit();
    }
  }



  }  // END MAIN CLASS
