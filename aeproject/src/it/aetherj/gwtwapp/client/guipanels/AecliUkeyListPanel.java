/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ListDataProvider;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.gui.AecliGenericCenterPanel;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.RecliGenericCallback;

/**
 * List the registered users in Aether, name and fingerprint, so, you can copy fingerprint for chat...
 * Need to add the info part, loaded on demand
 */
public final class AecliUkeyListPanel extends AecliGenericCenterPanel
  {
  private static final String classname="AecliUkeyListPanel.";
  
  private static final String cn_name = "ukey_name";   // the column name in gwt
  
  private final BoardCallback srvCallback = new BoardCallback();
  private final SearchLikeTimer searchLikeTimerTimer = new SearchLikeTimer();

  private final TextBox searchBox;

  private final DataGrid<AegwUkey> w_cellTable;
  private final ListDataProvider<AegwUkey> w_cellTableData;
  private AegwUkeyListRes w_List;
  
  
  public AecliUkeyListPanel(RecliStat stat)
    {
    super(stat,"Users List","aether-users-list-help");
    
    w_cellTable = newCellTable();
    w_cellTableData = new ListDataProvider<AegwUkey>();
    w_cellTableData.addDataDisplay(w_cellTable);    
    
    searchBox = newTextBox(30);
    searchBox.addKeyDownHandler(new SearchKeyDownHandler());
    
    addNorth(newSearchPanel(),northPanelHeight);

    setInnerCenterPanel(w_cellTable);
    }
  
  private Panel newSearchPanel ()
    {
    HorizontalPanel risul = new HorizontalPanel();
    
    risul.add(getGwtLabel("Search"));
    risul.add(searchBox);

    return risul;
    }
    	
  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }
  
  private void filterLikeSetData ()
    {
    if ( w_List == null ) return;
    
    ArrayList<AegwUkey> list = w_List.getLike(searchBox.getText());
    
    w_cellTableData.setList(list);
    }
  
  private ColumnSortEvent.Handler newColumnSortHandler ()
    {
    ColumnSortEvent.Handler risul = new ColumnSortEvent.Handler()
      {
      @Override
      public void onColumnSort(ColumnSortEvent event)
        {
        if ( w_List == null ) return;
        
        String dbcolname = event.getColumn().getDataStoreName();
        
        if ( dbcolname == null ) return;
        
        if ( dbcolname.equals(cn_name))
          w_List.sortByName(event.isSortAscending());
        
        // the data... see what happens
        filterLikeSetData();
        }
      };
      
    return risul;
    }
  
  private DataGrid<AegwUkey> newCellTable ()
    {
    DataGrid<AegwUkey> risul = new DataGrid<AegwUkey>(100,stat.dataGridResource);

    risul.setAutoHeaderRefreshDisabled(true);
            
    // ---------------------------------------------------------------
    TextColumn<AegwUkey> nameColumn = new TextColumn<AegwUkey>() 
      {
      @Override
      public String getValue(AegwUkey imp) { return imp.ukey_name;   }
      };

    nameColumn.setSortable(true);
    nameColumn.setDataStoreName(cn_name);
    risul.addColumn(nameColumn, getLabel("User Name"));
    risul.setColumnWidth(nameColumn, 15.0, Unit.EM);
    
    // ---------------------------------------------------------------
    TextColumn<AegwUkey> ownerColumn = new TextColumn<AegwUkey>() 
      {
      @Override
      public String getValue(AegwUkey contact) { return contact.ukey_fprint_hex;   }
      };

    risul.addColumn(ownerColumn, getLabel("FIngerprint"));
    risul.setColumnWidth(ownerColumn, 15.0, Unit.EM);

    // ---------------------------------------------------------------
    // May add something else later
    
    
    risul.addColumnSortHandler(newColumnSortHandler());
    
    return risul;
  	}
	
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  public void guiClear()
    {
    }
  

  public void postGuiRefreshReq ()
    {
    if ( w_List == null ) 
      {
      postServerRefreshReq();
      return;
      }

    int rowCountData = w_List.getListSize();
    
    w_cellTable.setRowCount(rowCountData, true);    
    w_cellTable.setVisibleRange(0, rowCountData);
    filterLikeSetData();
    }
  
  @Override
  public void postServerRefreshReq()
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwUkeyListReq req = new AegwUkeyListReq(stat);
    service.getUkeyList(req, srvCallback);
    }

private final class SearchLikeTimer extends Timer
  {
  @Override
  public void run()
    {
    filterLikeSetData();
    }
  }


private final class SearchKeyDownHandler implements KeyDownHandler
  {
  @Override
  public void onKeyDown(KeyDownEvent event)
    {
    searchLikeTimerTimer.cancel();
    searchLikeTimerTimer.schedule(1000);
    }
  }

private final class BoardCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    w_List = (AegwUkeyListRes)result;
  
    if ( isErrorMessage(w_List)) return;
    
    parseCallEndDate(w_List);    
    postGuiRefreshReq();
    }
  }


  }  // END MAIN CLASS
