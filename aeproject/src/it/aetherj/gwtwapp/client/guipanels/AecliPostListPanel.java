/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.gui.AecliGenericCenterPanel;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;
import it.aetherj.gwtwapp.client.uict.*;
import it.aetherj.protocol.MimPowValue;

/** 
 * Posts, this is a list representation, possibly becoming obsolete
 * Not to be used, see PostTreePanel
 */
public final class AecliPostListPanel extends AecliGenericCenterPanel
  {
  private static final String classname="AecliPostListPanel.";
  
  private final PostCallback srvCallback = new PostCallback();
  private final SearchLikeTimer searchLikeTimerTimer = new SearchLikeTimer();
  private final SaveCompleteCallback saveCompleteCallback = new SaveCompleteCallback();

  private final TextBox searchBox,ownerName;
  private final PushButton newPostButton,savePostButton;
  private final RichTextArea richTextArea;
  private final GwuiRichTextToolbar richTextToolbar;

  private final DataGrid<AegwPost> w_cellTable;
  private final ListDataProvider<AegwPost> w_cellTableData;
  private final SingleSelectionModel<AegwPost>selectionModel;
  
  private AegwPostListRes w_List;
  
  // do not let it become null
  private AegwPost aepost = new AegwPost();
  
  public AecliPostListPanel(RecliStat stat)
    {
    super(stat,"Post List","post-list-help");
    
    searchBox = newTextBox(20, new SearchKeyDownHandler());
    ownerName  = newTextBox(40);

    selectionModel=new SingleSelectionModel<>();
    w_cellTable = newCellTable();
    w_cellTableData = new ListDataProvider<AegwPost>();
    w_cellTableData.addDataDisplay(w_cellTable);
    
    newPostButton = stat.utils.newNewButton(new NewPostClick());
    savePostButton = stat.utils.newSaveButton(new SaveRowClick());

    
    richTextArea = new RichTextArea();
    richTextArea.setWidth("90%");
    
    richTextToolbar = new GwuiRichTextToolbar(richTextArea);
    
    addNorth(newNorthPanel(),256);
    
    setInnerCenterPanel(w_cellTable);
    }
  
  private Panel newNorthPanel ()
    {
    GwuiGrid risul = new GwuiGrid(4,1);
    risul.setWidth("100%");
 
    int rowIndex=0;
    risul.setWidget(rowIndex++, 0, newNorthTopPanel());
    risul.setWidget(rowIndex++, 0, newNorthOwnerPanel());
    risul.setWidget(rowIndex++, 0, richTextToolbar);
    risul.setWidget(rowIndex++, 0, richTextArea);

    return risul;
    }
  
  private Panel newNorthOwnerPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Owner Name"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(ownerName,HasVerticalAlignment.ALIGN_MIDDLE);

    return risul;
    }


  private Panel newNorthTopPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Search"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(searchBox,HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(newPostButton,HasVerticalAlignment.ALIGN_MIDDLE);

    risul.add(savePostButton);

    return risul;
    }
  
  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }
  
  
  private void filterLikeSetData ()
    {
    if ( w_List == null ) return;
    
    ArrayList<AegwPost> list = w_List.getLike(searchBox.getText());
    
    w_cellTableData.setList(list);
    }
  
  private ColumnSortEvent.Handler newColumnSortHandler ()
    {
    ColumnSortEvent.Handler risul = new ColumnSortEvent.Handler()
      {
      @Override
      public void onColumnSort(ColumnSortEvent event)
        {
        if ( w_List == null ) return;
        
        String dbcolname = event.getColumn().getDataStoreName();
        
        if ( dbcolname == null ) return;
        
        if ( dbcolname.equals("aemsg_title"))
          w_List.sortPostBody(event.isSortAscending());
        
        // the data... see what happens
        filterLikeSetData();
        }
      };
      
    return risul;
    }
  
  private ColumnImageClick<AegwPost> newUpvoteColumn ()
    {
    CellImageClickHandler clickHandler = new CellImageClickHandler()
      {
      @Override
      public void onClick(Object dataObject, int row)
        {
        voteAdjust((AegwPost) dataObject, true);
        }
      };
    
    CellImageClick jumpButton = new CellImageClick(clickHandler);
      
    ColumnImageClick<AegwPost> risul = new ColumnImageClick<AegwPost>(jumpButton) 
      {
      @Override
      public ImageResource getValue(AegwPost contact) { return stat.imageBoundle.newUp();   }
      };

    return risul;
    }

  private ColumnImageClick<AegwPost> newDownvoteColumn ()
    {
    CellImageClickHandler clickHandler = new CellImageClickHandler()
      {
      @Override
      public void onClick(Object dataObject, int row)
        {
        voteAdjust((AegwPost) dataObject, false);
        }
      };
    
    CellImageClick jumpButton = new CellImageClick(clickHandler);
      
    ColumnImageClick<AegwPost> risul = new ColumnImageClick<AegwPost>(jumpButton) 
      {
      @Override
      public ImageResource getValue(AegwPost contact) { return stat.imageBoundle.newDown();   }
      };

    return risul;
    }

  private void voteAdjust (AegwPost row, boolean upvote )
    {
    aepost = row;

    stat.logOnBrowser(classname+".voteAdjust: "+row+" upvote="+upvote);
    
    AegwVote aegwVote = new AegwVote();
    aegwVote.vote_board_id  = row.post_board_id;
    aegwVote.vote_thread_id = row.post_thread_id;
    aegwVote.vote_post_id   = row.post_id;
    aegwVote.vote_up        = upvote;
    
    AecliGenericPowCalculator gpowCalc = new AecliGenericPowCalculator(stat,new SaveVoteCommitPow());
    
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwVoteOprReq req = new AegwVoteOprReq(stat,aegwVote);
    service.postVoteOpr(req, gpowCalc.preparePowCallback);
    }
   
  private DataGrid<AegwPost> newCellTable ()
    {
    DataGrid<AegwPost> risul = new DataGrid<AegwPost>(100,stat.dataGridResource);

    risul.setAutoHeaderRefreshDisabled(true);
    
    // ---------------------------------------------------------------
    ColumnImageClick<AegwPost>aup = newUpvoteColumn();
    risul.addColumn(aup);
    risul.setColumnWidth(aup, 20.0, Unit.PX);    

    // ---------------------------------------------------------------
    TextColumn<AegwPost> votesColumn = new TextColumn<AegwPost>() 
      {
      @Override
      public String getValue(AegwPost row) { return ""+row.post_votes_cnt;   }
      };

    risul.addColumn(votesColumn, getLabel("Votes"));
    risul.setColumnWidth(votesColumn, 3, Unit.EM);    

    // ---------------------------------------------------------------
    ColumnImageClick<AegwPost>adown = newDownvoteColumn();
    risul.addColumn(adown);
    risul.setColumnWidth(adown, 20.0, Unit.PX);    

    // ---------------------------------------------------------------
    TextColumn<AegwPost> bodyColumn = new TextColumn<AegwPost>() 
      {
      @Override
      public String getValue(AegwPost imp) { return imp.getPostVotesBodyBegin(200);   }
      };

    bodyColumn.setSortable(true);
    bodyColumn.setDataStoreName("aemsg_title");
    risul.addColumn(bodyColumn, getLabel("Post Content"));
        
    // ---------------------------------------------------------------
    TextColumn<AegwPost> parentIdColumn = new TextColumn<AegwPost>() 
      {
      @Override
      public String getValue(AegwPost contact) { return ""+contact.post_parent_id;   }
      };

    risul.addColumn(parentIdColumn, getLabel("Parent"));
    risul.setColumnWidth(parentIdColumn, 4.0, Unit.EM);    
    
    risul.addColumnSortHandler(newColumnSortHandler());
    
    risul.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);    
    
    risul.setSelectionModel(selectionModel);
    selectionModel.addSelectionChangeHandler(new RowSelectionHandler());
    
    return risul;
  	}
  
	
  @Override
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  public void guiClear()
    {
    }

  private void saveRowPreparePow()
    {
    // this is the only thing to fix, the rest should be right...
    aepost.post_body = richTextArea.getText();
    
    AecliGenericPowCalculator gpowCalc = new AecliGenericPowCalculator(stat,new SavePostCommitPow());
    
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwPostOprReq req = new AegwPostOprReq(stat,aepost,AegwPostOprReq.req_save_prepare_pow);
    service.postPostOpr(req, gpowCalc.preparePowCallback);
    }

private class SavePostCommitPow implements AecliSavePowCommitRow
  {
  @Override
  public void saveRowCommitPow(MimPowValue powValue, Integer wpow_id)
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwPostOprReq req = new AegwPostOprReq(stat,powValue,wpow_id);
    service.postPostOpr(req, saveCompleteCallback); 
    }
  }

private class SaveVoteCommitPow implements AecliSavePowCommitRow
  {
  @Override
  public void saveRowCommitPow(MimPowValue powValue, Integer wpow_id)
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwVoteOprReq req = new AegwVoteOprReq(stat,powValue,wpow_id);
    service.postVoteOpr(req, saveCompleteCallback); 
    }
  }
  
  public void postGuiRefreshReq ()
    {
    if ( w_List == null ) 
      {
      postServerRefreshReq();
      return;
      }

    int rowCountData = w_List.getListSize();
    
    w_cellTable.setRowCount(rowCountData, true);    
    w_cellTable.setVisibleRange(0, rowCountData);
    filterLikeSetData();
    
    richTextArea.setText(aepost.post_body);
    }
  
private final class PostCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    w_List = (AegwPostListRes)result;

    if ( isErrorMessage(w_List)) return;
    
    parseCallEndDate(w_List);    
    postGuiRefreshReq();
    }
  }

private final class SearchLikeTimer extends Timer
  {
  @Override
  public void run()
    {
    filterLikeSetData();
    }
  }


private final class SearchKeyDownHandler implements KeyDownHandler
  {
  @Override
  public void onKeyDown(KeyDownEvent event)
    {
    searchLikeTimerTimer.cancel();
    searchLikeTimerTimer.schedule(1000);
    }

  }


@Override
public void postServerRefreshReq()
  {
  RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
  
  int thread_id=stat.statusBar.getThreadId();
  
  AegwPostListReq req = new AegwPostListReq(stat,thread_id);
  
  service.getPostList(req, srvCallback);
  }

private class NewPostClick implements ClickHandler 
  {
  public void onClick(ClickEvent event) 
    {
    int board_id  = stat.statusBar.getBoardId();
    int thread_id = stat.statusBar.getThreadId();

    if ( board_id <= 0 || thread_id <= 0 )
      {
      stat.alert("BAD from_user_id="+board_id+" or thread_id="+thread_id);
      return;
      }
    
    aepost.clear(board_id,thread_id);

    postGuiRefreshReq();
    }
  }

private final class SaveRowClick implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    saveRowPreparePow ();
    }
  }

 
/**
 * This is called when the insert is complete
 */
private class SaveCompleteCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwCommonOprRes res = (AegwCommonOprRes)result;
  
    if ( isErrorMessage(res)) return;
    
    stat.logOnBrowser("message="+res.getFeedbackMessage());
    
    stat.alert("Post save complete");
    }
  }

private class RowSelectionHandler implements SelectionChangeEvent.Handler
  {
  @Override
  public void onSelectionChange(SelectionChangeEvent event) 
    {
    AegwPost selected = selectionModel.getSelectedObject();
    if ( selected == null )
      return;
    
    aepost = selected;
    
    richTextArea.setText(selected.post_body);
    }
  }


  }  // END MAIN CLASS
