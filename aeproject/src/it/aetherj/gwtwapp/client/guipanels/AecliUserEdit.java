/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.gui.AecliGenericCenterPanel;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;
import it.aetherj.protocol.MimPowValue;

/**
 * A User that is authenticated can use this page to edit its details
 * It must be possible to import current aether user config
 * Also, there is NO new user, you can just save the current info and it will be a NEW user if there is no info on record
 */
public final class AecliUserEdit extends AecliGenericCenterPanel
  {
  private static final String classname="AecliUserEdit";
  
  private final DateTimeFormat lastTimeFormat = DateTimeFormat.getFormat("yyyy-MM-dd hh:mm:ss");

  private final UsubDataReadyCallback  usubDataReadyCallback = new UsubDataReadyCallback();
  private final UkeyDataReadyCallback ukeyDataReadyCallback = new UkeyDataReadyCallback(); 
  private final FormPanel uploadForm;
  private final PushButton uploadFormButton,saveUserButton;
  
  private final TextBox user_name;
  private final RichTextArea user_info;

  private final FileUpload oldconfig_upload;
  private final Label user_fingerprint,public_key;  

  private final DataGrid<AegwUsub> w_cellTable;
  private final ListDataProvider<AegwUsub> w_cellTableData;
  private final SingleSelectionModel<AegwUsub> w_selection_model = new SingleSelectionModel<AegwUsub>();

  private AegwUsubListRes r_List;     // the raw list, before filtering
  private AegwUkey aeukey = new AegwUkey();  // data to display, do not let it be null
  
  public AecliUserEdit(RecliStat stat)
    {
    super(stat,"Edit User","wuser-edit-help");
    
    saveUserButton = stat.utils.newSaveButton(new SaveRowClick());

    
    user_name = new TextBox();
    user_name.setWidth("50%");
    
    user_info = new RichTextArea();
    user_info.setWidth("90%");

    oldconfig_upload = new FileUpload();
    oldconfig_upload.setName(AegwUsub.cn_filename);

    user_fingerprint = new Label();
    public_key = new Label();
    
    w_cellTable = newCellTable();
    w_cellTableData = new ListDataProvider<AegwUsub>();
    w_cellTableData.addDataDisplay(w_cellTable);    

    uploadFormButton = stat.utils.newSaveButton(new FormSubmit());
    
    uploadForm = newFormPanel();
    
    addNorth(newNorthPanel(),300);

    setInnerCenterPanel(w_cellTable);
    }
  
  private Panel newNorthTopPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.add(saveUserButton);

    return risul;
    }

  private Panel newNorthPanel ()
    {
    GwuiGrid risul = new GwuiGrid(6,1);
    risul.setWidth("100%");
 
    int rowIndex=0;
    risul.setWidget(rowIndex++, 0, newNorthTopPanel());
    risul.setWidget(rowIndex++, 0, newUserProperties());
    risul.setWidget(rowIndex++, 0, user_info);
    risul.setWidget(rowIndex++, 0, uploadForm);
    
    return risul;
    }

  private Panel newUserProperties()
    {
    FlexTableEditable risul = new FlexTableEditable(this);

    int rowIndex=0; int colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Fingerprint: ");
    risul.setWidget(rowIndex,colIndex++,user_fingerprint);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Public Key: ");
    risul.setWidget(rowIndex,colIndex++,public_key);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"User Name ");
    risul.setWidget(rowIndex,colIndex++,user_name);
    
    return risul;
    }
  
  
  @Override
  public Image newPageButtonImage ()
    {
    return new Image(stat.imageBoundle.newHelp());
    }
  
  
  private DataGrid<AegwUsub> newCellTable ()
    {
    DataGrid<AegwUsub> risul = new DataGrid<AegwUsub>(100,stat.dataGridResource);

    // -------------------------------------------------------------
    TextColumn<AegwUsub> acol = new TextColumn<AegwUsub>() 
      {
      @Override
      public String getValue(AegwUsub arow) 
        { 
        return arow.board_name;   
        }
      };

    risul.addColumn(acol,getLabel("Board Name "));

    // -------------------------------------------------------------
    acol = new TextColumn<AegwUsub>() 
      {
      @Override
      public String getValue(AegwUsub arow) 
        { 
        return ""+arow.wusub_notify;   
        }
      };

    risul.addColumn(acol,getLabel("Notify "));

    // -------------------------------------------------------------
    acol = new TextColumn<AegwUsub>() 
      {
      @Override
      public String getValue(AegwUsub arow) 
        { 
        if ( arow.wusub_last_seen == null )
          return "";
        
        return lastTimeFormat.format(arow.wusub_last_seen);
        }
      };

    risul.addColumn(acol,getLabel("Last Seen "));

    risul.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);    
      
    risul.setSelectionModel(w_selection_model);
    w_selection_model.addSelectionChangeHandler(new SelectionChangeEvent.Handler() 
      {
      public void onSelectionChange(SelectionChangeEvent event) 
        {
        wusubEdit();
        }
      });    
    
    return risul;
    } 
  
  private void wusubEdit ()
    {
    if ( r_List == null ) return;
    
    if ( r_List.getListSize() < 1 ) return;

    AegwUsub arow = w_selection_model.getSelectedObject();
    
    if (arow == null) arow = r_List.get(0);
    
    // TODO what I should do is to enable/disable the boolean flag
    }
  
  private Panel newWorkPanel ()
    {
    GwuiDockLayoutPanel risul = new GwuiDockLayoutPanel();
    
    risul.addNorth(uploadForm, 100);
    
    risul.setCenter(w_cellTable);

    return risul;
    }

  /**
   * this menu item is visible only if the user has permission to change the users
   * NOTE: Permission check MUST be done at SERVER side, this is just some eye candy
   */
  public boolean isMenuTreeItemVisible ()
    {
    return stat.isWebSuperuser();
    }

  @Override
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  private Widget newFormPanelGui ()
    {
    HorizontalPanel risul = new HorizontalPanel();

    Label label = new Label("Upload Aether Client Config File: ");
    risul.add(label);
    risul.setCellVerticalAlignment(label, HasVerticalAlignment.ALIGN_MIDDLE);

    risul.add(oldconfig_upload);
    risul.setCellVerticalAlignment(oldconfig_upload, HasVerticalAlignment.ALIGN_MIDDLE);
    
    risul.add(uploadFormButton);
    
    return risul;
    }

  private FormPanel newFormPanel ()
    {
    FormPanel risul = new FormPanel();
    
    risul.setAction(stat.serviceFactory.importWusubUrl());
    
    // Because we're going to add a FileUpload widget, we'll need to set the
    // form to use the POST method, and multipart MIME encoding.
    risul.setEncoding(FormPanel.ENCODING_MULTIPART);
    risul.setMethod(FormPanel.METHOD_POST);
    
    risul.add(newFormPanelGui());
    
    risul.addSubmitCompleteHandler(new FormUploadComplete());
   
    return risul;
    }

  
  public void guiClear()
    {
    }
  
  private void guiRefreshUkey ()
    {
    user_name.setText(aeukey.ukey_name);
    user_info.setText(aeukey.ukey_info);
    public_key.setText(aeukey.ukey_pubkey_hex); 
    user_fingerprint.setText(aeukey.ukey_fprint_hex);
    }
  
  @Override
  public void postGuiRefreshReq ()
    {
    if ( r_List == null ) 
      {
      postServerRefreshReq();
      return;
      }

    guiRefreshUkey();
    
    int rowCountData = r_List.getListSize();
    
    w_cellTable.setRowCount(rowCountData, true);    
    w_cellTable.setVisibleRange(0, rowCountData);
    w_cellTableData.setList(r_List.getList()); 
    }


  public void postServerRefreshReq()
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();

    service.postUkeyOpr(new AegwUkeyOprReq(stat), ukeyDataReadyCallback);
    service.getWusubList(new AegwUsubListReq(stat), usubDataReadyCallback);
    }

  private void saveRowPreparePow()
    {
    aeukey.clear();
    
    aeukey.ukey_name = user_name.getText();
    aeukey.ukey_info = user_info.getText();
    
    AecliGenericPowCalculator gpowCalc = new AecliGenericPowCalculator(stat,new SaveUserCommitPow());
    
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwUkeyOprReq req = new AegwUkeyOprReq(stat,aeukey);
    service.postUkeyOpr(req, gpowCalc.preparePowCallback);
    }

private class SaveUserCommitPow implements AecliSavePowCommitRow
  {
  @Override
  public void saveRowCommitPow(MimPowValue powValue, Integer wpow_id)
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwUkeyOprReq req = new AegwUkeyOprReq(stat,powValue,wpow_id);
    service.postUkeyOpr(req, new SaveCompleteCallback()); 
    }
  }

private class SaveCompleteCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwCommonOprRes res = (AegwCommonOprRes)result;
  
    if ( isErrorMessage(res)) return;
    
    stat.logOnBrowser("message="+res.getFeedbackMessage());
    
    stat.alert("User save complete");
    }
  }
  
private class UkeyDataReadyCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwUkeyOprRes res = (AegwUkeyOprRes)result;

    if ( isErrorMessage(res)) return;

    if ( res.aeukey == null )
      {
      stat.alert(classname+".UkeyDataReadyCallback: res.aeukey == null ");
      return;
      }
    
    aeukey = res.aeukey;
    
    parseCallEndDate(res);
    
    guiRefreshUkey(); 
    }
  }

  
/**
 * Callback for when the User subscripting data is ready
 */
private class UsubDataReadyCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    r_List = (AegwUsubListRes)result;

    if ( isErrorMessage(r_List)) return;

    parseCallEndDate(r_List);    
    
    postGuiRefreshReq();
    }
  }



private class FormUploadComplete implements FormPanel.SubmitCompleteHandler
  {
  public void onSubmitComplete(FormPanel.SubmitCompleteEvent event) 
    {
    postServerRefreshReq();

    // When the form submission is successfully completed, this event is
    // fired. Assuming the service returned a response of type text/html,
    // we can get the result text here (see the FormPanel documentation for
    // further explanation).
    stat.logOnBrowser(event.getResults());
    }
  }

private class FormSubmit implements ClickHandler 
  {
  public void onClick(ClickEvent event) 
    {
    uploadForm.submit();
    }
  }

private final class SaveRowClick implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    saveRowPreparePow ();
    }
  }



  }  // END MAIN CLASS
