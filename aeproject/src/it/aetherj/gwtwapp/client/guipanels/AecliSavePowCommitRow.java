package it.aetherj.gwtwapp.client.guipanels;

import it.aetherj.protocol.MimPowValue;

public interface AecliSavePowCommitRow
  {
  public void saveRowCommitPow(MimPowValue mim_pow, Integer wpow_id);
  }
