/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.gui.AecliGenericCenterPanel;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;
import it.aetherj.protocol.MimPowValue;

/**
 * All the chat you have and a possibility of writing to them
 * So, if you are just talking to one it is just that
 * You have an option to delete a message
 */
public final class AecliChatListPanel extends AecliGenericCenterPanel
  {
  private static final String classname="AecliChatListPanel";
  
  private final SaveCompleteCallback saveCompleteCallback = new SaveCompleteCallback();

  private final ThreadCallback srvCallback = new ThreadCallback();
  private final SearchLikeTimer searchLikeTimerTimer = new SearchLikeTimer();

  private final TextBox searchBox;
  private final PushButton newChatButton,saveThreadButton;
  private final RichTextArea richTextArea;
  private final GwuiRichTextToolbar richTextToolbar;
  private final TextBox  toHostAddress;
  private final TextBox  toUkeyFingerprint;

  private final DataGrid<AegwChat> w_cellTable;
  private final ListDataProvider<AegwChat> w_cellTableData;
  private AegwChatListRes w_List;
  
  private final SingleSelectionModel<AegwChat>selectionModel;
  
  // this is the current thread that is being display, voted, edited: do not let it become null
  private AegwChat aechat = new AegwChat();
  
  public AecliChatListPanel(RecliStat stat)
    {
    super(stat,"Chat List","chat-list-help");
    
    selectionModel=new SingleSelectionModel<>();

    toHostAddress     = newTextBox(64);
    toUkeyFingerprint = newTextBox(64);

    
    w_cellTable = newCellTable();
    w_cellTableData = new ListDataProvider<AegwChat>();
    w_cellTableData.addDataDisplay(w_cellTable);    
    
    newChatButton = stat.utils.newNewButton(new ThreadNewClick());
    saveThreadButton = stat.utils.newSaveButton(new SaveRowClick());

    richTextArea = new RichTextArea();
    richTextArea.setWidth("90%");
    
    richTextToolbar = new GwuiRichTextToolbar(richTextArea);

    searchBox = newTextBox(20, new SearchKeyDownHandler());

    addNorth(newNorthPanel(),290);
    
    setInnerCenterPanel(w_cellTable);
    }

  private Panel newNorthPanel ()
    {
    GwuiGrid risul = new GwuiGrid(6,1);
    risul.setWidth("100%");
 
    int rowIndex=0;
    risul.setWidget(rowIndex++, 0, newNorthTopPanel());
    risul.setWidget(rowIndex++, 0, newNorthTitlePanel());
    risul.setWidget(rowIndex++, 0, newNorthLinkPanel());
    risul.setWidget(rowIndex++, 0, richTextToolbar);
    risul.setWidget(rowIndex++, 0, richTextArea);

    return risul;
    }

  private Panel newNorthTopPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Search"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(searchBox,HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(newChatButton,HasVerticalAlignment.ALIGN_MIDDLE);

    risul.add(saveThreadButton);

    return risul;
    }
      
  private Panel newNorthTitlePanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("To Host Address"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(toHostAddress,HasVerticalAlignment.ALIGN_MIDDLE);

    return risul;
    }

  private Panel newNorthLinkPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("To User Fingerprint"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(toUkeyFingerprint,HasVerticalAlignment.ALIGN_MIDDLE);

    return risul;
    }

  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }

  private void filterLikeSetData ()
    {
    if ( w_List == null ) return;
    
    ArrayList<AegwChat> list = w_List.getLike(searchBox.getText());
    
    w_cellTableData.setList(list);
    }
  
  private ColumnSortEvent.Handler newColumnSortHandler ()
    {
    ColumnSortEvent.Handler risul = new ColumnSortEvent.Handler()
      {
      @Override
      public void onColumnSort(ColumnSortEvent event)
        {
        if ( w_List == null ) return;
        
        String dbcolname = event.getColumn().getDataStoreName();
        
        if ( dbcolname == null ) return;
        
        if ( dbcolname.equals("aemsg_title"))
          w_List.sortImpiantoName(event.isSortAscending());
        
        // the data... see what happens
        filterLikeSetData();
        }
      };
      
    return risul;
    }
  
  private DataGrid<AegwChat> newCellTable ()
    {
    DataGrid<AegwChat> risul = new DataGrid<AegwChat>(100,stat.dataGridResource);

    risul.setAutoHeaderRefreshDisabled(true);
            
    // ---------------------------------------------------------------
    TextColumn<AegwChat> titleColumn = new TextColumn<AegwChat>() 
      {
      @Override
      public String getValue(AegwChat imp) { return imp.chat_from_uname;   }
      };

    titleColumn.setSortable(true);
    titleColumn.setDataStoreName("aemsg_title");
    risul.addColumn(titleColumn, getLabel("From User"));
//    risul.setColumnWidth(titleColumn, 40.0, Unit.PCT);    
                
    // ---------------------------------------------------------------
    TextColumn<AegwChat> threadBody = new TextColumn<AegwChat>() 
      {
      @Override
      public String getValue(AegwChat imp) { return imp.getMessageBegin(80);   }
      };

    risul.addColumn(threadBody,getLabel("Message"));
    risul.setColumnWidth(threadBody, 30.0, Unit.PCT);    
    
    risul.addColumnSortHandler(newColumnSortHandler());
    
    risul.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);    
    risul.setSelectionModel(selectionModel);
    selectionModel.addSelectionChangeHandler(new RowSelectionHandler());
    
    return risul;
    }
  
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  public void guiClear()
    {
    w_List = new AegwChatListRes();
    postGuiRefreshReq();
    stat.statusBar.setSelectedThread(null);
    }

  private void postGuiRefreshEdit ()
    {
//    toHostAddress.setText(aechat.thread_name);
//    toUkeyFingerprint.setText(aechat.thread_link);
    richTextArea.setText(aechat.chat_message);
    }
  
  public void postGuiRefreshReq ()
    {
    if ( w_List == null ) 
      {
      postServerRefreshReq();
      return;
      }

    int rowCountData = w_List.getListSize();
    
    w_cellTable.setRowCount(rowCountData, true);    
    w_cellTable.setVisibleRange(0, rowCountData);
    filterLikeSetData();
    
    stat.statusBar.setSelectedThread(null);
    
    postGuiRefreshEdit();
    }
  
  private void saveRowPreparePow()
    {
    aechat.to_hostname = toHostAddress.getText();
    aechat.chat_to_ukeyid = 1; // toUkeyFingerprint.getText();   TODO
    aechat.chat_message = richTextArea.getText();

    BimcliGenericPowCalculator gpowCalc = new BimcliGenericPowCalculator(stat,new SaveThreadCommitPow());
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwChatOprReq req = new AegwChatOprReq(stat,aechat);
    service.postChatOpr(req, gpowCalc.preparePowCallback);
    }

  @Override
  public void postServerRefreshReq()
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwChatListReq req = new AegwChatListReq(stat,0);
    service.getChatList(req, srvCallback);
    }

  
private class SaveThreadCommitPow implements AecliSavePowCommitRow
  {
  @Override
  public void saveRowCommitPow(MimPowValue powValue, Integer wpow_id)
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwChatOprReq req = new AegwChatOprReq(stat,powValue,wpow_id);
    service.postChatOpr(req, saveCompleteCallback); 
    }
  }

private final class ThreadCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    w_List = (AegwChatListRes)result;

    if ( isErrorMessage(w_List)) return;
    
    parseCallEndDate(w_List);    
    postGuiRefreshReq();
    }
  }

private final class SearchLikeTimer extends Timer
  {
  @Override
  public void run()
    {
    filterLikeSetData();
    }
  }


private final class SearchKeyDownHandler implements KeyDownHandler
  {
  @Override
  public void onKeyDown(KeyDownEvent event)
    {
    searchLikeTimerTimer.cancel();
    searchLikeTimerTimer.schedule(1000);
    }
  }



private class ThreadNewClick implements ClickHandler 
  {
  public void onClick(ClickEvent event) 
    {
    postGuiRefreshEdit();
    }
  }

private final class SaveRowClick implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    saveRowPreparePow ();
    }
  }


/**
 * This is called when the insert is complete
 */
private class SaveCompleteCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwCommonOprRes res = (AegwCommonOprRes)result;
  
    if ( isErrorMessage(res)) return;
    
    stat.logOnBrowser("message="+res.getFeedbackMessage());
    
    stat.alert("Thread save complete");
    }
  }

private class RowSelectionHandler implements SelectionChangeEvent.Handler
  {
  @Override
  public void onSelectionChange(SelectionChangeEvent event) 
    {
    AegwChat selected = selectionModel.getSelectedObject();
    
    if ( selected == null )
      return;
    
    aechat = selected;

    postGuiRefreshEdit();
    }
  }

  }  // END MAIN CLASS
