/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.gui.AecliGenericCenterPanel;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;
import it.aetherj.gwtwapp.client.uict.*;

/**
 * The list of boards suitable for word is at
 * https://sfwlist.getaether.net/sfwlist.json
 */
public final class AecliBoardListPanel extends AecliGenericCenterPanel
  {
  private static final String classname="AecliPopularListPanel.";
  
  private static final String cn_name   = "board_name";     // the column name in gwt
  private static final String cn_tcount = "thread_count";   // the column name in gwt
  
  private final BoardRefreshCallback srvCallback = new BoardRefreshCallback();
  private final SearchLikeTimer searchLikeTimerTimer = new SearchLikeTimer();

  private final PushButton newBoardButton,saveBoardButton;
  private final TextBox searchBox,boardName;
  private final Label ownerName;
  private final RichTextArea richTextArea;
  private final GwuiRichTextToolbar richTextToolbar;

  private final DataGrid<AegwBoard> w_cellTable;
  private final ListDataProvider<AegwBoard> w_cellTableData;
  private final SingleSelectionModel<AegwBoard>selectionModel;

  private AegwBoardListRes w_List;
  
  public AecliBoardListPanel(RecliStat stat)
    {
    super(stat,"Boards List","board-list-help");
    
    selectionModel=new SingleSelectionModel<>();
    
    newBoardButton = stat.utils.newNewButton(new BoardNewClick());
    saveBoardButton = stat.utils.newSaveButton(new SaveRowClick());

    searchBox = newTextBox(30,new SearchKeyDownHandler());
    boardName = newTextBox(40);
    ownerName = new Label();
    
    w_cellTable = newCellTable();
    w_cellTableData = new ListDataProvider<AegwBoard>();
    w_cellTableData.addDataDisplay(w_cellTable);    
    
    richTextArea = new RichTextArea();
    richTextArea.setWidth("90%");
    richTextToolbar = new GwuiRichTextToolbar(richTextArea);
    
    addNorth(newNorthPanel(),290);

    setInnerCenterPanel(w_cellTable);
    }
  
  private Panel newNorthTopPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Search"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(searchBox,HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(newBoardButton,HasVerticalAlignment.ALIGN_MIDDLE);

    risul.add(saveBoardButton);

    return risul;
    }

  private Panel newNorthTitlePanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Board Name"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.add(newLabelSpacer("1em"));
    risul.addValign(boardName,HasVerticalAlignment.ALIGN_MIDDLE);

    return risul;
    }

  private Panel newNorthOwnerPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Owner Name"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.add(newLabelSpacer("1em"));
    risul.addValign(ownerName,HasVerticalAlignment.ALIGN_MIDDLE);

    return risul;
    }

  private Panel newNorthPanel ()
    {
    GwuiGrid risul = new GwuiGrid(6,1);
    risul.setWidth("100%");
 
    int rowIndex=0;
    risul.setWidget(rowIndex++, 0, newNorthTopPanel());
    risul.setWidget(rowIndex++, 0, newNorthTitlePanel());
    risul.setWidget(rowIndex++, 0, newNorthOwnerPanel());
    risul.setWidget(rowIndex++, 0, richTextToolbar);
    risul.setWidget(rowIndex++, 0, richTextArea);

    return risul;
    }
    	
  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }
      
  private ColumnImageClick<AegwBoard> newJumpColumn ()
    {
    CellImageClickHandler clickJump = new CellImageClickHandler()
      {
      @Override
      public void onClick(Object dataObject, int row)
        {
        jumpToThreadList((AegwBoard) dataObject);
        }
      };
    
    CellImageClick jumpButton = new CellImageClick(clickJump);
      
    ColumnImageClick<AegwBoard> risul = new ColumnImageClick<AegwBoard>(jumpButton) 
      {
      @Override
      public ImageResource getValue(AegwBoard contact) { return stat.imageBoundle.newForward();   }
      };

    return risul;
    }
  
  
  private void filterLikeSetData ()
    {
    if ( w_List == null ) return;
    
    ArrayList<AegwBoard> list = w_List.getLike(searchBox.getText());
    
    w_cellTableData.setList(list);
    }
  
  private ColumnSortEvent.Handler newColumnSortHandler ()
    {
    ColumnSortEvent.Handler risul = new ColumnSortEvent.Handler()
      {
      @Override
      public void onColumnSort(ColumnSortEvent event)
        {
        if ( w_List == null ) return;
        
        String dbcolname = event.getColumn().getDataStoreName();
        
        if ( dbcolname == null ) return;
        
        if ( dbcolname.equals(cn_name))
          w_List.sortBoardName(event.isSortAscending());
        else if ( dbcolname.equals(cn_tcount))
          w_List.sortBoardTcount(event.isSortAscending());
        
        // the data... see what happens
        filterLikeSetData();
        }
      };
      
    return risul;
    }
  
  private DataGrid<AegwBoard> newCellTable ()
    {
    DataGrid<AegwBoard> risul = new DataGrid<AegwBoard>(100,stat.dataGridResource);

    risul.setAutoHeaderRefreshDisabled(true);
            
    // ---------------------------------------------------------------
    TextColumn<AegwBoard> nameColumn = new TextColumn<AegwBoard>() 
      {
      @Override
      public String getValue(AegwBoard imp) { return imp.board_name;   }
      };

    nameColumn.setSortable(true);
    nameColumn.setDataStoreName(cn_name);
    risul.addColumn(nameColumn, getLabel("Board Name"));
    risul.setColumnWidth(nameColumn, 15.0, Unit.EM);
    

    // ---------------------------------------------------------------
    TextColumn<AegwBoard> threadcntColumn = new TextColumn<AegwBoard>() 
      {
      @Override
      public String getValue(AegwBoard imp) { return ""+imp.board_treads_cnt;   }
      };
 
    threadcntColumn.setSortable(true);
    threadcntColumn.setDataStoreName(cn_tcount);
    risul.addColumn(threadcntColumn, getLabel("Threads"));
    risul.setColumnWidth(threadcntColumn, 4.0, Unit.EM);

    // ---------------------------------------------------------------
    ColumnImageClick<AegwBoard>ajump = newJumpColumn();
    risul.addColumn(ajump);
    risul.setColumnWidth(ajump, 20.0, Unit.PX);    
    
    // ---------------------------------------------------------------
/*
    TextColumn<AegwBoard> ownerColumn = new TextColumn<AegwBoard>() 
      {
      @Override
      public String getValue(AegwBoard contact) { return contact.board_owner_name;   }
      };

    risul.addColumn(ownerColumn, getLabel("Owner Name"));
    risul.setColumnWidth(ownerColumn, 15.0, Unit.EM);
*/                
    // ---------------------------------------------------------------
    TextColumn<AegwBoard> boardDesc = new TextColumn<AegwBoard>() 
      {
      @Override
      public String getValue(AegwBoard row) { return row.getBoardDescBegin(80);   }
      };

    risul.addColumn(boardDesc,getLabel("Description"));
    
    risul.addColumnSortHandler(newColumnSortHandler());
    
    risul.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);    
    risul.setSelectionModel(selectionModel);
    selectionModel.addSelectionChangeHandler(new RowSelectionHandler());
    
    return risul;
  	}
	
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }
  
  public void jumpToThreadList ( AegwBoard board )
    {
    // set this impianto as the selected one
    stat.statusBar.setSelectedBoard(board);
    
    // Show the thread list page
    stat.loggedinPage.setCenterPanel(stat.threadList);
    // and ask for refresh
    stat.threadList.postGuiRefreshReq();
    }

  public void guiClear()
    {
    w_List = new AegwBoardListRes();
    stat.statusBar.setSelectedBoard(null);
    postGuiRefreshReq();
    }

  private void postGuiRefreshEdit ()
    {
    AegwBoard b = stat.statusBar.getBoard();
    
    boardName.setText(b.board_name);
    richTextArea.setText(b.board_desc);
    ownerName.setText(b.board_owner_name);
    }

  public void postGuiRefreshReq ()
    {
    if ( w_List == null )
      {
      postServerRefreshReq();
      return;
      }

    int rowCountData = w_List.getListSize();
    
    w_cellTable.setRowCount(rowCountData, true);    
    w_cellTable.setVisibleRange(0, rowCountData);
    filterLikeSetData();
    
    postGuiRefreshEdit();
    }
  

private final class SearchLikeTimer extends Timer
  {
  @Override
  public void run()
    {
    filterLikeSetData();
    }
  }


private final class SearchKeyDownHandler implements KeyDownHandler
  {
  @Override
  public void onKeyDown(KeyDownEvent event)
    {
    searchLikeTimerTimer.cancel();
    searchLikeTimerTimer.schedule(1000);
    }
  }


  @Override
  public void postServerRefreshReq()
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwBoardListReq req = new AegwBoardListReq(stat);
    
    service.getBoardList(req, srvCallback);
    }

private final class BoardRefreshCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    w_List = (AegwBoardListRes)result;
  
    if ( isErrorMessage(w_List)) return;
    
    // Since new data has been requested it is fair to say that there is no selected board
    stat.statusBar.setSelectedBoard(null);
    
    parseCallEndDate(w_List);    
    
    postGuiRefreshReq();
    }
  }

private class BoardNewClick implements ClickHandler 
  {
  public void onClick(ClickEvent event) 
    {
    // TODO
//    aethread.clear(board_id);
//    postGuiRefreshEdit();
    }
  }

private final class SaveRowClick implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
//    saveRowPreparePow ();
    }
  }

private class RowSelectionHandler implements SelectionChangeEvent.Handler
  {
  @Override
  public void onSelectionChange(SelectionChangeEvent event) 
    {
    AegwBoard selected = selectionModel.getSelectedObject();
    
    stat.statusBar.setSelectedBoard(selected);
  
    postGuiRefreshEdit();
    }
  }

  }  // END MAIN CLASS
