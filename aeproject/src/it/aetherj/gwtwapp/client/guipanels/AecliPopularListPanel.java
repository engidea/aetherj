/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ListDataProvider;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.gui.AecliGenericCenterPanel;
import it.aetherj.gwtwapp.client.ui.RecliGenericCallback;
import it.aetherj.gwtwapp.client.uict.*;

/**
 * List the popular content bound to your user
 */
public final class AecliPopularListPanel extends AecliGenericCenterPanel
  {
  private static final String classname="AecliPopularListPanel.";
  
  private final SearchLikeTimer searchLikeTimerTimer = new SearchLikeTimer();

  private final TextBox searchBox;

  private final DataGrid<AegwThread> w_cellTable;
  private final ListDataProvider<AegwThread> w_cellTableData;
  private AegwThreadListRes w_List;
  
  
  public AecliPopularListPanel(RecliStat stat)
    {
    super(stat,"Pupular List","popular-list-help");
    
    w_cellTable = newCellTable();
    w_cellTableData = new ListDataProvider<AegwThread>();
    w_cellTableData.addDataDisplay(w_cellTable);    
    
    searchBox = newTextBox(30);
    searchBox.addKeyDownHandler(new SearchKeyDownHandler());

    addNorth(newSearchPanel(),northPanelHeight);
    
    setInnerCenterPanel(w_cellTable);
    }
  
  private Panel newSearchPanel ()
    {
    HorizontalPanel risul = new HorizontalPanel();
    
    risul.add(getGwtLabel("Search"));
    risul.add(searchBox);

    return risul;
    }
    
  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }
  
/*  
  private ColumnImageClick<AegwThread> newEditColumn ()
    {
    CellImageClickHandler clickEdit = new CellImageClickHandler()
      {
      @Override
      public void onClick(Object dataObject, int row)
        {
        if ( !( dataObject instanceof AegwThread)  ) return;
        
        AegwThread impianto = (AegwThread) dataObject;
        
        // set this impianto as the selected one
        stat.statusBar.setSelectedImp(impianto);
        // ask for data refresh
        stat.impiantoEditPage.postServerRefreshReq();
        // show the page
        stat.loggedinPage.setCenterPanel(stat.impiantoEditPage);
        }
      };
    
    CellImageClick editButton = new CellImageClick(clickEdit);
    
    ColumnImageClick<AegwThread> risul = new ColumnImageClick<AegwThread>(editButton) 
      {
      @Override
      public ImageResource getValue(AegwThread contact) { return stat.imageProvider.newEdit;   }
      };

    return risul;  
    }
*/    
  
  
  private ColumnImageClick<AegwThread> newJumpColumn ()
    {
    CellImageClickHandler clickJump = new CellImageClickHandler()
      {
      @Override
      public void onClick(Object dataObject, int row)
        {
        if ( !( dataObject instanceof AegwThread)  ) return;
//        jumpToImpianto((AegwThread) dataObject);
        }
      };
    
    CellImageClick jumpButton = new CellImageClick(clickJump);
      
    ColumnImageClick<AegwThread> risul = new ColumnImageClick<AegwThread>(jumpButton) 
      {
      @Override
      public ImageResource getValue(AegwThread contact) { return stat.imageBoundle.newForward();   }
      };

    return risul;
    }
  
  private void filterLikeSetData ()
    {
    if ( w_List == null ) return;
    
    ArrayList<AegwThread> list = w_List.getLike(searchBox.getText());
    
    w_cellTableData.setList(list);
    }
  
  private ColumnSortEvent.Handler newColumnSortHandler ()
    {
    ColumnSortEvent.Handler risul = new ColumnSortEvent.Handler()
      {
      @Override
      public void onColumnSort(ColumnSortEvent event)
        {
        if ( w_List == null ) return;
        
        String dbcolname = event.getColumn().getDataStoreName();
        
        if ( dbcolname == null ) return;
        
        if ( dbcolname.equals("aemsg_title"))
          w_List.sortImpiantoName(event.isSortAscending());
        
        // the data... see what happens
        filterLikeSetData();
        }
      };
      
    return risul;
    }
  
  private DataGrid<AegwThread> newCellTable ()
    {
    DataGrid<AegwThread> risul = new DataGrid<AegwThread>(100,stat.dataGridResource);

    risul.setAutoHeaderRefreshDisabled(true);
            
    // ---------------------------------------------------------------
    TextColumn<AegwThread> nameColumn = new TextColumn<AegwThread>() 
      {
      @Override
      public String getValue(AegwThread imp) { return imp.thread_name;   }
      };

    nameColumn.setSortable(true);
    nameColumn.setDataStoreName("aemsg_title");
    risul.addColumn(nameColumn, getLabel("Title"));
    
    // ---------------------------------------------------------------
    ColumnImageClick<AegwThread>ajump = newJumpColumn();
    risul.addColumn(ajump);
    risul.setColumnWidth(ajump, 20.0, Unit.PX);    
    
    // ---------------------------------------------------------------
    TextColumn<AegwThread> addressColumn = new TextColumn<AegwThread>() 
      {
      @Override
      public String getValue(AegwThread contact) { return contact.thread_link;   }
      };

    risul.addColumn(addressColumn, getLabel("URL"));
                
    // ---------------------------------------------------------------
    TextColumn<AegwThread> impNote = new TextColumn<AegwThread>() 
      {
      @Override
      public String getValue(AegwThread imp) { return imp.thread_desc;   }
      };

    risul.addColumn(impNote,getLabel("Site Note"));
    
    risul.addColumnSortHandler(newColumnSortHandler());
    
//    risul.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);    
    
    return risul;
  	}

  @Override
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  public void guiClear()
    {
    }

  public void postGuiRefreshReq ()
    {
    if ( w_List == null ) 
      {
      postServerRefreshReq();
      return;
      }

    int rowCountData = w_List.getListSize();
    
    w_cellTable.setRowCount(rowCountData, true);    
    w_cellTable.setVisibleRange(0, rowCountData);
    filterLikeSetData();
		
    }
  


	

private final class ImpiantoDataCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    w_List = (AegwThreadListRes)result;

    if ( isErrorMessage(w_List)) return;
    
    parseCallEndDate(w_List);    
    postGuiRefreshReq();
    }
  }




private final class SearchLikeTimer extends Timer
  {
  @Override
  public void run()
    {
    filterLikeSetData();
    }
  }


private final class SearchKeyDownHandler implements KeyDownHandler
  {
  @Override
  public void onKeyDown(KeyDownEvent event)
    {
    searchLikeTimerTimer.cancel();
    searchLikeTimerTimer.schedule(1000);
    }

  }


@Override
public void postServerRefreshReq()
  {
  // TODO Auto-generated method stub
  
  }


  }  // END MAIN CLASS
