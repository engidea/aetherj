/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.gui.AecliGenericCenterPanel;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;
import it.aetherj.gwtwapp.client.uict.*;
import it.aetherj.protocol.MimPowValue;

/**
 * List the popular content bound to your user
 */
public final class AecliThreadListPanel extends AecliGenericCenterPanel
  {
  private static final String classname="AecliThreadListPanel";

  private final ThreadListDataCallback srvCallback = new ThreadListDataCallback();
  private final SearchLikeTimer searchLikeTimerTimer = new SearchLikeTimer();

  private final TextBox searchBox;
  private final PushButton newThreadButton,saveThreadButton;
  private final RichTextArea richTextArea;
  private final GwuiRichTextToolbar richTextToolbar;
  private final TextBox  threadName,threadLink;
  private final Label ownerName;

  private final DataGrid<AegwThread> w_cellTable;
  private final ListDataProvider<AegwThread> w_cellTableData;
  private AegwThreadListRes w_List;
  
  private final SingleSelectionModel<AegwThread>selectionModel;
  
  public AecliThreadListPanel(RecliStat stat)
    {
    super(stat,"Thread List","thread-list-help");
    
    selectionModel=new SingleSelectionModel<>();

    searchBox  = newTextBox(20, new SearchKeyDownHandler());
    threadName = newTextBox(40);
    threadLink = newTextBox(60);
    ownerName  = new Label();
    
    w_cellTable = newCellTable();
    w_cellTableData = new ListDataProvider<AegwThread>();
    w_cellTableData.addDataDisplay(w_cellTable);    
    
    newThreadButton = stat.utils.newNewButton(new NewThreadClick());
    saveThreadButton = stat.utils.newSaveButton(new SaveRowClick());

    richTextArea = new RichTextArea();
    richTextArea.setWidth("90%");
    richTextToolbar = new GwuiRichTextToolbar(richTextArea);


    addNorth(newNorthPanel(),290);
    
    setInnerCenterPanel(w_cellTable);
    }

  private Panel newNorthPanel ()
    {
    GwuiGrid risul = new GwuiGrid(6,1);
    risul.setWidth("100%");
 
    int rowIndex=0;
    risul.setWidget(rowIndex++, 0, newNorthTopPanel());
    risul.setWidget(rowIndex++, 0, newNorthOwnerPanel());
    risul.setWidget(rowIndex++, 0, newNorthTitlePanel());
    risul.setWidget(rowIndex++, 0, newNorthLinkPanel());
    risul.setWidget(rowIndex++, 0, richTextToolbar);
    risul.setWidget(rowIndex++, 0, richTextArea);

    return risul;
    }

  private Panel newNorthOwnerPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Owner Name"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.add(newLabelSpacer("1em"));
    risul.addValign(ownerName,HasVerticalAlignment.ALIGN_MIDDLE);

    return risul;
    }

  private Panel newNorthTopPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Search"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(searchBox,HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(newThreadButton,HasVerticalAlignment.ALIGN_MIDDLE);

    risul.add(saveThreadButton);

    return risul;
    }
    	
  private Panel newNorthTitlePanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Thread Name"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.add(newLabelSpacer("1em"));
    risul.addValign(threadName,HasVerticalAlignment.ALIGN_MIDDLE);

    return risul;
    }

  private Panel newNorthLinkPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Link"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.add(newLabelSpacer("1em"));
    risul.addValign(threadLink,HasVerticalAlignment.ALIGN_MIDDLE);

    return risul;
    }

  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }
  
  private ColumnImageClick<AegwThread> newJumpColumn ()
    {
    CellImageClickHandler clickJump = new CellImageClickHandler()
      {
      @Override
      public void onClick(Object dataObject, int row)
        {
        jumpToPosts((AegwThread) dataObject);
        }
      };
     
    CellImageClick jumpButton = new CellImageClick(clickJump);
      
    ColumnImageClick<AegwThread> risul = new ColumnImageClick<AegwThread>(jumpButton) 
      {
      @Override
      public ImageResource getValue(AegwThread contact) { return stat.imageBoundle.newForward();   }
      };

    return risul;
    }
  
  public void jumpToPosts ( AegwThread thread )
    {
    stat.statusBar.setSelectedThread(thread);
    
    stat.postTree.postServerRefreshReq();
    
    // in the meantime show this page
    stat.loggedinPage.setCenterPanel(stat.postTree);
    }


  private void filterLikeSetData ()
    {
    if ( w_List == null ) return;
    
    ArrayList<AegwThread> list = w_List.getLike(searchBox.getText());
    
    w_cellTableData.setList(list);
    }
  
  private ColumnSortEvent.Handler newColumnSortHandler ()
    {
    ColumnSortEvent.Handler risul = new ColumnSortEvent.Handler()
      {
      @Override
      public void onColumnSort(ColumnSortEvent event)
        {
        if ( w_List == null ) return;
        
        String dbcolname = event.getColumn().getDataStoreName();
        
        if ( dbcolname == null ) return;
        
        if ( dbcolname.equals("aemsg_title"))
          w_List.sortImpiantoName(event.isSortAscending());
        
        // the data... see what happens
        filterLikeSetData();
        }
      };
      
    return risul;
    }
  

  private ColumnImageClick<AegwThread> newUpvoteColumn ()
    {
    CellImageClickHandler clickHandler = new CellImageClickHandler()
      {
      @Override
      public void onClick(Object dataObject, int row)
        {
        voteAdjust((AegwThread) dataObject, true);
        }
      };
    
    CellImageClick jumpButton = new CellImageClick(clickHandler);
      
    ColumnImageClick<AegwThread> risul = new ColumnImageClick<AegwThread>(jumpButton) 
      {
      @Override
      public ImageResource getValue(AegwThread contact) { return stat.imageBoundle.newUp();   }
      };

    return risul;
    }

  private ColumnImageClick<AegwThread> newDownvoteColumn ()
    {
    CellImageClickHandler clickHandler = new CellImageClickHandler()
      {
      @Override
      public void onClick(Object dataObject, int row)
        {
        voteAdjust((AegwThread) dataObject, false);
        }
      };
    
    CellImageClick jumpButton = new CellImageClick(clickHandler);
      
    ColumnImageClick<AegwThread> risul = new ColumnImageClick<AegwThread>(jumpButton) 
      {
      @Override
      public ImageResource getValue(AegwThread contact) { return stat.imageBoundle.newDown();   }
      };

    return risul;
    }

  private void voteAdjust (AegwThread row, boolean upvote )
    {
    stat.statusBar.setSelectedThread(row);

    stat.logOnBrowser(classname+".voteAdjust: "+row+" upvote="+upvote);
    
    AegwVote aegwVote = new AegwVote();
    aegwVote.vote_board_id  = row.thread_board_id;
    aegwVote.vote_thread_id = row.thread_id;
    aegwVote.vote_up        = upvote;
    
    AecliGenericPowCalculator gpowCalc = new AecliGenericPowCalculator(stat,new SaveVoteCommitPow());
    
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwVoteOprReq req = new AegwVoteOprReq(stat,aegwVote);
    service.postVoteOpr(req, gpowCalc.preparePowCallback);
    }

  
  private DataGrid<AegwThread> newCellTable ()
    {
    DataGrid<AegwThread> risul = new DataGrid<AegwThread>(100,stat.dataGridResource);

    risul.setAutoHeaderRefreshDisabled(true);
            
    // ---------------------------------------------------------------
    TextColumn<AegwThread> titleColumn = new TextColumn<AegwThread>() 
      {
      @Override
      public String getValue(AegwThread imp) { return imp.thread_name;   }
      };

    titleColumn.setSortable(true);
    titleColumn.setDataStoreName("aemsg_title");
    risul.addColumn(titleColumn, getLabel("Title"));
//    risul.setColumnWidth(titleColumn, 40.0, Unit.PCT);    
    
    // ---------------------------------------------------------------
    ColumnImageClick<AegwThread>aup = newUpvoteColumn();
    risul.addColumn(aup);
    risul.setColumnWidth(aup, 20.0, Unit.PX);    

    // ---------------------------------------------------------------
    TextColumn<AegwThread> votesColumn = new TextColumn<AegwThread>() 
      {
      @Override
      public String getValue(AegwThread row) { return ""+row.thread_votes_cnt;   }
      };

    risul.addColumn(votesColumn, getLabel("Votes"));
    risul.setColumnWidth(votesColumn, 3, Unit.EM);    

    // ---------------------------------------------------------------
    ColumnImageClick<AegwThread>adown = newDownvoteColumn();
    risul.addColumn(adown);
    risul.setColumnWidth(adown, 20.0, Unit.PX);    

    // ---------------------------------------------------------------
    TextColumn<AegwThread> postcntColumn = new TextColumn<AegwThread>() 
      {
      @Override
      public String getValue(AegwThread row) { return ""+row.thread_post_cnt;   }
      };

    risul.addColumn(postcntColumn, getLabel("Posts"));
    risul.setColumnWidth(postcntColumn, 3, Unit.EM);    

    // ---------------------------------------------------------------
    ColumnImageClick<AegwThread>ajump = newJumpColumn();
    risul.addColumn(ajump);
    risul.setColumnWidth(ajump, 20.0, Unit.PX);    
        
    // ---------------------------------------------------------------
    TextColumn<AegwThread> urlColumn = new TextColumn<AegwThread>() 
      {
      @Override
      public String getValue(AegwThread contact) { return contact.thread_link;   }
      };

    risul.addColumn(urlColumn, getLabel("URL"));
    risul.setColumnWidth(urlColumn, 30.0, Unit.PCT);    
                
    // ---------------------------------------------------------------
    TextColumn<AegwThread> threadBody = new TextColumn<AegwThread>() 
      {
      @Override
      public String getValue(AegwThread imp) { return imp.getThreadBodyBegin(80);   }
      };

    risul.addColumn(threadBody,getLabel("Content Body"));
    risul.setColumnWidth(threadBody, 30.0, Unit.PCT);    
    
    risul.addColumnSortHandler(newColumnSortHandler());
    
    risul.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);    
    risul.setSelectionModel(selectionModel);
    selectionModel.addSelectionChangeHandler(new RowSelectionHandler());
    
    return risul;
  	}
	
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  public void guiClear()
    {
    w_List = new AegwThreadListRes();
    postGuiRefreshReq();
    }

  private void postGuiRefreshEdit ()
    {
    AegwThread t = stat.statusBar.getThread();
    
    threadName.setText(t.thread_name);
    threadLink.setText(t.thread_link);
    richTextArea.setText(t.thread_body);
    ownerName.setText(t.thread_name);   // TODO make it right
    }
  
  private boolean isListBoundToCurrentBoard()
    {
    int llen = w_List.getListSize();

    // list is empty, so, it is technically bound correctly
    if ( llen <= 0 ) return true;
    
    AegwThread athread = w_List.get(0);
    
    AegwBoard cboard = stat.statusBar.getBoard();
    
    return athread.equalBoard(cboard);
    }

  
  
  public void postGuiRefreshReq ()
    {
    if ( w_List == null || ! isListBoundToCurrentBoard() ) 
      {
      postServerRefreshReq();
      return;
      }
    
    postGuiRefreshEdit();

    int rowCountData = w_List.getListSize();
    
    w_cellTable.setRowCount(rowCountData, true);    
    w_cellTable.setVisibleRange(0, rowCountData);
    filterLikeSetData();
    }
  
  private void saveRowPreparePow()
    {
    // It will be a new thread is previously I had a new
    AegwThread thread = stat.statusBar.getThread();
    
    thread.thread_name = threadName.getText();
    thread.thread_link = threadLink.getText();
    thread.thread_body = richTextArea.getText();
    
    AecliGenericPowCalculator gpowCalc = new AecliGenericPowCalculator(stat,new SaveThreadCommitPow());
    
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwThreadOprReq req = new AegwThreadOprReq(stat,thread);
    service.postThreadOpr(req, gpowCalc.preparePowCallback);
    }
  
  private void newEmptyThread()
    {
    int board_id = stat.statusBar.getBoardId();
  
    if ( board_id <= 0 )
      stat.alert("No Board selected");
    else
      stat.statusBar.setSelectedThread(new AegwThread(board_id));
  
    }
  
private class SaveThreadCommitPow implements AecliSavePowCommitRow
  {
  @Override
  public void saveRowCommitPow(MimPowValue powValue, Integer wpow_id)
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwThreadOprReq req = new AegwThreadOprReq(stat,powValue,wpow_id);
    service.postThreadOpr(req, new SaveCompleteCallback()); 
    }
  }

private class SaveVoteCommitPow implements AecliSavePowCommitRow
  {
  @Override
  public void saveRowCommitPow(MimPowValue powValue, Integer wpow_id)
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwVoteOprReq req = new AegwVoteOprReq(stat,powValue,wpow_id);
    service.postVoteOpr(req, new SaveCompleteCallback()); 
    }
  }

private final class ThreadListDataCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    w_List = (AegwThreadListRes)result;

    if ( isErrorMessage(w_List)) return;
    
    parseCallEndDate(w_List);
    
    newEmptyThread();

    postGuiRefreshReq();
    }
  }

private final class SearchLikeTimer extends Timer
  {
  @Override
  public void run()
    {
    filterLikeSetData();
    }
  }


private final class SearchKeyDownHandler implements KeyDownHandler
  {
  @Override
  public void onKeyDown(KeyDownEvent event)
    {
    searchLikeTimerTimer.cancel();
    searchLikeTimerTimer.schedule(1000);
    }

  }


@Override
public void postServerRefreshReq()
  {
  RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
  int board_id = stat.statusBar.getBoardId();
  AegwThreadListReq req = new AegwThreadListReq(stat,board_id);
  service.getThreadList(req, srvCallback);
  }

private class NewThreadClick implements ClickHandler 
  {
  public void onClick(ClickEvent event) 
    {
    newEmptyThread();
    postGuiRefreshEdit();
    }
  }

private final class SaveRowClick implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    saveRowPreparePow ();
    }
  }


/**
 * This is called when the insert is complete
 */
private class SaveCompleteCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwCommonOprRes res = (AegwCommonOprRes)result;
  
    if ( isErrorMessage(res)) return;
    
    stat.logOnBrowser("message="+res.getFeedbackMessage());
    
    stat.alert("Thread save complete");
    }
  }

private class RowSelectionHandler implements SelectionChangeEvent.Handler
  {
  @Override
  public void onSelectionChange(SelectionChangeEvent event) 
    {
    AegwThread selected = selectionModel.getSelectedObject();
    
    stat.statusBar.setSelectedThread(selected);

    postGuiRefreshEdit();
    }
  }

  }  // END MAIN CLASS
