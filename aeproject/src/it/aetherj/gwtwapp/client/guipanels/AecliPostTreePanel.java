/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import java.util.*;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.gui.AecliGenericCenterPanel;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;
import it.aetherj.protocol.MimPowValue;


/** 
 * Posts in a tree like format
 */
public final class AecliPostTreePanel extends AecliGenericCenterPanel
  {
  private static final String classname="AecliPostTreePanel";
  
  private final PostListDataCallback srvCallback = new PostListDataCallback();
  private final SearchLikeTimer searchLikeTimerTimer = new SearchLikeTimer();
  private final SaveCompleteCallback saveCompleteCallback = new SaveCompleteCallback();
  private final PostClickHandler postClickHandler = new PostClickHandler();

  private final TextBox searchBox,votesBox;
  private final PushButton newPostButton,savePostButton,upvoteButton,downvoteButton,replyButton;
  private final RichTextArea richTextArea;
  private final GwuiRichTextToolbar richTextToolbar;
  private final Label ownerName;
  
  private final PostTreeItem root;
  private final Tree postTree;
  
  private AegwPostListRes w_list;
  
  public AecliPostTreePanel(RecliStat stat)
    {
    super(stat,"Post List","post-list-help");
        
    newPostButton  = stat.utils.newNewButton(postClickHandler);
    savePostButton = stat.utils.newSaveButton(postClickHandler);
    upvoteButton   = stat.utils.newPushButton(stat.imageBoundle.newUp(), postClickHandler, "Up Vote");
    downvoteButton = stat.utils.newPushButton(stat.imageBoundle.newDown(), postClickHandler, "Down Vote");
    replyButton    = stat.utils.newPushButton(stat.imageBoundle.newReply(), postClickHandler, "Reply");

    searchBox = newTextBox(20, new SearchKeyDownHandler());
    votesBox  = new TextBoxInteger(3);
    ownerName = new Label();
    
    richTextArea = new RichTextArea();
    richTextArea.setWidth("90%");
    
    richTextToolbar = new GwuiRichTextToolbar(richTextArea);
    
    addNorth(newNorthPanel(),256);
    
    root = new PostTreeItem("root");

    postTree = new Tree();
    postTree.addItem(root);
    postTree.addSelectionHandler(new TreeSelectionHandler());
    
    setInnerCenterPanel(new ScrollPanel(postTree));
    }
  
  
  private Panel newNorthPanel ()
    {
    GwuiGrid risul = new GwuiGrid(5,1);
    risul.setWidth("100%");
 
    int rowIndex=0;
    risul.setWidget(rowIndex++, 0, newNorthTopPanel());
    risul.setWidget(rowIndex++, 0, newNorthOwnerPanel());
    risul.setWidget(rowIndex++, 0, richTextToolbar);
    risul.setWidget(rowIndex++, 0, richTextArea);

    return risul;
    }

  private Panel newNorthOwnerPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Owner Name"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.add(newLabelSpacer("1em"));
    risul.addValign(ownerName,HasVerticalAlignment.ALIGN_MIDDLE);

    return risul;
    }

  private Panel newNorthTopPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Search"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(searchBox,HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(newPostButton,HasVerticalAlignment.ALIGN_MIDDLE);
    risul.add(savePostButton);
    risul.addValign(upvoteButton,HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(votesBox,HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(downvoteButton,HasVerticalAlignment.ALIGN_MIDDLE);
    risul.add(replyButton);

    return risul;
    }
  
  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }

  private void voteAdjust (boolean upvote )
    {
    AegwPost cp = stat.statusBar.getPost();
    
    if ( ! cp.isValid() )
      {
      stat.alert(classname+".voteAdjust: current selected post invalid");
      return;
      }
    
    stat.logOnBrowser(classname+".voteAdjust: "+cp+" upvote="+upvote);
    
    AegwVote aegwVote = new AegwVote();
    aegwVote.vote_board_id  = cp.post_board_id;
    aegwVote.vote_thread_id = cp.post_thread_id;
    aegwVote.vote_post_id   = cp.post_id;
    aegwVote.vote_up        = upvote;
    
    AecliGenericPowCalculator gpowCalc = new AecliGenericPowCalculator(stat,new SaveVoteCommitPow());
    
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwVoteOprReq req = new AegwVoteOprReq(stat,aegwVote);
    service.postVoteOpr(req, gpowCalc.preparePowCallback);
    }
  

  @Override
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  public void guiClear()
    {
    }

  private void saveRowPreparePow()
    {
    AegwPost cp = stat.statusBar.getPost();
    
    // this is the only thing to fix, the rest should be right...
    cp.post_body = richTextArea.getText();
    
    AecliGenericPowCalculator gpowCalc = new AecliGenericPowCalculator(stat,new SavePostCommitPow());
    
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwPostOprReq req = new AegwPostOprReq(stat,cp,AegwPostOprReq.req_save_prepare_pow);
    service.postPostOpr(req, gpowCalc.preparePowCallback);
    }

  
  private ArrayList<AegwPost> addTopNodes ( )
    {
    AegwPost cp = stat.statusBar.getPost();
    
    int rows = w_list.getListSize();

    ArrayList<AegwPost>remaining = new ArrayList<>(rows);

    root.removeItems();
    
    for ( int index=0; index<rows; index++ )
      {
      AegwPost post = w_list.get(index);

      // if this node has a parent, it is not a top node
      if ( post.post_parent_id != null )
        {
        remaining.add(post);
        continue;
        }
      
      PostTreeItem postTitem = new PostTreeItem(post);
      
      root.addItem(postTitem);
      
      if ( cp.post_id == post.post_id )
        postTitem.setSelected(true);

      }
    
    return remaining;
    }

  /**
   * Add possible children of the nodes
   * @return
   */
  private void addChildren (ArrayList<AegwPost>remaining)
    {
    AegwPost cp = stat.statusBar.getPost();

    Iterator<TreeItem> treeiter = postTree.treeItemIterator();
    
    while ( treeiter.hasNext() )
      {
      boolean added_children=false;
      
      PostTreeItem titem = (PostTreeItem)treeiter.next();
      
      int w_parent_id = titem.getPrimaryId();
      
      for ( Iterator<AegwPost>riter=remaining.iterator(); riter.hasNext(); )
        {
        AegwPost post=riter.next();
        
        if ( post.hasParent(w_parent_id) )
          {
          PostTreeItem n_titem = new PostTreeItem(post);
          titem.addItem(n_titem);
          riter.remove();
          added_children=true;
          
          if ( cp.post_id == post.post_id )
            n_titem.setSelected(true);

          }
        }
      
      if ( added_children )
        titem.setState(true);
      }
    }
    
  private void postGuiRefreshEdit()
    {
    AegwPost cp = stat.statusBar.getPost();

    richTextArea.setText(cp.post_body);
    votesBox.setValue(""+cp.post_votes_cnt);
    ownerName.setText(cp.post_owner_name);
    
    String status="id="+cp.post_id+" p="+cp.post_parent_id+" b="+cp.post_board_id+" t="+cp.post_thread_id;
    stat.logOnBrowser(status);
    }
    
  
  private void postGuiRefreshTree()
    {
    ArrayList<AegwPost>remaining = addTopNodes();
    
    while ( remaining.size() > 0 )
      {
      int previous_len = remaining.size();
      
      addChildren(remaining);
      
      if ( remaining.size() == previous_len )
        {
        stat.alert(classname+".postGuiRefreshReq: weird: previous_len="+previous_len+" unchanged ");
        break;
        }
      }

    root.setState(true);
    
    postTree.ensureSelectedItemVisible();
    }
  
  
  private boolean isListBoundToCurrentThread()
    {
    int llen = w_list.getListSize();

    // list is empty, so, it is technically bound correctly
    if ( llen <= 0 ) return true;
    
    AegwPost apost = w_list.get(0);
    
    AegwThread cthread = stat.statusBar.getThread();
    
    return apost.equalThread(cthread);
    }
  
  /**
   * I need to reconstruct the tree and set as selected show_post_id
   */
  @Override
  public void postGuiRefreshReq ()
    {
    if ( w_list == null || ! isListBoundToCurrentThread() ) 
      {
      // surely if there is no list I have to refresh it
      // but also, I have to make sure that I show the list bound to the correct thread
      postServerRefreshReq();
      return;
      }

    postGuiRefreshEdit();
    postGuiRefreshTree();
    }

  
private class SavePostCommitPow implements AecliSavePowCommitRow
  {
  @Override
  public void saveRowCommitPow(MimPowValue powValue, Integer wpow_id)
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwPostOprReq req = new AegwPostOprReq(stat,powValue,wpow_id);
    service.postPostOpr(req, saveCompleteCallback); 
    }
  }

private class SaveVoteCommitPow implements AecliSavePowCommitRow
  {
  @Override
  public void saveRowCommitPow(MimPowValue powValue, Integer wpow_id)
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwVoteOprReq req = new AegwVoteOprReq(stat,powValue,wpow_id);
    service.postVoteOpr(req, saveCompleteCallback); 
    }
  }
  
  
private final class PostListDataCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    w_list = (AegwPostListRes)result;

    if ( isErrorMessage(w_list)) return;
    
    parseCallEndDate(w_list);
    
    AegwPost cp = stat.statusBar.getPost();
    
    if ( ! cp.isValid() )
      {
      // The idea is that If I do not have a "selected" post a make a new one...
      newEmptyPost();
      }
    
    postGuiRefreshReq();
    }
  }

private final class SearchLikeTimer extends Timer
  {
  @Override
  public void run()
    {
    }
  }


private final class SearchKeyDownHandler implements KeyDownHandler
  {
  @Override
  public void onKeyDown(KeyDownEvent event)
    {
    searchLikeTimerTimer.cancel();
    searchLikeTimerTimer.schedule(1000);
    }

  }


  @Override
  public void postServerRefreshReq()
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    int thread_id=stat.statusBar.getThreadId();
    AegwPostListReq req = new AegwPostListReq(stat,thread_id);
    service.getPostList(req, srvCallback);
    }
  
  private void newEmptyPost() 
    {
    int board_id  = stat.statusBar.getBoardId();
    int thread_id = stat.statusBar.getThreadId();

    if ( board_id <= 0 || thread_id <= 0 )
      {
      stat.alert("invalid from_user_id="+board_id+" or thread_id="+thread_id);
      return;
      }
    
    stat.statusBar.setSelected(new AegwPost(board_id,thread_id));
    }
  
  private void replyPostClick() 
    {
    AegwPost post = stat.statusBar.getPost();
    
    post.prepareReply();
    
    postGuiRefreshEdit();
    }

private final class PostClickHandler implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    Object src=event.getSource();
    
    if ( src==newPostButton )
      {
      newEmptyPost();
      postGuiRefreshEdit();
      }
    else if ( src == savePostButton )
      saveRowPreparePow();
    else if ( src == upvoteButton )
      voteAdjust(true);
    else if ( src == downvoteButton )
      voteAdjust(false);
    else if ( src == replyButton )
      replyPostClick();
    }
  }
 
/**
 * This is called when the insert is complete
 */
private class SaveCompleteCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwCommonOprRes res = (AegwCommonOprRes)result;
  
    if ( isErrorMessage(res)) return;
    
    stat.logOnBrowser("message="+res.getFeedbackMessage());
    
    stat.alert("Post save complete");
    }
  }

private class TreeSelectionHandler implements SelectionHandler<TreeItem>
  {
  @Override
  public void onSelection(SelectionEvent<TreeItem> event)
    {
    TreeItem selected = postTree.getSelectedItem();

    if ( selected == null )
      return;

    AegwPost post = (AegwPost)selected.getUserObject();
    
    stat.statusBar.setSelected(post);

    postGuiRefreshEdit();
    }
  }

public static class PostTreeItem extends TreeItem
  {
  private final AegwPost post;
  
  /**
   * This is here for the root node
   */
  public PostTreeItem()
    {
    this(new AegwPost());
    }
  
  public PostTreeItem(String label)
    {
    this();
    super.setText(label);
    }
  
  public PostTreeItem(AegwPost post)
    {
    this.post=post;
    super.setText(post.getPostVotesBodyBegin(80));
    }
  
  public int getPrimaryId ()
    {
    return post.post_id;
    }
  
  @Override
  public AegwPost getUserObject()
    {
    return post;
    }
  }

  }  // END MAIN CLASS
