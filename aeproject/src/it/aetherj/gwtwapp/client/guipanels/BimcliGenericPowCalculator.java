package it.aetherj.gwtwapp.client.guipanels;

import com.google.gwt.core.client.Scheduler;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.AegwCommonOprRes;
import it.aetherj.gwtwapp.client.ui.RecliGenericCallback;
import it.aetherj.protocol.*;

/**
 * I need this since the POW calculator for Bim is different than for Aether
 */
public class BimcliGenericPowCalculator 
  {
  private final RecliStat stat;
  private final AecliSavePowCommitRow pow_saver;
  
  public final PreparePowCallback preparePowCallback = new PreparePowCallback();

  public BimcliGenericPowCalculator(RecliStat stat, AecliSavePowCommitRow pow_saver)
    {
    this.stat=stat;
    this.pow_saver=pow_saver;
    }
  
/**
 * SInce I need to have the POW to be done here, on browser, I will receive the POW source and will send back the basic part of POW
 */
private class PreparePowCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwCommonOprRes res = (AegwCommonOprRes)result;
  
    if ( isErrorMessage(res)) return;

    Scheduler scheduler = Scheduler.get();
    
    RepeatingPow cmd = new RepeatingPow(res.hashForPow, res.wpow_id,res.w_pow_length);
    
    scheduler.scheduleIncremental(cmd);
    }
  }

  
protected class RepeatingPow implements  Scheduler.RepeatingCommand
  {
  private final byte[] sourceHash;
  private final BimPowCalculator calculator;
  private final MimPowValue powValue;
  private final Integer wpow_id;
  
  public RepeatingPow ( byte [] sourceHash, Integer wpow_id, int w_pow_length )
    {
    this.calculator=new BimPowCalculator();
    this.powValue=new MimPowValue(w_pow_length,calculator.newRandomSeed());
    
    this.sourceHash=sourceHash;
    this.wpow_id=wpow_id;
    
    stat.logOnBrowser("RepeatingPow: START wpow_id="+wpow_id+" pow_length="+w_pow_length);
    }
  
  @Override 
  public boolean execute()
    {
    boolean keepRunning = calculator.executePowUnsignedStep(powValue, sourceHash);
    
    if ( keepRunning )
      {
      stat.statusBar.postPowFeedback(powValue);
      }
    else
      {
      stat.logOnBrowser("POW counter="+powValue.getCounter());
      stat.statusBar.postPowFeedback(null);
      pow_saver.saveRowCommitPow(powValue, wpow_id);
      }

    return keepRunning;
    }
  
  }

  
  }
