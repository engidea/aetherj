/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.gui.AecliGenericCenterPanel;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;
import it.aetherj.gwtwapp.client.uict.*;

/**
 * List the popular content bound to your user
 */
public final class AecliRecentListPanel extends AecliGenericCenterPanel
  {
  private static final String classname="AecliRecentListPanel.";
  
  private final RecentListCallback srvCallback = new RecentListCallback();
  private final JumpDataReadyCallback jumpCallback = new JumpDataReadyCallback();
  private final SearchLikeTimer searchLikeTimerTimer = new SearchLikeTimer();

  private final TextBox searchBox;

  private final DataGrid<AegwPost> w_cellTable;
  private final ListDataProvider<AegwPost> w_cellTableData;
  private final RichTextArea richTextArea;
  private final GwuiRichTextToolbar richTextToolbar;
  private final SingleSelectionModel<AegwPost>selectionModel;
  
  private AegwRecentListRes w_List;

  // do not let it become null
  private AegwPost aepost = new AegwPost();
  
  public AecliRecentListPanel(RecliStat stat)
    {
    super(stat,"Recent List","recent-list-help");
    
    selectionModel=new SingleSelectionModel<>();

    w_cellTable = newCellTable();
    w_cellTableData = new ListDataProvider<AegwPost>();
    w_cellTableData.addDataDisplay(w_cellTable);    
    
    searchBox = newTextBox(30);
    searchBox.addKeyDownHandler(new SearchKeyDownHandler());
    
    richTextArea = new RichTextArea();
    richTextArea.setWidth("90%");
    
    richTextToolbar = new GwuiRichTextToolbar(richTextArea);
    
    addNorth(newNorthPanel(),256);
    
    setInnerCenterPanel(w_cellTable);
    }
  
  
  private Panel newNorthPanel ()
    {
    GwuiGrid risul = new GwuiGrid(3,1);
    risul.setWidth("100%");
 
    int rowIndex=0;
    risul.setWidget(rowIndex++, 0, newNorthTopPanel());
    risul.setWidget(rowIndex++, 0, richTextToolbar);
    risul.setWidget(rowIndex++, 0, richTextArea);

    return risul;
    }

  private Panel newNorthTopPanel ()
    {
    GwuiHorizontalPanel risul = new GwuiHorizontalPanel();
    
    risul.addValign(getGwtLabel("Search"),HasVerticalAlignment.ALIGN_MIDDLE);
    risul.addValign(searchBox,HasVerticalAlignment.ALIGN_MIDDLE);

    return risul;
    }
  	
  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }
  
  private void filterLikeSetData ()
    {
    if ( w_List == null ) return;
    
    ArrayList<AegwPost> list = w_List.getLike(searchBox.getText());
    
    w_cellTableData.setList(list);
    }
  
  private ColumnSortEvent.Handler newColumnSortHandler ()
    {
    ColumnSortEvent.Handler risul = new ColumnSortEvent.Handler()
      {
      @Override
      public void onColumnSort(ColumnSortEvent event)
        {
        if ( w_List == null ) return;
        
        String dbcolname = event.getColumn().getDataStoreName();
        
        if ( dbcolname == null ) return;
        
        if ( dbcolname.equals("aemsg_title"))
          w_List.sortPostBody(event.isSortAscending());
        
        // the data... see what happens
        filterLikeSetData();
        }
      };
      
    return risul;
    }
  
  private ColumnImageClick<AegwPost> newJumpColumn ()
    {
    CellImageClickHandler clickJump = new CellImageClickHandler()
      {
      @Override
      public void onClick(Object dataObject, int row)
        {
        jumpToPost((AegwPost) dataObject);
        }
      };
    
    CellImageClick jumpButton = new CellImageClick(clickJump);
      
    ColumnImageClick<AegwPost> risul = new ColumnImageClick<AegwPost>(jumpButton) 
      {
      @Override
      public ImageResource getValue(AegwPost contact) { return stat.imageBoundle.newForward();   }
      };

    return risul;
    }
  
  /**
   * I need to retrieve the full info on the post, Board and Thread and then I can jump...
   * @param post
   */
  public void jumpToPost ( AegwPost post )
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();

    AegwPostOprReq req = new AegwPostOprReq(stat, post,AegwPostOprReq.req_get_details);
    
    service.postPostOpr(req, jumpCallback);
    }
  
  
  
  private DataGrid<AegwPost> newCellTable ()
    {
    DataGrid<AegwPost> risul = new DataGrid<AegwPost>(100,stat.dataGridResource);

    risul.setAutoHeaderRefreshDisabled(true);

    // ---------------------------------------------------------------
    TextColumn<AegwPost> votesColumn = new TextColumn<AegwPost>() 
      {
      @Override
      public String getValue(AegwPost row) { return ""+row.post_votes_cnt;   }
      };

    risul.addColumn(votesColumn, getLabel("Votes"));
    risul.setColumnWidth(votesColumn, 3, Unit.EM);    
 
    // ---------------------------------------------------------------
    ColumnImageClick<AegwPost>ajump = newJumpColumn();
    risul.addColumn(ajump);
    risul.setColumnWidth(ajump, 20.0, Unit.PX);    
    
    // ---------------------------------------------------------------
    TextColumn<AegwPost> bodyColumn = new TextColumn<AegwPost>() 
      {
      @Override
      public String getValue(AegwPost row) { return row.getPostVotesBodyBegin(200);   }
      };

    bodyColumn.setSortable(true);
    bodyColumn.setDataStoreName("aemsg_title");
    risul.addColumn(bodyColumn, getLabel("Post Content"));
        
    // ---------------------------------------------------------------
    TextColumn<AegwPost> parentIdColumn = new TextColumn<AegwPost>() 
      {
      @Override
      public String getValue(AegwPost contact) { return ""+contact.post_parent_id;   }
      };

    risul.addColumn(parentIdColumn, getLabel("Parent"));
    risul.setColumnWidth(parentIdColumn, 4.0, Unit.EM);    
    
    risul.addColumnSortHandler(newColumnSortHandler());
    
    risul.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);    
    
    risul.setSelectionModel(selectionModel);
    selectionModel.addSelectionChangeHandler(new RowSelectionHandler());
    
    return risul;
    }
	
  @Override
  public String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  public void guiClear()
    {
    }

  public void postGuiRefreshReq ()
    {
    if ( w_List == null ) 
      {
      postServerRefreshReq();
      return;
      }

    int rowCountData = w_List.getListSize();
    
    w_cellTable.setRowCount(rowCountData, true);    
    w_cellTable.setVisibleRange(0, rowCountData);
    filterLikeSetData();
		
    }

  @Override
  public void postServerRefreshReq()
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    int thread_id=stat.statusBar.getThreadId();
    AegwRecentListReq req = new AegwRecentListReq(stat);
    service.getRecentList(req, srvCallback);
    }

private final class RecentListCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    w_List = (AegwRecentListRes)result;

    if ( isErrorMessage(w_List)) return;
    
    parseCallEndDate(w_List);    
    postGuiRefreshReq();
    }
  }

private final class JumpDataReadyCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwPostOprRes risul = (AegwPostOprRes)result;
  
    if ( isErrorMessage(risul)) return;

    // this will set the "current selected post"
    stat.statusBar.setSelected(risul.aeboard,risul.aethread,risul.aepost);
    
    // this will ask to refresh data
    stat.postTree.postServerRefreshReq();
    
    // in the meantime show this page
    stat.loggedinPage.setCenterPanel(stat.postTree);
    }
  }

private final class SelectDataReadyCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwPostOprRes risul = (AegwPostOprRes)result;
  
    if ( isErrorMessage(risul)) return;
  
    stat.statusBar.setSelected(risul.aeboard,risul.aethread);
    }
  }



private final class SearchLikeTimer extends Timer
  {
  @Override
  public void run()
    {
    filterLikeSetData();
    }
  }


private final class SearchKeyDownHandler implements KeyDownHandler
  {
  @Override
  public void onKeyDown(KeyDownEvent event)
    {
    searchLikeTimerTimer.cancel();
    searchLikeTimerTimer.schedule(1000);
    }

  }


private class RowSelectionHandler implements SelectionChangeEvent.Handler
  {
  @Override
  public void onSelectionChange(SelectionChangeEvent event) 
    {
    AegwPost selected = selectionModel.getSelectedObject();
    if ( selected == null )
      return;
    
    aepost = selected;
    
    richTextArea.setText(selected.post_body);
    
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwPostOprReq req = new AegwPostOprReq(stat, aepost, AegwPostOprReq.req_get_details);
    service.postPostOpr(req, new SelectDataReadyCallback());
    }
  }



  }  // END MAIN CLASS
