/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

public class AegwPostOprRes extends AegwCommonOprRes
  {
  private static final long serialVersionUID = 1L;

  public AegwBoard  aeboard;   // full detail of the board this post belong to, may be null
  public AegwThread aethread;  // full detail of the thread this post belong to, may be null
  public AegwPost   aepost;    // the update aepost, if any
  
  public AegwPostOprRes()
    {
    w_pow_length=20;
    }
  
  public AegwPostOprRes ( AegwPost aepost )
    {
    this();
    this.aepost=aepost;
    }
  }
