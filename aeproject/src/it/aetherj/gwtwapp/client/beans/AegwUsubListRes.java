/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.util.*;

import com.google.gwt.regexp.shared.RegExp;

public class AegwUsubListRes extends CommonListRes<AegwUsub>
  {
  private static final long serialVersionUID = 1L;
  
  public void sortBoardName (boolean ascending)
    {
    if (itemsList == null ) return;
    
    Comparator<AegwUsub> compara;
    
    if ( ascending )
      {
      compara = new Comparator<AegwUsub>() 
        {
        @Override
        public int compare(AegwUsub o1, AegwUsub o2)
          {
          return compareString(o1.board_name, o2.board_name);
          }
         };
      }
    else
      {
      compara = new Comparator<AegwUsub>() 
        {
        @Override
        public int compare(AegwUsub o1, AegwUsub o2)
          {
          return -compareString(o1.board_name, o2.board_name);
          }
        };
      }

    Collections.sort(itemsList,compara);
    }
  

  
  public ArrayList<AegwUsub> getLike ( String like )
    {
    if ( like == null || like.length() < 1 ) return super.getList();
    
    RegExp regExp = RegExp.compile(like, "i");
    
    return getLike ( regExp);
    }
  
  
  public ArrayList<AegwUsub> getLike ( RegExp regExp )
    {
    int listSize = super.getListSize();
    
    ArrayList<AegwUsub> risul = new ArrayList<AegwUsub>(listSize);
    
    for ( int index=0; index<listSize; index++ )
      {
      AegwUsub imp = super.get(index);
      
      if ( imp.like ( regExp )) risul.add(imp);
      }
    
    return risul;
    }
  
  
  

  }
