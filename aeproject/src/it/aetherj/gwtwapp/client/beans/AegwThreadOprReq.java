/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.protocol.MimPowValue;

/**
 * An operation on thread, can be get, save
 * The POW must be calculated in the web client if we wish to have any kind of possibility of using more than one client
 * So, client sent the thread to be saved with flag req_save_prepare_pow, server reply with info for client to prepare pow
 * the next request is for client to send with flag req_save_using_pow
 */
public class AegwThreadOprReq extends CommonReq 
  {
	private static final long serialVersionUID = 1L;
	
	public static final int req_get_details=0;       // by default just get the details of a given thread
	public static final int req_save_prepare_pow=1;  // client ask for pow preparation
	public static final int req_save_using_pow=2;    // client is giving a pow for the given thread
	
	public int req_code;   // one of the two above 
	
  public int board_id;
  
  public AegwThread aethread;
  
  public MimPowValue mimPowValue;   // this is the POW that the web client has calculated
  public Integer wpow_id;           // you can pick up the pieces from this wpow_id row
  
  public AegwThreadOprReq()
    {
    }
  
  public AegwThreadOprReq(RecliStat stat, AegwThread aethread)
    {
    super(stat);      
    req_code=req_save_prepare_pow;
    this.aethread = aethread;
    }
  
  public AegwThreadOprReq (RecliStat stat, MimPowValue mimPowValue,  Integer wpow_id)
    {
    super(stat);
    req_code=req_save_using_pow;
    this.mimPowValue=mimPowValue;
    this.wpow_id=wpow_id;
    }

  public boolean isPreparePow ()
    {
    return req_code == req_save_prepare_pow;
    }

  public boolean isUsingPow ()
    {
    return req_code == req_save_using_pow;
    }

  }
