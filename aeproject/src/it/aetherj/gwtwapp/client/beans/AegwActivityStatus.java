/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * I need a way to sent to client when is the last time an "activity" has been completed with regard to a well defined page
 * Basically a client will ask to server: give me the last update date on a given gwtIdent
 * When a client requests an action to the server it MUST obviously send the same gwtIdent to server so the hashmap can be updated
 * Clearly once the server reply with a date it is duty of the client to check if there is a new date and do a refresh
 * This is here so it is shared in both GWT, the servlets and backend
 */
public class AegwActivityStatus implements Serializable
	{
	private static final long serialVersionUID = 5518929859056732698L;

	public String gwtIdent;
  public Date   updateDate;
  
  public AegwActivityStatus ( )
  	{
  	// for serializable
  	}
  
  public AegwActivityStatus ( String gwtIdent )
  	{
  	this.gwtIdent = gwtIdent;
  	}
  
  
  public String toString()
  	{
  	return gwtIdent+"."+updateDate;
  	}
	}
