/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;

import com.google.gwt.regexp.shared.RegExp;

/**
 * A Post
 */
public class AegwPost implements Serializable
  {
  private static final long serialVersionUID = 1L;
  
  public int       post_id;           // used when I am saving the translation
  public int       post_thread_id;    // 
  public int       post_board_id;     // The board it is attached to
  public int       post_owner_id;     // not null

  public Integer   post_parent_id;    // may be null, to do a tree, parent id of the given post
  
  public String    post_body="";
  public String    post_owner_name="";   
  
  public int       post_votes_cnt;
  
  public AegwPost()
    {
    // for GWT
    }
  
  public AegwPost( int board_id, int thread_id )
    {
    post_board_id=board_id;
    post_thread_id=thread_id; 
    }
  
  public AegwPost( int board_id, int thread_id, int post_if )
    {
    this(board_id,thread_id);
    post_id = post_if;
    }

  public void setFrom ( AegwPost src )
    {
    post_id        =src.post_id;
    post_board_id  =src.post_board_id;
    post_thread_id =src.post_thread_id;
    post_body      =src.post_body;
    post_votes_cnt =src.post_votes_cnt;
    post_parent_id =src.post_parent_id;
    }
  
  public void clear ( int board_id, int thread_id )
    {
    post_id=CommonReq.NULL_int;
    post_board_id=board_id;
    post_thread_id=thread_id;
    post_parent_id=null;
    post_body="";
    post_votes_cnt=0;
    }

  public boolean equalThread ( AegwThread other )
    {
    if ( other == null ) return false;
    
    return post_thread_id == other.thread_id;
    }
  
  /**
   * To prepare params for a reply creation, just this should be cleaned up
   * For a reply to CURRENT post the parent must become this post id
   */
  public void prepareReply ()
    {
    // the new post parent is this post id
    post_parent_id=post_id;
    // this post id becomes null, to create a new post
    post_id=CommonReq.NULL_int;
    // let the user write a new body
    post_body="";
    // and, just to clean things up, no votes...
    post_votes_cnt=0;
    }
  
  public boolean isValid ()
    {
    return post_id != 0 && post_board_id != 0 && post_thread_id != 0;
    }
  
  public boolean hasParent ( int parent_id )
    {
    if ( post_parent_id == null )
      return false;
    
    return post_parent_id.intValue() == parent_id;
    }
  
  /**
   * Return just the beginning of a post body
   */
  public String getPostVotesBodyBegin ( int len )
    {
    if ( post_body == null )
      return "";

    StringBuilder risul = new StringBuilder(len+20);

    risul.append('[');
    risul.append(post_votes_cnt);
    risul.append("] ");
    
    if ( len >= post_body.length() )
      risul.append(post_body);
    else
      risul.append(post_body.substring(0, len));
    
    return risul.toString();
    }

  
  
  
  public boolean like ( RegExp expr )
    {
    if ( expr.test(post_body)) return true;
    
    if ( expr.test(post_owner_name)) return true;
    
    return false;
    }


  public final String toString()
    {
    return "id="+post_id+" owner="+post_owner_name;
    }
  }
