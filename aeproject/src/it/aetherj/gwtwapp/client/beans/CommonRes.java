package it.aetherj.gwtwapp.client.beans;

public class CommonRes extends ServiceFeedback 
  {
  private static final long serialVersionUID = 1L;

  public static final int NULL_int=0;   // a null value when needed as integer is this
  
  public String system_feedback_msg;    // possibly a not null message to display to the user

  
  public boolean hasFeedbackMessage ()
    {
    return system_feedback_msg != null;
    }
  
  public String getFeedbackMessage ()
    {
    return system_feedback_msg;
    }
  }
