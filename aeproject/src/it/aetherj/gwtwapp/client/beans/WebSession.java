/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;

/**
 * This class holds the user authentication information
 * This basically means that this class MUST be passed to each service that requires some sort of authentication
 * NOTE: this is NOT a class holding parameters for the FIRST user authentication, it is what must be passed back and forth on FOLLOWING authentication !
 * 
 * @author damiano
 */
public final class WebSession implements Serializable
  {
  private static final long serialVersionUID = 1L;
  
  public Integer primary_id;     // Uniquely identify the user in a Dbase, MUST be unique
  public long    sessionToken;   // this is the current token to be passed to obtain services
	public boolean wana_read_only; // if true then this wana can only view locally acquired data
	
  public WebSession()
    {
    // needed to satisfy Serializable interface
    }

  public WebSession(Integer wana_id)
    {
    this.primary_id = wana_id;
    }

  @Override
  public String toString()
    {
    return "WebSession: user_id=" + primary_id + " Token=" + sessionToken;
    }
  
  @Override
  public boolean equals(Object other)
    {
    try
      {
			if ( other == null ) return false;
			
			if ( ! (other instanceof WebSession) ) return false;

      WebSession asession = (WebSession) other;
						
			// if for any reason the to primary id are null
			if ( primary_id == null || asession.primary_id == null ) return false;
			
			// the primary id are not null, but are they the same ?
      if ( !primary_id.equals(asession.primary_id)) return false;

			// and is the token the same ?
      if (sessionToken != asession.sessionToken) return false;

      return true;
      }
    catch (Exception exc)
      {
      return false;
      }
    }

  @Override
  public int hashCode()
    {
    int hash = 3;
    hash = 79 * hash + (this.primary_id != null ? this.primary_id.hashCode() : 0);
    hash += 79 * hash + (int)sessionToken;
    return hash;
    }

  }
