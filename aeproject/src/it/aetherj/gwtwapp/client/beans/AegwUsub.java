/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;
import java.sql.Timestamp;

import com.google.gwt.regexp.shared.RegExp;

/**
 * Web user subscription to a Sub
 */
public class AegwUsub implements Serializable
  {
  private static final long serialVersionUID = 1L;
  
  public static final String cn_filename="cn_filename";
  
  public Integer   wusub_id;           // used when I am saving the translation
  public Integer   wusub_wuser_id;     // this sub belongs to this user
  public Integer   wusub_board_id;
  public String    board_fingerprint;  // used when the board id is not known
  public String    board_name;         // used when returning the board name
  public Boolean   wusub_notify;       // if true then notify user of events
  public Timestamp wusub_last_seen;    // Possibly input only, not really...
  public String    wusub_desc;         // 
  
  public volatile boolean isUpdated;    // used in GUI to know if a content has changed
  
  public boolean like ( RegExp expr )
    {
    if ( expr.test(board_name)) return true;
    
    return false;
    }
  
  public final String toString()
    {
    return "id="+wusub_id+" desc="+wusub_desc;
    }
  }
