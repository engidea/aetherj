/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.util.*;

import com.google.gwt.regexp.shared.RegExp;

public class AegwChatListRes extends CommonListRes<AegwChat>
  {
  private static final long serialVersionUID = 1L;

  public void sortImpiantoName (boolean ascending)
    {
    if (itemsList == null ) return;
    
    Comparator<AegwChat> compara;
    
    if ( ascending )
      {
      compara = new Comparator<AegwChat>() 
        {
        @Override
        public int compare(AegwChat o1, AegwChat o2)
          {
          return compareString(o1.chat_message, o2.chat_message);
          }
         };
      }
    else
      {
      compara = new Comparator<AegwChat>() 
        {
        @Override
        public int compare(AegwChat o1, AegwChat o2)
          {
          return -compareString(o1.chat_message, o2.chat_message);
          }
        };
      }

    Collections.sort(itemsList,compara);
    }
  

  
  public ArrayList<AegwChat> getLike ( String like )
    {
    if ( like == null || like.length() < 1 ) return super.getList();
    
    RegExp regExp = RegExp.compile(like, "i");
    
    return getLike ( regExp);
    }
  
  
  public ArrayList<AegwChat> getLike ( RegExp regExp )
    {
    int listSize = super.getListSize();
    
    ArrayList<AegwChat> risul = new ArrayList<AegwChat>(listSize);
    
    for ( AegwChat imp : itemsList )
      if ( imp.like ( regExp )) 
        risul.add(imp);
    
    return risul;
    }
  
  
  

  }
