/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.util.ArrayList;

import it.aetherj.gwtwapp.client.RecliStat;

public class CommonListReq<T> extends CommonReq
  {
	private static final long serialVersionUID = 1L;
	
	protected ArrayList<T> itemsList;
  
  public CommonListReq()
    {
    itemsList = new ArrayList<T>();
    }
  
  public CommonListReq(RecliStat stat )
    {
    super(stat);
    
    itemsList = new ArrayList<T>();
    }
  
  public int getListSize ()
    {
    return itemsList.size();    
    }
  
  public void add ( T item )
    {
    itemsList.add(item);
    }
  
  public T get ( int rowIndex )
    {
    return itemsList.get(rowIndex);  
    }
  
  public void set ( int index, T value )
    {
    itemsList.set(index,value);    
    }
  
  @Override
  public String toString ()
    {
    return " list size="+getListSize();  
    }
  }
