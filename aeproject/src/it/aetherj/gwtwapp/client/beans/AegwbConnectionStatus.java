/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.util.Date;

public class AegwbConnectionStatus extends ServiceFeedback
  {
	private static final long serialVersionUID = 1L;
	
  public String    imp_msg;             // can be null
  public String    imp_msg_error;       // can be null
  public Date      imp_msg_date;        // when last message has been updated
  
  public AegwActivityStatus activityStatus;   // reported info on this activity status 
  }
