/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;
import java.sql.Date;

import com.google.gwt.regexp.shared.RegExp;

/**
 * A Thread, note that the list of subscribed thread is something else
 */
public class AegwBoard implements Serializable
  {
  private static final long serialVersionUID = 1L;
  
  public int       board_id;           
  public String    board_name="";
  public String    board_owner_name="";
  public String    board_desc="";         // can be null to indicate that the value is label_english
  
  public int       board_treads_cnt;   // how many threads it has
  public Date      board_thlastd;      // the last modified date of the thread
  
  public AegwBoard()
    {
    // for serialization
    }

  public AegwBoard(int board_id, String board_name)
    {
    this.board_id=board_id;
    this.board_name=board_name;
    }
  
  public boolean like ( RegExp expr )
    {
    if ( expr.test(board_name)) return true;
    
    if ( expr.test(board_owner_name)) return true;
    
    if ( expr.test(board_desc)) return true;
    
    return false;
    }
  
  public boolean equals(Object object)
    {
    if ( object instanceof AegwBoard )
      {
      AegwBoard other =(AegwBoard)object;
      return board_id==other.board_id;
      }
  
    return false;
    }
  
  

  public String getBoardNameBegin ( int len )
    {
    if ( board_name == null )
      return "";
    
    if ( len >= board_name.length() )
      return board_name;
    
    return board_name.substring(0, len);
    }

  public String getBoardDescBegin ( int len )
    {
    if ( board_desc == null )
      return "";
    
    if ( len >= board_desc.length() )
      return board_desc;
    
    return board_desc.substring(0, len);
    }
  
  
  public final String toString()
    {
    return "id="+board_id+" eng="+board_name;
    }

  @Override
  public int hashCode()
    {
    return board_id;
    }
  
  }
