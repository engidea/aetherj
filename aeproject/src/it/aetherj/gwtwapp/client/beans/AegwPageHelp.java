/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import com.google.gwt.user.client.rpc.IsSerializable;

public final class AegwPageHelp implements IsSerializable
  {
  public static final String default_key="aetherj_man_pdf";
  
  public static final String  cn_tbl    = "mlang.pagehelp_tbl";  
  public static final String  cn_id     = "pagehelp_id";
  public static final String  cn_key    = "pagehelp_key";
  public static final String  cn_filename = "pagehelp_filename";
  public static final String  cn_lang   = "pagehelp_lang";
  public static final String  cn_desc   = "pagehelp_desc";
  public static final String  cn_pdf    = "pagehelp_pdf";
  
  
  public Integer pagehelp_id;
  public String  pagehelp_lang;
  public String  pagehelp_key;
  public String  pagehelp_desc;
  public String  pagehelp_filename;
  public byte[]  pagehelp_pdf;
  
  public String toString()
    {
    StringBuilder risul = new StringBuilder();
    
    risul.append("pagehelp_id="+pagehelp_id);
    risul.append(" desc="+pagehelp_desc);
    risul.append(" fname="+pagehelp_filename);
    if ( pagehelp_pdf != null ) risul.append(" pdf len="+pagehelp_pdf.length);
    

    return risul.toString();
    }
  }
