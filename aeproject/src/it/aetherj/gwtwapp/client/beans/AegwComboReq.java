/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import it.aetherj.gwtwapp.client.RecliStat;

/**
 * This request a combo values from the server
 * Uff, I need to differentiate between integer and string keys since GWT does not handle Object nicely AND comparing a string and an int gves
 * different result.... (meaning that the key must be of the same type os the list)
 */
public class AegwComboReq extends CommonReq
  {
	private static final long serialVersionUID = 1L;
	
	public static final int key_type_integer=1;
  public static final int key_type_string=2;
  
	public static final int combo_selector_null=0;       // combo selector is not used
	public static final int combo_selector_first=1;      // 
	public static final int combo_selector_second=2;     // 
	
	private int combo_selector=combo_selector_null;    // assume no combo selector
  public  int combo_key_type=key_type_integer;   // normally an integer key
	
  public String combo_tbl;                // filled at server side
  public String key_col;                  // filled at server side  
  public String value_col;                // filled at server side
  public String select_key;               // filled at server side
  public String select_and_key;           // filled at server side, if needed
  
  public Integer select_value_integer;    // if the values are the result of a subselect this is the value 
  public String  select_value_string;     // if the values are the result of a subselect this is the value
  public int select_and_value;               
  
  public AegwComboReq()
    {
    // needed for serializable...      
    }
	
	public AegwComboReq(RecliStat stat,int combo_selector)
    {
    super(stat);	
		this.combo_selector = combo_selector;
		}
	
	
	public int getComboSelector()
		{
		return combo_selector;			
		}
  }
