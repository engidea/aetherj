/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.protocol.MimPowValue;

/**
 * When creating a new user there is work to do to create private keys + fingerprint and POW
 * Unfortunately, I cannot move all of it on the web since ...... crap compatibility
 * Once upon a time, there where java applets but thanks to Microsoft screaming.... they got dumped
 * Now, .... new and worse, web assembly... welcome to clown world
 * So, this is used for when to create a new user from scratsh. not for when importing an existing Aether user
 */
public class AegwUkeyOprReq extends CommonReq 
  {
	private static final long serialVersionUID = 1L;
	
	public static final int req_get_details=0;       // by default just get the details of a given record
	public static final int req_save_prepare_pow=1;  // client ask for pow preparation
	public static final int req_save_using_pow=2;    // client is giving a pow for the given thread
	
	public int req_code;              // one of the above, assume I want details 
	
  public AegwUkey aeukey;           // if empty it will mean to operate on the current logged in user id
  
  public MimPowValue mimPowValue;   // this is the POW that the web client has calculated
  public Integer wpow_id;           // you can pick up the pieces from this wpow_id row
  
  public AegwUkeyOprReq()
    {
    // for GWT
    }
  
  /**
   * Use this to retrieve user info for current user
   */
  public AegwUkeyOprReq(RecliStat stat)
    {
    super(stat);
    aeukey=new AegwUkey();
    req_code=req_get_details;
    }
  
  /**
   * use this when you wish to prepare a POW for a new user
   */
  public AegwUkeyOprReq(RecliStat stat, AegwUkey aeukey )
    {
    super(stat);      
    this.aeukey = aeukey;
    this.req_code = req_save_prepare_pow;
    }
  
  /**
   * Use this when you want to save a pow
   * @param mimPowValue
   * @param wpow_id
   */
  public AegwUkeyOprReq (RecliStat stat, MimPowValue mimPowValue,  Integer wpow_id)
    {
    super(stat);
    this.req_code = req_save_using_pow;
    this.mimPowValue=mimPowValue;
    this.wpow_id=wpow_id;
    }

  public boolean isPreparePow ()
    {
    return req_code == req_save_prepare_pow;
    }

  public boolean isUsingPow ()
    {
    return req_code == req_save_using_pow;
    }
  }
