/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;

/**
 * Web user
 * I need this to be able to send data info on  a web user back and forth
 */
public class AegwUser implements Serializable
  {
  private static final long serialVersionUID = 1L;
  
  public Integer   wuser_id;           // if null then this is a new Wuser
  public Integer   wuser_ukey_id;      // This user refers to this ukey 
  public String    wuser_login;
  public String    wuser_password;      // can be null to indicate that the value is label_english
  public String    wuser_note;        
  
  public volatile boolean isUpdated;    // used in GUI to know if a content has changed
  
  public AegwUser()
    {
    }
 
  public AegwUser(Integer wuser_id, String login, String password )
    {
    this.wuser_id=wuser_id;
    this.wuser_login=login;
    this.wuser_password=password;
    }
  
  
  public boolean hasLogin ()
    {
    return wuser_login != null && wuser_login.length() >= 4;
    }
  
  public boolean hasPassword ()
    {
    return wuser_password != null && wuser_password.length() >= 8;
    }
  
  public final String toString()
    {
    return "id="+wuser_id+" login="+wuser_login;
    }
  }
