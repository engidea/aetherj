/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Include this class into the response to have feedback on what happened
 * Of course if service feedback is null then there is no feedback !
 */
public class ServiceFeedback implements Serializable
  {
	private static final long serialVersionUID = 1L;
	
	private String serviceEnglishErrorMessage;
  private Timestamp service_start_date;
  private Timestamp callEndDate;

  /**
   * This is the normal constructor, assuming that there is no error to report
   */
  public ServiceFeedback()
    {
    this.service_start_date = new Timestamp(System.currentTimeMillis());
    }
  
  /**
   * If an error occurs and you wish to report it, then write the message here
   * As far as I know this should be in English language, I think.
   * @param serviceEnglishErrorMessage
   */
  public void setEnglishErrorMessage(String serviceEnglishErrorMessage)
    {
    this.serviceEnglishErrorMessage = serviceEnglishErrorMessage;
    }

  public void setEnglishExcption(String location, Exception exc)
    {
    serviceEnglishErrorMessage = location+" "+exc;
    }

  public String getEnglishErrorMessage ()
    {
    return  serviceEnglishErrorMessage;     
    }
  
  public void markEnd ( )
    {
    callEndDate = new Timestamp(System.currentTimeMillis());
    }

  public Date getCallEndDate ()
  	{
  	return callEndDate;
  	}
  
  @Override
  public String toString()
    {
    StringBuilder risul = new StringBuilder();
    
    risul.append("start[" + service_start_date+"] ");
    risul.append("end[" + callEndDate+"] ");
    
    if ( serviceEnglishErrorMessage != null ) 
      risul.append(" erroMessage='" + serviceEnglishErrorMessage + "'");
    
    return risul.toString();
    }
  }
