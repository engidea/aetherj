/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;

import com.google.gwt.regexp.shared.RegExp;

/**
 * A Chat, unfortunately I need to calculate pow on server side since there are quite a few things needed
 */
public class AegwChat implements Serializable
  {
  private static final long serialVersionUID = 1L;
  
  public int       chat_id;
  
  public String    chat_from_uname;  // username this chat is from
  public int       chat_from_id;     // This chat content is coming from this user id
  
  public String    to_hostname;      // including the port eg: https://2.2.2.2:9898
  public int       chat_to_ukeyid;   // id in ukey of what user to address this message to
  
  public String    chat_message;
  
  public AegwChat()
    {
    // for serialization
    }
  
  public AegwChat(int chat_id, String chat_message)
    {
    this.chat_id=chat_id;
    this.chat_message=chat_message;
    }
  
  public boolean like ( RegExp expr )
    {
    if ( expr.test(chat_message)) return true;
    
    return false;
    }

  public String getMessageBegin ( int len )
    {
    if ( chat_message == null )
      return "";
    
    if ( len >= chat_message.length() )
      return chat_message;
    
    return chat_message.substring(0, len);
    }
  
  public final String toString()
    {
    return "id="+chat_id+" msg="+getMessageBegin(30);
    }
  }
