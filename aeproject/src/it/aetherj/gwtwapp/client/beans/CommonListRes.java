/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.util.*;

public class CommonListRes<T> extends CommonRes 
  {
	private static final long serialVersionUID = 1L;
	
	protected ArrayList<T> itemsList;
  
  public CommonListRes()
    {
    itemsList = new ArrayList<T>();
    }
  
	/**
	 * Whoever needs it can have the arrayList that backs this implementation
	 * @return
	 */
	public ArrayList<T> getList ()
		{
		return itemsList;			
		}

  /**
   * Whoever needs it can SET the arrayList that backs this implementation
   * @param alist a non null array list value 
  public void setList (ArrayList<T> alist)
    {
    if ( alist == null ) return;
    
    itemsList = alist;     
    }
   */

	public int getListSize ()
    {
    return itemsList.size();    
    }
  
  public void add ( T item )
    {
    itemsList.add(item);
    }
  
  public final T get ( int rowIndex )
    {
    return itemsList.get(rowIndex);  
    }
  
  public final int indexOf ( T obj )
    {
    return itemsList.indexOf(obj);      
    }
  
  public void set ( int index, T value )
    {
    itemsList.set(index,value);    
    }
  
  public void clear ()
    {
    itemsList.clear();
    }
  
  @Override
  public String toString ()
    {
    return "Feedback["+super.toString()+"] list size="+getListSize();  
    }

  /**
   * This behaves nicely when null values are given
   * I Normally wish the nulls to be at the beginning, so if I compare a null with something null wins
   * note that if there are two nulls then the result is equality
   * @param o1
   * @param o2
   * @return
   */
  protected int compareDouble ( Double o1, Double o2 )
    {
    // two null make one equal
    if ( o1 == null && o2 == null ) return 0;
    
    if ( o1 == null ) return -1;
    
    if ( o2 == null ) return 1;
    
    return o1.compareTo(o2);
    }
  
  /**
   * This behaves nicely when null values are given
   * I Normally wish the nulls to be at the beginning, so if I compare a null with something null wins
   * note that if there are two nulls then the result is equality
   * @param o1
   * @param o2
   * @return
   */
  protected int compareString ( String o1, String o2 )
    {
    // two null make one equal
    if ( o1 == null && o2 == null ) return 0;
    
    if ( o1 == null ) return -1;
    
    if ( o2 == null ) return 1;
    
    return o1.compareTo(o2);
    }

  /**
   * This behaves nicely when null values are given
   * I Normally wish the nulls to be at the beginning, so if I compare a null with something null wins
   * note that if there are two nulls then the result is equality
   * @param o1
   * @param o2
   * @return
   */
  protected int compareDate ( Date o1, Date o2 )
    {
    // two null make one equal
    if ( o1 == null && o2 == null ) return 0;
    
    if ( o1 == null ) return -1;
    
    if ( o2 == null ) return 1;
    
    return o1.compareTo(o2);
    }
  
  /**
   * Just for similarity with previous methods
   * @param o1
   * @param o2
   * @return
   */
  protected int compareInt ( int o1, int o2 )
    {
    return o1 - o2;
    }

  
  
  }
