/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;

import com.google.gwt.regexp.shared.RegExp;

/**
 * An Aether user
 */
public class AegwUkey implements Serializable
  {
  private static final long serialVersionUID = 1L;
  
  public int       ukey_id;           
  public String    ukey_name;
  public String    ukey_info;         // can be null to indicate that the value is label_english
  public String    ukey_fprint_hex;   // fingerprint in hex format
  public String    ukey_pubkey_hex;   // public key in hex format
  
  public AegwUkey()
    {
    // for serialization
    }
  
  public void clear ()
    {
    ukey_id = 0;
    ukey_name = null;
    ukey_info = null;
    ukey_fprint_hex=null;
    ukey_pubkey_hex=null;
    }
  
  public boolean like ( RegExp expr )
    {
    if ( expr.test(ukey_name)) return true;
    
    if ( expr.test(ukey_info)) return true;
    
    return false;
    }
  
  public final String toString()
    {
    return "id="+ukey_id+" eng="+ukey_name;
    }
  }
