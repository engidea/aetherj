/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;

import com.google.gwt.regexp.shared.RegExp;

/**
 * A Thread, note that the list of subscribed thread is something else
 */
public class AegwThread implements Serializable
  {
  private static final long serialVersionUID = 1L;
  
  public int       thread_id;
  public int       thread_board_id;    
  
  public String    thread_name="";
  public String    thread_board_name="";
  public String    thread_link="";
  public String    thread_body="";
  public String    thread_desc="";         // can be null to indicate that the value is label_english
  
  public int       thread_post_cnt;     // how many posts this thread has 
  public int       thread_votes_cnt;    // how many votes ... +-
  
  public AegwThread()
    {
    // for serialization
    }

  /**
   * Create a new thread that has board_id as parent
   * @param board_id
   */
  public AegwThread(int board_id )
    {
    this.thread_board_id=board_id;
    }
  
  public boolean equalBoard(AegwBoard other)
    {
    if ( other == null ) return false;
    
    return thread_board_id == other.board_id;
    }

  public void clear ( int board_id )
    {
    thread_board_id=board_id;
    thread_id=CommonReq.NULL_int;
    thread_name="";
    thread_link="";
    thread_body="";
    }
  
  public boolean like ( RegExp expr )
    {
    if ( expr.test(thread_name)) return true;
    
    if ( expr.test(thread_link)) return true;
    
    if ( expr.test(thread_desc)) return true;
    
    return false;
    }

  public String getThreadNameBegin ( int len )
    {
    if ( thread_name == null )
      return "";
    
    if ( len >= thread_name.length() )
      return thread_name;
    
    return thread_name.substring(0, len);
    }

  public String getThreadBodyBegin ( int len )
    {
    if ( thread_body == null )
      return "";
    
    if ( len >= thread_body.length() )
      return thread_body;
    
    return thread_body.substring(0, len);
    }
  
  public final String toString()
    {
    return "id="+thread_id+" eng="+thread_name;
    }
  }
