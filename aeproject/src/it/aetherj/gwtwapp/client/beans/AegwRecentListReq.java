/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.util.Date;

import it.aetherj.gwtwapp.client.RecliStat;

/**
 * The idea is that I wish to find out "recent" posts starting from a date, that is possibly now
 */
public class AegwRecentListReq extends CommonReq 
  {
	private static final long serialVersionUID = 1L;
	
  public Date want_date = new Date();  // give me all posts that are older than this data
  public int want_limit = 100;    // give me this amount of them
  
  public AegwRecentListReq()
    {
    }
  
  public AegwRecentListReq(RecliStat stat)
    {
    super(stat);      
    }
  }
