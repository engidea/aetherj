package it.aetherj.gwtwapp.client.beans;

public class AegwCommonOprRes extends CommonRes
  {
  private static final long serialVersionUID = 1L;

  public Integer wpow_id;       // the wpow_id to be sent back on the following request
  public String  jsonForPow;    // this is the json that has to be powed by client
  public int     w_pow_length;  // also called difficulty, longer length are more difficult
  public byte  []hashForPow;    // in bim you have the hash calculated, up to the point when you do pow
  
  public AegwCommonOprRes()
    {
    }
  }
