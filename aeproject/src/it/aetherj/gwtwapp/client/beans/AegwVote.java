/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;

/**
 * A Vote
 * This is actually not really transmitted fomr server to client, but just from web client to server
 */
public class AegwVote implements Serializable
  {
  private static final long serialVersionUID = 1L;
  
  public int       vote_id;         // used on server, not provided by client
  
  public int       vote_board_id;   // for a new vote I need to know the ...
  public int       vote_thread_id;  // for a new vote I need to know the ...
  public int       vote_post_id;    // for a new vote I need to know the ... if 0 then the vote is for a thread

  public boolean   vote_up;         // if true vote up, false down

  // I get the owner from my identity
  
  public final String toString()
    {
    return "thread_id="+vote_thread_id+" vote_up="+vote_up;
    }
  }
