/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details. 
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootLayoutPanel;

import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.gui.*;
import it.aetherj.gwtwapp.client.guipanels.*;
import it.aetherj.gwtwapp.client.images.ImageBundleInterface;
import it.aetherj.gwtwapp.client.services.RewebServiceFactory;
import it.aetherj.gwtwapp.client.ui.GwtWidgetProvider;
import it.aetherj.gwtwapp.client.uict.DataGridResourceCss;
import it.aetherj.gwtwapp.client.varie.*;
 

/**
 * @author damiano
 */
public final class RecliStat
  {
  private final RootLayoutPanel  rootPanel;   
	
  public DataGridResourceCss     dataGridResource; // created at boot
	public ImageBundleInterface    imageBoundle;     // created at boot
  public RewebServiceFactory     serviceFactory;   // created at boot
  public RecliUtils              utils;            // generic conversion and checking functions
	
  public AegwDictionary          dictionary;       // created before logging in
  public RecliFormatter          formatter;        // date time number formatters
  public DictionaryAssistant     dictionaryAssist; // assistant to manage labels
  public AecliLoginPage          loginPage;        // the actual login page 
  public AegwLoginRes            login_res;        // created once user is logged in
  
  // these are created as new on allocation of DefaultLoggedinPage
  public AecliInfoShow          sysinfoPanel;
  public AecliLoggedinPage      loggedinPage;
  public AecliStatusBar         statusBar;
  public NavigationButtons      navigationButtons; 

  public AecliDummyPage         dummyPanel;
  public AecliPopularListPanel  popularPanel;
  public AecliRecentListPanel   recentPanel;
  public AecliThreadListPanel   threadList;
  public AecliBoardListPanel    boardList;
  public AecliPostTreePanel     postTree;
  public AecliPageHelp          pageHelp;
  public AecliUserEdit          userEditPanel;
  public AecliChatListPanel     chatListPanel;
  public AecliUkeyListPanel     ukeyListPanel;
  
  public RecliStat ()
    {
    this.rootPanel  = RootLayoutPanel.get();      
    }
  
  public synchronized void  setRootCanvas ( GwtWidgetProvider provider )
    {
    rootPanel.clear();
    rootPanel.add(provider.getPanelToDisplay());
    }
  
  public WebSession getWebSession()
    {
    if (login_res == null ) return null;
    
    return login_res.webSession;
    }
  
	public final boolean isWebSuperuser ()
		{
    return true;
		}
	
	public final boolean isReadOnly ()
		{
		// it is really safer to deny save if there is no user
		if ( login_res == null ) return true;
		
		WebSession wsession = login_res.webSession;
		
		if ( wsession == null ) return true;
		
		return wsession.wana_read_only;
		}
	
  /**
   * In this case the default value (the one returned if nothing is found) is the label itself
   * @param label_english
   * @param from_page
   * @return
   */
  public String getLabel ( String label_english, String from_page )
    {
    return getLabel(label_english,from_page,label_english);
    }
  
  /**
   * In this case, the default value is the one given !
   * @param label_english
   * @param from_page
   * @param default_value
   * @return
   */
  public String getLabel(  String label_english, String from_page, String default_value )
    {
    if ( label_english == null ) return "";
    
    if ( label_english.length() < 1 ) return "";
    
    // It is OK to throw a NPE if dictionary is not defined !!!
    String risul = dictionary.getLabel(label_english);

    // this is used to report of page existence
    dictionaryAssist.addLabel(label_english,from_page);

    if ( risul != null ) return risul;
    
    return default_value;
    }
  
  
  public native void logOnBrowser(String message)
  /*-{
      console.log(message);
  }-*/;
  
  public final void alert ( String message )
    {
    Window.alert(message); 
    }
  }
