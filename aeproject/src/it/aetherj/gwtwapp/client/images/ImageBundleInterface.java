/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.images; 

import com.google.gwt.resources.client.*;


/**
 * You can simply use return new Image (stat.imageBoundle.btn_chart_line()); to create a new image
 */
public interface ImageBundleInterface extends ClientBundle
   {
   @Source ("new-32x32.png")
   public abstract ImageResource newNew();

   @Source ("heart-32x32.png")
   public abstract ImageResource newHeart();
   
   @Source ("heart-32x32-bright.png")
   public abstract ImageResource newHeartBright();
   
   @Source ("edit-32x32.png")
   public abstract ImageResource newEdit();
   
   @Source ("pencil_go-16x16.png")
   public abstract ImageResource newPencilGo();

   @Source ("online.gif")
   public abstract ImageResource newOnline();
   
   @Source ("offline.gif")
   public abstract ImageResource newOffline();

   @Source ("info-32x32.png")
   public abstract ImageResource newInfo();

   @Source ("help-32x32.png") 
   public abstract ImageResource newHelp();

   @Source ("back.gif")
   public abstract ImageResource newBack();

   @Source ("forward-16x16.gif") 
   public abstract ImageResource newForward();

   @Source ("thumb_up-16x16.png")
   public abstract ImageResource newUp();

   @Source ("thumb_down-16x16.png")
   public abstract ImageResource newDown();
   
   @Source ("logoff-32x32.gif")
   public abstract ImageResource newLogoff();
   
   @Source ("refresh-32x32.png")
   public abstract ImageResource newRefresh();

   @Source ("save-32x32.png")
   public abstract ImageResource newSave();

   @Source ("key_r-32x32.png")
   public abstract ImageResource newReply();

   }
