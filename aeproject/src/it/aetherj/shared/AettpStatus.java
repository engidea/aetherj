package it.aetherj.shared;

public class AettpStatus
  {
  public static final int res_NULL=0;
  public static final int res_OK=200;
  public static final int res_NOContent=204;  // see respwriter.go
  public static final int res_BAD=400;
  public static final int res_NOTFound=404;
  public static final int res_NOTAllowed=405;
  public static final int res_TOOBusy=429;
  public static final int res_Exception=430;
  public static final int res_Timeout=431;
  public static final int res_ConnRefused=432;
  public static final int res_NoRoute=433;
  public static final int res_NoToDo=434;                // nothing to do
  public static final int res_Mismatch=435;              // Jakson telling me nothing to read
  public static final int res_Shutdown=436;              // System is being shutdown
  
  public static final AettpStatus OK = new AettpStatus(res_OK);
  public static final AettpStatus NOContent = new AettpStatus(res_NOContent);
  public static final AettpStatus Exception = new AettpStatus(res_Exception);
  public static final AettpStatus Shutdown = new AettpStatus(res_Shutdown);
  public static final AettpStatus NoToDo = new AettpStatus(res_NoToDo);

  
  public final int rescode;
  
  public AettpStatus ( int rescode )
    {
    this.rescode=rescode;
    }
  
  public boolean isOK ()
    {
    return rescode == res_OK;
    }
  
  public boolean isFail ()
    {
    return rescode != res_OK;
    }
  
  public String getMessage ()
    {
    return getResMessage(rescode);
    }
  
  public static String getResMessage(int rescode)
    {
    String msg;
    
    switch ( rescode)
      {
      case res_OK:          msg="OK";           break;
      case res_BAD:         msg="BAD Request";  break;
      case res_NOContent:   msg="NO Content";   break;
      case res_NOTFound:    msg="NOT Found";    break;
      case res_NOTAllowed:  msg="NOT Allowed";  break;
      case res_TOOBusy:     msg="TOO Busy";     break;
      case res_Exception:   msg="Exception";    break;
      case res_Timeout:     msg="Timeout";      break;
      case res_ConnRefused: msg="Conn Refused"; break;
      default: msg="Unknown code "+rescode;
      }
    
    return "rescode "+rescode+" "+msg;
    }
  
  }
