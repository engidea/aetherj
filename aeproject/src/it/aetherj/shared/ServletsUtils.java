/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Timestamp;
import java.util.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import it.aetherj.boot.dbase.Brs;

public class ServletsUtils implements Serializable
  {
	private static final long serialVersionUID = 1L;
	private static final String classname="ServletsUtils.";
	
  public static final int samplesCount   = 24;                    // sono 24 ore
  public static final int PlotAreaWidth  = samplesCount * 25;     // è molto ma si può fare
  public static final int PlotAreaHeight = 160;                  // altezza standard
  
  
  public static final int plotBaseX   = 60;
  public static final int plotMarginX = 50;   // leave this space at the end of X azis
  
  public static final int legendBaseY = 22;   // on top of this there is the title, lower numbers get things higher
  public static final int plotBaseY   = 65;   // measured from the top ot the image
  public static final int plotMarginY = 50;   // leave this space at the end of Y azis
  
  public static final int XYChartWidth  = PlotAreaWidth + plotBaseX + plotMarginX;
  public static final int XYChartHeight = PlotAreaHeight + plotBaseY + plotMarginY;
  
  public static final int PlotAreaX = 40;
  public static final int PlotAreaY = 20;

  private final Font font = new Font("Serif", Font.BOLD, 12);

  
  public ServletsUtils()
    {
    }

  public static double[] convArrayListDouble ( ArrayList<Double>input)
    {
    double[] risul = new double[input.size()];
    
    for (int index = 0; index < risul.length; index++)
      risul[index] = input.get(index).doubleValue();
      
    return risul;
    }
  
  public Timestamp[] convArrayListTimestamp ( ArrayList<Timestamp>input)
    {
    Timestamp[] risul = new Timestamp[input.size()];
    for (int index = 0; index < risul.length; index++)
      risul[index] = input.get(index);
      
    return risul;
    }
  
/*
  public Color[] convArrayListColor ( ArrayList<Color>input)
    {
    Color[] risul = new Color[input.size()];
    for (int index = 0; index < risul.length; index++)
      risul[index] = input.get(index);
      
    return risul;
    } 
*/

  public Timestamp[] convertTimestampData(Brs rows, String colname)
    {
    rows.beforeFirst();
    
    ArrayList<Timestamp> risul = new ArrayList<Timestamp>();
    
    while ( rows.next())
      {
      Timestamp value = rows.getTimestamp(colname);
      risul.add(value);
      }    
    
    return convArrayListTimestamp(risul);
    }
  

    
	
	public double findLowestValue ( double [] input )
		{
		double lowest = Double.MAX_VALUE;
		
		for (int index=0; index<input.length; index++)
			{
			double aval = input[index];

			if ( aval < lowest ) lowest = aval;
			}
		
		return lowest;
		}
  
	
	public String getMonthNameEnglish ( int month_id )
		{
		switch (month_id)
			{
			case Calendar.JANUARY+1: return "January";
			case Calendar.FEBRUARY+1: return "February";
			case Calendar.MARCH+1: return "March";
			case Calendar.APRIL+1: return "April";
			case Calendar.MAY+1: return "May";
			case Calendar.JUNE+1: return "June";
			case Calendar.JULY+1: return "July";
			case Calendar.AUGUST+1: return "August";
			case Calendar.SEPTEMBER+1: return "September";
			case Calendar.OCTOBER+1: return "October";
			case Calendar.NOVEMBER+1: return "November";
			case Calendar.DECEMBER+1: return "December";
			default: return "??"+month_id+"??";
			}
		}

	public double findHighestValue ( double [] input )
		{
		double highest = Double.MIN_VALUE;
		
		for (int index=0; index<input.length; index++)
			{
			double aval = input[index];

			if ( aval > highest ) highest = aval;
			}
		
		return highest;
		}
  
  public void writeHtmlMessage(HttpServletResponse resp, String message ) throws IOException 
    {
    resp.setContentType("text/html");
    resp.setCharacterEncoding("UTF-8");

    PrintWriter out = resp.getWriter();
    
    out.println("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN' >");
    out.println("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
    out.println("<head>");
    out.println("<title>System Message</title>");
    out.println("</head>");
    out.println("<body>");
    out.println(message);
    out.println("</body>");
    out.println("</html>");
		
    out.close();    
    }
  
  public void writeImageMessage(HttpServletResponse resp, int width, int height, String message ) throws IOException 
    {
    BufferedImage bimg = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);

    Graphics2D canvas = (Graphics2D)bimg.getGraphics();
    canvas.setBackground(Color.white);
    canvas.clearRect(0,0,width,height);
    
    if ( message != null )
      {
      canvas.setFont(font);
      canvas.setColor(Color.red);
      canvas.drawString(message, 0, height/2);
      }
    
    resp.setContentType("image/gif");

    OutputStream out = resp.getOutputStream();
    ImageIO.write(bimg,"gif",out);
    out.close();    
    }
  

  public String convNibbleToHexString(int value)
    {
    switch (value)
      {
      case 0x0: return "0";
      case 0x1: return "1";
      case 0x2: return "2";
      case 0x3: return "3";
      case 0x4: return "4";
      case 0x5: return "5";
      case 0x6: return "6";
      case 0x7: return "7";
      case 0x8: return "8";
      case 0x9: return "9";
      case 0xA: return "A";
      case 0xB: return "B";
      case 0xC: return "C";
      case 0xD: return "D";
      case 0xE: return "E";
      case 0xF: return "F";
      default: return "?";
      }
    }
    
    
  public String convCharToHexString(char value)
    {
    if (value > 255) return "??";
    
    byte byteval = (byte)value;
    
    int msbval = (byteval >> 4) & 0x0F;
    int lsbval = byteval & 0x0F;
    
    return convNibbleToHexString(msbval) + convNibbleToHexString(lsbval);
    }

  public String convAsciiStringToQuotedString(String input)
    {
    if (input == null) return "";
    
    int in_len = input.length();

    if (in_len < 1) return "";
    
    // I allocate a bigger risul so I can reasonably fit possible mapping
    StringBuilder builder = new StringBuilder(in_len + 20);
    
    char[] inchar = input.toCharArray();
    
    for (int index = 0; index < in_len; index++)
      {
      char ch = inchar[index];
      
      if (ch < ' ')
        {
        // basically I want to transform all char less than space into backslash and an HEX string
        builder.append("\\");
        builder.append(convCharToHexString(ch));
        continue;
        }
      
      // but there are normal chas the must be quoted, otherwise they mess up things.
      // note that th decoder is simpler than the encoder since all that follows a \ 
      // must be converted with the same algorithm.
      switch (ch)
        {
        case '"':
        builder.append("\\22");   // see the position of " in ascii map
        break;
        
        case '\\':
        // I need to quote the backslash too !
        builder.append("\\5C");
        break;
        
        default:
        builder.append(ch);
        break;
        }
      }
    
    return builder.toString();
    }


  public String stripLeadingTrailingQuote ( String input )
    {
    if (input == null) return "";
    
    if ( input.length() < 1 ) return "";
    
    int firstpos = input.indexOf('"');
    if (firstpos < 0) firstpos = 0;
    
    int lastpos = input.lastIndexOf("\"");
    if (lastpos < 0) lastpos = input.length();
    
    int copylen = lastpos - firstpos;
    
    return input.substring(firstpos + 1, copylen);
    }

  public Integer convStringToInteger ( String input )
    {
    if ( input == null ) return null;
    
    try
      {
      return Integer.parseInt(input);
      }
    catch ( Exception exc )
      {
      return null;        
      }
    }

  public static int convStringToInt ( String input, int defval )
    {
    if ( input == null ) return defval;
    
    try
      {
      return Integer.parseInt(input);
      }
    catch ( Exception exc )
      {
      return defval;        
      }
    }


  /**
   * It will return exception if the data is bigger than 1mbyte
   * @param stream
   * @return
   */
  public byte[] readByteStream ( InputStream stream, int buffer_max ) throws IOException
    {
    int letti=0;
    
    byte[] buffer = new byte[buffer_max];
    while(letti < buffer_max)
      {
      int result = stream.read(buffer, letti, buffer_max - letti);
      if (result == -1) break;
      letti += result;
      }
    
    byte[] risul = new byte[letti];
    System.arraycopy(buffer,0,risul,0,letti);
    return risul;
    }



	public java.util.Date parseAAMMGG (String raw_val )
		{
		if ( raw_val == null || raw_val.length() < 6 ) return new java.util.Date();
		
		String raw_year = raw_val.substring(0, 2);
		int year = Aeutils.parseInt(raw_year, 10) + 2000;
		
		String raw_month = raw_val.substring(2, 4);
		int month = Aeutils.parseInt(raw_month, 1);
		month -= 1;  // since java month starts at 0
		
		String raw_day = raw_val.substring(4, 6);
		int mday = Aeutils.parseInt(raw_day, 1);
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(year, month, mday);
		
		return calendar.getTime();
		}

  } // END main class
