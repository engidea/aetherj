/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared;

import javax.servlet.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.server.WebServer;
import it.aetherj.boot.PrintlnProvider;

/**
 * NOTE that this HAS to be in web.xml in the WEB-INF directory
 * Eg: 
 
  <listener>
    <description>Prepare an environment for Web</description>
    <listener-class>it.aetherj.shared.WebContextListener</listener-class>
  </listener>

 * This is used to "bind" instances from the Aether server to the web server part
 * What happens is that a shared Stat that is peculiar to the web services is created
 */
public final class WebContextListener implements ServletContextListener
  {
  private static final String classname="WebContextListener";
  
  public static final String KEY_WebStat = "WebStat";
  
  public void contextInitialized(ServletContextEvent event)
    {
    System.out.println(classname + "contextInitialized: CALLED");

    // now it is time to share this stat to the various servlets
    ServletContext context = event.getServletContext();
    
    Stat systemStat = (Stat)context.getAttribute(WebServer.share_SystemStat);
    PrintlnProvider websrvLog = (PrintlnProvider)context.getAttribute(WebServer.share_CntxLog);
															
    if ( systemStat == null )
      throw new IllegalArgumentException("MISSING systemStat");
    
    context.setAttribute(KEY_WebStat, newRewebStat(systemStat, websrvLog));
    }
 

  private WebStat newRewebStat (Stat systemStat, PrintlnProvider weblog)
    {
    WebStat wstat = new WebStat(weblog);

    weblog.println(classname+".newRewebStat: CALL ");
 
    wstat.dbg               = systemStat.dbg;
    wstat.appVersion        = systemStat.appVersion;
    wstat.mainDbaseProperty = systemStat.config.mainDbaseProperty;
		wstat.nonceMap          = systemStat.nonceMap;
		wstat.labelFactory      = systemStat.labelFactory;
		wstat.aedbFactory       = systemStat.aedbFactory;
		wstat.aetherParams      = systemStat.aeParams;
    wstat.mcwAddressModel   = systemStat.mcwAddressModel;
    wstat.mcwConsole        = systemStat.mcwConsole;

    wstat.connpool          = new WebConnpool(wstat);

    return wstat;          
    }

  public void contextDestroyed(ServletContextEvent event)
    {
    System.out.println(classname + ".contextDestroyed: CALLED");

    ServletContext context = event.getServletContext();

    WebStat stat = (WebStat)context.getAttribute(KEY_WebStat);
    
    stat.connpool.close();
    }
  }
