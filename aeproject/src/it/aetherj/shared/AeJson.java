package it.aetherj.shared;

import java.io.*;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.io.CharacterEscapes;
import com.fasterxml.jackson.databind.*;

/**
 * Wrap to keep all Json stuff in one place
 * SOurce code is here, I am using the 2.12.0 version, so far
 * https://github.com/FasterXML
 * Useful commands
 * git status
 * git branch -a
 * git checkout [branch]
 * 
 * AHHHHHHH so, one of the issues is that golang has LOWERCASE hex, jackson (ancd quite a few rest of the world) had UPPERCASE hex
 * 
 * Char mapping is defined in CharTypes in package com.fasterxml.jackson.core.io and is easy to "adjust", BUT
 * jackson tests fail on building and so, you need to compile using this
 * 
 * $ mvn package -Dmaven.test.skip=true^
 * 
 * 
 * to see differences in a long line use
 *  
 * $ git diff --word-diff f1 f2
 * 
 * the differences will be marked by [-world-]{+universe+}
 * 
 * Yes, it is confusing.... whatever
 * 
 * To enable special check for parsed data use beanshell and
 * stat.dbg.wishMaskLevel(0x2010,0x1);
 * 
 * Use https://json-diff.com/ the above is crap, 
 * 
 */
public class AeJson
  {
  private static final JsonFactory jsonFactory = newJsonFactory();

  private final ObjectMapper om;
   
  public AeJson ()
    {
    om = new ObjectMapper(jsonFactory);
    }
  
  private static JsonFactory newJsonFactory ()
    {
    JsonFactory jsonFactory = new JsonFactory();

    jsonFactory.setCharacterEscapes(new CustomCharacterEscapes()); 

    return jsonFactory;
    }
  
  /**
   * Eg: configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
   * @param feature
   * @param state
   */
  public void configure (DeserializationFeature feature, boolean state)
    {
    om.configure(feature, state);
    }
  
  
  public <T> T readValue (BufferedReader source, Class<T> valueType) throws IOException 
    {
    return om.readValue(source, valueType);
    }
  
  public <T> T readValue (byte [] source, Class<T> valueType) throws IOException 
    {
    return om.readValue(source, valueType);
    }
  
  public <T> T readValue (String source, Class<T> valueType) throws JsonMappingException, JsonProcessingException 
    {
    return om.readValue(source, valueType);
    }
  
  public String writeValueAsString ( Object arg ) throws JsonProcessingException
    {
    if ( arg == null )
      return null;
    
    return om.writeValueAsString(arg);
    }
  
  /**
   * This is NOT compatible with ANY of Aether, use to save config only
   */
  public String writeValueAsStringPretty ( Object arg ) throws JsonProcessingException
    {
    if ( arg == null )
      return null;
    
    return om.writerWithDefaultPrettyPrinter().writeValueAsString(arg);
    }
  
  /**
   * This closes the stream at the end of writing
   * @param out
   * @param value
   * @throws IOException
   */
  public void writeValue (OutputStream out, Object value) throws IOException
    {
    om.writeValue(out, value);
    }

/**
 * The mapping between a value and a ascii for hex is in CharTypes in package com.fasterxml.jackson.core.io
 * AHHHHH, I need to escape some HTML chars since golang does it.... crappy golang
 */
private static class CustomCharacterEscapes extends CharacterEscapes
  {
  private static final long serialVersionUID = 1L;
  private final int[] _asciiEscapes;

  public CustomCharacterEscapes()
    {
    _asciiEscapes = standardAsciiEscapesForJSON();
    _asciiEscapes['<'] = CharacterEscapes.ESCAPE_STANDARD; 
    _asciiEscapes['>'] = CharacterEscapes.ESCAPE_STANDARD;
    _asciiEscapes['&'] = CharacterEscapes.ESCAPE_STANDARD;
    }

  @Override
  public int[] getEscapeCodesForAscii()
    {
    return _asciiEscapes;
    }

  @Override
  public SerializableString getEscapeSequence(int i)
    {
    return null;
    }
  }  
  
  
  }