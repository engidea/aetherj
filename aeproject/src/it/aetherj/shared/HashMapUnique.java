/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared;

import java.util.*;

/**
 * In many cases I need a HashMap that signal an error if I try to overwrite a key that
 * is already present. Also I need key handling to be a bit simpler, specifically I need the objects
 * to provide the key programmatically.
 * It does not extends HashMap since I want to be sure that data is accessed as I need.
 */
public class HashMapUnique implements Iterable<HashMapKeyProvider>
  {
  private static final String classname="HashMapUnique.";

  private final HashMap<String,HashMapKeyProvider> hashMap;

  /**
   * Constructor: You must give me an idea of how many elements will be in the map
   */
  public HashMapUnique ( int size )
    {
    hashMap = new HashMap<String,HashMapKeyProvider>(size);
    }
    
  /**
   * Store anObject into the HashMap, is the object key is already present then an exception is raised.
   * @param anObject
   * @return the object you have given, so you can concatenate things.
   */
  public Object put ( HashMapKeyProvider anObject )
    {
    // I need to make sure that I have a stack trace of who tried to insert a null object.
    if ( anObject == null ) throw new IllegalArgumentException (classname+"put() anObject==null");
    
    String key = anObject.getHashMapKey();
    
    if ( hashMap.containsKey(key) )
      throw new IllegalArgumentException (classname+"put: Already present key="+key);

    hashMap.put(key,anObject);

    return anObject;
    }
    
  /**
   * Retrieve the object with the given key from the map.
   * @return the object with the given key or null if not found.
   * @param key a non null string otherwise an exception is raised.
   */
  public HashMapKeyProvider get ( String key )
    {
    if ( key == null ) 
      throw new IllegalArgumentException (classname+"get(): ERROR key == null");
    
    return hashMap.get(key);
    }

  /**
   * Returns a collection view of the values contained in this map.  The
   * collection is backed by the map, so changes to the map are reflected in
   * the collection, and vice-versa.  The collection supports element
   * removal, which removes the corresponding mapping from this map, via the
   * <tt>Iterator.remove</tt>, <tt>Collection.remove</tt>,
   * <tt>removeAll</tt>, <tt>retainAll</tt>, and <tt>clear</tt> operations.
   * It does not support the <tt>add</tt> or <tt>addAll</tt> operations.
   *
   * @return a collection view of the values contained in this map.   
   */
  public Collection<HashMapKeyProvider> values ()
    {
    return hashMap.values();
    }
  
  @Override
  public Iterator<HashMapKeyProvider> iterator ()
    {
    return values().iterator();
    }
  }
  
