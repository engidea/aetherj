/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared;

import java.awt.Desktop;
import java.awt.event.ActionListener;
import java.io.File;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.protocol.AetherCo;


public final class Aeutils
  {
  private static final String classname="Utils.";

  private final Stat stat;
  private final DateFormat shortTimeFormat = DateFormat.getTimeInstance(DateFormat.SHORT);
  private final DateFormat updateTimeFormat;
  private final DateFormat shortDateFormat;
	private DecimalFormat moneyFormat;        // Money have to be formatted in a different way...

  public static final long MILLIS_IN_DAY = (1000 * 60 * 60 * 24 );
	
  
  public Aeutils(Stat stat)
    {
    this.stat = stat;
    
    updateTimeFormat = new SimpleDateFormat("dd/MM HH.mm");
		
    shortDateFormat  = new SimpleDateFormat("yyyy-MM-dd");
		
    moneyFormat  = (DecimalFormat)NumberFormat.getNumberInstance();      
    moneyFormat.setDecimalSeparatorAlwaysShown(true);
    moneyFormat.setMinimumFractionDigits(2);
    }

  /**
   * Returns a string with the given amount formatted as money.
   * NOTE: I need to handle the currency symbol.
   */
  public final String getDecimalMoney ( BigDecimal amount )
    {
    if ( amount == null ) return "";
    
    return "€ "+moneyFormat.format(amount);
    }


  public static final boolean isIntegerEqual ( Integer one, Integer two )
    {
    if ( one == null && two ==  null ) return true;
    
    if ( one != null && two == null ) return false;
    
    if ( one == null && two != null ) return false;
    
    return one.equals(two);
    }

  
  public static final boolean isBooleanEqual ( Boolean one, Boolean two )
    {
    if ( one == null && two ==  null ) return true;
    
    if ( one != null && two == null ) return false;
    
    if ( one == null && two != null ) return false;
    
    return one.equals(two);
    }

  
  public static boolean isNonEmptyString ( String astring )
    {
    if ( astring == null ) return false;
    
    return astring.length() > 1;
    }
  
  public static Thread newThreadStart ( Runnable runnable, String name )
    {
    Thread risul = new Thread(runnable,name);
    risul.setPriority(Thread.MIN_PRIORITY);
    risul.setDaemon(true);
    risul.start();
    return risul;
    }
    
  /**
   * Sleep for the given amount of millisec and return true if the sleep is fine
   * return false if an interrupt occourred.
   * @param millis
   * @return
   */
  public static boolean sleepMsec ( int millis )
    {
    try
      {
      Thread.sleep(millis);
      return true;
      }
    catch ( Exception exc )
      {
      return false;
      }
    }

  /**
   * Sleep for the given amount of seconds and return true if the sleep is fine
   * return false if an interrupt occourred.
   * @param seconds
   * @return
   */
  public static boolean sleepSec ( int seconds )
    {
    return sleepMsec(seconds * 1000);
    }


  /**
   * Convert the number of minutes given as a date that has as starting point uear 1989
   * @throws SQLException
   */
  public static Timestamp convMinutedToTimestamp ( int minutes )
    {
    GregorianCalendar calendar = new GregorianCalendar(1989,0,1);

    // ok, do teh addition. This assumes that I am in winter time, that is when I am not in daylygth saving
    // that is I am in solar time, meaning that a midday the sun is perpendicular
    calendar.add(Calendar.MINUTE,minutes);

    TimeZone pdt = calendar.getTimeZone();
    
    if ( pdt.useDaylightTime() && pdt.inDaylightTime(calendar.getTime()) ) 
      {
      // Apparently MIDI, when on daylygth, adds an hour to the value, SO, to have the correct date I need
      // (if I am in daylight saving) to subtract one hour to the values being returned.
      // This would need some investigation, actually, since the daylight saving is something to be taken into account when an hour is computed !
      // WARNING: MIDI is WRONG the idea being that the minutes from a given date CANNOT go backward, as it may be the case if we are not careful.
      // this also messes up the time computation using the minutes returned from MIDI
      calendar.add(Calendar.HOUR,-1);
      }

    return new Timestamp(calendar.getTimeInMillis());
    }

  /**
   * Convert the number of minutes given as a date that has as starting point uear 1989
   * @throws SQLException
   */
  public static int convDateToMinutes ( Timestamp nowDate )
    {
    GregorianCalendar nowCal = new GregorianCalendar();
    nowCal.setTime(nowDate);
    long nowMilli = nowCal.getTimeInMillis();
    
    GregorianCalendar baseCal = new GregorianCalendar(1989,0,1);
    long baseMilli = baseCal.getTimeInMillis();
    
    if ( nowMilli < baseMilli )
      {
      System.err.println("convDateToMinutes: BAD nowDate="+nowDate);
      return 0;
      }
      
    long delta = nowMilli - baseMilli;
    
    // I want the minutes and delta is in milliseconds
    return (int)(delta / 60000);
    }

  /**
   * Attempt to parse an int and if it fails it return the default value
   * @param input
   * @param defaultValue
   * @return
   */
  public static int parseInt ( String input, int defaultValue )
    {
    try
      {
      return Integer.parseInt(input);
      }
    catch ( Exception exc )
      {
      return defaultValue;
      }
    }

  /**
   * Attempt to parse an Integer and if it fails it return the default value
   * @param input
   * @param defaultValue
   * @return
   */
  public static Integer parseInteger ( String input, Integer defaultValue )
    {
    try
      {
      return Integer.decode(input);
      }
    catch ( Exception exc )
      {
      return defaultValue;
      }
    }

  
  /**
   * Attempt to parse a BigDecimal and if it fails it return the dafult value
   * @param input
   * @param defaultValue
   * @return
   */
  public static double parseDouble ( String input, double defaultValue )
    {
    try
      {
      return Double.parseDouble(input);
      }
    catch ( Exception exc )
      {
      return defaultValue;
      }
    }
  

  /**
   * Attempt to parse a BigDecimal and if it fails it return the dafult value
   * @param input
   * @param defaultValue
   * @return
   */
  public static BigDecimal parseBigDecimal ( String input, BigDecimal defaultValue )
    {
    try
      {
      return new BigDecimal(input);
      }
    catch ( Exception exc )
      {
      return defaultValue;
      }
    }

    
  public String dateToSmallTime( Date input )
    {
    if ( input == null ) return "";
    
    return shortTimeFormat.format(input);
    }
    
  public String dateToUpdateFormat( Date input )
    {
    if ( input == null ) return "";
    
    return updateTimeFormat.format(input);
    }
    
  public String dateToShortDate( Date input )
    {
    if ( input == null ) return "";
    
    return shortDateFormat.format(input);
    }

  /**
   * Return a new TImestamp object initialized with the current time.
   * @return
   */
  public static final Timestamp newTimestamp()
    {
    return new Timestamp(System.currentTimeMillis());
    }
    
    
  /**
   * Get a Timestamp that has only the day,month,year part, not the hour minutes seconds
   * @return
   */
  public static final Timestamp convTimestampToDMY(long timeInMillis)
    {
    GregorianCalendar acal = new GregorianCalendar();
    acal.setTimeInMillis(timeInMillis);
    int day   = acal.get(Calendar.DAY_OF_MONTH);
    int month = acal.get(Calendar.MONTH);
    int year  = acal.get(Calendar.YEAR);
    acal = new GregorianCalendar(year,month,day);
    return new Timestamp(acal.getTimeInMillis());
    }

  /**
   * Get a new Timestamp that has only the day,month,year part, not the hour min sec
   * @param input a valid timestamp, if null then the current DMY as timestamp is returned.
   * @return
   */
  public final Timestamp convTimestampToDMY(Timestamp input)
    {
    long millisec;
    
    if ( input == null ) 
      millisec = System.currentTimeMillis();
    else
      millisec = input.getTime();
    
    return convTimestampToDMY(millisec);
    }


  /**
   * This function add amount what_part to input
   * @param what_part, can be GregorianCalendar.MONTH... 
   * @param amount
   * @return
   */
  public static final Timestamp addTimestamp ( Timestamp input, int what_part, int amount )
    {
    GregorianCalendar acal = new GregorianCalendar();
    acal.setTime(input);
    acal.add(what_part,amount);
    
    return new Timestamp(acal.getTimeInMillis());
    }

 
  
  
  /**
   * returns the difference between two dates including the begin and end day
   * @param start
   * @param end
   * @return may be negative if incorrect parameters
   */
  public int timestampDiffDaysInclusive ( Timestamp start, Timestamp end )
    {
    int risul = timestampDiffDays(start,end);
    
    if ( risul < 0 ) return risul;
    
    return risul + 1;
    }

  /**
   * returns the number of days between the two Timestamp
   * @param start
   * @param end must be later or equal than start
   * @return a negative number if invalid parameters
   */
  public int timestampDiffDays ( Timestamp start, Timestamp end )
    {
    if ( start == null || end == null ) return -1;
    
    GregorianCalendar start_cal = new GregorianCalendar();
    start_cal.setTime(start);
    
    GregorianCalendar end_cal = new GregorianCalendar();
    end_cal.setTime(end);
    
    return calendarDiffDays(start_cal, end_cal);
    }
  
  /**
   * returns the number of days between two calendar dates
   * see http://tripoverit.blogspot.it/2007/07/java-calculate-difference-between-two.html
   * @param start_cal 
   * @param end_cal later or equal than start_cal
   * @return -1 if illegal parameters, otherwise >= 0
   */
  public int calendarDiffDays(Calendar start_cal, Calendar end_cal)
    {
    if ( start_cal == null || end_cal == null ) return -1;
    
    long end_milli = end_cal.getTimeInMillis();
    
    long delta_milli = end_milli - start_cal.getTimeInMillis();
    
    if ( delta_milli < 0 ) return -2;
    
    int presumedDays = (int) (delta_milli / MILLIS_IN_DAY);
    
    Calendar astart = (Calendar) start_cal.clone();
    
    astart.add(Calendar.DAY_OF_YEAR, presumedDays);
    
    long astart_milli = astart.getTimeInMillis();

    if( astart_milli == end_milli ) return presumedDays;
    
    // WARNING: Not tested !!!
    
    if ( astart_milli > end_milli )
      {
      do
        {
        astart.add(Calendar.DAY_OF_MONTH, -1);
        presumedDays -= 1;
        }
      while(astart.getTimeInMillis() > end_milli);
      }
    else
      {
      do
        {
        astart.add(Calendar.DAY_OF_MONTH, 1);
        presumedDays += 1;
        }
      while(astart.getTimeInMillis() < end_milli);
      }

    return presumedDays;
    }  
  
  /**
   * Parse a string that should have a short date and returns the equivalent timestamp
   * if error on conversion null is returned.
   * @param input
   * @return
   */
  public Timestamp parseShortDateString ( String input )
    {
    try
      {
      Date adate = shortDateFormat.parse(input);
      return new Timestamp(adate.getTime());
      }
    catch ( Exception exc )
      {
      return null;
      }
    }
    





  /**
   * Convert a string to a quoted string, the quoting consists in transforming
   * the actual value into the equivalent ASCII representation coded as a HEX string 
   * the hex string is ALWAYS two digits.
   * @param input
   * @return
   */
  public static final String convAsciiStringToQuotedString ( String input )
    {
    if ( input == null ) return null;
    
    if ( input.length() < 1 ) return "";
    
    // I allocate a bigger risul so I can reasonably fit possible mapping
    StringBuilder builder = new StringBuilder(input.length() + 20);

    int strlen = input.length();
    for (int index=0; index<strlen; index++)
      {
      char ch = input.charAt(index);
      
      if ( ch < ' ' )
        {
        // basically I want to transform all char less than space into
        builder.append("\\");
        int charval = ch;
        if ( charval <= 0x0F ) builder.append("0");
        builder.append(Integer.toHexString(charval));
        continue;
        }
        
      // but there are normal chas the must be quoted, otherwise they mess up things.
      // note that th decoder is simpler than the encoder since all that follows a \ 
      // must be converted with the same algorithm.
      switch ( ch )
        {
        case '"':
          builder.append("\\22");   // see the position of " in ascii map
          break;
          
        case '\\':
          // I need to quote the backslash too !
          builder.append("\\5C");
          break;

        default:
          builder.append(ch);
        }
      }
    
    return builder.toString();
    }

  /**
   * Attempt to strip leading and trailing quote, if there is any...
   * If there is a leading quote there MUST be a trailing quote, otherwise it is assumed there is nothing to touch.
   * @param input
   * @return
   */
  public static final String stripLeadingTrailingQuote ( String input )
    {
    if ( input == null )   return null;
    if ( input.length() < 1 ) return "";
    
    int firstpos = input.indexOf('"');
    if ( firstpos >= 0 ) 
      {
      // I can have a leading quote , meaning a quote on the first char of the string
      int lastpos = input.lastIndexOf("\"");
      // and right after it a trailing quote
      if ( lastpos > 0 ) return input.substring(firstpos+1,lastpos);
      }

    return input;    
    }
    
  /**
   * Convert a quoted string to a normal string, this is the opposite of
   * @see Aeutils#convAsciiStringToQuotedString(java.lang.String)
   * @param input
   * @return
   */
  public static final String convAsciiQuotedStringToString ( String input )
    {
    if ( input == null ) return null;
    
    if ( input.length() < 1 ) return "";

    // The risul string should be less or equal to the imput
    StringBuilder builder = new StringBuilder(input.length());
    
    int strlen = input.length();
    for (int index=0; index<strlen; index++)
      {
      char insCh = input.charAt(index);
      
      if ( insCh == '\\' )
        {
        // I know that after a backslash there are two chars encoding the hex value of the char
        // NOTE: substring second index is NOT included in the substring, so it is +3 
        String hexChar = input.substring(index+1,index+3);
        insCh = (char)Integer.parseInt(new String(hexChar),16);
        index += 2;
        }

      builder.append(insCh);
      }
    
    return builder.toString();
    }






  
  public static Integer convStringToInteger ( String input, Integer defvalue )
    {
    try
      {
      return Integer.valueOf(input);
      }
    catch ( Exception exc )
      {
      return defvalue;
      }
    }
    
  public static final int convIntToSwappedInt ( int input )
    {
    // ultimo byte diventa il primo byte del risultato
    int risul = (input & 0xFF000000) >> 24;
    
    // il menultimo diventa il secondo byte del risultato
    risul |= (input & 0x00FF0000) >> 8;
    
    // il terzo da sinistra diventa il terzo da destra
    risul |= (input & 0x0000FF00) << 8;

    // ultimo a destra diventa il primo in alto
    risul |= (input & 0x000000FF) << 24;
    
    return risul;
    }
	
	
	public static final byte[] convIntToBytes ( int input, boolean want_little_endian )
		{
		byte []risul = new byte[4];
		if ( want_little_endian )
			{
			risul[0] = (byte)(input & 0xFF);
			input >>= 8;
			risul[1] = (byte)(input & 0xFF);
			input >>= 8;
			risul[2] = (byte)(input & 0xFF);
			input >>= 8;
			risul[3] = (byte)(input & 0xFF);
			}
		else
			{
			risul[3] = (byte)(input & 0xFF);
			input >>= 8;
			risul[2] = (byte)(input & 0xFF);
			input >>= 8;
			risul[1] = (byte)(input & 0xFF);
			input >>= 8;
			risul[0] = (byte)(input & 0xFF);
			}

		return risul;
		}
    
	/**
	 * The input value is taken to be a short value and is converted/filled in a byte array
	 * @param input
	 * @param want_little_endian
	 * @return
	 */
	public static final byte[] convShortToBytes ( int input, boolean want_little_endian )
		{
		byte []risul = new byte[2];
		if ( want_little_endian )
			{
			risul[0] = (byte)(input & 0xFF);
			input >>= 8;
			risul[1] = (byte)(input & 0xFF);
			}
		else
			{
			risul[1] = (byte)(input & 0xFF);
			input >>= 8;
			risul[0] = (byte)(input & 0xFF);
			}

		return risul;
		}


	/**
	 * this will open a file or browse a directory, depending on the file
	 * @param pdf_file
	 */
  public void browseFile ( File pdf_file )
    {
    if ( ! Desktop.isDesktopSupported() )
      {
      stat.println(classname+"browseFile: ERROR: NO DesktopSupport ! ");
      return;
      }
      
    Desktop desktop = Desktop.getDesktop();
    if ( ! desktop.isSupported(Desktop.Action.OPEN))
      {
      stat.println(classname+"browseFile: ERROR: Desktop.Action.OPEN NOT supported ! ");
      return;
      }
    
    try
      {
      stat.println(classname+"open "+pdf_file);
      desktop.open(pdf_file);
      }
    catch ( Exception exc )
      {
      stat.log.exceptionPrint(classname+"browseFile",exc);
      }
    }
    
    
/**
 * it is often needed to return a couple..
 */
public static  class DayMonth
  {
  public final int day;
  public final int month;
  
  public DayMonth ( int day, int month )
    {
    this.day = day;
    this.month = month;
    }
    
  public String toString()
    {
    return day+"/"+month;
    }
  }

	/**
	 * This returns the translated month label
	 * @param month_index
	 * @return
	 */
	public String getMonthName ( int month_index )
		{
		return stat.labelFactory.getLabel(classname, getMonthNameEnglish(month_index));
		}
	
	public String getMonthNameEnglish ( int month_index )
		{
		switch ( month_index )
			{ 
			case AetherCo.month_JANUARY:
				return "January";
				
			case AetherCo.month_FEBRUARY:
				return "February";
			
			case AetherCo.month_MARCH:
				return "March";
			
			case AetherCo.month_APRIL:
				return "April";
			
			case AetherCo.month_MAY:
				return "May";
			
			case AetherCo.month_JUNE:
				return "June";
			
			case AetherCo.month_JULY:
				return "July";
			
			case AetherCo.month_AUGUST:
				return "August";
			
			case AetherCo.month_SEPTEMBER:
				return "September";
			
			case AetherCo.month_OCTOBER:
				return "October";
			
			case AetherCo.month_NOVEMBER:
				return "November";
			
			case AetherCo.month_DECEMBER:
				return "December";

			default:
				return "M?"+month_index+"?h";
			}
		}
  
  public Date dateCalcDeltaMinutes ( Date source, int deltaMinutes )
    {
    if ( source == null ) return null;
    
    GregorianCalendar calendar = new GregorianCalendar();
    calendar.setTime(source);
    calendar.add(Calendar.MINUTE, deltaMinutes);
    
    return calendar.getTime();
    }
	
  /**
   * If the given input starts with the given string the it returns the rest of the string
   * in any other case it returns null
   * @param input
   * @param prefix
   * @return
   */
  static public String stringStartWith ( String input, String prefix )
    {
    if ( input == null || prefix == null ) return null;
    
    if ( ! input.startsWith(prefix) ) return null;
    
    return input.substring(prefix.length());
    }
  
  
  public JButton newJButton ( String label_en, ActionListener listener )
    {
    JButton risul = new JButton(label_en);
    
    if ( listener != null )
      risul.addActionListener(listener);
    
    return risul;
    }
  
  
  /**
   * Return a free port to be used
   * @return
   */
  public int findFreeTcpPort()
    {
    try
      {
      int port=0;
      // 0 means find a free port to be used
      ServerSocket s = new ServerSocket(0);
      port=s.getLocalPort();
      s.close();
      return port;
      } 
    catch (Exception exc)
      {
      stat.log.println(classname+".findFreePort", exc);
      return 0;
      }
    }

  /**
   * Return true if the given port is available to be a listen port
   * NOTE that from the time the test is made to the actual use the port may become in use again
   * @param port
   * @return
   */
  public boolean isPortAvailable(int port)
    {
    try
      {
      ServerSocket s = new ServerSocket(port);
      s.close();
      return true;
      } 
    catch (Exception exc)
      {
      stat.log.println(classname+".isPortAvailable", exc);
      return false;
      }
    }

  public static boolean SwingIsEventDispatchThread ()
    {
    return SwingUtilities.isEventDispatchThread();
    }
  
  public static void SwingInvokeAndWait ( Runnable run )
    {
    try
      {
      SwingUtilities.invokeAndWait(run);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      }
    }
    
  public static void SwingInvokeLater ( Runnable run )
    {
    try
      {
      SwingUtilities.invokeLater(run);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      }
    }
  
  }  // End of main class
