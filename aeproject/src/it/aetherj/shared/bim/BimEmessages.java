/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.bim;

import com.fasterxml.jackson.annotation.JsonInclude;

import it.aetherj.protocol.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BimEmessages implements MimDigestable
  {
  public BimChat      []chats;
  
  private void digestArray ( MimDigest digest, MimDigestable [] rows)
    {
    if ( rows == null )
      return;
    
    int len=rows.length;
    for (int index=0; index < len; index++)
      rows[index].digestPart(digest);
    }
  
  /**
   * Add this object to part of the things
   * @param digest
   */
  public void digestPart(MimDigest digest)
    {
    digestArray(digest, chats);
    }

  }


