package it.aetherj.shared.bim;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

public class BimNode extends BimEmessageHeader
  {
  // node_fingerprint is the node that has generated the message
  // owner_fingerprint is the user that has generated the message
  // owner_public_key is the key needed to verify this message
  
  public String node_name;   // this is bound to the fingerprint, once set you cannot change it
    
  public String node_info;   // you can change the node info   
  
  @Override
  public void digestPart(MimDigest digest)
    {
    // add the parent part
    super.digestPart(digest);
    
    digest.digestPart(node_name);
    }

  
  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.bimnode;
    }
  
  @Override
  public MimTimestamp peekLastUpdate()
    {
    return update_last;
    }

  @Override
  public BimNodeIndex peekMimIndex ()
    {
    BimNodeIndex idx = new BimNodeIndex();
    return idx;
    }

  
  @Override
  public boolean verifyFingerprint( ) throws Exception
    {
    MimFingerprint n_fprint = calcFingerprint();
    
    // the fingerprint is the same if the new fingerprint is the same as the one we have
    return n_fprint.equals(fingerprint);
    }
  
  /**
   * Calculate the input fingerprint
   * Fingerprint does NOT include in calculation, fingerprint, updated_pow, updated_isgnature
   * BUT: it does include the POW and signature, so, you have to do them earlier
   * SInce POW and signature include the name... there is no need to include it in here
   * So, this COULD GO INTo SUPERCLASS !!!!! 
   */
  public MimFingerprint calcFingerprint()  
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    proof_of_work.digestPart(digest);
    signature.digestPart(digest);
       
    return MimFingerprint.newFingerprint(digest);
    }

  
  /**
   * Signature is first, in Bim you MUST have a signature, you can create a throw away signature
   * So, the signature covers all relevant parts of the content
   */
  @Override
  public boolean verifySignature(CryptoEddsa25519 crypto) throws Exception
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    digest.digestPart(node_name);
    digest.digestPart(node_info);
    
    if ( update_signature.isEmpty() )
      {
      return crypto.mimVerify(this.owner_publickey, digest, signature);
      }
    else
      {
      signature.digestPart(digest);
      proof_of_work.digestPart(digest);
      update_last.digestPart(digest);
      
      return crypto.mimVerify(this.owner_publickey, digest, update_signature);
      }
    }

  /**
   * I Know that this is a NEW signature, on a new object, all the update part is not taken into consideration
   */
  public MimSignature newSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    digest.digestPart(node_name);
    digest.digestPart(node_info);
    
    return crypto.mimSign(privateKey, digest);
    }

  /**
   * This return a signature that can go in the updated part of the object, maybe I should just do it..
   * @param crypto
   * @param privateKey
   * @return
   * @throws CloneNotSupportedException
   * @throws JsonProcessingException
   */
  public MimSignature updateSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    digest.digestPart(node_name);
    digest.digestPart(node_info);

    signature.digestPart(digest);
    proof_of_work.digestPart(digest);
    update_last.digestPart(digest);
    
    return crypto.mimSign(privateKey, digest);
    }

  public MimPowValue verifyPow () throws Exception
    {
    BimPowCalculator powCalculator = new BimPowCalculator();

    if ( update_proof_of_work.isNull() )
      {
      byte []hash = calcInsertPowSource();
      boolean valid=powCalculator.isPowValid(hash, proof_of_work);
      return proof_of_work.setValid(valid);
      }
    else
      {
      byte []hash = calcUpdatePowSource();
      boolean valid=powCalculator.isPowValid(hash, update_proof_of_work);
      return update_proof_of_work.setValid(valid);
      }
    }

  /**
   * calculate the POW source to be used on insert
   * POW includes the signature, so, no need to include the rest
   */
  public byte [] calcInsertPowSource ( ) 
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    signature.digestPart(digest);

    return digest.digestEnd();
    }

  /**
   * calculate the POW source to be used on object update
   */
  public byte [] calcUpdatePowSource ( ) 
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    signature.digestPart(digest);

    update_last.digestPart(digest);
    update_signature.digestPart(digest);
    
    return digest.digestEnd();
    }



  }
