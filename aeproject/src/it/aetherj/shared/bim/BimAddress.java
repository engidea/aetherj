/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.bim;

import java.net.*;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 */
 

public class BimAddress extends BimEmessageHeader
  {
  public static final int url_len_max=200;
  
  public static final int location_type_IIPv4=4;
  public static final int location_type_IIPv6=6;
    
  public String url_host=""; 
  public int    location_type=location_type_IIPv4;  // assume IP v4
  public int    port;
    
  public BimAddress()
    {
    }

  public BimAddress (int listen_port)
    {
    port=listen_port;
    }

  public BimAddress (String url_host, int port)
    {
    this.url_host=url_host;
    this.port=port;
    }


  /**
   * Something like host:port do NOT include the first https:// or http://
   * @param from_url
   */
  public BimAddress ( String from_url )
    {
    try
      {
      if ( ! from_url.startsWith("https://"))
        from_url="https://"+from_url;

      URL url = new URL(from_url);
      url_host = url.getHost();
      port= url.getPort();
      validate();
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      throw new IllegalArgumentException("BAD address "+from_url);
      }
    }

  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.bimaddress;
    }

  public String toUrl ()
    {
    try
      {
      return new URL("https",url_host,port,"").toString();
      } 
    catch (MalformedURLException e)
      {
      e.printStackTrace();
      return null;
      }
    }
  
  /**
   * Add this object to part of the things
   * @param digest
   */
  @Override
  public void digestPart(MimDigest digest)
    {
    digest.digestPart(url_host+location_type+port);
    }

  public void validate ()
    {
    if ( url_host == null || url_host.length() < 3 )
      url_host="null_or_empty";
    
    if ( port == 0 )
      port=80;
    
    if ( location_type==0 ) 
      location_type=location_type_IIPv4;
    }

  
  @Override
  public String toString ()
    {
    StringBuilder risul = new StringBuilder(200);
    
    risul.append(url_host);
    risul.append(':');
    risul.append(port);
    
    return risul.toString();
    }

  @Override
  public boolean verifyFingerprint() throws Exception
    {
    return true;
    }
  
  @Override
  public boolean verifySignature(CryptoEddsa25519 crypto) throws Exception
    {
    return true;
    }

  @Override
  public MimFingerprint peekFingerprint()
    {
    return null;
    }

  @Override
  public MimPowValue verifyPow() throws Exception
    {
    return new MimPowValue();
    } 

  @Override
  public MimTimestamp peekLastUpdate()
    {
    return update_last;
    }

  @Override
  public MimIndexesMethods peekMimIndex ()
    {
    return null;
    }

  }
