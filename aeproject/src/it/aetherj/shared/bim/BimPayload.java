/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.bim;

import java.security.PrivateKey;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * Bim is the version B of Mim 
 *  
 */ 

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BimPayload implements MimEntityMethods
  {
  public MimFingerprint node_fingerprint;   // TODO The idea is that I have the info on who is generating this payload 
  public MimSignature   node_signature;     // Source node signature, this is the old page_signature
  public MimPublicKey   node_public_key;    // Public key to check the signature
  public MimPowValue    proof_of_work;      
  public MimNonce       nonce;              // need a value
  public BimAddress     address;            // must be not null at send time
  public MimEntity      entity;             // what this thing is
  public MimFilter []   filters;            // empty response do not have this field
  public MimTimestamp   timestamp=new MimTimestamp();   // MUST be NOW since it used by the nonce algorithm 
  public MimTimestamp   starts_from;       // WHAT IS THIS FOR ???  
  public MimTimestamp   ends_at;           // WHAT IS THIS FOR ???
  public BimEmessages   emessages = new BimEmessages();  // MUST create it
  
  public BimPayload()
    {
    // for Json serialization
    }
  
  public BimPayload(MimEntity entity)
    {
    this.entity = entity; 
    }
  
  public void signPayload (CryptoEddsa25519 crypto, PrivateKey privateKey ) 
    {
    // you need to sum up all beside the signature itself and the proof_of_work  
    MimDigest digest = new MimDigest();

    node_fingerprint.digestPart(digest);
    node_public_key.digestPart(digest);
    nonce.digestPart(digest);
    address.digestPart(digest);
    entity.digestPart(digest);
    timestamp.digestPart(digest);
    emessages.digestPart(digest);

    node_signature = crypto.mimSign(privateKey, digest);
    }

  /**
   * Signature need to check that the page has not been tampered with, so, it is mostly on everything beside
   * node_signature and proof of work
   */
  public boolean isSignatureValid ( CryptoEddsa25519 crypto ) throws JsonProcessingException
    {
    if ( node_signature == null || node_signature.isEmpty() )
      return true;
    
    if ( node_public_key == null || node_public_key.isNull() )
      return true;
    
    // you need to sum up all beside the signature itself and the proof_of_work  
    MimDigest digest = new MimDigest();
    
    node_fingerprint.digestPart(digest);
    node_public_key.digestPart(digest);
    nonce.digestPart(digest);
    address.digestPart(digest);
    entity.digestPart(digest);
    timestamp.digestPart(digest);
    emessages.digestPart(digest);
    
    return crypto.mimVerify(node_public_key, digest, node_signature);
    }
  
  /**
   * return true if Pow is valid
   * @return
   */
  public boolean isPowValid ()  
    {
    MimDigest digest = new MimDigest();
    
    node_fingerprint.digestPart(digest);
    node_public_key.digestPart(digest);
    nonce.digestPart(digest);
    address.digestPart(digest);
    entity.digestPart(digest);
    timestamp.digestPart(digest);
    emessages.digestPart(digest);
    node_signature.digestPart(digest); // adding the signature to POW
    
    BimPowCalculator powCalculator = new BimPowCalculator();
    
    boolean valid = powCalculator.isPowValid(digest.digestEnd(), proof_of_work);
    
    proof_of_work.setValid(valid);
    
    return valid;
    }
  
  @Override    
  public String toString()
    {
    StringBuilder risul = new StringBuilder(200);
    
    risul.append("entity="+entity);
    risul.append(" host="+address.url_host);
    risul.append(" port="+address.port);
    
    return risul.toString();
    }

  @Override
  public MimEntity getMimEntity()
    {
    return entity;
    }
  }
