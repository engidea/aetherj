/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.bim;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * The idea is that I will search the db table for things to send and once I have sent them I will delete the row.... maybe
 *  
 */
public class BimChat extends BimEmessageHeader
  {
  // node_fingerprint is the node that has generated the message
  // owner_fingerprint is the user that has generated the message
  //owner_public_key is the key needed to verify this message
  
  public BimAddress     from_url;           // provided to send the reply to right address, may me null ?
  
  public BimNode        to_node;            //
  public BimAddress     to_url;             // 
  public MimFingerprint to_ukey_fprint;     // needed to identify destination
  
  public String chat_message;

  /**
   * Used to do basic verification of a message, include only vital parts
   * Adding where the message comes from or where it is heading is not really useful
   */
  @Override
  public void digestPart(MimDigest digest)
    {
    // add the parent part
    super.digestPart(digest);
    
    to_ukey_fprint.digestPart(digest);
    
    digest.digestPart(chat_message);
    }

  
  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aechat;
    }
  
  @Override
  public MimTimestamp peekLastUpdate()
    {
    return update_last;
    }

  @Override
  public MimPostIndex peekMimIndex ()
    {
    MimPostIndex idx = new MimPostIndex();
    return idx;
    }

  @Override
  public boolean verifyFingerprint( ) throws Exception
    {
    MimFingerprint n_fprint = calcFingerprint();
    return n_fprint.equals(fingerprint);
    }
  
  /**
   * Note that fingerprint stay the same, even if message is updated
   * It is the "primary key" of the message, it includes the POW and the signature, no need to include the message again
   * TODO make it into superclass !!
   */
  public MimFingerprint calcFingerprint() throws CloneNotSupportedException, JsonProcessingException 
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    proof_of_work.digestPart(digest);
    signature.digestPart(digest);
       
    return MimFingerprint.newFingerprint(digest);
    }

  
  
  @Override
  public boolean verifySignature(CryptoEddsa25519 crypto) throws Exception
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    to_ukey_fprint.digestPart(digest);
    digest.digestPart(chat_message);
    
    if ( update_signature.isEmpty() )
      {
      return crypto.mimVerify(this.owner_publickey, digest, signature);
      }
    else
      {
      signature.digestPart(digest);
      proof_of_work.digestPart(digest);
      update_last.digestPart(digest);
      
      return crypto.mimVerify(this.owner_publickey, digest, update_signature);
      }
    }

  /**
   * I Know that this is a NEW signature, on a new object
   */
  public MimSignature newSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    to_ukey_fprint.digestPart(digest);
    digest.digestPart(chat_message);

    return crypto.mimSign(privateKey, digest);
    }

  public MimSignature updateSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    to_ukey_fprint.digestPart(digest);
    digest.digestPart(chat_message);

    signature.digestPart(digest);
    proof_of_work.digestPart(digest);
    update_last.digestPart(digest);

    return crypto.mimSign(privateKey, digest);
    }

  @Override
  public MimPowValue verifyPow () throws Exception
    {
    BimPowCalculator powCalculator = new BimPowCalculator();

    if ( update_proof_of_work.isNull() )
      {
      byte []hash = calcInsertPowSource();
      boolean valid=powCalculator.isPowValid(hash, proof_of_work);
      return proof_of_work.setValid(valid);
      }
    else
      {
      byte []hash = calcUpdatePowSource();
      boolean valid=powCalculator.isPowValid(hash, update_proof_of_work);
      return update_proof_of_work.setValid(valid);
      }
    }

  /**
   * calculate the POW source to be used on insert
   * POW includes the signature, so, no need to include the rest
   */
  public byte [] calcInsertPowSource ( ) 
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    signature.digestPart(digest);

    return digest.digestEnd();
    }

  /**
   * calculate the POW source to be used on object update
   */
  public byte [] calcUpdatePowSource ( ) 
    {
    MimDigest digest = new MimDigest();
    
    super.digestPart(digest);
    signature.digestPart(digest);

    update_last.digestPart(digest);
    update_signature.digestPart(digest);
    
    return digest.digestEnd();
    }



  }
