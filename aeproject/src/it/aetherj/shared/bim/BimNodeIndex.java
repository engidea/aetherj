package it.aetherj.shared.bim;

import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

public class BimNodeIndex implements MimIndexesMethods
  {
  public MimFingerprint fingerprint;
  public MimTimestamp   creation;
  public MimTimestamp   lastupdate;
  public int            entity_version;
  
  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.bimnode;
    }
  }