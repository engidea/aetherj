package it.aetherj.shared.v0;

import it.aetherj.protocol.*;

/**

type BoardIndex struct {
  Fingerprint   Fingerprint `json:"fingerprint"`
  Owner         Fingerprint `json:",omitempty"`
  Creation      Timestamp   `json:"creation"`
  LastUpdate    Timestamp   `json:"last_update"`
  EntityVersion int         `json:"entity_version"`
  PageNumber    int         `json:"page_number"`
}

 */
public class MimBoardIndex implements MimIndexesMethods
  {
  public MimFingerprint fingerprint;
  public MimFingerprint Owner;
  public MimTimestamp   creation;
  public MimTimestamp   lastupdate;
  public int            entity_version;
  public int            page_number=0;

  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aeboard;
    }
  }
