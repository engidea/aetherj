package it.aetherj.shared.v0;

import com.fasterxml.jackson.annotation.*;

/**
 * This is here to give a type to the lang field
 * @author damiano
 *
 */
public class MimLang
  {
  public static final int len_max=20;
  
  private String lang;
  
  public MimLang()
    {
    }
  
  @JsonCreator
  public MimLang(String lang)
    {
    this.lang=lang;
    }
  
  public boolean isNull()
    {
    return lang==null;
    }
  
  @JsonValue
  public String toString ()
    {
    // mim wants an empty value on null content, there is a difference between null and empty, but who cares....
    return lang == null ? "" : lang;
    }
  }
