package it.aetherj.shared.v0;

import it.aetherj.protocol.*;

/*

type TruststateIndex struct {
  Fingerprint   Fingerprint `json:"fingerprint"`
  Owner         Fingerprint `json:",omitempty"`
  Target        Fingerprint `json:"target"`
  Creation      Timestamp   `json:"creation"`
  LastUpdate    Timestamp   `json:"last_update"`
  EntityVersion int         `json:"entity_version"`
  PageNumber    int         `json:"page_number"`
}

 */
public class MimTruststateIndex implements MimIndexesMethods
  {
  public MimFingerprint fingerprint;
  public MimFingerprint Owner;
  public MimFingerprint target;
  public MimTimestamp   creation;
  public MimTimestamp   lastupdate;
  public int            entity_version;
  public int            page_number;
  
  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aetrustate;
    }
  }
