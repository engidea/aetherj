/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 

type Answer struct { // Bodies of API Endpoint responses from remote. This will be filled and unused field will be omitted.
  Boards      []Board      `json:"boards,omitempty"`
  Threads     []Thread     `json:"threads,omitempty"`
  Posts       []Post       `json:"posts,omitempty"`
  Votes       []Vote       `json:"votes,omitempty"`
  Keys        []Key        `json:"keys,omitempty"`
  Truststates []Truststate `json:"truststates,omitempty"`
  Addresses   []Address    `json:"addresses,omitempty"`

  BoardIndexes      []BoardIndex      `json:"boards_index,omitempty"`
  ThreadIndexes     []ThreadIndex     `json:"threads_index,omitempty"`
  PostIndexes       []PostIndex       `json:"posts_index,omitempty"`
  VoteIndexes       []VoteIndex       `json:"votes_index,omitempty"`
  KeyIndexes        []KeyIndex        `json:"keys_index,omitempty"`
  TruststateIndexes []TruststateIndex `json:"truststates_index,omitempty"`
  AddressIndexes    []AddressIndex    `json:"addresses_index,omitempty"`

  BoardManifests      []PageManifest `json:"boards_manifest,omitempty"`
  ThreadManifests     []PageManifest `json:"threads_manifest,omitempty"`
  PostManifests       []PageManifest `json:"posts_manifest,omitempty"`
  VoteManifests       []PageManifest `json:"votes_manifest,omitempty"`
  KeyManifests        []PageManifest `json:"keys_manifest,omitempty"`
  TruststateManifests []PageManifest `json:"truststates_manifest,omitempty"`
  AddressManifests    []PageManifest `json:"addresses_manifest,omitempty"`
}

 */


@JsonInclude(JsonInclude.Include.NON_NULL)
public class MimAnswer
  {
  public MimBoard      []boards;
  public MimThread     []threads;
  public MimPost       []posts;
  public MimVote       []votes;
  public MimKey        []keys;
  public MimTruststate []truststates;
  public MimAddress    []addresses;
  
  public MimBoardIndex   []boards_index;
  public MimThreadIndex  []threads_index;
  public MimPostIndex    []posts_index;
  public MimVoteIndex    []votes_index;
  public MimKeyIndex     []keys_index;
  public MimTruststateIndex []truststates_index;
  public MimAddress      []addresses_index;
  
  public MimPageManifest []boards_manifest;
  public MimPageManifest []threads_manifest;
  public MimPageManifest []posts_manifest;
  public MimPageManifest []votes_manifest;
  public MimPageManifest []keys_manifest;
  public MimPageManifest []truststates_manifest;
  public MimPageManifest []addresses_manifest;
  }


