/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

public class MimClient implements MimGetSignatureSource
  {
  public static final int    name_len_max=255;   // as defined in the original DB
  public static final int    vmajor_old=2;
  public static final String default_name="Aether";
  
  public int version_major=vmajor_old;
  public int version_minor;
  public int version_patch;
  public String name=default_name;
  
  public void validate ()
    {
    if ( version_major==0 )
      version_major=vmajor_old;
    
    if ( name == null || name.length() < 1 )
      name = default_name;
    }
  
  @Override
  public void getPowSource(StringBuilder result)
    {
    result.append(name);
    result.append(version_major);
    }
  }
