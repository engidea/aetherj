/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import java.security.PrivateKey;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;
import it.aetherj.shared.AeJson;

/**
 * 

type ApiResponse struct {
  NodeId        Fingerprint   `json:"-"` // Generated and used at the ApiResponse signature verification, from the NodePublicKey. It doesn't transmit in or out, only generated on the fly. This blocks both inbound and outbound.
  NodePublicKey string        `json:"node_public_key,omitempty"`
  Signature     Signature     `json:"page_signature,omitempty"`
  ProofOfWork   ProofOfWork   `json:"proof_of_work"`
  Nonce         Nonce         `json:"nonce,omitempty"`
  EntityVersion int           `json:"entity_version,omitempty"`
  Address       Address       `json:"aeaddres,omitempty"`
  Entity        string        `json:"entity,omitempty"`
  Endpoint      string        `json:"endpoint,omitempty"`
  Filters       []Filter      `json:"filters,omitempty"`
  Timestamp     Timestamp     `json:"timestamp,omitempty"`
  StartsFrom    Timestamp     `json:"starts_from,omitempty"`
  EndsAt        Timestamp     `json:"ends_at,omitempty"`
  Pagination    Pagination    `json:"pagination,omitempty"`
  Caching       Caching       `json:"caching,omitempty"`
  Results       []ResultCache `json:"results,omitempty"`  // Pages
  ResponseBody  Answer        `json:"response,omitempty"` // Entities, Full size or Index versions.
}

 *  
 */ 

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MimPayload implements MimEntityMethods
  {
  public MimPublicKey  node_public_key;   // need a value
  public MimSignature  page_signature;    // normally need a value, leave null on construct 
  public MimPowValue   proof_of_work=new MimPowValue();  // need a "" value to calculate pow
  public MimNonce      nonce;              // need a value
  public int           entity_version=1;
  public MimAddress    address;            // must be not null at send time
  public MimEntity     entity;             // what this thing is
  public MimEndpoint   endpoint;           // coming from
  public MimFilter []  filters;            // empty response do not have this field
  public MimTimestamp  timestamp=new MimTimestamp();   // MUST be NOW since it used by the nonce algorithm 
  public MimTimestamp  starts_from;       // WHAT IS THIS FOR ???  
  public MimTimestamp  ends_at;           // WHAT IS THIS FOR ???
  public MimPagination pagination=new MimPagination();
  public MimCaching    caching = new MimCaching();
  public MimResultsCache []results;             // this may be null
  public MimAnswer     response = new MimAnswer();  // MUST create it
  
  public MimPayload()
    {
    // for Json serialization
    }
  
  public MimPayload(MimEntity entity)
    {
    this.entity = entity; 
    }
  
  
  public void signPayload (CryptoEddsa25519 crypto, PrivateKey privateKey ) throws JsonProcessingException
    {
    // zap possibly previous signature, note that it is a null and NOT an empty value
    page_signature = null;
    
    AeJson om = new AeJson();
    String source = om.writeValueAsString(this);
    page_signature = crypto.mimSign(privateKey, source);
    }
  
  /**
   * See line 537 of createverify.go and note that line 466 of apistructs.go say
   * Signature     Signature     `json:"page_signature,omitempty"`
   * @param crypto
   * @return
   * @throws JsonProcessingException
   */
  public boolean isSignatureValid ( CryptoEddsa25519 crypto ) throws JsonProcessingException
    {
    if ( page_signature == null || page_signature.isEmpty() )
      return true;
    
    if ( node_public_key == null || node_public_key.isNull() )
      return true;
    
    // save  
    MimSignature sign_s = page_signature;
    MimPowValue pow_s = proof_of_work;
    
    // zap it, golang does not have it into the marshalling, crappy golang
    // this means that when I am testing the signature I HAVE to null this field...
    page_signature = null;
    
    // However, for some reason, this other field should be initialized as Empty
    // Note that the golang verify does not do it, probably it is in the "correct spot" ....
    proof_of_work = new MimPowValue();
     
    AeJson om = new AeJson();
    String source = om.writeValueAsString(this);
    
    // put signature back...
    page_signature = sign_s;
    proof_of_work = pow_s;
    
    boolean pl_valid = crypto.mimVerify(node_public_key, source, sign_s);
    
    if ( ! pl_valid )
      source=null;
     
    return pl_valid;
    }
  
  
  public MimPowValue verifyPow (MimPowCalculator powCalculator) throws JsonProcessingException 
    {
    MimPowValue saved = proof_of_work;
    
    // zap the current value
    proof_of_work=new MimPowValue();

    if ( powCalculator == null )
      powCalculator = new MimPowCalculator();
    
    AeJson om = new AeJson();
    String s = om.writeValueAsString(this);

    saved.setValid(powCalculator.isPowValid(s, saved));
    
    proof_of_work=saved;
    
    return saved;
    }
  
  @Override    
  public String toString()
    {
    StringBuilder risul = new StringBuilder(200);
    
    risul.append("endpoint="+endpoint);
    risul.append("entity="+entity);
    risul.append("aeaddres.port="+address.port);
    
    return risul.toString();
    }

  @Override
  public MimEntity getMimEntity()
    {
    return entity;
    }
  }
