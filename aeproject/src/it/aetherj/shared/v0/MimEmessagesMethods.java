/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;

/**
 * This applies to Entity messages, not all entity are messages
 * Basically, for a given message I wish to be able to do a set of methods
 */
public interface MimEmessagesMethods extends MimEntityMethods
  {
  
  /**
   * This method verifies the fingerprint
   * @return true if fingerprint is good
   */
  public boolean verifyFingerprint( ) throws Exception;

  /**
   * This the signature
   */
  public boolean verifySignature(CryptoEddsa25519 crypto) throws Exception;  
  

  /**
   * Proof of Work
   * I return the PowValue since I may need to further verify it using the signature or check the strength
   */
  public MimPowValue verifyPow() throws Exception;  

  /**
   * Cannot have a get Fingerprint otherwise Json will not convert the field fingerprint
   * @return
   */
  public MimFingerprint peekFingerprint();
  
  /**
   * It is not just the last update field it is the creation if last_update is null
   * @return a non null timestamp in any case
   */
  public MimTimestamp peekLastUpdate();
  
  
  public MimIndexesMethods peekMimIndex ();
  
  
  
  }
