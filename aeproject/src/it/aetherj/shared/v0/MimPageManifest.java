package it.aetherj.shared.v0;

/**
 * Apparently this does not have the entity it belongs to in the API.....
 * So, I need a hack to have it, eventually
 * 
 
 // Manifest type
type PageManifest struct {
  Page     uint64               `json:"page_number"`
  Entities []PageManifestEntity `json:"entities"`
}

type PageManifestEntity struct {
  Fingerprint Fingerprint `json:"fingerprint"`
  LastUpdate  Timestamp   `json:"last_update"`

 */

public class MimPageManifest
  {
  public int page_number;
  public MimPageManifestE []entities;

  public MimPageManifest()
    {
    }
  
  public MimPageManifest(int page_number, MimPageManifestE []entities)
    {
    this.page_number=page_number;
    this.entities=entities;
    }
  
  public MimPageManifest []tosSingleArray()
    {
    MimPageManifest []risul = new MimPageManifest[1];
    risul[0]=this;
    return risul;
    }
  
  public String toString()
    {
    if ( entities != null )
      return page_number+" e_count="+entities.length;
    else
      return page_number+" null entities";
    }
  }
