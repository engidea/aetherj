/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import java.util.Random;

import com.fasterxml.jackson.annotation.*;

import it.aetherj.protocol.*;

/**
 * A nonce is just some random
 * A class is needed to wrap behavior
 */
public class MimNonce
  {
  public static final int NONCE_STR_LEN=64;       // len of a transmitted nonce 

  private String nonce;
  
  public MimNonce()
    {
    try
      {
      byte [] randBytes = new byte[NONCE_STR_LEN/2];
      Random randomAlgo = new Random(System.currentTimeMillis());
      randomAlgo.nextBytes(randBytes);
      nonce=MimHex.convBinToHexString(randBytes);
      }
    catch ( Exception exc )
      {
      nonce="12121212121212121212121212121212121212121212";
      }
    }
  
  /**
   * Add this object to part of the things
   * @param digest
   */
  public void digestPart(MimDigest digest)
    {
    digest.digestPart(nonce);
    }

  
  public boolean equals(Object obj)
    {
    if ( obj instanceof MimNonce )
      {
      MimNonce other = (MimNonce)obj;
      
      return nonce.equals(other.nonce);
      }
    else
      return false;
    }
  
  public int hashCode ()
    {
    if ( nonce == null ) 
      return 0;
    
    return nonce.hashCode();
    }

  public boolean isNull ()
    {
    return nonce == null || nonce.length() < NONCE_STR_LEN/2;
    }
  
  @JsonCreator
  public MimNonce(String nonce)
    {
    this.nonce=nonce;
    }
    
  @JsonValue
  public String toString()
    {
    return nonce;
    }
  }
