/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;
import it.aetherj.shared.AeJson;

/**
 
type Vote struct { // Mutables: Type, Meta
  ProvableFieldSet
  Board          Fingerprint `json:"board"`
  Thread         Fingerprint `json:"thread"`
  Target         Fingerprint `json:"target"`
  Owner          Fingerprint `json:"owner"`
  OwnerPublicKey string      `json:"owner_publickey"`
  TypeClass      int         `json:"typeclass"`
  Type           int         `json:"type"`
  EntityVersion  int         `json:"entity_version"`
  Meta           string      `json:"meta"`
  RealmId        Fingerprint `json:"realm_id"`
  EncrContent    string      `json:"encrcontent"`
  UpdateableFieldSet
}

type UpdateableFieldSet struct { // Common set of properties for all objects that are updateable.
  LastUpdate        Timestamp   `json:"last_update"`
  UpdateProofOfWork ProofOfWork `json:"update_proof_of_work"`
  UpdateSignature   Signature   `json:"update_signature"`
}

I could extend a class to include all updateableFieldSet, BUT the ordering is wrong, they are ath the end, not at the beginning...
 
 *
 */
public class MimVote extends MimProvableFieldSet 
  {
  // Many thanks to nytpu aether://user/22262a061f1dca376d8640a1ada3364ac3307b370bfbf868c1ea4cf64d4e2c38 for this investigation
  public static final int typclass_discussion=1;   // thread or comment
  public static final int typclass_report=2;       //
  public static final int typclass_moderate=3;     //

  public static final int type_NULL=0;             // This might be the way to have a no vote...
  public static final int type_upvote=1;           // This is an up vote
  public static final int type_downvote=2;         // This is a down vote
  
  public static final int default_entity_v=1;   // 
  
  public MimFingerprint board;
  public MimFingerprint thread;
  public MimFingerprint target;   // The thread, the post, or the vote, ha ha , you guess who it is, have fun
  public MimFingerprint owner;
  public MimPublicKey owner_publickey;
  public int typeclass=typclass_discussion;  
  public int type=type_NULL;
  public int entity_version=default_entity_v;
  public MimMeta meta=new MimMeta();           // must be non null to match signature
  public MimFingerprint realm_id=new MimFingerprint();    
  public String encrcontent="";
   
  // the following will be in more than one class...
  public MimTimestamp last_update;
  public MimPowValue  update_proof_of_work;
  public MimSignature update_signature;

  @Override
  public MimEntity getMimEntity() 
    {
    return MimEntityList.aevote;
    }

  @Override
  public MimTimestamp peekLastUpdate()
    {
    return last_update.isNull() ? creation : last_update;
    }

  @Override
  public MimVoteIndex peekMimIndex ()
    {
    MimVoteIndex idx = new MimVoteIndex();
    idx.fingerprint = fingerprint;
    idx.Owner = owner;
    idx.board = board;
    idx.thread = thread;
    idx.target = target;
    idx.creation = creation;
    idx.creation = creation;
    idx.lastupdate = last_update;
    idx.entity_version = entity_version;
    
    return idx;
    }

  /**
   * Returns the string version of the object ready for FIngerprint
   * See io/api/cvset-v1.go line 723
   */
  @Override
  public boolean verifyFingerprint( ) throws Exception
    {
    MimFingerprint n_fprint = calcFingerprint();
    return n_fprint.equals(fingerprint);
/*
    MimVote risul = (MimVote) this.clone();
    
    risul.last_update=new MimTimestamp(0);
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    risul.type=0;
    risul.meta=new MimMeta();
    
    risul.fingerprint=new MimFingerprint();
    
    AeJson om = new AeJson();
    String s=om.writeValueAsString(risul);
    return fingerprint.verify(s);
*/    
    }
  
  /**
   * Note how fingerprint depends on POW
   */
  public MimFingerprint calcFingerprint() throws CloneNotSupportedException, JsonProcessingException
    {
    MimVote risul = (MimVote) this.clone();
    
    risul.last_update=new MimTimestamp(0);
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    risul.type=type_NULL;
    risul.meta=new MimMeta();
    risul.fingerprint=new MimFingerprint();
    
    AeJson om = new AeJson();
    String s=om.writeValueAsString(risul);
    return MimFingerprint.newFingerprint(s);
    }
  
  /**
   * I Know that this is a NEW signature, on a new object
   */
  public void firstSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimVote risul = (MimVote) this.clone();

    // a signature on a new object assumes all this fields are empty
    risul.signature=new MimSignature();
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    risul.last_update=new MimTimestamp(0); 
    risul.fingerprint=new MimFingerprint();
    risul.proof_of_work=new MimPowValue();

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    signature=crypto.mimSign(privateKey, s);
    }

  public void updateSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimVote risul = (MimVote) this.clone();

    // a signature on an update object assume just the following are empty
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    update_signature=crypto.mimSign(privateKey, s);
    }

  
  @Override
  public boolean verifySignature(CryptoEddsa25519 crypto) throws Exception
    {
    MimVote risul = (MimVote) this.clone();

    // assume it is this one
    MimSignature signature = risul.update_signature;
    
    risul.update_signature=new MimSignature();
    risul.update_proof_of_work=new MimPowValue();
    
    if ( this.update_signature.isEmpty() )
      {
      // since the update signature is empty, then, use the normal one
      signature = risul.signature;

      risul.fingerprint=new MimFingerprint();
      risul.last_update=new MimTimestamp(0); 
      risul.signature=new MimSignature();
      risul.proof_of_work=new MimPowValue();
      }

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    return crypto.mimVerify(this.owner_publickey, s, signature);
    }

  /**
   * calculate the POW source to be used on insert
   */
  public String calcInsertPowSource ( ) throws JsonProcessingException, CloneNotSupportedException
    {
    MimVote risul = (MimVote) this.clone();

    risul.fingerprint=new MimFingerprint();
    risul.last_update=new MimTimestamp(0); 
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    risul.proof_of_work=new MimPowValue();
    risul.proof_of_work=new MimPowValue();

    AeJson om = new AeJson();
    return om.writeValueAsString(risul);
    }

  /**
   * calculate the POW source to be used on object update
   */
  public String calcUpdatePowSource ( ) throws JsonProcessingException, CloneNotSupportedException
    {
    // it is fair to zap the current one since it is the only one being replaced
    update_proof_of_work=new MimPowValue();
    
    AeJson om = new AeJson();
    return om.writeValueAsString(this);
    }

  
  /**
   * If a pow calculator is provided then it will be used, otherwise a new one will be created
   * NOTE: A powCalculator is non reentrant, you MUST ensure it is used single thread
   * If Aether was done in a minimal OO way a LOT of code could have gone into a superclass
   * @param powCalculator
   * @return
   * @throws Exception
   */
  @Override
  public MimPowValue verifyPow () throws Exception
    {
    MimVote risul = (MimVote) this.clone();

    // assume it is this one
    MimPowValue pow = risul.update_proof_of_work;
    
    risul.update_proof_of_work=new MimPowValue();
    
    if ( this.update_proof_of_work.isNull() )
      {
      // since the update POW is empty, then, use the normal one
      pow = risul.proof_of_work;

      risul.fingerprint=new MimFingerprint();
      risul.last_update=new MimTimestamp(0); 
      risul.proof_of_work=new MimPowValue();
      }

    MimPowCalculator powCalculator = new MimPowCalculator();
    
    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);

    pow.setValid(powCalculator.isPowValid(s, pow));
    
    return pow;
    }
  }
