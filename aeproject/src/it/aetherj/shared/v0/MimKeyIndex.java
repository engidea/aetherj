package it.aetherj.shared.v0;

import it.aetherj.protocol.*;

/*
 

type KeyIndex struct {
  Fingerprint   Fingerprint `json:"fingerprint"`
  Creation      Timestamp   `json:"creation"`
  LastUpdate    Timestamp   `json:"last_update"`
  EntityVersion int         `json:"entity_version"`
  PageNumber    int         `json:"page_number"`
}

 */
public class MimKeyIndex implements MimIndexesMethods
  {
  public MimFingerprint fingerprint;
  public MimTimestamp   creation;
  public MimTimestamp   lastupdate;
  public int            entity_version;
  public int            page_number;

  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aekey;
    }
  }
