package it.aetherj.shared.v0;

import com.fasterxml.jackson.annotation.*;

/**
 * Meta is a boilerplate for everything, yes, let's dump types and everything is a string, great
 */
public class MimMeta
  {
  // in Aether DB it is a text, meaning possibly unlimited len... should I have a CLOB ?
  public static final int len_max=65535;
  
  private String meta;
  
  public MimMeta()
    {
    }
  
  @JsonCreator
  public MimMeta(String meta)
    {
    this.meta=meta;
    }
  
  public boolean isNull()
    {
    return meta==null;
    }
  
  @JsonValue
  public String toString ()
    {
    // mim wants an empty value on null content, there is a difference between null and empty, but who cares....
    return meta == null ? "" : meta;
    }

  }
