/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;
import it.aetherj.shared.AeJson;

/**
 * Ok, found out this is the public key of users, basically, the main authentication method
 * As usual you CANNOT change variables names OR order
 
type Key struct { // Mutables: Expiry, Info, Meta
  ProvableFieldSet
  Type          string      `json:"type"`
  Key           string      `json:"key"`
  Expiry        Timestamp   `json:"expiry"`
  Name          string      `json:"name"`
  Info          string      `json:"info"`
  EntityVersion int         `json:"entity_version"`
  Meta          string      `json:"meta"`
  RealmId       Fingerprint `json:"realm_id"`
  EncrContent   string      `json:"encrcontent"`
  UpdateableFieldSet
}

type UpdateableFieldSet struct { // Common set of properties for all objects that are updateable.
  LastUpdate        Timestamp   `json:"last_update"`
  UpdateProofOfWork ProofOfWork `json:"update_proof_of_work"`
  UpdateSignature   Signature   `json:"update_signature"`
}

I could extend a class to include all updateableFieldSet, BUT the ordering is wrong, they are at the end, not at the beginning...
Also, what is a mutable field is just a comment in golang... the way it should have been is two classes, one for constant and one for mutable fields.
 
 */
public class MimKey extends MimProvableFieldSet 
  {
  public static final String type_ed25519="ed25519";
  
  public String       type=type_ed25519;   // normally ed25519
  public MimPublicKey key;        
  public MimTimestamp expiry=new MimTimestamp(0);    // normally 0
  public String       name;              // this is the username registered in Aether
  public String       info;              // whatever the user wrote when created account
  public int          entity_version=1;                // normally 1
  public String       meta="";                         // empty
  public MimFingerprint realm_id=new MimFingerprint(); // empty   
  public String   encrcontent="";                      // empty
   
  // It is appropriate to have a last update to zero
  public MimTimestamp last_update=new MimTimestamp(0);
  public MimPowValue  update_proof_of_work;
  public MimSignature update_signature;

  @JsonIgnore
  public MimPrivateKey privateKey;  // local users have private key stored

  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aekey;
    }
  
  @Override
  public MimTimestamp peekLastUpdate()
    {
    return last_update.isNull() ? creation : last_update;
    }

  @Override
  public MimKeyIndex peekMimIndex ()
    {
    MimKeyIndex idx = new MimKeyIndex();
    idx.fingerprint = fingerprint;
    idx.creation    = creation;
    idx.lastupdate  = last_update;
    idx.entity_version = entity_version;
    
    return idx;
    }

  /**
   * See cvset-v1.go line 759
   */
  @Override
  public boolean verifyFingerprint( ) throws Exception
    {
    MimFingerprint n_fprint = calcFingerprint();
    return n_fprint.equals(fingerprint);

    /*
    MimKey risul = (MimKey) this.clone();
    
    risul.last_update=new MimTimestamp(0);
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    risul.info="";
    risul.expiry=new MimTimestamp(0);
    risul.meta="";
    
    risul.fingerprint=new MimFingerprint();
    
    AeJson om = new AeJson();
    String s=om.writeValueAsString(risul);
    
    return fingerprint.verify(s);
    */
    }

  public MimFingerprint calcFingerprint () throws JsonProcessingException, CloneNotSupportedException
    {
    MimKey risul = (MimKey) this.clone();
    
    risul.last_update=new MimTimestamp(0);
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    risul.info="";
    risul.expiry=new MimTimestamp(0);
    risul.meta="";
    
    risul.fingerprint=new MimFingerprint();
  
    AeJson om = new AeJson();
    String s=om.writeValueAsString(risul);
    return MimFingerprint.newFingerprint(s);
    }
  
  /**
   * See cvset-v1.go line 1323
   */
  @Override
  public boolean verifySignature(CryptoEddsa25519 crypto) throws Exception
    {
    MimKey risul = (MimKey) this.clone();

    // assume it is this one
    MimSignature signature = risul.update_signature;
    
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    if ( this.update_signature.isEmpty() )
      {
      // since the update signature is empty, then, use the normal one
      signature = risul.signature;

      risul.last_update=new MimTimestamp(0); 
      risul.fingerprint=new MimFingerprint();
      risul.signature=new MimSignature();
      risul.proof_of_work=new MimPowValue();
      }

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    return crypto.mimVerify(this.key, s, signature);
    }
  
  /**
   * I Know that this is a NEW signature, on a new object
   */
  public MimSignature newSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimKey risul = (MimKey) this.clone();

    // a signature on a new object assumes all this fields are empty
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    risul.last_update=new MimTimestamp(0); 
    risul.fingerprint=new MimFingerprint();
    risul.signature=new MimSignature();
    risul.proof_of_work=new MimPowValue();

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    return crypto.mimSign(privateKey, s);
    }

  public MimSignature updateSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimKey risul = (MimKey) this.clone();

    // a signature on an update object assume just the following are empty
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    return crypto.mimSign(privateKey, s);
    }

  
  /**
   * If a pow calculator is provided then it will be used, otherwise a new one will be created
   * NOTE: A powCalculator is non reentrant, you MUST ensure it is used single thread
   * @param powCalculator
   * @return
   * @throws Exception
   */
  @Override
  public MimPowValue verifyPow () throws Exception
    {
    MimKey risul = (MimKey) this.clone();

    // assume it is this one
    MimPowValue pow = risul.update_proof_of_work;
    
    risul.update_proof_of_work=new MimPowValue();
     
    if ( this.update_proof_of_work.isNull() )
      {
      // since the update POW is empty, then, use the normal one
      pow = risul.proof_of_work;

      risul.fingerprint=new MimFingerprint();
      risul.last_update=new MimTimestamp(0); 
      risul.proof_of_work=new MimPowValue();
      }

    MimPowCalculator powCalculator = new MimPowCalculator();
    
    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);

    pow.setValid(powCalculator.isPowValid(s, pow));
    
    return pow;
    }
  
  /**
   * calculate the POW source to be used on insert
   */
  public String calcInsertPowSource ( ) throws JsonProcessingException, CloneNotSupportedException
    {
    MimKey risul = (MimKey) this.clone();

    risul.fingerprint=new MimFingerprint();
    risul.last_update=new MimTimestamp(0); 
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    risul.proof_of_work=new MimPowValue();

    AeJson om = new AeJson();
    return om.writeValueAsString(risul);
    }

  
  /**
   * calculate the POW source to be used on object update
   */
  public String calcUpdatePowSource ( ) throws JsonProcessingException, CloneNotSupportedException
    {
    // it is fair to zap the current one since it is the only one being replaced
    update_proof_of_work=new MimPowValue();
    
    AeJson om = new AeJson();
    return om.writeValueAsString(this);
    }

  
  }
