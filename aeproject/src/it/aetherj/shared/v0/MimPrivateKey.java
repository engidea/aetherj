/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.*;

import it.aetherj.protocol.MimHex;
import net.i2p.crypto.eddsa.EdDSAPrivateKey;
import net.i2p.crypto.eddsa.spec.*;

/**
 * This is mostly for reference, since MIM does not really transfer the private key anywhere
 * BUT I need a clear way to import a previous private key, so, I need this class
 * Generally speaking MIM transfer the key as bytes of the seed
 */
public class MimPrivateKey
  {
  public static final EdDSAParameterSpec paramSpec=EdDSANamedCurveTable.getByName(EdDSANamedCurveTable.ED_25519);

  public static final int priv_key_bytes_len=32;
  
  private byte[] seed_bytes;
  
  public MimPrivateKey()
    {
    }
  
  @JsonCreator
  public MimPrivateKey(String pkString)
    {
    if ( pkString == null )
      return;
    
    seed_bytes=MimHex.convStringToBin(pkString);
    }
  
  /**
   * From dbase you can use this method, dbase key is stored in seed_bytes way
   * @param seed_bytes
   */
  public MimPrivateKey(byte []seed_bytes)
    {
    this.seed_bytes = seed_bytes;
    }
  
  /**
   * If you have a real PrivateKey use this one
   * @param key
   */
  public MimPrivateKey(PrivateKey key)
    {
    try
      {
      // Need this since the generic PrivateKey does NOT have a getSeed()
      PKCS8EncodedKeySpec encoded = new PKCS8EncodedKeySpec(key.getEncoded());
      EdDSAPrivateKey s_private = new EdDSAPrivateKey(encoded);
      seed_bytes = s_private.getSeed();
      }
    catch ( Exception exc )
      {
      seed_bytes=null;
      }
    }

  /**
   * This returns a key that is usable to do a verification using a public key
   * @return a good public key or null if no original info available
   */
  public PrivateKey getPrivateKey ()
    {
    if ( seed_bytes == null || seed_bytes.length < priv_key_bytes_len)
      return null;

    EdDSAPrivateKeySpec privateSpec = new EdDSAPrivateKeySpec(seed_bytes, paramSpec);
    return new EdDSAPrivateKey(privateSpec);
    }
  
  public boolean isNull()
    {
    return seed_bytes == null;
    }
  
  public boolean equals(Object obj)
    {
    if ( obj instanceof MimPrivateKey )
      {
      MimPrivateKey other = (MimPrivateKey)obj;
      
      return Arrays.equals(seed_bytes, other.seed_bytes);
      }
    else
      return false;
    }
  
  public int hashCode ()
    {
    if ( seed_bytes == null ) 
      return 0;
    
    return seed_bytes.hashCode();
    }
    
  /**
   * May be null
   * @return
   */
  public byte []getSeed_bytes()
    {
    return seed_bytes;
    }
      
  @JsonValue
  public String toString()
    {
    if ( seed_bytes == null )
      return "";
    
    return MimHex.convBinToHexString(seed_bytes);
    }

  }
