/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.aetherj.protocol.MimFingerprint;

/**
 * 

type Filter struct { // Timestamp filter or embeds, or fingerprint
  Type   string   `json:"type"`
  Values []string `json:"values"`
}

 *
 */
public class MimFilterFingerprint extends MimFilter
  {
  @JsonIgnore
  private final ArrayList<MimFingerprint>fingerList = new ArrayList<>();
  
  public MimFilterFingerprint ( MimFilter parent )
    {
    this.type = parent.type;
    this.values=parent.values;   // just to be coherent with inheritance

    int size=values.length;

    fingerList.ensureCapacity(size);
    
    for (int index=0; index<size; index++)
      fingerList.add(new MimFingerprint(values[index]));
    
    }

  
  @JsonIgnore
  public int size()
    {
    return fingerList.size();
    }
  
  @JsonIgnore
  public MimFingerprint get(int index )
    {
    return fingerList.get(index);
    }
  
  public MimFilterFingerprint ( ArrayList<MimFingerprint> fingerList )
    {
    super.type=type_fingerprint;
    
    int size=fingerList.size();
    
    values = new String[size];
    
    for (int index=0; index<size; index++)
      {
      MimFingerprint fpri=fingerList.get(index);
      
      values[index] = fpri.toString(); 
      }
    }
  }
