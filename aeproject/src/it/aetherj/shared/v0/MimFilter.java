/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 

type Filter struct { // Timestamp filter or embeds, or fingerprint
  Type   string   `json:"type"`
  Values []string `json:"values"`
}
 
 *
 */
public class MimFilter
  {
  public static final String type_timestamp="timestamp";
  public static final String type_fingerprint="fingerprint";
  
  public String type;  // eg timestamp
  public String []values;  
  
  @JsonIgnore
  public boolean isNull ()
    {
    return type == null || type.length() < 2;
    }
  
  @JsonIgnore
  public boolean isTimestamp ()
    {
    return type_timestamp.equals(type);
    }
   
  @JsonIgnore
  public boolean isFingerprint ()
    {
    return type_fingerprint.equals(type);
    }
  
  @Override
  public String toString()
    {
    return "MimFilter: "+type;
    }
  }
