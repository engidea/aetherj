/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.*;

import it.aetherj.protocol.MimDigest;

/**
 * A timestamp_s in MIM is sent / received as a long with seconds since epoch
 * A class is needed to wrap behavior
 */
public class MimTimestamp
  {
  private final long timestamp_s;
  
  /**
   * Create a MimTimestamp with current time
   * This has become a behavior contract that is extremely difficult to change, actually, do NOT change it
   */
  public MimTimestamp()
    {
    timestamp_s = System.currentTimeMillis()/1000;
    }
  
  public MimTimestamp(String mimStringValue )
    {
    timestamp_s=fromMimString(mimStringValue);
    }
  
  /**
   * Add this object to part of the things
   * @param digest
   */
  public void digestPart(MimDigest digest)
    {
    digest.digestPart(Long.toString(timestamp_s));
    }

  private long fromMimString (String value )
    {
    try
      {
      return Long.parseLong(value);
      }
    catch ( Exception exc )
      {
      return 0;
      }
    }
  
  @JsonCreator
  public MimTimestamp(Long mimValue)
    {
    if ( mimValue == null )
      timestamp_s=0;
    else
      timestamp_s = mimValue.longValue();
    }
  
  /**
   * When you just want to create a new one
   * NOTE that this one does NOT accept null values
   */
  public MimTimestamp(long time_s)
    {
    timestamp_s=time_s;
    }

  public MimTimestamp(Timestamp from_sql)
    {
    if ( from_sql == null )
      timestamp_s=0;
    else
      timestamp_s = from_sql.getTime()/1000;
    }

  /**
   * A MimTimestamp is considered null if it has a value of 0
   */
  public boolean isNull ()
    {
    return timestamp_s == 0;
    }
  
  /**
   * Return true is this MimTimestamp is after the one given
   * @param other
   * @return
   */
  public boolean isAfter ( MimTimestamp other )
    {
    if ( other == null )
      return true;
    
    return timestamp_s > other.timestamp_s;
    }
  
  /**
   * This is not equal since I really wish to have a MimTimestamp
   * @param other
   * @return
   */
  public boolean isEqual ( MimTimestamp other )
    {
    if ( other == null )
      return false;
    
    return timestamp_s == other.timestamp_s;
    }

  /**
   * If you add a negative number you go back in time
   */
  public MimTimestamp add_seconds ( int seconds )
    {
    return new MimTimestamp(timestamp_s+seconds);
    }

  public MimTimestamp add_hours ( int hours )
    {
    return add_seconds(hours * 60*60);
    }

  public MimTimestamp add_days ( int days )
    {
    return add_hours(days * 24);
    }

  /**
   * This is NOT fully correct since it assumes a month has 30 days, always
   * @param months to add or subtract (if negative)
   * @return a new MimTimestamp
   */
  public MimTimestamp add_months ( int months )
    {
    return add_days(months * 30);
    }

  public Date getDate ()
    {
    return new Date(timestamp_s * 1000);
    }
  
  public Timestamp getTimestamp ()
    {
    return new Timestamp(timestamp_s * 1000);
    }
  
  @JsonValue
  public long getJsonValue()
    {
    return timestamp_s;
    }
  
  @Override
  public String toString()
    {
    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    return f.format(getDate());
    }

  }
