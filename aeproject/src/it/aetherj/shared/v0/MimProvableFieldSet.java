/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.aetherj.protocol.*;

/**
 * 

type ProvableFieldSet struct {
  Fingerprint Fingerprint `json:"fingerprint"`
  Creation    Timestamp   `json:"creation"`
  ProofOfWork ProofOfWork `json:"proof_of_work"`
  Signature   Signature   `json:"signature"`
  Verified    bool        `json:"-"`
}

 *
 */
public abstract class MimProvableFieldSet implements MimEmessagesMethods
  {
  public MimFingerprint fingerprint=new MimFingerprint();  // should be correct to have a non null empty fingerprint
  public MimTimestamp creation=new MimTimestamp(); // it is logically correct that when you create a new instance this is non null
  public MimPowValue proof_of_work;
  public MimSignature signature;
  
  public MimFingerprint peekFingerprint()
    {
    return fingerprint; 
    }
  
  @JsonIgnore
  public boolean isNull ()
    {
    return fingerprint.isNull();
    }
  
  }
