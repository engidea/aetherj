/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

/**
 * What is this for ?

type ResultCache struct { // These are caches shown in the index endpoint of a particular entity.
  ResponseUrl string    `json:"response_url"`
  StartsFrom  Timestamp `json:"starts_from"`
  EndsAt      Timestamp `json:"ends_at"`
}

 */
public class MimResultsCache
  {
  public static final int level_count=1;   // cannot understand if there are any more pages or not
  public static final String cache_prefix="cache_";
  
  public String response_url;
  public MimTimestamp starts_from;
  public MimTimestamp ends_at;

  public MimResultsCache()
    {
    }
  
  public MimResultsCache(Integer primary_id, MimTimestamp starts_from, MimTimestamp ends_at)
    {
    // if you want a longer cache id you can change it here
    this.response_url=String.format("%s%05d", cache_prefix,primary_id);
    this.starts_from=starts_from;
    this.ends_at=ends_at;
    }
  
  
  
  }
