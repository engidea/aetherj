package it.aetherj.shared.v0;

import java.util.ArrayList;

/**
 * Need this since quite a few of items of MIM API are arrays of types
 * I need a simple way to add rows to a list and then convert to array at the right time
 * I may need to decide the behavior when the list is empty, return null or a zero len array ?
 */
public class MimItemsArray<T>
  {
  private final ArrayList<T>items = new ArrayList<>(500);
  private T []r_type;
  
  // Quite often there is the need to keep track of last item time, to generate time ranges...
  public MimTimestamp last_item_time = new MimTimestamp();
  
  /**
   * Since Generics use type erasure it is not possible to generic type the result array
   * There is the need to pass a zero length array to produce the right array type as result
   * @param r_type
   */
  public MimItemsArray ( T [] r_type )
    {
    this.r_type = r_type;
    }
  
  public void add ( T row )
    {
    if ( row == null )
      return;
    
    items.add(row);
    }
  
  public int size()
    {
    return items.size();
    }
  
  public T getFirst ()
    {
    return items.size() > 0 ? items.get(0) : null; 
    }
    
  public T[] toArray ()  
    {
    int listSize = items.size();
    
    if ( listSize < 1 )
      return null;
    
    return items.toArray(r_type);
    }
    
  }
