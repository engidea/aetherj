package it.aetherj.shared.v0;

import java.util.ArrayList;

/**
 * Need this since the filter is an array of filters...
 * Well, actually, no, at least not current Aether. Meaning that the API is an array, the behavior.... probably not
 * The query coming from the remote caused an error in the local database while trying to respond to this request. Error: &errors.errorString{s:"You can either search for a time range, or for fingerprint(s). You can't do both or neither at the same time - you have to do one
 * So, just use one...
 * TODO: Use MimItemsArray instead of this one
 */
public class MimFilterArray
  {
  private ArrayList<MimFilter>filterList = new ArrayList<>(10);
    
  public void add ( MimFilter filter )
    {
    if ( filter == null )
      return;
    
    filterList.add(filter);
    }
  
  public MimFilter[] toArray ()
    {
    int listSize = filterList.size();
    
    if ( listSize < 1 )
      return null;
    
    MimFilter []risul = new MimFilter[listSize];
    filterList.toArray(risul);
    
    return risul;
    }
    
  }
