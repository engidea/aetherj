/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.aetherj.protocol.MimEntity;

/**
 * Methods that apply to the standard Mim entity
 * This is used to "publish" an entity that will match with the DB providers
 */
public interface MimEntityMethods extends Cloneable
  {

  /**
   * The aJson annotation works even on inherited classes
   * This is used to report "what kind of object" it is, even for objects that do not have an entity...
   * NOTE: There is ONE entity variable in MimPayload that MUST get serialized and it will NOT if there is an Ignore on a getter method
   * SO, this method MUST change name to allow for serialization of the variable
   * this is why this is called getMimEntity and not just getEntity
   * @return
   */
  @JsonIgnore
  public MimEntity getMimEntity();
  }
