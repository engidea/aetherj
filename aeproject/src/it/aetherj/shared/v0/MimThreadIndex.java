package it.aetherj.shared.v0;

import it.aetherj.protocol.*;

/*
 TODO The fields are all here, but the ORDERING is wrong, I may need to do the ordering "by hand"
 otherwise the check on content will fail
 

 type ThreadIndex struct {
  Fingerprint   Fingerprint `json:"fingerprint"`
  Owner         Fingerprint `json:",omitempty"`
  Board         Fingerprint `json:"board"`
  Creation      Timestamp   `json:"creation"`
  LastUpdate    Timestamp   `json:"last_update"`
  EntityVersion int         `json:"entity_version"`
  PageNumber    int         `json:"page_number"`
}

 */

public class MimThreadIndex implements MimIndexesMethods
  {
  public MimFingerprint fingerprint;
  public MimFingerprint Owner;
  public MimFingerprint board;
  public MimTimestamp   creation;
  public MimTimestamp   lastupdate;
  public int            entity_version;
  public int            page_number;

  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aethread;
    }
  }
