package it.aetherj.shared.v0;

import it.aetherj.protocol.*;


/*
 
type PostIndex struct {
  Fingerprint   Fingerprint `json:"fingerprint"`
  Owner         Fingerprint `json:",omitempty"`
  Board         Fingerprint `json:"board"`
  Thread        Fingerprint `json:"thread"`
  Parent        Fingerprint `json:",omitempty"`
  Creation      Timestamp   `json:"creation"`
  LastUpdate    Timestamp   `json:"last_update"`
  EntityVersion int         `json:"entity_version"`
  PageNumber    int         `json:"page_number"`
}

 */

public class MimPostIndex implements MimIndexesMethods
  {
  public MimFingerprint fingerprint;
  public MimFingerprint Owner;
  public MimFingerprint board;
  public MimFingerprint thread;
  public MimFingerprint parent;
  public MimTimestamp   creation;
  public MimTimestamp   lastupdate;
  public int            entity_version;
  public int            page_number;
  
  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aepost;
    }
  }
