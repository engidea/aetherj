/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The idea is that you have a method that returns the source for signature...
 * @author damiano
 *
 */
public interface MimGetSignatureSource
  {
  /**
   * I pass a StringBuilder for faster handling
   * @param result
   */
  @JsonIgnore
  public void getPowSource ( StringBuilder result );
  
  /**
   * What if I have a copy for xxxx... that does the right thing ?
   */
  }
