/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import java.io.UnsupportedEncodingException;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;
import it.aetherj.shared.AeJson;

/**
 
 It 
 
 type Board struct { // Mutables: BoardOwners, Description, Meta 
  ProvableFieldSet
  Name           string       `json:"name"`         // Max 255 char unicode
  BoardOwners    []BoardOwner `json:"board_owners"` // max 128 owners
  Description    string       `json:"description"`  // Max 65535 char unicode
  Owner          Fingerprint  `json:"owner"`
  OwnerPublicKey string       `json:"owner_publickey"`
  EntityVersion  int          `json:"entity_version"`
  Language       string       `json:"language"`
  Meta           string       `json:"meta"` // This is the dynamic JSON field
  RealmId        Fingerprint  `json:"realm_id"`
  EncrContent    string       `json:"encrcontent"`
  UpdateableFieldSet
}

type UpdateableFieldSet struct { // Common set of properties for all objects that are updateable.
  LastUpdate        Timestamp   `json:"last_update"`
  UpdateProofOfWork ProofOfWork `json:"update_proof_of_work"`
  UpdateSignature   Signature   `json:"update_signature"`
}

I could extend a class to include all updateableFieldSet, BUT the ordering is wrong, they are at the end, not at the beginning...
 
 
 *
 */
public class MimBoard extends MimProvableFieldSet 
  {
  public String name="";
  public MimBoardOwner []board_owners = new MimBoardOwner[0];
  public String description="";
  public MimFingerprint owner;
  public MimPublicKey owner_publickey;

  public int    entity_version=1;
  public MimLang language=new MimLang();
  public String  meta="";
  public MimFingerprint realm_id=new MimFingerprint();
  public String encrcontent="";
  
  // the following will be in more than one class...
  public MimTimestamp last_update;
  public MimPowValue  update_proof_of_work;
  public MimSignature update_signature;

  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aeboard;
    }
  
  @Override
  public MimTimestamp peekLastUpdate()
    {
    return last_update.isNull() ? creation : last_update;
    }

  @Override
  public MimBoardIndex peekMimIndex ()
    {
    MimBoardIndex idx = new MimBoardIndex();
    idx.fingerprint = fingerprint;
    idx.Owner = owner;
    idx.creation = creation;
    idx.lastupdate = last_update;
    idx.entity_version = entity_version;
    
    return idx;
    }

  /**
   * See cvset-v1.go line 686
   * @throws UnsupportedEncodingException 
   */
  public boolean verifyFingerprint( ) throws Exception
    {
    MimFingerprint n_fprint = calcFingerprint();
    return n_fprint.equals(fingerprint);
    
    /*
    MimBoard risul = (MimBoard) this.clone();
    
    risul.last_update=new MimTimestamp(0);
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    // this is SOOOOO idiotic, Json has null value but somewhere in Aether.go, when doing the test for the signature
    // the value is reset to an empty array (that is different than null)
    risul.board_owners=new MimBoardOwner[0];

    risul.description="";
    risul.meta="";
    
    risul.fingerprint=new MimFingerprint();
    
    AeJson om = new AeJson();
    String s=om.writeValueAsString(risul);
    return fingerprint.verify(s);
    */
    }

  /**
   * Calculate the input fingerprint, it needs to zap some fields to do the calculation
   */
  public MimFingerprint calcFingerprint() throws CloneNotSupportedException, JsonProcessingException 
    {
    MimBoard risul = (MimBoard) this.clone();
    
    risul.last_update=new MimTimestamp(0);
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    // this is SOOOOO idiotic, Json has null value but somewhere in Aether.go, when doing the test for the signature
    // the value is reset to an empty array (that is different than null)
    risul.board_owners=new MimBoardOwner[0];

    risul.description="";
    risul.meta="";
    
    risul.fingerprint=new MimFingerprint();
    
    AeJson om = new AeJson();
    String s=om.writeValueAsString(risul);
    return MimFingerprint.newFingerprint(s);
    }
  
  
  /**
   * I Know that this is a NEW signature, on a new object
   */
  public MimSignature newSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimBoard risul = (MimBoard) this.clone();

    // a signature on a new object assumes all this fields are empty
    risul.fingerprint=new MimFingerprint();
    risul.signature=new MimSignature();
    risul.proof_of_work=new MimPowValue();

    // this is SOOOOO idiotic, Json has null value but somewhere in Aether.go, when doing the test for the signature
    // the value is reset to an empty array (that is different than null)
    risul.board_owners = new MimBoardOwner[0];

    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    risul.last_update=new MimTimestamp(0); 

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    return crypto.mimSign(privateKey, s);
    }

  
  /**
   * To be tested 10 april 2021
   * @param crypto
   * @param privateKey
   * @return
   * @throws CloneNotSupportedException
   * @throws JsonProcessingException
   */
  public MimSignature updateSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimBoard risul = (MimBoard) this.clone();

    // a signature on an update object assume just the following are empty
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();

    // this is SOOOOO idiotic, Json has null value but somewhere in Aether.go, when doing the test for the signature
    // the value is reset to an empty array (that is different than null)
    risul.board_owners = new MimBoardOwner[0];

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    return crypto.mimSign(privateKey, s);
    }

  
  /**
   * See cvset-v1.go line 1170
   */
  @Override
  public boolean verifySignature(CryptoEddsa25519 crypto) throws Exception
    {
    MimBoard risul = (MimBoard) this.clone();

    // assume it is this one
    MimSignature signature = risul.update_signature;
    
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    // this is SOOOOO idiotic, Json has null value but somewhere in Aether.go, when doing the test for the signature
    // the value is reset to an empty array (that is different than null)
    risul.board_owners = new MimBoardOwner[0];

    if ( this.update_signature.isEmpty() )
      {
      // since the update signature is empty, then, use the normal one
      signature = risul.signature;

      risul.last_update=new MimTimestamp(0); 
      risul.fingerprint=new MimFingerprint();
      risul.signature=new MimSignature();
      risul.proof_of_work=new MimPowValue();
      }

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    
    return crypto.mimVerify(this.owner_publickey, s, signature);
    }
  
  /**
   * calculate the POW source to be used on insert
   */
  public String calcInsertPowSource ( ) throws JsonProcessingException, CloneNotSupportedException
    {
    MimBoard risul = (MimBoard) this.clone();

    risul.fingerprint=new MimFingerprint();
    risul.last_update=new MimTimestamp(0); 
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    risul.proof_of_work=new MimPowValue();

    AeJson om = new AeJson();
    return om.writeValueAsString(risul);
    }

  /**
   * calculate the POW source to be used on object update
   */
  public String calcUpdatePowSource ( ) throws JsonProcessingException, CloneNotSupportedException
    {
    // it is fair to zap the current one since it is the only one being replaced
    update_proof_of_work=new MimPowValue();
    
    AeJson om = new AeJson();
    return om.writeValueAsString(this);
    }

  /**
   * If a pow calculator is provided then it will be used, otherwise a new one will be created
   * NOTE: A powCalculator is non reentrant, you MUST ensure it is used single thread
   * If Aether was done in a minimal OO way a LOT of code could have gone into a superclass
   * @param powCalculator
   * @return
   * @throws Exception
   */
  @Override
  public MimPowValue verifyPow () throws Exception
    {
    MimBoard risul = (MimBoard) this.clone();

    // assume it is this one
    MimPowValue pow = risul.update_proof_of_work;
    
    risul.update_proof_of_work=new MimPowValue();
    
    if ( this.update_proof_of_work.isNull() )
      {
      // since the update POW is empty, then, use the normal one
      pow = risul.proof_of_work;

      risul.fingerprint=new MimFingerprint();
      risul.last_update=new MimTimestamp(0); 
      risul.proof_of_work=new MimPowValue();
      }

    MimPowCalculator powCalculator = new MimPowCalculator();
    
    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);

    pow.setValid(powCalculator.isPowValid(s, pow));
    
    return pow;
    }
  }
