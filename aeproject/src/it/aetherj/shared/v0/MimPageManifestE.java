package it.aetherj.shared.v0;

import it.aetherj.protocol.MimFingerprint;

/*

// Manifest type
type PageManifest struct {
  Page     uint64               `json:"page_number"`
  Entities []PageManifestEntity `json:"entities"`
}

type PageManifestEntity struct {
  Fingerprint Fingerprint `json:"fingerprint"`
  LastUpdate  Timestamp   `json:"last_update"`

 */

public class MimPageManifestE
  {
  public MimFingerprint fingerprint;
  public MimTimestamp last_update;
  
  public MimPageManifestE()
    {
    }
  
  public MimPageManifestE(MimFingerprint fp, MimTimestamp lu )
    {
    fingerprint=fp;
    last_update=lu;
    }
  }
