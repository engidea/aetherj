/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import it.aetherj.protocol.MimFingerprint;

/**
 * A Board may have some owners and so, there is a list of ukey fingerprints that are bound to a board
 * In dbase I have to say that a row has to be bound to a board and a ukey
 * The handling in dbase is kind of custom, since this is a detail table of the Board table
 
type BoardOwner struct {
  KeyFingerprint Fingerprint `json:"key_fingerprint"` // Fingerprint of the key the ownership is associated to.
  Expiry         Timestamp   `json:"expiry"`          // When the ownership expires.
  Level          uint8       `json:"level"`           // mod(1)
}

 */
public class MimBoardOwner
  {
  public MimFingerprint key_fingerprint;
  public MimTimestamp   expiry;
  public int            level=1;
  }
