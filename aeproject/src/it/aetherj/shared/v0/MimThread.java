/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;
import it.aetherj.shared.AeJson;

/**
 IMPORTANT: You CANNOT change ANY of the fields name OR order since fingerprinting DEPENDS on names and order
 
 type Thread struct { // Mutables: Body, Meta
  ProvableFieldSet
  Board          Fingerprint `json:"board"`
  Name           string      `json:"name"`
  Body           string      `json:"body"`
  Link           string      `json:"link"`
  Owner          Fingerprint `json:"owner"`
  OwnerPublicKey string      `json:"owner_publickey"`
  EntityVersion  int         `json:"entity_version"`
  Meta           string      `json:"meta"`
  RealmId        Fingerprint `json:"realm_id"`
  EncrContent    string      `json:"encrcontent"`
  UpdateableFieldSet
}

type UpdateableFieldSet struct { // Common set of properties for all objects that are updateable.
  LastUpdate        Timestamp   `json:"last_update"`
  UpdateProofOfWork ProofOfWork `json:"update_proof_of_work"`
  UpdateSignature   Signature   `json:"update_signature"`
}

I could extend a class to include all updateableFieldSet, BUT the ordering is wrong, they are ath the end, not at the beginning...

select * from aethread_view where aethread_fingerprint=hextoraw('7c27133008aceb4922fa7a2f36c98ca23910d24378377107f7349a6723785e6e')
select rawtohex(aeboard_fingerprint) from aethread_view where aethread_fingerprint=hextoraw('7c27133008aceb4922fa7a2f36c98ca23910d24378377107f7349a6723785e6e')
select aethread_name from aethread_view where aethread_fingerprint=hextoraw('7c27133008aceb4922fa7a2f36c98ca23910d24378377107f7349a6723785e6e')
select rawtohex(ukey_fingerprint) from aethread_view where aethread_fingerprint=hextoraw('7c27133008aceb4922fa7a2f36c98ca23910d24378377107f7349a6723785e6e')
select aethread_creation from aethread_view where aethread_fingerprint=hextoraw('7c27133008aceb4922fa7a2f36c98ca23910d24378377107f7349a6723785e6e')
select aethread_pow from aethread_view where aethread_fingerprint=hextoraw('7c27133008aceb4922fa7a2f36c98ca23910d24378377107f7349a6723785e6e')

 *
 */
public class MimThread extends MimProvableFieldSet 
  {
  public static final int name_len_max=255;
  public static final int link_len_max=5000;
  public static final int body_len_max=100000;
  public static final int def_entity_v=1;
  
  public MimFingerprint board;      
  public String name="";
  public String body="";
  public String link="";
  public MimFingerprint owner;          
  public MimPublicKey owner_publickey;
  public int    entity_version=def_entity_v;
  public String meta="";
  public String realm_id="";        // Fingerprint
  public String encrcontent="";
   
  // the following will be in more than one class...
  public MimTimestamp last_update;
  public MimPowValue  update_proof_of_work;
  public MimSignature update_signature;

  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aethread;
    }
  
  @Override
  public MimTimestamp peekLastUpdate()
    {
    return last_update.isNull() ? creation : last_update;
    }

  @Override
  public MimThreadIndex peekMimIndex ()
    {
    MimThreadIndex idx = new MimThreadIndex();
    idx.fingerprint = fingerprint;
    idx.Owner = owner;
    idx.board = board;
    idx.creation = creation;
    idx.creation = creation;
    idx.lastupdate = last_update;
    idx.entity_version = entity_version;
    
    return idx;
    }

  /**
   * See cvset-v1.go line 705
   * the fingerprint is the same if the new fingerprint is the same as the one we have
   */
  @Override
  public boolean verifyFingerprint( ) throws Exception
    {
    MimFingerprint n_fprint = calcFingerprint();
    return n_fprint.equals(fingerprint);
    
    /*
    MimThread risul = (MimThread) this.clone();
    
    risul.last_update=new MimTimestamp(0);
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    risul.body="";
    risul.meta="";
    
    risul.fingerprint=new MimFingerprint();
    
    AeJson om = new AeJson();
    String s=om.writeValueAsString(risul);
    return fingerprint.verify(s);
    */
    }
  
  /**
   * Calculate the input fingerprint, it needs to zap some fields to do the calculation
   */
  public MimFingerprint calcFingerprint() throws CloneNotSupportedException, JsonProcessingException 
    {
    MimThread risul = (MimThread) this.clone();

    // Signature is used on the fingerprint, so, it has to be done earlier
    risul.fingerprint=new MimFingerprint();

    risul.body="";
    risul.meta="";

    risul.last_update=new MimTimestamp(0);
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    AeJson om = new AeJson();
    String s=om.writeValueAsString(risul);
    return MimFingerprint.newFingerprint(s);
    }

  @Override
  public boolean verifySignature(CryptoEddsa25519 crypto) throws Exception
    {
    MimThread risul = (MimThread) this.clone();

    // assume it is this one
    MimSignature signature = risul.update_signature;
    
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    if ( this.update_signature.isEmpty() )
      {
      // since the update signature is empty, then, use the normal one
      signature = risul.signature;

      risul.last_update=new MimTimestamp(0); 
      risul.fingerprint=new MimFingerprint();
      risul.signature=new MimSignature();
      risul.proof_of_work=new MimPowValue();
      }

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    return crypto.mimVerify(this.owner_publickey, s, signature);
    }

  /**
   * I Know that this is a NEW signature, on a new object
   */
  public MimSignature newSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimThread risul = (MimThread) this.clone();

    // a signature on a new object assumes all this fields are empty
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    risul.last_update=new MimTimestamp(0); 
    risul.fingerprint=new MimFingerprint();
    risul.signature=new MimSignature();
    risul.proof_of_work=new MimPowValue();

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    return crypto.mimSign(privateKey, s);
    }
  
  public MimSignature updateSignature (CryptoEddsa25519 crypto, MimPrivateKey privateKey ) throws CloneNotSupportedException, JsonProcessingException
    {
    MimThread risul = (MimThread) this.clone();

    // a signature on an update object assume just the following are empty
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    return crypto.mimSign(privateKey, s);
    }

  /**
   * If a pow calculator is provided then it will be used, otherwise a new one will be created
   * NOTE: A powCalculator is non reentrant, you MUST ensure it is used single thread
   * If Aether was done in a minimal OO way a LOT of code could have gone into a superclass
   * see api/cvset-v1.go line 333
   * @param powCalculator
   * @return
   * @throws Exception
   */
  @Override
  public MimPowValue verifyPow () throws Exception
    {
    MimThread risul = (MimThread) this.clone();

    // assume it is this one
    MimPowValue pow = risul.update_proof_of_work;
    
    risul.update_proof_of_work=new MimPowValue();
    
    if ( this.update_proof_of_work.isNull() )
      {
      // since the update POW is empty, then, use the normal one
      pow = risul.proof_of_work;

      risul.fingerprint=new MimFingerprint();
      risul.last_update=new MimTimestamp(0); 
      risul.proof_of_work=new MimPowValue();
      }

    MimPowCalculator powCalculator = new MimPowCalculator();
    
    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);

    pow.setValid(powCalculator.isPowValid(s, pow));
    
    return pow;
    }

  /**
   * OBSOLETE, just for reference
   * You calcUpdatePowSource, have the web compute the POW and THEN sign it and insert
  private MimPowValue newPowSigned ( MimPowParams params, CryptoEddsa25519 crypto, MimPrivateKey privateKey) throws JsonProcessingException, CloneNotSupportedException
    {
    MimThread risul = (MimThread) this.clone();

    risul.fingerprint=new MimFingerprint();
    risul.last_update=new MimTimestamp(0); 
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    risul.proof_of_work=new MimPowValue();
    risul.proof_of_work=new MimPowValue();

    MimPowCalculator powc = new MimPowCalculator();
    
    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);

    MimPowValue pow = powc.newPowUnsigned(params, s);

    MimSignature signature = crypto.mimSign(privateKey, pow.toString());
    
    return pow.getSigned(signature);
    }
   */
  
  /**
   * calculate the POW source to be used on insert
   */
  public String calcInsertPowSource ( ) throws JsonProcessingException, CloneNotSupportedException
    {
    MimThread risul = (MimThread) this.clone();

    risul.fingerprint=new MimFingerprint();
    risul.last_update=new MimTimestamp(0); 
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    risul.proof_of_work=new MimPowValue();

    AeJson om = new AeJson();
    return om.writeValueAsString(risul);
    }

  /**
   * calculate the POW source to be used on object update
   */
  public String calcUpdatePowSource ( ) throws JsonProcessingException, CloneNotSupportedException
    {
    // it is fair to zap the current one since it is the only one being replaced
    update_proof_of_work=new MimPowValue();
    
    AeJson om = new AeJson();
    return om.writeValueAsString(this);
    }
  
  /**
   * OBSOLETE, just for reference
   * You calcUpdatePowSource, have the web compute the POW and THEN sign it and insert
  private MimPowValue updatePowSigned ( MimPowParams params, CryptoEddsa25519 crypto, MimPrivateKey privateKey) throws JsonProcessingException, CloneNotSupportedException
    {
    MimThread risul = (MimThread) this.clone();

    risul.update_proof_of_work=new MimPowValue();

    MimPowCalculator powc = new MimPowCalculator();
    
    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);

    MimPowValue pow = powc.newPowUnsigned(params, s);

    MimSignature signature = crypto.mimSign(privateKey, pow.toString());
    
    return pow.getSigned(signature);
    }
   */


  }
