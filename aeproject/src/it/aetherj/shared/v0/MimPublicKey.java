/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.*;

import it.aetherj.protocol.*;
import net.i2p.crypto.eddsa.EdDSAPublicKey;
import net.i2p.crypto.eddsa.spec.*;

/**
 * Generally speaking a public key is transferred as a string of hex digits
 * I should move here methods to "transform" a key from bin to hex
 */
public class MimPublicKey
  {
  public static final EdDSAParameterSpec paramSpec=EdDSANamedCurveTable.getByName(EdDSANamedCurveTable.ED_25519);

  public static final int pub_key_bytes_len=32;
  
  private byte[] pub_A_bytes;
  
  public MimPublicKey()
    {
    }
  
  @JsonCreator
  public MimPublicKey(String pkString)
    {
    if ( pkString == null )
      return;
    
    pub_A_bytes=MimHex.convStringToBin(pkString);
    }
  
  /**
   * From dbase you can use this method, dbase public key is stored in pub_A way
   * @param pub_A_bytes
   */
  public MimPublicKey(byte []pub_A_bytes)
    {
    this.pub_A_bytes = pub_A_bytes;
    }
  
  /**
   * If you have a real PublicKey use this one
   * @param publicKey
   */
  public MimPublicKey(PublicKey publicKey)
    {
    try
      {
      // Need this since the generic PublicKey does NOT have a getA()
      X509EncodedKeySpec encoded = new X509EncodedKeySpec(publicKey.getEncoded());
      EdDSAPublicKey s_public = new EdDSAPublicKey(encoded);
      pub_A_bytes = s_public.getA().toByteArray();
      }
    catch ( Exception exc )
      {
      }
    }

  /**
   * Add this object to part of the things
   * @param digest
   */
  public void digestPart(MimDigest digest)
    {
    digest.digestPart(pub_A_bytes);
    }
  
  /**
   * This returns a key that is usable to do a verification using a public key
   * @return a good public key or null if no original info available
   */
  public PublicKey getPublicKey ()
    {
    if ( pub_A_bytes == null || pub_A_bytes.length < pub_key_bytes_len)
      return null;

    EdDSAPublicKeySpec publicSpec = new EdDSAPublicKeySpec(pub_A_bytes, paramSpec);
    return new EdDSAPublicKey(publicSpec);
    }
  
  public boolean isNull()
    {
    return pub_A_bytes == null;
    }
  
  public boolean equals(Object obj)
    {
    if ( obj instanceof MimPublicKey )
      {
      MimPublicKey other = (MimPublicKey)obj;
      
      return Arrays.equals(pub_A_bytes, other.pub_A_bytes);
      }
    else
      return false;
    }
  
  public int hashCode ()
    {
    if ( pub_A_bytes == null ) 
      return 0;
    
    return pub_A_bytes.hashCode();
    }
    
  /**
   * May be null
   * @return
   */
  public byte []getPub_A_bytes()
    {
    return pub_A_bytes;
    }
    
  /**
   * This is used by mim to create a fingerprint of the public key
   * @return a new fingerprint of this public key
   */
  public MimFingerprint getFingerprint()
    {
    return MimFingerprint.newFingerprint(toString());
    }
  
  @JsonValue
  public String toString()
    {
    if ( pub_A_bytes == null )
      return "";
    
    return MimHex.convBinToHexString(pub_A_bytes);
    }

  }
