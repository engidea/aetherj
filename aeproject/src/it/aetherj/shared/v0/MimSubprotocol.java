/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

public class MimSubprotocol implements MimGetSignatureSource
  {
  public static final String subprotocol_c0="c0";
  
  public String name=subprotocol_c0;
  public int    version_major=1;
  public int    version_minor;
  public String [] supported_entities = {"board","thread","post","vote","key","truststate"};
  
  public MimSubprotocol()
    {
    }

  @Override
  public void getPowSource(StringBuilder result)
    {
    result.append(name);
    result.append(version_major);

    for (int index=0; index<supported_entities.length; index++ )
      result.append(supported_entities[index]);
      
    }
  }
