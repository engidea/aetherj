/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.protocol.*;
import it.aetherj.shared.AeJson;

/**
 * Not really used, as far as I can tell
  
type Truststate struct { // Mutables: Type, Expiry, Meta (Domain is immutable)
  ProvableFieldSet
  Target         Fingerprint `json:"target"`
  Owner          Fingerprint `json:"owner"`
  OwnerPublicKey string      `json:"owner_publickey"`
  TypeClass      int         `json:"typeclass"`
  Type           int         `json:"type"`
  Domain         Fingerprint `json:"domain"`
  Expiry         Timestamp   `json:"expiry"`
  EntityVersion  int         `json:"entity_version"`
  Meta           string      `json:"meta"`
  RealmId        Fingerprint `json:"realm_id"`
  EncrContent    string      `json:"encrcontent"`
  UpdateableFieldSet

type UpdateableFieldSet struct { // Common set of properties for all objects that are updateable.
  LastUpdate        Timestamp   `json:"last_update"`
  UpdateProofOfWork ProofOfWork `json:"update_proof_of_work"`
  UpdateSignature   Signature   `json:"update_signature"`
}

I could extend a class to include all updateableFieldSet, BUT the ordering is wrong, they are at the end, not at the beginning...
Also, what is a mutable field is just a comment in golang... the way it should heve been is two classes, one for constant and one for mutable fields.
 
 */
public class MimTruststate extends MimProvableFieldSet 
  {
  public static final int def_typeclass=1;  // maybe
  public static final int def_type=1;  // maybe
  public static final int def_entity_v=1;  // maybe
  
  public MimFingerprint target; 
  public MimFingerprint owner;
  public MimPublicKey   owner_publickey;
  public int            typeclass;
  public int            type;
  public MimFingerprint domain;
  public MimTimestamp   expiry;      // normally 0
  public int            entity_version;  // normally 1
  public String         meta="";         // empty
  public MimFingerprint realm_id;  // empty   
  public String         encrcontent="";  // empty
   
  // the following will be in more than one class...
  public MimTimestamp last_update;
  public MimPowValue  update_proof_of_work;
  public MimSignature update_signature;

  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aekey;
    }
  
  @Override
  public MimTimestamp peekLastUpdate()
    {
    return last_update.isNull() ? creation : last_update;
    }

  @Override
  public MimTruststateIndex peekMimIndex ()
    {
    MimTruststateIndex idx = new MimTruststateIndex();
    idx.fingerprint = fingerprint;
    idx.Owner = owner;
    idx.target = target;
    idx.creation = creation;
    idx.creation = creation;
    idx.lastupdate = last_update;
    idx.entity_version = entity_version;
    
    return idx;
    }

  /**
   * Returns the string version of the object ready for FIngerprint
   * See cvset-v1.go line 759
   */
  @Override
  public boolean verifyFingerprint( ) throws Exception
    {
    MimTruststate risul = (MimTruststate) this.clone();
    
    risul.last_update=new MimTimestamp(0);
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    risul.type=0;
    risul.expiry=new MimTimestamp(0);
    risul.meta="";
    
    risul.fingerprint=new MimFingerprint();
    
    AeJson om = new AeJson();
    String s=om.writeValueAsString(risul);
    return fingerprint.verify(s);
    }
  
  @Override
  public boolean verifySignature(CryptoEddsa25519 crypto) throws Exception
    {
    MimTruststate risul = (MimTruststate) this.clone();

    // assume it is this one
    MimSignature signature = risul.update_signature;
    
    risul.last_update=new MimTimestamp(0); 
    risul.update_proof_of_work=new MimPowValue();
    risul.update_signature=new MimSignature();
    
    if ( this.update_signature.isEmpty() )
      {
      // since the update signature is empty, then, use the normal one
      signature = risul.signature;

      risul.fingerprint=new MimFingerprint();
      risul.signature=new MimSignature();
      risul.proof_of_work=new MimPowValue();
      }

    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);
    return crypto.mimVerify(this.owner_publickey, s, signature);

    }
  
  /**
   * If a pow calculator is provided then it will be used, otherwise a new one will be created
   * NOTE: A powCalculator is non reentrant, you MUST ensure it is used single thread
   * If Aether was done in a minimal OO way a LOT of code could have gone into a superclass
   * @param powCalculator
   * @return
   * @throws Exception
   */
  @Override
  public MimPowValue verifyPow () throws Exception
    {
    MimTruststate risul = (MimTruststate) this.clone();

    // assume it is this one
    MimPowValue pow = risul.update_proof_of_work;
    
    risul.update_proof_of_work=new MimPowValue();
    
    if ( this.update_proof_of_work.isNull() )
      {
      // since the update POW is empty, then, use the normal one
      pow = risul.proof_of_work;

      risul.fingerprint=new MimFingerprint();
      risul.last_update=new MimTimestamp(0); 
      risul.signature=new MimSignature();
      risul.proof_of_work=new MimPowValue();
      }

    MimPowCalculator powCalculator = new MimPowCalculator();
    
    AeJson om = new AeJson();
    String s = om.writeValueAsString(risul);

    pow.setValid(powCalculator.isPowValid(s, pow));
    
    return pow;
    }

  
  }
