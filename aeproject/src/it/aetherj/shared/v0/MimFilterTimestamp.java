/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 

type Filter struct { // Timestamp filter or embeds, or fingerprint
  Type   string   `json:"type"`
  Values []string `json:"values"`
}

 *
 */
public class MimFilterTimestamp extends MimFilter
  {
  @JsonIgnore
  private MimTimestamp start;

  @JsonIgnore
  private MimTimestamp end;

  public MimFilterTimestamp( MimFilter parent )
    {
    this.type = parent.type;
    this.values=parent.values;   // just to be coherent with inheritance
    
    try
      {
      start = new MimTimestamp(parent.values[0]);
      }
    catch ( Exception exc )
      {
      start = new MimTimestamp(0);
      }

    try
      {
      end = new MimTimestamp(parent.values[1]);
      }
    catch ( Exception exc ) 
      {
      end = new MimTimestamp(0);
      }
    }

  @JsonIgnore
  public MimTimestamp getStart ()
    {
    return start;
    }
  
  @JsonIgnore
  public MimTimestamp getEnd ()
    {
    return end;
    }

  public MimFilterTimestamp ( MimTimestamp start, MimTimestamp end )
    {
    super.type=type_timestamp;
    super.values=new String[2];
    
    values[0]=start.toString();
    values[1]=end.toString();
    }
  
  /**
   * From the end add the given number of days
   * @param add_days if negative, go back in time
   * @param end from where to add
   */
  public MimFilterTimestamp (int add_days, MimTimestamp end)
    {
    this(end.add_days(add_days),end);
    }

  /**
   * Add a time range with end time as NOW and span the given days
   * @param add_days if negative, go back in time
   */
  public MimFilterTimestamp ( int add_days )
    {
    this(add_days, new MimTimestamp());
    }

  
  
  }
