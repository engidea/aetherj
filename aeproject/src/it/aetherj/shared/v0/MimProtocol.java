/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

public class MimProtocol implements MimGetSignatureSource
  {
  public static final int vmajor_old=1;
  
  public int version_major=vmajor_old;
  public int version_minor;
  public MimSubprotocol [] subprotocols ;
  
  public MimProtocol()
    {
    subprotocols=newSubs();
    }

  public void validate()
    {
    if ( version_major==0)
      version_major=vmajor_old;
    
    if ( subprotocols == null || subprotocols.length < 1 )
      subprotocols=newSubs();
    
    }

  private MimSubprotocol [] newSubs()
    {
    MimSubprotocol [] risul=new MimSubprotocol[1];
    
    risul[0] = new MimSubprotocol();
    
    return risul;
    }
  
  @Override
  public void getPowSource(StringBuilder result)
    {
    result.append(version_major);
    subprotocols[0].getPowSource(result);
    }
  }
