/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared;

public final class Aedbg 
  {
  public static final int L_debug              = 0x00000001;      // 
  public static final int L_trace              = 0x00000002;      // 
  public static final int L_notice             = 0x00000004;      //  
  public static final int L_warning            = 0x00000008;      // 
  public static final int L_error              = 0x00000010;      // 
  public static final int L_fatal              = 0x00000020;
  public static final int L_panic              = 0x00000040;
  public static final int L_HTTP_rx            = 0x00000080;      // specific level for HTTP rx since I wish to filter by entity
  public static final int L_HTTP_tx            = 0x00000100;
  
  
  public static final int free_1               = 0x00000001;      // 
  public static final int M_dbase              = 0x00000002;      // 
  public static final int free_2               = 0x00000004;      //  
  public static final int M_HTTP_tx            = 0x00000008;      // 
  public static final int M_HTTP_rx            = 0x00000010;      // 
  public static final int M_scheduler          = 0x00000020;
  public static final int M_compare_rx         = 0x00000040;  
  public static final int M_res_filter         = 0x00000080;
  
  public static final int entity_Node          = 0x00000140;
  public static final int entity_Addresses     = 0x00000280;
  public static final int entity_Ukey          = 0x00000400;
  public static final int entity_Board         = 0x00000800;
  public static final int entity_Thread        = 0x00001000;
  public static final int entity_Post          = 0x00002000;
  public static final int entity_Vote          = 0x00004000;
  public static final int entity_Truststate    = 0x00008000;

  public static final int entity_ANY           = 0x0000FF00;

  public static final int M_Gwt_Connection     = 0x00010000;
  public static final int M_POW_calculation    = 0x00020000;
  
  
  private int w_mask, w_level;
  
  public void wishMaskLevel ( int mask, int level )
    {
    w_mask = mask;
    w_level = level;
    }
    
  public boolean shouldPrint(int h_mask, int h_level )
    {
    return ((w_mask & h_mask) != 0) && ((w_level & h_level) != 0);
    }
  }
