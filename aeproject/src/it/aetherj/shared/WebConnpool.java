/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared;

import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.boot.dbase.Connpool;

public class WebConnpool extends Connpool
  {
  private final WebStat stat;
  
  public WebConnpool(WebStat stat)
    {
    super(stat);
 
    this.stat = stat;
    
    setReallyCloseConnection(true);
    }
  
  public AeDbase newConnectedDbase ()
    {
    AeDbase dbase = new AeDbase(stat);
    dbase.connect(stat.mainDbaseProperty); 
    return dbase;      
    }
  
  }

