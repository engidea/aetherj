/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.bimwapp.servlets;

import java.io.IOException;

import javax.servlet.http.*;

import it.aetherj.backend.mimclient.McwAddress;
import it.aetherj.backend.mimclient.ssl.McwJobStats;
import it.aetherj.bimwapp.BimServiceEnv;
import it.aetherj.protocol.*;
import it.aetherj.shared.AettpStatus;
import it.aetherj.shared.bim.BimPayload; 

/**
 * It should accept a Chat request and reply in the proper manner
 */
public final class ChatServlet extends BimDefaultServlet
  {
  private static final long serialVersionUID = 1L;
  private static final String classname="ChatServlet";

  private static final MimEntity entity = MimEntityList.aechat;

  @Override
  protected MimEntity entity()
    {
    return  entity;
    } 
   
  /**
   * Do the actual work, here I know that the session is valid.
   */
  protected void doPostWork (BimServiceEnv env, HttpServletRequest req, HttpServletResponse resp ) throws IOException 
    {
    env.println(classname+".doPostWork() POST from "+req.getRemoteAddr());
    
    // need to pick up the information that is being sent
    BimPayload mimreq = getBimPayload(env, req, resp);
    
    if ( mimreq == null )
      return;
    
    // make sure host is correct, as from network information
    mimreq.address.url_host = env.getRemoteAddress();
    
    // assume it is good, save the given address as a peer
    McwAddress address = env.mcwAddressSave(mimreq.address);

    // ================ response
    BimPayload mimres = env.newPayload(entity);
    
    // TODO: should add content here !!
    
    env.writeResponse(resp, mimres);

    address.setSyncStats(new McwJobStats(AettpStatus.OK, getStartTime()));
    }
  }
