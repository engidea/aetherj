package it.aetherj.bimwapp.servlets;

import java.io.*;
import java.sql.SQLException;

import javax.servlet.http.*;

import it.aetherj.bimwapp.BimServiceEnv;
import it.aetherj.protocol.*;
import it.aetherj.shared.AettpStatus;
import it.aetherj.shared.bim.BimPayload;

public class BimDefaultServlet extends HttpServlet
  {
  private static final long serialVersionUID = 1L;
  private static final String classname="BimDefaultServlet";
  
  public static final String index_json = "index.json";
  
  public static MimEntity default_entity=MimEntityList.bimwwwroot;

  private long srvlet_start_time;  // set when either GET or POST is called
  
  /**
   * Subclasses define a method that returns the entity we are working with
   * With this I should be able to manage the GET cache in a reasonable way
   * @return
   */
  protected MimEntity entity()
    {
    return default_entity;
    }
  
  /**
   * If subclasses wish to set a specific mask they must override this
   * @return
   */
  protected int getDebugMask()
    {
    return 0;
    }
  
  /**
   * Subclasses provides me with the path base, that is the part that "constant"
   * @return
   */
  protected final String pbase()
    {
    MimEntity e = entity();
    return e.getMethod()+"/";
    }
  
  protected void resetStartTime ()
    {
    srvlet_start_time=System.currentTimeMillis();
    }
  
  protected long getStartTime ()
    {
    return srvlet_start_time;
    }
  
  
  
  protected void doGetWork (BimServiceEnv env, HttpServletRequest req, HttpServletResponse resp ) throws IOException, SQLException 
    {
    final String uri = req.getRequestURI();

    if ( default_entity.equals(entity()) )
      {
      String msg= classname+".doGetWork: UNMAPPED mimwwwroot wanted "+uri;
      writeErrorResponse(env, req, resp, AettpStatus.res_NOTFound, msg);
      return;
      }
    
    String sub = uri.substring(pbase().length());
    
    String [] split = sub.split("/");

    String method = split[0]; 
    
    env.println(classname+".doGetWork: entity="+entity()+" sub="+sub+" method="+method);
     
    String msg= "UNMAPPED entity="+entity()+" wanted "+uri;
    writeErrorResponse(env, req, resp, AettpStatus.res_NOTFound, msg);
    }

  public final void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException 
    {
    BimServiceEnv env = new BimServiceEnv(this,req).setIdent(classname+".doGet");

    try
      { 
      resetStartTime();
      doGetWork(env,req,resp);
      }
    catch ( Exception exc )
      {
      // I do not know why but apparently there is an exception every request ...
      env.println(classname+".doGet",exc);        
      }
    
    env.dispose();
    }
  
  
  
  protected void writeErrorResponse ( BimServiceEnv  env, HttpServletRequest req, HttpServletResponse resp, int errcode, String msg ) throws IOException  
    {
    env.println(classname+".writeErrorResponse() from "+env.getRemoteAddress()+" url "+req.getRequestURI());
    env.println("   errcode="+errcode+" msg="+msg);

    resp.setContentType("text/html");
    resp.setStatus(errcode);

    PrintWriter out = resp.getWriter();
    
    out.println("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN' >");
    out.println("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
    out.println("<head>");
    out.println("<title>Aetherj error message</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>"+msg+"</p>");
    out.println("</body>");
    out.println("</html>");
    out.close();
    }
  
  
  protected BimPayload getBimPayload ( BimServiceEnv  env, HttpServletRequest req, HttpServletResponse resp) throws IOException 
    {
    // need to pick up the information that is being sent
    BimPayload mimreq = env.getRequest( BimPayload.class );

    if ( ! mimreq.isSignatureValid(env.crypto))
      {
      writeErrorResponse(env, req, resp, AettpStatus.res_Mismatch, "Signature is NOT valid");
      return null;
      }
    
    if ( ! env.isNonceValid(mimreq.node_public_key, mimreq.nonce, mimreq.timestamp))
      {
      writeErrorResponse(env, req, resp, AettpStatus.res_Mismatch, "NOnce is NOT valid");
      return null;
      }
    
    if ( ! mimreq.isPowValid() )
      {
      writeErrorResponse(env, req, resp, AettpStatus.res_Mismatch, "POW is NOT valid");
      return null;
      }
    
    return mimreq;
    }
  
  protected void doPostWork ( BimServiceEnv  env, HttpServletRequest req, HttpServletResponse resp) throws Exception
    {
    writeErrorResponse(env,req,resp,AettpStatus.res_NOTFound,"UNSUPPORTED url");
    }
  
  public final void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException 
    {
    BimServiceEnv env = new BimServiceEnv(this,req).setIdent(classname+"doPost");

    try
      {
      resetStartTime();
      doPostWork(env,req,resp);
      }
    catch ( Exception exc )
      {
      env.println(classname+"doPost",exc);        
      }
    
    env.dispose();
    }
  }
