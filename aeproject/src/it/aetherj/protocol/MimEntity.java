/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;

import com.fasterxml.jackson.annotation.*;

/**
 * This replaces a generic string when talking about "identifiers"
 * It is a way to make sure that you are actually passing a MimEntiry identifier around
 */
public class MimEntity implements Comparable<MimEntity>
  {
  public static final int entity_len_max=20;
  
  private final String aentity;  // The Aether entity value
  private final String amethod;  // the Web method to call for post requests, including initial slash
  
  private final String hashkey;
  
  @JsonCreator
  public MimEntity(String p_value)
    {
    this(p_value,"direct"); 
    } 
  
  public MimEntity(String p_aentity, String p_amethod)
    {
    if ( p_aentity == null ) 
      throw new IllegalArgumentException("MimEntity: value == null ");

    aentity = p_aentity;
    amethod=p_amethod;
  
    hashkey=aentity+"."+amethod;
    }
  
  /**
   * Add this object to part of the things
   * @param digest
   */
  public void digestPart(MimDigest digest)
    {
    digest.digestPart(aentity);
    }

  public String getValue ()
    {
    return aentity;
    }
  
  public String getMethod ()
    {
    return amethod;
    }
  
  public MimEndpoint getIndexEndpoint ()
    {
    return new MimEndpoint(aentity+"_index");
    }
    
  @Override
  public int compareTo(MimEntity input)
    {
    if ( input == null ) return -1;
    
    return aentity.compareTo(input.aentity);
    }

  /**
   * Equality is based on the value not on the object itself
   */
  @Override
  public final boolean equals ( Object input )
    {
    // if it is the same pointer, then it is obviously the same
    if ( input == this ) return true;
    
    if ( ! ( input instanceof MimEntity )) return false;

    MimEntity a_input = (MimEntity)input;
    
    return hashkey.equals(a_input.hashkey);
    }

  @Override
  public int hashCode()
    {
    return hashkey.hashCode();
    }

  
  /**
   * SInce this is a JsonValue it MUST return the value only
   */
  @JsonValue
  public String toString()
    {
    return aentity;
    }

  }
