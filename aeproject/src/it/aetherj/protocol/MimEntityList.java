/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;

/**
 * The string constants MUST be unique in this class
 * I could have it by making it a class and having an initialized on a unique hash map
 */
public final class MimEntityList
  {
  public static final MimEntity webuser    = new MimEntity("WebUser");      // this is a web user, it may be different than a Mim user
  public static final MimEntity wusub      = new MimEntity("WebUserSub");   // To web user subscriptions
  public static final MimEntity brdowner   = new MimEntity("BoardOwner");   // Board Owner has a special handling since it is an array
  public static final MimEntity aetstamp   = new MimEntity("aeTstamp");     // To manage timestamps
  public static final MimEntity aecache    = new MimEntity("aeCache");      // To manage Cache
  public static final MimEntity aewanted   = new MimEntity("aeWanted");     // To manage Wanted MimFIngerprint
  public static final MimEntity aewpow     = new MimEntity("aeWpow");       // To manage Web pow calculation

  public static final MimEntity mimwwwroot = new MimEntity("mimwwwroot","/v0");    // used in Mim default servlet

  public static final MimEntity pingstatus = new MimEntity("status","/v0/ping/status");   // GET only
  public static final MimEntity aestatus   = new MimEntity("status","/v0/status");

  public static final MimEntity pingnode   = new MimEntity("node","/v0/ping/node");       // a mirror of /
  public static final MimEntity aenode     = new MimEntity("node","/v0/node");
  
  public static final MimEntity bootstrap  = new MimEntity("bootstrappers","/v0/bootstrappers");
  public static final MimEntity aeaddres   = new MimEntity("addresses","/v0/addresses");
  
  public static final MimEntity aekey      = new MimEntity("keys"       ,"/v0/c0/keys");
  public static final MimEntity aeboard    = new MimEntity("boards"     ,"/v0/c0/boards");
  public static final MimEntity aethread   = new MimEntity("threads"    ,"/v0/c0/threads");
  public static final MimEntity aepost     = new MimEntity("posts"      ,"/v0/c0/posts");
  public static final MimEntity aevote     = new MimEntity("votes"      ,"/v0/c0/votes");
  public static final MimEntity aetrustate = new MimEntity("truststates","/v0/c0/truststates");
  
  // reverse connection cannot really be used if we have GWT sharing the same port
  public static final MimEntity aerevconn  = new MimEntity("revconn",   "/v0/revconn");
  
  // All usable entities, in the correct order
  public static final MimEntity []entityList = { aenode, aekey,  aeboard, aethread, aepost, aevote };

  
  public static final MimEntity bimwwwroot = new MimEntity("bimwwwroot","/bim");   // used in Bim default servlet

  public static final MimEntity bimstatus  = new MimEntity("bimstatus","/bim/status");
  public static final MimEntity bimaddress = new MimEntity("bimaddress","/bim/address");
  public static final MimEntity aechat     = new MimEntity("aechat","/bim/c0/chat");
  public static final MimEntity bimnode    = new MimEntity("bimnode","/bim/node");
  
  
  public static final MimEntity getEntity(String mim_name)
    {
    if ( mim_name == null )
      return null;
    
    if ( mim_name.equals(pingnode.getValue()))
      return pingnode;

    if ( mim_name.equals(bootstrap.getValue()))
      return bootstrap;

    if ( mim_name.equals(aestatus.getValue()))
      return aestatus;

    if ( mim_name.equals(aenode.getValue()))
      return aenode;

    if ( mim_name.equals(aethread.getValue()))
      return aethread;

    if ( mim_name.equals(aeboard.getValue()))
      return aeboard;

    if ( mim_name.equals(aepost.getValue()))
      return aepost;

    if ( mim_name.equals(aevote.getValue()))
      return aevote;

    if ( mim_name.equals(aekey.getValue()))
      return aekey;

    if ( mim_name.equals(aeaddres.getValue()))
      return aeaddres;

    if ( mim_name.equals(aetrustate.getValue()))
      return aetrustate;

    return null;
    }
  
  }
 
