/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.*; 

/**
 * It is defined as simple string in Aethergo, I need a way to have a proper handling 

type Fingerprint string // 64 char, it is actually a hex of a binary hash

Fingerprints are how objects refer to other. 
A fingerprint is a SHA256 hash of the object. 
Signatures are how objects are linked to their creators. 
A signature is an ECDSA signature with the user's private key, and can only be created by the user creating the object. 
Signatures are optional, it's OK to be anonymous and thus have no key and no signatures. 
Proof of Work provides protection against spam and DDoS by creation of objects computationally expensive enough 
to make bulk creation infeasible.

HSQLDB

if you wish to see the hex of a binary use rawtohex eg:

select  rawtohex(aewant_fprint) as aahex, aewant_tbl.* from aewant_tbl


 */
public final class MimFingerprint implements MimDigestable
  {
  public static final int bytes_len=32;   // length of a fingerprint in bytes (NOT the hex, that binary bytes)
  
  private byte []fingerprint;
    
  /**
   * there are quite a few places where a "null" fingerprint is needed
   */
  public MimFingerprint()
    {
    fingerprint=null;
    }
  
  /**
   * This wants an ALREADY CALCULATED fingerprint in HEX format
   */
  @JsonCreator
  public MimFingerprint(String fingerprint_hex)
    {
    if ( fingerprint_hex == null || fingerprint_hex.length() < bytes_len * 2)
      fingerprint=null;
    else
      fingerprint=MimHex.convStringToBin(fingerprint_hex);
    }

  /**
   * Used by dbase, when picking up values
   * Also used to construct a Fingerprint when you have the sha256 hash
   * Also used to generate a "temporary, fake fingerprint while inserting a new user, so, do NOT put a check on the content lenght...
   */
  public MimFingerprint(byte []fingerprint_bin)
    {
    fingerprint = fingerprint_bin;
    }
  
  @Override
  public void digestPart(MimDigest digest)
    {
    digest.digestPart(fingerprint);
    }
  
  /**
   * Needed to be able to have unique fingerprints in a list of fingerprints
   */
  @Override
  public boolean equals ( Object input )
    {
    if ( input instanceof MimFingerprint )
      {
      // instanceof also handles null
      MimFingerprint ofinger = (MimFingerprint)input;
      // Arrays.equals takes care of null values
      return Arrays.equals(fingerprint, ofinger.fingerprint);
      }

    return false;
    }

  @Override
  public int hashCode()
    {
    return isNull() ? 0 : fingerprint.hashCode();
    }
  
  /**
   * Create a new fingerprint by digesting the given string
   * @param input
   */
  public static MimFingerprint newFingerprint ( String input ) 
    {
    return new MimFingerprint(calculate(input));
    }
  
  public static MimFingerprint newFingerprint ( MimDigest digest ) 
    {
    return new MimFingerprint(digest.digestEnd());
    }
  

  public boolean verify ( String input ) throws UnsupportedEncodingException
    {
    if ( fingerprint==null || input == null || input.length() < 1 )
      return false;
          
    return Arrays.equals(fingerprint, calculate(input));
    }
  
  private static byte []calculate ( String input ) 
    {
    MimDigest digestAlgo = new MimDigest();
    return digestAlgo.digestOnce(input);
    }

  public boolean isInvalid ()
    {
    return fingerprint==null || fingerprint.length < bytes_len;
    }
  
  public boolean isNull ()
    {
    return fingerprint==null;
    }
  
  public byte[] getFingerprint()
    {
    return fingerprint;
    }
  
  @JsonValue
  public String toString()
    {
    if ( fingerprint == null )
      return "";
    
    return MimHex.convBinToHexString(fingerprint);
    }

  }
