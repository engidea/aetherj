/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;

// This should hold CONSTANTS that are used to talk between various Aether implementations
// You can change the .java but you MUST NOT change the .H since that is generated FROM the java by
// running the shell and awk script that is in this directory
// to make things SIMPLE comments MUST start with a // so they are easy to spot
// The translator will look for the following
// line begins with a // then the whole line is copied to output
// line begins with public, or there is a { or a } then the line is skipped
// line begins with String then it will be parsed for the name, skip the = get the actual string, skip the semicolon copy the rest...
// constants that begins with cmd_name_ are commands to be used with the cmd_tbl
// NOTE on row_range: it is a String that is a sequence of numbers separated by space to indicate specific rows or there is a dash between to indicate a range

public interface AetherCo
  {
  String DIGEST_ALGO="SHA-256";  // Use this when you need a digest
  String POW_CHARSET="UTF-8";

  String gwtwapp_context="/aetherjweb";
  String mimwapp_context="/v0";
  String bimwapp_context="/bim";
  
  int month_JANUARY = 1;
  int month_FEBRUARY = 2;
  int month_MARCH = 3;
  int month_APRIL = 4;
  int month_MAY = 5;
  int month_JUNE = 6;
  int month_JULY = 7;
  int month_AUGUST = 8;
  int month_SEPTEMBER = 9;
  int month_OCTOBER = 10;
  int month_NOVEMBER = 11;
  int month_DECEMBER = 12;
  
  }




