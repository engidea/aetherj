/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;
 
import java.io.Serializable;

import com.fasterxml.jackson.annotation.*;

/**
 * Helper to properly format the POW    
 * This has to stay here since it will need to be used GWT client side  
 * NOTE the use of JsonCreator and JsonValue to hide the entire inner behavior  
 * The contract is that if any value is invalid an empty string is emitted  
 */
public class MimPowValue implements Serializable
  {
  private static final long serialVersionUID = 1L;

  // used to allocate space in DB
  public static final int pow_ascii_len=1024;
  public static final int pow_difficulty_max=22;
    
  private static final String begins="MIM1:";
  
  private int difficulty;
  private String salt;
  private int counter=0;
  private String signature;
  
  private boolean isValid=false;  // will become true once it is verified
  private int powPerSecond;       // Used to give feedback to user on how many pow per second I am able to calculate
  private int incrementalPowStep; // Used to give feedback to user on the step I am currently in

  /**
   * It is legal to have an "empty" pow value, you need it since the GO logic is to have a Json with an empty string 
   * It would have been easier if no pow field was present at all.
   */
  public MimPowValue()
    {
    }
  
  @JsonCreator
  public MimPowValue(String formatted)
    {
    if ( formatted == null ) 
      return;
    
    if ( ! formatted.startsWith(begins)) 
      return;
    
    try
      {
      String [] split = formatted.split(":");
      difficulty = Integer.parseInt(split[1]);
      salt= split[5];
      counter = Integer.parseInt(split[6]); 
      signature= split[7];
      }
    catch ( Exception exc )
      {
      }
    }

  
  /**
   * This is when you calculate Pow and have an initial setting
   * Also, you might initialize a Pow to do a step by step calculation
   * @param difficulty
   * @param salt
   * @param counter
   */
  public MimPowValue ( int difficulty, String salt )
    {
    this.difficulty=difficulty;
    this.salt=salt;
    }
  
  
  /**
   * Add this object to part of the things
   * @param digest
   */
  public void digestPart(MimDigest digest)
    {
    digest.digestPart(toString());
    }


  /**
   * Call this to increment the steps and report how many pow per seconds I am doing
   * @param powPerSecond
   */
  public void setCounterIncStepCalcPows ( int initialCounter, int counter )
    {
    setCounter(counter);
    
    incrementalPowStep++;
    powPerSecond=counter-initialCounter;
    }

  /**
   * When you have the elapsed time and you know the counter started at zero
   * @param initialCounter
   * @param counter
   */
  public void setCounterIncStepCalcPows ( int counter, long start_time_ms, long end_time_ms )
    {
    setCounter(counter);
    
    incrementalPowStep++;
    
    int delta_s = (int)(end_time_ms - start_time_ms)/1000;;
    
    powPerSecond=delta_s > 0 ? counter/delta_s : counter;
    }

  
  public int getStep () 
    {
    return incrementalPowStep;
    }

  public int getPowPerSecond()
    {
    return powPerSecond;
    }
  
  /**
   * A POW is null if difficulty is <= 0
   * @return true if this Pow is logically null
   */
  public boolean isNull()
    {
    return difficulty <= 0;
    }
  
  public boolean isValid ()
    {
    return isValid;
    }
  
  /**
   * A POW is valid if the source + value result in the same POW
   * @param valid
   * @return myself, so you can concatenate events
   */
  public MimPowValue setValid ( boolean valid )
    {
    isValid = valid;
    
    return this;
    }
  
  public int getDifficulty()
    {
    return difficulty;
    }
  
  public void setCounter(int counter)
    {
    this.counter=counter;
    }
  
  public int getCounter()
    {
    return counter;
    }

  public String getSalt()
    {
    return salt;
    }

  /**
   * Add the given signature to this POW and return itself for further use
   * @param signature
   * @return
   */
  public MimPowValue getSigned ( MimSignature signature )
    {
    this.signature=signature.toString();

    return this;
    }
    
  /**
   * Careful, you CANNOT change this method since it is used in fingerprinting !!
   */
  @JsonValue
  public String toString()
    {
    if ( counter == 0 )
      return "";
          
    StringBuilder risul = new StringBuilder(500);
    
    risul.append(begins);
    risul.append(difficulty);
    risul.append("::::");
    risul.append(salt);
    risul.append(":");
    risul.append(counter);
    risul.append(":");

    if ( signature != null )
      risul.append(signature);
    
    return risul.toString();
    }

  }

