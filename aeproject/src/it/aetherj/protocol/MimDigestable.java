package it.aetherj.protocol;

/**
 * A mim object is digestable if it implements this
 * For a superclass this is the MINIMUM that should be digested, so, when you need extra fields you just add them in inherited method
 */
public interface MimDigestable
  {
  public void digestPart(MimDigest digest);

  }
