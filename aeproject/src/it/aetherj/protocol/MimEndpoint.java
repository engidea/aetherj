package it.aetherj.protocol;

import com.fasterxml.jackson.annotation.*;

/**
 * As from postrespgenerator.go look for resp.Endpoint
 * Also server.go 
 * Looking at the indexes, the endpoint is the same as the Entity
 */
public class MimEndpoint
  {
  private final String endpoint;

  public static final String post_response="post_response";
  public static final String endpoint_entity="entity";
  public static final String bootstrappers="bootstrappers";
  public static final String mainfest="manifest";
  
  @JsonCreator 
  public MimEndpoint(String p_value)
    {
    endpoint = p_value;
    }
  
  public MimEndpoint ( MimEntity entity )
    {
    endpoint = entity.getValue();
    }
  
  /**
   * Add this object to part of the things
   * @param digest
   */
  public void digestPart(MimDigest digest)
    {
    digest.digestPart(endpoint);
    }

  
  /**
   * SInce this is a JsonValue it MUST return the value only
   */
  @JsonValue
  public String toString()
    {
    return endpoint;
    }
  }
