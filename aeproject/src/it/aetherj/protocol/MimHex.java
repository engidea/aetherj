/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;

/**
 * Need to be sure that Hex conversion is done as desired by mim, lowercase 
 */
public class MimHex
  {
  private static final String classname="MimHex";
  
  public static final String convNibbleToHexString ( byte anibble )
    {
    switch ( anibble )
      {
      case 0:  return "0";
      case 1:  return "1";
      case 2:  return "2";
      case 3:  return "3";
      case 4:  return "4";
      case 5:  return "5";
      case 6:  return "6";
      case 7:  return "7";
      case 8:  return "8";
      case 9:  return "9";
      case 10: return "a";
      case 11: return "b";
      case 12: return "c";
      case 13: return "d";
      case 14: return "e";
      case 15: return "f";
      default: return "?";        
      }
    }

  
  public static final String convBinToHexString ( byte abyte )
    {
    // MSB
    byte anibble  = (byte)((abyte >> 4) & 0x0F);
    String risul = convNibbleToHexString(anibble);          
    // LSB
    anibble = (byte)(abyte & 0x0F);
    return risul + convNibbleToHexString(anibble);          
    }

  /**
   * Convert the given byte array into a sequence of HEX values, in the order of the array
   * @param value the byte array to convert, if null or empty then an empty string will be returned
   * @return
   */
  public static final String convBinToHexString ( byte [] value )
    {
    if ( value == null ) 
      return "";
    
    if ( value.length < 1 ) 
      return "";
    
    int vaLen = value.length;
    
    // So I avoid copying because risul is too short
    StringBuilder risul = new StringBuilder(vaLen * 2 + 10);
    
    for (int index=0; index<vaLen; index++)
      risul.append(convBinToHexString(value[index]));

    return risul.toString();
    }

  public static final byte[] convStringToBin ( String input )
    {
    if ( input == null ) 
      return null;
    
    if ( input.length() < 1 ) 
      return new byte[0];
    
    if ( (input.length() % 2) != 0)
      throw new IllegalArgumentException(classname+"convStringToBin: ODD string len="+input.length());
      
    int forlen = input.length() / 2;
    
    byte[] risul = new byte[forlen];
    
    for (int index=0; index<forlen; index++)
      {
      int abyteIndex = index*2;
      String abyte = input.substring(abyteIndex, abyteIndex+2);
      int aval = Integer.parseInt(abyte,16);
      risul[index] = (byte)(aval & 0xFF);
      }
  
    return risul;
    }

  }
