/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;

import com.fasterxml.jackson.annotation.*;

/**
 * It is defined as simple string in Aethergo, I need a way to have a proper handling 

// type Signature [512]byte
type Signature string // temp

 *
 */
public final class MimSignature
  {
  // A signature is a SHA256 signed and converted to HEX
  // NOTE the actual signing DOUBLES the 32bytes SHA to a 64 bytes
  public static final int signature_len=64;
  
  private byte[] signature;
  
  public MimSignature()
    {
    }
  
  @JsonCreator
  public MimSignature(String signature_hex)
    {
    if ( signature_hex == null || signature_hex.length() < signature_len * 2 )
      return;
    
    this.signature=MimHex.convStringToBin(signature_hex);
    }

  public MimSignature(byte [] signature)
    {
    this.signature=signature;
    }
  
  /**
   * Add this object to part of the things
   * @param digest
   */
  public void digestPart(MimDigest digest)
    {
    digest.digestPart(signature);
    }

  public byte[]getBytes()
    {
    return signature;
    }
  
  public boolean isEmpty ()
    {
    return signature==null || signature.length < signature_len;
    }
  
  @JsonValue
  public String toString()
    {
    if ( signature == null )
      return "";
    
    return MimHex.convBinToHexString(signature);
    }
  }
