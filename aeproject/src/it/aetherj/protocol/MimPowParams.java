package it.aetherj.protocol;

public class MimPowParams
  {
  private int difficulty;   // number of bits to be zero for acceptance
  private int timeout_s;    // seconds timeout

  /**
   * Use this one when you wish to use this as parameters for calculation
   * @param difficulty
   * @param timeout_s
   */
  public MimPowParams ( int difficulty, int timeout_s )
    {
    this.difficulty=difficulty;
    this.timeout_s=timeout_s > 20 ? timeout_s : 20;
    }

  public int getTimeout_s ()
    {
    return timeout_s;
    }
  
  public int getDifficulty ()
    {
    return difficulty;
    }
  }
