/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;

import java.security.MessageDigest;
import java.util.Random;

import it.aetherj.backend.gui.config.AetherParams;

/**
 * This is used by anybody (even a web client) to calculate proof of work The
 * original is proofofwork.go this one retains the logic
 * Since this is used by BOTH Java server + GWT Client (web browser) you MUST not put dependent parts in it
 * NOTE that the way things are done you have to have the server provide the raw string for POW and the web client
 * to do the calculation that will be sent over the wire to server
 */
public final class MimPowCalculator
  {
  // this is a default set of params, reasonable for normal entities
  public static MimPowParams powParams = new MimPowParams(21, 600);

  // I want to have a bitmask to see if any of the leftside bits are 1
  // All bytes (even in LE CPU) have MSB on the left and if we just want ONE bit, the mask to apply is 0x80 
  private static final int MASKS [] = { 0, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE };

  private final MessageDigest digestAlgo;
  private final Random randomAlgo;

  public MimPowCalculator() 
    {
    digestAlgo = newSha256();
    randomAlgo = new Random(System.currentTimeMillis());
    }
  
  private MessageDigest newSha256 ()
    {
    try
      {
      return MessageDigest.getInstance(AetherCo.DIGEST_ALGO);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      return null;
      }
    }
  
  /**
   * Pitfalls: encoding conversion, number of digest used
   * @param input
   * @return
   */
  public byte [] powDigest ( String input ) 
    {
    try
      {
      // every time digest() is called the "engine" is reset
      byte [] out = digestAlgo.digest(input.getBytes(AetherCo.POW_CHARSET));
      out = digestAlgo.digest(out);
      return digestAlgo.digest(out);
      }
    catch ( Exception exc )
      {
      // hopefully this is portable in the browser too...
      exc.printStackTrace();
      return new byte[0];
      }
    }
  
  private static final String LETTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  private static final int    SEED_LEN = 16;

  public String newRandomSeed () 
    {
    final int charsLen = LETTERS.length();
    
    StringBuilder seedString = new StringBuilder(SEED_LEN);
    
    for (int index = 0; index < SEED_LEN; index++) 
      seedString.append(LETTERS.charAt(randomAlgo.nextInt(charsLen)));
    
    return seedString.toString();     
    }
  
  /**
   * The test starts from the beginning of the array, index 0
   * @param buffer
   * @param bytesCount how many bytes to check for zero
   * @param bitsCount how man of the remaining bits to check
   * @return true if it has enough zeros at the beginning
   */
  private boolean hasEnoughZeros ( byte [] buffer, int bytesCount, int bitsCount )
    {
    int index;
    
    for ( index=0; index<bytesCount; index++)
      if ( buffer[index] != 0 )
        return false;

    // Leave the generic algorithm, it works even when bitsCount==0
    int lastByte = buffer[index] & 0xFF;

    // it has all bits if the last byte anded with the mask is actually zero
    return ( lastByte & MASKS[bitsCount]) == 0;
    }

  /**
   * Execute one step of POW calculation, to be used by the GWT, and a mechanism to avoid getting the browser stuck
   * Crappy javascript
   * @param powValue it is used to pass the difficulty and the salt and keep track on where we have arrived with the counter
   * @param input the input to be powed
   * @return true if I have to keep going or false if the wanted pow has been found
   */
  public boolean executePowUnsignedStep ( MimPowValue powValue, String input )
    {
    final int difficulty = powValue.getDifficulty();
    
    StringBuilder inputToBePoWd = new StringBuilder(input.length()+200);
    inputToBePoWd.append(difficulty);
    inputToBePoWd.append(input);
    inputToBePoWd.append(powValue.getSalt());
    
    final int input_len = inputToBePoWd.length();
    
    final int w_zeroBytesCount = difficulty / 8;
    final int w_zeroBitsCount = difficulty % 8;
    
    long timeout = System.currentTimeMillis() + 1000;

    int timeoutPacifier=0; 
    int initialCounter=powValue.getCounter();
    int counter=initialCounter;

    for(;;)
      {
      inputToBePoWd.setLength(input_len);
      inputToBePoWd.append(counter);
      
      byte []hash = powDigest (inputToBePoWd.toString());
      
      if ( hasEnoughZeros(hash, w_zeroBytesCount, w_zeroBitsCount))
        {
        powValue.setCounter(counter);
        return false;
        }
      
      counter++;

      // if no enough cycles have passed for testing the timeout
      if ( timeoutPacifier++ < 1000 ) continue;
      
      timeoutPacifier=0;
      
      if ( System.currentTimeMillis() > timeout )
        break;
      
      }

    powValue.setCounterIncStepCalcPows(initialCounter,counter);
    return true;
    }
    
  /**
   * Calculate an unsigned POW given the input, this will just keep working until result
   * To be used by the Java server
   * @return the calculated POW value, not signed
   */
  public MimPowValue newPowUnsigned ( MimPowParams params, String input )
    {
    final int difficulty = params.getDifficulty();
    final int powTimeout_s = params.getTimeout_s();
    
    if ( difficulty > MimPowValue.pow_difficulty_max )
      throw new IllegalArgumentException("difficulty "+difficulty+" is too big");

    if ( powTimeout_s < AetherParams.min_PowTimeout_s || powTimeout_s > AetherParams.max_PowTimeout_s )
      throw new IllegalArgumentException("bailout_s "+powTimeout_s+" is < 10 || > 600");

    String salt = newRandomSeed();
    
    final long initial_time_ms = System.currentTimeMillis(); 

    // the future time to check to decide if timeout
    final long timeout_future = initial_time_ms + powTimeout_s * 1000;
    
    StringBuilder inputToBePoWd = new StringBuilder(input.length()+200);
    inputToBePoWd.append(difficulty);
    inputToBePoWd.append(input);
    inputToBePoWd.append(salt);
    
    final int input_len = inputToBePoWd.length();

    final int w_zeroBytesCount = difficulty / 8;
    final int w_zeroBitsCount = difficulty % 8;
    
    int counter=0;
    int timeoutPacifier=0; 
    
    // initial set of risul, difficulty, salt, will remain constant
    MimPowValue risul = new MimPowValue(difficulty,salt);
    
    for(;;)
      {
      inputToBePoWd.setLength(input_len);
      inputToBePoWd.append(counter);

      byte []hash = powDigest (inputToBePoWd.toString());
      
      if ( hasEnoughZeros(hash, w_zeroBytesCount, w_zeroBitsCount))
        break;
      
      counter++;

      // if no enough cycles have passed for testing the timeout
      if ( timeoutPacifier++ < 10000 ) continue;
      
      timeoutPacifier=0;
      
      if ( System.currentTimeMillis() > timeout_future)
        throw new IllegalArgumentException("difficulty is too high for the allotted time");
      }
    
    risul.setCounterIncStepCalcPows(counter, initial_time_ms, System.currentTimeMillis());
    
    return risul;
    }
  
  /**
   * Test if the given pow is valid with the given input
   * Note that there is no timeout here since it is a test on a single, specific, value
   * @param input
   * @return
   */
  public boolean isPowValid ( String input, MimPowValue pow)
    {
    int difficulty = pow.getDifficulty();
    
    String inputToBePoWd = difficulty + input + pow.getSalt();
    
    int w_zeroBytesCount = difficulty / 8;
    int w_zeroBitsCount = difficulty % 8;

    byte []hash = powDigest (inputToBePoWd + pow.getCounter());
    
    return hasEnoughZeros(hash,w_zeroBytesCount, w_zeroBitsCount);
    }
  
  
  }
