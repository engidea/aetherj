package it.aetherj.protocol;

import java.security.MessageDigest;

/**
 * Quite often I need to create an engine for sha256 and there is the usual issue of the exception
 * So, wrap it up and enclose all peculiarities here, including the digesting of strings
 * AHHHH clone() is not supported on MessageDigest in JS, crap JS, so, cannot store the current status of the digest...
 */
public class MimDigest 
  {
  public static final String digest_charset="UTF-8";

  private final MessageDigest digestAlgo;

  public MimDigest()  
    {
    digestAlgo = newSha256();
    }
    
  private MessageDigest newSha256 ()
    {
    try
      {
      return MessageDigest.getInstance(AetherCo.DIGEST_ALGO);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      return null;
      }
    }

  public void reset()
    {
    digestAlgo.reset();
    }
    
  public void digestPart ( int value )
    {
    digestPart(Integer.toString(value));
    }
  
  public void digestPart ( String source )
    {
    if ( source == null )
      return;
    
    try
      {
      byte []b_input = source.getBytes(digest_charset);
      digestAlgo.update(b_input);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      }
    }

  public void digestPart ( byte [] source )
    {
    if ( source == null )
      return;
    
    digestAlgo.update(source);
    }

  public byte[] digestEnd ()
    {
    return digestAlgo.digest();
    }
    
  public byte[] digestOnce (String source)
    {
    if ( source == null )
      return null;
    
    try
      {
      byte []b_input = source.getBytes(digest_charset);
      return digestAlgo.digest(b_input);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      return new byte[0];
      }

    }
    
  
  }
