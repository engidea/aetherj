#!/bin/bash

if [ "$1" == "cleanwar" ]; then
   rm websrv/gwtwar.war 
   rm websrv/mimwar.war
   rm websrv/bimwar.war
fi

if [ "$1" == "cleandatastore" ]; then
   rm datastore/aetherdb.lobs
   rm datastore/aetherdb.properties
   rm datastore/aetherdb.script
   rm datastore/parameters.json
fi

exec java  -jar aetherj.jar
